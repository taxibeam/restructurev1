/* ------------------------------------------------
 *		Configuration
 * ------------------------------------------------ */

module.exports = {


	PRODUCTION: {
		APP_NAME: "Taxi",
		APP_VERSION: "1.0.7",
		passengerappversion_android: "2.0.32",
		passengerappversion_ios: "1.0.39",
		psgsmilesappversion_android: "0.1.0",
		psgsmilesappversion_ios: "0.1.0",
		mobileappversion: "1.0.55",
		smilesappversion: "0.2.1056",

		SERVER_PORT: 4416,

		//SOCKETIO_PORT: 1115,
		DB_HOST: "ecvttaxidb1.ecartstudio.com",
		DB_PORT: "27017",
		DB_NAME: "taxi",
		DB_USER: "",
		DB_PASSWORD: "",

		SAMSUNG_API_KEY: "2212224236248",
		KKNBP_API_KEY: "34121231231",
		BEAMPARTNER_API_KEY: "3133263393412",

		BOONTERM_API_KEY: "wjU5pblaGrP7vEsgqN928HdtxDVeLc", 		
		BT_API_KEY: "DWI9sHCS7leYU8nvuo1ENyGf4BQz3K",
		HALOBEAM_API_KEY: "DaJHZwYe97IfWcBhEl3Ar",

		available_country: ['th'],
		BSessionTimeout: 300000,
		
		PWD_KEY: "84494b5a", 		// PWD_KEY สำหรับเข้ารหัส password

		testhostcallcenter: "callcenter-test.taxi-beam.com",
		hostcallcenter: "callcenter.taxi-beam.com",

		smshostname: "sms.narun.me",
		smshostport: 443,
		smshostpath: "/SMSMeJson.svc/SendMessage",
		smshostmethod: "POST",
		smssender: "TaxiBeam",

		SMTPSentEmail: "taxibeam9@gmail.com",
		SMTPSentPass: "TaxiBeam!0!",
		InfoEmail: "txbm_support@taxi-beam.com",
		AlertEmail: "txbm-alert@taxi-beam.com",

		MoneyPerUse: 20,
		TimePerUse: 15,

		MoneyPerUseAPP: 20,
		TimePerUseAPP: 15,

		shiftprice: 20,
		ccsearchpsgradian: 10000,
		ccsearchpsgamount: 30,

		smilesprice: 10,

		ccsearchdrvradian: 100000,
		ccsearchdrvamount: 500,

		psgsearchpsgradian: 100000,
		psgsearchpsgamount: 30,

		drvsearchpsgradian: 10000,			// รัศมีการ bradcast งานของ passenger : meters
		drvsearchpsgamount: 10,				// จำนวนคันในการจ่ายงานใกล้สุด

		distoShowPhone: 15,					// หน่วยเป็น km.
		psgSearchDrvInterval: 60000,		// interval for psg app to search drv in millisecond  
		psgGetstatusInterval: 10000,		// interval for psg app to interval getstatus
		psgGetDrvInterval: 30000,			// interval for psg app to interval matched drv

		drvlogservicehost: '192.168.16.45',
		drvlogserviceport: 3003,
		drvlogservicepath: '/service/silver/drvsvsts',
		drvlogservicemethod: 'POST',

		drvSearchPsgInterval: 5000,
		drvGetstatusInterval: 5000,

		biddingtimeoutdelayprocess: 2000,

		halodrvsearchradian: 5000,
		halodrvsearchamount: 10,

		samsunghost: 'localhost',
		samsungport: 80,
		samsungpath: '/g_services/v1',

		drvlogservice: false,

		ccgroupNSP: [
			{ "name": "ccubon" }, 			// อู่โตโต้
			{ "name": "ubnvip" }, 			// อู่ แท็กซี่vip
			{ "name": "ubngservice" }, 		// อู่บริการดี
			{ "name": "ubnskt" }, 			// อุบล
			{ "name": "ubntaxiubn" },			// Taxi Ubon
			{ "name": "nmachai" },			// Taxi ชัยพัฒนยนต์จำกัด โคราช
			{ "name": "nmadpromt" }			// Taxi ดีพร้อม โคราช
		],

		'facebookAuth': {
			'clientID': '201498690221911', // your App ID
			'clientSecret': '2d88f0b22514f6b5143f5c03f5b39753', // your App Secret
			'callbackURL': 'http://lite-test.taxi-beam.com/auth/facebook/callback'
		},

		'googleAuth': {
			'clientID': '143053232828-oo83ae6jnltp73g1qdf5af2fbfhsqtti.apps.googleusercontent.com',
			'clientSecret': 'FOaFOrVB_AR5_upAHsYOvnWd',
			'callbackURL': 'http://lite-test.taxi-beam.com/auth/google/callback'
		}

	}

}