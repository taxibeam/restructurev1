
/* ------------------------------------------------
 *		Configuration
 * ------------------------------------------------ */

var CONFIG = require('./config.js');

module.exports = {

	/*=== production ======================================================*/

	production: {

		SAMSUNG_API_KEY: CONFIG.PRODUCTION.SAMSUNG_API_KEY,

		KKNBP_API_KEY: CONFIG.PRODUCTION.KKNBP_API_KEY,

		BOONTERM_API_KEY: CONFIG.PRODUCTION.BOONTERM_API_KEY,

		BEAMPARTNER_API_KEY: CONFIG.PRODUCTION.BEAMPARTNER_API_KEY,

		BT_API_KEY: CONFIG.PRODUCTION.BT_API_KEY,

		HALOBEAM_API_KEY: CONFIG.PRODUCTION.HALOBEAM_API_KEY,

		available_country: CONFIG.PRODUCTION.available_country,
		BSessionTimeout: CONFIG.PRODUCTION.BSessionTimeout,

		app_name: CONFIG.PRODUCTION.APP_NAME,

		app_version: CONFIG.PRODUCTION.APP_VERSION,

		PORT: CONFIG.PRODUCTION.SERVER_PORT,

		socketIoPort: CONFIG.PRODUCTION.SOCKETIO_PORT,

		database: {
			name: "",
			url: "mongodb://" + CONFIG.PRODUCTION.DB_HOST + ":" + CONFIG.PRODUCTION.DB_PORT + "/" + CONFIG.PRODUCTION.DB_NAME
			//url: "mongodb://phuree:DDpasswd!234@cluster0-shard-00-00-ynxki.mongodb.net:27017,cluster0-shard-00-01-ynxki.mongodb.net:27017,cluster0-shard-00-02-ynxki.mongodb.net:27017/taxi?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin"
		},

		pwd_key: CONFIG.PRODUCTION.PWD_KEY,

		passengerappversion_android: CONFIG.PRODUCTION.passengerappversion_android,
		passengerappversion_ios: CONFIG.PRODUCTION.passengerappversion_ios,
		psgsmilesappversion_android: CONFIG.PRODUCTION.psgsmilesappversion_android,
		psgsmilesappversion_ios: CONFIG.PRODUCTION.psgsmilesappversion_ios,

		mobileappversion: CONFIG.PRODUCTION.mobileappversion,
		smilesappversion: CONFIG.PRODUCTION.smilesappversion,

		testhostcallcenter: CONFIG.PRODUCTION.testhostcallcenter,
		hostcallcenter: CONFIG.PRODUCTION.hostcallcenter,

		smshostname: CONFIG.PRODUCTION.smshostname,
		smshostport: CONFIG.PRODUCTION.smshostport,
		smshostpath: CONFIG.PRODUCTION.smshostpath,
		smshostmethod: CONFIG.PRODUCTION.smshostmethod,
		smssender: CONFIG.PRODUCTION.smssender,

		SMTPSentEmail: CONFIG.PRODUCTION.SMTPSentEmail,
		SMTPSentPass: CONFIG.PRODUCTION.SMTPSentPass,
		InfoEmail: CONFIG.PRODUCTION.InfoEmail,
		AlertEmail: CONFIG.PRODUCTION.AlertEmail,

		SMTPSentEmail: CONFIG.PRODUCTION.SMTPSentEmail,
		SMTPSentPass: CONFIG.PRODUCTION.SMTPSentPass,
		InfoEmail: CONFIG.PRODUCTION.InfoEmail,
		AlertEmail: CONFIG.PRODUCTION.AlertEmail,

		MoneyPerUse: CONFIG.PRODUCTION.MoneyPerUse,
		TimePerUse: CONFIG.PRODUCTION.TimePerUse,
		shiftprice: CONFIG.PRODUCTION.shiftprice,
		MoneyPerUseAPP: CONFIG.PRODUCTION.MoneyPerUseAPP,
		TimePerUseAPP: CONFIG.PRODUCTION.TimePerUseAPP,

		smilesprice: CONFIG.PRODUCTION.smilesprice,

		ccsearchpsgradian: CONFIG.PRODUCTION.ccsearchpsgradian,
		ccsearchpsgamount: CONFIG.PRODUCTION.ccsearchpsgamount,

		ccsearchdrvradian: CONFIG.PRODUCTION.ccsearchdrvradian,
		ccsearchdrvamount: CONFIG.PRODUCTION.ccsearchdrvamount,

		psgsearchpsgradian: CONFIG.PRODUCTION.psgsearchpsgradian,
		psgsearchpsgamount: CONFIG.PRODUCTION.psgsearchpsgamount,

		drvsearchpsgradian: CONFIG.PRODUCTION.drvsearchpsgradian,
		drvsearchpsgamount: CONFIG.PRODUCTION.drvsearchpsgamount,

		uploadsPath: CONFIG.PRODUCTION.UPLOADS_PATH,

		distoShowPhone: CONFIG.PRODUCTION.distoShowPhone,
		psgSearchDrvInterval: CONFIG.PRODUCTION.psgSearchDrvInterval,
		psgGetstatusInterval: CONFIG.PRODUCTION.psgGetstatusInterval,
		psgGetDrvInterval: CONFIG.PRODUCTION.psgGetDrvInterval,

		drvSearchPsgInterval: CONFIG.PRODUCTION.drvSearchPsgInterval,
		drvGetstatusInterval: CONFIG.PRODUCTION.drvGetstatusInterval,

		drvlogservicehost: CONFIG.PRODUCTION.drvlogservicehost,
		drvlogserviceport: CONFIG.PRODUCTION.drvlogserviceport,
		drvlogservicepath: CONFIG.PRODUCTION.drvlogservicepath,
		drvlogservicemethod: CONFIG.PRODUCTION.drvlogservicemethod,

		showdrvradian: CONFIG.PRODUCTION.showdrvradian,
		showdrvamount: CONFIG.PRODUCTION.showdrvamount,

		biddingtimeoutdelayprocess: CONFIG.PRODUCTION.biddingtimeoutdelayprocess,

		halodrvsearchradian: CONFIG.PRODUCTION.halodrvsearchradian,
		halodrvsearchamount: CONFIG.PRODUCTION.halodrvsearchamount,

		samsunghost: CONFIG.PRODUCTION.samsunghost,
		samsungport: CONFIG.PRODUCTION.samsungport,
		samsungpath: CONFIG.PRODUCTION.samsungpath,

		drvlogservice: CONFIG.PRODUCTION.drvlogservice,

		ccgroupNSP: CONFIG.PRODUCTION.ccgroupNSP

	}

}