var mongoose = require('mongoose');
var DriversModel = mongoose.model('DriversModel');

module.exports = (io) => {

    io.on('connection', (socket) => {

        socket.on('DRIVER_CONNECT', (id) => {
            socket.join(id);
        });

        socket.on('UPDATE_LAT_LNG', (data) => {
            new DriversModel().updateLatLng(data, (err, result) => { return; });
        });

        socket.on('disconnect', (id) => {
            //console.log('Socket:: connection disconnect.');
        });
    });
}