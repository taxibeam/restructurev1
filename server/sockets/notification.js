var mongoose = require('mongoose') ;
var SocketsModel = mongoose.model('SocketsModel');

module.exports = function (io) {

    io.on('connection', function (socket) {

        socket.on('BACKGROUND_START', function (id) {
            socket.join(id);
            console.log(id);
        });

        socket.on("BACKGROUND_TERMINATE", function (id) {
        });
    });
};