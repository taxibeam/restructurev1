var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ShareActivitiesLog = new Schema({
    sharessucceed: { type: Boolean, default: false },
    share_fbdata: {},
    sharedate: { type: Date, trim: true, default: Date.now },
    created: { type: Date, trim: true, default: Date.now },
    expired: { type: Date, trim: true, default: Date.now },
    hashtag: { type: String },
    shareimg: { type: String, trim: true },
    sharelink: { type: String, trim: true },
    detail: { type: String, trim: true },
    topic: { type: String, trim: true },
    sharetype: { type: String, trim: true },
    _id_job: { type: Schema.Types.ObjectId, ref: 'JoblistdetailModel' },
    _id_drv: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
    _id_psg: { type: Schema.Types.ObjectId, ref: 'PassengerModel' },
    _id_share: { type: Schema.Types.ObjectId, ref: 'ShareActivitiesModel' },

},
    {
        collection: 'ShareActivitiesLog',
        versionKey: false,
        strict: false
    }
);

mongoose.model('ShareActivitiesLogModel', ShareActivitiesLog);
