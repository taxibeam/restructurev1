var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

	var Session = new Schema({
		session: { type: String },
		expires: { type: Date },
	},
	{
		collection: 'sessions', 
		versionKey: false,
		strict : false
	});

mongoose.model('SessionModel', Session);