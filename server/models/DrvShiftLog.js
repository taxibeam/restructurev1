var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var DrvShiftLog = new Schema(
	{
		action: { type: String },
		job_id: { stype: String },
		_id_jobhotel: { type: Schema.Types.ObjectId, ref: 'JoblisthotelModel' },	// When refre job from hotel timeout		
		_id_psg: { type: Schema.Types.ObjectId, ref: 'PassengerModel' },
		_id_drv: { type: Schema.Types.ObjectId, ref: 'DriversModel' }

	},
	{
		collection: 'drvshiftlog',
		versionKey: false,
		strict : false
	}
);

mongoose.model('DrvShiftLogModel', DrvShiftLog);