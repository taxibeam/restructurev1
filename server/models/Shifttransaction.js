var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Shifttransaction = new Schema(
	{		
		DateTime: { type: Date, default: Date.now },
		AdminID: { type: String },
		Type: { type: String },				// Add, Use, Refund, 
        Amount: { type: Number },
		PostShift:  [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				Amount: { type: Number, trim: true }
			}
		],		
		PreShift:  [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				Amount: { type: Number, trim: true }
			}
		],		
		AccountIDs:  [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' }
			}
		]
	},
	{
		collection: 'shifttransaction', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('ShifttransactionModel', Shifttransaction);