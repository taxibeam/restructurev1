var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var lk_zonelayer = new Schema(
	{	
		_id_garage : 		[{ type: Schema.Types.ObjectId, ref: 'Lk_garageModel' }] ,
		type:               { type:String, trim: true, default: 'province' } ,
        piority:            { type:Number, trim: true, default: 99 } ,
        searchpsgradian : 	{ type:Number, trim: true, default: 10000 } ,	
		polygons : 			{} ,
		Long : 				{ type:Number, trim: true } ,
		Lat : 				{ type:Number, trim: true } ,
		ZoneID :    		{ type:Number, trim: true } ,
		NameTH : 			{ type:String, trim: true } ,	
		NameEN :			{ type:String, trim: true } ,
		ProvinceID :		{ type:Number, trim: true } ,
	},
	{
		collection: 'lk_zonelayer',
		versionKey: false,
		strict: false
	}
);

// create the model for users and expose it to our app
module.exports = mongoose.model('Lk_zonelayerModel', lk_zonelayer);