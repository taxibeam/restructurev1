var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Buyshifttransaction = new Schema(
	{		
		created:  { type: Date, default: Date.now },
		Actiondate: { type: Date },
		Action: { type: String },				// BUY
		MoneyPerUseAPP: { type: Number },
		TimePerUseAPP: { type: Number },
		PostShift: { type: Number },
		PreShift: { type: Number },
        Amount: { type: Number },
		PostAmount: { type: Number },
        PreAmount: { type: Number },  
        _id_driver : { type: Schema.Types.ObjectId, ref: 'DriversModel' }	
	},
	{
		collection: 'buyshifttransaction', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('BuyshifttransactionModel', Buyshifttransaction);