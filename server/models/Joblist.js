var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Joblist = new Schema(
	{
		created:			{ type: Date, default: Date.now } ,
		drvarroundlist: 	[
			{ 				
				_id : { type: Schema.Types.ObjectId, ref: 'DriversModel' } ,
				dist : { type:Number, trim: true } ,
				dist_km : { type:Number, trim: true } 
			 }
		] ,
		drvcancelwhere: 	{ type:String, trim: true } ,
		datedrvcancel: 		{ type: Date } ,
		psgcancelwhere: 	{ type:String, trim: true } ,
		datepsgcancel: 		{ type: Date } ,
		datepsgthanks: 		{ type: Date } ,
		datedrvdrop: 		{ type: Date } ,
		datedrvpick: 		{ type: Date } ,
		datepsgaccept: 		{ type: Date } ,
		datedrvwait: 		{ type: Date } ,
		datepsgcall:		{ type: Date } ,
		createdvia: 		{ type:String, trim: true } ,
		cgroup: 			{ type:String, trim: true } ,
		detail:				{ type:String, trim: true } ,
		tips:				{ type:String, trim: true } ,
		ZoneID:     		{ type:Number, trim: true } ,
		NameTH: 			{ type:String, trim: true } ,
		NameEN:				{ type:String, trim: true } ,
		ProvinceID: 		{ type:Number, trim: true } ,
		desloc:				{ type:Array, trim: true } ,						// for drop off location [ lng,lat ]
		deslat:				{ type:Number, trim: true } ,
		deslng:				{ type:Number, trim: true } ,
		destination:		{ type:String, trim: true } ,
		curloc:				{ type:Array, trim: true, default:[] } ,			// created index:   db.Joblisthotel.createIndex({curloc:"2d"}) &  db.Joblisthotel.createIndex({ curloc : "2dsphere" }) 
		curlat:				{ type:Number, trim: true } ,
		curlng:				{ type:Number, trim: true } ,
		curaddr:			{ type:String, trim: true } ,
		psg_path: 			[ ] ,
		drv_path: 			[ ] ,
		drv_carplate: 		{ type:String, trim: true } ,
		drv_phone: 			{ type:String, trim: true } ,
		drv_name: 			{ type:String, trim: true } ,
		drv_device_id:	 	{ type:String, trim: true } ,
		drv_id:				{ type: Schema.Types.ObjectId, ref: 'DriversModel' } ,
		psg_phone: 			{ type:String, trim: true } ,
		psg_device_id:		{ type:String, trim: true } ,
		displayName: 		{ type:String, trim: true } ,
		psg_id:				{ type: Schema.Types.ObjectId, ref: 'PassengerModel' } ,
		job_id: 			{ type:String, trim: true } ,
		status:				{ type:String, trim: true } ,
		cancel_case_id:		{ type:String, trim: true },
		cancel_message:		{ type:String, trim: true } , 
		cancel_opened:		{ type:Boolean, default: false } ,
		cancel_verify:		{ type:Boolean, default: false } ,
		cancel_memo:		{ type: String, trim: true },
		cancel_reason:		[ ]
	},
	{
		collection: 'joblist', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('JoblistModel', Joblist);