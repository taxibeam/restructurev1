var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Shifthistory = new Schema({
    created: { type: Date,  },
    shift_end: { type: Date, default: Date.now },
    shift_start: { type: Date },    
    job_id: { type: String, trim: true },
    _id_drv: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
},
    {
        collection: 'shifthistory',
        versionKey: false,
        strict: false
    });

mongoose.model('ShifthistoryModel', Shifthistory);