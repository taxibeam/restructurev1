var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var Passenger = new Schema(
	{
		cgroup: { type: String, trim: true, default: 'beam' },
		psg_point: { type: Number, trim: true, default: 0 },
		occupied: { type: Boolean, default: false },
		created: { type: Date, default: Date.now },
		updated: { type: Date, default: Date.now },
		active: { type: String, trim: true, default: 'N' },
		drvarroundlist: [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				dist: { type: Number, trim: true },
				dist_km: { type: Number, trim: true }
			}
		],
		smsconfirm: { type: String, trim: true, default: '0000' },		// random SMS
		createdjob: { type: Date },										// Date of create current task or date for the advance task
		appversion: { type: String, trimt: true, default: '1' }, 		// if 1 default process = ON > WAIT > BUSY > PICK > THANKS > OFF
		jobtype: { type: String, trim: true, default: '' },			// [ "QUEUE", "ADVANCE"]		
		status: { type: String, trim: true, default: 'OFF' },		// [ "OFF","ON","WAIT","BUSY","THANKS","INITIATE","DPENDING","ASSIGNED","BROADCAST"]
		deniedTaxiIds: { type: Array, trim: true, default: [] },
		drv_carplate: { type: String, trim: true, default: '' },			// drv_carplate  for assigned task to the car which is not in the system.
		drv_id: { type: String, trim: true, default: '' },			// ห้าม populate ไม่งั้น passengergetstatus จะใช้ไม่ได้
		favdrv: { type: Array, trim: true, default: [] },
		favcartype: { type: Array, trim: true, default: [] },
		detail: { type: String, trim: true, default: '' },
		tips: { type: Number, trim: true, default: '' },
		desloc: { type: Array, trim: true, default: [] },
		deslat: { type: Number, trim: true, default: '' },
		deslng: { type: Number, trim: true, default: '' },
		des_dist: { type: Number, trim: true, default: '' },
		destination: { type: String, trim: true, default: '' },
		curloc: { type: Array, trim: true, default: [] },			// created index:   db.passengers.createIndex({curloc:"2d"}) &  db.passengers.createIndex({ curloc : "2dsphere" }) 
		curlat: { type: Number, trim: true, default: '' },			// for pick up latitude
		curlng: { type: Number, trim: true, default: '' },			// for pick up longitude
		realloc: { type: Array, trim: true, default: [] },			// real time position
		reallat: { type: Number, trim: true, default: '' },			// real time latitude
		reallng: { type: Number, trim: true, default: '' },			// real time logitude
		curaddr: { type: String, trim: true, default: '' },
		createdvia: { type: String, trim: true, default: 'MOBILE' },		// This passenger has been  created via MOBILE or CALLCENTER
		psgtype: { type: String, trim: true, default: 'Person' },		// psg type: Person, Hotel		
		phone: { type: String, trim: true, default: '' },
		pictureprofile: { type: String, trim: true, default: '' },
		email: { type: String, trim: true, default: '' },
		gender: { type: String, trim: true, default: '' },
		lname: { type: String, trim: true, default: '' },
		fname: { type: String, trim: true, default: '' },
		displayName: { type: String, trim: true, default: '' },
		job_id: { type: String, trim: true, default: 'default' },
		phoneid: { type: String, trim: true },			// for BeamPass indentification
		device_id: { type: String, trim: true, default: '' },
		local: {
			username: String,
			email: String,
			password: String,
		},
		mobiledata: {},
		facebook: {
			id: String,
			token: String,
			email: String,
			name: String,
			displayName: String,
			pictureprofile: String,
			username: String,
			about: String,
			gender: String,
			first_name: String,
			last_name: String
		},
		twitter: {
			id: String,
			token: String,
			displayName: String,
			username: String
		},
		google: {
			id: String,
			token: String,
			email: String,
			name: String,
			give_name: String,
			family_name: String,
			link: String,
			picture: String,
			gender: String,
			locale: String,
			hd: String
		}
	},
	{
		collection: 'passengers',
		versionKey: false,
		strict: false
	}
);

//--------->> mongoose.model('PassengerModel', Passenger);
//https://scotch.io/tutorials/easy-node-authentication-setup-and-local
// generating a hash
Passenger.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
Passenger.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('PassengerModel', Passenger);