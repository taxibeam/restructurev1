var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Joblisthotel = new Schema(
	{
		occupied: { type: Boolean, default: false },
		created: { type: Date, default: Date.now },
		updated: { type: Date, default: Date.now },
		drvbiddingjob: [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				dist: { type: Number, trim: true },
				created: { type: Date }
			}
		],
		biddingend: { type: Boolean, default: false },
		biddingtimeout: { type: Date },
		biddingstart: { type: Boolean, default: false },
		drvarroundlist: [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				dist: { type: Number, trim: true },
				dist_km: { type: Number, trim: true }
			}
		],
		drvcancelwhere: { type: String, trim: true, default: '' },
		datedrvcancel: { type: Date },
		psgcancelwhere: { type: String, trim: true, default: '' },
		datepsgcancel: { type: Date },
		datepsgthanks: { type: Date },
		datedrvdrop: { type: Date },
		datedrvpick: { type: Date },
		datepsgaccept: { type: Date },
		datedrvwait: { type: Date },
		datepsgcall: { type: Date },
		createdvia: { type: String },
		calltime: { type: String },
		jobhistory: {},
		deniedTaxiIds: { type: Array, trim: true, default: [] },
		_id_garage: { type: Schema.Types.ObjectId, ref: 'Lk_garageModel' },
		cgroup: { type: String, trim: true, default: '' },
		detail: { type: String, trim: true, default: '' },
		jobmode: { type: String, trim: true },
		jobtype: { type: String, trim: true, default: '' },			// [ "QUEUE", "ADVANCE"]		
		status: { type: String, trim: true, default: 'ON' },		// [ "ON", "BROADCAST" ]
		favcartype: { type: Array, trim: true, default: [] },
		tips: { type: Number, trim: true, default: '' },
		ZoneID: { type: Number, trim: true },
		NameTH: { type: String, trim: true },
		NameEN: { type: String, trim: true },
		ProvinceID: { type: Number, trim: true },
		desloc: { type: Array, trim: true, default: [] },			// for drop off location [ lng,lat ]
		deslat: { type: Number, trim: true },
		deslng: { type: Number, trim: true },
		destination: { type: String, trim: true },
		curloc: { type: Array, trim: true, default: [] },			// created index:   db.Joblisthotel.createIndex({curloc:"2d"}) &  db.Joblisthotel.createIndex({ curloc : "2dsphere" }) 
		curlat: { type: Number, trim: true },
		curlng: { type: Number, trim: true },
		curaddr: { type: String, trim: true },
		path: [{ lnglat: [], timestamp: { type: Date, default: Date.now } }],
		drv_carplate: { type: String, trim: true, default: '' },
		psgtype: { type: String, trim: true, default: 'Hotel' },			// ใช้แยกงาน Person & Hotel & Callcenter (BROADCAST)
		drv_phone: { type: String, trim: true, default: '' },
		drv_name: { type: String, trim: true, default: '' },
		drv_device_id: { type: String, trim: true, default: '' },
		drv_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
		psg_phone: { type: String, trim: true, default: '' },
		psg_name: { type: String, trim: true, default: '' },
		hotel_phone: { type: String, trim: true, default: '' },
		displayName: { type: String, trim: true, default: '' },
		hotel_id: { type: Schema.Types.ObjectId, ref: 'HotelsModel' },
		_id_callcenterpsg: { type: Schema.Types.ObjectId, ref: 'CallcenterpsgModel' },
		job_id: { type: String, trim: true, default: '' } 						// เป็น Job_id เพื่ออ้างถึงงานหนึ่งๆที่อาจมีการเรียกรถหลายคัน	
	},
	{
		collection: 'joblisthotel',
		versionKey: false,
		strict: false
	}
);

mongoose.model('JoblisthotelModel', Joblisthotel);