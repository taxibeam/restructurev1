var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

	var DAkeeplog = new Schema({
        created :           { type: Date, default: Date.now } ,							
		logdata :           { } ,													// keep changed data
		logaction :       	{ type: String } ,										// [ "active", "editprofile" ]
		cgroup :            { type:String } ,										// users' cgroup
		username :          { type:String } ,										// username 
		_id_user :          { type: Schema.Types.ObjectId, ref: 'UserModel' } 		// users' ObjectId
	},
	{
		collection: 'DAkeeplog', 
		versionKey: false,
		strict : false
	});

mongoose.model('DAkeeplogModel', DAkeeplog);