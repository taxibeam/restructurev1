var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var POIs = new Schema(
	{
		_owner: { type: Schema.Types.ObjectId, ref: 'HotelsModel' },
		name: { type: String, trim: true },
		formatter: { type: String, trim: true },
		curloc: { type: Array },
		type: { type: String },
		detail: { type: String },
		garage: { type: String },
		destination: { type: String },
		count: { type: Number }
	},
	{
		collection: 'poi_passenger',
		versionKey: false,
		strict: false
	}
);

module.exports = mongoose.model('POIsModel', POIs);