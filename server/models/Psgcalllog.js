var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Psgcalllog = new Schema(
	{
		created:		{ type: Date, default: Date.now } ,
		cgroup:		{ type:String, trim: true, default:'' } ,
		callby: 			{ type:String, required: true, trim: true, default:'' } ,
		drv_carplate:		{ type:String, required: true, trim: true, default:'' } ,
		drv_phone:		{ type:String, required: true, trim: true, default:'' } ,
		drv_id:			{ type:String, required: true, trim: true, default:'' } ,
		ZoneID:     		{ type:Number, trim: true } ,
		NameTH: 		{ type:String, trim: true } ,	
		NameEN:		{ type:String, trim: true } ,
		ProvinceID: 		{ type:Number, trim: true } ,
		curloc:			{ type:Array, trim: true, default:[] } ,			// created index:   db.Joblisthotel.createIndex({curloc:"2d"}) &  db.Joblisthotel.createIndex({ curloc : "2dsphere" }) 
		curlat:			{ type:Number, trim: true, default:'' } ,
		curlng:			{ type:Number, trim: true, default:'' } ,
		displayName: 		{ type:String, trim: true, default:'' } ,	
		psg_phone:		{ type:String, trim: true, default:'' } ,
		psg_device_id: 	{ type:String, required: true, trim: true, default:'' } ,
		psg_id:			{ type:String, required: true, trim: true, default:'' }
	},
	{
		collection: 'psgcalllog', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('PsgcalllogModel', Psgcalllog);