var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var lk_provinces = new Schema(
	{	
		_id_garage : 		[{ type: Schema.Types.ObjectId, ref: 'Lk_garageModel' }] ,	// array ของ ObjectId ของแต่ละอู่ที่อยู่ในจังหวัดนี้
		Nameth: 			{ type:String, trim: true } ,								// ชื่อภาษาไทย for multiple language
		Nameen:				{ type:String, trim: true } ,								// ชื่อภาษาอังกฤษ for multiple language
		psgfare	:		 	{ type:Number, trim: true, default: 20 } ,					// ค่าบริการแยกตามพื้นที่ แยกตามจังหวัด
		searchpsgamount	: 	{ type:Number, trim: true, default: 10 } ,					// ใช้สำหรับกำหนดจำนวนรถที่จะส่งงานให้จากจุดเรียกของผู้โดยสาร  แยกตามจังหวัด
		searchpsgradian :	{ type:Number, trim: true, default: 5000 } ,				// ใช้สำหรับกำหนดรัศมีที่จะส่งงานให้รถจากจุดเรียกของผู้โดยสาร  แยกตามจังหวัด			
		polygons: 			{} ,														// polygon ของจังหวัด
		Long: 				{ type:Number, trim: true } ,								// Longitude
		Lat: 				{ type:Number, trim: true } ,								// Latitude
		ZoneID:     		{ type:Number, trim: true } ,								// id ของ zone (ภาค)
		NameTH: 			{ type:String, trim: true } ,								// ชื่อภาษาไทย
		NameEN:				{ type:String, trim: true } ,								// ชื่อภาษาอังกฤษ
		ProvinceID: 		{ type:Number, trim: true } ,								// id หลักของจังหวัด
	},
	{
		collection: 'lk_provinces',
		versionKey: false,
		strict: false
	}
);

// create the model for users and expose it to our app
module.exports = mongoose.model('Lk_provincesModel', lk_provinces);