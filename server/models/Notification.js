var shortid = require('shortid');
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var NotificationModel = new Schema({
		uid:		{ type: String, default: shortid.generate, required: true },
		type:		{ type: String, trim: true, required: true },
		title: 		{ type: String, trim: true, required: true },
		message:	{ type: String, trim: true, required: true },
		image_url: 	{ type: String, trim: true },
		status: 	{ type: String, trim: true, required: true },
		start: 		{ type: Date, required: true },
		end:		{ type: Date, required: true },
		start_now:	{ type: Boolean, require: true },

        driver_selected: { type: Array },
        garage_selected: { type: Array },
        province_selected: { type: Array },

		created:	{ type: Date },
		updated:	{ type: Date },

		drivers: [{ type: Schema.Types.ObjectId, ref: 'DriversModel' }]
	},
	{
		collection: 'notification', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('NotificationModel', NotificationModel);