var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Wallettransaction = new Schema(
	{		
		DateTime: { type: Date, default: Date.now },
		AdminID: { type: String },
		Type: { type: String },				// Add, Use, Refund, 
        Amount: { type: Number },
		PostWallet:  [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				Amount: { type: Number, trim: true }
			}
		],		
		PreWallet:  [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
				Amount: { type: Number, trim: true }
			}
		],		
		AccountIDs:  [
			{
				_id: { type: Schema.Types.ObjectId, ref: 'DriversModel' }
			}
		]
	},
	{
		collection: 'wallettransaction', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('WallettransactionModel', Wallettransaction);