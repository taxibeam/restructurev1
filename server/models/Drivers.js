var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Drivers = new Schema(
	{
		created: { type: Date, default: Date.now },
		updated: { type: Date, default: Date.now },
		shift_end: { type: Date },
		shift_start: { type: Date },
		firstshift_type: { type: String, trim: true },
		firstshift_id: { type: String, trim: true },	
		firstshift: { type: Boolean },	
		drv_shift: { type: Number, trim: true, default: 0 },  			// จำนวนเงินรอบที่มี ณ ปัจจุบัน
		drv_wallet: { type: Number, trim: true, default: 0 },  			// จำนวนเงินที่เติมผ่านบุญเติม application
		drv_point: { type: Number, trim: true, default: 0 },
		pwdhint: { type: String },
		useappfreemsg: { type: String, trim: true, default: 'ช่วงโปรโมชั่น ใช้ฟรี ถึง 31 ธ.ค. 59' },
		useappfree: { type: String, trim: true, default: 'N' },
		updateprofilehistory: { type: Array },									// Keep input update profile data
		expdategarage: { type: Date, default: Date.now }, 				// Expired date สำหรับการใช้บริการ call center, default เป็น null
		moneygarage: { type: Number, trim: true, default: 0 },			// จำนวนเงินที่เหลือจริงๆในระบบสำหรับบริการ call center
		paymentgaragehistory: { type: Array },									// เก็บประวัตการหักเงินสำหรับการใช้บริการ call center
		topupgaragehistory: { type: Array },				  					//  เก็บประวัติการเติมเงินเข้าระบบสำหรับการใช้บริการ call center
		expdateapp: { type: Date, default: Date.now }, 				// Expired date สำหรับการใช้บริการ application, default เป็น now
		moneyapp: { type: Number, trim: true, default: 0 },  			// จำนวนเงินที่เหลือจริงๆในระบบสำหรับบริการ application
		paymentapphistory: { type: Array }, 									// เก็บประวัตการหักเงินสำหรับการใช้บริการ application
		topupapphistory: { type: Array },     								//  เก็บประวัติการเติมเงินเข้าระบบสำหรับการใช้บริการ application
		appversion: { type: String, trimt: true, default: '1' },
		status: { type: String, trim: true, default: 'PENDING' },	// PENDING > APPROVED > INACTIVE > OFF > ON > WAIT > BUSY > PICK // DPENDING, DEPENDING_REJECT
		psgtype: { type: String, trim: true, default: 'Person' },
		jobtype: { type: String, trim: true, default: '' },
		psg_detail: { type: String, trim: true, default: '' },
		psg_destination: { type: String, trim: true, default: '' },
		psg_curaddr: { type: String, trim: true, default: '' },
		datereadmsg: { type: Date },
		msgtimeout: { type: Number, trim: true, default: '' },			// msgtimeout get value from ccwaiting time for each garage
		msgstatus: { type: String, trim: true, default: '' },			// msg status NEW: not read yet,  OLD: already read		
		msgnote: { type: String, trim: true, default: '' },
		msgphone: { type: String, trim: true, default: '' },
		active: { type: String, trim: true, default: 'N' },			// activate by SMS
		smsconfirm: { type: String, trim: true, default: '0000' },		// random SMS		
		psg_id: { type: String, trim: true, default: '' },			// passenger id
		job_id: { type: String, trim: true, default: '' },
		car_no: { type: String, trim: true, default: '' },			// car no : the car reference for  using of each garage
		brokenpicture: { type: Array, trim: true, default: [] },
		brokendetail: { type: String, trim: true, default: '' },
		brokenname: { type: Array, trim: true, default: [] },
		degree: { type: Number, trim: true, default: '' },
		accuracy: { type: Number, trim: true, default: '' },
		curProvinceID: { type: Number, trim: true, default: '' },			// ID จังหวัด ณ ปัจจุบัน
		curloc: { type: Array, trim: true, default: [] },			// created index:   db.drivers.createIndex({curloc:"2d"}) &  db.drivers.createIndex({ curloc : "2dsphere" }) 
		curlat: { type: Number, trim: true, default: '' },
		curlng: { type: Number, trim: true, default: '' },
		imgczid: { type: String, trim: true, default: '' },			// รูปบัตรประชาชน
		imgcar: { type: String, trim: true, default: '' },			// รูปรถ
		imglicence: { type: String, trim: true, default: '' },			// รูปบัตรติดหน้ารถ
		imgface: { type: String, trim: true, default: '' },			// รูปหน้าคนขับ
		carryon: { type: String, trim: true, default: '' },			// ขนของหรือไม => { 'Y','N'}่
		outbound: { type: String, trim: true, default: '' },			// รับงานเหมานอกพื้ื้นที่หรือไม่ => {'Y','N'}
		carcolor: { type: String, trim: true, default: '' },			// สีรถ
		cartype: { type: String, trim: true, default: '' },			// ชนิดของรถ => car, minican
		carprovince: { type: String, trim: true },						// จังหวัดที่จดทะเบียน
		carprovinceid: { type: Number, trim: true },						// id จังหวัดที่จดทะเบียน 		
		carplate_formal: { type: String, trim: true },						// ทะเบียนรถ แบบเต็ม
		carplate: { type: String, trim: true },						// ทะเบียนรถ
		prefixcarplate: { type: String, trim: true },						// ทะเบียนรถ 3 ตัวหน้า
		english: { type: String, trim: true, default: '' },			// พูดอังกฤษได้หรือไม่ => {'Y','N'}
		zipcode: { type: String, trim: true, default: '' },			// zipcode
		tambon: { type: String, trim: true, default: '' },			// tambon
		district: { type: String, trim: true, default: '' },			// district
		province: { type: String, trim: true, default: '' },			// province
		address: { type: String, trim: true, default: '' },			// address
		workstatus: { type: String, trim: true, default: '' },			// สถานะ ยัังขับอยู่ / ไม่ไ้ด้ขับแล้ว => {'Y','N'}
		workperiod: { type: String, trim: true, default: '' },			// ช่วงเวลาเช่า : เช้า-เย็น-ทั้งวัน => {'am','pm','all'}
		nname: { type: String, trim: true, default: '' },			// ชื่อเล่น
		taxiID: { type: String, trim: true, default: '' },			// บัตรประจำตัว Taxi 6 หลัก
		citizenid: { type: String },							  		// บัตรประจำตัวประชาชน 13 หลัก
		carreturnwhere: { type: String, trim: true, default: '' },  		// ส่งรถที่
		carreturn: { type: String, trim: true, default: '' },  		// ส่งรถ (Y/N) => status => RETURNCAR
		allowpsgcontact: { type: String, trim: true, default: 'Y' }, 		// อนุญาติให้ผดส.ติดต่อหรือไม่: Y/N
		gender: { type: String, trim: true }, 						// เพศ M/F
		dob: { type: String, trim: true }, 						// วันเดือนปีเกิด dd/mm/yyyy
		emergencyphone: { type: String, trim: true }, 						// เบอร์ติดต่อฉุกเฉิน
		phone: { type: String, required: 'Please inset phone number', trim: true, default: '' },
		lname: { type: String, required: 'Please insert last name', trim: true, default: '' },
		fname: { type: String, required: 'Please insrt first name', trim: true, default: '' },
		device_id: { type: String, required: true, trim: true, default: '' },
		cprovincename: { type: String, trim: true, default: '' },
		cgroupname: { type: String, trim: true, default: '' },
		cgroup: { type: String, trim: true, default: '' },
		mobiledata: {},
		notifications: [{
			_id: false,
			ref: { type: Schema.Types.ObjectId, ref: 'NotificationModel' },
			state: { type: String, trim: true, required: true }
		}],
		_id_garage: { type: Schema.Types.ObjectId, ref: 'Lk_garageModel' },
		fb_id: { type: String, trim: true, default: '' },
		password: { type: String, trim: true, default: '' },			// Password encrypt by crypto.createHmac('sha256', config.pwd_key).update(password).digest('base64');
		username: { type: String, trim: true, default: '' } 			// User name		
	},
	{
		collection: 'drivers',
		versionKey: false,
		strict: false
	}
);

// update latitude and longitude
Drivers.methods.updateLatLng = function (data, cb) {
	return this.model('DriversModel').findOneAndUpdate({ _id: data.id }, { curloc: [data.longitude, data.latitude], curlat: data.latitude, curlng: data.longitude }, { upsert: false }, cb);
};

mongoose.model('DriversModel', Drivers);