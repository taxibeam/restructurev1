var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Messageboard = new Schema({

	createdby:		{type: String, trim: true },
	created:		{type: Date, trim: true, default: Date.now},
	updated:		{type: Date, trim: true, default: Date.now},
	expired:		{type: Date, trim: true, default: Date.now},
	detail:			{type:String, trim: true, default:''},
	topic:			{type:String, trim: true, default:''},
	lang:	    	{type:String, trim: true, default:'th'},
	status: 		{type:String, trim: true, default:'N'},	
	msgtype:		{type:String, trim: true, default:''}	// data : { PRG_WELCOME, PSG_ABOUT, PSG_TEREM, PSG_NEWSACT, PSG_PRBANNER, PSG_PROMOTION }

},
{
	collection: 'messageboard', 
	versionKey: false,
	strict : false
}
);

mongoose.model('MessageboardModel', Messageboard);
