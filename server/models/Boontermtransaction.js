var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Boontermtransaction = new Schema(
	{
		created: { type: Date, default: Date.now },
		status: { type: String, trim: true },
		Transactiondate: { type: Date },
		BSessionTimeout: { type: Date },
		Sessiondate: { type: Date, default: Date.now },
		Desc: { type: String, trim: true  },
        Code: { type: String, trim: true  },
        ReqTrans: { },
        ReqSess: { },
		SessionID: { type: String, trim: true  },
        Amount: { type: Number, trim: true  },
		PostAmount: { type: Number, trim: true  },
        PreAmount: { type: Number, trim: true  },
        KeyOperator: { type: String, trim: true  },
        KeyBoonterm: { type: String, trim: true  },        
        IDNUM: { type: String, trim: true  },        
		REFID: { type: String, trim: true  },
		Name: { type: String, trim: true  },		
        citizenid: { type: String, trim: true  },
        _id_driver : { type: Schema.Types.ObjectId, ref: 'DriversModel' }
	},
	{
		collection: 'boontermtransaction',
		versionKey: false,
		strict : false
	}
);

mongoose.model('BoontermtransactionModel', Boontermtransaction);