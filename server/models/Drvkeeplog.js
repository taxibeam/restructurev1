var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

	var drvkeeplog = new Schema({
        created : { type: Date, default: Date.now } ,
		status : { type: String },
		ncount : { type: Number },
        ddate : { type: String },
        cgroup: { type:String },
		carplate: { type:String },
		phone: { type:String },
		lname: { type:String },
		fname: { type:String },					
		_driver : { type: Schema.Types.ObjectId, ref: 'DriversModel' }
	},
	{
		collection: 'drvkeeplog', 
		versionKey: false,
		strict : false
	});

mongoose.model('DrvkeeplogModel', drvkeeplog);