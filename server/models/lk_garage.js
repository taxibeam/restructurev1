var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var lk_garage = new Schema(
	{
		active: 			{ type: String, trim: true, default:'Y'} ,		// N ถ้าไม่ต้องการให้แสดงบน driver app ตอน register
		bidjobradian:		{ type: Number, trim:true, default:2000 },		// รัศมีรับงาน default		meter
		bidjobwaitingtime: 	{ type: Number, trim:true, default:30000 },		// เวลารอเพื่อ bid งาาน  	millisecond
    	ccwaitingtime: 		{ type: Number, trim:true, default:180000 },	// กำหนดเวลาเพื่อรับงานจาก callcenter
		searchpsgamount	: 	{ type:Number, trim: true, default: 10 } ,		// ใช้กับงาน broadcast จาก cc ว่าให้รถกี่คันเห็น แยกตามอู่
		searchpsgradian :	{ type:Number, trim: true, default: 5000 } ,	// ใช้กับงาน braodcast จาก cc ว่าให้อยู่่ในรัศมีกี่เมตร แยกตามอู่		
		ProvinceID: 		{ type: Number, trim:true } ,					// ID จังหวัดของอู่
    	drvsearchpsgamount: { type: Number, trim:true } ,					// จำนวนงานที่ขึ้นในลิสต์ของ driver
    	drvsearchpsgradian: { type: Number, trim:true } ,					// รัศมีการ search หางานของ driver
		cprovincename: 		{ type: String, trim: true, default:''} ,		// ชื่อจังหวัดที่อู่นี้ตั้งอยู่ จะอยู่่ในวงเล็บบนหน้า psg ตอนที่กดเรียกดูรายละเอียดของคนขับ
		cgroupname: 		{ type: String, trim: true, default:''} ,		// ชื่อของ
		version: 			{ type: String, trim:true, default:''} ,		// version ของ iconurl จะต้องถูกเปลี่ยนเมื่อ iconurl มีการเปลี่ยนแปลง
		iconurl_active:  	{ type: String, trim:true, default:''} ,		// iconurl ของแท็กซี่ตอนที่กดเลือก
		iconurl: 			{ type: String, trim:true, default:''} ,		// iconurl ของแท็กซี่ตอนปกติ
		phone2: 			{ type: String, trim: true, default:''} ,		// เบอร์ติดต่อที่ 2
		phone: 				{ type: String, trim: true, default:''} ,		// เบอร์ติดต่อที่ 1
		province:     		{ type: String, trim: true, default:''} ,		// จังหวัดของอู่
		fullname: 			{ type: String, trim: true, default:''} ,		// ชื่ออู่เต็ม
		name:				{ type: String, trim: true, default:'' } ,		// ชื่ออู่
		cgroup: 			{ type: String, trim:true, default:''},			// ชื่อcgroup ที่จะใช้เชื่อมระหว่าง คนขับ อู่ ผู้โดยสาร unique***
		broadcast:			{ type:Boolean, default: false } ,
		allow_adjust_poi: 	{ type:Boolean, default: false } 
	},
	{
		collection: 'lk_garage',
		versionKey: false,
		strict: false
	}
);

// create the model for users and expose it to our app
module.exports = mongoose.model('Lk_garageModel', lk_garage);