var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var joblistdetail = new Schema(
	{
		created:			{ type: Date, default: Date.now } ,		
		updated:			{ type:Date, default: Date.now } ,		
		drvcancelwhere: 	{ type:String, trim: true, default:'' } ,
		datedrvcancel: 		{ type: Date } ,
		psgcancelwhere: 	{ type:String, trim: true, default:'' } ,
		datepsgcancel: 		{ type: Date } ,
		datepsgthanks: 		{ type: Date } ,
		datedrvdrop: 		{ type: Date } ,
		datedrvpick: 		{ type: Date } ,		
		datepsgaccept: 		{ type: Date } ,		
		datedrvwait: 		{ type: Date } ,	
		datepsgcall:		{ type: Date } ,
		createdvia: 		{ type: String },
		calltime: 			{ type: String },
		jobhistory:     	{ } ,
		deniedTaxiIds: 		{ type:Array, trim: true, default:[]} ,
		cgroup: 			{ type:String, trim: true, default:'' } ,		
		status:				{ type:String, trim: true } ,
		favcartype:			{ type:Array, trim: true, default:[] } ,		
		ProvinceID: 		{ type:Number, trim: true } ,		
		_id_province:       { type:Schema.Types.ObjectId, ref: 'ProvinceModel' } ,
        detail:				{ type:String, trim: true, default:'' } ,
        tips:				{ type:Number, trim: true, default:'' } ,
        desloc:				{ type:Array, trim: true, default:[] } ,			// for drop off location [ lng,lat ]
		deslat:				{ type:Number, trim: true, default:'' } ,
		deslng:				{ type:Number, trim: true, default:'' } ,
		destination:		{ type:String, trim: true, default:'' } ,
		curloc:				{ type:Array, trim: true, default:[] } ,			// created index:   db.Joblisthotel.createIndex({curloc:"2d"}) &  db.Joblisthotel.createIndex({ curloc : "2dsphere" }) 
		curlat:				{ type:Number, trim: true, default:'' } ,
		curlng:				{ type:Number, trim: true, default:'' } ,
		curaddr:			{ type:String, trim: true, default:'' } ,
		psg_path: 			[ { lnglat: [] , timestamp:  { type: Date, default: Date.now } } ] ,
        drv_path: 			[ { lnglat: [] , timestamp:  { type: Date, default: Date.now } } ] ,
		jobtype: 			{ type:String, trim: true } ,		// ex: PSG, HOTEL, CC, CCPSG
        drv_carplate: 		{ type:String, trim: true, default:'' } ,		
		drv_phone: 			{ type:String, trim: true, default:'' } ,
		drv_name: 			{ type:String, trim: true, default:'' } ,
		drv_device_id:	 	{ type:String, trim: true, default:'' } ,		
        _id_drv:			{ type:Schema.Types.ObjectId, ref: 'DriversModel' } ,
		psg_phone: 			{ type:String, trim: true, default:'' } ,
		psg_name: 			{ type:String, trim: true, default:'' } ,
		hotel_phone: 		{ type:String, trim: true, default:'' } ,
		displayName: 		{ type:String, trim: true, default:'' } ,
		_id_hotel:			{ type:Schema.Types.ObjectId, ref: 'HotelsModel' } ,
        _id_psg:	        { type:Schema.Types.ObjectId, ref: 'PassengerModel' } ,	
		_id_garage:         { type:Schema.Types.ObjectId, ref: 'Lk_garageModel' } ,
        job_id:             { type:String, trim: true, default:'' } 		
	},
	{
		collection: 'joblistdetail', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('JoblistdetailModel', joblistdetail);