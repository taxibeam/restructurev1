var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

	var Sockets = new Schema({
		driverId: { type: String },
		socketId: { type: String },
		_user : { type: Schema.Types.ObjectId, ref: 'UserModel' },
		_driver : { type: Schema.Types.ObjectId, ref: 'DriversModel' },
		_passenger : { type: Schema.Types.ObjectId, ref: 'PassengersModel' }
	},
	{
		collection: 'Socket', 
		versionKey: false,
		strict : false
	});

mongoose.model('SocketsModel', Sockets);