var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var Layergarage = new Schema(
	{
		created:			{ type: Date, default: Date.now } ,
		createdby: 			{ type: String } ,
        polygons: 		    { } ,
        radian:             { type: Number, trim: true, default: 3000 },                      // radian : unit in meters        
        NameEN:             { type: String } ,
        NameTH:             { type: String } ,
		_id_garage:			{ type: Schema.Types.ObjectId, ref: 'Lk_garageModel' } ,
	},
	{
		collection: 'layergarage', 
		versionKey: false,
		strict : false
	}
);

mongoose.model('LayergarageModel', Layergarage);


