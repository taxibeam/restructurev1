var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ShareActivities = new Schema({
	createdby: { type: String, trim: true },
	psgtype: { type: String, trim: true },	// data: { beam, smiles }
	created: { type: Date, trim: true, default: Date.now },
	expired: { type: Date, trim: true, default: Date.now },
    hashtag: { type: String },
	shareimg: { type: String, trim: true },
	sharelink: { type: String, trim: true },
	detail: { type: String, trim: true },
	topic: { type: String, trim: true},
	lang: { type: String, trim: true },
	sharetype: { type: String, trim: true }	// data : { facebook, twitter }
},
	{
		collection: 'shareactivities',
		versionKey: false,
		strict: false
	}
);

mongoose.model('ShareActivitiesModel', ShareActivities);
