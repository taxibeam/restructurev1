var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var msghistory = new Schema(
	{	
        created:            { type: Date, default: Date.now },
		desloc:				{ type:Array, trim: true } ,								// destination location
		curloc:				{ type:Array, trim: true } ,								// curaddr location
		des: 				{ type:String, trim: true } ,								// destination
		cur:				{ type:String, trim: true } ,								// curaddr
        phone:		 		{ type:String, trim: true } ,								// phone
		cgroup:             { type:String, trim: true } ,              
        plate:              { type:String, trim: true } ,
		drv_phone:	 		{ type:String, trim: true } ,								// drv_phone
		imei:				{ type:String, trim: true }									// IMEI
	},
	{
		collection: 'msghistory',
		versionKey: false,
		strict: false
	}
);

// create the model for users and expose it to our app
module.exports = mongoose.model('MsghistoryModel', msghistory);