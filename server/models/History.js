var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var History = new Schema(
	{
        type: { type: String, trim: true },
		_ownerId: { type: Schema.Types.ObjectId, ref: 'DriversModel' },
		_callcenterId: { type: Schema.Types.ObjectId, ref: 'CallcenterpsgModel' },
        _passengerId: { type: Schema.Types.ObjectId, ref: 'PassengerModel' },
		_passengerJobDetail: { type: Schema.Types.ObjectId, ref: 'JoblistModel' },
		status: { type: String, trim: true },
	    created: { type: Date, default: Date.now }
	},
	{
		collection: 'history',
		versionKey: false,
		strict: false
	}
);

module.exports = mongoose.model('HistoryModel', History);