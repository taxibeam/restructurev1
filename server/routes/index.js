////////////////////////////////////
// Taxi-Beam Route for  API 
// version : 1.1.1
// Date revision: Sept 19, 2017 
// Created by Hanzen@BRET
////////////////////////////////////
var config = require('../../config/func').production;
var mongoose = require('mongoose');
var moment = require('moment');
var Controller = require('../controllers/Controller');
var DriverCS = require('../controllers/Drivercs');
var PassengerTP = require('../controllers/Passengertp');
var Hotels = require('../controllers/Hotels');
var Garage = require('../controllers/Garage');
var Ecadmin = require('../controllers/Ecadmin');
var UbeamTP = require('../controllers/Ubeamtp');
var Admin = require('../controllers/Admin');
var Admin = require('../controllers/Admin');
var Notification = require('../controllers/Notification');
var LogService = require('../controllers/LogService');
var SocketController = require('../controllers/SocketController');
var Samsung = require('../controllers/Samsung');
var KKNBeamPass = require('../controllers/KKNBeamPass');
var BeamPartner = require("../controllers/BeamPartner");
var Boonterm = require('../controllers/Boonterm');
var ShifttransactionModel = mongoose.model('ShifttransactionModel');
var HaloBeam = require("../controllers/HaloBeam");
var WallettransactionModel = mongoose.model('WallettransactionModel');

//var utils = require('../../config/utils');
// var fs = require('fs');
// var util = require('util');
// var log_file = fs.createWriteStream('C:/nodejs/nodebug.log', {flags : 'w'});
// var log_stdout = process.stdout;

module.exports = function (app, io, passport) {

    /* --------------------------------------------
     *    Service
     * --------------------------------------------*/
    app.get('/service/me', function (req, res) {
        var users = req.users;
        if (user) {
            //res.json({
            var user_id = user.local._id
            //});
        }
        //console.log('user_id = '.user_id);
    });

    // Background service 
    app.get('/send/message/:message', function (req, res) {
        io.emit('new message', {
            status: true,
            phone: "ALL",
            message: req.params.message || '...'
        });
        res.send('message sent.');
    });

    app.get('/send/message/:message/:phone', function (req, res) {
        io.emit('new message', {
            status: true,
            phone: req.params.phone,
            message: req.params.message || '...'
        });
        res.send('message sent.');
    });

    app.get('/send/ll/:message/:phone', function (req, res) {
        io.emit('reqlatlng', {
            status: true,
            phone: req.params.phone,
            message: req.params.message || '...'
        });
        res.send('reqll sent.');
    });

    //////
    // Background service 
    app.get('/bus/get', function (req, res) {
        /*       
         io.emit('new message', {
              status: true,
              phone: "ALL",
              message: req.params.message || '...'
          }); */
        res.send('message sent.');
    });
    app.post('/service/bg/_bgservice_autonotified', Ecadmin._bgservice_autonotified(io));

    /////
    app.post('/service/passenger/listall', isLoggedIn, Ecadmin.ListPassenger);
    app.post('/service/taxi/listpending', isLoggedIn, Ecadmin.ListPendingDrv);
    app.post('/service/taxi/listall', isLoggedIn, Ecadmin.ListDrv);
    app.post('/service/users/listall', isLoggedIn, Ecadmin.ListUser);
    app.post('/service/users/getusergr', isLoggedIn, Ecadmin.getusergr);
    app.post('/manage_data/update_main', isLoggedIn, Ecadmin.update_main);


    /////////////////////

    //isAuthenticated = true ;   //     test
    // app.post('/manage_data/update_main',  function(req, res) {  
    //   var main = JSON.parse(req.body.main);
    //   var gid = req.body.gid;
    //   main.table = [];
    //   main.subform = [];
    //  console.log(main)
    // });
    ////

    // Garage api
    app.post('/service/garage/settings/all', isLoggedIn, Garage.getSettings);
    app.post('/service/garage/settings/save', isLoggedIn, Garage.saveSettings);


    // new API driver countryside version ////////////////////////////////////////////////////////////////////////////////
    app.post('/service/drivercs/loadconfig', DriverCS.driverloadconfig);
    app.post('/service/drivercs/GetCreditAndRoundInfo', DriverCS.GetCreditAndRoundInfo);    
    app.post('/service/drivercs/Login', DriverCS.driverLogin);
    app.post('/service/drivercs/register', DriverCS.driverRegister(io));
    app.post('/service/drivercs/checksamecarplate', DriverCS.checksamecarplate);
    app.post('/service/drivercs/checksameaccount', DriverCS.checksameaccount);
    app.post('/service/drivercs/updateProfile', DriverCS.driverUpdateProfile);
    app.post('/service/drivercs/changeOnOff', DriverCS.driverchangeOnOff);
    app.post('/service/drivercs/getStatus', DriverCS.driverGetStatus);
    app.post('/service/drivercs/searchPassenger', DriverCS.driverSearchPassenger);
    app.post('/service/drivercs/acceptCall', _checkdrvshift, DriverCS.driverAcceptCall(io));
    app.post('/service/drivercs/acceptHotel', _checkdrvshift, DriverCS.driverAcceptHotel(io));
    app.post('/service/drivercs/cancelCall', DriverCS.driverCancelCall(io));
    app.post('/service/drivercs/cancelHotel', DriverCS.driverCancelHotel(io));
    app.post('/service/drivercs/CancelCallCenter', DriverCS.driverCancelCallCenter(io));
    app.post('/service/drivercs/pickPassenger', DriverCS.driverPickPassenger(io));
    app.post('/service/drivercs/pickHotel', DriverCS.driverPickHotel(io));
    app.post('/service/drivercs/endTrip', DriverCS.driverEndTrip(io));
    app.post('/service/drivercs/EndTripHotel', DriverCS.driverEndTripHotel(io));
    app.post('/service/drivercs/getByID', DriverCS.driverGetByID);
    app.post('/service/drivercs/sendComment', DriverCS.driverSendComment);
    app.post('/service/drivercs/uploadFace', DriverCS.driveruploadFace);
    app.post('/service/drivercs/uploadLicence', DriverCS.driveruploadLicence);
    app.post('/service/drivercs/uploadCar', DriverCS.driveruploadCar);
    app.post('/service/drivercs/brokenAdd', DriverCS.driverBrokenAdd);
    app.post('/service/drivercs/brokenEdit', DriverCS.driverBrokenEdit);
    app.post('/service/drivercs/brokenDel', DriverCS.driverBrokenDel);
    app.post('/service/drivercs/brokenReport', DriverCS.driverBrokenReport);
    app.post('/service/drivercs/brokenCancel', DriverCS.driverBrokenCancel);
    app.post('/service/drivercs/drivergotmsg', DriverCS.drivergotmsg);
    app.post('/service/drivercs/gotdispatchaction', _checkdrvshift, DriverCS.gotdispatchaction(io));
    app.post('/service/drivercs/driverEndTask', DriverCS.driverEndTask(io));
    app.post('/service/drivercs/sendSMSUP', DriverCS.sendSMSUP);
    app.post('/service/drivercs/ChangePWDonly', DriverCS.ChangePWDonly);
    app.post('/service/drivercs/sendSMSConfirm', DriverCS.sendSMSConfirmation);
    app.post('/service/drivercs/driverReturnCar', DriverCS.driverReturnCar);
    app.post('/service/drivercs/GetParkingQueue', DriverCS.driverGetParkingQueue);
    app.post('/service/drivercs/BookParkingQueue', DriverCS.driverBookParkingQueue);
    app.post('/service/drivercs/LeftParkingQueue', DriverCS.driverLeftParkingQueue);
    app.post('/service/drivercs/checkIconURL', DriverCS.checkIconURL);
    app.post('/service/drivercs/getDriverHistoryApp', DriverCS.getDriverHistoryApp);
    app.post('/service/drivercs/getDriverHistoryCC', DriverCS.getDriverHistoryCC);
    app.post('/service/drivercs/getMsgHistory', DriverCS.getMsgHistory);
    app.post('/service/drivercs/getHistoryList', DriverCS.getHistoryList);
    app.post('/service/drivercs/getHistoryDetail', DriverCS.getHistoryDetail);
    app.post('/service/drivercs/drvbuyashift', DriverCS.drvbuyashift);
    app.post('/service/drivercs/getAllCityGarage', DriverCS.getAllCityGarage);

    // This service use from UbeamTP
    app.post('/service/drivercs/announcementGet', UbeamTP.announcementGet);
    app.post('/service/ubeam/requestCancelJob', UbeamTP.requestCancelJob(io));
    app.post('/service/ubeam/announcementGet', UbeamTP.announcementGet);

    // This service use from UbeamTP    
    app.post('/service/drivercs/askGGcharge', UbeamTP.askGGcharge);
    app.post('/service/drivercs/DrvGGAddMoney', UbeamTP.DrvGGAddMoney);
    app.post('/service/drivercs/DrvGGAddHour', UbeamTP.DrvGGAddHour);
    app.post('/service/drivercs/askAPPcharge', UbeamTP.askAPPcharge);
    app.post('/service/drivercs/DrvAPPAddMoney', UbeamTP.DrvAPPAddMoney);
    app.post('/service/drivercs/DrvAPPAddHour', UbeamTP.DrvAPPAddHour);
    app.post('/service/drivercs/getAllGarage', UbeamTP.getAllGarage);
    app.post('/service/drivercs/all', DriverCS.all);

    ///////////////////////////////  for SMILEsAPP //////////////////////////////////////////////////////////////
    app.post('/smiles/service/drivercs/loadconfig', DriverCS.driverloadconfig);
    app.post('/smiles/service/drivercs/Login', DriverCS.driverLogin);
    app.post('/smiles/service/drivercs/register', DriverCS.driverRegister(io));
    app.post('/smiles/service/drivercs/checksamecarplate', DriverCS.checksamecarplate);
    app.post('/smiles/service/drivercs/checksameaccount', DriverCS.checksameaccount);
    app.post('/smiles/service/drivercs/updateProfile', DriverCS.driverUpdateProfile);
    app.post('/smiles/service/drivercs/changeOnOff', DriverCS.driverchangeOnOff);
    app.post('/smiles/service/drivercs/getStatus', DriverCS.driverGetStatus);
    app.post('/smiles/service/drivercs/searchPassenger', DriverCS.driverSearchPassenger);
    app.post('/smiles/service/drivercs/acceptCall', _checksmilesshift, DriverCS.driverAcceptCall(io));
    app.post('/smiles/service/drivercs/acceptHotel', _checksmilesshift, DriverCS.driverAcceptHotel(io));
    app.post('/smiles/service/drivercs/cancelCall', DriverCS.driverCancelCall(io));
    app.post('/smiles/service/drivercs/cancelHotel', DriverCS.driverCancelHotel(io));
    app.post('/smiles/service/drivercs/CancelCallCenter', DriverCS.driverCancelCallCenter(io));
    app.post('/smiles/service/drivercs/pickPassenger', DriverCS.driverPickPassenger(io));
    app.post('/smiles/service/drivercs/pickHotel', DriverCS.driverPickHotel(io));
    app.post('/smiles/service/drivercs/endTrip', DriverCS.driverEndTrip(io));
    app.post('/smiles/service/drivercs/EndTripHotel', DriverCS.driverEndTripHotel(io));
    app.post('/smiles/service/drivercs/getByID', DriverCS.driverGetByID);
    app.post('/smiles/service/drivercs/sendComment', DriverCS.driverSendComment);
    app.post('/smiles/service/drivercs/uploadFace', DriverCS.driveruploadFace);
    app.post('/smiles/service/drivercs/uploadLicence', DriverCS.driveruploadLicence);
    app.post('/smiles/service/drivercs/uploadCar', DriverCS.driveruploadCar);
    app.post('/smiles/service/drivercs/brokenAdd', DriverCS.driverBrokenAdd);
    app.post('/smiles/service/drivercs/brokenEdit', DriverCS.driverBrokenEdit);
    app.post('/smiles/service/drivercs/brokenDel', DriverCS.driverBrokenDel);
    app.post('/smiles/service/drivercs/brokenReport', DriverCS.driverBrokenReport);
    app.post('/smiles/service/drivercs/brokenCancel', DriverCS.driverBrokenCancel);
    app.post('/smiles/service/drivercs/drivergotmsg', DriverCS.drivergotmsg);
    app.post('/smiles/service/drivercs/gotdispatchaction', _checksmilesshift, DriverCS.gotdispatchaction(io));
    app.post('/smiles/service/drivercs/driverEndTask', DriverCS.driverEndTask(io));
    app.post('/smiles/service/drivercs/sendSMSUP', DriverCS.sendSMSUP);
    app.post('/smiles/service/drivercs/ChangePWDonly', DriverCS.ChangePWDonly);
    app.post('/smiles/service/drivercs/sendSMSConfirm', DriverCS.sendSMSConfirmation);
    app.post('/smiles/service/drivercs/driverReturnCar', DriverCS.driverReturnCar);
    app.post('/smiles/service/drivercs/GetParkingQueue', DriverCS.driverGetParkingQueue);
    app.post('/smiles/service/drivercs/BookParkingQueue', DriverCS.driverBookParkingQueue);
    app.post('/smiles/service/drivercs/LeftParkingQueue', DriverCS.driverLeftParkingQueue);
    app.post('/smiles/service/drivercs/checkIconURL', DriverCS.checkIconURL);
    app.post('/smiles/service/drivercs/getDriverHistoryApp', DriverCS.getDriverHistoryApp);
    app.post('/smiles/service/drivercs/getDriverHistoryCC', DriverCS.getDriverHistoryCC);
    app.post('/smiles/service/drivercs/getMsgHistory', DriverCS.getMsgHistory);
    app.post('/smiles/service/drivercs/getHistoryList', DriverCS.getHistoryList);
    app.post('/smiles/service/drivercs/getHistoryDetail', DriverCS.getHistoryDetail);
    app.post('/smiles/service/drivercs/getAllCityGarage', DriverCS.getAllCityGarage);
    app.post('/smiles/service/drivercs/announcementGet', UbeamTP.announcementGet);
    app.post('/smiles/service/ubeam/requestCancelJob', UbeamTP.requestCancelJob(io));  
    app.post('/smiles/service/ubeam/announcementGet', UbeamTP.announcementGet);


    ///////////////////////////////  for admin notification //////////////////////////////////////////////////////////////
    app.post('/service/drivercs/notification/update', DriverCS.updateNotification(io));
    app.post('/service/drivercs/notification/sync', DriverCS.syncUpNotification);

    // new API  UBEAM  countryside version ///////////////////////////////////////////////////////////////////////////  
    app.post('/service/ubeam/searchPsg', UbeamTP.ubeamSearchPassenger);
    app.post('/service/ubeam/searchnamecar', UbeamTP.searchnamecar);
    app.post('/service/ubeam/senddrvmsg', UbeamTP.senddrvmsg);
    app.post('/service/ubeam/addjoblist', UbeamTP.addjoblist(io));
    app.post('/service/ubeam/editjoblist', UbeamTP.editjoblist(io));
    app.post('/service/ubeam/getinitiatelist', isLoggedIn, UbeamTP.getinitiatelist);
    app.post('/service/ubeam/getadvancelist', isLoggedIn, UbeamTP.getadvancelist);
    app.post('/service/ubeam/getdpendinglist', isLoggedIn, UbeamTP.getdpendinglist);
    app.post('/service/ubeam/getassignlist', isLoggedIn, UbeamTP.getassignlist);
    app.post('/service/ubeam/getsendjoblist', isLoggedIn, UbeamTP.getsendjoblist);
    app.post('/service/ubeam/getpsgsphonelist', isLoggedIn, UbeamTP.getpsgsphonelist);
    app.post('/service/ubeam/assigndrvtopsg', UbeamTP.assigndrvtopsg(io));
    app.post('/service/ubeam/checkinitiatepsg', UbeamTP.checkinitiatepsg);
    app.post('/service/ubeam/checkavailabledrv', UbeamTP.checkavailabledrv);
    app.post('/service/ubeam/getpsgstatuslist', UbeamTP.getpsgstatuslist);
    app.post('/service/ubeam/addlocalpoi', UbeamTP.addlocalpoi);
    app.post('/service/ubeam/cancelpsgdrv', UbeamTP.cancelpsgdrv(io));
    app.post('/service/ubeam/searchdrv', isLoggedIn, UbeamTP.searchdrv);
    app.post('/service/ubeam/countTaxi', UbeamTP.countTaxi);
    app.post('/service/ubeam/checkdrvstatus', UbeamTP.checkdrvstatus);
    app.post('/service/ubeam/deletejob', UbeamTP.deletejob(io));
    app.post('/service/ubeam/updatedrvappversion', UbeamTP.updatedrvappversion);
    app.post('/service/ubeam/getreassignjobdetail', UbeamTP.getreassignjobdetail);
    app.post('/service/ubeam/getPassengerDetail', UbeamTP.getPassengerDetail);
    app.post('/service/ubeam/askGGcharge', UbeamTP.askGGcharge);
    app.post('/service/ubeam/DrvGGAddMoney', UbeamTP.DrvGGAddMoney);
    app.post('/service/ubeam/DrvGGAddHour', UbeamTP.DrvGGAddHour);
    app.post('/service/ubeam/askAPPcharge', UbeamTP.askAPPcharge);
    app.post('/service/ubeam/DrvAPPAddMoney', UbeamTP.DrvAPPAddMoney);
    app.post('/service/ubeam/DrvAPPAddHour', UbeamTP.DrvAPPAddHour);
    app.post('/service/ubeam/getPOIandParking', UbeamTP.getPOIandParking);
    app.post('/service/ubeam/UpdateAllDriverInfo', UbeamTP.UpdateAllDriverInfo);
    app.post('/service/ubeam/updatedrvcgroup', UbeamTP.updatedrvcgroup);
    app.post('/service/ubeam/getjoblistbydate', UbeamTP.getjoblistbydate);
    app.post('/service/ubeam/searchFinishList', isLoggedIn, UbeamTP.searchFinishList);
    app.post('/service/ubeam/announcementAdd', UbeamTP.announcementAdd);
    app.post('/service/ubeam/announcementEdit', UbeamTP.announcementEdit);
    app.post('/service/ubeam/announcementDel', UbeamTP.announcementDel);    
    app.post('/service/ubeam/getpoirecommended', UbeamTP.getpoirecommended);
    app.post('/service/ubeam/getpoirecommendedgroup', UbeamTP.getpoirecommendedgroup);
    app.post('/service/ubeam/getpoirecommendedgrouplot', UbeamTP.getpoirecommendedgrouplot);
    app.post('/service/ubeam/UpdateAllDrivercgroupname', UbeamTP.UpdateAllDrivercgroupname);
    app.post('/service/ubeam/doccbroadcast', UbeamTP.doccbroadcast(io));
    app.post('/service/ubeam/FindDrvinRadian', UbeamTP.FindDrvinRadian);

    /////////////////// for Report ////////////////////////////////////////
    app.post('/service/ubeam/CountJobPerDrv', UbeamTP.CountJobPerDrv);
    app.post('/service/ubeam/CountAppJobPerDrv', UbeamTP.CountAppJobPerDrv);
    app.post('/service/ubeam/getAvgGetJobPerMinute', UbeamTP.getAvgGetJobPerMinute);
    app.post('/service/ubeam/MostHitStartPlace', UbeamTP.MostHitStartPlace);
    app.post('/service/ubeam/MostHitDestinationPlace', UbeamTP.MostHitDestinationPlace);
    app.post('/service/ubeam/MostHitDestinationPlace', UbeamTP.MostHitDestinationPlace);
    app.post('/service/ubeam/MostHitHours', UbeamTP.MostHitHours);
    app.post('/service/ubeam/PsgCallLog', UbeamTP.PsgCallLog);
    app.post('/service/ubeam/JoblistLog', UbeamTP.JoblistLog);
    app.post('/service/ubeam/SearchDrvAll', UbeamTP.searchdrvall);
    app.post('/service/ubeam/searchPassengerDrv', UbeamTP.driverSearchPassengerDrv);
    app.post('/service/ubeam/getJobListLogAll', UbeamTP.getJobListLogAll);
    app.post('/service/ubeam/getJobListLogWait', UbeamTP.getJobListLogWait);
    app.post('/service/ubeam/getJobListLogPick', UbeamTP.getJobListLogPick);
    app.post('/service/ubeam/getJobListLogAccept', UbeamTP.getJobListLogAccept);
    app.post('/service/ubeam/getJobListLogDrop', UbeamTP.getJobListLogDrop);
    app.post('/service/ubeam/getJobListLogPSGCancel', UbeamTP.getJobListLogPSGCancel);
    app.post('/service/ubeam/getJobListLogDRVCancel', UbeamTP.getJobListLogDRVCancel);
    app.post('/service/ubeam/getdrvjobsincgroup', UbeamTP.getdrvjobsincgroup);
    app.post('/service/ubeam/checkcitizenid', UbeamTP.checkcitizenid);

    /////////////////// for SC_getmeterstatus ////////////////////////////////////////
    app.post('/service/ubeam/getmeterstatus', UbeamTP.getmeterstatus);

    ///////////////////////////////  for ccadmin //////////////////////////////////////////////////////////////
    var multer = require('multer');
    app.post('/service/ubeam/getdriverdlist', UbeamTP.getdriverdlist);
    app.post('/service/ubeam/addjob2dlist', UbeamTP.addjob2dlist(io));
    app.post('/service/ubeam/getdriverlist', isLoggedIn, UbeamTP.getdriverlist);
    app.post('/service/ubeam/getdriverpendinglist', isLoggedIn, UbeamTP.getdriverpendinglist);
    app.post('/service/ubeam/getdriverdetail', isLoggedIn, UbeamTP.getdriverdetail);
    app.post('/service/ubeam/drvregister', multer({ dest: '../temp/files/' }), isLoggedIn, UbeamTP.drvregister(io));
    app.post('/service/ubeam/driverEdit', multer({ dest: '../temp/files/' }), isLoggedIn, UbeamTP.driverEdit(io));
    app.post('/service/ubeam/driverRemove', isLoggedIn, UbeamTP.driverRemove);
    app.post('/service/ubeam/driverToggleActive', isLoggedIn, UbeamTP.driverToggleActive);

    ///////////////////////////////  for admin //////////////////////////////////////////////////////////////
    app.post('/service/admin/getCancelJobList', isLoggedIn, Admin.getCancelJobList);
    app.post('/service/admin/getCancelJobDetail', isLoggedIn, Admin.getCancelJobDetail);
    app.post('/service/admin/getCancelJobListByDriver', isLoggedIn, Admin.getCancelJobListByDriver);
    app.post('/service/admin/getVerifiedCancelJobList', isLoggedIn, Admin.getVerifiedCancelJobList);
    app.post('/service/admin/verifyCancelJob', isLoggedIn, Admin.verifyCancelJob);
    app.get('/service/admin/getDriverList', isLoggedIn, Admin.getDriverList);
    app.get('/service/admin/getDriverDetail/:id', isLoggedIn, Admin.getDriverDetail);
    app.post('/service/admin/editDriver', multer({ dest: '../temp/files/' }), isLoggedIn, Admin.editDriver);
    app.post('/service/admin/getAllGarage', isLoggedIn, UbeamTP.getAllGarage);

    ///////////////////////////////  for admin notification //////////////////////////////////////////////////////////////
    app.get('/admin/notification', isLoggedIn, Ecadmin.notificationManagement);
    app.post('/admin/notification/getAllGarage', isLoggedIn, Notification.getAllGarage);
    app.post('/admin/notification/getAllDriver', isLoggedIn, Notification.getAllDriver);
    app.post('/admin/notification/getNotificationList', isLoggedIn, Notification.getNotificationList);
    app.post('/admin/notification/getNotificationDetail', isLoggedIn, Notification.getNotificationDetail);
    app.post('/admin/notification/addNotification', multer({ dest: '../temp/files/' }), Notification.addNotification(app.get('notificationSender')));
    app.post('/admin/notification/editNotification', multer({ dest: '../temp/files/' }), Notification.editNotification(app.get('notificationSender')));

    ///////////////////////////////  for admin notification //////////////////////////////////////////////////////////////
    app.post('/service/drivercs/notification/update', DriverCS.updateNotification(io));
    app.post('/service/drivercs/notification/sync', DriverCS.syncUpNotification);

    // # # # # # # # # # # # # # # # Log Service start # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # //
    app.post('/service/log/PsgCallLog', LogService.PsgCallLog);
    app.post('/service/log/getMobileLog', LogService.getMobileLog);
    app.post('/service/log/getCallcenterLog', LogService.getCallcenterLog);
    app.post('/service/log/JoblistLog', LogService.JoblistLog);
    app.post("/service/log/getTaxiUsageList", LogService.getTaxiUsageList);
    app.post("/service/log/getCallcenterUsageList", LogService.getCallcenterUsageList);
    app.post("/service/log/mostHit", LogService.MostHitHours, LogService.MostHitStartPlace, LogService.MostHitDestinationPlace);
    app.post("/service/log/hcdata/:appRequest", LogService.hcData);
    app.post("/service/log/hcdata-callcenter", LogService.hcDataCallcenter);
    app.post("/service/log/countTaxiTaskByGarage/:responseType", LogService.countTaxiTaskByGarage);
    // # # # # # # # # # # # # # # # Log Service end # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # //

    app.post('/service/ubeam/getAllGarage', UbeamTP.getAllGarage);
    app.post('/service/ubeam/announcement/all', UbeamTP.callcenterAnnounceAll);
    app.post('/service/ubeam/announcement/add', UbeamTP.callcenterAnnounceAdd);
    app.post('/service/ubeam/announcement/edit', UbeamTP.callcenterAnnounceEdit);
    app.post('/service/ubeam/announcement/delete', UbeamTP.callcenterAnnounceDel);
    app.post('/service/ubeam/UpdateExpAPPDate', UbeamTP.UpdateExpAPPDate);
    app.post('/service/ubeam/TestUpdatePassword', UbeamTP.TestUpdatePassword);

    // new API  passenger touchpong version ///////////////////////////////////////////////////////////////////////////  
    app.post('/service/passengertp/loadconfig', PassengerTP.passengerloadconfig);
    app.post('/service/passengertp/searchDrv', PassengerTP.passengerSearchDrv);
    app.post('/service/passengertp/passengerSearchDrvView', PassengerTP.passengerSearchDrvView);
    app.post('/service/passengertp/callDrv', PassengerTP.passengerCallDrv(io));
    //app.get('/service/passengertp/callDrv', PassengerTP.passengerCallDrv(io));
    //app.get('/service/passengertp/searchDrv2', PassengerTP.passengerSearchDrv);
    app.post('/service/passengertp/register', PassengerTP.passengerRegister);
    app.post('/service/passengertp/Recall', PassengerTP.passengerReCall(io));
    app.post('/service/passengertp/getStatus', PassengerTP.passengerGetStatus);
    app.post('/service/passengertp/acceptDrv', PassengerTP.passengerAcceptDrv(io));
    app.post('/service/passengertp/cancelCall', PassengerTP.passengerCancelCall(io));
    app.post('/service/passengertp/getByID', PassengerTP.passengerGetByID);
    app.post('/service/passengertp/sendComment', PassengerTP.passengerSendComment);
    app.post('/service/passengertp/getDrvLoc', PassengerTP.passengerGetDrvLoc);
    app.post('/service/passengertp/endTrip', PassengerTP.passengerEndTrip);
    app.post('/service/passengertp/FavDrvAdd', PassengerTP.passengerFavDrvAdd);
    app.post('/service/passengertp/FavDrvDel', PassengerTP.passengerFavDrvDel);
    app.post('/service/passengertp/Psgcalllog', PassengerTP.Psgcalllog);
    app.post('/service/passengertp/getPassengerHistory', PassengerTP.getPassengerHistory);
    //app.post('/service/passengertp/passengerCallDirect', PassengerTP.passengerCallDirect);
    app.post('/service/passengertp/sendSMSRegister', PassengerTP.sendSMSRegister);
    app.post('/service/passengertp/UpdatePhoneNumber', PassengerTP.UpdatePhoneNumber);
    app.post('/service/passengertp/loadpsgfare', PassengerTP.loadpsgfare);
    app.post('/service/passengertp/psgshareactivities', PassengerTP.psgshareactivities);
    app.post('/service/passengertp/psgsaveactivities', PassengerTP.psgsaveactivities);
    // app.get('/service/passengertp/callDrv', function(req, res) {
    //   console.log("All query strings: " + JSON.stringify(req.query));
    // });

    // new API  passenger touchpong version ///////////////////////////////////////////////////////////////////////////  
    app.post('/smiles/service/passengertp/loadconfig', PassengerTP.passengerloadconfig);
    app.post('/smiles/service/passengertp/searchDrv', PassengerTP.passengerSearchDrv);
    app.post('/smiles/service/passengertp/passengerSearchDrvView', PassengerTP.passengerSearchDrvView);
    app.post('/smiles/service/passengertp/callDrv', PassengerTP.passengerCallDrv(io));  
    app.post('/smiles/service/passengertp/register', PassengerTP.passengerRegister);
    app.post('/smiles/service/passengertp/Recall', PassengerTP.passengerReCall(io));
    app.post('/smiles/service/passengertp/getStatus', PassengerTP.passengerGetStatus);
    app.post('/smiles/service/passengertp/acceptDrv', PassengerTP.passengerAcceptDrv(io));
    app.post('/smiles/service/passengertp/cancelCall', PassengerTP.passengerCancelCall(io));
    app.post('/smiles/service/passengertp/cancelDrv', PassengerTP.passengerCancelDrv(io));
    app.post('/smiles/service/passengertp/getByID', PassengerTP.passengerGetByID);
    app.post('/smiles/service/passengertp/sendComment', PassengerTP.passengerSendComment);
    app.post('/smiles/service/passengertp/getDrvLoc', PassengerTP.passengerGetDrvLoc);
    app.post('/smiles/service/passengertp/endTrip', PassengerTP.passengerEndTrip);
    app.post('/smiles/service/passengertp/FavDrvAdd', PassengerTP.passengerFavDrvAdd);
    app.post('/smiles/service/passengertp/FavDrvDel', PassengerTP.passengerFavDrvDel);
    app.post('/smiles/service/passengertp/Psgcalllog', PassengerTP.Psgcalllog);
    app.post('/smiles/service/passengertp/getPassengerHistory', PassengerTP.getPassengerHistory);
    //app.post('/smiles/service/passengertp/passengerCallDirect', PassengerTP.passengerCallDirect);
    app.post('/smiles/service/passengertp/sendSMSRegister', PassengerTP.sendSMSRegister);
    app.post('/smiles/service/passengertp/UpdatePhoneNumber', PassengerTP.UpdatePhoneNumber);
    app.post('/smiles/service/passengertp/loadpsgfare', PassengerTP.loadpsgfare);
    app.post('/smiles/service/passengertp/psgshareactivities', PassengerTP.psgshareactivities);
    app.post('/smiles/service/passengertp/psgsaveactivities', PassengerTP.psgsaveactivities);

    // Hotel API ///////////////////////////////////////////////////////////////////////////     
    app.post('/service/hotels/register', Hotels.hotelRegister);
    app.post('/service/hotels/updateProfile', Hotels.hotelUpdateProfile);
    app.post('/service/hotels/GetStatus', Hotels.hotelGetStatus);
    app.post('/service/hotels/SearchDrv', Hotels.hotelSearchDrv);
    app.post('/service/hotels/CallDrv', Hotels.hotelCallDrv);
    app.post('/service/hotels/hotelReCall', Hotels.hotelReCall);
    app.post('/service/hotels/hotelTaxiArrived', Hotels.hotelTaxiArrived);
    app.post('/service/hotels/viewJoblist', Hotels.hotelviewJoblist);
    app.post('/service/hotels/CancelCall', Hotels.hotelCancelCall(io));
    app.post('/service/hotels/getHotelHistory', Hotels.getHotelHistory);
    app.post('/service/hotels/hotelDeleteJob', Hotels.hotelDeleteJob);

    app.post('/socket/getDriverOnline', SocketController.getDriverOnline(io));

    app.post('/passenger/poi/add/:phone', isLoggedIn, Hotels.addNewPOIs);
    app.post('/passenger/poi/edit/:id', isLoggedIn, Hotels.editPOI);
    app.post('/passenger/poi/remove/:id', isLoggedIn, Hotels.removePOI);
    app.post('/passenger/poi/remove/all/:passengerId', isLoggedIn, Hotels.removeAllPOIs);
    app.post('/passenger/poi/list/all', isLoggedIn, Hotels.getAllPOIsByGarage);
    app.get('/passenger/poi/list/:passengerId', isLoggedIn, Hotels.getAllPOIsByPassengerId);
    app.post('/passenger/poi/list/byphone/:phone', isLoggedIn, Hotels.getAllPOIsByPhone);
    app.post('/passenger/poi/search/byphone/:phone/:garage', isLoggedIn, Hotels.searchPOIsByPhone);
    app.get('/passenger/poi/get/:id', isLoggedIn, Hotels.getPOI);
    app.get('/passenger/poi/find/start/:passengerId', isLoggedIn, Hotels.getPOIStart);
    app.get('/passenger/poi/find/end/:passengerId', isLoggedIn, Hotels.getPOIEnd);
    app.post('/passenger/getHistory/byphone', Ecadmin.getPassengerHistoryByPhone);


    app.post('/garage/poi/add', Garage.addNewPOI);
    app.post('/garage/poi/edit', Garage.editPOI);
    app.post('/garage/poi/delete', Garage.removePOI);
    app.post('/garage/poi/updateCurrentPOI', Garage.updateCurrentPOI);


    /////////////////// Start Samsung functtion ////////////////////////////////////////
    app.post('/sspsg/v1/gettaxilist', checkSAMSUNGAPIKey, Samsung.gettaxilist);
    app.post('/sspsg/v1/gettaxiinfo', checkSAMSUNGAPIKey, Samsung.gettaxiinfo);
    app.post('/sspsg/v1/getsspsgstatus/:passenger_ssid', checkSAMSUNGAPIKey, Samsung.getsspsgstatus);
    app.post('/sspsg/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid', checkSAMSUNGAPIKey, Samsung.passengercreatejob);
    app.put('/sspsg/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/finish', checkSAMSUNGAPIKey, Samsung.passengerfinishjob);
    app.put('/sspsg/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/cancel', checkSAMSUNGAPIKey, Samsung.passengercanceljob(io));
    app.post('/sspsg/v1/getsercharge', checkSAMSUNGAPIKey, Samsung.getsercharge);
    ////////////// for testing as a Samsung server
    app.post('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid', checkSAMSUNGAPIKey, Samsung.driverreviewedjob);
    app.post('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/accept', checkSAMSUNGAPIKey, Samsung.driveraccepted);
    app.put('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/pickup', checkSAMSUNGAPIKey, Samsung.driverpickedup);
    app.put('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/drop', checkSAMSUNGAPIKey, Samsung.driverdropped);
    app.put('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/cancel', checkSAMSUNGAPIKey, Samsung.drivercancelled);
    /////////////////// End Samsung function *****************************************************


    //////////////////////////////////// Start KKNBeamPass Team //////////////////////////////////////
    /// KKNBeamPass for web app
    app.post('/kknexe/v1/gettaxilist', checkKKNBPAPIKey, KKNBeamPass.gettaxilist);
    app.post('/kknexe/v1/gettaxiinfo', checkKKNBPAPIKey, KKNBeamPass.gettaxiinfo);
    app.post('/kknexe/v1/getsspsgstatus/:passenger_ssid', checkKKNBPAPIKey, KKNBeamPass.getsspsgstatus);
    app.post('/kknexe/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid', checkKKNBPAPIKey, KKNBeamPass.passengercreatejob);
    app.put('/kknexe/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/finish', checkKKNBPAPIKey, KKNBeamPass.passengerfinishjob);
    app.put('/kknexe/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/cancel', checkKKNBPAPIKey, KKNBeamPass.passengercanceljob(io));
    app.post('/kknexe/v1/getsercharge', checkKKNBPAPIKey, KKNBeamPass.getsercharge);
    /// KKNBeamPass for psg app
    app.post('/kknexe/v1/psgloadconfig', checkKKNBPAPIKey, KKNBeamPass.psgloadconfig);
    app.post('/kknexe/v1/psggetsercharge', checkKKNBPAPIKey, KKNBeamPass.psggetsercharge);
    app.post('/kknexe/v1/psgregister', checkKKNBPAPIKey, KKNBeamPass.psgregister);
    app.post('/kknexe/v1/psglogin', checkKKNBPAPIKey, KKNBeamPass.psglogin);
    app.post('/kknexe/v1/psgotprequest', checkKKNBPAPIKey, KKNBeamPass.psgotprequest);
    app.post('/kknexe/v1/psgsmsconfirm', checkKKNBPAPIKey, KKNBeamPass.psgsmsconfirm);
    app.post('/kknexe/v1/psgupdateprofile', checkKKNBPAPIKey, KKNBeamPass.psgupdateprofile);
    app.post('/kknexe/v1/psgsmsprofileconfirm', checkKKNBPAPIKey, KKNBeamPass.psgsmsprofileconfirm);
    app.post('/kknexe/v1/psguploadface', checkKKNBPAPIKey, KKNBeamPass.psguploadface);
    app.post('/kknexe/v1/psggettaxilist', checkKKNBPAPIKey, KKNBeamPass.psggettaxilist);
    app.post('/kknexe/v1/psggettaxiinfo', checkKKNBPAPIKey, KKNBeamPass.psggettaxiinfo);
    app.post('/kknexe/v1/psggetstatus', checkKKNBPAPIKey, KKNBeamPass.psggetstatus);
    app.post('/kknexe/v1/psgcalldrv', checkKKNBPAPIKey, KKNBeamPass.psgcalldrv);
    app.post('/kknexe/v1/psgrecall', checkKKNBPAPIKey, KKNBeamPass.psgrecall);
    app.post('/kknexe/v1/psgendtrip', checkKKNBPAPIKey, KKNBeamPass.psgendtrip);
    app.post('/kknexe/v1/psgcancelcall', checkKKNBPAPIKey, KKNBeamPass.psgcancelcall(io));
    app.post('/kknexe/v1/psggetwelcome', checkKKNBPAPIKey, KKNBeamPass.psggetwelcome);
    app.post('/kknexe/v1/psggetabout', checkKKNBPAPIKey, KKNBeamPass.psggetabout);
    app.post('/kknexe/v1/psggetterm', checkKKNBPAPIKey, KKNBeamPass.psggetterm);
    app.post('/kknexe/v1/psggetnewsact', checkKKNBPAPIKey, KKNBeamPass.psggetnewsact);
    app.post('/kknexe/v1/psggetprbanner', checkKKNBPAPIKey, KKNBeamPass.psggetprbanner);
    app.post('/kknexe/v1/psggetpromotion', checkKKNBPAPIKey, KKNBeamPass.psggetpromotion);
    app.post('/kknexe/v1/psggethistory', checkKKNBPAPIKey, KKNBeamPass.psggethistory);
    app.post('/kknexe/v1/psggethistorydetail', checkKKNBPAPIKey, KKNBeamPass.psggethistorydetail);
    //////////////////////////////////// End KKNBeamPass Team ******************************************


    /////////////////// Start BeamPartner function ////////////////////////////////////////
    app.post('/bpnpsg/v1/gettaxilist', checkBEAMPARTNERAPIKey, BeamPartner.gettaxilist);
    app.post('/bpnpsg/v1/gettaxiinfo', checkBEAMPARTNERAPIKey, BeamPartner.gettaxiinfo);
    app.post('/bpnpsg/v1/getpsgstatus/:passenger_bpnid', checkBEAMPARTNERAPIKey, BeamPartner.getsspsgstatus);
    app.post('/bpnpsg/v1/bpnpassenger/:passenger_bpnid/requests/:passenger_jobid', checkBEAMPARTNERAPIKey, BeamPartner.passengercreatejob);
    app.put('/bpnpsg/v1/bpnpassenger/:passenger_bpnid/requests/:passenger_jobid/finish', checkBEAMPARTNERAPIKey, BeamPartner.passengerfinishjob);
    app.put('/bpnpsg/v1/bpnpassenger/:passenger_bpnid/requests/:passenger_jobid/cancel', checkBEAMPARTNERAPIKey, BeamPartner.passengercanceljob(io));
    app.post('/bpnpsg/v1/getsercharge', checkBEAMPARTNERAPIKey, BeamPartner.getsercharge);
    ////////////// for testing as a Samsung server
    // app.post('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid', checkSAMSUNGAPIKey, Samsung.driverreviewedjob); 
    // app.post('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/accept', checkSAMSUNGAPIKey, Samsung.driveraccepted); 
    // app.put('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/pickup', checkSAMSUNGAPIKey, Samsung.driverpickedup); 
    // app.put('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/drop', checkSAMSUNGAPIKey, Samsung.driverdropped);
    // app.put('/g_services/v1/sspassenger/:passenger_ssid/requests/:passenger_jobid/cancel', checkSAMSUNGAPIKey, Samsung.drivercancelled);
    /////////////////// End BeamPartner function *****************************************************

    // Start new API driver countryside version ////////////////////////////////////////////////////////////////////////////////
    app.post('/bt/v1/session', checkBOONTERMAPIKey, Boonterm.Bsession);
    app.post('/bt/v1/transaction', checkBOONTERMAPIKey, Boonterm.Btransaction(io));
    // End new API driver countryside version ////////////////////////////////////////////////////////////////////////////////

    /////////////////// Start HALOBEAM functtion ////////////////////////////////////////
    app.post('/halobeam/v1/getgaragelist', checkHALOBEAMAPIKey, HaloBeam.getgaragelist);
    app.post('/halobeam/v1/gettaxilist', checkHALOBEAMAPIKey, HaloBeam.gettaxilist);
    app.post('/halobeam/v1/psgcalllog', checkHALOBEAMAPIKey, HaloBeam.psgcalllog);
    // app.post('/halobeam/v1/gettaxiinfo', checkHALOBEAMAPIKey, HaloBeam.gettaxiinfo);
    // app.post('/halobeam/v1/psgsendshare', checkHALOBEAMAPIKey, HaloBeam.psgsendshare);
    // app.post('/halobeam/v1/psgsaveshare', checkHALOBEAMAPIKey, HaloBeam.psgsaveshare);
    /////////////////// END HALOBEAM functtion ////////////////////////////////////////

    //app.post('/kknexe/v1/gettaxilist', checkKKNBPAPIKey, KKNBeamPass.gettaxilist);
    /*Handling routes.*/
    //app.get('/',function(req,res){
    //    res.sendfile("index.html");
    //});

    app.post('/api/photo', function (req, res) {
        if (done == true) {
            //console.log(req.files);
            res.end("File uploaded.");
        }
    });


    app.get('/bot', function (req, res) {
        res.render("bot.ejs");
    });


    app.get('/report', isLoggedIn, function (req, res) {
        res.render("callcenter/report-window.ejs", {
            user: req.user
        });
    });


    app.get('/closeopener', isLoggedIn, function (req, res) {
        res.render("callcenter/closeopener.ejs", {
            user: req.user
        });
    });


    app.get('/msghistory/:phone', DriverCS.msghistory);


    // Use quickthumb
    //app.use(qt.static(__dirname + '/'));


    // Show the upload form 
    /*
    app.get('/', function (req, res){
      res.writeHead(200, {'Content-Type': 'text/html' });
      var form = '<form action="/service/driver/uploadFace" enctype="multipart/form-data" method="post">Add a title: <input name="title" type="text" /><br><br><input multiple="multiple" name="upload" type="file" /><br><br><input type="submit" value="Upload" /></form>';
      res.end(form); 
    }); 
    */


    // --------------------------------------------
    //      AUTHENTICATE (FIRST LOGIN)
    // --------------------------------------------
    // LOGIN ===============================
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // SIGNUP =================================
    // app.post('/signup', passport.authenticate('local-signup', {
    //     successRedirect: '/signup', // redirect to the secure profile section
    //     failureRedirect: '/signup', // redirect back to the signup page if there is an error
    //     failureFlash: true // allow flash messages
    // }));
    app.post('/signup', function (req, res, next) {
        passport.authenticate('local-signup', function (error, user, info) {
            if (error) {
                return res.status(500).json(error);
            }
            if (!user) {
                return res.status(401).json(info.message);
            }
            res.json({
                status: true,
                data: user
            });
        })(req, res, next);
    });

    // --------------------------------------------
    //      AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) 
    // --------------------------------------------
    app.get('/connect/local', function (req, res) {
        res.render('connect-local.ejs', { message: req.flash('loginMessage') });
    });
    app.post('/connect/local', passport.authenticate('local-signup', {
        successRedirect: '/selectpage', // redirect to the secure profile section
        failureRedirect: '/connect/local', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));



    // --------------------------------------------
    //      UNLINK ACCOUNTS 
    // --------------------------------------------
    app.get('/unlink/local', isLoggedIn, function (req, res) {
        var users = req.user;
        users.local.email = undefined;
        user.local.password = undefined;
        user.save(function (err) {
            res.redirect('/');
        });
    });



    app.get('/', function (req, res) {
        if (req.headers.host == config.testhostcallcenter || req.headers.host == config.hostcallcenter) {
            if (req.isAuthenticated()) {
                Ecadmin.callcenter(req, res);
            } else {
                res.sendfile('./public/app/login.html');
            }
        } else if (req.headers.host == "admin.taxi-beam.com" || req.headers.host == "admin-dev.taxi-beam.com" || req.headers.host == "admin-test.taxi-beam.com") {
            console.log(req.headers.host)
            if (req.isAuthenticated()) {
                Ecadmin.admin(req, res);
            } else {
                res.sendfile('./public/app/login.html');
            }
        } else if (req.headers.host == "localhost" || req.headers.host == "lite-dev.taxi-beam.com" || req.headers.host == "lite-test.taxi-beam.com") {
            if (typeof req.user !== 'undefined') {
                Ecadmin.calltaxi(req, res)
            } else {
                console.log('oktaxi 1')
                res.render('oktaxi.ejs');
            }
        } else {
            console.log('oktaxi 2')
            res.render('oktaxi.ejs');
        }
    });


    app.get('/viewalldrv', function (req, res) {
        Ecadmin.viewalldrv(req, res)
    });

    // app.get('/viewdrvpsg_lite', function (req, res) {
    //     Ecadmin.viewdrvpsg_lite(req, res)
    // });

    app.get('/calltaxi_lite', function (req, res) {
        Ecadmin.calltaxi_lite(req, res)
    });

    app.get('/smartbus', function (req, res) {
        Ecadmin.smartbus(req, res)
    });

    app.get('/passenger', Ecadmin.web);
    //app.get('/web', Ecadmin.web);
    app.get('/question', Ecadmin.question);
    app.get('/contactus', Ecadmin.contactus);
    app.get('/calltaxi', isPSGLoggedIn, Ecadmin.calltaxi);
    app.get('/user/signup', Ecadmin.signup);
    app.get('/profile', isLoggedIn, Ecadmin.profile);
    app.get('/adminviewgarage', isLoggedIn, Ecadmin.adminviewgarage);
    app.get('/ccadmin/drivers', isLoggedIn, Ecadmin.ccadmindriverlist);
    app.get('/ccadmin/driver/pending', isLoggedIn, Ecadmin.driverpendinglist);
    app.get('/ccadmin/settings', isLoggedIn, Ecadmin.ccadminsettings);
    app.get('/ccadmin/poimanager', isLoggedIn, Ecadmin.POIManager);
    app.get('/ccadmindriverlist', isLoggedIn, Ecadmin.ccadmindriverlist);
    app.get('/ecartreport', isLoggedIn, Ecadmin.adminEcartReport);
    app.get('/admin', isLoggedIn, Ecadmin.admin);
    app.get('/admin/report', isLoggedIn, Ecadmin.adminReport);
    app.get('/admin/monitor', isLoggedIn, Ecadmin.adminMonitoring);
    app.get('/admin/old', isLoggedIn, Ecadmin.adminV1);
    app.get('/admin/job/cancel', isLoggedIn, Ecadmin.jobCancelManager);
    app.get('/admin/driver/list', isLoggedIn, Ecadmin.driverList);
    app.get('/user/changepwd', isLoggedIn, Ecadmin.changepwd);
    app.get('/user/templ.js', Ecadmin.template);
    app.get('/callcenter', Ecadmin.callcenter);
    app.get('/autocall', Ecadmin.autocall);

    app.get('/index', function (req, res) {
        res.render('index.ejs');
    });



    // =====================================
    // Admin SECTION =====================
    // =====================================
    app.get('/login', function (req, res) {
        res.sendfile('./public/app/login.html');
    });
    app.get('/selectpage', isLoggedIn, function (req, res) {
        res.sendfile('./public/app/selectpage.html');
    });
    // app.get('/signup', function (req, res) {
    //     res.sendfile('./public/app/signup.html');
    // });
    app.get('/chat', isLoggedIn, function (req, res) {
        var user_gr = req.user.group; // ["user"]   // req.user.group ;
        res.render('chat.ejs', {
            user: req.user,  // get the user out of session and pass to template
            user_gr: user_gr
        });
    });
    app.get('/abc', UbeamTP.getinitiatelist);
    //app.get('/user/templ.js'      , isLoggedIn, Ecadmin.template ); 
    app.get('/download', Ecadmin.download);
    app.get('/psgdownload', Ecadmin.psgdownload);
    app.get('/drvdownload', Ecadmin.drvdownload);
    //app.get('/tools/jsontable'        , Ecadmin.jsontable);        
    app.get('/templ/get', Ecadmin.templ_get);
    app.get('/templ/index', isLoggedIn, Ecadmin.templ_index);
    app.get('/templ/new', isLoggedIn, Ecadmin.templ_new);
    app.get('/templ/edit', isLoggedIn, Ecadmin.templ_edit);
    app.get('/templ/get_combofile', Ecadmin.get_combofile);
    app.get('/templ/list_folder_pins', Ecadmin.list_folder_pins);
    app.get('/templ/list_pins', Ecadmin.list_pins);



    // =====================================
    // Passenger SECTION =====================
    // =====================================
    // --------------------------------------------
    //      AUTHENTICATE (FIRST LOGIN) for Passenger
    // --------------------------------------------
    // process the login form for passenger  Previous working version with passport 
    app.post('/psglogin', passport.authenticate('local-psg-login', {
        //successRedirect : '/psgcalltaxi', // redirect to the secure profile section
        successRedirect: '/calltaxi', // redirect to the secure profile section
        failureRedirect: '/psglogin', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // not working with passport
    // app.post("/psglogin", passport.authenticate('local-psg-login',
    //     { failureRedirect: '/psglogin',
    //     failureFlash: true }), function(req, res) {
    //     if (req.body.remember) {
    //         req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000; 
    //     } else {
    //         req.session.cookie.expires = false; 
    //     }
    //     res.redirect('/calltaxi');
    // });

    // process the signup form for passenger
    app.post('/psgsignup', passport.authenticate('local-psg-signup', {
        successRedirect: '/psglogin', // redirect to the secure profile section
        failureRedirect: '/psgsignup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    app.post('/psgforgetpassword', function (req, res) {
        // call forgetpassword service
        console.log(' for get password ')
    });

    //app.post('/service/ubeam/psgforgetpassword'           , UbeamTP.psgforgetpassword);

    ///////////////////////////////////////// Passenger singup 
    app.get('/psglogin', function (req, res) {
        // render the page and pass in any flash data if it exists
        res.render('psglogin.ejs', { message: req.flash('PSGloginMessage') });
    });
    app.get('/psgsignup', function (req, res) {
        //res.sendfile('./public/app/psgsignup.html');
        res.render('psgsignup.ejs', { message: req.flash('PSGsignupMessage') });
    });
    app.get('/psgforgetpassword', function (req, res) {
        // render the page and pass in any flash data if it exists
        res.render('psgforgetpassword.ejs', {});
    });
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/psgcalltaxi', isLoggedIn, function (req, res) {
        res.render('calltaxi.ejs', {
            appversion: "xxx",
            user: req.user, // get the user out of session and pass to template
            displayName: req.displayName
        });
    });
    app.get('/psgprofile', isLoggedIn, function (req, res) {
        //console.log(req)
        console.log('psgprofile ' + req.displayPicture)
        res.render('psgprofile.ejs', {
            user: req.user,// get the user out of session and pass to template
            displayPicture: req.user.displayPicture,
            displayName: req.user.displayName,
            email: req.user.email,
            phone: req.user.phone,
            gender: req.user.gender

        });
    });

    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email'] }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/calltaxi',
            failureRedirect: '/'
        })
    );
    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/',
            failureRedirect: '/'
        })
    );




    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/psglogout', function (req, res) {
        req.logout();
        res.redirect('/');
    });
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });



    var _defaultPath = './public/app/none.html';

    app.get('/:site', function (req, res) {
        var site = req.params.site;
        //console.log('111'+site)
        if (['passenger', 'passengerdev', 'taxi', 'template', 'ecmap', 'garage', 'ubeam', 'ubeam2'].indexOf(site) != -1) {
            res.sendfile('./public/app/' + site + '/index.html');
        } else {
            res.sendfile('./public/app/login.html');
            //res.sendfile(_defaultPath);               
            //res.render('web.ejs', { });
        }
    });


    app.get('/ubeam/home', function (req, res) {
        res.sendfile('./public/app/ubeam/taxiadmin-home.html');
    });

    app.get('/ubeam/driverlist', function (req, res) {
        res.sendfile('./public/app/ubeam/taxiadmin-driver-list.html');
    });

    app.get('/ubeam/taxilist', function (req, res) {
        res.sendfile('./public/app/ubeam/taxiadmin-taxi-list.html');
    });

    app.get('/ubeam/message', function (req, res) {
        res.sendfile('./public/app/ubeam/taxiadmin-message.html');
    });

    app.get('/ubeam/stats', function (req, res) {
        res.sendfile('./public/app/ubeam/taxiadmin-stat.html');
    });

    app.get('/ubeam/settings', function (req, res) {
        res.sendfile('./public/app/ubeam/taxiadmin-setting.html');
    });

    app.get('*', function (req, res) {
        res.sendfile(_defaultPath);
        //res.render('web.ejs', { });
    });

    // =====================================
    // Testing Section  =======================
    // =====================================
    app.get('/testleaflet', function (req, res) {
        res.render('testleaflet.ejs', {
        });
    });

    app.get('/testleafletgoogle', function (req, res) {
        res.render('testleafletgoogle.ejs', {
        });
    });

    app.get('/calltaxi-bak', function (req, res) {
        res.render('calltaxi-bak.ejs', {
        });
    });
};          //------------------ end module.exports





// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {

    if (req.isAuthenticated()) {
        //console.log(' is loggin = true ')
        return next();
    } else {
        //console.log(' **************************************************** ')
        //console.log(req)
        console.log('isLoggedIn === false for ' + req.url)
        //console.log(' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ')

        //console.log(' **************************************************** ')
        res.redirect('/');
    }
}


// route middleware to make sure a user is logged in
function isPSGLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated()) {
        console.log(' is  psg loggin = true ')
        return next();
    } else {
        console.log(' is  psg loggin = false ')
        res.redirect('/index');
    }
    // if they aren't redirect them to the home page
}


// route middleware to ensure user is logged in
function isAccess(req, res, next) {
    var chk = Ecadmin.ChkAccess(req.user.group, '/admin');
    //  console.log(req.user.group)
    if (chk)
        return next();
    res.redirect('/');
}



function checkSAMSUNGAPIKey(req, res, next) {
    var req_API_KEY = req.headers.api_key;

    if (req_API_KEY == config.SAMSUNG_API_KEY) {
        return next();
    } else {
        res.status(403).json({ status: false, msg: ' 403 Forbidden URL ' });
    }
}



function checkKKNBPAPIKey(req, res, next) {
    var req_API_KEY = req.headers.api_key;

    if (req_API_KEY == config.KKNBP_API_KEY) {
        return next();
    } else {
        res.status(403).json({ status: false, msg: ' 403 Forbidden URL ' });
    }
}



function checkBEAMPARTNERAPIKey(req, res, next) {
    var req_API_KEY = req.headers.api_key;

    if (req_API_KEY == config.BEAMPARTNER_API_KEY) {
        return next();
    } else {
        res.status(403).json({ status: false, msg: ' 403 Forbidden URL ' });
    }
}



function checkBOONTERMAPIKey(req, res, next) {
    var req_API_KEY = req.headers.api_key;

    if (req_API_KEY == config.BOONTERM_API_KEY) {
        return next();
    } else {
        res.json({
            Code: "0001",
            Desc: "Invalid API_KEY"
        });        
    }
}



function _checkdrvshift(req, res, next) {
    var _id = req.body._id;
    var device_id = req.body.device_id;

    DriversModel.findOne(
        { _id: _id, device_id: device_id, _id_garage: { $ne:'597b0e04dc04ea092db44a7c'} },
        function (err, drv) {
            if (drv == null) {
                res.json({
                    status: false,
                    msg: "No driver was found, please login."
                })
            } else {
                if (drv.shift_end > new Date()) {
                    //console.log(' driver can accept job ')
                    drv.firstshift = false;
                    drv.save(function (err, response) {
                        if (err) {
                            res.json({
                                status: false,
                                msg: err
                            })
                        } else {
                            return next();
                        }
                    });                                           
                } else if (drv.drv_shift > 0) {
                    // > 0 -> -1 and set start end time  
                    var PreShift = drv.drv_shift;
                    var PostShift = drv.drv_shift - 1  ;    
                    var shift_start = new Date();
                    var shift_end = new Date().getTime() + (config.TimePerUseAPP*60*60*1000);
                    var firstshift_type;
                    var firstshift_id;
                    if (req.url == '/service/drivercs/gotdispatchaction') {
                        firstshift_type = 'DIRECTCC';
                        firstshift_id = req.body.passengerID;
                    } else if (req.url == '/service/drivercs/acceptCall') {
                        firstshift_type = 'PSG';
                        firstshift_id = req.body.psg_id;
                    } else if (req.url == '/service/drivercs/acceptHotel') {
                        firstshift_type = 'BCHOTEL';
                        firstshift_id = req.body.psg_id;
                    }
                    drv.drv_shift = PostShift ;
                    drv.shift_start = shift_start;
                    drv.shift_end = shift_end;
                    drv.firstshift = true;
                    drv.firstshift_type = firstshift_type;
                    drv.firstshift_id = firstshift_id;
                    drv.save(function (err, response) {
                        if (err) {
                            res.json({
                                status: false,
                                msg: err
                            })
                        } else {
							ShifttransactionModel.create({
								AccountIDs: [ { _id: _id } ] ,
								PreShift: [ { _id: _id, Amount: PreShift } ] ,
                                PostShift: [ { _id: _id, Amount: PostShift } ] ,
                                Amount: 1 ,
                                Type: "Use",
                                AdminID: "SYST",
								DateTime: new Date()
							},
								function (err, response) {
									if (err) {
										res.json({ status: false, msg: err, data: err });
									} else {
										return next();
									}
								}
							);
                        }
                    });
                } else {
                    console.log(' cannot accept job ')
                    res.json({
                        status: false,
                        msg: "Please add your driver shift.",
                        data: {
                            errcode: "NOSHIFT"
                        }
                    })
                }
            }
        }
    );
}



function checkHALOBEAMAPIKey(req, res, next) {
    var req_API_KEY = req.headers.api_key;
    if (req_API_KEY == config.HALOBEAM_API_KEY) {
        return next();
    } else {
        res.status(403).json({ 
            status: false, 
            msg: ' 403 Forbidden URL invalid Key ' 
        });
    }
}



function _checksmilesshift(req, res, next) {
	return next();
    // var _id = req.body._id;
    // var device_id = req.body.device_id;
    // DriversModel.findOne(
    //     { _id: _id, device_id: device_id, _id_garage: '597b0e04dc04ea092db44a7c' },
    //     function (err, drv) {
    //         if (drv == null) {
    //             res.json({
    //                 status: false,
    //                 msg: "No driver was found, please login."
    //             })
    //         } else {
    //             if (drv.drv_wallet > config.smilesprice) {
    //                 var PreWallet = drv.drv_wallet;
    //                 var PostWallet = drv.drv_wallet - config.smilesprice  ;    
    //                 var firstshift_type;
    //                 var firstshift_id;
    //                 if (req.url == '/service/drivercs/gotdispatchaction') {
    //                     firstshift_type = 'DIRECTCC';
    //                     firstshift_id = req.body.passengerID;
    //                 } else if (req.url == '/service/drivercs/acceptCall') {
    //                     firstshift_type = 'PSG';
    //                     firstshift_id = req.body.psg_id;
    //                 } else if (req.url == '/service/drivercs/acceptHotel') {
    //                     firstshift_type = 'BCHOTEL';
    //                     firstshift_id = req.body.psg_id;
    //                 }
    //                 drv.drv_wallet = PostWallet ;
    //                 drv.firstshift = true;
    //                 drv.firstshift_type = firstshift_type;
    //                 drv.firstshift_id = firstshift_id;
    //                 drv.save(function (err, response) {
    //                     if (err) {
    //                         res.json({
    //                             status: false,
    //                             msg: err
    //                         })
    //                     } else {
	// 						WallettransactionModel.create({
	// 							AccountIDs: [ { _id: _id } ] ,
	// 							PreWallet: [ { _id: _id, Amount: PreWallet } ] ,
    //                             PostWallet: [ { _id: _id, Amount: PostWallet } ] ,
    //                             Amount: config.smilesprice ,
    //                             Type: "Use",
    //                             AdminID: "SYST",
	// 							DateTime: new Date()
	// 						},
	// 							function (err, response) {
	// 								if (err) {
	// 									res.json({ status: false, msg: err, data: err });
	// 								} else {
	// 									return next();
	// 								}
	// 							}
	// 						);
    //                     }
    //                 });
    //             } else {
    //                 console.log(' cannot accept job ')
    //                 res.json({
    //                     status: false,
    //                     msg: "Please add your wallet."
    //                 })
    //             }
    //         }
    //     }
    // );
}