////////////////////////////////////
// Taxi-Beam Route for  API 
// version : 1.0.1
// Date revision: May 10, 2016 
// Created by Hanzen@BRET
////////////////////////////////////
//var config = require('../../config/func').production;

var DriverCS = require('../controllers/Drivercs');
var PassengerTP = require('../controllers/Passengertp');
var Hotels = require('../controllers/Hotels');
var Share = require('../controllers/Share');
var Garage = require('../controllers/Garage');
var Ecadmin = require('../controllers/Ecadmin');
var UbeamTP = require('../controllers/Ubeamtp');
var LogService = require('../controllers/LogService');
var SocketController = require('../controllers/SocketController');
//var utils = require('../../config/utils');
//var bcrypt = require('bcrypt');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');

var Boonterm = require('../controllers/Boonterm');
// var fs = require('fs');
// var util = require('util');
// var log_file = fs.createWriteStream('C:/nodejs/nodebug.log', {flags : 'w'});
// var log_stdout = process.stdout;

module.exports = function (router, io, passport) {

    router.use(expressJWT({ secret: 'myScottSummer'}).unless({ path: [ '/boonterm/v1/session', '/api/service/drivercs/login' , '/api/service/drivercs/sendSMSUP'  , '/api/service/drivercs/announcementGet'   ] }));

    /* --------------------------------------------
     *    Service
     * --------------------------------------------*/
    router.get('/service/me', function (req, res) {
        var users = req.users;
        if (user) {
            //res.json({
            console.log(' service me ')
            var user_id = user.local._id
            //});
        }
        //console.log('user_id = '.user_id);
    });
    
    // Start new API driver countryside version ////////////////////////////////////////////////////////////////////////////////
    app.post('/boonterm/v1/session', Boonterm.Bsession);
    app.post('/boonterm/v1/transaction', Boonterm.Btransaction);
    // End new API driver countryside version ////////////////////////////////////////////////////////////////////////////////

    // new API driver countryside version ////////////////////////////////////////////////////////////////////////////////
    router.post('/service/drivercs/Login', DriverCS.driverLogin);
    router.post('/service/drivercs/register', DriverCS.driverRegister);
    //router.post('/service/drivercs/autoLogin'        , DriverCS.driverAutoLogin); 
    router.post('/service/drivercs/checksamecarplate', DriverCS.checksamecarplate);
    router.post('/service/drivercs/checksameaccount', DriverCS.checksameaccount);    
    router.post('/service/drivercs/updateProfile', DriverCS.driverUpdateProfile);
    router.post('/service/drivercs/changeOnOff', DriverCS.driverchangeOnOff);
    router.post('/service/drivercs/getStatus', DriverCS.driverGetStatus(io));
    router.post('/service/drivercs/searchPassenger', DriverCS.driverSearchPassenger);    
    router.post('/service/drivercs/acceptCall', DriverCS.driverAcceptCall(io));
    router.post('/service/drivercs/acceptHotel', DriverCS.driverAcceptHotel(io));    
    router.post('/service/drivercs/cancelCall', DriverCS.driverCancelCall(io));
    router.post('/service/drivercs/cancelHotel', DriverCS.driverCancelHotel(io));
    router.post('/service/drivercs/CancelCallCenter', DriverCS.driverCancelCallCenter(io));
    router.post('/service/drivercs/pickPassenger', DriverCS.driverPickPassenger(io));
    router.post('/service/drivercs/pickHotel', DriverCS.driverPickHotel(io));
    router.post('/service/drivercs/endTrip', DriverCS.driverEndTrip(io));
    router.post('/service/drivercs/EndTripHotel', DriverCS.driverEndTripHotel(io));
    router.post('/service/drivercs/getByID', DriverCS.driverGetByID);
    router.post('/service/drivercs/sendComment', DriverCS.driverSendComment);
    router.post('/service/drivercs/uploadFace', DriverCS.driveruploadFace);
    router.post('/service/drivercs/uploadLicence', DriverCS.driveruploadLicence);
    router.post('/service/drivercs/uploadCar', DriverCS.driveruploadCar);
    router.post('/service/drivercs/brokenAdd', DriverCS.driverBrokenAdd);
    router.post('/service/drivercs/brokenEdit', DriverCS.driverBrokenEdit);
    router.post('/service/drivercs/brokenDel', DriverCS.driverBrokenDel);
    router.post('/service/drivercs/brokenReport', DriverCS.driverBrokenReport);
    router.post('/service/drivercs/brokenCancel', DriverCS.driverBrokenCancel);
    router.post('/service/drivercs/drivergotmsg', DriverCS.drivergotmsg);
    router.post('/service/drivercs/gotdispatchaction', DriverCS.gotdispatchaction(io));
    router.post('/service/drivercs/driverEndTask', DriverCS.driverEndTask(io));

    router.post('/service/drivercs/sendSMSUP', DriverCS.sendSMSUP);
    router.post('/service/drivercs/ChangePWDonly', DriverCS.ChangePWDonly);
    router.post('/service/drivercs/sendSMSConfirm', DriverCS.sendSMSConfirmation);
    router.post('/service/drivercs/driverReturnCar', DriverCS.driverReturnCar);
    router.post('/service/drivercs/GetParkingQueue', DriverCS.driverGetParkingQueue);
    router.post('/service/drivercs/BookParkingQueue', DriverCS.driverBookParkingQueue);
    router.post('/service/drivercs/LeftParkingQueue', DriverCS.driverLeftParkingQueue);
    router.post('/service/drivercs/checkIconURL', DriverCS.checkIconURL);
    router.post('/service/drivercs/getDriverHistoryApp', DriverCS.getDriverHistoryApp);
    router.post('/service/drivercs/getDriverHistoryCC', DriverCS.getDriverHistoryCC);
    
    // This service use from UbeamTP
    router.post('/service/drivercs/announcementGet', UbeamTP.announcementGet);
    router.post('/service/drivercs/askGGcharge', UbeamTP.askGGcharge);
    router.post('/service/drivercs/DrvGGAddMoney', UbeamTP.DrvGGAddMoney);
    router.post('/service/drivercs/DrvGGAddHour', UbeamTP.DrvGGAddHour);
    router.post('/service/ubeam/askAPPcharge', UbeamTP.askAPPcharge);
    router.post('/service/ubeam/DrvAPPAddMoney', UbeamTP.DrvAPPAddMoney);
    router.post('/service/ubeam/DrvAPPAddHour', UbeamTP.DrvAPPAddHour);

    //router.post('/service/drivercs/Activation'       , DriverCS.driverActivation);   
    //router.post('/service/drivercs/testSocket'       , DriverCS.drivertestSocket);
    router.post('/service/drivercs/all', DriverCS.all);


    // new API  passenger touchpong version ///////////////////////////////////////////////////////////////////////////     
    router.post('/service/passengertp/loadconfig', PassengerTP.passengerloadconfig);
    router.post('/service/passengertp/searchDrv', PassengerTP.passengerSearchDrv);
    router.post('/service/passengertp/passengerSearchDrvView', PassengerTP.passengerSearchDrvView);
    router.post('/service/passengertp/callDrv', PassengerTP.passengerCallDrv);
    router.get('/service/passengertp/callDrv', PassengerTP.passengerCallDrv);
    // router.get('/service/passengertp/callDrv', function(req, res) {
    //   console.log("All query strings: " + JSON.stringify(req.query));
    // });
    router.post('/service/passengertp/register', PassengerTP.passengerRegister); 
    router.post('/service/passengertp/Recall', PassengerTP.passengerReCall);
    router.post('/service/passengertp/getStatus', PassengerTP.passengerGetStatus);
    router.post('/service/passengertp/acceptDrv', PassengerTP.passengerAcceptDrv(io));
    router.post('/service/passengertp/cancelCall', PassengerTP.passengerCancelCall(io));
    router.post('/service/passengertp/cancelDrv', PassengerTP.passengerCancelDrv(io));
    router.post('/service/passengertp/getByID', PassengerTP.passengerGetByID);
    router.post('/service/passengertp/sendComment', PassengerTP.passengerSendComment);
    router.post('/service/passengertp/getDrvLoc', PassengerTP.passengerGetDrvLoc);
    router.post('/service/passengertp/endTrip', PassengerTP.passengerEndTrip);
    router.post('/service/passengertp/FavDrvAdd', PassengerTP.passengerFavDrvAdd);
    router.post('/service/passengertp/FavDrvDel', PassengerTP.passengerFavDrvDel);
    router.post('/service/passengertp/Psgcalllog', PassengerTP.Psgcalllog);
    router.post('/service/passengertp/getPassengerHistory', PassengerTP.getPassengerHistory);

    router.post('/service/hotels/register', Hotels.hotelRegister); 
    router.post('/service/hotels/updateProfile', Hotels.hotelUpdateProfile); 
    router.post('/service/hotels/GetStatus', Hotels.hotelGetStatus); 
    router.post('/service/hotels/SearchDrv', Hotels.hotelSearchDrv);
    router.post('/service/hotels/CallDrv', Hotels.hotelCallDrv); 
    router.post('/service/hotels/hotelReCall', Hotels.hotelReCall);
    router.post('/service/hotels/hotelTaxiArrived', Hotels.hotelTaxiArrived);
    router.post('/service/hotels/viewJoblist', Hotels.hotelviewJoblist);
    router.post('/service/hotels/CancelCall', Hotels.hotelCancelCall(io));
    router.post('/service/hotels/getHotelHistory', Hotels.getHotelHistory);
    router.post('/service/hotels/hotelDeleteJob', Hotels.hotelDeleteJob);
    
    router.post('/socket/getDriverOnline', SocketController.getDriverOnline(io));
    

    router.post('/api/photo', function (req, res) {
        if (done == true) {
            //console.log(req.files);
            res.end("File uploaded.");
        }
    });


};          //------------------ end module.exports