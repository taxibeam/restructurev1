var mongoose = require('mongoose') ;
var JoblistModel = mongoose.model('JoblistModel');
var DriversModel = mongoose.model('DriversModel') ;
var DAkeeplogModel = mongoose.model('DAkeeplogModel');

var fs = require('fs-extra');

/*
* Job Cancel Management
* # CORE-47
* CORE Sprint 2, CORE Sprint 3
* หลังจากคนขับรับงานแล้ว ห้ามยกเลิก ในส่วนเรียกงานจากผู้โดยสาร และ Business app
*/
exports.getCancelJobList = function(req, res) {
    JoblistModel
        .where('status').equals('DRIVER_CANCEL')
        .where('cancel_verify').equals(false)
        .where('cgroup').ne(null)
        .limit(100)
        .sort('-created')
        .select('cancel_case_id drv_name drv_phone cgroup curaddr destination created cancel_opened _id')
        .exec(function(err, result) {
            if(err)
                res.json(err);
            res.status(200).json(result);
        });
}

exports.getCancelJobListByDriver = function(req, res) {
    JoblistModel
        .where('status').equals('DRIVER_CANCEL')
        .where('drv_id').equals(req.body.drv_id)
        .limit(100)
        .sort('-created')
        .select('cancel_case_id drv_name drv_phone cgroup curaddr destination created _id cancel_opened drv_id')
        .exec(function(err, result) {
            if(err)
                res.json(err);
            res.status(200).json(result);
        });
}

exports.getCancelJobDetail = function(req, res) {
    JoblistModel
        .where('_id').equals(req.body._id)
        .exec(function(err, result) {
            if(err)
                res.json(err);
            res.status(200).json(result[0]);
        });
}

exports.verifyCancelJob = function(req, res) {
    JoblistModel
        .findOne({ _id: req.body._id })
        .exec(function(err, job) {

            if(err) {
                res.json(err);
            }

            job.cancel_opened = true;
            job.cancel_verify = (parseInt(req.body.cancel_verify) === 1 ? true : false);
            job.cancel_memo = req.body.cancel_memo;
            job.cancel_reason = req.body.cancel_reason;
            
            job.save(function(err, result) {

                if(err) {
                    res.status(200).json({
                        status: true,
                        message: "Fail",
                        data: err
                    });
                }

                res.status(200).json({
                    status: true,
                    message: "Success",
                    data: result
                })
            });
        });
}

exports.getVerifiedCancelJobList = function(req, res) {
    JoblistModel
        .where('status').equals('DRIVER_CANCEL')
        .where('cancel_verify').equals(true)
        .where('cgroup').ne(null)
        .limit(1000)
        .sort('-created')
        .select('cancel_case_id drv_name drv_phone cgroup curaddr destination created _id cancel_opened drv_id')
        .exec(function(err, result) {
            if(err)
                res.json(err);
            res.status(200).json(result);
        });
}


/*
* Admin Driver List Management
*/
exports.getDriverList = function(req, res) {
    DriversModel.find({}).select('username fname lname phone cgroup province carplate carcolor created updated status taxiID citizenid address tambon district zipcode imgcar imglicence imgface drv_point drv_wallet drv_shift shift_end shift_start').exec(function(err, result) {
        res.status(200).json(result);
    });
}

exports.getDriverDetail = function(req, res) {
    DriversModel
        .findById(req.params.id)
            .select('-password')
            .exec(function (err, driver) {

            if (err || !driver) {
                res.status(200).json({ status: false, data: err || driver });
            }

            if(driver) {
                res.status(200).json({ status: true, data: err || driver });
            }
        });
}

exports.editDriver = function (req, res) {
    DriversModel.findOne({ citizenid: req.body.citizenid, _id: { $ne: req.body._id } }, function (err, existDriver) {
        if (err) {
            removeUploadImg(req.files);
            res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
        } else {
            if (existDriver) {
                removeUploadImg(req.files);
                var erro_code = "CITIZENID_DUPLICATED";
                var message = "Your current citizen id is already used.";
                if (existDriver.username === req.body.username) {
                    message = "Your current username is already used.";
                    erro_code = "USERNAME_DUPLICATED"
                }
                res.json({ status: false, code: erro_code, message: message });
            } else {
                DriversModel.findOne({ _id: req.body._id }, function (err, currentDriver) {
                    if (err) {
                        removeUploadImg(req.files);
                        res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
                    } else {

                        Object.keys(req.files).forEach(function (file) {
                            currentDriver[file] = currentDriver._id + '_' + file + '.png';
                        });

                        var diffData = {};

                        Object.keys(req.body).forEach(function (field) {
                            if (currentDriver[field] !== req.body[field] && field !== '_id') {
                                diffData[field] = req.body[field];
                            }
                        });

                        Object.keys(req.files).forEach(function (file) {
                            diffData[file] = currentDriver._id + '_' + file + '.png';
                        });

                        Object.keys(req.body).forEach(function (field) {

                            if (field === '_id') {
                                return;
                            }

                            if (field === 'citizenid') {
                                req.body.citizenid = req.body.citizenid.replace(/ /g, '');
                            }

                            if (field === 'phone') {
                                req.body.phone = req.body.phone.replace(/-/, '').replace(/ /g, '');
                            }

                            currentDriver[field] = req.body[field];
                        });

                        currentDriver.carplate_formal = currentDriver.prefixcarplate + " " + currentDriver.carplate;

                        currentDriver.save(function (err, result) {
                            if (err) {
                                res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
                            } else {

                                DAkeeplogModel.create({
                                    logdata: diffData,
                                    logaction: "DRV_EDIT",
                                    cgroup: req.user.local.cgroup,
                                    username: req.user.username,
                                    _id_user: req.user._id
                                }, function (err, result) {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("create new edit log success...");
                                    }
                                });

                                Object.keys(req.files).forEach(function (file) {
                                    var dest = '../upload_drivers/' + result._id + '_' + file + '.png';
                                    fs.move(req.files[file].path, dest, { clobber: true }, function (err) {
                                        if (err) fs.removeSync(req.files[file].path);
                                    });
                                });

                                res.json({ status: true, driver: result });
                            }
                        });
                    }
                });
            }
        }
    });
}

var removeUploadImg = function (files) {
    Object.keys(files).forEach(function (file) {
        fs.removeSync(files[file].path);
    });
};