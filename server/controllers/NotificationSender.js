/**
 * เดี๋ยวมีต่อใน CORE-511
 */
var _ = require('underscore');
var moment = require('moment');
var lt = require('long-timeout');
var mongoose = require('mongoose');
var NotificationModel = mongoose.model('NotificationModel');
var GarageModel = mongoose.model('Lk_garageModel');
var DriverModel = mongoose.model('DriversModel');


module.exports = {

    /**
     * Cons
     */
    STATE: {
        UNREAD: 'UNREAD',
        READ: 'READ',
        DELETED: 'DELETED'
    },


    /**
     * websocket object รับมาตอนสั่ง intialize
     */
    mSocket: null,


    /**
     * เอาไว้เก็บ id ของ timer แต่ละตัว
     * เพื่อเอาไว้ใช้ในการลบ timer ทิ้งไปเมื่อทำการส่งข้อความแล้ว
     */
    timerUID: {},


    /**
     * สั่ง initial เมื่อตัว web service เริ่ม start
     * @param {object} socket Object ของ web socket ที่ได้จากไฟล์ app.js
     * @param {Function} getClientFallback function เอาไว้หาค่า socket id ที่ได้จากไฟล์ app.js
     */
    initialize: function(socket, getClientFallback) {


        let _this = this;


        /**
         * กำหนด socket object
         */
        _this.mSocket = socket;


        // watching every 5 minutes
        let loopDelay = 5 * ( 60 * 1000 );


        _this.initializeScheduledTimeout(loopDelay);
        _this.initializeActiveTimeout(loopDelay);
    },


    /**
     * initialization 'SCHEDULED' notification task.
     */
    initializeScheduledTimeout: function (loopDelay) {


        let _this = this;


        start = () => {


            /**
             * ค้นหา notification ที่ยังสถานะเป็น SCHEDULED
             */
            NotificationModel.find({ status: 'SCHEDULED' }).exec((err, notifications) => {


                /**
                 * วนลูป notification ที่ query มาได้ วนลูป settimeout เวลาส่งอีกที
                 */
                notifications.forEach((notification, index) => {


                    /**
                     * ใช้ moment หาช่วงเวลาก่อนที่จะต้องส่งข้อความ
                     */
                    var delay = moment(notification.start).diff(moment());


                    /**
                     * ตรวจสอบว่าเป็นอดีตไปแล้วหรือยัง
                     */
                    if (delay < 0) {
                        // console.log('ช่วงเวลาที่กำหนดเป็นอดีตไปแล้ว');
                        return;
                    }


                    if (delay > 0 && delay <= loopDelay) {
                        /**
                         * เริ่มสร้าง timeout object
                         */
                        _this.startCountdown(notification.uid, notification._id, delay);
                    }
                });
            });
        }


        setInterval(start, loopDelay);


        start();
    },


    /**
     * initialization 'ACTIVE' notification task.
     */
    initializeActiveTimeout: function (loopDelay) {


        let _this = this;


        start = () => {


            /**
             * ค้นหา notification ที่ยังสถานะเป็น ACTIVE
             */
            NotificationModel.find({ status: 'ACTIVE' }).exec((err, notifications) => {


                /**
                 * วนลูป notification ที่ query มาได้
                 */
                notifications.forEach((notification, index) => {


                    /**
                     * ใช้ moment หาช่วงเวลาที่เหลือก่อนจะ OBSOLETE
                     */
                    var timeLeft = moment(notification.end).diff(moment());


                    /**
                     * ถ้า timeLeft มีค่ามากว่า 0
                     * และ timeLeft มีค่าน้อยกว่าหรือเท่ากับ loopDelay(1 นาที)
                     * ให้เริ่ม settimeout นับถอยหลังเพื่อ set เป็น Obsolute
                     * อธิบายคือ ช่วงเวลาที่ notification.end ต้องไม่น้อยกว่า 0 หรือยังไม่ถึงเวลา Obsolute
                     * ให้ทำการ countdown to set to Obsolute
                     */
                    if (timeLeft > 0 && timeLeft <= loopDelay) {


                        /**
                         * เริ่มสร้าง timeout object
                         */
                        _this.startCountdownToObsolute(notification.uid, notification._id, timeLeft);
                    }


                    /**
                     * แต่ถ้า timeLeft มีค่าน้อยกว่า 0
                     * แสดงว่ามัน Obsolute ไปแล้ว ให้สั่ง Obsolute ไปได้เลย
                     */
                    else if( timeLeft < 0 ) {

                        var query = { _id: notification._id };
                        var update = { status: 'OBSOLETE' };
                        var options = { new: true };

                        NotificationModel.findOneAndUpdate(query, update, options, function(err, notification) {

                            if (err) {
                                throw err;
                            }

                            _this.mSocket.of('/admin').emit('NOTIFICATION_OBSOLETE', notification);
                            return;
                        });
                    }
                });
            });
        }


        setInterval(start, loopDelay);


        start();
    },


    /**
     * สร้าง timeout object และเริ่มต้นนับถ่อยหลังที่จะส่งข้อความ
     * @param {String} uid รหัสที่ไม่ซ้ำกัน ได้มาจาก notification.uid
     * @param {String} id ไอดี ของ notification (_id)
     * @param {number} delay ช่วงเวลาที่จะนับถ่อยหลังเพื่อเริ่มส่งข้อความ 
     */
    startCountdownToObsolute: function(uid, id, delay) {


        var _this = this;


        /*
        * เริ่ม loop สร้างการนับถอยหลังที่จะทำการส่งข้อความ
        */
        this.timerUID[uid] = lt.setTimeout(() => {

            var query = { _id: id };
            var update = { status: 'OBSOLETE' };
            var options = { new: true };

            NotificationModel.findOneAndUpdate(query, update, options, function(err, notification) {

                if (err) {
                    throw err;
                }

                _this.mSocket.of('/admin').emit('NOTIFICATION_OBSOLETE', notification);
                return;
            });


            /*
            * ลบ timeout object ออกจาก timerUID โดยอ้างอิงจาก uid
            */
            lt.clearTimeout(_this.timerUID[uid]);

        }, delay);
    },


    /**
     * สร้าง timeout object และเริ่มต้นนับถ่อยหลังที่จะส่งข้อความ
     * @param {String} uid รหัสที่ไม่ซ้ำกัน ได้มาจาก notification.uid
     * @param {String} id ไอดี ของ notification (_id)
     * @param {number} delay ช่วงเวลาที่จะนับถ่อยหลังเพื่อเริ่มส่งข้อความ 
     */
    startCountdown: function(uid, id, delay) {

        var _this = this;

        /*
        * เริ่ม loop สร้างการนับถอยหลังที่จะทำการส่งข้อความ
        */
        this.timerUID[uid] = lt.setTimeout(() => {


            /*
            * เริ่มทำการค้นหาผู้รับข้อความ
            */
            _this.findReceiver(id);


            /*
            * ลบ timeout object ออกจาก timerUID โดยอ้างอิงจาก uid
            */
            lt.clearTimeout(_this.timerUID[uid]);

        }, delay);
    },


    /**
     * ค้นหารายการผู้รับข้อความ
     * @param {String} id ไอดี ของ notification (_id)
     */
    findReceiver: function(id) {

        var _this = this;

        // TODO :   1.Query ข้อมูลของ notification นี้ออกมา
        //          2.ตรวจสอบว่า garage_selected มีค่าอยู่หรือเปล่า
        //          3.loop ค่า garage_selected ออกมาเพื่อนำไปหา driver ทั้งหมดที่อยู่ใน อู่ที่เลือก (ถ้า garage_selected ไม่ว่าง)
        NotificationModel.findOne({ _id: id }).exec((err, notification) => {

            if (err) {
                return console.log(err);
            }


            /**
             * ถ้าตอนสร้าง มี การเลือกอู่เอาไว้ ให้นำอู่มาค้นหารายการคนขับก่อน
             */
            if (notification.garage_selected.length > 0) {


                /** 
                 * นำรายการ id ของอู่มาค้นหาค่า cgroup
                 * */
                GarageModel.find({ _id: { '$in': notification.garage_selected } }).select('-_id cgroup').exec((err, garages) => {

                    if (err) {
                        return console.log(err);
                    }


                    var cgroup = [];
                    garages.forEach((garage, index) => {
                        cgroup.push(garage.cgroup);
                    });


                    /**
                     * นำรายการ cgroup ที่ได้มาหา driver ไอดี (_id)
                     */
                    DriverModel.find({ cgroup: { '$in': cgroup } }).lean().select('_id').exec((err, driverIds) => {

                        if (err) {
                            return console.log(err);
                        }


                        var ids = [];
                        driverIds.forEach((driver, index) => {
                            ids.push(driver._id.toString());
                        });


                        /**
                         * ทำให้ id ที่ได้จาก driver_selected กับ driverIds ไม่ซ้ำกัน
                         */
                        var IDs = _.uniq(notification.driver_selected.concat(ids));
                        _this.sendNotification(IDs, notification);
                    });
                });
            }


            /**
             * ถ้าตอนสร้าง ไม่มี การเลือกอู่เอาไว้ ให้นำรายการไอดีของคนขับมาใช้ค้นหา Socket id 
             * ที่อยู่ใน app.js::clients มาเพื่อที่จะ loop ยิง socket ไปยังคนขับนั้นๆ
             */
            else {
                _this.sendNotification(notification.driver_selected, notification);
            }
        });
    },


    /**
     * วน loop ค้นหา socket id แล้วส่งข้อความไปคนขับ
     * @param {Array} driverIds Array ของไอดี ของคนขับ
     * @param {object} notificationData notification object
     */
    sendNotification: function(driverIds, notificationData) {

        var _this = this;

        if (!driverIds instanceof Array || driverIds.length === 0) {
            return console.log('Driver ID zero list');
        }


        /** 
        * ถ้าพบ mSocket ไม่ null
        */
        if(!_this.mSocket) {
            return console.log('mSocket object is undefined');
        }


        /**
         * วน loop ค้นหา socket id แล้วส่งที่ละคน
         */
        driverIds.forEach((driverId, index) => {


            /** 
             * ให้ยิง socket ไปยังคนขับนั้นๆ
             */
            _this.mSocket.to(driverId).emit('new notification', notificationData);
        });


        _this.storeSendingResult(driverIds, notificationData._id);
    },


    /**
     * Store sending result
     */
    storeSendingResult: function(driverIds, notificationId) {

        var _this = this;

        DriverModel.update(

            // find conditions
            { _id: { '$in': driverIds } }, 

            // push into sub schema
            { $push: { 'notifications': { 'ref': notificationId, 'state': _this.STATE.UNREAD } } }, 

            // set multi: true for update multiple documents
            { multi: true }, 
            
            // callback
            (err, count) => {

                if (err) {
                    return console.log(err);
                }

                NotificationModel.findOneAndUpdate(

                    // find conditions
                    { _id: notificationId },

                    // set data
                    { $set: { 'drivers': driverIds, 'status': 'ACTIVE' } },

                    // set new: true for get updated document
                    { new: true },

                    // callback
                    (err, notification) => {

                        if (err) {
                            return console.log(err);
                        }

                        _this.mSocket.of('/admin').emit('NOTIFICATION_SENT', notification);
                        return;
                    });
            });
    }
}