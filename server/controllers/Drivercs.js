////////////////////////////////////
// TaxiBeam API for Driver
// version : 1.0.5
// UpDate October 5, 2016 
// Created by Hanzen@BRET
////////////////////////////////////
const _ = require('underscore');
var config = require('../../config/func').production;
var langdrvth = require('../../config/langth').langdrvth;
var crypto = require('crypto');
var compareVersions = require('compare-versions');

// start nodemailer and define param
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var SMTPSentEmail = config.SMTPSentEmail;
var SMTPSentPass = config.SMTPSentPass;
var InfoEmail = config.InfoEmail;
var AlertEmail = config.AlertEmail;
// end define param for nodemailer

var Url = require('url');
var mongoose = require('mongoose');

var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var moment = require('moment');

var PassengerModel = mongoose.model('PassengerModel');
var JoblistModel = mongoose.model('JoblistModel');
var ParkinglotModel = mongoose.model('ParkinglotModel');
var ParkingqueueModel = mongoose.model('ParkingqueueModel');
var CallcenterpsgModel = mongoose.model('CallcenterpsgModel');
var UserModel = mongoose.model('UserModel');
var DriversModel = mongoose.model('DriversModel');
var DrvLogModel = mongoose.model('DrvLogModel');
var CommentModel = mongoose.model('CommentModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var EmgListModel = mongoose.model('EmgListModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var HotelsModel = mongoose.model('HotelsModel');
var JoblisthotelModel = mongoose.model('JoblisthotelModel');
var Lk_garageModel = mongoose.model('Lk_garageModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var DrvkeeplogModel = mongoose.model('DrvkeeplogModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');
var Lk_zonelayerModel = mongoose.model('Lk_zonelayerModel');
var MsghistoryModel = mongoose.model('MsghistoryModel');
var HistoryModel = mongoose.model('HistoryModel');
var NotificationModel = mongoose.model('NotificationModel');
var BuyshifttransactionModel = mongoose.model('BuyshifttransactionModel');

// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var qs = require('querystring');
var ios = require('socket.io');

// var crypto;
// try {
//   crypto = require('crypto');  
// } catch (err) {
//   console.log('crypto support is disabled!');
// }

function _joblistupdate(job_id, drv_id, drv_device_id, cgroup, drv_name, drv_phone, drv_carplate, action, cwhere, curlng, curlat) {
	JoblistModel.findOne(
		{ job_id: job_id },
		function (err, job) {
			if (job == null) {
				//console.log (' no job id')
			} else {
				switch (action) {
					case "datedrvwait":
						job.drv_id = drv_id;
						job.drv_device_id = drv_device_id;
						job.drv_name = drv_name;
						job.drv_phone = drv_phone;
						job.drv_carplate = drv_carplate;
						job.cgroup = cgroup;
						job.status = "WAIT";
						job.datedrvwait = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "WAIT",
							created: new Date()
						});
						break;
					case "datedrvpick":
						job.status = "PICK";
						job.datedrvpick = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "PICK",
							created: new Date()
						});
						break;
					case "datedrvdrop":
						job.status = "DROP";
						job.datedrvdrop = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "DROP",
							created: new Date()
						});
						break;
					case "datedrvcancel":
						job.status = "DRV_CANCELLED";
						job.datedrvcancel = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "DRV_CANCELLED",
							created: new Date()
						});
						job.drvcancelwhere = cwhere;
						break;
				}
				job.save(function (err, response) {
					if (err) {
						console.log(err)
					} else {
						console.log(' update job list ')
					}
				});
			}
		}
	);
}


// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}


function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}


function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);
		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};


function _getErrDetail(errCode, callback) {
	ErrorcodeModel.findOne(
		{ errcode: errCode },
		{ errth: 1 },
		function (err, response) {
			if (response == null) {
				callback('');
			} else {
				callback(response.errth)
			}
		}
	);
};


function _driverAutoLogin(req, res) {
	//console.log(req.body)
	var device_id = req.body.device_id;
	var _id = req.body._id;
	DriversModel.findOne(
		{ device_id: device_id },
		function (err, drv) {
			if (drv == null) {
				// This phone(IMEI) have never been in the system, send to registration page.
				console.log(' _driverAutoLogin 111')
				res.json({
					status: false,
					target: "REGISTRATION"
				});
			} else {
				if (drv._id == _id) {
					if (drv.active == 'Y') {
						if (drv.status == 'BROKEN') {
							// Already in the system and SMS :Y but status BROKEN, send to broken page
							res.json({
								status: false,
								target: "BROKEN"
							});
						} else {
							drv.status = "ON";
							drv.updated = new Date().getTime();
							drv.save(function (err, response) {
								if (err) {
									console.log('_driverAutoLogin = ' + err)
									res.json({
										status: false,
										msg: "error"
									});
								} else {
									// device_id: Y, _id: Y, SMScofirm: Y [ Welcome to Taxi-Beam ]
									res.json({
										status: true,
										data: {
											_id: response._id,
											status: response.status
										}
									});
								}
							});
						}
					} else {
						// Already in the system but SMS: N, send to SMS page
						console.log('_driverAutoLogin 222')
						res.json({
							status: false,
							target: "SMSCONFIRM"
						});
					}
				} else {
					// in case of change mobile owner, send to update page to update driver info	
					console.log('_driverAutoLogin 333')
					res.json({
						status: false,
						target: "BLANKPROFILE"
					});
				}
			}
		}
	);
};


function _driverCheckLogin(device_id, _id, res) {
	DriversModel.findOne(
		{ device_id: device_id },
		function (err, drv) {
			if (drv == null) {
				// This phone(IMEI) have never been in the system, send to registration page.
				res.json({
					status: false,
					target: "REGISTRATION"
				});
			} else {
				if (drv._id == _id) {
					if (drv.active == 'Y') {
						if (drv.status == 'BROKEN') {
							// Already in the system and SMS :Y but status BROKEN, send to broken page
							res.json({
								status: false,
								target: "BROKEN"
							});
						} else {
							drv.status = "ON";
							drv.updated = new Date().getTime();
							drv.save(function (err, response) {
								if (err) {
									console.log('driverchecklogin = ' + err)
									res.json({
										status: false,
										msg: "error"
									});
								} else {
									// device_id: Y, _id: Y, SMScofirm: Y [ Welcome to Taxi-Beam ]
									res.json({
										status: true,
										data: {
											_id: response._id,
											status: response.status
										}
									});
								}
							});
						}
					} else {
						// Already in the system but SMS: N, send to SMS page			    			
						res.json({
							status: false,
							target: "SMSCONFIRM"
						});
					}
				} else {
					// in case of change mobile owner, send to update page to update driver info
					res.json({
						status: false,
						target: "BLANKPROFILE"
					});
				}
			}
		}
	);
};



function _sub_samsungaction(passenger_ssid, passenger_jobid, taxi_duid, passenger_status, action, method) {
	var reqset = '{"taxi_duid": "' + taxi_duid + '", "status": "' + passenger_status + '"}';
	var postData = JSON.parse(JSON.stringify(reqset));
	var options = {
		host: config.samsunghost,
		port: config.samsungport,
		path: config.samsungpath + '/' + passenger_ssid + '/requests' + passenger_jobid + '/' + action,
		method: method,
		headers: {
			'Content-Type': 'application/json; charset=utf-8',
			'Content-Length': Buffer.byteLength(postData)
		}
	};

	var request = http.request(options, function (response) {
		response.on('data', function (dog) {
		});
		response.on('end', function (cat) {
		});
	});
	request.on('error', function (err) {
		console.log('cannot do _sub_drvlogservice')
	});
	request.write(postData);
	request.end();
}




// NOTE ****************************** for simulate (bot) driver
exports.all = function (req, res) {

	var driver = "";
	if (req.body.cgroup) {
		driver = DriversModel.find({ cgroup: req.body.cgroup });
	} else {
		driver = DriversModel.find({});
	}

	if (parseInt(req.body.number) > 0) {
		driver.limit(parseInt(req.body.number));
	}
	driver.exec(function (err, drivers) {
		res.send(drivers);
	});
};


// NOTE *******************************  start service for DRIVER
exports.driverloadconfig = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var appversion = req.body.version;
	var platform = req.body.platform;	// [ ANDROID, iOS ]
	var mobiledata = req.body.model;	// driver's mobile data info

	DriversModel.update(
		{ _id: _id, device_id: device_id },
		{
			$set:
			{
				appversion: appversion,
				platform: platform,
				mobiledata: mobiledata
			}
		},
		{ upsert: false, new: false },
		function (err, tank) {
			if (err) return handleError(err);
			if (tank == null) {
				res.json({
					status: true,
					data: {
						drvSearchPsgInterval: config.drvSearchPsgInterval,
						drvGetstatusInterval: config.drvGetstatusInterval
					}
				});
			} else {
				res.json({
					status: true,
					data: {
						drv_shift: tank.drv_shift,
						shift_start: tank.shift_start,
						shift_end: tank.shift_end,
						drvSearchPsgInterval: config.drvSearchPsgInterval,
						drvGetstatusInterval: config.drvGetstatusInterval
					}
				});
			}
		}
	);
}




exports.GetCreditAndRoundInfo = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		function (err, tank) {
			if (err) return handleError(err);
			if (tank == null) {
				res.json({
					status: false,
					msg: "Driver info was not found."
				});
			} else {
				res.json({
					status: true,
					data: {
						drv_wallet: tank.drv_wallet,
						drv_shift: tank.drv_shift,
						MoneyPerUseAPP: config.MoneyPerUseAPP,
						shift_start: tank.shift_start? new Date(tank.shift_start).getTime(): null,
						shift_end: tank.shift_end? new Date(tank.shift_end).getTime(): null,						
						drvSearchPsgInterval: config.drvSearchPsgInterval,
						drvGetstatusInterval: config.drvGetstatusInterval
					}
				});
			}
		}
	);
}




exports.checkIconURL = function (req, res) {
	Lk_garageModel.find(
		{},
		{ _id: 0, cgroup: 1, fullname: 1, iconurl: 1, iconurl_active: 1, version: 1, phone: 1, cgroupname: 1, cprovincename: 1 },
		function (err, result) {
			if (result == 0) {
				res.json({
					status: false,
					msg: "No iconurl"
				})
			} else {
				res.json({
					status: true,
					msg: "This is iconurl",
					data: result
				})
			}
		}
	)
};




exports.getMsgHistory = function (req, res) {
	MsghistoryModel.find(
		{ drv_phone: req.body.phone },
		{ phone: 1, cur: 1, des: 1, created: 1 },
		{ sort: { 'created': -1 } }).limit(5).exec(function (err, result) {
			if (result == 0) {
				res.json({
					status: false
				});
			} else {
				res.json({
					status: true,
					data: result
				});
			}
		}
		)
}




exports.ChangePWDonly = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var username = req.body.username;
	var otp_number = req.body.otp_number;
	var passwordreal = req.body.password;
	var newpassword = crypto.createHmac('sha256', config.pwd_key).update(passwordreal).digest('base64');
	var myquery ;

	username = username.toLowerCase();

	if(req.url.substring(1,7)=='smiles'){
		myquery = { username: username, _id_garage: { $exists: true }, _id_garage: '597b0e04dc04ea092db44a7c' };
	} else {
		myquery = { username: username, _id_garage: { $exists: true }, _id_garage: { $ne:'597b0e04dc04ea092db44a7c'}  };
	}

	DriversModel.findOne(
		myquery,
		{ _id: 1, smsconfirm: 1, device_id: 1, fname: 1, lname: 1, phone: 1, citizenid: 1, taxiID: 1, address: 1, province: 1, district: 1, tambon: 1, zipcode: 1, english: 1, carplate: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, imgface: 1, imglicence: 1, imgcar: 1, allowpsgcontact: 1, carreturn: 1, carreturnwhere: 1, status: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, carprovince: 1, carprovinceid: 1 },
		function (err, drv) {
			if (drv == null) {
				_getErrDetail('err014', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			} else {
				if (drv.smsconfirm == otp_number) {
					drv.password = newpassword;
					drv.pwdhint = req.body.password;
					drv.updated = new Date().getTime();
					drv.save(function (err, response) {
						if (err) {
							_getErrDetail('err099', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail,
								});
							});
						} else {
							var dataDriver = DriversModel.findOne(
								{ _id: drv._id, _id_garage: { $exists: true } },
								{
									_id: 1, device_id: 1, username: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1,
									fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, address: 1,
									province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
									imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
								}
							);
							dataDriver.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });
							dataDriver.exec(function (err, resultdata) {
								if (resultdata == null) {
									res.json({ status: false, msg: "error" });
								} else {
									var myToken = jwt.sign({
										_id: req.body._id
									}, 'myScottSummer', {
											algorithm: 'HS256',
											expiresIn: 604800		// in second = 7 days
										});
									res.json({
										status: true,
										myToken,
										data: resultdata
									});
								}
							}
							);
						}
					});
				} else {
					// "msg": "เลข OPT ไม่ถูกต้อง"
					_getErrDetail('err013', function (errorDetail) {
						res.json({
							status: false,
							msg: errorDetail,
						});
					});
				}
			}
		}
	);
};




exports.driverAutoLogin = function (req, res) {
	_driverAutoLogin(req, res);
};



exports.driverLogin = function (req, res) {
	var username = req.body.username;
	var device_id = req.body.device_id;
	var passwordreal = req.body.password;
	var appversion = req.body.appversion;
	var password = crypto.createHmac('sha256', config.pwd_key).update(passwordreal).digest('base64');
	var myquery ;

	if(req.url.substring(1,7)=='smiles'){
		myquery = { username: username, _id_garage: { $exists: true }, _id_garage: '597b0e04dc04ea092db44a7c' };
	} else {
		myquery = { username: username, _id_garage: { $exists: true }, _id_garage: { $ne: '597b0e04dc04ea092db44a7c' } };
	}	

	if (!req.body.username) {
		res.status(400).send('username required');
		return;
	} else {
		username = username.toLowerCase();
	}

	if (!req.body.password) {
		res.status(400).send('password required');
		return;
	}

	var dataDriver = DriversModel.findOne(
		myquery,
		{
			_id: 1, device_id: 1, username: 1, password: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1,
			fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, carplate_formal: 1, address: 1,
			province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
			imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
		}
	);

	dataDriver.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });

	dataDriver.exec(function (err, drv) {
		if (drv == null) {
			// This phone(IMEI) have never been in the system, send to registration page.
			res.json({
				status: false,
				msg: "ไม่พบชื่อผู้ใช้งานในระบบ\n\nโปรดลงทะเบียนใหม่"
			});
		} else {
			if (drv.status == "DELETED") {
				res.json({ status: false, msg: "ขออภัยค่ะ ระบบไม่สามารถให้บริการได้ เนื่องจากบัญชีของคุณอยู่ในสถานะถูกลบออกจากระบบ หากท่านต้องการใช้งาน กรุณาติดต่อ (02)643-0807-9 จันทร์-ศุกร์ เวลา 9.00-18.00 น." });
			} else {
				if (drv.password == password) {
					if (drv.device_id == device_id) {
						// same user account and same device //
						if (drv.status == "PENDING") {
							res.json({
								status: false,
								msg: 'บัญชีของท่านยังไม่ได้รับการตรวจสอบ กรุณารออีกซักระยะ'
							});
						} else {
							if (drv.status == "OFF" || drv.status == "APPROVED") {
								drv.status = "ON";
							}
							drv.appversion = appversion;
							drv.updated = new Date().getTime();
							drv.save(function (err, response) {
								if (err) {
									res.json({
										status: false,
										errcode: "err002",
										msg: err
									});
								} else {
									DriversModel.find(
										{ cgroup: drv.cgroup, prefixcarplate: drv.prefixcarplate, carplate: drv.carplate, _id: { $ne: drv._id }, status: { $nin: ["OFF", "BROKEN", "APPROVED", "DELETED", "PENDING"] } },
										{ fname: 1, lname: 1, carplate: 1, phone: 1, status: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, appversion: 1 },
										function (err, result) {
											if (result == 0) {
												var dataDriver = DriversModel.findOne(
													{ _id: drv._id, _id_garage: { $exists: true } },
													{
														_id: 1, device_id: 1, username: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1,
														fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, carplate_formal: 1, address: 1,
														province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
														imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
													}
												);
												dataDriver.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });
												dataDriver.exec(function (err, resultdata) {
													if (resultdata == null) {
														res.json({
															status: false,
															errcode: "err00221",
															msg: err
														});
													} else {
														var myToken = jwt.sign({
															_id: req.body._id
														}, 'myScottSummer', {
																algorithm: 'HS256',
																expiresIn: 604800		// in second = 7 days
															});
														res.json({
															status: true,
															myToken,
															msg: "You can use this application : step 01",
															data: resultdata
														});
													}
												});
											} else {
												drv.status = "OFF";
												drv.updated = new Date().getTime();
												drv.save(function (err, response) {
													if (err) {
														res.json({
															status: false,
															msg: err
														});
													} else {
														console.log('checksameaccount : err003')
														res.json({
															status: false,
															errcode: "err003",
															msg: "ขออภัยค่ะ ระบบไม่สามารถให้บริการได้ เนื่องจากมีผู้ใช้รถเลขทะเบียนเดียวกันกับคุณ กำลังเปิดใช้งานอยู่ หากประส่งค์จะใช้บริการ กรุณาติดต่อผู้ใช้ตามรายละเอียดนี้เพื่อออกจากระบบ"
														});
													}
												});
											}
										}
									);
								}
							});
						}
					} else {
						if (drv.status == "PENDING") {
							res.json({
								status: false,
								msg: 'บัญชีของท่านยังไม่ได้รับการตรวจสอบ'
							});
						} else {
							// same user account but different device
							if (drv.status == "OFF" || drv.status == "APPROVED") {
								drv.appversion = appversion;
								drv.device_id = device_id;
								drv.status = "ON";
								drv.updated = new Date().getTime();
								drv.save(function (err, response) {
									if (err) {
										//console.log('checksameaccount : err004')
										res.json({
											status: false,
											errcode: "err004",
											msg: err
										});
									} else {
										var dataDriver1 = DriversModel.findOne(
											{ _id: drv._id, device_id: device_id, _id_garage: { $exists: true } },
											{
												_id: 1, device_id: 1, username: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1,
												fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, carplate_formal: 1, address: 1,
												province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
												imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
											}
										);
										dataDriver1.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });
										dataDriver1.exec(function (err, resultdata) {
											if (resultdata == 0) {
												//console.log('checksameaccount : err005')
												res.json({
													status: false,
													errcode: "err005",
													msg: err
												});
											} else {
												var myToken = jwt.sign({
													_id: req.body._id
												}, 'myScottSummer', {
														algorithm: 'HS256',
														expiresIn: 604800		// in second = 7 days
													});
												res.json({
													status: true,
													myToken,
													msg: "You can use the application : step 02",
													data: resultdata
												});
											}
										});
									}
								});
							} else {
								//console.log('checksameaccount : err006')
								res.json({
									status: false,
									errcode: "err006",
									msg: "ขออภัยค่ะระบบไม่สามารถให้บริการได้  เนื่องจากมีการใช้งานด้วยชื่อผู้ใช้เดียวกันนี้ที่อุปกรณ์อื่น หากมีความประสงค์จะใช้งาน กรุณาปิดการใช้งานที่อุปกรณ์ก่อนหน้านี้"
								});
							}
						}
					}
				} else {
					// in case of change mobile owner, send to update page to update driver info
					res.json({
						status: false,
						msg: "รหัสผ่านไม่ถูกต้อง!"
					});
				}
			}
		}
	});
};




exports.driverRegister = function (socket) {
	return function (req, res) {
		var device_id = req.body.device_id;
		var username = req.body.username;
		var citizenid = req.body.citizenid;
		var passwordreal = req.body.password;
		var password = crypto.createHmac('sha256', config.pwd_key).update(passwordreal).digest('base64');
		var prefixcarplate;
		var cgroup;
		var cgroupname;
		var cprovincename;
		var _id_garage = req.body._id_garage;
		
		if(req.url.substring(1,7)=='smiles'){
			appversion = config.smilesappversion;
		} else {
			appversion = config.mobileappversion;
		}

		if (typeof device_id == 'undefined' && device_id == null) {
			res.status(400).send('device_id required');
			return;
		} 
		if (typeof prefixcarplate == 'undefined' && prefixcarplate == null) {
			prefixcarplate = "";
		} else {
			prefixcarplate = req.body.prefixcarplate + ' ';
		}
		//var password = crypto.createHash('md5').update(passwordreal).digest("hex","latin1","base64");
		username = username.toLowerCase();
		if (req.body._id_garage) {
			Lk_garageModel.findOne(
				{ _id: req.body._id_garage },
				{ _id: 0, cgroup: 1, fullname: 1, iconurl: 1, iconurl_active: 1, version: 1, phone: 1, cgroupname: 1, cprovincename: 1 },
				function (err, result) {
					if (result == null) {
						cgroup = "";
						cgroupname = "";
						cprovincename = "";
						_CreateDriver(cgroup, cgroupname, cprovincename);
					} else {
						cgroup = result.cgroup;
						cgroupname = result.cgroupname;
						cprovincename = result.cprovincename;
						_CreateDriver(cgroup, cgroupname, cprovincename);
					}
				}
			)
		} else {
			cgroup = "";
			cgroupname = "";
			cprovincename = "";
			_CreateDriver(cgroup, cgroupname, cprovincename);
		}

		function _CreateDriver(cgroup, cgroupname, cprovincename) {
			var myquery ;
			if(req.url.substring(1,7)=='smiles'){
				// chekck ว่าเป็น อู่ smileapp หรือปล่าว
				myquery = {
					citizenid: citizenid,
					_id_garage: '597b0e04dc04ea092db44a7c'
					};
			} else {
				myquery = {
					citizenid: citizenid,
					_id_garage: { $ne: '597b0e04dc04ea092db44a7c' }					
				};
			}
			DriversModel.findOne(
				myquery ,
				function (err, drv1) {
					if (drv1 == null) {
						console.log(' citizenid unique => check username ')
						// valid citizenid, so chekc user name
						DriversModel.findOne(
							{ username: username },
							function (err, drv2) {
								if (drv2 == null) {
									console.log(' citizenid & username unique => create driver ')
									// valid username, go ahead do not check device_id
									DriversModel.create({
										device_id: device_id,
										username: username,
										password: password,
										pwdhint: req.body.password,
										fname: req.body.fname,
										lname: req.body.lname,
										phone: req.body.phone,
										allowpsgcontact: req.body.allowpsgcontact,
										citizenid: req.body.citizenid,
										taxiID: req.body.taxiID,
										address: req.body.address,
										province: req.body.province,
										district: req.body.district,
										tambon: req.body.tambon,
										zipcode: req.body.zipcode,
										english: req.body.english,
										prefixcarplate: req.body.prefixcarplate,
										carplate: req.body.carplate,
										prefixcarplate: req.body.prefixcarplate,
										carplate_formal: prefixcarplate + req.body.carplate,
										carprovince: req.body.carprovince,
										carprovinceid: req.body.carprovinceid,
										cartype: req.body.cartype,
										carcolor: req.body.carcolor,
										outbound: req.body.outbound,
										carryon: req.body.carryon,
										active: "Y",
										status: "PENDING",
										msgphone: "",
										msgnote: "",
										msgstatus: "",
										psg_curaddr: "",
										psg_destination: "",
										psg_detail: "",
										jobtype: "",
										appversion: appversion,
										created: new Date().getTime(),
										updated: new Date().getTime(),
										expdategarage: new Date().getTime(),
										expdateapp: new Date().getTime() + (7776000000 * 4),	//  7776000000 = 3 months		
										smsconfirm: ranSMS(),
										_id_garage: req.body._id_garage,
										cgroup: cgroup,
										cgroupname: cgroupname,
										cprovincename: cprovincename,
										updateprofilehistory: {
											register_data: req.body,
											created: new Date()
										}
									},
										function (err, response) {
											if (err) {
												res.json({ status: false, msg: err, data: err });
											} else {

												var transporter = nodemailer.createTransport(smtpTransport({
													service: 'gmail',
													auth: {
														user: SMTPSentEmail, // my mail
														pass: SMTPSentPass
													}
												}));
												// setup e-mail data with unicode symbols
												var mailOptions = {
													from: '"Taxi-Beam auto sent mail" <' + InfoEmail + '>', // sender address
													to: InfoEmail, // list of receivers
													subject: 'มีแท็กซี่สมัครใหม่ กรุณาตรวจสอบ', // Subject line
													text: 'plaintext body', // plaintext body
													html: '<b>ชื่อ</b> ' + req.body.fname + ' ' + req.body.lname + '<br><b>โทรศัพท์</b> ' + req.body.phone + '<br><b>อู่</b> ' + cgroupname + '<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
												};

												// send mail with defined transport object
												transporter.sendMail(mailOptions, function (error, info) {
													if (error) {
														return console.log(error);
													}
												});
												var myToken = jwt.sign({
													_id: response._id
												}, 'myScottSummer', {
														algorithm: 'HS256',
														expiresIn: 604800		// in second = 7 days
													});
												res.json({
													status: true,
													myToken,
													msg: "new",
													data: {
														_id: response._id,
														status: response.status,
														allowpsgcontact: response.allowpsgcontact
													}
												});
												socket.of('/' + response.cgroup).emit('request approve', response);
											}
										}
									);
								} else {
									//console.log(' citizenid unique but username  dup => check imei ')
									if(req.url.substring(1,7)=='smiles'){
										res.json({
											status: false,
											msg: "ไม่สามารถลงทะเบียนได้ เนื่องจากมีคนใช้ username นี้แล้ว หากท่านต้องการเข้าใช้งานระบบ กรุณาติดต่อ 098-865-4965"
										});
									} else {
										res.json({
											status: false,
											msg: "ไม่สามารถลงทะเบียนได้ เนื่องจากมีคนใช้ username นี้แล้ว หากท่านต้องการเข้าใช้งานระบบ กรุณาติดต่อ (02)643-0807-9 จันทร์-ศุกร์ เวลา 9.00-18.00 น."
										});
									}
								}
							}
						);
					} else {
						//console.log(' ***  citizenid  dup *** ')
						if(req.url.substring(1,7)=='smiles'){							
							res.json({
								status: false,
								msg: "ไม่สามารถลงทะเบียนได้ เนื่องจากเลขบัตรประชาชนของท่านมีอยู่ในระบบแล้ว หากท่านต้องการเข้าใช้งานระบบ กรุณาติดต่อ 098-865-4965"
							});							
						} else {
							res.json({
								status: false,
								msg: "ไม่สามารถลงทะเบียนได้ เนื่องจากเลขบัตรประชาชนของท่านมีอยู่ในระบบแล้ว หากท่านต้องการเข้าใช้งานระบบ กรุณาติดต่อ 026430807-9 ทุกวันจันทร์-ศุกร์ เวลา 9.00-18.00 น."
							});							
						}						
					}
				}
			);
		}
	}
};




exports.driveruploadFace = function (req, res) {
	res.connection.setTimeout(0);
	var form = new formidable.IncomingForm();
	var imgtype = "imgface";
	// processing 
	console.log('driveruploadFace')
	form.parse(req, function (err, fields, files) {
		if (typeof files.upload == 'undefined' && files.upload == null) {
			console.log("fail upload ! ; no image input driveruploadFace")
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else {
				_getErrDetail('err004', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			}
		} else {
			// check image type			
			if (files.upload.type == 'image/jpeg' || files.upload.type == 'image/jpg' || files.upload.type == 'image/png' || files.upload.type == 'application/octet-stream') {
				_id = fields._id;
				device_id = fields.device_id;
				findandUpload(_id, device_id);
			} else {
				// kick out
				//res.json({ status: false , msg: "Invalid image type, Please input type JPEG or PNG" });
				_getErrDetail('err005', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		}
	});
	function findandUpload(drvId, deviceId) {
		DriversModel.findOne(
			{ _id: drvId, device_id: deviceId },
			{ device_id: 1 },
			function (err, response) {
				if (response == null) {
					_driverCheckLogin(deviceId, drvId, res);
				} else {
					if (form) {
						var files = form.openedFiles;
						if (files) {
							var file = files[0];
							if (file) {
								var temp_path = file.path;
								var extension = path.extname(file.name);
								var new_location = '../upload_drivers/';
								newimgupload = drvId + '_' + imgtype + extension;
								fs.copy(temp_path, new_location + newimgupload, function (err) {
									if (err) {
										console.log(err);
									} else {
										console.log("success upload !")
										DriversModel.findOne(
											{ _id: drvId },
											{ imgface: 1, status: 1, updated: 1 },
											function (err, drvupfile) {
												drvupfile.imgface = newimgupload;
												drvupfile.updated = new Date().getTime();
												drvupfile.save(function (err, response) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														//success, your image has been uploaded.
														res.json({
															status: true,
															data: { imgname: newimgupload }
														});
													}
												});
											}
										);
									}
								});
							}
						}
					} else {
						console.log("do not have form")
						res.json({ status: false, msg: "no form" });
					}
				}
			}
		);
	}
}




exports.driveruploadLicence = function (req, res) {
	res.connection.setTimeout(0);
	var form = new formidable.IncomingForm();
	var imgtype = "imglicence";
	// processing
	console.log('driveruploadLicence')
	form.parse(req, function (err, fields, files) {
		if (typeof files.upload == 'undefined' && files.upload == null) {
			console.log("fail upload ! ; no image input driveruploadLicence")
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else {
				_getErrDetail('err004', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		} else {
			// check image type
			if (files.upload.type == 'image/jpeg' || files.upload.type == 'image/jpg' || files.upload.type == 'image/png' || files.upload.type == 'application/octet-stream') {
				_id = fields._id;
				device_id = fields.device_id;
				findandUpload(_id, device_id);
			} else {
				// kick out
				//res.json({ status: false , msg: "Invalid image type, Please input type JPEG or PNG" });	
				_getErrDetail('err005', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			}
		}
	});
	function findandUpload(drvId, deviceId) {
		DriversModel.findOne(
			{ _id: drvId, device_id: deviceId },
			{ device_id: 1 },
			function (err, response) {
				if (response == null) {
					_driverCheckLogin(deviceId, drvId, res);
				} else {
					if (form) {
						var files = form.openedFiles;
						if (files) {
							var file = files[0];
							if (file) {
								var temp_path = file.path;
								var extension = path.extname(file.name);
								var new_location = '../upload_drivers/';
								newimgupload = drvId + '_' + imgtype + extension;
								fs.copy(temp_path, new_location + newimgupload, function (err) {
									if (err) {
										console.log(err);
									} else {
										console.log("success upload !")
										DriversModel.findOne(
											{ _id: drvId },
											{ imglicence: 1, status: 1, updated: 1 },
											function (err, drvupfile) {
												if (drvupfile.status == "") {
													drvupfile.status = "PENDING";
												}
												drvupfile.imglicence = newimgupload;
												drvupfile.updated = new Date().getTime();
												drvupfile.save(function (err, response) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														res.json({
															status: true,
															data: { imgname: newimgupload }
														});
													}
												});
											}
										);
									}
								});
							}
						}
					} else {
						console.log("do not have form")
						res.json({ status: false, msg: "no form" });
					}
				}
			}
		);
	}
}




exports.driveruploadCar = function (req, res) {
	res.connection.setTimeout(0);
	var form = new formidable.IncomingForm();
	var imgtype = "imgcar";
	// processing
	console.log('driveruploadCar')
	form.parse(req, function (err, fields, files) {
		if (typeof files.upload == 'undefined' && files.upload == null) {
			console.log("fail upload ! ; no image input driveruploadCar")
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else {
				_getErrDetail('err004', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			}
		} else {
			// check image type
			if (files.upload.type == 'image/jpeg' || files.upload.type == 'image/jpg' || files.upload.type == 'image/png' || files.upload.type == 'application/octet-stream') {
				_id = fields._id;
				device_id = fields.device_id;
				findandUpload(_id, device_id);
			} else {
				// kick out
				//res.json({ status: false , msg: "Invalid image type, Please input type JPEG or PNG" });	
				_getErrDetail('err005', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		}
	});
	function findandUpload(drvId, deviceId) {
		DriversModel.findOne(
			{ _id: drvId, device_id: deviceId },
			{ device_id: 1 },
			function (err, response) {
				if (response == null) {
					_driverCheckLogin(deviceId, drvId, res);
				} else {
					if (form) {
						var files = form.openedFiles;
						if (files) {
							var file = files[0];
							if (file) {
								var temp_path = file.path;
								var extension = path.extname(file.name);
								var new_location = '../upload_drivers/';
								newimgupload = drvId + '_' + imgtype + extension;
								fs.copy(temp_path, new_location + newimgupload, function (err) {
									if (err) {
										console.log(err);
									} else {
										console.log("success upload !")
										DriversModel.findOne(
											{ _id: drvId },
											{ imgcar: 1, status: 1, updated: 1 },
											function (err, drvupfile) {
												drvupfile.imgcar = newimgupload;
												drvupfile.updated = new Date().getTime();
												drvupfile.save(function (err, response) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														res.json({
															status: true,
															data: { imgname: newimgupload }
														});
													}
												});
											}
										);
									}
								});
							}
						}
					} else {
						//console.log("do not have a form")
						res.json({ status: false, msg: "no form" });
					}
				}
			}
		);
	}
}




exports.sendSMSUP = function (req, res) {
	/*
	http.get("http://www.google.com/index.html", function(res) {
	  console.log("Got response: " + res.statusCode);
	}).on('error', function(e) {
	  console.log("Got error: " + e.message);
	});
	*/
	var gensms = ranSMS();
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var citizenid = req.body.citizenid;
	var myquery;
	var smssender;

	if(req.url.substring(1,7)=='smiles'){
		myquery = { citizenid: citizenid, _id_garage: { $exists: true }, _id_garage: '597b0e04dc04ea092db44a7c' };
		smssender = "SmilesApp";
	} else {
		myquery = { citizenid: citizenid, _id_garage: { $exists: true }, _id_garage: { $ne: '597b0e04dc04ea092db44a7c' } };
		smssender = "TaxiBeam";
	}
	
	DriversModel.findOne(
		myquery,
		{ _id: 1, status: 1, phone: 1, smsconfirm: 1, username: 1, password: 1 },
		function (err, response) {
			if (response == null) {
				//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
				_getErrDetail('err012', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				response.smsconfirm = gensms;
				response.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						////////////////////////////////////////////////////////
						var smsconfirm = gensms;
						var smsphone = response.phone;
						var https = require('https');
						var postData = JSON.stringify({
							"sender": smssender,
							"to": smsphone,
							"msg": " ชื่อผู้ใช้งาน ของคุณคือ  " + response.username + "  เลข OTP ของคุณคือ " + gensms
						});
						var options = {
							hostname: config.smshostname,
							port: config.smshostport,
							path: config.smshostpath,
							method: config.smshostmethod,
							headers: {
								'Content-Type': 'application/json; charset=utf-8',
								'Content-Length': Buffer.byteLength(postData)
							}
						};
						var request = https.request(options, function (response) {

							var body = '';
							response.on('data', function (d) {
								body += d;
							});
							response.on('end', function () {
								if (body) {
									var parsed = JSON.parse(body);
									if (parsed.SendMessageResult.ErrorCode == 1) {
										//msg: "success, sms has been sent.",
										res.json({
											status: true,
											msg: "SMS sent Successful"
										});
									} else {
										var transporter = nodemailer.createTransport(smtpTransport({
											service: 'gmail',
											auth: {
												user: SMTPSentEmail, // my mail
												pass: SMTPSentPass
											}
										}));
										// setup e-mail data with unicode symbols
										var mailOptions = {
											from: '"Taxi-Beam auto sent mail" <' + AlertEmail + '>', // sender address
											to: AlertEmail, // list of receivers
											subject: 'There is some errors for SMS system, please check.', // Subject line
											text: 'plaintext body', // plaintext body
											html: 'There is some errors for SMS system, please check.<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
										};
										// send mail with defined transport object
										transporter.sendMail(mailOptions, function (error, info) {
											if (error) {
												return console.log(error);
											}
										});
										//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
										_getErrDetail('err015', function (errorDetail) {
											res.json({
												status: false,
												msg: errorDetail
											});
										});
									}
								} else {
									console.log('sms no body ')
									//msg: "ระบบไม่สามารถให้บริการได้ กรุณาติดต่อ Taxi-Beam"
									_getErrDetail('err015', function (errorDetail) {
										res.json({
											status: false,
											msg: errorDetail
										});
									});
								}
							});
						});
						request.on('error', function (err) {
							res.json({ status: false, msg: 'ขออภัยค่ะ ไม่สามารถทำงานต่อได้\nเรากำลังดำเนินการแก้ไขให้เร็วที่สุด' });
						});
						request.write(postData);
						request.end();
						////////////////////////////////////////////////////////
					}
				});
			}
		}
	);
};
/*
1=Successful
21=SMS Sending Failed
22=Invalid username/password combination
23=Credit exhausted
24=Gateway unavailable
25=Invalid schedule date format
26=Unable to schedule
27=Username is empty
28=Password is empty
29=Recipient is empty
30=Message is empty
31=Sender is empty
32=One or more required fields are empty
*/



exports.sendSMSConfirmation = function (req, res) {
	/*
	http.get("http://www.google.com/index.html", function(res) {
	  console.log("Got response: " + res.statusCode);
	}).on('error', function(e) {
	  console.log("Got error: " + e.message);
	});
	*/
	var gensms = ranSMS();
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var smssender;
	
	if(req.url.substring(1,7)=='smiles'){
		myquery = { citizenid: citizenid, _id_garage: { $exists: true }, _id_garage: '597b0e04dc04ea092db44a7c' };
		smssender = "TaxiBeam";
	} else {
		myquery = { citizenid: citizenid, _id_garage: { $exists: true }, _id_garage: { $ne: '597b0e04dc04ea092db44a7c' } };
		smssender = "TaxiBeam";
	}

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ _id: 1, status: 1, phone: 1, smsconfirm: 1 },
		function (err, response) {
			if (response == null) {
				//status: false , msg: "The phone number does not exist. Please register and try again."
				/*
				res.json({ 
					"SendMessageResult": {
					"ErrorCode": 0,
					"ReferenceCode": "",
					"Success": false
					}
					
				});
				*/
				//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
				_getErrDetail('err006', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				response.smsconfirm = gensms;
				response.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						////////////////////////////////////////////////////////
						var smsconfirm = gensms;
						var smsphone = response.phone;
						var https = require('https');
						var postData = JSON.stringify({
							"sender": smssender,
							"to": smsphone,
							//"msg"	: "Taxi-Beam OTP code is "+smsconfirm+", Please use it for verification."
							"msg": "กรุณาใส่เลขรหัส 4 หลักนี้  " + smsconfirm + " ในช่องที่กำหนด เพื่อยืนยันการเข้าใช้บริการค่ะ  "
						});
						var options = {
							hostname: config.smshostname,
							port: config.smshostport,
							path: config.smshostpath,
							method: config.smshostmethod,
							headers: {
								'Content-Type': 'application/json; charset=utf-8'
								//'Content-Length': postData.length
							}
						};
						var request = https.request(options, function (response) {
							var body = '';
							response.on('data', function (d) {
								body += d;
							});
							response.on('end', function () {
								if (body) {
									var parsed = JSON.parse(body);
									if (parsed.SendMessageResult.ErrorCode == 1) {
										//msg: "success, sms has been sent.",
										res.json({
											status: true,
											msg: "SMS sent Successful"
										});
									} else {
										var transporter = nodemailer.createTransport(smtpTransport({
											service: 'gmail',
											auth: {
												user: SMTPSentEmail, // my mail
												pass: SMTPSentPass
											}
										}));
										// setup e-mail data with unicode symbols
										var mailOptions = {
											from: '"Taxi-Beam auto sent mail" <' + AlertEmail + '>', // sender address
											to: AlertEmail, // list of receivers
											subject: 'There is some errors for SMS system, please check.', // Subject line
											text: 'plaintext body', // plaintext body
											html: 'There is some errors for SMS system, please check.<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
										};
										// send mail with defined transport object
										transporter.sendMail(mailOptions, function (error, info) {
											if (error) {
												return console.log(error);
											}
										});
										//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
										_getErrDetail('err015', function (errorDetail) {
											res.json({
												status: false,
												msg: errorDetail
											});
										});
									}
								} else {
									console.log('sms no body ')
									//msg: "ระบบไม่สามารถให้บริการได้ กรุณาติดต่อ Taxi-Beam"
									_getErrDetail('err015', function (errorDetail) {
										res.json({
											status: false,
											msg: errorDetail
										});
									});
								}
							});
						});
						request.on('error', function (err) {
							res.json({ status: false, msg: 'ขออภัยค่ะ ไม่สามารถทำงานต่อได้\nเรากำลังดำเนินการแก้ไขให้เร็วที่สุด' });
						});
						request.write(postData);
						request.end();
						////////////////////////////////////////////////////////
					}
				});
			}
		}
	);
};
/*
1=Successful
21=SMS Sending Failed
22=Invalid username/password combination
23=Credit exhausted
24=Gateway unavailable
25=Invalid schedule date format
26=Unable to schedule
27=Username is empty
28=Password is empty
29=Recipient is empty
30=Message is empty
31=Sender is empty
32=One or more required fields are empty
*/




exports.driverUpdateProfile = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var prefixcarplate = req.body.prefixcarplate;
	if (typeof prefixcarplate == 'undefined' && prefixcarplate == null) {
		prefixcarplate = "";
	} else {
		prefixcarplate = req.body.prefixcarplate + ' ';
	}

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ device_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, citizenid: 1, taxiID: 1, address: 1, province: 1, district: 1, tambon: 1, zipcode: 1, phone: 1, english: 1, carplate: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, status: 1, updateprofilehistory: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
			} else {
				/* update drv by _id */
				drv.updateprofilehistory.push({
					update_data: req.body,
					created: new Date()
				});
				if (req.body.fname) { drv.fname = req.body.fname ? req.body.fname : drv.fname; }
				if (req.body.lname) { drv.lname = req.body.lname ? req.body.lname : drv.lname; }
				//if(req.body.citizenid) 	{ drv.citizenid  = req.body.citizenid ? req.body.citizenid : drv.citizenid; }				
				if (req.body.phone) { drv.phone = req.body.phone ? req.body.phone : drv.phone; }
				if (req.body.taxiID) { drv.taxiID = req.body.taxiID ? req.body.taxiID : drv.taxiID; }
				if (req.body.prefixcarplate) { drv.prefixcarplate = req.body.prefixcarplate ? req.body.prefixcarplate : drv.prefixcarplate; }
				if (req.body.carplate) { drv.carplate = req.body.carplate ? req.body.carplate : drv.carplate; }
				if (req.body.address) { drv.address = req.body.address ? req.body.address : drv.address; }
				if (req.body.province) { drv.province = req.body.province ? req.body.province : drv.province; }
				if (req.body.district) { drv.district = req.body.district ? req.body.district : drv.district; }
				if (req.body.tambon) { drv.tambon = req.body.tambon ? req.body.tambon : drv.tambon; }
				if (req.body.zipcode) { drv.zipcode = req.body.zipcode ? req.body.zipcode : drv.zipcode; }
				if (req.body.english) { drv.english = req.body.english ? req.body.english : drv.english; }
				if (req.body.cartype) { drv.cartype = req.body.cartype ? req.body.cartype : drv.cartype; }
				if (req.body.carcolor) { drv.carcolor = req.body.carcolor ? req.body.carcolor : drv.carcolor; }
				if (req.body.allowpsgcontact) { drv.allowpsgcontact = req.body.allowpsgcontact ? req.body.allowpsgcontact : drv.allowpsgcontact; }
				if (req.body.outbound) { drv.outbound = req.body.outbound ? req.body.outbound : drv.outbound; }
				if (req.body.carryon) { drv.carryon = req.body.carryon ? req.body.carryon : drv.carryon; }
				if (req.body.carplate) { drv.carplate_formal = prefixcarplate + req.body.carplate; }
				drv.updated = new Date().getTime();
				drv.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						// res.json({ 
						// 	//msg: "success, driver profile updated",
						// 	status: true ,
						// 	data: result
						// });
						var dataDriver = DriversModel.findOne(
							{ _id: drv._id, _id_garage: { $exists: true } },
							{
								_id: 1, device_id: 1, username: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1, carplate_formal: 1,
								fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, address: 1,
								province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
								imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
							}
						);
						dataDriver.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });
						dataDriver.exec(function (err, resultdata) {
							if (resultdata == null) {
								res.json({ status: false, msg: "error" });
							} else {
								var myToken = jwt.sign({
									_id: req.body._id
								}, 'myScottSummer', {
										algorithm: 'HS256',
										expiresIn: 604800		// in second = 7 days
									});
								res.json({
									status: true,
									myToken,
									data: resultdata
								});
							}
						});
					}
				});
			}
		}
	);
};




exports.driverReturnCar = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var nowdate = new Date().getTime();
	var carreturn = req.body.carreturn;
	var carreturnwhere = req.body.carreturnwhere;

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ _id: 1, device_id: 1, carreturn: 1, carreturnwhere: 1, status: 1 },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
			} else {
				if (req.body.carreturn) { drv.carreturn = req.body.carreturn ? req.body.carreturn : drv.carreturn; }
				if (req.body.carreturnwhere) { drv.carreturnwhere = req.body.carreturnwhere ? req.body.carreturnwhere : drv.carreturnwhere; }
				drv.updated = new Date().getTime();
				drv.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						res.json({
							status: true,
							data: drv
						});
					}
				});
			}
		}
	);
};




exports.driverGetStatus = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var nowdate = new Date().getTime();
	var cgroup = req.body.cgroup;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var degree = req.body.degree;
	var accuracy = req.body.accuracy;
	var checkSmilesAPP; 
	var appversion;
	
	if(req.url.substring(1,7)=='smiles'){
		appversion = config.smilesappversion;
		checkSmilesAPP = true;
	} else {
		appversion = config.mobileappversion;
	}

	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	var dataDriver = DriversModel.findOne(
		{ _id: _id, device_id: device_id, _id_garage: { $exists: true } },
		{ fname: 1, lname: 1, phone: 1, carplate: 1, cgroup: 1, psg_id: 1, status: 1, msgphone: 1, msgnote: 1, msgstatus: 1, jobtype: 1, expdategarage: 1, moneygarage: 1, expdateapp: 1, moneyapp: 1, psg_detail: 1, psg_destination: 1, psg_curaddr: 1, appversion: 1, useappfree: 1, useappfreemsg: 1, active: 1, _id_garage: 1 });
	dataDriver.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1, name: 1, phone: 1 });
	dataDriver.exec(function (err, drv) {
		if (drv == null) {
			res.json({
				status: false,
				msg: langdrvth.code_0000,
				data: Object.drv
			});
		} else {
			//_sub_drvlogservice(_id,device_id,curlng,curlat,curloc,drv.status,degree,accuracy);
			if ((drv.status == 'ON' || drv.status == 'OFF') && drv.active == 'N') {
				drv.ccwaitingtime = drv._id_garage.ccwaitingtime;
				res.json({
					status: false,
					name: 'abc',
					msg: langdrvth.code_0001 + ' ' + drv._id_garage.name + ' ' + drv._id_garage.phone,
					data: drv
				});
			} else {
				if (drv.expdategarage) {
					expdategarage = new Date(drv.expdategarage).getTime();
				} else {
					expdategarage = nowdate;
				}
				if (drv.expdateapp) {
					expdateapp = new Date(drv.expdateapp).getTime();
				} else {
					expdateapp = nowdate;
				}
				timegarageleft = expdategarage - nowdate;
				timeappleft = expdateapp - nowdate;
				if (timegarageleft) {
					timegarageleft = timegarageleft;
				} else {
					timegarageleft = 0;
				}
				if (timeappleft) {
					timeappleft = timeappleft;
				} else {
					timeappleft = 0;
				}
				if (drv.status == "APPROVED") {
					drv.status = "ON";
				}
				drv.accuracy = req.body.accuracy ? req.body.accuracy : drv.accuracy;
				drv.degree = req.body.degree ? req.body.degree : drv.degree;
				drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng;
				drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat;
				drv.curloc = curloc ? curloc : drv.curloc;
				drv.updated = new Date().getTime();
				drv.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						var curdate = new Date().getTime() + 25200000;	// because we keep date as tring so we have to +7 hrs. to get Thai time							
						var rightNow = new Date(curdate);
						var ddate = rightNow.toISOString().slice(0, 10);
						//var ddate = rightNow.toISOString().slice(0,10).replace(/-/g,"");		
						DrvkeeplogModel.findOne(
							{
								_driver: _id,
								ddate: ddate
							},
							function (err, kplog) {
								if (err) {
									res.json({
										err: err
									})
								} else {
									if (kplog == null) {
										DrvkeeplogModel.create({
											_driver: req.body._id,
											fname: drv.fname,
											lname: drv.lname,
											carplate: drv.carplate,
											phone: drv.phone,
											cgroup: drv.cgroup,
											ddate: ddate,
											ncount: 10,
											status: drv.status,
											created: new Date().getTime()
										},
											function (err, response) {
												if (err) {
													res.json({ status: false, msg: err });
												} else {
													res.json({
														status: true,
														timegarageleft: timegarageleft,
														timeappleft: timeappleft,
														//appversion: appversion, 
														appversion:  checkSmilesAPP ? config.smilesappversion : config.mobileappversion, 
														data: drv
													});
												}
											});
									} else {
										if (drv.status != "OFF") {
											kplog.ncount = kplog.ncount + 5;
											kplog.status = drv.status;
											kplog.save(function (err, response) {
												if (err) {
													res.json({ status: false, msg: err });
												} else {
													res.json({
														status: true,
														timegarageleft: timegarageleft,
														timeappleft: timeappleft,
														//appversion: appversion, 
														appversion:  checkSmilesAPP ? config.smilesappversion : config.mobileappversion, 
														ccwaitingtime: drv._id_garage.ccwaitingtime,
														data: drv
													});
												}
											});
										} else {
											res.json({
												status: true,
												timegarageleft: timegarageleft,
												timeappleft: timeappleft,
												//appversion: appversion, 
												appversion:  checkSmilesAPP ? config.smilesappversion : config.mobileappversion, 
												data: drv
											});
										}
									}
								}
							}
						);
					}
				});
			}
		}
	});

	function _sub_drvlogservice(_id, device_id, curlng, curlat, curloc, status, degree, accuracy) {
		if (config.drvlogservice) {
			var reqset = '{"_id": "' + _id + '", "imei": "' + device_id + '", "lat": ' + curlat + ', "lng": ' + curlng + ', "loc": [' + curloc + '], "sts": "' + status + '"}';
			var postData = JSON.parse(JSON.stringify(reqset));
			var options = {
				host: config.drvlogservicehost,
				port: config.drvlogserviceport,
				path: config.drvlogservicepath,
				method: config.drvlogservicemethod,
				headers: {
					'Content-Type': 'application/json; charset=utf-8',
					'Content-Length': Buffer.byteLength(postData)
				}
			};
			var request = http.request(options, function (response) {
				response.on('data', function (d) {
				});
				response.on('end', function () {
				});
			});
			request.on('error', function (err) {
				console.log('cannot do _sub_drvlogservice')
			});
			request.write(postData);
			request.end();
		}
	}
};




exports.driverGetByID = function (req, res) {
	var _id = req.body._id;
	DriversModel.findOne(
		{ _id: _id, active: "Y" },
		{ _id: 1, fname: 1, lname: 1, phone: 1, allowpsgcontact: 1, carreturn: 1, carreturnwhere: 1, english: 1, carplate_formal: 1, carplate: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, curlat: 1, curlng: 1, curloc: 1, imgface: 1, imgcar: 1, imglicence: 1, psg_id: 1, status: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
		function (err, drv) {
			if (drv == null) {
				//res.json({ status: false , msg: " ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง" });
				_getErrDetail('err009', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "",
					data: drv
				});
			}
		}
	);
};




exports.driverchangeOnOff = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ device_id: 1, carreturn: 1, carreturnwhere: 1, status: 1 },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
				//res.json({ status: false ,   msg: "error", data: err  });
			} else {
				if (drv.status == "DELETED") {
					res.json({ status: false, msg: "ขออภัยค่ะ ระบบไม่สามารถให้บริการได้ เนื่องจากบัญชีของคุณอยู่ในสถานะถูกลบออกจากระบบ หากท่านต้องการใช้งาน กรุณาติดต่อ (02)643-0807-9 จันทร์-ศุกร์ เวลา 9.00-18.00 น." });
				} else {
					if (req.body.status) { drv.status = req.body.status ? req.body.status : drv.status; }
					//drv.psg_id = "";
					drv.updated = new Date().getTime();
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							res.json({
								status: true,
								data: {
									status: drv.status,
									carreturn: drv.carreturn,
									carreturnwhere: drv.carreturnwhere
								}
							});
						}
					});
				}
			}
		}
	);
};




exports.driverSearchPassenger = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var radian = req.body.radian;
	var amount = req.body.amount;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (typeof radian == 'undefined' && radian == null) {
		radian = config.drvsearchpsgradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.drvsearchpsgamount;
	}

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
			} else {
				if (drv.status == "DIRECT") {
					PassengerModel.findOne(
						{
							_id: drv.psg_id, status: "DIRECT"
						},
						{ _id: 1, device_id: 1, job_id: 1, psgtype: 1, createdjob: 1, phone: 1, contactphone: 1, createdvia: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, drv_id: 1, jobtype: 1, status: 1 },
						function (err, psglist) {
							if (psglist == null) {
								drv.psg_id = "";
								drv.status = "ON";
								drv.updated = new Date().getTime();
								drv.save(function (err, result) {
									if (err) {
										res.json({ status: false, msg: "error", data: err });
									} else {
										res.json({
											status: true,
											drvstatus: drv.status
										});
									}
								})
							} else {
								res.json({
									status: true,
									mgs: "data direct joblist",
									drvstatus: drv.status,
									data: psglist
								});
							}
						}
					);
				} else {
					JoblisthotelModel.find(
						{
							"drvarroundlist._id": _id,
							"status": { $in: ["ON", "BROADCAST"] },
							"favcartype": { $in: [[], drv.cartype] }
						},
						{ _id: 1, device_id: 1, job_id: 1, psgtype: 1, createdjob: 1, hotel_phone: 1, psg_phone: 1, createdvia: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, drv_id: 1, jobtype: 1, psgtype: 1, status: 1 },
						{ limit: amount },
						function (err, hotellist) {
							if (hotellist == 0) {
								PassengerModel.find(
									{
										//curloc : { $near : curloc, $maxDistance: radian },   => for 2d index => not working
										// do not forget  => db.passengers.createIndex( { curloc : "2dsphere" } )
										// Donot forget to create 2d Index for drivers collection : curloc!!!!	- > not working  user 2d sphere instead
										//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
										"drvarroundlist._id": _id,
										"status": { $in: ["ON"] },
										"favcartype": { $in: [[], drv.cartype] }
									},
									{ _id: 1, device_id: 1, job_id: 1, psgtype: 1, createdjob: 1, phone: 1, contactphone: 1, createdvia: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, drv_id: 1, jobtype: 1, status: 1 },
									{ limit: amount },
									function (err, psglist) {
										if (psglist == 0) {
											res.json({
												status: false,
												drvstatus: drv.status,
												msg: "No data"
											});
										} else {
											res.json({
												status: true,
												mgs: "data passenger joblist",
												drvstatus: drv.status,
												data: psglist,
												datahotel: hotellist
											});
										}
									}
								);
							} else {
								PassengerModel.find(
									{
										//curloc : { $near : curloc, $maxDistance: radian },   => for 2d index => not working
										// do not forget  => db.passengers.createIndex( { curloc : "2dsphere" } )
										// Donot forget to create 2d Index for drivers collection : curloc!!!!	- > not working  user 2d sphere instead
										//Specifies a point for which a geospatial query returns the documents from nearest to farthest.											
										"drvarroundlist._id": _id,
										"status": { $in: ["ON"] },
										"favcartype": { $in: [[], drv.cartype] }
									},
									{ _id: 1, device_id: 1, job_id: 1, psgtype: 1, createdjob: 1, phone: 1, contactphone: 1, createdvia: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, drv_id: 1, jobtype: 1, status: 1 },
									{ limit: amount },
									function (err, psglist) {
										if (psglist == 0) {
											res.json({
												status: true,
												mgs: "data : hotel joblist",
												drvstatus: drv.status,
												data: psglist,
												datahotel: hotellist
											});
										} else {
											res.json({
												status: true,
												mgs: "data hotel and passenger joblist 1",
												drvstatus: drv.status,
												data: psglist,
												datahotel: hotellist
											});
										}
									}
								);
							}
						}
					);
				}
			}
		}
	);
};





exports.driverSearchHotel = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var radian = req.body.radian;
	var amount = req.body.amount;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (typeof radian == 'undefined' && radian == null) {
		radian = config.drvsearchpsgradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.drvsearchpsgamount;
	}

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ status: 1, cartype: 1 },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
			} else {
				if (req.body.accuracy) { drv.accuracy = req.body.accuracy ? req.body.accuracy : drv.accuracy; }
				if (req.body.degree) { drv.degree = req.body.degree ? req.body.degree : drv.degree; }
				if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
				if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
				if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
				drv.updated = new Date().getTime();
				drv.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						JoblisthotelModel.find(
							{
								//curloc : { $near : curloc, $maxDistance: radian },   => for 2d index => not working
								// do not forget  => db.passengers.createIndex( { curloc : "2dsphere" } )
								// curloc:  { 
								// 	$near: {
								// 		$geometry: {
								// 			type: "Point" ,
								// 			coordinates: curloc
								// 		} , 
								// 		$maxDistance: radian 
								// 		//$minDistance: 1 
								// 	}
								// }
								// , status : "ON"
								// , favcartype: { $in: [ [] , drv.cartype] }
								"drvarroundlist._id": _id,
								"status": { $in: ["ON", "BROADCAST"] },
								"favcartype": { $in: [[], drv.cartype] }
							},
							{ _id: 1, device_id: 1, job_id: 1, psgtype: 1, createdjob: 1, phone: 1, createdvia: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, drv_id: 1, jobtype: 1 },
							{ limit: amount },
							function (err, hotellist) {
								// Donot forget to create 2d Index for drivers collection : curloc!!!!	- > not working  user 2d sphere instead
								if (hotellist == 0) {
									res.json({
										status: false,
										drvstatus: drv.status,
										msg: "No data"
									});
								} else {
									res.json({
										//msg: "This is passenger list", 
										status: true,
										drvstatus: drv.status,
										data: hotellist
									});
									//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
								}
							}
						);
					}
				});
			}
		}
	);
};




exports.driverAcceptCall = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		var psgappcompare;
		var firstshift;
		var response;
		var responsedrv;

		PassengerModel.update(
			{ _id: psg_id, status: "ON", occupied: false },
			{ $set: { occupied: true } },
			{ new: true },
			function (err, tank) {
				if (err) return handleError(err);
				if (tank.nModified == 0) {
					res.json({
						status: false,
						msg: "งานนี้มีผู้รับไปแล้ว กรุณาเลือกงานใหม่ค่ะ"
					});
				} else {
					PassengerModel.findOne(
						{ _id: psg_id, "status": { $in: ["ON"] } },
						{ device_id: 1, status: 1, job_id: 1, psgtype: 1, jobtype: 1, appversion: 1, cgroup: 1 },
						function (err, result) {
							if (result == null) {
								//res.json({ status: false , msg:  "ผู้โดยสารท่านนี้ได้ถูกจองไปแล้ว กรุณาเลือกผู้โดยสารท่านใหม่", data:err });
								_getErrDetail('err008', function (errorDetail) {
									res.json({
										status: false,
										msg: errorDetail
									});
								});
							} else {
								passenger_ssid = result.device_id;
								passenger_jobid = result.job_id;
								taxi_duid = _id;
								if (result.cgroup=='smiles'){
									psgappcompare = '2';
								} else {
									psgappcompare = result.appversion;
								}
								if (compareVersions(psgappcompare, '1') > 0) {
									DriversModel.findOne(
										{ _id: _id, device_id: device_id },
										{ status: 1, updated: 1, fname: 1, lname: 1, phone: 1, carplate: 1, cgroup: 1, firstshift: 1 },
										function (err, drv) {
											if (drv == null) {
												_driverAutoLogin(req, res);
											} else {
												drv.psg_id = psg_id;
												drv.job_id = result.job_id;
												drv.updated = new Date().getTime();
												drv.status = "BUSY";
												drv.jobtype = result.jobtype;
												drv.save(function (err, responsedrv) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														PassengerModel.findOne(
															{ _id: psg_id },
															{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1, psgtype: 1 },
															function (err, psg) {
																if (psg == null) {
																	res.json({ status: false, msg: "This passenger is not available." });
																} else {
																	psg.drv_id = _id;
																	psg.updated = new Date().getTime();
																	psg.status = "BUSY";
																	psg.save(function (err, response) {
																		if (err) {
																			res.json({ status: false, msg: "error", data: err });
																		} else {
																			socket.emit("PassengerSocketOn", response);
																			_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvwait', '', curlng, curlat);
																			_sub_samsungaction(passenger_ssid, passenger_jobid, taxi_duid, "BUSY", "accept", "POST");
																			res.json({
																				status: true,
																				data: response
																			});
																		}
																	});
																}
															}
														);
													}
												});
											}
										}
									);
								} else {
									DriversModel.findOne(
										{ _id: _id, device_id: device_id },
										{ status: 1, updated: 1, fname: 1, lname: 1, phone: 1, carplate: 1, cgroup: 1, firstshift: 1 },
										function (err, drv) {
											if (drv == null) {
												//res.json({ status: false , msg: "Your phone does not exist. Please register and try again." });
												_driverAutoLogin(req, res);
											} else {
												drv.psg_id = psg_id;
												drv.job_id = result.job_id;
												drv.updated = new Date().getTime();
												drv.status = "WAIT";
												drv.jobtype = result.jobtype;
												drv.save(function (err, responsedrv) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														PassengerModel.findOne(
															{ _id: psg_id },
															{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1, psgtype: 1 },
															function (err, psg) {
																if (psg == null) {
																	res.json({ status: false, msg: "This passenger is not available." });
																} else {
																	psg.drv_id = _id;
																	psg.updated = new Date().getTime();
																	psg.status = "WAIT";
																	psg.save(function (err, response) {
																		if (err) {
																			res.json({ status: false, msg: "error", data: err });
																		} else {
																			socket.emit("PassengerSocketOn", response);
																			_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvwait', '', curlng, curlat);
																			res.json({
																				status: true,
																				data: response
																			});
																		}
																	});
																}
															}
														);
													}
												});
											}
										}
									);
								}
							}
						}
					);
				}
			}
		)
	};
}




exports.driverAcceptHotel = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		var firstshift;
		var response;
		var responsedrv;
		// Load config for this driver
		var dataDriver = DriversModel.findOne(
			{ _id: _id, _id_garage: { $exists: true } },
			{ _id: 1, device_id: 1, carplate: 1, prefixcarplate: 1, status: 1, _id_garage: 1, curlat: 1, curlng: 1, curloc: 1, firstshift: 1 }
		);
		dataDriver.populate('_id_garage', { searchpsgradian: 1, ccwaitingtime: 1, bidjobwaitingtime: 1, bidjobradian: 1 });
		dataDriver.exec(function (err, resultdata) {
			if (resultdata == null) {
				_getErrDetail('err008', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				var searchpsgradian = resultdata._id_garage.searchpsgradian;
				var bidjobradian = resultdata._id_garage.bidjobradian;
				var ccwaitingtime = resultdata._id_garage.ccwaitingtime;
				var bidjobwaitingtime = resultdata._id_garage.bidjobwaitingtime;
				JoblisthotelModel.findOne(
					{
						"_id": psg_id,
						"status": { $in: ["ON", "BROADCAST"] },
						"drvarroundlist._id": _id
					},
					{ status: 1, job_id: 1, psgtype: 1, _id_callcenterpsg: 1, drv_id: 1, drv_device_id: 1, drvarroundlist: 1, biddingstart: 1, biddingend: 1, drvbiddingjob: 1 },
					function (err, result) {
						if (result == null) {
							//res.json({ status: false , msg:  "ผู้โดยสารท่านนี้ได้ถูกจองไปแล้ว กรุณาเลือกผู้โดยสารท่านใหม่", data:err });
							_getErrDetail('err008', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail
								});
							});
						} else {
							if (result.status == "BROADCAST") {
								////////////////////////////////////  BROADCAST
								var curdrvdist = result.drvarroundlist.find(function (drv) {
									if (drv._id.toString() === _id) {
										return drv
									}
								})
								if (curdrvdist.dist < bidjobradian) {
									// รับงานทันที
									console.log('รับ งานทันที')
									JoblisthotelModel.update(
										{ _id: psg_id, occupied: false },
										{ $set: { occupied: true } },
										{ new: true },
										function (err, tank) {
											if (err) return handleError(err);
											// res.send(tank);
											if (tank.nModified == 0) {
												res.json({
													status: false,
													msg: "งานนี้มีผู้รับไปแล้ว กรุณาเลือกงานใหม่ค่ะ"
												});
											} else {
												result.drv_id = _id;
												result.drv_device_id = device_id;
												result.biddingend = true;
												result.status = "BUSY";
												result.save(function (err, result2) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														DriversModel.findOne(
															{ _id: _id, device_id: device_id },
															function (err, drv) {
																if (drv == null) {
																	res.json({ status: false, msg: "Driver is missing", data: err });
																} else {
																	drv.psg_id = psg_id;
																	drv.job_id = result.job_id;
																	drv.updated = new Date();
																	drv.status = "BUSY";
																	drv.jobtype = "HOTELS";
																	drv.save(function (err, response) {
																		if (err) {
																			console.log(err)
																			res.json({ status: false, msg: "Cannot save drivers data", data: err });
																		} else {
																			CallcenterpsgModel.findOne(
																				{ _id: result._id_callcenterpsg },
																				function (err, psg) {
																					if (psg == null) {
																						res.json({ status: false, msg: "Psg info is missing (c)" });
																					} else {
																						//////////////////////
																						// รับงาน
																						psg.acceptedTaxiIds.push({
																							device_id: req.body.device_id,
																							drv_id: _id,
																							acceptedDate: new Date()
																						});
																						psg.jobhistory.push({
																							action: "Drv accepted job immediately",
																							status: "ASSIGNED",
																							actionby: req.body._id,
																							reqbody: req.body,
																							created: new Date()
																						});
																						/////////////////////////	
																						psg.status = "ASSIGNED";	// if previous is depending, psg_status get data from input to function
																						psg.drv_id = drv._id;
																						psg.drv_carplate = drv.carplate_formal;
																						psg.carplate = drv.carplate;
																						psg.prefixcarplate = drv.prefixcarplate;
																						psg.updated = new Date();
																						psg.datereadmsg = new Date();
																						psg.dassignedjob = new Date();
																						psg.save(function (err, result) {
																							if (err) {
																								console.log(err)
																								res.json({ status: false, msg: "Cannot save passengers data", data: err });
																							} else {
																								JoblisthotelModel.findOne(
																									{ _id: psg_id },
																									function (err, psgjoblist) {
																										if (psgjoblist == null) {
																											res.json({ status: false, msg: "This passenger is not available." });
																										} else {
																											psgjoblist.drv_id = drv._id;
																											psgjoblist.drv_device_id = drv.device_id;
																											psgjoblist.drv_name = drv.fname + ' ' + drv.lname;
																											psgjoblist.drv_phone = drv.phone;
																											psgjoblist.drv_carplate = drv.carplate_formal;
																											psgjoblist.carplate = drv.carplate;
																											psgjoblist.prefixcarplate = drv.prefixcarplate;
																											psgjoblist.datedrvwait = new Date();
																											psgjoblist.updated = new Date();
																											psgjoblist.status = "BUSY";
																											psgjoblist.save(function (err, psgjoblistresult) {
																												if (err) {
																													res.json({ status: false, msg: "error", data: err });
																												} else {
																													_sub_ClearBiddingDrv();
																													socket.of('/' + psg.cgroup).emit("gotdispatchaction", { response: response, psg_data: result });
																													res.json({
																														status: true,
																														data: {
																															firstshift: response.firstshift,
																															status: response.status,
																															psg_id: response.psg_id,
																															jobtype: response.jobtype,
																															curaddr: psg.curaddr,
																															destination: psg.destination,
																															detail: psg.detail,
																															dassignedjob: psg.dassignedjob,
																															psgtype: "callcenter"
																														}
																													});
																												}
																											});
																										}
																									}
																								);
																							}
																						});
																					}
																				}
																			);
																		}
																	});
																}
															}
														);
													}
												});
											}
										}
									);
								} else {
									// ให้ทำการ bidding โดยดูว่าเป็นรอบแรกหรือปล่าว ถ้าไม่ใช่ ไม่ต้องตั้ง timeout							
									if (result.biddingstart) {
										// biddingstart = true แสดงว่าไม่ใช่รอบแรก SO DONOT SET TIMEOUT 
										// ก่อนหน้านั้นเช็คก่อนว่างานถูกรับไปหรือยังด้วย atomic function update
										JoblisthotelModel.update(
											{ 
												"_id": psg_id,
												"occupied": false,
												"status": { $in: ["BROADCAST"] },
												"drvarroundlist._id": _id	
											},
											{
												$push: {
													drvbiddingjob: { _id: _id, dist: curdrvdist.dist,created: new Date() }
												}
												// $set:
												// {
												// 	drvbiddingjob.push({  });
												// }
											},
											{ new: true },
											function (err, tank) {
												if (err) return handleError(err);
												if (tank.nModified == 0) {
													// tank = 0 คือไม่เกิดการ update ขึ้น condition ไม่ผ่าน รับงานไม่ได้
													res.json({ status: false, msg: "Cannot bid a job" });
												} else {
													DriversModel.findOne(
														{ _id: _id, device_id: device_id },
														function (err, drv) {
															if (drv == null) {
																res.json({ status: false, msg: "Driver is missing", data: err });
															} else {
																drv.psg_id = psg_id;
																drv.job_id = result.job_id;
																drv.updated = new Date();
																drv.status = "WAIT";
																drv.jobtype = "HOTELS";
																drv.save(function (err, response) {
																	if (err) {
																		console.log(err)
																		res.json({ status: false, msg: "Cannot save drivers data", data: err });
																	} else {
																		CallcenterpsgModel.findOne(
																			{ _id: result._id_callcenterpsg },
																			function (err, psg) {
																				if (psg == null) {
																					res.json({ status: false, msg: "Psg info is missing (b)" });
																				} else {
																					//////////////////////
																					// รับงาน
																					psg.drvbiddingjob.push({
																						_id: _id,
																						created: new Date()
																					});
																					psg.acceptedTaxiIds.push({
																						device_id: req.body.device_id,
																						drv_id: _id,
																						acceptedDate: new Date()
																					});
																					psg.jobhistory.push({
																						action: "Drv Bidding job",
																						status: "WAIT",
																						actionby: req.body._id,
																						reqbody: req.body,
																						created: new Date()
																					});
																					/////////////////////////	
																					//psg.status = "WAIT";	// if previous is depending, psg_status get data from input to function
																					psg.updated = new Date();
																					psg.datereadmsg = new Date();
																					psg.dassignedjob = new Date();
																					psg.save(function (err, result) {
																						if (err) {
																							console.log(err)
																							res.json({ status: false, msg: "Cannot save passengers data", data: err });
																						} else {
																							/////// ---------DO NOT SET TIMEOUT for bidding function
																							res.json({
																								status: true,
																								data: {
																									firstshift: response.firstshift,
																									status: response.status,
																									psg_id: response.psg_id,
																									jobtype: response.jobtype,
																									curaddr: psg.curaddr,
																									destination: psg.destination,
																									detail: psg.detail,
																									dassignedjob: psg.dassignedjob,
																									psgtype: "callcenter"
																								}
																							});
																						}
																					});
																				}
																			}
																		);
																	}
																});
															}
														}
													);
												}
											}
										)
									} else {
										// SET TIMEOUT 
										result.drvbiddingjob.push({
											_id: _id,
											dist: curdrvdist.dist,
											created: new Date()
										});
										result.biddingstart = true;
										result.biddingend = false;
										result.biddingtimeout = new Date() + bidjobwaitingtime;
										result.save(function (err, result2) {
											if (err) {
												res.json({ status: false, msg: "error", data: err });
											} else {
												DriversModel.findOne(
													{ _id: _id, device_id: device_id },
													function (err, drv) {
														if (drv == null) {
															res.json({ status: false, msg: "Driver is missing", data: err });
														} else {
															drv.psg_id = psg_id;
															drv.job_id = result.job_id;
															drv.updated = new Date();
															drv.status = "WAIT";
															drv.jobtype = "HOTELS";
															drv.save(function (err, response) {
																if (err) {
																	console.log(err)
																	res.json({ status: false, msg: "Cannot save drivers data", data: err });
																} else {																																		
																	CallcenterpsgModel.findOne(
																		{ _id: result._id_callcenterpsg },
																		function (err, psg) {
																			if (psg == null) {
																				res.json({ status: false, msg: "Drv info is missing (a)" });
																			} else {
																				//////////////////////
																				// รับงาน
																				psg.drvbiddingjob.push({
																					_id: _id,
																					created: new Date()
																				});
																				psg.acceptedTaxiIds.push({
																					device_id: req.body.device_id,
																					drv_id: _id,
																					acceptedDate: new Date()
																				});
																				psg.jobhistory.push({
																					action: "Drv Bidding job",
																					status: "WAIT",
																					actionby: req.body._id,
																					reqbody: req.body,
																					created: new Date()
																				});
																				/////////////////////////	
																				//psg.status = "WAIT";	// if previous is depending, psg_status get data from input to function
																				psg.updated = new Date();
																				psg.datereadmsg = new Date();
																				psg.dassignedjob = new Date();
																				psg.save(function (err, result) {
																					if (err) {
																						console.log(err)
																						res.json({ status: false, msg: "Cannot save passengers data", data: err });
																					} else {
																						/////// --------- SET TIMEOUT for bidding function
																						setTimeout(function () {
																							_SetBiddingTimeout();
																						}, bidjobwaitingtime);
																						res.json({
																							status: true,
																							data: {
																								firstshift: response.firstshift,
																								status: response.status,
																								psg_id: response.psg_id,
																								jobtype: response.jobtype,
																								curaddr: psg.curaddr,
																								destination: psg.destination,
																								detail: psg.detail,
																								dassignedjob: psg.dassignedjob,
																								psgtype: "callcenter"
																							}
																						});
																					}
																				});
																			}
																		}
																	);
																}
															});
														}
													}
												);
											}
										});
									}
								}
							} else {
								DriversModel.findOne(
									{ _id: _id, device_id: device_id },
									{ status: 1, updated: 1, _id: 1, device_id: 1, fname: 1, lname: 1, phone: 1, carplate: 1, cgroup: 1, firstshift: 1 },
									function (err, drv) {
										if (drv == null) {
											//res.json({ status: false , msg: "Your phone does not exist. Please register and try again." });
											_driverAutoLogin(req, res);
										} else {
											drv.psg_id = psg_id;
											drv.job_id = result.job_id;
											drv.updated = new Date();
											drv.status = "BUSY";
											drv.jobtype = "HOTELS";
											drv.save(function (err, responsedrv) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													JoblisthotelModel.findOne(
														{ _id: psg_id },
														{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1, psgtype: 1 },
														function (err, psg) {
															if (psg == null) {
																res.json({ status: false, msg: "This passenger is not available." });
															} else {
																psg.drv_id = _id;
																psg.drv_device_id = drv.device_id;
																psg.drv_name = drv.fname + ' ' + drv.lname;
																psg.drv_phone = drv.phone;
																psg.drv_carplate = drv.carplate;
																psg.cgroup = drv.cgroup;
																psg.datedrvwait = new Date();
																psg.updated = new Date();
																psg.status = "BUSY";
																psg.save(function (err, response) {
																	if (err) {
																		res.json({ status: false, msg: "error", data: err });
																	} else {
																		socket.emit("HotelSocketOn", response);
																		res.json({
																			status: true,
																			data: response
																		});
																	}
																});
															}
														}
													);
												}
											});
										}
									}
								);
							}
						}
					}
				);
			}
		});


		function _sub_ClearBiddingDrv() {
			JoblisthotelModel.findOne(
				{ "_id": psg_id, "occupied": true },
				function (err, result) {
					if (err) return handleError(err);
					if (result == null) {
						console.log(' no winning drv id 111 ')
					} else {
						var lostdrv_id = result.drvbiddingjob;
						for (var i = 0, len = lostdrv_id.length; i < len; i++) {
							DriversModel.findOneAndUpdate(
								{ _id: lostdrv_id[i]._id, status: "WAIT" },
								{ $set: { status: "ON", msgstatus: "OLD" } },
								{ upsert: false, new: true },
								function (err, drv) {
									if (err) return handleError(err);
									if (result == null) {
										console.log(' no winning drv id 111 ')
									} else {
										socket.emit("DriverSocketOn", drv);
										//socket.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ขออภัยค่ะ คุณไม่สามารถรับงานนี้ได้', message: 'งานนี้ได้ถูกส่งให้คนขับที่ใกล้ผู้โดยสารที่สุดแล้ว' });
									}
								}
							);
						}
					}
				}
			);
		}


		function _SetBiddingTimeout() {
			JoblisthotelModel.findOneAndUpdate(
				{ "_id": psg_id, "occupied": false, "status": { $in: ["BROADCAST"] } },
				{ $set: { occupied: true } },
				{ upsert: false, new: true },
				function (err, result) {
					if (err) return handleError(err);
					if (result == null) {
						console.log(' no winning drv id 111 ')
					} else {
						var arrshuffle = result.drvbiddingjob;
						if (arrshuffle.length > 0) {
							// ในกรณีที่ boradcast ซ้ำงานเดิม ตัว timeout เก่ายังไม่ถูกยกเลิก แต่ไม่มี winner drv จะทำให้ reserver restart ต้องเช็คก่อน
							setTimeout(function () {
								// delay การเคลีย drv status from WATI to ON เผื่่อยังมี process WAIT ค้างอยู่
								var arrsort = arrshuffle.sort(dynamicSort('dist'));
								var winnindrv_id = arrsort[0]._id;
								var lostdrv_id = arrsort.slice(1);

								for (var i = 0, len = lostdrv_id.length; i < len; i++) {
									DriversModel.findOneAndUpdate(
										{ _id: lostdrv_id[i]._id },
										{ $set: { status: "ON", msgstatus: "OLD" } },
										{ upsert: false, new: true },
										function (err, drv) {
											if (err) return handleError(err);
											if (result == null) {
												console.log(' no winning drv id 111 ')
											} else {
												socket.emit("DriverSocketOn", drv);
												socket.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ขออภัยค่ะ คุณไม่สามารถรับงานนี้ได้', message: 'งานนี้ได้ถูกส่งให้คนขับที่ใกล้ผู้โดยสารที่สุดแล้ว' });
											}
										}
									);
								}

								DriversModel.findOne(
									{ _id: winnindrv_id },
									function (err, drv) {
										if (drv == null) {
											console.log(' no winning drv id 222 ')
										} else {
											drv.psg_id = psg_id;
											drv.job_id = result.job_id;
											drv.updated = new Date();
											drv.status = "BUSY";
											drv.jobtype = "HOTELS";
											drv.save(function (err, response) {
												if (err) {
													console.log(err)
												} else {
													CallcenterpsgModel.findOne(
														{ _id: result._id_callcenterpsg },
														function (err, psg) {
															if (psg == null) {
																console.log(' no winning drv id 333 ')
															} else {
																//////////////////////
																// รับงาน
																psg.acceptedTaxiIds.push({
																	device_id: drv.device_id,
																	drv_id: drv._id,
																	acceptedDate: new Date()
																});
																psg.jobhistory.push({
																	action: "Drv accepted job who won bidding result",
																	status: "ASSIGNED",
																	actionby: req.body._id,
																	reqbody: req.body,
																	created: new Date()
																});
																/////////////////////////	
																psg.status = "ASSIGNED";	// if previous is depending, psg_status get data from input to function
																psg.drv_id = drv._id;
																psg.drv_carplate = drv.carplate_formal;
																psg.carplate = drv.carplate;
																psg.prefixcarplate = drv.prefixcarplate;
																psg.updated = new Date();
																psg.datereadmsg = new Date();
																psg.dassignedjob = new Date();
																psg.save(function (err, result) {
																	if (err) {
																		console.log(err)
																	} else {
																		JoblisthotelModel.findOne(
																			{ _id: psg_id },
																			function (err, psgjoblist) {
																				if (psgjoblist == null) {
																					console.log('This passenger is not available.');
																				} else {
																					psgjoblist.biddingend = true;
																					psgjoblist.drv_id = drv._id;
																					psgjoblist.drv_device_id = drv.device_id;
																					psgjoblist.drv_name = drv.fname + ' ' + drv.lname;
																					psgjoblist.drv_phone = drv.phone;
																					psgjoblist.drv_carplate = drv.carplate_formal;
																					psgjoblist.carplate = drv.carplate;
																					psgjoblist.prefixcarplate = drv.prefixcarplate;
																					psgjoblist.datedrvwait = new Date();
																					psgjoblist.updated = new Date();
																					psgjoblist.status = "BUSY";
																					psgjoblist.save(function (err, psgjoblistresult) {
																						if (err) {
																							console.log(' no winning drv id 666 ')
																						} else {
																							_sub_ClearBiddingDrv();
																							socket.emit("DriverSocketOn", drv);
																							socket.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ยินดีด้วย คุณได้รับงาน', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
																							socket.of('/' + psg.cgroup).emit("gotdispatchaction", { response: response, psg_data: result });
																						}
																					});
																				}
																			}
																		);
																	}
																});
															}
														}
													);
												}
											});
										}
									}
								);
							}, config.biddingtimeoutdelayprocess);
						}
					}
				}
			);
		};


		function dynamicSort(property) {
			return function (a, b) {
				return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			}
		}				

	};
}




exports.driverCancelCall = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			{ status: 1, updated: 1, fname: 1, lname: 1, phone: 1, carplate: 1, cgroup: 1 },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = "";
					drv.job_id = "";
					drv.status = "ON";
					drv.updated = new Date().getTime();
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							PassengerModel.findOne(
								{ _id: psg_id },
								{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1 },
								function (err, psg) {
									if (psg == null) {
										res.json({
											status: false,
											msg: "error - This passenger  does not exist. Please check the information. ",
											data: err
										});
									} else {
										passenger_ssid = psg.device_id;
										passenger_jobid = psg.job_id;
										taxi_duid = _id;
										if (psg.status == "ON") {
											_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvcancel', 'driverCancelCall-ON', curlng, curlat);
											res.json({
												status: true,
												msg: "",
												//data: { status: response.status }
											});
										} else {
											psg.drv_id = "";
											psg.job_id = "";
											psg.status = "DRVDENIED";
											psg.updated = new Date().getTime();
											psg.save(function (err, response) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													socket.emit("PassengerSocketOn", response);
													_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvcancel', 'driverCancelCall-DRVDENIED', curlng, curlat);
													_sub_samsungaction(passenger_ssid, passenger_jobid, taxi_duid, "DRVDENIED", "cancel", "PUT");
													res.json({
														status: true,
														msg: "Update driver and passenger to ON => driver cancelled passenger",
														data: { status: response.status }
													});
												}
											});
										}
									}
								}
							);
						}
					});
				}
			}
		);
	};
}




exports.driverCancelHotel = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		//console.log('driverCancelHotel')
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			{ status: 1, updated: 1, fname: 1, lname: 1, phone: 1, carplate: 1 },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = "";
					drv.job_id = "";
					drv.status = "ON";
					drv.updated = new Date().getTime();
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							JoblisthotelModel.findOne(
								{ _id: psg_id },
								{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1 },
								function (err, psg) {
									if (psg == null) {
										//msg: "This passenger  does not exist. Please check the information."
										res.json({ status: false, msg: "error", data: err });
									} else {
										if (psg.status == "ARRIVED") {
											res.json({
												status: false,
												data: { status: psg.status }
											});
										} else {
											psg.drv_id = "";
											psg.job_id = "";
											psg.cgroup = "";
											psg.drvcancelwhere = "";
											psg.status = "DRVDENIED";
											psg.datedrvcancel = new Date().getTime();
											psg.updated = new Date().getTime();
											psg.save(function (err, response) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													if (psg.jobmode == "BROADCAST") {
														CallcenterpsgModel.findOne(
															{ _id: psg._id_callcenterpsg },
															function (err, ccpsg) {
																if (ccpsg == null) {
																	res.json({ status: false, msg: "Cannot find callcenterpsg data", data: err });
																} else {
																	//////////////////////
																	// pick hotel psg from BROADCAST
																	ccpsg.jobhistory.push({
																		action: "Drv calcelled psghotel",
																		status: "CANCELLED",
																		actionby: req.body._id,
																		reqbody: req.body,
																		created: new Date()
																	});
																	/////////////////////////	
																	ccpsg.status = "DEPENDING_REJECT";	// if previous is depending, psg_status get data from input to function
																	ccpsg.updated = new Date().getTime();
																	ccpsg.save(function (err, result) {
																		if (err) {
																			console.log(err)
																			res.json({ status: false, msg: "Cannot save drop hotelpsg data", data: err });
																		} else {
																			socket.emit("HotelSocketOn", response);
																			res.json({
																				status: true,
																				data: { status: response.status }
																			});
																		}
																	});
																}
															}
														);
													} else {
														socket.emit("HotelSocketOn", response);
														res.json({
															status: true,
															data: { status: response.status }
														});
													}
												}
											});
										}
									}
								}
							);
						}
					});
				}
			}
		);
	};
}





exports.driverCancelCallCenter = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		//console.log('driverCancelCallCenter')
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			{ status: 1, updated: 1 },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = "";
					drv.updated = new Date().getTime();
					drv.status = "ON";
					drv.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							CallcenterpsgModel.findOne(
								{ _id: psg_id },
								{ status: 1, updated: 1, cgroup: 1, jobhistory: 1 },
								function (err, psg) {
									if (psg == null) {
										//msg: "This passenger  does not exist. Please check the information."
										res.json({ status: false, msg: "error", data: err });
									} else {

										psg.jobhistory.push({
											action: "Drv cancelled job",
											status: "DEPENDING_REJECT",
											actionby: req.body._id,
											reqbody: req.body,
											created: new Date()
										});
										psg.drv_id = "";
										psg.updated = new Date().getTime();
										psg.status = "DEPENDING_REJECT";
										//console.log(req.body)
										psg.save(function (err, response) {
											if (err) {
												res.json({ status: false, msg: "error", data: err });
											} else {
												//console.log( ' cancel callcenter ')
												socket.of('/' + psg.cgroup).emit("driverCancelCallCenter", response);
												res.json({
													//msg: "Update driver and passenger to ON => driver canceled passenger",
													status: true,
													data: { status: response.status }
												});
											}
										});
									}
								}
							);
						}
					});
				}
			}
		);
	};
}




exports.driverPickPassenger = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		//console.log('driverPickPassenger')
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			{ device_id: 1, fname: 1, lname: 1, phone: 1, carplate: 1, cgroup: 1 },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = psg_id;
					drv.firstshift = false;
					drv.updated = new Date().getTime();
					drv.status = "PICK";
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							if (drv.jobtype == "HOTELS") {
								JoblisthotelModel.findOne(
									{ _id: psg_id },
									{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1 },
									function (err, psg) {
										if (psg == null) {
											res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
										} else {
											psg.drv_id = _id;
											psg.updated = new Date().getTime();
											psg.status = "PICK";
											psg.datedrvpick = new Date().getTime();
											psg.save(function (err, response) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													if (psg.jobmode == "BROADCAST") {
														CallcenterpsgModel.findOne(
															{ _id: psg._id_callcenterpsg },
															function (err, ccpsg) {
																if (ccpsg == null) {
																	res.json({ status: false, msg: "Cannot find callcenterpsg data", data: err });
																} else {
																	//////////////////////
																	// pick hotel psg from BROADCAST
																	ccpsg.jobhistory.push({
																		action: "Drv pick psghotel",
																		status: "PICK",
																		actionby: req.body._id,
																		reqbody: req.body,
																		created: new Date()
																	});
																	/////////////////////////	
																	ccpsg.status = "PICK";	// if previous is depending, psg_status get data from input to function
																	ccpsg.updated = new Date().getTime();
																	ccpsg.save(function (err, result) {
																		if (err) {
																			console.log(err)
																			res.json({ status: false, msg: "Cannot save pick hotelpsg data", data: err });
																		} else {
																			socket.emit("HotelSocketOn", response);
																			res.json({
																				status: true,
																				data: { status: response.status }
																			});
																		}
																	});
																}
															}
														);
													} else {
														socket.emit("HotelSocketOn", response);
														res.json({
															status: true,
															data: { status: response.status }
														});
													}
												}
											});
										}
									}
								);
							} else {
								PassengerModel.findOne(
									{ _id: psg_id },
									{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1, psgtype: 1, cgroup: 1 },
									function (err, psg) {
										if (psg == null) {
											res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
										} else {
											passenger_ssid = psg.device_id;
											passenger_jobid = psg.job_id;
											taxi_duid = _id;
											if (psg.psgtype == "BROADCAST") {
												CallcenterpsgModel.findOne(
													{ _id: psg.job_id },
													function (err, ccpsg) {
														if (psg == null) {
															DriversModel.findOne(
																{ _id: _id, device_id: device_id },
																function (err, drv) {
																	if (drv == null) {
																		res.json({ status: false, msg: "Drv info is missing (a)", data: err });
																		err
																	} else {
																		res.json({ status: false, msg: "Psg info is missing (b)", data: err });
																	}
																}
															);
														} else {
															//////////////////////
															// รับ psg
															ccpsg.acceptedTaxiIds.push({
																device_id: device_id,
																drv_id: _id,
																pickupDate: new Date()
															});
															ccpsg.jobhistory.push({
																action: "Drv pick psg",
																status: "PICK",
																actionby: _id,
																reqbody: req.body,
																created: new Date()
															});
															/////////////////////////	
															ccpsg.status = "PICK";	// if previous is depending, psg_status get data from input to function
															ccpsg.drv_id = _id;
															ccpsg.updated = new Date().getTime();
															ccpsg.save(function (err, result) {
																if (err) {
																	console.log(err)
																	res.json({ status: false, msg: "Cannot save passengers data", data: err });
																} else {
																	PassengerModel.findOne(
																		{ _id: psg_id },
																		function (err, realpsg) {
																			if (realpsg == null) {
																				res.json({ status: false, msg: "This passenger is not available." });
																			} else {
																				realpsg.drv_id = _id;
																				realpsg.status = "PICK";
																				realpsg.updated = new Date().getTime();
																				realpsg.save(function (err, response) {
																					if (err) {
																						res.json({ status: false, msg: "error", data: err });
																					} else {
																						socket.of('/' + psg.cgroup).emit("gotdispatchaction", { response: response, psg_data: result });
																						res.json({
																							status: true,
																							data: {
																								status: response.status,
																								psg_id: response.psg_id,
																								jobtype: response.jobtype,
																								curaddr: psg.curaddr,
																								destination: psg.destination,
																								detail: psg.detail,
																								dassignedjob: psg.dassignedjob,
																								psgtype: "callcenter"
																							}
																						});
																					}
																				});
																			}
																		}
																	);
																}
															});
														}
													}
												);
											} else {
												psg.drv_id = _id;
												psg.updated = new Date().getTime();
												psg.status = "PICK";
												psg.save(function (err, response) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														socket.emit("PassengerSocketOn", response);
														_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvpick', '', curlng, curlat);
														_sub_samsungaction(passenger_ssid, passenger_jobid, taxi_duid, "PICK", "pickup", "PUT");
														res.json({
															//msg: "Update driver and passenger to PICK => driver picked passenger",
															status: true,
															data: { status: response.status }
														});
													}
												});
											}
										}
									}
								);
							}
						}
					});
				}
			}
		);
	};
};




exports.driverPickHotel = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			{ device_id: 1, fname: 1, lname: 1, phone: 1, carplate: 1 },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = psg_id;
					drv.firstshift = false;
					drv.updated = new Date().getTime();
					drv.status = "PICK";
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							JoblisthotelModel.findOne(
								{ _id: psg_id },
								{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1 },
								function (err, psg) {
									if (psg == null) {
										res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
									} else {
										psg.drv_id = _id;
										psg.updated = new Date().getTime();
										psg.status = "PICK";
										psg.datedrvpick = new Date().getTime();
										psg.save(function (err, response) {
											if (err) {
												res.json({ status: false, msg: "error", data: err });
											} else {
												if (psg.jobmode == "BROADCAST") {
													CallcenterpsgModel.findOne(
														{ _id: psg._id_callcenterpsg },
														function (err, ccpsg) {
															if (ccpsg == null) {
																res.json({ status: false, msg: "Cannot find callcenterpsg data", data: err });
															} else {
																//////////////////////
																// pick hotel psg from BROADCAST
																ccpsg.jobhistory.push({
																	action: "Drv pick psghotel",
																	status: "PICK",
																	actionby: req.body._id,
																	reqbody: req.body,
																	created: new Date()
																});
																/////////////////////////	
																ccpsg.status = "PICK";	// if previous is depending, psg_status get data from input to function
																ccpsg.updated = new Date().getTime();
																ccpsg.save(function (err, result) {
																	if (err) {
																		console.log(err)
																		res.json({ status: false, msg: "Cannot save pick hotelpsg data", data: err });
																	} else {
																		socket.emit("HotelSocketOn", response);
																		res.json({
																			status: true,
																			data: { status: response.status }
																		});
																	}
																});
															}
														}
													);
												} else {
													socket.emit("HotelSocketOn", response);
													res.json({
														status: true,
														data: { status: response.status }
													});
												}
											}
										});
									}
								}
							);
						}
					});
				}
			}
		);
	};
};





function saveHistory(driverObj, passengerId, success) {

	// callcenter job
	if (driverObj.jobtype) {
		if (driverObj.jobtype === 'HOTELS') {
			JoblisthotelModel.findOne({ _id: passengerId }).exec(function (err, job) {
				if (!err && job) {

					HistoryModel.find({
						_ownerId: driverObj._id,
						_callcenterId: job._id_callcenterpsg
					}).exec(function (err, exists) {
						if (!err && exists.length === 0) {
							HistoryModel.create({
								type: 'CALLCENTER',
								status: success ? 'SUCCESSED' : 'CANCELLED',
								_ownerId: driverObj._id,
								_callcenterId: job._id_callcenterpsg,
							}, function (err, history) {
								if (!err && history)
									console.log('history create success');
							});
						}
					});
				}
			});
		} else {
			HistoryModel.find({
				_ownerId: driverObj._id,
				_callcenterId: passengerId
			}).exec(function (err, exists) {
				if (!err && exists.length === 0) {
					HistoryModel.create({
						type: 'CALLCENTER',
						status: success ? 'SUCCESSED' : 'CANCELLED',
						_ownerId: driverObj._id,
						_callcenterId: passengerId,
					}, function (err, history) {
						if (!err && history)
							console.log('history create success');
					});
				}
			});
		}
	}
	// passenger job
	else {
		JoblistModel.findOne({ job_id: driverObj.job_id }).select('psg_id').exec(function (err, job) {
			if (!err, job) {
				HistoryModel.find({
					_ownerId: driverObj._id,
					_passengerId: job.psg_id,
					_passengerJobDetail: job._id
				}).exec(function (err, exists) {
					if (!err && exists.length === 0) {
						HistoryModel.create({
							type: 'PASSENGER',
							status: success ? 'SUCCESSED' : 'CANCELLED',
							_ownerId: driverObj._id,
							_passengerId: job.psg_id,
							_passengerJobDetail: job._id
						}, function (err, history) {
							if (!err && history)
								console.log('history create success');
						});
					}
				});
			}
		});
	}
}



exports.driverEndTrip = function (socket) {
	return function (req, res) {
		// This API was called in the end of CC DPENDING ASSIGNED process
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		var jobsendby = "";
		//console.log('driverEndTrip')
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			function (err, drv) {
				if (drv.status == "ASSIGNED") {
					jobsendby = "CALLCENTER";
				}
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					saveHistory(drv, psg_id, true);
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = "";
					drv.job_id = "";
					drv.firstshift = false;
					drv.status = "ON";
					drv.updated = new Date().getTime();
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							if (drv.jobtype == "HOTELS") {
								JoblisthotelModel.findOne(
									{ _id: psg_id },
									function (err, psg) {
										if (psg == null) {
											res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
										} else {
											psg.updated = new Date().getTime();
											psg.deniedTaxiIds = [];
											psg.status = "THANKS";
											psg.datedrvdrop = new Date().getTime();
											psg.jobhistory.push({
												action: "Drv drop psghotel",
												status: "THANKS",
												reqbody: req.body,
												created: new Date()
											});											
											psg.save(function (err, response) {
												if (err) {
													res.json({ status: false });
												} else {
													if (psg.jobmode == "BROADCAST") {
														CallcenterpsgModel.findOne(
															{ _id: psg._id_callcenterpsg },
															function (err, ccpsg) {
																if (ccpsg == null) {
																	res.json({ status: false, msg: "Cannot find callcenterpsg data", data: err });
																} else {
																	//////////////////////
																	// pick hotel psg from BROADCAST
																	ccpsg.jobhistory.push({
																		action: "Drv drop psghotel",
																		status: "DROP",
																		actionby: req.body._id,
																		reqbody: req.body,
																		created: new Date()
																	});
																	/////////////////////////	
																	ccpsg.status = "FINISHED";	// if previous is depending, psg_status get data from input to function
																	ccpsg.updated = new Date().getTime();
																	ccpsg.save(function (err, result) {
																		if (err) {
																			res.json({ status: false, msg: "Cannot save pick hotelpsg data", data: err });
																		} else {
																			socket.of('/' + result.cgroup).emit("driverEndTask", result);
																			socket.emit("HotelSocketOn", response);
																			res.json({
																				status: true,
																				data: { status: response.status }
																			});
																		}
																	});
																}
															}
														);
													} else {
														socket.emit("HotelSocketOn", response);
														res.json({
															status: true,
															data: { status: response.status }
														});
													}
												}
											});
										}
									}
								);
							} else if (drv.jobtype == "QUEUE") {
								if (jobsendby == "CALLCENTER") {
									CallcenterpsgModel.findOne(
										{ _id: psg_id },
										function (err, ccenter) {
											if (ccenter == null) {
												res.json({ status: false, msg: "This  CC passenger  does not exist. Please check the information." });
											} else {
												ccenter.jobhistory.push({
													action: "Drv finished job",
													status: "FINISHED",
													actionby: req.body._id,
													reqbody: req.body,
													created: new Date()
												});
												ccenter.updated = new Date().getTime();
												ccenter.deniedTaxiIds = [];
												ccenter.status = "FINISHED";
												ccenter.save(function (err, response) {
													if (err) {
														res.json({ status: false });
													} else {
														socket.of('/' + response.cgroup).emit("driverEndTask", response);
														res.json({
															status: true,
															msg: "this is callcenter psg",
															data: { status: response.status }
														});
													}
												});
											}
										}
									);
								} else {
									res.json({
										status: true,
										msg: "this is related psg",
										data: { 
											status: result.status 
										}
									});
								}
							} else {
								PassengerModel.findOne(
									{ _id: psg_id },
									// { status:1, updated:1, job_id:1, _id:1, device_id:1, psgtype:1 },
									function (err, psg) {
										if (psg == null) {
											res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
										} else {
											passenger_ssid = psg.device_id;
											passenger_jobid = psg.job_id;
											taxi_duid = _id;
											if (psg.psgtype == "BROADCAST") {
												CallcenterpsgModel.findOne(
													{ _id: psg.job_id },
													function (err, psg) {
														if (psg == null) {
															DriversModel.findOne(
																{ _id: _id, device_id: device_id },
																function (err, drv) {
																	if (drv == null) {
																		res.json({ status: false, msg: "Drv info is missing (a)", data: err });
																		err
																	} else {
																		res.json({ status: false, msg: "Psg info is missing (b)", data: err });
																	}
																}
															);
														} else {
															//////////////////////
															// รับ psg
															psg.acceptedTaxiIds.push({
																device_id: device_id,
																drv_id: _id,
																pickupDate: new Date()
															});
															psg.jobhistory.push({
																action: "Drv drop psg",
																status: "DROP",
																actionby: _id,
																reqbody: req.body,
																created: new Date()
															});
															/////////////////////////	
															psg.status = "DROP";	// if previous is depending, psg_status get data from input to function
															psg.drv_id = _id;
															psg.updated = new Date().getTime();
															psg.save(function (err, result) {
																if (err) {
																	console.log(err)
																	res.json({ status: false, msg: "Cannot save passengers data", data: err });
																} else {
																	PassengerModel.findOne(
																		{ job_id: psg_id },
																		function (err, realpsg) {
																			if (realpsg == null) {
																				res.json({ status: false, msg: "This passenger is not available." });
																			} else {
																				realpsg.drv_id = _id;
																				realpsg.status = "PICK";
																				realpsg.updated = new Date().getTime();
																				realpsg.save(function (err, realpsgresult) {
																					if (err) {
																						res.json({ status: false, msg: "error", data: err });
																					} else {
																						//socket.of('/'+psg.cgroup).emit("gotdispatchaction", { response: response, psg_data: result } );		
																						socket.of('/' + result.cgroup).emit("driverEndTask", result);
																						res.json({
																							status: true,
																							data: {
																								status: response.status,
																								psg_id: response.psg_id,
																								jobtype: response.jobtype,
																								curaddr: psg.curaddr,
																								destination: psg.destination,
																								detail: psg.detail,
																								dassignedjob: psg.dassignedjob,
																								psgtype: "callcenter"
																							}
																						});
																					}
																				});
																			}
																		}
																	);
																}
															});
														}
													}
												);
											} else {
												if (psg.status == "ON") {
													//socket.emit("PassengerSocketOn", response);
													_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvdrop', '', curlng, curlat);
													res.json({
														status: true,
														msg: "",
														//data : { status: response.status }
													});
												} else {
													psg.drv_id = "";
													psg.job_id = "";
													psg.updated = new Date().getTime();
													psg.deniedTaxiIds = [];
													psg.status = "THANKS";
													psg.save(function (err, response) {
														if (err) {
															res.json({ status: false });
														} else {
															socket.emit("PassengerSocketOn", response);
															_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvdrop', '', curlng, curlat);
															_sub_samsungaction(passenger_ssid, passenger_jobid, taxi_duid, "THANKS", "drop", "PUT");
															res.json({
																status: true,
																data: { status: response.status }
															});
														}
													});
												}
											}
										}
									}
								);
							}
						}
					});
				}
			}
		);
	};
};




exports.driverEndTripHotel = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			{ status: 1, updated: 1, fname: 1, lname: 1, phone: 1, carplate: 1 },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
					if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
					if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
					drv.psg_id = "";
					drv.job_id = "";
					drv.status = "ON";
					drv.firstshift = false;
					drv.updated = new Date().getTime();
					drv.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							JoblisthotelModel.findOne(
								{ _id: psg_id },
								{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1 },
								function (err, psg) {
									if (psg == null) {
										res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
									} else {
										psg.updated = new Date().getTime();
										psg.deniedTaxiIds = [];
										psg.status = "THANKS";
										psg.datedrvdrop = new Date().getTime();
										psg.save(function (err, response) {
											if (err) {
												res.json({ status: false });
											} else {
												if (psg.jobmode == "BROADCAST") {
													CallcenterpsgModel.findOne(
														{ _id: psg._id_callcenterpsg },
														function (err, ccpsg) {
															if (ccpsg == null) {
																res.json({ status: false, msg: "Cannot find callcenterpsg data", data: err });
															} else {
																//////////////////////
																// pick hotel psg from BROADCAST
																ccpsg.jobhistory.push({
																	action: "Drv drop psghotel",
																	status: "DROP",
																	actionby: req.body._id,
																	reqbody: req.body,
																	created: new Date()
																});
																/////////////////////////	
																ccpsg.status = "FINISHED";	// if previous is depending, psg_status get data from input to function
																ccpsg.updated = new Date().getTime();
																ccpsg.save(function (err, result) {
																	if (err) {
																		console.log(err)
																		res.json({ status: false, msg: "Cannot save drop hotelpsg data", data: err });
																	} else {
																		socket.emit("HotelSocketOn", response);
																		res.json({
																			status: true,
																			data: { status: response.status }
																		});
																	}
																});
															}
														}
													);
												} else {
													socket.emit("HotelSocketOn", response);
													res.json({
														status: true,
														data: { status: response.status }
													});
												}
											}
										});
									}
								}
							);
						}
					});
				}
			}
		);
	};
};



exports.driverSendComment = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var commtype = "DRV";
	var topic = req.body.topic;
	var comment = req.body.comment;
	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		function (err, drivers) {
			if (drivers == null) {
				//res.json({ status: false , msg: "Your phone number does not exist, please register"});
				_driverAutoLogin(req, res);
			} else {
				CommentModel.create({
					commtype: commtype,
					user_id: _id,
					device_id: device_id,
					topic: topic,
					comment: comment
				},
					function (err, response) {
						if (err) {
							res.send(err)
						} else {
							// "ขอขอบคุณสำหรับคำแนะนำ/ความคิดเห็นของท่าน ทางเราจะนำมาพิจารณาเพื่อพัฒนาบริการให้ดียิ่งขึ้นต่อไป"
							_getErrDetail('err010', function (errorDetail) {
								res.json({
									status: true,
									msg: errorDetail
								});
							});
						}
					}
				);
			}
		}
	);
};




exports.driverBrokenAdd = function (req, res) {
	var form = new formidable.IncomingForm();
	var imgtype = "imgbroken";
	// processing
	form.parse(req, function (err, fields, files) {
		if (typeof files.upload == 'undefined' && files.upload == null) {
			console.log("fail upload ! ; no image input")
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else {
				_getErrDetail('err004', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		} else {
			// check image type
			if (files.upload.type == 'image/jpeg' || files.upload.type == 'image/jpg' || files.upload.type == 'image/png' || files.upload.type == 'application/octet-stream') {
				_id = fields._id;
				device_id = fields.device_id;
				DriversModel.aggregate(
					{ $match: { _id: new mongoose.Types.ObjectId(_id) } },   // Aggregate not accept normal _id, has to convert type
					{ $project: { _id: 0, brokenpicCount: { $size: "$brokenpicture" } } },
					function (err, result) {
						xcount = result[0].brokenpicCount
						//console.log(result)
						/*
						res.json({ 
							status: true, 	
							msg: result,
							count: result[0].brokenpicCount
						});
						*/
						if (xcount == 4) {
							res.json({
								status: false,
								msg: "Max upload images is 4"
							});
						} else {
							findandUpload(_id, device_id)
						}
					}
				)
			} else {
				// kick out
				//res.json({ status: false , msg: "Invalid image type, Please input type JPEG or PNG" });
				_getErrDetail('err005', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			}
		}
	});

	function findandUpload(drvId, deviceId) {
		DriversModel.findOne(
			{ _id: drvId, device_id: deviceId },
			{ device_id: 1, brokenpicture: 1 },
			function (err, response) {
				if (response == null) {
					_driverCheckLogin(deviceId, drvId, res);
				} else {
					if (form) {
						var files = form.openedFiles;
						if (files) {
							var file = files[0];
							if (file) {
								var temp_path = file.path;
								var extension = path.extname(file.name);
								var new_location = '../upload_brokens/';
								newimgupload = drvId + '_' + imgtype + '_' + ranSMS() + extension;
								arrpix = response.brokenpicture;
								arrpix.push(newimgupload);			// push arrary at the last
								fs.copy(temp_path, new_location + newimgupload, function (err) {
									if (err) {
										console.log(err);
									} else {
										console.log("success upload ! a broken image > name : " + newimgupload)
										DriversModel.findOne(
											{ _id: drvId },
											{ status: 1, updated: 1 },
											function (err, drvupfile) {
												drvupfile.brokenpicture = arrpix;
												drvupfile.updated = new Date().getTime();
												drvupfile.save(function (err, response) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														res.json({
															//msg: "success, your image has been uploaded.",
															status: true,
															data: { imgname: newimgupload }
														});
													}
												});
											}
										);
									}
								});
							}
						}
					} else {
						console.log("do not have form")
						res.json({ status: false, msg: "no form" });
					}
				}
			}
		);
	}
};




exports.driverBrokenEdit = function (req, res) {
	var form = new formidable.IncomingForm();
	var imgtype = "imgbroken";
	var oldimg = "";
	// processing
	form.parse(req, function (err, fields, files) {
		if (typeof files.upload == 'undefined' && files.upload == null) {
			console.log("fail upload ! ; no image input")
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else {
				//res.json({status: false , msg : 'Please put some images.'});
				_getErrDetail('err004', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		} else {
			// check image type
			if (files.upload.type == 'image/jpeg' || files.upload.type == 'image/jpg' || files.upload.type == 'image/png' || files.upload.type == 'application/octet-stream') {
				_id = fields._id;
				device_id = fields.device_id;
				oldimg = fields.oldimg;
				findandUpload(_id, device_id);
			} else {
				// kick out
				//res.json({ status: false , msg: "Invalid image type, Please input type JPEG or PNG" });
				_getErrDetail('err005', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		}
	});

	function findandUpload(drvId, deviceId) {
		DriversModel.findOne(
			{ _id: drvId, device_id: deviceId },
			{ device_id: 1, brokenpicture: 1 },
			function (err, response) {
				if (response == null) {
					_driverCheckLogin(deviceId, drvId, res);
				} else {
					if (form) {
						var files = form.openedFiles;
						if (files) {
							var file = files[0];
							if (file) {
								var temp_path = file.path;
								var extension = path.extname(file.name);
								var new_location = '../upload_brokens/';

								var filePath = '../upload_brokens/' + oldimg;
								fs.exists(filePath, function (exists) {
									if (exists) {
										fs.unlinkSync(filePath);
									}
								});

								newimgupload = drvId + '_' + imgtype + '_' + ranSMS() + extension;
								arrpix = response.brokenpicture;
								arrpix[arrpix.indexOf(oldimg)] = newimgupload;

								fs.copy(temp_path, new_location + newimgupload, function (err) {
									if (err) {
										console.log(err);
									} else {
										console.log("success upload !")
										DriversModel.findOne(
											{ _id: drvId },
											{ status: 1, updated: 1 },
											function (err, drvupfile) {
												drvupfile.brokenpicture = arrpix;
												drvupfile.updated = new Date().getTime();
												drvupfile.save(function (err, response) {
													if (err) {
														res.json({ status: false, msg: "error", data: err });
													} else {
														res.json({
															//msg: "success, your image has been replaced." ,
															status: true,
															data: { imgname: newimgupload }
														});
													}
												});
											}
										);
									}
								});
							}
						}
					} else {
						console.log("do not have form")
						res.json({ status: false, msg: "no form" });
					}
				}
			}
		);
	}
};




exports.driverBrokenDel = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var oldimg = req.body.oldimg;
	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ device_id: 1, brokenpicture: 1 },
		function (err, response) {
			if (response == null) {
				_driverAutoLogin(req, res);
			} else {
				arrpix = response.brokenpicture;
				imgorder = arrpix.indexOf(oldimg)
				if (imgorder > 0) {
					arrpix.splice(imgorder, 1);	// delete array at immgorder	
				}
				DriversModel.findOne(
					{ _id: _id },
					{ status: 1, updated: 1 },
					function (err, drv) {
						drv.brokenpicture = arrpix;
						drv.updated = new Date().getTime();
						drv.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error", data: err });
							} else {
								var fs = require('fs');
								var filePath = '../upload_brokens/' + oldimg;
								fs.exists(filePath, function (exists) {
									if (exists) {
										fs.unlinkSync(filePath);
									}
								});
								//msg: "success, your image has been deleted."
								res.json({
									status: true
								});
							}
						});
					}
				);
			}
		}
	);
};




exports.driverBrokenReport = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ status: 1, updated: 1 },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
			} else {
				if (req.body.brokenname) { drv.brokenname = req.body.brokenname ? req.body.brokenname : drv.brokenname; }
				if (req.body.brokendetail) { drv.brokendetail = req.body.brokendetail ? req.body.brokendetail : drv.brokendetail; }
				drv.status = "BROKEN";
				drv.updated = new Date().getTime();
				drv.save(function (err, response) {
					if (err) {
						res.json({
							status: false,
							msg: "Please input latitude and longitude.",
							data: err
						});
					} else {
						res.json({
							//msg: "success, your broken report has benn sent." 
							status: true,
							data: { status: response.status }
						});
					}
				});
			}
		}
	);
};




exports.driverBrokenCancel = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ device_id: 1, brokenpicture: 1 },
		function (err, response) {
			if (response == null) {
				_driverAutoLogin(req, res);
			} else {
				arrpix = response.brokenpicture;
				var i;
				for (i = 0; i < arrpix.length; i++) {
					var filePath = '../upload_brokens/' + arrpix[i];
					// fs.removeSync(filePath);
					// fs.exists(filePath, function(exists) {	
					// 	console.log('filePath2 = '+filePath)	
					// 	console.log('exists='+exists)
					// 	if (exists) {
					// 		console.log('exists2='+exists)
					// 		fs.removeSync(filePath);
					// 	}
					// });

					if (fs.existsSync(filePath)) {
						fs.removeSync(filePath);
					}
				}
				DriversModel.findOne(
					{ _id: _id, device_id: device_id },
					{ status: 1, updated: 1 },
					function (err, drv) {
						drv.brokenname = [];
						drv.brokendetail = "";
						drv.brokenpicture = [];
						drv.status = "ON";
						drv.updated = new Date().getTime();
						drv.save(function (err, result) {
							if (err) {
								res.json({ status: false, msg: "error", data: err });
							} else {
								res.json({
									//msg: "success, your car is back to available." 
									status: true,
									data: { status: result.status }
								});
							}
						});
					}
				);
			}
		}
	);
};




exports.drivertestSocket = function (req, res) {
	var io = require('socket.io').listen(80);

	io.sockets.on('connection', function (socket) {
		socket.emit('news', { hello: 'world' });
		socket.on('my other event', function (data) {
			//console.log(data);
		});
	});

	res.json({
		//msg: "success, your car is back to available." 
		status: true,
		msg: ""
	});
};




exports.drivergotmsg = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		{ device_id: 1 },
		function (err, drv) {
			if (drv == null) {
				_driverAutoLogin(req, res);
			} else {
				drv.msgstatus = "OLD";
				drv.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						res.json({
							status: true,
							data: { status: drv.status }
						});
					}
				});
			}
		}
	);
};




exports.gotdispatchaction = function (socket) {
	return function (req, res) {
		var device_id = req.body.device_id;
		var drv_id = req.body._id;
		var psg_id = req.body.passengerID;
		var drv_action = req.body.drv_action; // { yes / no }
		//console.log(' psg_id  =' + psg_id)
		switch (drv_action) {
			case "Y":
				updategotdispatch(psg_id, device_id, drv_id, "ASSIGNED", "ASSIGNED");
				break;

			case "N":
				updategotdispatch(psg_id, device_id, drv_id, "DEPENDING_REJECT", "ON");
				break;

			case "T":
				updategotdispatch(psg_id, device_id, drv_id, "DEPENDING_TIMEOUT", "ON");
				break;

			case "R": 		// = read
				updatereadmsg(drv_id, device_id);
				break;
		}
		//console.log(req.body)
		// user CallcenterpsgModel => จ่ายง่าน ผ่าน call center 

		function updatereadmsg(drv_id, device_id) {
			DriversModel.findOne(
				{ _id: drv_id, device_id: device_id },
				function (err, drv) {
					if (drv == null) {
						res.json({ status: false, msg: "Driver is missing", data: err });
					} else {
						drv.msgstatus = "OLD";
						drv.datereadmsg = new Date().getTime();
						drv.updated = new Date().getTime();
						drv.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "Cannot save drivers data", data: err });
							} else {
								//socket.emit("gotdispatchaction", response);      
								res.json({
									status: true,
									data: {
										status: response.status,
										psg_id: response.psg_id,
										msgphone: response.msgphone,
										msgnote: response.msgnote,
										msgstatus: response.msgstatus,
										jobtype: response.jobtype
									}
								});
							}
						});
					}
				}
			);
		}

		function updategotdispatch(psg_id, device_id, drv_id, psg_status, drv_status) {
			CallcenterpsgModel.findOne(
				{ _id: psg_id },
				function (err, psg) {
					if (psg == null) {
						DriversModel.findOne(
							{ _id: drv_id, device_id: device_id },
							function (err, drv) {
								if (drv == null) {
									res.json({ status: false, msg: "Driver is missing", data: err });
									err
								} else {
									drv.psg_id = psg_id;
									drv.msgnote = "";
									drv.msgphone = "";
									drv.msgstatus = "OLD";
									drv.updated = new Date().getTime();
									drv.save(function (err, response) {
										if (err) {
											res.json({ status: false, msg: "Cannot save drivers data", data: err });
										} else {
											console.log(' updategotdispatch 1 no psg_id not matched ')
											//socket.emit("gotdispatchaction", response);  
											console.log(response)
											res.json({
												status: true,
												msg: "Passenger ID is missing.",
												data: {
													status: response.status,
													psg_id: response.psg_id,
													msgphone: response.msgphone,
													msgnote: response.msgnote,
													msgstatus: response.msgstatus,
													jobtype: response.jobtype
												}
											});
										}
									});
								}
							}
						);
					} else {
						if (drv_id == psg.drv_id) {	// recheck ว่า drv_id ของคนรับ กับ drv_id ของงานที่จ่ายตรงกันมั๊ย?
							if (drv_action == "N") {
								psg.deniedTaxiIds.push({
									device_id: req.body.device_id,
									drv_id: req.body._id,
									deniedDate: new Date()
								});
								psg.jobhistory.push({
									action: "Drv denied job",
									status: "DEPENDING_REJECT",
									actionby: req.body._id,
									reqbody: req.body,
									created: new Date()
								});
							}
							//////////////////////
							// เวลาหมด
							if (drv_action == "T") {
								psg.deniedTaxiIds.push({
									device_id: req.body.device_id,
									drv_id: req.body._id,
									deniedDate: new Date()
								});
								psg.jobhistory.push({
									action: "job timeout",
									status: "DEPENDING_TIMEOUT",
									actionby: req.body._id,
									reqbody: req.body,
									created: new Date()
								});
							}
							//////////////////////
							// รับงาน
							if (drv_action == "Y") {
								psg.acceptedTaxiIds.push({
									device_id: req.body.device_id,
									drv_id: req.body._id,
									acceptedDate: new Date()
								});
								psg.jobhistory.push({
									action: "Drv accepted job for direct job",
									status: "ASSIGNED",
									actionby: req.body._id,
									reqbody: req.body,
									created: new Date()
								});
							}
							/////////////////////////
							if (psg.status == "DPENDING") {
								psg.status = psg_status;	// if previous is depending, psg_status get data from input to function
								psg.drv_id = drv_id;
							} else {
								// งานถูกลบก่อนกดยกเลิก หรือยอมรับ
								psg_id = "";
								drv_status = "ON";
							}
							//psg.nccdpendingcount = psg.nccdpendingcount + 1;
							psg.updated = new Date().getTime();
							psg.datereadmsg = new Date().getTime();
							psg.dassignedjob = new Date().getTime();
							psg.save(function (err, result) {
								if (err) {
									console.log(err)
									res.json({ status: false, msg: "Cannot save passengers data", data: err });
								} else {
									//console.log('xxxxxxx')
									DriversModel.findOne(
										{ _id: drv_id, device_id: device_id },
										function (err, drv) {
											if (drv == null) {
												res.json({ status: false, msg: "Driver is missing", data: err });
											} else {
												drv.psg_id = psg_id;
												drv.msgstatus = "OLD";
												drv.datereadmsg = new Date().getTime();
												drv.updated = new Date().getTime();
												drv.status = drv_status;
												drv.save(function (err, response) {
													if (err) {
														console.log(err)
														res.json({ status: false, msg: "Cannot save drivers data", data: err });
													} else {
														console.log(' updategotdispatch 2  match psg_id ')
														console.log(' drv_action = ' + drv_action)
														socket.of('/' + psg.cgroup).emit("gotdispatchaction", { response: response, psg_data: result });
														//console.log(result)
														console.log(response.status)
														res.json({
															status: true,
															data: {
																firstshift: response.firstshift,
																status: response.status,
																psg_id: response.psg_id,
																msgphone: response.msgphone,
																msgnote: response.msgnote,
																msgstatus: response.msgstatus,
																jobtype: response.jobtype,
																curaddr: psg.curaddr,
																destination: psg.destination,
																detail: psg.detail,
																dassignedjob: psg.dassignedjob,
																curlat: psg.curlat,
																curlng: psg.curlng,
																curloc: psg.curloc,
																deslat: psg.deslat,
																deslng: psg.deslng,
																desloc: psg.desloc,
																psgtype: "callcenter"
															}
														});
													}
												});
											}
										}
									);
								}
							});
						} else {
							res.json({ status: false, msg: "Cannot save drivers data", data: err });
						}
					}
				}
			);
		}
	}
}



exports.driverEndTask = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.passengerID;
		var jobsendby;
		DriversModel.findOne(
			{ _id: _id, device_id: device_id },
			function (err, drv) {
				if (drv == null) {
					//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
					_driverAutoLogin(req, res);
				} else {
					console.log("drv.status1 = " + drv.status)
					if (drv.status == "ASSIGNED") {
						jobsendby = "CALLCENTER";
					}
					drv.psg_id = "";
					drv.status = "ON";
					drv.firstshift = false;
					drv.updated = new Date().getTime();
					drv.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							console.log("drv.status2 = " + drv.status)
							if (jobsendby == "CALLCENTER") {
								CallcenterpsgModel.findOne(
									{ _id: psg_id },
									function (err, ccenter) {
										if (ccenter == null) {
											res.json({ status: false, msg: "This  CC passenger  does not exist. Please check the information." });
										} else {
											ccenter.jobhistory.push({
												action: "Drv finished job",
												status: "FINISHED",
												actionby: req.body._id,
												reqbody: req.body,
												created: new Date()
											});
											ccenter.updated = new Date().getTime();
											ccenter.deniedTaxiIds = [];
											ccenter.status = "FINISHED";
											ccenter.save(function (err, response) {
												if (err) {
													res.json({ status: false });
												} else {
													socket.of('/' + response.cgroup).emit("driverEndTask", response);
													res.json({
														status: true,
														msg: "this is callcenter psg",
														data: { status: response.status }
													});
												}
											});
										}
									}
								);
							} else {
								PassengerModel.findOne(
									{ _id: psg_id },
									function (err, psg) {
										if (psg == null) {
											res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
										} else {
											psg.drv_id = "";
											psg.updated = new Date().getTime();
											psg.deniedTaxiIds = [];
											psg.status = "OFF";
											psg.save(function (err, response) {
												if (err) {
													res.json({ status: false });
												} else {
													//socket.emit("driverEndTask", response);
													res.json({
														status: true,
														msg: "this is app psg",
														data: { status: response.status }
													});
												}
											});
										}
									}
								);
							}
						}
					});
				}
			}
		);
	};
}




exports.driverGetParkingQueue = function (req, res) {
	var _id_garage = req.body._id_garage;
	var cgroup = req.body.cgroup;
	var parkinglot = req.body.parkinglot;
	var _id_parkinglot = req.body._id_parkinglot;
	var drv_id = req.body.drv_id;
	ParkingqueueModel.count(
		{ _id_garage: _id_garage, _id_parkinglot: _id_parkinglot, drv_id: drv_id },
		function (err, result) {
			if (err) {
				res.json({ status: false, data: err });
			} else {
				if (result == 0) {
					res.json({
						status: false
					});
				} else {
					res.json({
						status: true,
						totalqueue: result
					});
				}
			}
		}
	);
};




exports.driverBookParkingQueue = function (req, res) {

};




exports.driverLeftParkingQueue = function (req, res) {

};



exports.checksamecarplate = function (req, res) {
	res.json({
		status: true,
		msg: "You can use the application"
	});
}




exports.checksameaccount = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var cgroup = req.body.cgroup;
	var carplate = req.body.carplate;
	var appversion = req.body.appversion;
	//console.log('checksameaccount')
	DriversModel.findOne(
		{ _id: _id },
		function (err, drv) {
			if (drv == null) {
				//console.log('checksameaccount : err001')
				res.json({
					status: false,
					errcode: "err001",
					msg: "No id"
				});
			} else {
				if (drv.device_id == device_id) {
					// same user account and same device //
					if (drv.status == "OFF") {
						drv.status = "ON";
					}
					drv.appversion = appversion;
					drv.updated = new Date().getTime();
					drv.save(function (err, response) {
						if (err) {
							//console.log('checksameaccount : err002')
							res.json({
								status: false,
								errcode: "err002",
								msg: err
							});
						} else {
							DriversModel.find(
								{ cgroup: drv.cgroup, prefixcarplate: drv.prefixcarplate, carplate: drv.carplate, _id: { $ne: _id }, status: { $nin: ["OFF", "BROKEN", "APPROVED", "DELETED", "PENDING"] }, _id_garage: { $exists: true } },
								{ fname: 1, lname: 1, carplate: 1, phone: 1, status: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, appversion: 1 },
								function (err, result) {
									if (result == 0) {
										var dataDriver = DriversModel.findOne(
											{ _id: _id, _id_garage: { $exists: true } },
											{
												_id: 1, device_id: 1, username: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1,
												fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, address: 1,
												province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
												imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
											}
										);
										dataDriver.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });
										dataDriver.exec(function (err, resultdata) {
											if (resultdata == null) {

											} else {
												var myToken = jwt.sign({
													_id: req.body._id
												}, 'myScottSummer', {
														algorithm: 'HS256',
														expiresIn: 604800		// in second = 7 days
													});
												res.json({
													status: true,
													myToken,
													msg: "You can use this application : step 01",
													data: resultdata
												});
											}
										});
									} else {
										drv.status = "OFF";
										drv.updated = new Date().getTime();
										drv.save(function (err, response) {
											if (err) {
												res.json({
													status: false,
													msg: err
												});
											} else {
												console.log('checksameaccount : err003')
												res.json({
													status: false,
													errcode: "err003",
													msg: "ขออภัยค่ะ ระบบไม่สามารถให้บริการได้ เนื่องจากมีผู้ใช้รถเลขทะเบียนเดียวกันกับคุณ กำลังเปิดใช้งานอยู่ หากประส่งค์จะใช้บริการ กรุณาติดต่อผู้ใช้ตามรายละเอียดนี้เพื่อออกจากระบบ"
												});
											}
										});
									}
								}
							);
						}
					});
				} else {
					// same user account but different device
					if (drv.status == "OFF") {
						//console.log('checksameaccount.drv.status.OFF')
						drv.appversion = appversion;
						drv.device_id = device_id;
						drv.status = "ON";
						drv.updated = new Date().getTime();
						drv.save(function (err, response) {
							if (err) {
								//console.log('checksameaccount : err004')
								res.json({
									status: false,
									errcode: "err004",
									msg: err
								});
							} else {
								var dataDriver1 = DriversModel.findOne(
									{ _id: _id, device_id: device_id, _id_garage: { $exists: true } },
									{
										_id: 1, device_id: 1, username: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, citizenid: 1,
										fname: 1, lname: 1, allowpsgcontact: 1, taxiID: 1, phone: 1, carplate: 1, prefixcarplate: 1, address: 1,
										province: 1, district: 1, tambon: 1, zipcode: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1,
										imgface: 1, imglicence: 1, imgcar: 1, status: 1, carreturn: 1, carreturnwhere: 1, carprovince: 1, carprovinceid: 1, _id_garage: 1
									}
								);
								dataDriver1.populate('_id_garage', { drvsearchpsgradian: 1, drvsearchpsgamount: 1, ProvinceID: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, ccwaitingtime: 1 });
								dataDriver1.exec(function (err, resultdata) {
									if (resultdata == 0) {
										//console.log('checksameaccount : err005')
										res.json({
											status: false,
											errcode: "err005",
											msg: err
										});
									} else {
										var myToken = jwt.sign({
											_id: req.body._id
										}, 'myScottSummer', {
												algorithm: 'HS256',
												expiresIn: 604800		// in second = 7 days
											});
										res.json({
											status: true,
											myToken,
											msg: "You can use the application : step 02",
											data: resultdata
										});
									}
								}
								);
							}
						});
					} else {
						//console.log('checksameaccount : err006')
						res.json({
							status: false,
							errcode: "err006",
							msg: "ขออภัยค่ะระบบไม่สามารถให้บริการได้  เนื่องจากมีการใช้งานด้วยชื่อผู้ใช้เดียวกันนี้ที่อุปกรณ์อื่น หากมีความประสงค์จะใช้งาน กรุณาปิดการใช้งานที่อุปกรณ์ก่อนหน้านี้"
						});
					}
				}
			}
		}
	);
}




exports.getDriverHistoryApp = function (req, res) {
	var _id = req.body._id;
	JoblistModel.find(
		{ drv_id: _id },
		{ ProvinceID: 1, NameEN: 1, NameTH: 1, job_id: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, cgroup: 1, curaddr: 1, destination: 1, tips: 1, detail: 1, createdvia: 1, curloc: 1, datepsgcall: 1, datedrvwait: 1, datepsgaccept: 1, datedrvpick: 1, datedrvdrop: 1, datepsgcancel: 1, datedrvcancel: 1 },
		{ sort: { 'datepsgcall': -1 } },
		function (err, job) {
			if (job == 0) {
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail('err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "History detail",
					data: job
				});
			}
		}
	);
};




exports.getDriverHistoryCC = function (req, res) {
	var drv_carplate = req.body.drv_carplate;
	CallcenterpsgModel.find(
		{ drv_carplate: drv_carplate },
		{ phone: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, cgroup: 1, curaddr: 1, destination: 1, tips: 1, detail: 1, createdvia: 1, createdjob: 1, dassignedjob: 1, dpendingjob: 1, status: 1 },
		{ sort: { 'createdjob': -1 } },
		function (err, job) {
			if (job == 0) {
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail('err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "History detail",
					data: job
				});
			}
		}
	);
};




exports.getPassengerCallHistory = function (req, res) {
	var _id = req.body._id;
	PsgcalllogModel.find(
		{ drv_id: _id },
		{},
		{ sort: { 'datepsgcall': -1 } },
		function (err, job) {
			if (job == 0) {
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail('err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "Call history detail",
					data: job
				});
			}
		}
	);
};




exports.getAllCityGarage = function (req, res) {
	// example: http://stackoverflow.com/questions/22932364/mongodb-group-values-by-multiple-fields
	// plus : http://www.mkyong.com/mongodb/mongodb-aggregate-and-group-example/
	var myquery ;

	// if(checkSmilesAPP){
	// 	myquery = { $match: { "active": 'Y', "cgroup" : "bkksmileapp", }};
	// } else {
	// 	myquery = { $match: { "active": 'Y', "cgroup": { $ne: "bkksmileapp" }}};
	// }

	if(req.url.substring(1,7)=='smiles'){		
		myquery = { $match: { "active": 'Y', _id: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c') }};
	} else {
		myquery = { $match: { "active": 'Y', _id: { $ne: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c')} }};
	}

	Lk_garageModel.aggregate([
		myquery,
		{
			"$group": {
				"_id": {
					"_id": "$_id",
					"ProvinceID": "$ProvinceID",
					"province": "$province",
					"cgroup": "$cgroup",
					"cgroupname": "$cgroupname",
					"cprovincename": "$cprovincename",
					"ccwaitingtime": "$ccwaitingtime"
				},
				"grageCount": { "$sum": 1 },
			}
		},
		{
			"$group": {

				"_id": {
					"provinceid": "$_id.ProvinceID",
					"province": "$_id.province"
				},
				"garagelist": {
					"$push": {
						"_id": "$_id._id",
						"cgroup": "$_id.cgroup",
						"cgroupname": "$_id.cgroupname",
						"cprovincename": "$_id.cprovincename",
						"ccwaitingtime": "$_id.ccwaitingtime"
					},
				},
				"grageCount": { "$sum": "$grageCount" }
			}
		},
		{ "$sort": { "grageCount": -1 } }
	], function (err, result) {
		if (err) {
			res.json({ status: false, data: err });
		} else {
			res.json({
				status: true,
				data: result
			});
		}
	});
};



exports.getHistoryList = function (req, res) {
	if (!req.body.driver_id) {
		res.json({ status: false, mgs: 'error request body id not found.' });
		return;
	}

	if (req.body.last_history_id) {
		HistoryModel.findOne({ _id: req.body.last_history_id }).select({ created: 1 }).exec(function (err, lastHistory) {
			if (!err && lastHistory) {
				HistoryModel
					.find({ _ownerId: req.body.driver_id, created: { "$lt": new Date(lastHistory.created) } })
					.populate('_passengerJobDetail _callcenterId', 'curaddr destination created')
					.sort({ created: -1 })
					.limit(10)
					.exec(function (err, result) {

						if (result) {
							res.json({ status: true, data: result });
						}
					});
			} else if (err) {
				res.json({ status: false, msg: err });
			} else if (!result) {
				res.json({ status: false, msg: 'data is null' });
			} else {
				res.json({ status: false, msg: 'internal server error' });
			}
		});
	} else {
		HistoryModel.find({ _ownerId: req.body.driver_id }).populate('_passengerJobDetail _callcenterId', 'curaddr destination created').sort({ created: -1 }).limit(10).exec(function (err, result) {
			if (!err && result) {
				if (result) {
					res.json({ status: true, data: result });
				}
			}
			return;
		});
	}
}


exports.getHistoryDetail = function (req, res) {
	if (!req.body.id || !req.body.type) {
		res.json({ status: false, mgs: 'error request body id or type not found.' });
		return;
	}
	if (req.body.type === 'CALLCENTER') {
		CallcenterpsgModel.findById(req.body.id).exec(function (err, result) {
			if (!err && result) {
				res.json({ status: true, data: result });
			} else {
				res.json({ status: false, data: "data not found." });
			}
		});
	} else if (req.body.type === 'PASSENGER') {
		JoblistModel.findById(req.body.id).exec(function (err, result) {
			if (!err && result) {
				res.json({ status: true, data: result });
			} else {
				res.json({ status: false, data: "data not found." });
			}
		});
	}
}




exports.msghistory = function (req, res) {
	var baseUrl = req.protocol + '://' + req.get('host');

	MsghistoryModel.find(
		{ drv_phone: req.params.phone },
		{ phone: 1, cur: 1, des: 1, created: 1 },
		{ sort: { 'created': -1 } }).limit(5).exec(function (err, result) {
			if (result == 0) {
				res.send('Your phone is ' + req.params.phone);
			} else {
				res.render('msghistory.ejs',
					{
						baseUrl: baseUrl,
						status: true,
						data: result
					}
				);
			}
		}
		)
}




exports.updateNotification = (io) => {
	return (req, res) => {
		DriversModel.update(

			{ '_id': req.body.id, 'notifications.ref': { '$in': [req.body.notification.ref_id] } },

			{ '$set': { 'notifications.$.state': req.body.notification.status } },

			(err) => {
				if (!err) res.json({
					status: true
				});
			});
	}
}




exports.syncUpNotification = (req, res) => {

	const _ = require('underscore');

	DriversModel.findOne({ _id: req.body.id }).populate('notifications.ref', 'title message image_url type uid created').exec((err, driver) => {

		if (err) return res.json({ status: false, data: err });

		let notifications = [];
		let notificationUpdated = [];

		driver.notifications.forEach((dataItem, dataIndex) => {

			if (dataItem) {

				let d = dataItem.ref.toJSON();
				d.ref_id = d._id.toString();
				let created = moment(d.created).format('DD/MM/YYYY HH:mm');
				delete d.created;
				d.created = created;
				d.status = "UNREAD";

				let noti;

				req.body.notificationList.find((reqItem, reqIndex) => {
					if (dataItem.ref._id.toString() === reqItem.ref_id) {
						d.status = reqItem.status;
						noti = {
							state: reqItem.status,
							ref: dataItem.ref._id
						};
					} else {
						noti = {
							state: dataItem.state,
							ref: dataItem.ref._id
						};
					}
				});

				if (noti) {
					notifications.push(noti);
				}
				notificationUpdated.push(d);
			}
		});

		if (notifications.length > 0) {
			driver.notifications = notifications;
			driver.save();
		}

		return res.json({ status: true, data: notificationUpdated });
	});
}




exports.drvbuyashift = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;

	DriversModel.findOne(
		{ _id: _id, device_id: device_id },
		function (err, drv) {
			if (drv == null) {
				res.json({
					status: false,
					errcode: "drvbuyashift err004",
					msg: "No id"
				});
			} else {
				//if (drv.drv_wallet > config.MoneyPerUseAPP) {
					var Preshift = drv.drv_shift;
					var Postshift = Preshift + 1;
					var PreAmount = drv.drv_wallet;	
					var PostAmount = PreAmount - config.MoneyPerUseAPP;				
					drv.drv_shift = Postshift ;
					//drv.drv_wallet = PostAmount ;
					drv.save(function (err, response) {
						if (err) {
							res.json({
								status: false,
								errcode: "drvbuyashift err004",
								msg: err
							});
						} else {
							BuyshifttransactionModel.create({
								_id_driver: _id,
								PreAmount: PreAmount,
								PostAmount: PostAmount,
								Amount: config.MoneyPerUseAPP,
								Preshift: Preshift,
								Postshift: Postshift,
								TimePerUseAPP: config.TimePerUseAPP,
								MoneyPerUseAPP: config.MoneyPerUseAPP,
								Action: "Buy",
								Actiondate: new Date()
							},
								function (err, response) {
									if (err) {
										res.json({ status: false, msg: err, data: err });
									} else {
										res.json({
											status: true,											
											msg: "คุณได้เติมรอบของ Taxi-Beam สำเร็จแล้ว"
										});
									}
								}
							);
						}
					});
				// } else {
				// 	res.json({
				// 		status: false,
				// 		msg: "คุณมีเงินเหลือใน wallet ไม่เพียงพอต่อการซื้อรอบ",
				// 		data: {
				// 			errcode: "NOMONEY"
				// 		}
				// 	});
				// }
			}
		}
	);
}




//////////////////////////////////////////////
var MD5 = function (string) {

	function RotateLeft(lValue, iShiftBits) {
		return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
	}

	function AddUnsigned(lX, lY) {
		var lX4, lY4, lX8, lY8, lResult;
		lX8 = (lX & 0x80000000);
		lY8 = (lY & 0x80000000);
		lX4 = (lX & 0x40000000);
		lY4 = (lY & 0x40000000);
		lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
		if (lX4 & lY4) {
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}
		if (lX4 | lY4) {
			if (lResult & 0x40000000) {
				return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
			} else {
				return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
			}
		} else {
			return (lResult ^ lX8 ^ lY8);
		}
	}

	function F(x, y, z) { return (x & y) | ((~x) & z); }
	function G(x, y, z) { return (x & z) | (y & (~z)); }
	function H(x, y, z) { return (x ^ y ^ z); }
	function I(x, y, z) { return (y ^ (x | (~z))); }

	function FF(a, b, c, d, x, s, ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function GG(a, b, c, d, x, s, ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function HH(a, b, c, d, x, s, ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function II(a, b, c, d, x, s, ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function ConvertToWordArray(string) {
		var lWordCount;
		var lMessageLength = string.length;
		var lNumberOfWords_temp1 = lMessageLength + 8;
		var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
		var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
		var lWordArray = Array(lNumberOfWords - 1);
		var lBytePosition = 0;
		var lByteCount = 0;
		while (lByteCount < lMessageLength) {
			lWordCount = (lByteCount - (lByteCount % 4)) / 4;
			lBytePosition = (lByteCount % 4) * 8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
			lByteCount++;
		}
		lWordCount = (lByteCount - (lByteCount % 4)) / 4;
		lBytePosition = (lByteCount % 4) * 8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
		lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
		lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
		return lWordArray;
	};

	function WordToHex(lValue) {
		var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
		for (lCount = 0; lCount <= 3; lCount++) {
			lByte = (lValue >>> (lCount * 8)) & 255;
			WordToHexValue_temp = "0" + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
		}
		return WordToHexValue;
	};

	function Utf8Encode(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	};

	var x = Array();
	var k, AA, BB, CC, DD, a, b, c, d;
	var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
	var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
	var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
	var S41 = 6, S42 = 10, S43 = 15, S44 = 21;

	string = Utf8Encode(string);

	x = ConvertToWordArray(string);

	a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

	for (k = 0; k < x.length; k += 16) {
		AA = a; BB = b; CC = c; DD = d;
		a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
		d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
		c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
		b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
		a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
		d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
		c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
		b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
		a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
		d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
		c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
		b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
		a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
		d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
		c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
		b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
		a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
		d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
		c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
		b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
		a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
		d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
		c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
		b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
		a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
		d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
		c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
		b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
		a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
		d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
		c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
		b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
		a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
		d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
		c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
		b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
		a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
		d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
		c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
		b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
		a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
		d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
		c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
		b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
		a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
		d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
		c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
		b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
		a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
		d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
		c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
		b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
		a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
		d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
		c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
		b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
		a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
		d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
		c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
		b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
		a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
		d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
		c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
		b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
		a = AddUnsigned(a, AA);
		b = AddUnsigned(b, BB);
		c = AddUnsigned(c, CC);
		d = AddUnsigned(d, DD);
	}

	var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);

	return temp.toLowerCase();
}
//////////////////////////////////////////////