////////////////////////////////////
// Taxi-Beam Hotel API 
// version : 1.0.0
// Date May 10, 2016 
// Created by Hanzen@BRET
////////////////////////////////////

var config = require('../../config/func').production;

var Url = require('url');
var mongoose = require('mongoose');
var HotelsModel = mongoose.model('HotelsModel');
var JoblisthotelModel = mongoose.model('JoblisthotelModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var UserModel = mongoose.model('UserModel');
var DriversModel = mongoose.model('DriversModel');
var CommentModel = mongoose.model('CommentModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var EmgListModel = mongoose.model('EmgListModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var POIsModel = mongoose.model('POIsModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');
var PoirecommendedModel = mongoose.model('PoirecommendedModel');
var randomIntArray = require('random-int-array');


require('mongoose').Query.prototype.random = function (count, unique, callback) {
	var query = this;

	return query.count(function (err, max) {
		if (err || (max <= 0)) {
			callback(err)
			return;
		}
		var docNumbers = randomIntArray({ count: count, max: max, unique: unique });
		var docs = [];
		var promises = docNumbers.map(function (n, index) {
			return new Promise(function (resolve, reject) {
				query.model.find(query._conditions).skip(n).limit(-1).exec(function (err, doc) {
					if (err) return reject(err);
					docs[index] = doc[0];
					resolve();
				});
			});
		});

		Promise.all(promises).then(function () {
			callback(null, docs);
		}).catch(function (err) { callback(err); });
	});
}


// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var qs = require('querystring');
var ios = require('socket.io');


// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}

function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}


function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);
		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};


function _pathLog(data, res) {
	//console.log(data);
	//data.timestamp = data.timestamp
	PathLogModel.create(data)
}


function _getErrDetail(errCode, callback) {
	ErrorcodeModel.findOne(
		{ errcode: errCode },
		{ errth: 1 },
		function (err, response) {
			if (response == null) {
				callback('');
			} else {
				callback(response.errth)
			}
		}
	);
};



function _hotelAutoLogin(req, res) {
	device_id = req.body.device_id;
	_id = req.body._id;
	HotelsModel.findOne(
		{ device_id: device_id },
		function (err, result) {
			if (result == null) {
				// This phone(IMEI) have never been in the system, send to registration page.
				res.json({
					status: false,
					target: "REGISTRATION"
				});
			} else {
				if (result._id == _id) {
					result.status = "OFF";
					result.updated = new Date().getTime();
					result.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							// device_id: Y, _id: Y =>  Welcome to Taxi-Beam ]
							res.json({
								status: true,
								data: {
									status: result.status
								}
							});
						}
					});
				} else {
					// in case of change mobile owner, send to update page to update driver info
					res.json({
						status: false,
						target: "BLANKPROFILE"
					});
				}
			}
		}
	);
};




function _hotelPassLogin(req, res) {
	device_id = req.body.device_id;
	_id = req.body._id;
	HotelsModel.findOne(
		{ device_id: device_id },
		function (err, result) {
			if (result == null) {
				// This phone(IMEI) have never been in the system, send to registration page.
				res.json({
					status: false,
					target: "REGISTRATION"
				});
			} else {
				if (result._id == _id) {
					result.status = "OFF";
					result.updated = new Date().getTime();
					result.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							// device_id: Y, _id: Y =>  Welcome to Taxi-Beam ]
							res.json({
								status: true,
								data: {
									status: result.status
								}
							});
						}
					});
				} else {
					// in case of change mobile owner, send to update page to update driver info
					res.json({
						status: false,
						target: "BLANKPROFILE"
					});
				}
			}
		}
	);
};




function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}




// start ::: Passenger API Mobile Version
exports.hotelAutoLogin = function (req, res) {
	//_passengerAutoLogin (req, res);
	res.json({
		status: false,
		msg: "Your account is not valid"
	});
};




exports.hotelRegister = function (req, res) {
	var device_id = req.body.device_id;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	HotelsModel.findOne(
		{ device_id: device_id },
		function (err, result) {
			if (result == null) {
				HotelsModel.create({
					device_id: device_id,
					displayName: req.body.displayName,
					curaddr: req.body.displayName,
					email: req.body.email,
					phone: req.body.phone,
					curlng: curlng,
					curlat: curlat,
					curloc: curloc,
					createdvia: req.body.createdvia
				},
					function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							res.json({
								//msg:  "Your account has been created.", 
								status: true,
								data: {
									_id: response._id,
									msg: "register success",
									status: response.status
								}
							});
						}
					}
				);
			} else {
				// duplicated IMEI/App ID => updated the current data for device_id.	
				result.displayName = req.body.displayName;
				result.email = req.body.email;
				result.phone = req.body.phone;
				result.curaddr = req.body.displayName;
				result.curlng = curlng;
				result.curlat = curlat;
				result.curloc = curloc;
				result.destination = "";
				result.des_dist = "";
				result.deslng = null;
				result.deslat = null;
				result.desloc = [];
				result.tips = "";
				result.detail = "";
				result.favcartype = [];
				result.favdrv = [];
				result.drv_id = null;
				result.status = "OFF";
				result.createdvia = req.body.createdvia;
				result.created = new Date().getTime();
				result.updated = new Date().getTime();
				result.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							//msg:  "Your account has been updated." , 
							status: true,
							msg: "",
							data: {
								_id: response._id,
								msg: "re-register success",
								status: response.status
							}
						});
					}
				});
			}
		}
	);
};




exports.hotelUpdateProfile = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;


	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	HotelsModel.findOne(
		{ _id: _id, device_id: device_id },
		function (err, result) {
			if (result == null) {
				res.json({
					status: false,
					msg: "Your account is not valid"
				});
			} else {
				if (req.body.curlng) { result.curlng = req.body.curlng ? req.body.curlng : result.curlng; }
				if (req.body.curlat) { result.curlat = req.body.curlat ? req.body.curlat : result.curlat; }
				if (curloc) { result.curloc = curloc ? curloc : result.curloc; }
				if (req.body.displayName) { result.displayName = req.body.displayName ? req.body.displayName : result.displayName; }
				if (req.body.curaddr) { result.curaddr = req.body.curaddr ? req.body.curaddr : result.curaddr; }
				if (req.body.phone) { result.phone = req.body.phone ? req.body.phone : result.phone; }
				result.updated = new Date().getTime();
				result.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							status: true,
							msg: "success, hotel profile updated"
						});
					}
				});
			}
		}
	);
};




exports.hotelGetStatus = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var reallng = req.body.curlng;
	var reallat = req.body.curlat;
	var realloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	//console.log('passengerGetStatus : device_id = '+device_id)
	if (typeof reallng == 'undefined' && reallng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof reallat == 'undefined' && reallat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	HotelsModel.findOne(
		{ device_id: device_id },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, curloc: 1, reallat: 1, reallng: 1, realloc: 1, createdvia: 1 },
		function (err, result) {
			if (result == null) {
				res.json({
					status: false,
					msg: "error no data"
				});
			} else {
				//Set the two dates
				var currentTime = new Date();
				var currDate = currentTime.getMonth() + 1 + "/" + currentTime.getDate() + "/" + currentTime.getFullYear(); //Todays Date - implement your own date here.
				var iniPastedDate = result.updated; // "8/7/2012" //PassedDate - Implement your own date here.

				//currDate = 8/17/12 and iniPastedDate = 8/7/12

				function DateDiff(date1, date2) {
					var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
					return (datediff / (24 * 60 * 60 * 1000)); //Convert values to -/+ days and return value      
				}

				//Write out the returning value should be using this example equal -10 which means 
				//it has passed by ten days. If its positive the date is coming +10.    
				//console.log (DateDiff(new Date(iniPastedDate),new Date(currDate))); //Print the results...

				//console.log(result.updated)

				//if(req.body.curlng) 	{ result.curlng = req.body.curlng ? req.body.curlng : result.curlng; }
				//if(req.body.curlat) 	{ result.curlat = req.body.curlat ? req.body.curlat : result.curlat; }
				if (reallng) { result.reallng = reallng ? reallng : result.reallng; }
				if (reallat) { result.reallat = reallat ? reallat : result.reallat; }
				if (realloc) { result.realloc = realloc ? realloc : result.realloc; }
				//result.updated = new Date().getTime();
				result.save(function (err, result) {
					if (err) {
						res.json({
							status: false,
							msg: "error"
						});
					} else {
						//res.set( 'Content-type: application/json;charset=utf-8' );
						//res.set('Access-Control-Allow-Origin: *');					
						res.json({
							status: true,
							msg: "success, there is your status.",
							data: {
								_id: result._id,
								createdvia: result.createdvia,
								status: result.status,
								drv_id: result.drv_id,
								phone: result.phone,
								device_id: result.device_id,
								curlat: result.curlat,
								curlng: result.curlng,
								curloc: result.curloc,
								reallat: result.reallat,
								reallng: result.reallng,
								realloc: result.realloc
							}
						});
					}
				});
			}
		}
	);
};




exports.hotelGetByID = function (req, res) {
	var _id = req.body._id;
	HotelsModel.findOne(
		{ _id: _id },
		{ _id: 1, email: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, tips: 1, detail: 1, favcartype: 1, favdrv: 1, drv_id: 1, status: 1 },
		function (err, result) {
			if (result == null) {
				//res.json({ status: false , msg: "There is some data missing, please try again."});
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail('err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				if (err) {
					res.json({ status: false, msg: "error", data: err });
				} else {
					res.json({
						status: true,
						msg: "Hotel detail",
						data: result
					});
				}
			}
		}
	);
};




exports.hotelSearchDrv = function (req, res) {
	var favcartype = req.body.favcartype;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	var radian = req.body.radian;
	var amount = req.body.amount;

	// short form : if(req.body.uid) { device.uid = req.body.uid ? req.body.uid : device.uid; }		
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (typeof radian == 'undefined' && radian == null) {
		radian = config.psgsearchpsgradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.psgsearchpsgamount;
	}

	DriversModel.find(
		//{ active:"Y", status: "ON", curloc : { $near : curloc, $maxDistance: radian  }  },
		{
			//curloc : { $near : curloc, $maxDistance: radian },   => for 2d index => not working
			// do not forget  => db.passengers.createIndex( { curloc : "2dsphere" } )
			status: "ON",
			active: "Y",
			curlng: { $ne: null },
			curlat: { $ne: null },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: radian
					//$minDistance: 1 
				}
			},
			cartype: { $in: favcartype }
		},
		{ fname: 1, lname: 1, phone: 1, carplate: 1, cartype: 1, carcolor: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, status: 1, allowpsgcontact: 1, carreturn: 1, carreturnwhere: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
		{ limit: amount },
		function (err, drvlist) {
			// Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!
			if (drvlist == null) {
				res.json({ status: false, msg: "No data" });
			} else {
				res.json({
					status: true,
					data: drvlist
				});
				//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
			}
		}
	);
};




exports.hotelCallDrv = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var psg_name = req.body.psg_name;
	var psg_phone = req.body.psg_phone;
	var amount = req.body.amount;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc;
	var destination = req.body.destination;
	var detail = req.body.detail;
	var tips = req.body.tips;
	var calltime = req.body.calltime;
	var deslat;
	var deslng;
	var desloc;
	var favcartype = req.body.favcartype;
	var createdvia = req.body.createdvia;
	var job_id = new Date().getTime() + '-' + ranSMS();

	if (isNumeric(amount)) {
		amount = amount;
	} else {
		amount = 1;
	}

	if (typeof curlng == 'undefined' && typeof curlat == 'undefined') {
		curloc = [];
	} else {
		curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		curlng = "";
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		curlat = "";
	}

	deslng = req.body.deslng;
	deslat = req.body.deslat;

	if (typeof deslng == 'undefined' && typeof deslat == 'undefined') {
		desloc = [];
	} else {
		desloc = [parseFloat(req.body.deslng), parseFloat(req.body.deslat)];
	}
	if (typeof deslng == 'undefined' && deslng == null) {
		deslng = "";
	}
	if (typeof deslat == 'undefined' && deslat == null) {
		deslat = "";
	}

	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				searchpsgamount = config.searchpsgamount;
				searchpsgradian = config.searchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			} else {
				searchpsgamount = response.searchpsgamount;
				searchpsgradian = response.searchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			}
		}
	);

	function findPsginZone(searchpsgradian, searchpsgamount) {
		DriversModel.aggregate(
			[
				{
					$geoNear: {
						query: {
							status: "ON",
							active: "Y",
							//favcartype: { $in: [ [] , req.body.cartype ] }
							//cartype: { $in: req.body.favcartype }
						},
						near: { type: "Point", coordinates: curloc },
						distanceField: "dist",
						maxDistance: searchpsgradian,
						num: searchpsgamount,
						spherical: true
					}
				},
				{
					$project: {
						_id: 1,
						dist: 1,
						dist_km: {
							$divide: [
								{
									$subtract: [
										{ $multiply: ['$dist', 1] },
										{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
									]
								}, 1000]
						},
					},
				},
				{ $sort: { dist: 1 } }
			],
			function (err, drvlist) {
				// if(drvlist == null) {
				// 	PassengerModel.findOne(
				// 		{ device_id: device_id }, 
				// 		{ device_id:1, job_id:1, curaddr:1, curloc:1, destination:1, desloc:1, des_dist:1, tips:1, detail:1, favcartype:1, status:1, updated:1, createdjob: 1 },
				// 		function(err, psg) {			
				// 			if(psg == null) {
				// 				res.json({
				// 					status: false , 
				// 					msg: "no psg id"
				// 				});			
				// 			} else {		    																			
				// 				psg.status = "NOCARNEARBY";
				// 				psg.updated = new Date().getTime();								
				// 				psg.save(function(err, response) {
				// 					if (err) {
				// 						res.json({ 
				// 							status: false , 
				// 							msg: "error", 
				// 							data: err							
				// 						});
				// 					} else {										
				// 						res.json({status: true, msg: "ขออภัยค่ะ ขณะนี้ไม่มีแท็กซี่ว่างในบริเวณใกล้เคียง"});
				// 					}
				// 				});
				// 			}
				// 		}
				// 	);		
				// } else {	
				HotelsModel.findOne(
					{ device_id: device_id },
					{ device_id: 1, job_id: 1, curaddr: 1, curloc: 1, curlat: 1, curlng: 1, tips: 1, detail: 1, favcartype: 1, status: 1, updated: 1, createdjob: 1 },
					function (err, psg) {
						if (psg == null) {
							res.json({
								status: false,
								msg: "error : device id for hotel to creat job was not found.",
								data: err
							});
						} else {
							hotel_phone = psg.phone;
							displayName = psg.displayName;
							curaddr = psg.curaddr;
							curlng = psg.curlng;
							curlat = psg.curlat;
							curloc = [parseFloat(psg.curlng), parseFloat(psg.curlat)];
							if (typeof req.body.favcartype == 'undefined') {
								favcartype = [];
							} else {
								favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype;
							}
							//var results = [];             
							for (i = 0; i < amount; i++) {
								InsertHotelLoop(job_id, _id, displayName, hotel_phone, psg_name, psg_phone, favcartype, curaddr, destination, detail, curlng, curlat, curloc, deslat, deslng, desloc, tips, createdvia, calltime, drvlist);
							}
							res.json({
								status: true,
								msg: "Add jobs completed"
							})
						}
					}
				);
				//}
			}
		);
	}

	function InsertHotelLoop(job_id, hotel_id, displayName, hotel_phone, psg_name, psg_phone, favcartype, curaddr, destination, detail, curlng, curlat, curloc, deslat, deslng, desloc, tips, createdvia, calltime, drvlist) {
		var d = {
			job_id: job_id,
			hotel_id: hotel_id,
			displayName: displayName,
			hotel_phone: hotel_phone,
			psg_name: psg_name,
			psg_phone: psg_phone,
			favcartype: favcartype,
			curaddr: curaddr,
			curlng: curlng,
			curlat: curlat,
			curloc: curloc,
			destination: destination,
			deslng: deslng,
			deslat: deslat,
			desloc: desloc,
			detail: detail,
			tips: tips,
			status: "ON",
			createdvia: createdvia,
			drvarroundlist: drvlist,
			calltime: calltime,
			updated: new Date().getTime(),
			created: new Date().getTime(),
			createdjob: new Date().getTime(),
			datepsgcall: new Date().getTime(),
			jobhistory: [{
				action: "add hotel job list",
				status: "ON",
				created: new Date()
			}]
		}
		JoblisthotelModel.create(d, function (err, response) {
			if (err) {
				console.log('JoblisthotelModel created : err = ' + err);
			} else {
				console.log('JoblisthotelModel created : success = ');
				//socket.of('/'+req.user.local.cgroup).emit('addjoblist', response);				
			}
		});
	}
};




exports.hotelReCall = function (req, res) {
	var _id = req.body._id;
	var oldpsg_id = req.body.oldpsg_id;
	var device_id = req.body.device_id;
	var psg_name = req.body.psg_name;
	var psg_phone = req.body.psg_phone;
	var amount = req.body.amount;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc;
	var destination = req.body.destination;
	var detail = req.body.detail;
	var tips = req.body.tips;
	var calltime = req.body.calltime;
	var deslat;
	var deslng;
	var desloc;
	var favcartype = req.body.favcartype;
	var createdvia = req.body.createdvia;

	amount = 1;

	if (typeof curlng == 'undefined' && typeof curlat == 'undefined') {
		curloc = [];
	} else {
		curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		curlng = "";
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		curlat = "";
	}

	deslng = req.body.deslng;
	deslat = req.body.deslat;

	if (typeof deslng == 'undefined' && typeof deslat == 'undefined') {
		desloc = [];
	} else {
		desloc = [parseFloat(req.body.deslng), parseFloat(req.body.deslat)];
	}
	if (typeof deslng == 'undefined' && deslng == null) {
		deslng = "";
	}
	if (typeof deslat == 'undefined' && deslat == null) {
		deslat = "";
	}

	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				searchpsgamount = config.searchpsgamount;
				searchpsgradian = config.searchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			} else {
				searchpsgamount = response.searchpsgamount;
				searchpsgradian = response.searchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			}
		}
	);

	function findPsginZone(searchpsgradian, searchpsgamount) {
		DriversModel.aggregate(
			[
				{
					$geoNear: {
						query: {
							status: "ON",
							active: "Y",
							//favcartype: { $in: [ [] , req.body.cartype ] }
						},
						near: { type: "Point", coordinates: curloc },
						distanceField: "dist",
						maxDistance: searchpsgradian,
						num: searchpsgamount,
						spherical: true
					}
				},
				{
					$project: {
						_id: 1,
						dist: 1,
						dist_km: {
							$divide: [
								{
									$subtract: [
										{ $multiply: ['$dist', 1] },
										{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
									]
								}, 1000]
						},
					},
				},
				{ $sort: { dist: 1 } }
			],
			function (err, drvlist) {
				HotelsModel.findOne(
					{ device_id: device_id },
					function (err, result) {
						if (result == null) {
							res.json({
								status: false,
								msg: "error no device id was found.",
								data: err
							});
						} else {
							hotel_phone = result.phone;
							displayName = result.displayName;
							curaddr = result.curaddr;
							curlng = result.curlng;
							curlat = result.curlat;
							curloc = [parseFloat(result.curlng), parseFloat(result.curlat)];
							var drecall = {
								job_id: job_id,
								hotel_id: _id,
								displayName: displayName,
								hotel_phone: hotel_phone,
								psg_name: psg_name,
								psg_phone: psg_phone,
								favcartype: favcartype,
								curaddr: curaddr,
								curlng: curlng,
								curlat: curlat,
								curloc: curloc,
								destination: destination,
								deslng: deslng,
								deslat: deslat,
								desloc: desloc,
								detail: detail,
								tips: tips,
								status: "ON",
								drvarroundlist: drvlist,
								createdvia: createdvia,
								calltime: calltime,
								updated: new Date().getTime(),
								created: new Date().getTime(),
								datepsgcall: new Date().getTime(),
								jobhistory: [{
									action: "add  recall hotel job list",
									status: "ON",
									created: new Date()
								}]
							}

							JoblisthotelModel.findOne(
								{ _id: oldpsg_id, status: "DRVDENIED" },
								function (err, psg) {
									if (psg == null) {
										res.json({ status: false, msg: "hotelReCall - error00" });
									} else {
										psg.status = "CANCELLED";				// cancelled งานเก่า และ เรียกงานใหม่
										psg.save(function (err, response) {
											if (err) {
												res.json({ status: false, msg: "hotelReCall - error02" });
											} else {
												JoblisthotelModel.create(drecall, function (err, response) {
													if (err) {
														res.json({
															status: false,
															msg: "Hotel Jbsrecall ::::: fail !!!"
														})
													} else {

														res.json({
															status: true,
															msg: "Hotel Jobsrecall: completed"
														})
													}
												});
											}
										});
									}
								}
							);
						}
					}
				);
				// HotelsModel.findOne(
				// 	{ device_id: device_id }, 
				// 	{ device_id:1, job_id:1, curaddr:1, curloc:1, tips:1, detail:1, favcartype:1, status:1, updated:1, createdjob: 1 },
				// 	function(err, psg) {			
				// 		if(psg == null) {
				// 			res.json({ 
				// 				status: false , 
				// 				msg: "error : device id for hotel to creat job was not found.", 
				// 				data: err							
				// 			});	
				// 		} else {
				// 			hotel_phone = psg.phone ; 
				// 			displayName = psg.displayName ;
				// 			curaddr = psg.curaddr ;
				// 			curlng = psg.curlng ;
				// 			curlat = psg.curlat ;
				// 			curloc 	= [parseFloat(psg.curlng),parseFloat(psg.curlat)] ;								
				// 			if (typeof req.body.favcartype == 'undefined' ) {		
				// 				favcartype = []; 
				// 			} else {
				// 				favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype; 	
				// 			}             
				// 			for (i = 0; i < amount; i++) {					
				// 				InsertHotelLoop(job_id, _id, displayName , hotel_phone, psg_name, psg_phone, favcartype, curaddr, destination, detail, curlng, curlat, curloc, deslat, deslng, desloc, tips, createdvia, calltime, drvlist );
				// 			}
				// 			res.json({
				// 				status: true,
				// 				msg: "Add jobs completed"
				// 			})	
				// 		}
				// 	}
				// );
			}
		);
	}
};




exports.hotelviewJoblist = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	JoblisthotelModel.find(
		{ hotel_id: _id, status: { $in: ["ON", "BUSY", "DRVDENIED", "BUSY", "PICK"] } },
		{ _id: 1, job_id: 1, hotel_id: 1, psg_name: 1, psg_phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, detail: 1, status: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, calltime: 1, created: 1 },
		{ sort: { "datepsgcall": -1 } },
		function (err, psg) {
			if (psg == 0) {
				res.json({
					status: false,
					msg: "error",
					data: err
				});
			} else {
				res.json({
					status: true,
					msg: "Joblist detail",
					data: psg
				});
			}
		}
	);
};




exports.hotelTaxiArrived = function (req, res) {
	var _id = req.body._id;	// Hotel id
	var device_id = req.body.device_id;	// Hotel device id
	var psg_id = req.body.psg_id;	// JoblisthotelModel  ObjectId
	//console.log ( 'cancel  call')
	JoblisthotelModel.findOne(
		{ _id: psg_id, hotel_id: _id, status: "BUSY" },
		{ status: 1, updated: 1, drv_id: 1, job_id: 1 },
		function (err, psg) {
			if (psg == null) {
				res.json({ status: false, msg: "no  hotel and job id or status is not BUSY" });
			} else {
				psg.updated = new Date().getTime();
				psg.status = "ARRIVED";
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error02" });
					} else {
						res.json({
							status: true,
							msg: "Taxi is arrived.",
							data: response
						});
					}
				});
			}
		}
	);
};





exports.hotelCancelCall = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psg_id = req.body.psg_id;	// JoblisthotelModel  ObjectId

		JoblisthotelModel.findOne(
			{ _id: psg_id },
			{ status: 1, updated: 1, drv_id: 1, job_id: 1 },
			function (err, psg) {
				if (psg == null) {
					res.json({ status: false, msg: "error00" });
				} else {
					//drv_id = psg.drv_id;					
					if (psg.drv_id == undefined) {
						psg.drv_id = null;
						psg.updated = new Date().getTime();
						psg.deniedTaxiIds = [];
						psg.status = "OFF";
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error01" });
							} else {
								res.json({
									status: true,
									msg: "cancelled a call",
									data: { status: response.status }
								});
							}
						});
					} else {
						canceldrvid = psg.drv_id
						psg.drv_id = null;
						psg.updated = new Date().getTime();
						psg.deniedTaxiIds = [];
						psg.status = "OFF";
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error02" });
							} else {
								DriversModel.findOne(
									{ _id: canceldrvid },
									{ _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, status: 1 },
									function (err, drv) {
										if (drv == null) {
											res.json({ status: false, msg: "error03" });
										} else {
											drv.psg_id = null;
											drv.status = "ON";
											drv.save(function (err, result) {
												if (err) {
													res.json({ status: false, msg: "error04" });
												} else {
													socket.emit("DriverSocketOn", result);
													res.json({
														status: true,
														//msg: "Update passenger to ON => passenger cancel driver" 
														msg: "cancelled a driver",
														data: response,
														result: result
													});
												}
											});
										}
									}
								)
							}
						});
					}
				}
			}
		);
	};
};




exports.hotelCancelDrv = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;	// Lite version use phone as a unique key
		var drv_id = req.body.drv_id;
		//var curloc = [parseFloat(req.body.curlng),parseFloat(req.body.curlat)];
		JoblisthotelModel.findOne(
			{ device_id: device_id },
			{ status: 1, updated: 1, job_id: 1 },
			function (err, psg) {
				if (psg == null) {
					res.json({
						status: false,
						msg: "Your account is not valid"
					});
				} else {
					psg.drv_id = null;
					psg.updated = new Date().getTime();
					psg.deniedTaxiIds = [];
					psg.status = "ON";
					psg.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							DriversModel.findOne(
								{ _id: drv_id },
								{ _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, status: 1, updated: 1 },
								function (err, drv) {
									if (drv == null) {
										_getErrDetail('err009', function (errorDetail) {
											res.json({
												status: false,
												msg: errorDetail
											});
										});
									} else {
										drv.psg_id = null;
										drv.status = "ON";
										drv.save(function (err, response) {
											if (err) {
												res.json({ status: false, msg: "error", data: err });
											} else {
												socket.emit("DriverSocketOn", drv);
												//socket.emit("passengerCancelDrv", drv);									
												res.json({
													status: true,
													//msg: "Update passenger to ON => passenger cancel driver" 
													data: { status: response.status }
												});
											}
										});
									}
								}
							);
						}
					});
				}
			}
		);
	};
};




exports.getHotelHistory = function (req, res) {
	var _id = req.body._id;
	// { _id:1, job_id:1, hotel_id:1, psg_name:1, psg_phone:1, curaddr:1, curlng:1, curlat:1, curloc:1, destination:1, detail:1, status:1, drv_id: 1, drv_name:1, drv_phone: 1, drv_carplate: 1, calltime:1, created:1  },
	JoblisthotelModel.aggregate([
		{
			$match:
			{
				hotel_id: new mongoose.Types.ObjectId(_id), status: { $nin: ["DELETED"] }
			}
		},
		{
			"$group": {
				_id: {
					"$dateToString": { format: "%Y-%m-%d", date: { $subtract: ["$datepsgcall", -25200000] } },
				},
				"detail": { $push: "$$ROOT" },
				"jobCount": { "$sum": 1 },
			}
		},
		{
			"$sort": { "datepsgcall": -1 }
		}
	], function (err, job) {
		if (job == 0) {
			console.log(job)
			_getErrDetail('err011', function (errorDetail) {
				res.json({
					status: false,
					msg: errorDetail
				});
			});
		} else {
			res.json({
				status: true,
				msg: "hotel history detail",
				data: job
			});
		}
	});
};



exports.hotelDeleteJob = function (req, res) {
	var _id = req.body._id;	// Hotel id
	var device_id = req.body.device_id;	// Hotel device id
	var psg_id = req.body.psg_id;	// JoblisthotelModel  ObjectId
	//console.log ( 'cancel  call')
	JoblisthotelModel.findOne(
		{ _id: psg_id, hotel_id: _id },
		{ status: 1, updated: 1, drv_id: 1, job_id: 1 },
		function (err, psg) {
			if (psg == null) {
				res.json({ status: false, msg: "There is no  hotel and job id or status is available" });
			} else {
				psg.updated = new Date().getTime();
				psg.status = "DELETED";
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error02" });
					} else {
						res.json({
							status: true,
							msg: "This job has been deleted.",
							data: response
						});
					}
				});
			}
		}
	);
};


//---------- POIs ----------//
exports.addNewPOIs = function (req, res) {

	if (!req.params.phone) {
		res.status(200).json({ status: false, data: "request parameter phone is not defined." });
	}

	HotelsModel
		.findOne({ phone: req.params.phone, cgroup: req.user.local.cgroup, _id_garage: req.user._id_garage })
		.exec(function (err, hotel) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !hotel) {

				var passenger = new HotelsModel({
					cgroup: req.user.local.cgroup,
					_id_garage: req.user._id_garage,
					phone: req.body.phone
				});

				passenger.save(function (err, hotel) {

					if (err) {
						res.status(200).json({ status: false, data: err });
					}

					var poi = new POIsModel({
						_owner: hotel._id,
						name: req.body.name,
						formatter: req.body.formatter,
						curloc: req.body.curloc,
						type: req.body.type,
						garage: req.user.local.cgroup,
						detail: req.body.detail,
						destination: req.body.destination,
						count: 1
					});

					poi.save(function (err, result) {
						if (err) { console.log(err); }
						res.status(200).json({ status: true, data: result });
					});
				});
			}

			if (hotel) {

				POIsModel.findOne({ _owner: hotel._id, name: req.body.name }).exec(function (err, poi) {

					if (err) {
						res.status(200).json({ status: false, data: err });
					}

					// edit mode.
					if (poi) {

						poi.name = req.body.name;
						poi.detail = req.body.detail;
						poi.formatter = req.body.formatter;
						poi.curloc = req.body.curloc;
						poi.type = req.body.type;
						poi.garage = req.user.local.cgroup,
							poi.destination = req.body.destination;

						poi.save(function (err, result) {
							if (err) {
								res.status(200).json({ status: false, data: err });
							}
							res.status(200).json({ status: true, data: result, mode: 'EDIT' });
						});
					}

					// add mode
					if (!err && !poi) {

						POIsModel.create({
							_owner: hotel._id,
							name: req.body.name,
							formatter: req.body.formatter,
							curloc: req.body.curloc,
							type: req.body.type,
							garage: req.user.local.cgroup,
							detail: req.body.detail,
							destination: req.body.destination,
							count: 1
						}, function (err, resulta) {
							if (err) { console.log(err); }
							res.status(200).json({ status: true, data: resulta });
						});
					}
				});
			}
		}
		);
}


exports.getAllPOIsByGarage = function (req, res) {
	POIsModel
		.find({ garage: req.user.local.cgroup })
		.populate('_owner', 'phone')
		.exec(function (err, result) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !result) {
				res.status(200).json({ status: false, data: result });
			}

			if (result) {
				res.status(200).json({ status: true, data: result });
			}
		}
		);
}


exports.getAllPOIsByPassengerId = function (req, res) {
	POIsModel
		.find({ _owner: req.params.passengerId, garage: req.user.local.cgroup })
		.exec(function (err, result) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !result) {
				res.status(200).json({ status: false, data: result });
			}

			if (result) {
				res.status(200).json({ status: true, data: result });
			}
		}
		);
}


exports.getAllPOIsByPhone = function (req, res) {

	if (!req.params.phone) {
		res.status(200).json({ status: false, data: 'req.params.phone not found.' });
	}

	HotelsModel.findOne({ phone: req.params.phone, cgroup: req.user.local.cgroup }).select('_id').exec(function (err, passenger) {

		if (err) {
			res.status(200).json({ status: false, data: err });
			return;
		}

		if (!err && !passenger) {
			res.status(200).json({ status: false, data: passenger });
			return;
		}

		POIsModel
			.find({ garage: req.user.local.cgroup, _owner: passenger._id }).populate('_owner', 'phone').exec(function (err, result) {

				if (err) {
					res.status(200).json({ status: false, data: err });
					return;
				}

				if (!err && !result) {
					res.status(200).json({ status: false, data: result });
					return;
				}

				if (result) {
					res.status(200).json({ status: true, data: result });
				}
			}
			);
	});
};


exports.searchPOIsByPhone = function (req, res) {

	function replaceAll(str) {
		return str.replace(/[.*+?^${}()|[\]\\]/g, "");
	}

	var cgroup = req.user.local.cgroup;
	req.body.query = replaceAll(req.body.query);

	if (req.params.phone === 'UNKNOW') {
		if (req.body.query) {
			PoirecommendedModel
				.find({ cgroup: req.user.local.cgroup, parkingname: new RegExp(req.body.query) })
				.select({ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1 })
				.sort({ 'parkinglot': 1, 'parkingname': 1 })
				.limit(15)
				.exec(function (err, poi) {
					res.status(200).json({ status: true, data: poi });
				});
		} else {
			PoirecommendedModel
				.find({ cgroup: req.user.local.cgroup })
				.select({ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1 })
				.sort({ 'parkinglot': 1, 'parkingname': 1 })
				.random(15, true, function (err, poi) {
					res.status(200).json({ status: true, data: poi });
				});
		}
	} else {
		HotelsModel
			.findOne({ phone: req.params.phone, cgroup: cgroup })
			.exec(function (err, hotel) {

				if (err) {
					res.status(200).json({ status: false, data: err });
				}

				if (hotel) {
					if (req.body.query) {
						POIsModel.find({ _owner: hotel._id, garage: hotel.cgroup, name: new RegExp(req.body.query) }).limit(5).exec(function (err, result) {

							if (err) {
								res.status(200).json({ status: false, data: err });
								return;
							}

							if (!err && !result) {
								res.status(200).json({ status: false, data: result });
								return;
							}

							if (req.params.garage) {
								PoirecommendedModel
									.find({ cgroup: req.user.local.cgroup, parkingname: new RegExp(req.body.query) })
									.select({ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1 })
									.sort({ 'parkinglot': 1, 'parkingname': 1 })
									.limit(15)
									.exec(function (err, poi) {

										result.forEach(function (item, index) {
											item.parkinglot = "history";
										});

										var data = poi ? result.concat(poi) : result;

										res.status(200).json({ status: true, data: data, hasHistory: true });
									});
							} else {
								res.status(200).json({ status: true, data: result });
							}
						});
					} else {
						POIsModel.find({ _owner: hotel._id, garage: hotel.cgroup }).limit(5).exec(function (err, result) {

							if (err) {
								res.status(200).json({ status: false, data: err });
								return;
							}

							if (!err && !result) {
								res.status(200).json({ status: false, data: result });
								return;
							}

							if (req.params.garage) {
								PoirecommendedModel
									.find({ cgroup: req.user.local.cgroup })
									.select({ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1 })
									.sort({ 'parkinglot': 1, 'parkingname': 1 })
									.random(15, true, function (err, poi) {

										result.forEach(function (item, index) {
											item.parkinglot = "history";
										});

										var data = poi ? result.concat(poi) : result;

										res.status(200).json({ status: true, data: data, hasHistory: true });
									});
							} else {
								res.status(200).json({ status: true, data: result });
							}
						});
					}
				} else {
					if (req.body.query) {
						if (req.params.garage) {
							PoirecommendedModel
								.find({ cgroup: req.user.local.cgroup, parkingname: new RegExp(req.body.query) })
								.select({ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1 })
								.sort({ 'parkinglot': 1, 'parkingname': 1 })
								.limit(15)
								.exec(function (err, poi) {
									res.status(200).json({ status: true, data: poi, hasHistory: false });
								});
						} else {
							res.status(200).json({ status: true, data: result });
						}
					} else {
						if (req.params.garage) {
							PoirecommendedModel
								.find({ cgroup: req.user.local.cgroup })
								.select({ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1 })
								.sort({ 'parkinglot': 1, 'parkingname': 1 })
								.random(15, true, function (err, poi) {
									res.status(200).json({ status: true, data: poi, hasHistory: false });
								});
						} else {
							res.status(200).json({ status: true, data: result });
						}
					}
				}
			}
			);
	}
}


exports.getPOI = function (req, res) {
	POIsModel.findById(req.params.id).populate('_owner').exec(function (err, result) {

		if (err) {
			res.status(200).json({ status: false, data: err });
		}

		if (!err && !result) {
			res.status(200).json({ status: false, data: result });
		}

		if (result) {
			res.status(200).json({ status: true, data: result });
		}
	});
}


exports.getPOIStart = function (req, res) {
	POIsModel
		.find({ _owner: req.params.passengerId, garage: req.user.local.cgroup, type: "start" })
		.exec(function (err, result) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !result) {
				res.status(200).json({ status: false, data: result });
			}

			if (result) {
				res.status(200).json({ status: true, data: result });
			}
		}
	);
}


exports.getPOIEnd = function (req, res) {
	POIsModel
		.find({ _owner: req.params.passengerId, garage: req.user.local.cgroup, type: "end" })
		.exec(function (err, result) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !result) {
				res.status(200).json({ status: false, data: result });
			}

			if (result) {
				res.status(200).json({ status: true, data: result });
			}
		}
	);
}


exports.editPOI = function (req, res) {
	POIsModel
		.findById(req.params.id).exec(function (err, poi) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !poi) {
				res.status(200).json({ status: false, data: poi });
			}

			if (poi) {

				poi.name = req.body.name;
				poi.detail = req.body.detail;
				poi.formatter = req.body.formatter;
				poi.curloc = req.body.curloc;
				poi.type = req.body.type;
				poi.garage = req.user.local.cgroup;
				poi.destination = req.body.destination;

				poi.save(function (err, result) {
					res.status(200).json({ status: true, data: result });
				});
			}
		}
	);
}


exports.removePOI = function (req, res) {
	POIsModel
		.findById(req.params.id).exec(function (err, poi) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !poi) {
				res.status(200).json({ status: false, data: poi });
			}

			if (poi) {
				poi.remove(function (err, result) {
					res.status(200).json({ status: true, data: result || err });
				});
			}
		}
	);
}


exports.removeAllPOIs = function (req, res) {
	POIsModel
		.find({ _owner: req.params.passengerId, garage: req.user.local.cgroup }).exec(function (err, pois) {

			if (err) {
				res.status(200).json({ status: false, data: err });
			}

			if (!err && !pois) {
				res.status(200).json({ status: false, data: pois });
			}

			if (pois) {

				POIsModel.remove({
					_id: { $in: pois }
				}, function (err, result) {
					res.status(200).json({ status: true, data: result || err });
				});

				// pois.remove(function(err, result) {
				// 	res.status(200).json({ status: true, data: result || err });
				// });
			}
		}
	);
}