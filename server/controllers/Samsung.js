////////////////////////////////////
// TaxiBeam API for Samsung
// version : 1.0.0
// UpDate April 25, 2017 
// Created by Hanzen@BRET
////////////////////////////////////
var config = require('../../config/func').production;
var langdrvth = require('../../config/langth').langdrvth;
var crypto = require('crypto');
var compareVersions = require('compare-versions');
var lang = 'en';

// start nodemailer and define param
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var SMTPSentEmail = config.SMTPSentEmail;
var SMTPSentPass = config.SMTPSentPass;
var InfoEmail = config.InfoEmail;
// end define param for nodemailer

var Url = require('url');
var mongoose = require('mongoose');

var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var moment = require('moment');

var PassengerModel = mongoose.model('PassengerModel');
var DriversModel = mongoose.model('DriversModel');
var JoblistModel = mongoose.model('JoblistModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var JoblisthotelModel = mongoose.model('JoblisthotelModel');
var Lk_garageModel = mongoose.model('Lk_garageModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');
var HistoryModel = mongoose.model('HistoryModel');

// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var qs = require('querystring');
var ios = require('socket.io');

// var crypto;
// try {
//   crypto = require('crypto');  
// } catch (err) {
//   console.log('crypto support is disabled!');
// }



// NOTE *******************************  start service for Samsung 
exports.gettaxilist = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var favcartype = req.body.favcartype;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var radian = req.body.radian;
	var amount = req.body.amount;
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current Longitude is not defined" });
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current Latitude is not defined" });
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof radian == 'undefined' && radian == null) {
		radian = config.psgsearchpsgradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.psgsearchpsgamount;
	}

	// ไม่สามารถให้เช็ค device_id ได้ เพราะไม่อย่างนั้น ในกรณีของ webapp ที่เปิดมาครั้งแรกยังไม่มีการสร้าง device_id จะไม่สามารถเห็นรถได้    

	var dataDriver = DriversModel.find(
		{
			status: "ON",
			active: "Y",
			curlng: { $ne: null },
			curlat: { $ne: null },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: radian
				}
			},
			cartype: { $in: favcartype },
			_id_garage: { $exists: true }
		},
		{
			_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, phone: 1, carplate: 1, curlat: 1, curlng: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1, imgface: 1, status: 1, _id_garage: 1
		}
	);
	dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1, province: 1 });
	dataDriver.exec(function (err, result) {
		if (result == 0) {
			res.json({ status: true, data: result, msg: "No data" });
		} else {
			res.json({
				status: true,
				data: result
			});
		}
	});
};




exports.gettaxiinfo = function (req, res) {
	var lang = req.params.lang ? req.params.lang : "en";
	var taxi_duid = req.body.taxi_duid;
	// ไม่สามารถให้เช็ค device_id ได้ เพราะไม่อย่างนั้น ในกรณีของ webapp ที่เปิดมาครั้งแรกยังไม่มีการสร้าง device_id จะไม่สามารถเห็นรถได้    
	var dataDriver = DriversModel.findOne(
		{
			_id: taxi_duid,
			active: "Y",
			_id_garage: { $exists: true }
		},
		{
			_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, phone: 1, carplate: 1, curlat: 1, curlng: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1, imgface: 1, status: 1, _id_garage: 1
		}
	);
	dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1 });
	dataDriver.exec(function (err, result) {
		if (result == null) {
			res.json({ status: true, data: result, msg: "No data" });
		} else {
			res.json({
				status: true,
				data: result
			});
		}
	});
};




exports.getsspsgstatus = function (req, res) {
	var lang = req.params.lang ? req.params.lang : "en";
	var device_id = req.params.passenger_ssid;
	var displayName = req.body.displayName;
	var phone = req.body.phone;
	PassengerModel.findOne(
		{ device_id: device_id, cgroup: "samsung" },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, createdvia: 1, job_id: 1 },
		function (err, result) {
			if (result == null) {
				var reqset = '{"passenger_ssid": "' + passenger_ssid + '", "passenger_jobid": "' + passenger_jobid + '"}';
				var postData = JSON.parse(JSON.stringify(reqset));
				var options = {
					host: config.samsunghost,
					port: config.samsungport,
					path: config.samsungpath + '/' + passenger_ssid + '/requests' + passenger_jobid + '/' + action,
					method: method,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Content-Length': Buffer.byteLength(postData)
					}
				};

				var request = http.request(options, function (response) {
					var body = '';
					response.on('data', function (dog) {
						body += dog;
					});
					response.on('end', function (cat) {
						if (body) {
							var parsed = JSON.parse(body);
							if (parsed.SendMessageResult.ErrorCode == 1) {
								PassengerModel.create({
									device_id: device_id,
									displayName: displayName,
									phone: phone,
									occupied: false,
									updated: new Date(),
									createdvia: "SAMSUNG",
									cgroup: "samsung",
									appversion: "2",
									status: "OFF"
								},
									function (err, response) {
										if (err) {
											res.json({
												status: false,
												msg: "cannot create passenger",
												data: err
											});
										} else {
											res.json({
												status: true,
												msg: "create new a passenger",
												data: {
													passenger_ssid: response.device_id,
													passenger_jobid: response.job_id,
													passenger_status: response.status,
													taxi_duid: response.drv_id
												}
											});
										}
									}
								);
							} else {
								res.json({
									status: false,
									msg: "cannot create passenger",
									data: err
								});
							}
						} else {
							res.json({
								status: false,
								msg: "cannot create passenger",
								data: err
							});
						}
					});
				});
				request.on('error', function (err) {
					res.json({
						status: false,
						msg: "cannot create passenger",
						data: err
					});
				});
				request.write(postData);
				request.end();
			} else {
				res.json({
					status: true,
					data: {
						passenger_ssid: result.device_id,
						passenger_jobid: result.job_id,
						passenger_status: result.status,
						taxi_duid: result.drv_id
					}
				});
			}
		}
	);
};




exports.passengercreatejob = function (req, res) {
	var device_id = req.params.passenger_ssid;
	var job_id = req.params.passenger_jobid;
	var displayName = req.body.displayName;
	var phone = req.body.phone;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curaddr = req.body.curaddr;
	var deslng = req.body.deslng;
	var deslat = req.body.deslat;
	var destination = req.body.destination;
	var favcartype = req.body.favcartype;
	var radian = req.body.radian;
	var tips = req.body.tips;
	var detail = req.body.detail;
	var promotioncode = req.body.promotioncode;
	var createdvia = "SAMSUNG";
	var jobtype = "SAMSUNG";
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}	
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "The current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "The current latitude is not valid" })
		return;
	}
	if (typeof deslng == 'undefined' && deslng == null) {
		res.json({ status: false, msg: "The destination longitude is not valid" })
		return;
	}
	if (typeof deslat == 'undefined' && deslat == null) {
		res.json({ status: false, msg: "The destination latitude is not valid" })
		return;
	}

	JoblistModel.findOne(
		{
			psg_device_id: device_id, job_id: job_id
		}, function (err, joblist) {
			if (joblist == null) {
				Lk_provincesModel.findOne(
					{
						polygons:
						{
							$geoIntersects:
							{
								$geometry:
								{
									"type": "Point",
									"coordinates": curloc
								}
							}
						}
					},
					function (err, response) {
						if (response == null) {
							searchpsgamount = config.psgsearchpsgamount;
							searchpsgradian = config.psgsearchpsgradian;
							findPsginZone(searchpsgradian, searchpsgamount);
						} else {
							searchpsgamount = response.searchpsgamount;
							searchpsgradian = response.searchpsgradian;
							findPsginZone(searchpsgradian, searchpsgamount);
						}
					}
				);

				function findPsginZone(searchpsgradian, searchpsgamount) {
					DriversModel.aggregate(
						[
							{
								$geoNear: {
									query: {
										status: "ON",
										active: "Y",
										favcartype: { $in: [[], req.body.cartype] }
									},
									near: { type: "Point", coordinates: curloc },
									distanceField: "dist",
									maxDistance: searchpsgradian,
									num: searchpsgamount,
									spherical: true
								}
							},
							{
								$project: {
									_id: 1,
									dist: 1,
									dist_km: {
										$divide: [
											{
												$subtract: [
													{ $multiply: ['$dist', 1] },
													{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
												]
											}, 1000]
									},
								},
							},
							{ $sort: { dist: 1 } }
						],
						function (err, drvlist) {
							PassengerModel.findOne(
								{ device_id: device_id, cgroup: "samsung" },
								function (err, psg) {
									if (psg == null) {
										var reqset = '{"passenger_ssid": "' + passenger_ssid + '", "passenger_jobid": "' + passenger_jobid + '"}';
										var postData = JSON.parse(JSON.stringify(reqset));
										var options = {
											host: config.samsunghost,
											port: config.samsungport,
											path: config.samsungpath + '/' + passenger_ssid + '/requests' + passenger_jobid + '/' + action,
											method: method,
											headers: {
												'Content-Type': 'application/json; charset=utf-8',
												'Content-Length': Buffer.byteLength(postData)
											}
										};

										var request = http.request(options, function (response) {
											var body = '';
											response.on('data', function (dog) {
												body += dog;
											});
											response.on('end', function (cat) {
												if (body) {
													var parsed = JSON.parse(body);
													if (parsed.SendMessageResult.ErrorCode == 1) {
														// id is valide
														PassengerModel.create({
															device_id: device_id,
															displayName: displayName,
															phone: phone,
															curaddr: curaddr,
															curlng: curlng,
															curlat: curlat,
															curloc: curloc,
															destination: destination,
															deslng: deslng,
															deslat: deslat,
															favcartype: favcartype,
															tips: tips,
															detail: detail,
															job_id: job_id,
															drvarroundlist: drvlist,
															occupied: false,
															createdjob: new Date(),
															updated: new Date(),
															createdvia: "SAMSUNG",
															appversion: "2",
															cgroup: "samsung",
															status: "ON"
														},
															function (err, response) {
																if (err) {
																	res.json({
																		status: false,
																		msg: "cannot create passenger",
																		data: err
																	});
																} else {
																	_joblistcreate(job_id, jobtype, drvlist, req);
																	res.json({
																		status: true,
																		msg: "create new a passenger",
																		data: {
																			passenger_ssid: response.device_id,
																			passenger_jobid: response.job_id,
																			passenger_status: response.status,
																			taxi_duid: response.drv_id,
																			taxi_list: response.drvarroundlist
																		}
																	});
																}
															}
														);
													} else {
														res.json({
															status: false,
															msg: "cannot create passenger",
															data: err
														});
													}
												} else {
													res.json({
														status: false,
														msg: "cannot create passenger",
														data: err
													});
												}
											});
										});
										request.on('error', function (err) {
											res.json({
												status: false,
												msg: "cannot create passenger",
												data: err
											});
										});
										request.write(postData);
										request.end();
									} else {
										psg.phone = req.body.phone ? phone : psg.phone;
										psg.curaddr = req.body.curaddr ? curaddr : psg.curaddr;
										psg.curlng = req.body.curlng ? curlng : psg.curlng;
										psg.curlat = req.body.curlat ? curlat : psg.curlat;
										psg.curloc = curloc ? curloc : psg.curloc;
										if (typeof req.body.favcartype == 'undefined') {
											psg.favcartype = [];
										} else {
											psg.favcartype = req.body.favcartype ? favcartype : psg.favcartype;
										}
										psg.destination = req.body.destination ? destination : psg.destination;
										psg.deslng = req.body.deslng ? deslng : psg.deslng;
										psg.deslat = req.body.deslat ? deslat : psg.deslat;
										psg.tips = req.body.tips ? tips : 0;
										psg.detail = req.body.detail ? detail : psg.detail;
										psg.createdvia = createdvia;
										psg.job_id = job_id;
										psg.status = "ON";
										psg.occupied = false;
										psg.drvarroundlist = drvlist;
										psg.updated = new Date().getTime();
										psg.createdjob = new Date().getTime();
										psg.save(function (err, response) {
											if (err) {
												res.json({
													status: false,
													msg: "error",
													data: err
												});
											} else {
												_joblistcreate(job_id, jobtype, drvlist, req);
												res.json({
													status: true,
													data: {
														passenger_ssid: response.device_id,
														passenger_jobid: response.job_id,
														passenger_status: response.status,
														taxi_duid: response.drv_id,
														taxi_list: response.drvarroundlist
													}
												});
											}
										});
									}
								}
							);
						}
					);
				}
			} else {
				res.json({
					status: false,
					msg: "passenger_jobid is already exists, please use the new one"
				});
			}
		})
};




exports.passengerfinishjob = function (req, res) {
	var device_id = req.params.passenger_ssid;
	var job_id = req.params.passenger_jobid;
	PassengerModel.findOneAndUpdate({
		device_id: device_id, cgroup: "samsung", status: "THANKS"
	},
		{
			job_id: null,
			curaddr: null,
			destination: null,
			tips: 0,
			drv_id: null,
			updated: new Date(),
			status: "OFF",
			occupied: false
		},
		{ upsert: false, new: true }, function (err, response) {
			if (err) {
				res.json({
					status: false,
					msg: "invalid error 500"
				});
			} else {
				if (response == null) {
					res.json({
						status: false,
						msg: "passenger ssid is invalid"
					});
				} else {
					_joblistupdate(job_id, req, 'datepsgthanks', '');
					res.json({
						status: true,
						data: {
							passenger_ssid: response.device_id,
							passenger_jobid: response.job_id,
							passenger_status: response.status,
							taxi_duid: response.drv_id
						}
					});
				}
			}
		}
	);
};




exports.passengercanceljob = function (socket) {
	return function (req, res) {
		var device_id = req.params.passenger_ssid;
		var job_id = req.params.passenger_jobid;
		PassengerModel.findOne(
			{ device_id: device_id, cgroup: "samsung", job_id: job_id }, 
			function (err, psg) {
				if (psg == null) {
					res.json({
						status: false,
						msg: "passenger ssid is invalid or passenger jobid is invalid"
					});
				} else {
					old_job_id = psg.job_id;
					if (psg.drv_id == '' || psg.drv_id == null) {
						psg.job_id = null;
						psg.drv_id = null;
						psg.updated = new Date();
						psg.deniedTaxiIds = [];
						psg.status = "OFF";
						psg.occupied = false;
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error01" });
							} else {
								_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel before drv');
								res.json({
									status: true,
									msg: "yes 1",
									data: {
										passenger_ssid: response.device_id,
										passenger_jobid: response.job_id,
										passenger_status: response.status,
										taxi_duid: response.drv_id
									}
								});
							}
						});
					} else {
						saveHistory(psg.drv_id, psg._id, psg.job_id, false);
						// for detected cancelled driver 
						canceldrvid = psg.drv_id;
						//
						psg.job_id = null;
						psg.drv_id = null;
						psg.updated = new Date();
						psg.deniedTaxiIds = [];
						psg.status = "OFF";
						psg.occupied = false;
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "cancelcall error02" });
							} else {
								_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel after drv');
								DriversModel.findOne(
									{ _id: canceldrvid }, 
									function (err, drv) {
										if (drv == null) {
											res.json({ status: false, msg: "cancelcall error03" });
										} else {
											drv.psg_id = null;
											drv.status = "ON";
											drv.save(function (err, result) {
												if (err) {
													res.json({ status: false, msg: "cancelcall error04" });
												} else {
													socket.emit("DriverSocketOn", result);
													res.json({
														status: true,
														msg: "cancelled",
														result: result,
														data: {
															passenger_ssid: response.device_id,
															passenger_jobid: response.job_id,
															passenger_status: response.status,
															taxi_duid: response.drv_id
														}
													});
												}
											});
										}
									}
								)
							}
						});
					}
				}
			}
		);
	};
};




exports.getsercharge = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		{ polygons: 0, Lat: 0, Long: 0, _id_garage: 0, searchpsgamount: 0, searchpsgradian: 0 },
		function (err, response) {
			if (response == null) {
				res.json({
					status: false,
					data: {}
				});
			} else {
				res.json({
					status: true,
					data: {
						psgfare: response.psgfare,
						ProvinceID: response.ProvinceID,
						Name: response._doc["Name" + (req.body.lang ? req.body.lang : "en")] || response._doc["Nameen"]
					}
				});
			}
		}
	);
}




///////////////////////////////// Driver SS API /////////////////////////////////////////
exports.driverreviewedjob = function (req, res) {

}




exports.driveraccepted = function (socket) {
	return function (req, res) {
		var passenger_ssid = req.params.passenger_ssid;
		var job_id = req.params.passenger_jobid;
		var device_id = req.body.device_id;
		var _id = req.body.taxi_duid;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

		PassengerModel.update(
			{ device_id: passenger_ssid, status: "ON", occupied: false },
			{ $set: { occupied: true } },
			{ new: true },
			function (err, tank) {
				if (err) return handleError(err);
				if (tank.nModified == 0) {
					res.json({
						status: false,
						msg: "งานนี้มีผู้รับไปแล้ว กรุณาเลือกงานใหม่ค่ะ"
					});
				} else {
					PassengerModel.findOne(
						{ device_id: passenger_ssid, cgroup: "samsung", status: { $in: ["ON"] } },
						function (err, result) {
							if (result == null) {
								//res.json({ status: false , msg:  "ผู้โดยสารท่านนี้ได้ถูกจองไปแล้ว กรุณาเลือกผู้โดยสารท่านใหม่", data:err });
								_getErrDetail('err008', function (errorDetail) {
									res.json({
										status: false,
										msg: errorDetail
									});
								});
							} else {
								DriversModel.findOne(
									{ _id: _id, device_id: device_id },
									function (err, drv) {
										if (drv == null) {
											res.json({
												status: false
											});
										} else {
											drv.psg_id = result.psg_id;
											drv.job_id = job_id;
											drv.status = "BUSY";
											drv.jobtype = result.jobtype;
											drv.updated = new Date();
											drv.save(function (err, responsedrv) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													PassengerModel.findOne(
														{ device_id: passenger_ssid, cgroup: "samsung" },
														{ status: 1, updated: 1, job_id: 1, _id: 1, device_id: 1, psgtype: 1, drv_id: 1 },
														function (err, psg) {
															if (psg == null) {
																res.json({ status: false, msg: "This passenger is not available." });
															} else {
																psg.drv_id = _id;
																psg.updated = new Date();
																psg.status = "BUSY";
																psg.save(function (err, response) {
																	if (err) {
																		res.json({ status: false, msg: "error", data: err });
																	} else {
																		socket.emit("PassengerSocketOn", response);
																		_drvjoblistupdate(job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvwait', '', curlng, curlat);
																		res.json({
																			status: true,
																			data: response
																		});
																	}
																});
															}
														}
													);
												}
											});
										}
									}
								);
							}
						}
					);
				}
			}
		)
	};
}




exports.driverpickedup = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var passenger_ssid = req.params.passenger_ssid;
		var job_id = req.params.passenger_jobid;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		PassengerModel.findOne(
			{ device_id: passenger_ssid, cgroup: "samsung" },
			function (err, psg) {
				if (psg == null) {
					res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
				} else {
					psg.drv_id = _id;
					psg.status = "PICK";
					psg.updated = new Date();
					psg.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							DriversModel.findOne(
								{ _id: _id, device_id: device_id },
								function (err, drv) {
									if (drv == null) {
										res.json({ status: false, msg: "Your driver does not exist. Please register and try again." });
									} else {
										if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
										if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
										if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
										drv.psg_id = psg_id;
										drv.updated = new Date();
										drv.status = "PICK";
										drv.save(function (err, result) {
											if (err) {
												res.json({ status: false, msg: "error", data: err });
											} else {
												socket.emit("PassengerSocketOn", response);
												_drvjoblistupdate(job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvpick', '', curlng, curlat);
												res.json({
													status: true,
													data: { status: response.status }
												});
											}
										});
									}
								}
							);
						}
					});
				}
			}
		);
	};
};




exports.driverdropped = function (socket) {
	return function (req, res) {
		// This API was called in the end of CC DPENDING ASSIGNED process
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var passenger_ssid = req.params.passenger_ssid;
		var job_id = req.params.passenger_jobid;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
		var jobsendby = "";
		PassengerModel.findOne(
			{ device_id: passenger_ssid, cgroup: "samsung" },
			function (err, psg) {
				if (psg == null) {
					res.json({ status: false, msg: "This passenger  does not exist. Please check the information." });
				} else {
					if (psg.status == "ON") {
						_joblistupdate(psg.job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvdrop', '', curlng, curlat);
						res.json({
							status: true,
							msg: "",
						});
					} else {
						psg.drv_id = null;
						psg.job_id = null
						psg.updated = new Date();
						psg.deniedTaxiIds = [];
						psg.status = "THANKS";
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false });
							} else {
								socket.emit("PassengerSocketOn", response);
								DriversModel.findOne(
									{ _id: _id, device_id: device_id },
									function (err, drv) {
										if (drv == null) {
											res.json({ status: false, msg: "Your phone does not exist. Please register and try again." });
										} else {
											drvsaveHistory(drv, psg_id, true);
											drv.psg_id = null;
											drv.job_id = null;
											drv.status = "ON";
											drv.updated = new Date();
											drv.save(function (err, result) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													_drvjoblistupdate(job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvdrop', '', curlng, curlat);
													res.json({
														status: true,
														data: { status: response.status }
													});
												}
											});
										}
									}
								);
							}
						});
					}
				}
			}
		);
	};
};




exports.drivercancelled = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var passenger_ssid = req.params.passenger_ssid;
		var job_id = req.params.passenger_jobid;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

		PassengerModel.findOne(
			{ device_id: passenger_ssid, cgroup: "samsung" },
			function (err, psg) {
				if (psg == null) {
					res.json({
						status: false,
						msg: "error - This passenger  does not exist. Please check the information. ",
						data: err
					});
				} else {
					if (psg.status == "ON") {
						_drvjoblistupdate(job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvcancel', 'driverCancelCall-ON', curlng, curlat);
						res.json({
							status: true,
							msg: "",
							//data: { status: response.status }
						});
					} else {
						psg.drv_id = null;
						psg.job_id = null;
						psg.status = "DRVDENIED";
						psg.updated = new Date();
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error", data: err });
							} else {
								DriversModel.findOne(
									{ _id: _id, device_id: device_id },
									function (err, drv) {
										if (drv == null) {
											//res.json({ status: false , msg: "Your phone does not exist. Please register and try again."});
											res.json({
												status: false
											});
										} else {
											if (req.body.curlng) { drv.curlng = req.body.curlng ? req.body.curlng : drv.curlng; }
											if (req.body.curlat) { drv.curlat = req.body.curlat ? req.body.curlat : drv.curlat; }
											if (curloc) { drv.curloc = curloc ? curloc : drv.curloc; }
											drv.psg_id = null;
											drv.job_id = null;
											drv.status = "ON";
											drv.updated = new Date();
											drv.save(function (err, result) {
												if (err) {
													res.json({ status: false, msg: "error", data: err });
												} else {
													socket.emit("PassengerSocketOn", response);
													_drvjoblistupdate(job_id, _id, device_id, drv.cgroup, drv.fname + ' ' + drv.lname, drv.phone, drv.carplate, 'datedrvcancel', 'driverCancelCall-DRVDENIED', curlng, curlat);
													res.json({
														status: true,
														msg: "Update driver and passenger to ON => driver cancelled passenger",
														data: { status: response.status }
													});
												}
											});
										}
									}
								);
							}
						});
					}
				}
			}
		);
	};
}



///////////////////////////////// common functions //////////////////////////////////////



function _joblistcreate(job_id, jobtype, drvarroundlist, req) {
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				var ProvinceID = 0;
				var NameEN = "undefined";
				var NameTH = "undefined";
				var ZoneID = 0;
				_do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID);
			} else {
				var ProvinceID = response.ProvinceID;
				var NameEN = response.NameEN;
				var NameTH = response.NameTH;
				var ZoneID = response.ZoneID;
				_do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID);
			}
		}
	);

	function _do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID) {
		PassengerModel.findOne(
			{ device_id: req.params.passenger_ssid, cgroup: "samsung" },
			function (err, psg) {
				if (psg == null) {
					console.log('_function_joblistcreate no device_id');
				} else {
					JoblistModel.create(
						{
							job_id: job_id,
							displayName: psg.displayName,
							psg_id: psg._id,
							psg_device_id: req.params.passenger_ssid,
							psg_phone: req.body.phone,
							curaddr: req.body.curaddr,
							curlng: req.body.curlng,
							curlat: req.body.curlat,
							curloc: [parseFloat(req.body.curlng), parseFloat(req.body.curlat)],
							destination: req.body.destination,
							deslng: req.body.deslng,
							deslat: req.body.deslat,
							tips: req.body.tips,
							detail: req.body.detail,
							promotioncode: req.body.promotioncode,
							datepsgcall: new Date().getTime(),
							createdvia: "SAMSUNG",
							favcartype: req.body.favcartype,
							drvarroundlist: drvarroundlist,
							status: "ON",
							jobtype: jobtype,
							ProvinceID: ProvinceID,
							NameEN: NameEN,
							NameTH: NameTH,
							ZoneID: ZoneID
						},
						function (err, record) {
							if (err) {
								console.log(err)
							} else {
								console.log(' success created SAMSUNG job ')
							}
						}
					);
				}
			}
		);
	}
}



function _joblistupdate(job_id, req, action, cwhere) {
	JoblistModel.findOne(
		{ job_id: job_id },
		function (err, job) {
			if (job == null) {
				console.log(' no job id')
			} else {
				switch (action) {
					case "datepsgaccept":
						job.status = "BUSY";
						job.datepsgaccept = new Date().getTime();
						break;
					case "datepsgthanks":
						job.status = "THANKS";
						job.datepsgthanks = new Date().getTime();
						break;
					case "datepsgcancel":
						job.status = "PSG_CANCELLED";
						job.datepsgcancel = new Date().getTime();
						job.psgcancelwhere = cwhere;
						break;
				}
				job.save(function (err, response) {
					if (err) {
						console.log(err)
					} else {
						console.log(' update job list ')
					}
				});
			}
		}
	);
}



function _drvjoblistupdate(job_id, drv_id, drv_device_id, cgroup, drv_name, drv_phone, drv_carplate, action, cwhere, curlng, curlat) {
	JoblistModel.findOne(
		{ job_id: job_id },
		function (err, job) {
			if (job == null) {
				//console.log (' no job id')
			} else {
				switch (action) {
					case "datedrvwait":
						job.drv_id = drv_id;
						job.drv_device_id = drv_device_id;
						job.drv_name = drv_name;
						job.drv_phone = drv_phone;
						job.drv_carplate = drv_carplate;
						job.cgroup = cgroup;
						job.status = "WAIT";
						job.datedrvwait = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "WAIT",
							created: new Date()
						});
						break;
					case "datedrvpick":
						job.status = "PICK";
						job.datedrvpick = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "PICK",
							created: new Date()
						});
						break;
					case "datedrvdrop":
						job.status = "DROP";
						job.datedrvdrop = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "DROP",
							created: new Date()
						});
						break;
					case "datedrvcancel":
						job.status = "DRV_CANCELLED";
						job.datedrvcancel = new Date().getTime();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "DRV_CANCELLED",
							created: new Date()
						});
						job.drvcancelwhere = cwhere;
						break;
				}
				job.save(function (err, response) {
					if (err) {
						console.log(err)
					} else {
						console.log(' update job list ')
					}
				});
			}
		}
	);
}



function drvsaveHistory(driverObj, passengerId, success) {
	// cancel job
	if (driverObj.jobtype) {
		if (driverObj.jobtype === 'HOTELS') {
			JoblisthotelModel.findOne({ _id: passengerId }).exec(function (err, job) {
				if (!err && job) {
					HistoryModel.find({
						_ownerId: driverObj._id,
						_callcenterId: job._id_callcenterpsg
					}).exec(function (err, exists) {
						if (!err && exists.length === 0) {
							HistoryModel.create({
								type: 'CALLCENTER',
								status: success ? 'SUCCESSED' : 'CANCELLED',
								_ownerId: driverObj._id,
								_callcenterId: job._id_callcenterpsg,
							}, function (err, history) {
								if (!err && history)
									console.log('history create success');
							});
						}
					});
				}
			});
		} else {
			HistoryModel.find({
				_ownerId: driverObj._id,
				_callcenterId: passengerId
			}).exec(function (err, exists) {
				if (!err && exists.length === 0) {
					HistoryModel.create({
						type: 'CALLCENTER',
						status: success ? 'SUCCESSED' : 'CANCELLED',
						_ownerId: driverObj._id,
						_callcenterId: passengerId,
					}, function (err, history) {
						if (!err && history)
							console.log('history create success');
					});
				}
			});
		}
	}
	// passenger job
	else {
		JoblistModel.findOne({ job_id: driverObj.job_id }).select('psg_id').exec(function (err, job) {
			if (!err, job) {
				HistoryModel.find({
					_ownerId: driverObj._id,
					_passengerId: job.psg_id,
					_passengerJobDetail: job._id
				}).exec(function (err, exists) {
					if (!err && exists.length === 0) {
						HistoryModel.create({
							type: 'PASSENGER',
							status: success ? 'SUCCESSED' : 'CANCELLED',
							_ownerId: driverObj._id,
							_passengerId: job.psg_id,
							_passengerJobDetail: job._id
						}, function (err, history) {
							if (!err && history)
								console.log('history create success');
						});
					}
				});
			}
		});
	}
}



function saveHistory(driverId, passengerId, job_id, success) {
	JoblistModel.findOne({ job_id: job_id }).select('psg_id').exec(function (err, job) {
		if (!err, job) {
			HistoryModel.find({
				_ownerId: driverId,
				_passengerId: passengerId,
				_passengerJobDetail: job._id
			}).exec(function (err, exists) {
				if (!err && exists.length === 0) {
					HistoryModel.create({
						type: 'PASSENGER',
						status: success ? 'SUCCESSED' : 'CANCELLED',
						_ownerId: driverId,
						_passengerId: passengerId,
						_passengerJobDetail: job._id
					}, function (err, history) {
						if (!err && history)
							console.log('history create success');
					});
				}
			});
		}
	});
}



// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}


function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}


function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);
		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};


function _getErrDetail(lang, errCode, callback) {
	ErrorcodeModel.findOne(
		{ errcode: errCode },
		function (err, response) {
			if (response == null) {
				callback('');
			} else {
				callback(response._doc["err" + (lang ? lang : "en")] || response._doc["erren"]);
			}
		}
	);
};