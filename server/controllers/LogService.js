var config = require('../../config/func').production ;
var mongoose = require('mongoose');
var JoblistModel = mongoose.model('JoblistModel');
var PsgcalllogModel  = mongoose.model('PsgcalllogModel');
var CallcenterpsgModel  = mongoose.model('CallcenterpsgModel');
var DriverModel  = mongoose.model('DriversModel');
var moment = require('moment');
var fs = require('fs');


var CallcenterLogUtil = function() {

	this.getViewModeFormat = function($date, viewMode) {

		var result = "";

		switch (viewMode) {

			case 'hour':
				result = {
					$dateToString: { format: "%Y-%m-%d %H:00", date: { $subtract: [$date, -25200000] }},
				};
				break;

			case 'day':
				result = { 
					$dateToString: { format: "%Y-%m-%d", date: { $subtract: [$date, -25200000] }},
				};
				break;

			case 'week':
				result = {
					week: { $week: { $subtract: [$date, -25200000] } },
					year: { $year: { $subtract: [$date, -25200000] } },
				};
				break;

			case 'month':
				result = {
					$dateToString: { format: "%Y-%m", date: { $subtract: [$date, -25200000] }},
				};
				break;

			default: 
				result = { 
					$dateToString: { format: "%Y-%m-%d", date: { $subtract: [$date, -25200000] }},
				};
				break; 
		}

		return result;
	}

	this.createCondition = function(startTime, endTime, logType) {

		var conditions = {};

		if(startTime && endTime) {
			conditions.created = { $gt: new Date(startTime), $lt: new Date(endTime) }
		}

		switch (logType) {

			case 'registered':
				conditions.drv_id = { $nin: [ "" , null ] };
				conditions.status = { $ne: "DELETED" };
			break;

			case 'unregister':
				conditions.drv_id = { $in: [ "" , null ] };
				conditions.status = { $ne: "DELETED" }; 
			break;

			case 'deleted':
				conditions.status = "DELETED";
			break;
		}

		return conditions;
	}

	this.createConditionGroup = function(conditions, cgroup) {
		if( cgroup !== undefined && cgroup instanceof Array && cgroup.length > 0) {
			conditions.cgroup = { $in: cgroup }
		}
		else if (cgroup !== undefined && typeof cgroup === 'string') {
			conditions.cgroup = { $in: [ cgroup ] }
		}

		return conditions;
	}

	this.createGroup = function(viewMode) {
		return {
			_id: this.getViewModeFormat("$created", viewMode),
			count: { $sum: 1 }
		}
	}
};


var MobileAppLogUtil = function() {

	this.getViewModeFormat = function(dateField, viewMode) {

		var result = "";
		$date = { $subtract: [("$" + dateField), -25200000] };

		switch (viewMode) {

			case 'hour':
				result = {
					$dateToString: { format: "%Y-%m-%d %H:00", date: $date },
				};
				break;

			case 'day':
				result = { 
					$dateToString: { format: "%Y-%m-%d", date: $date },
				};
				break;

			case 'week':
				result = {
					week: { $week: $date },
					year: { $year: $date },
				};
				break;

			case 'month':
				result = {
					$dateToString: { format: "%Y-%m", date: $date },
				};
				break;

			default: 
				result = { 
					$dateToString: { format: "%Y-%m-%d", date: $date },
				};
				break; 
		}

		return result;
	}

	this.createCondition = function(startTime, endTime, logType) {

		var conditions = {};

		if(startTime && endTime) {
			conditions.created = { $gt: new Date(startTime), $lt: new Date(endTime) }
		}

		var dateField = this.getDateField(logType);
		if( dateField && dateField !== "created" ) {
			conditions[dateField] = { $exists: true };
		}

		return conditions;
	}

	this.createConditionGroup = function(conditions, cgroup, province, logType, appRequest) {


		if( appRequest === 'admin' ) {
			if(
				logType !== "all"
				&& logType !== "psgcancel"
				&& logType !== undefined 
				&& cgroup !== undefined) {
					if(typeof cgroup === "string") {
						cgroup = [ cgroup ];
					}
					conditions.cgroup = { $in: cgroup }
			}
		} else if( appRequest === 'callcenter') {
			if(
				logType !== "all" 
				&& logType !== undefined 
				&& cgroup !== undefined) {
					if(typeof cgroup === "string") {
						cgroup = [ cgroup ];
					}
					conditions.cgroup = { $in: cgroup }
			}
		}


		if( province !== undefined && province instanceof Array && province.length > 0) {
			conditions.ProvinceID = { $in: province }
		}

		return conditions;
	}

	this.createGroup = function(viewMode, logType) {

		var dateField = this.getDateField(logType);

		return {
			_id: this.getViewModeFormat(dateField, viewMode),
			count: { $sum: 1 }
		}
	}

	this.getDateField = function(logType) {

		var dateField = "";

		switch (logType) {

			case 'all':
				dateField = "created";
			break;

			case 'wait':
				dateField = "datedrvwait";
			break;

			case 'accept':
				dateField = "datepsgaccept"; 
			break;

			case 'pick':
				dateField = "datedrvpick";
			break;

			case 'drop':
				dateField = "datedrvdrop";
			break;

			case 'psgcancel':
				dateField = "datepsgcancel";
			break;

			case 'drvcancel':
				dateField = "datedrvcancel";
			break;
		}

		return dateField;
	}
};


var PassengerLogUtil = function() {

	this.createCondition = function(startTime, endTime, cgroup) {

		var conditions = {
			created: { $gt: new Date(startTime), $lt: new Date(endTime) }
		};

		if(cgroup) {
			conditions.cgroup = { $in: cgroup }
		}

		return conditions;
	}
};


exports.getCallcenterLog = function(req, res) {

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);
	var cgroup = req.body.cgroup;
	var viewMode = req.body.viewMode;
	var logType = req.body.logType;

    var util = new CallcenterLogUtil();

    var conditions = util.createCondition(startTime, endTime, logType);
    conditions = util.createConditionGroup(conditions, cgroup, logType);

    var grouping = util.createGroup(viewMode);

    var aggregate = [

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group: grouping
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ];

    var onSuccess = function (err, result) {
       if(err) {
           res.json({ status: false , data: err  });      
       } else {
            res.json({ 
                status: true,
                data: result
            });
        }
    };

    CallcenterpsgModel.aggregate(aggregate, onSuccess);
};


exports.getMobileLog = function(req, res) {

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);
	var cgroup = req.body.cgroup;
	var viewMode = req.body.viewMode;
	var logType = req.body.logType;
	var province = req.body.province;

	if(province) {
		province.forEach(function(el, index) {
			province[index] = parseInt(el);
		});
	}

    var util = new MobileAppLogUtil();

    var conditions = util.createCondition(startTime, endTime, logType);
    conditions = util.createConditionGroup(conditions, cgroup, province, logType);

    var grouping = util.createGroup(viewMode, logType);
    var aggregate = [

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group: grouping
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ];
    
    var onSuccess = function (err, result) {
       if(err) {
           res.json({ status: false , data: err  });      
       } else {
            res.json({ 
                status: true,
                data: result,
                count: result.length
            });    
        }
    };

    JoblistModel.aggregate(aggregate, onSuccess);
};


exports.PsgCallLog = function(req, res) {

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);
	var cgroup = req.body.cgroup;

    var util = new PassengerLogUtil();

    var conditions = util.createCondition(startTime, endTime, cgroup);

    var resultFields = { "_id": 1, "psg_phone": 1, "drv_phone": 1, "drv_carplate": 1, "callby": 1, "created": 1 , "cgroup": 1, "province": 1 };

    var sorting = { "sort": { "created" : -1 } };

	PsgcalllogModel.find(conditions, resultFields, sorting, function(err,result) {
    	if(result === 0) {
    		res.json({
    			status: false
    		});             
    	}
    	else {
    		res.json({
    			status: true,
    			data: result
    		});
    	}
    });
};


exports.JoblistLog = function (req, res) {

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);
	var cgroup = req.body.cgroup;
	var viewMode = req.body.viewMode;
	var logType = req.body.logType;
	var province = req.body.province;

	if(province) {
		province.forEach(function(el, index) {
			province[index] = parseInt(el);
		});
	}

    var util = new MobileAppLogUtil();

    var conditions = util.createCondition(startTime, endTime, logType);
    conditions = util.createConditionGroup(conditions, cgroup, province, logType);

    var aggregate = [

        // conditions
        {
            $match: conditions
        },

        // project
        { 
        	$project : { 
        		_id:0, 
        		drvcancelwhere:1, 
        		datedrvcancel:1, 
        		psgcancelwhere:1, 
        		datepsgcancel:1, 
        		datepsgthanks:1, 
        		datedrvdrop:1, 
        		datedrvpick:1, 
        		datepsgaccept:1, 
        		datedrvwait:1, 
        		datepsgcall:1, 
        		destination:1, 
        		curaddr:1, 
        		drv_carplate:1, 
        		drv_phone:1, 
        		drv_name:1, 
        		psg_phone:1,
        		cgroup: 1,
        		NameTH: 1 } 
    	},

        // sorting
        {
            $sort: { created: -1 }
        }
    ];

    var callback = function (err, result) {
       if(err) {
           res.json({ status: false , data: err  });      
       } else {
            res.json({ 
                status: true,
                data: result,
                count: result.length
            });    
        }
    };

    JoblistModel.aggregate(aggregate, callback);
}


exports.getTaxiUsageList = function(req, res) {

    // example: http://stackoverflow.com/questions/22932364/mongodb-group-values-by-multiple-fields
    // plus : http://www.mkyong.com/mongodb/mongodb-aggregate-and-group-example/

    var cgroup = req.body.cgroup;
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);

    if(!cgroup) {
    	res.status(200).json({ status: false, message: "cgroup is not defined." });
    }

    else {
    	
	    var conditions = {};

	    if(startTime && endTime) {
	    	conditions.created = { $gte: new Date(startTime), $lte: new Date(endTime) };
	    }
	    conditions.drv_id = { $ne: '' };
	    conditions.cgroup = cgroup;

	    var aggregate = [
	    		{
	    			"$match": conditions
	    		},
		    	{ "$group": 
				    {
				    	"_id": {
				    		"drv_id": "$drv_id",
				    		"drv_name": "$drv_name",
				    		"drv_phone": "$drv_phone",
							"psg_phone": "$psg_phone",
				    		"drv_carplate": "$drv_carplate",
				    		"curaddr": "$curaddr",
				    		"destination": "$destination",
				    		"curloc": "$curloc",
				    		"created": "$created",
								"datepsgcall": "$datepsgcall",
								"datepsgcancel": "$datepsgcancel",
								"datedrvdrop": "$datedrvdrop",
								"datedrvpick": "$datedrvpick",
								"datepsgaccept": "$datepsgaccept",
								"datedrvwait": "$datedrvwait",
								"datedrvcancel": "$datedrvcancel"
				    	},
				    	"jobCount": { "$sum": 1 },
				    }
				},
			    {"$sort": { "_id.created": -1 } },
				{ "$group": 
					{
						"_id": { 
							"drv_id": "$_id.drv_id",
							"name": "$_id.drv_name",
							"phone": "$_id.drv_phone",
							"carplate": "$_id.drv_carplate"
						},
						"joblist": { 
							"$push": { 
								"psg_phone": "$_id.psg_phone",
								"curaddr": "$_id.curaddr",
								"destination": "$_id.destination", 
								"curloc": "$_id.curloc",
								"created": "$_id.created",
								"datepsgcall": "$_id.datepsgcall",
								"datepsgcancel": "$_id.datepsgcancel",
								"datedrvdrop": "$_id.datedrvdrop",
								"datedrvpick": "$_id.datedrvpick",
								"datepsgaccept": "$_id.datepsgaccept",
								"datedrvwait": "$_id.datedrvwait",
								"datedrvcancel": "$_id.datedrvcancel",
							},
						},
					"jobCount": { "$sum": "$jobCount" }
				}
			},
			{ "$sort": { "jobCount": -1 } }
		];

		JoblistModel.aggregate(aggregate).exec(function(err, result) {
			if(err) {
				res.json({ status: false , data: err  });
			} else {
				res.json({ status: true, data: result });    
			}
		});
    }
}


exports.getCallcenterUsageList = function(req, res) {

    // example: http://stackoverflow.com/questions/22932364/mongodb-group-values-by-multiple-fields
    // plus : http://www.mkyong.com/mongodb/mongodb-aggregate-and-group-example/

    var cgroup = req.body.cgroup;
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);

    if(!cgroup) {
    	res.status(200).json({ status: false, message: "cgroup is not defined." });
    }

    else {

	    var conditions = {};

	    if(startTime && endTime) {
	    	conditions.created = { $gte: new Date(startTime), $lte: new Date(endTime) };
	    }
	    conditions.cgroup = cgroup;

	    var aggregate = [
	    		{
	    			"$match": conditions
	    		},
		    	{ "$group": 
				    {
				    	"_id": {
				    		"drv_carplate": "$drv_carplate",
				    		"curaddr": "$curaddr",
				    		"destination": "$destination",
				    		"created": "$created",
				    		"drv_id": "$drv_id"
				    	},
				    	"jobCount": { "$sum": 1 },
				    }
				},
			    {"$sort": { "_id.created": -1 } },
				{ "$group": 
					{
						"_id": {
							"carplate": "$_id.drv_carplate"
						},
						"joblist": { 
							"$push": { 
								"drv_id": "$_id.drv_id",
								"carplate": "$_id.drv_carplate",
								"curaddr": "$_id.curaddr",
								"destination": "$_id.destination",
								"created": "$_id.created"
							},
						},
					"jobCount": { "$sum": "$jobCount" }
				}
			},
			{ "$sort": { "jobCount": -1 } }
		];

		CallcenterpsgModel.aggregate(aggregate, function (err, result) {
			if(err) {
				res.json({ status: false , data: err  });
			} else {
				res.json({ 
					status: true,
					data: result,
					carplate_Count: result.length
				});    
			}
		});
    }
}


exports.getAllJob = function(req, res, next) {

    var startTime = new Date(parseInt(req.body.startTime));
    var endTime = new Date(parseInt(req.body.endTime));
    var cgroup = req.body.cgroup;

    CallcenterpsgModel.count({

    	"created": { $gte: startTime, $lt: endTime },
    	"cgroup": cgroup

    }).exec(function(err, count) {

    	if(err)
    		res.status(200).json({ status: false, message: err });

    	req.data = {
    		count: count
    	}

    	next();
    });
}


exports.MostHitHours = function(req, res, next) {

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);
	var cgroup = req.body.cgroup;

	var conditions = {};

	if(startTime && endTime) {
		conditions.created = { $gte: new Date(startTime), $lt: new Date(endTime) };
	}
	conditions.cgroup = cgroup;

	CallcenterpsgModel.aggregate([
	{
		$match: conditions
	},
	{ 
		"$project": {
			"y":{"$year":"$created"},
			"m":{"$month":"$created"},
			"d":{"$dayOfMonth":"$created"},
			"h":{"$hour":"$created"},
			"device_id":1
		}
	},
	{ "$group":{ 
		"_id": { "year":"$y","month":"$m","day":"$d","hour":"$h"},
		"total":{ "$sum": 1}
	}},
	],function (err, result) {

		if(err)
			res.status(200).json({ status: false, message: err });

		req.data = {
			MostHitHours: result[0]
		};

		next();
	});
}


exports.MostHitStartPlace = function(req, res, next) {

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);
	var cgroup = req.body.cgroup;

	var conditions = {};

	if(startTime && endTime) {
		conditions.created = { $gte: new Date(startTime), $lt: new Date(endTime) };
	}
	conditions.curaddr = { $ne: '' };
	conditions.cgroup = cgroup;

	CallcenterpsgModel.aggregate([
		{ $match: conditions  },
		{ $group: {_id: "$curaddr" , count: {$sum : 1 }}},
		{ $sort: { count: -1 }},
		], function (err, result) {

			if(err)
				res.status(200).json({ status: false, message: err });

			req.data.MostHitStartPlace = result[0];
			next();
		});
}


exports.MostHitDestinationPlace = function(req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {};

    if(startTime && endTime) {
    	conditions.created = { $gte: new Date(startTime), $lt: new Date(endTime) };
    }
    conditions.destination = { $ne: '', $ne: '-' };
    conditions.cgroup = cgroup;

   CallcenterpsgModel.aggregate([
        { $match: conditions },
        { $group: {_id: "$destination" , count: { $sum : 1 }}},
        { $sort: { count: -1 }},
    ], function (err, result) {

    	if(err)
    		res.status(200).json({ status: false, message: err });

    	req.data.MostHitDestinationPlace = result[0];
    	res.status(200).json({
    		status: true,
    		data: req.data
    	});
    });
}


exports.hcData = function(req, res) {

	if(!req.body.logType)
		res.status(200).json({ status: false, message: "logType is undefined." });

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);

	var logType = req.body.logType;
	var cgroup = req.body.cgroup;
	var province = req.body.province;
	var appRequest = req.params.appRequest || 'callcenter';

    var util = new MobileAppLogUtil();

	if(province) {
		province.forEach(function(el, index) {
			province[index] = parseInt(el);
		});
	}

    var conditions = util.createCondition(startTime, endTime, logType);
    conditions = util.createConditionGroup(conditions, cgroup, province, logType, appRequest);

    var format = "%Y-%m-%d";
    if(startTime && endTime) {
    	// within 1 hour.
    	if((endTime - startTime) <= (1000 * 3600 * 24)) {
    		format = "%Y-%m-%d %H:%M"
    	}
    	// within 7 days
    	else if (( endTime - startTime ) <= 1000 * 3600 * 24 * 7) {
    		format = "%Y-%m-%d %H:00";
    	}
    	// more than 7 days
    	else if (( endTime - startTime ) > 1000 * 3600 * 24 * 7) {
    		format = "%Y-%m-%d";
    	}
    }

	JoblistModel.aggregate([
	{
		"$match": conditions,
	},
	{	
		"$group": {
			"_id": {
				"$dateToString": {
					"date": "$created",
					"format": format,
				},
			},
			"count": {
				"$sum": 1
			}
		}
	},
	{
		"$sort": {
			"_id": 1
		}
	}
	]).exec(function(err, result) {

		if(err)
			res.status(500).json(err);

		if(result) {

			var total = 0;

			result.forEach(function(r) {
				total += r.count;
			});

			res.status(200).json({
				result: result,
				total: total
			});
		}
	});
}


exports.hcDataCallcenter = function(req, res) {

	if(!req.body.logType)
		res.status(200).json({ status: false, message: "logType is undefined." });

	var startTime = parseInt(req.body.startTime);
	var endTime = parseInt(req.body.endTime);

	var logType = req.body.logType;
	var cgroup = req.body.cgroup;
	var province = req.body.province;

    var util = new CallcenterLogUtil();

    var conditions = util.createCondition(startTime, endTime, logType);
    conditions = util.createConditionGroup(conditions, cgroup, logType);

    var format = "%Y-%m-%d";
    if(startTime && endTime) {
    	// within 1 hour.
    	if((endTime - startTime) <= (1000 * 3600 * 24)) {
    		format = "%Y-%m-%d %H:%M"
    	}
    	// within 7 days
    	else if (( endTime - startTime ) <= 1000 * 3600 *24 * 7) {
    		format = "%Y-%m-%d %H:00";
    	}
    	// more than 7 days
    	else if (( endTime - startTime ) > 1000 * 3600 *24 * 7) {
    		format = "%Y-%m-%d";
    	}
    }

	CallcenterpsgModel.aggregate([
	{
		"$match": conditions,
	},
	{	
		"$group": {
			"_id": {
				"$dateToString": {
					"date": "$created",
					"format": format,
				},
			},
			"count": { "$sum": 1 }
		}
	},
	{
		"$sort": {
			"_id": 1
		}
	}
	]).exec(function(err, result) {

		if(err)
			res.status(500).json(err);

		if(result) {

			var total = 0;

			result.forEach(function(r) {
				total += r.count;
			});

			res.status(200).json({
				result: result,
				total: total
			});
		}
	});
}


exports.countTaxiTaskByGarage = function(req, res) {

	DriverModel.find({ cgroup: req.body.cgroup || req.params.cgroup }, { _id: 1 }, function(err, drivers) {

		if(err)
			res.status(500).json({ message: err });

		drivers = drivers.map(function(driver) { return driver._id.toString(); });

		JoblistModel.aggregate([ 
			{ "$match" : { "drv_id" : { "$in": drivers }, "created": { "$gte": new Date("2016-06-01"), "$lt": new Date("2016-07-01") } } },
			// { "$project": { "yearMonthDay": { "$dateToString": { format: "%Y-%m-%d", date: "$created" } }, "$drv_carplate": 1 } },
			{ "$group": { "_id": { "date": { "$dateToString": { format: "%Y-%m-%d", date: "$created" } }, "name": "$drv_name" }, "count": { "$sum": 1 } } },
			{ "$sort": { "_id.date": 1 } }
		], function(err, jobs) {

					var jsonArr = [];
					var daysInMonth = moment().daysInMonth();

				drivers.forEach(function(driver, index) {

					JoblistModel.aggregate([
						{ '$match': { 'drv_id': driver.toString() } },
						{ '$group': { '$dateToString': { format: "%Y-%m-%d", date: "$created" } } }
					], function(err, result) {

					var template = {
						'carplate': '',
					};
					
					for (var i = 1; i <= daysInMonth; i++) {
						template[i] = '';
					}

					});

					jsonArr.push(template);
					console.log(jsonArr);
				});
				
				res.status(200).json({ data: jobs });


				// if(err)
				// 	res.status(500).json({ message: err });

				// if(req.params.responseType === 'json') {

				// 	res.status(200).json({ data: jobs });

				// } else if(req.params.responseType === 'xlsx') {
				// } else {
				// 	res.status(200).json({ message: 'cannot response current type.' });
				// }
		});
	});
}