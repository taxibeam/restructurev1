////////////////////////////////////
// TaxiBeam Garage API
// version : 1.0.0.1 
// Date August 16, 2015 
// Crated by Hanzen
////////////////////////////////////

var Url = require('url'),
	mongoose  = require('mongoose'),
  	PassengerModel  = mongoose.model('PassengerModel'),
  	UserModel  = mongoose.model('UserModel'),
 	PathLogModel = mongoose.model('PathLogModel'),
	DriversModel = mongoose.model('DriversModel'),
	CommentModel  = mongoose.model('CommentModel'),
	AnnounceModel	 = mongoose.model('AnnounceModel'),
	EmgListModel = mongoose.model('EmgListModel'),
	ErrorcodeModel =  mongoose.model('ErrorcodeModel'),
	Lk_garageModel = mongoose.model('Lk_garageModel'),
	PoirecommendedModel = mongoose.model('PoirecommendedModel'),
	mailer = require("nodemailer");

// for upload file
var 	path = require('path'),
	formidable = require('formidable'),
	util = require('util'),
	fs = require('fs-extra'),
 	qt = require('quickthumb'),
	http = require('http'),
	qs = require('querystring');

// for all API
var 	_id = "",
	device_id = "",
	smsconfirm = "",
	newimgupload = "",
	bupload = "",
	imgtype = "",
	curloc = "",
	cartype = "",
	radian = "5000",		// in meters
	amount = "",
	psg_id = "",
	commtype = "",
	topic = "",
	comment = "",
	favcartype = "",
	favtaxi = "",
	radian = "",
	amount = "",
	curaddr = "",
	destination = "",
	desloc = "",
	des_dist = "",
	tips = "",
	detail = "",
	favcartype = "",
	taxi_id = "",
	emgtype = "";

// Test javascript at "http://js.do/"
function ranSMS(){
    var str = Math.random();
    var str1 = str.toString();
	var res = str1.substring(2,6);
	return res;
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function tryParseJSON (jsonString){
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
        if (o && typeof o === "object" && o !== null) {
            return o;
        }
    }
    catch (e) { }
    return false;
};


function _pathLog(data,res){
	//console.log(data);
	//data.timestamp = data.timestamp
	PathLogModel.create(data)
}


function _getErrDetail (errid) {
	ErrorcodeModel.findOne( 
		{ errid: errid },
		{ erren:1, errth:1 }, 		
		function(err, response) {
		    	if (response==null) { 
		    		return "";
		    	}  else  {
			    	
				return response[0];
		    	}	    	
		}
	);
};



// Use Smtp Protocol to send Email
var smtpTransport = mailer.createTransport({
	service: "Gmail",
	auth: {
		user: "krit@ecartstudio.com",
		pass: "********"
	}
});


var mail = {
	from: "Krit Lor <krit@ecartstudio.com>",
	to: "hanzen@hotmail.com",
	subject: "Send Email Using Node.js",
	text: "Node.js New world for me",
	html: "<b>Node.js New world for me</b>"
}




// NOTE *******************************  Start a new API
// the new API service for GARAGE
exports.userlogin = function(req, res) {
// check Email & Password
email = req.body.email;
password = req.body.password;
	GarageModel.findOne(
		{ email: email, password: password }, 
		function(err, grg) {	    	
		    if(drv == null) { 
				res.json({ status: false , msg: "invalid email or password" });
		    } else { 
				res.json({ 
					status: true,
					msg:  "Welcome to Taxi-Beam" ,	
					data: { 
						_id: grg._id, 
						status: grg.status,
						active: grg.active
					} 
				});
			}
		}
	);
};





exports.forgetpwd = function(req, res) {
// http://stackoverflow.com/questions/4113701/sending-emails-in-node-js
email = req.body.email;
	GarageModel.findOne(
		{ email: email }, 
		function(err, grg) {	    	
		    if(drv == null) { 
				res.json({ status: false , msg: "Your email address is invalid, please try again." });
		    } else { 
			    smtpTransport.sendMail(mail, function(error, response){
			        if(error){
			            console.log(error);
			        }else{
			            console.log("Message sent: " + response.message);
			        }

			        smtpTransport.close();
			    });
				res.json({ 
					status: true,
					msg:  "Please check your email"
				});
			}
		}
	);
};




exports.testsendmail = function(req, res) {
// http://stackoverflow.com/questions/4113701/sending-emails-in-node-js
	smtpTransport.sendMail(mail, function(error, response){
		if(error){
	        		console.log(error);
				res.json({ 
					status: false,
					msg:  "cannot send mail",
					data: error
				});
	    	}else{
	       		 console.log("Message sent: " + response.message);
				res.json({ 
					status: true,
					msg:  "Please check your email"
				});
	   	}
	    	smtpTransport.close();
	});
};




exports.showcars = function(req, res) {
_id = req.body._id			;
device_id = req.body.device_id		;
curlng = req.body.curlng		;
curlat = req.body.curlat			;
curloc = [parseFloat(req.body.curlng),parseFloat(req.body.curlat)];
	if (typeof curlng == 'undefined' && curlng == null) {		
		res.json({ status: false, msg: "current longitude is not valid" })
		return;	
	}
	if (typeof curlat == 'undefined' && curlat == null) {		
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	CarsModel.find(
		{ curloc : { $near : curloc } },
		{ device_id:0, curloc:0, desloc:0, updated:0, created:0 },
		{ limit : amount },
		function(err,carlist){
			// Donot forget to create 2d Index for drivers collection : curloc!!!!	
			if(carlist == 0){
				res.json({					
					status: false, 
					msg: ""
				});									
			} else {
				res.json({				
					status: true, 
					msg: "", 
					data: carlist
				});				
			}
		}
	);
};




exports.searchcars = function(req, res) {
_id = req.body._id			;
keyword = req.body.keyword		;
	CarsModel.find(
		{ 						
			curloc : { $near : curloc }, 
		},
		{ device_id:0, curloc:0, desloc:0, updated:0, created:0 },
		{limit : amount},
		function(err,carlist){
			// Donot forget to create 2d Index for drivers collection : curloc!!!!	
			if(carlist == 0){
				res.json({					
					status: false, 
					msg: ""
				});									
			} else {
				res.json({				
					status: true, 
					msg: "", 
					data: carlist
				});				
			}
		}
	);
};




exports.getdrvinfo = function(req, res) {
_id = req.body._id	;
	DriversModel.findOne(
		{ _id : _id, active: "Y"}, 
		{ _id:0, device_id: 0, nname:0, curloc:0, imgczid:0, workperiod:0, workstatus:0, car_id:0, grg_id:0, updated:0, created:0, smsconfirm:0 },
		function(err, drv) {		
	    		if(drv == null) { 
				//res.json({ status: false , msg: " ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง" });
				_getErrDetail('err009', function (errorDetail){
					res.json({ 
						status: false, 
						msg: errorDetail
					});
				});				
	    		} else {
				res.json({ 
					status: true , 
					msg:  "", 
					data: drv
				});
			}
		}
	);
};	




exports.getdrvhistory = function(req, res) {

}




exports.getjobinfo = function(req, res) {

}




exports.getpathinfo = function(req, res) {

}




exports.getdrvlist = function(req, res) {

}




exports.updatedrvinfo = function(req, res) {

}




exports.getcarlist = function(req, res) {

}




exports.getcarinfo = function(req, res) {
_id = req.body._id	;
	CarsModel.findOne(
		{ _id : _id, active: "Y"}, 
		{ _id:0, device_id: 0, nname:0, curloc:0, imgczid:0, workperiod:0, workstatus:0, car_id:0, grg_id:0, updated:0, created:0, smsconfirm:0 },
		function(err, car) {		
	    		if(car == null) { 
				//res.json({ status: false , msg: " ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง" });
				_getErrDetail('err009', function (errorDetail){
					res.json({ 
						status: false, 
						msg: errorDetail
					});
				});				
	    		} else {
				res.json({ 
					status: true , 
					msg:  "", 
					data: car
				});
			}
		}
	);
};




exports.updatecarinfo = function(req, res) {

}




exports.senddrvmsg = function(req, res) {

}




exports.changepwd = function(req, res) {

}




exports.statsearch = function(req, res) {

}




exports.insertcars = function(req, res) {

}




exports.getSettings = function(req, res) {
	
	var garageId =  req.body.garageId;
	
	Lk_garageModel
		.findOne({ _id: garageId })
			.select('ccwaitingtime searchpsgamount searchpsgradian broadcast allow_adjust_poi bidjobwaitingtime bidjobradian')
			.exec(function(err, garage) {

                if(err) {
                    res.status(200).json({
                        status: true,
                        message: "Fail",
                        data: err
                    });
                }
				
				res.status(200).json({
					status: true,
					message: "Success",
					data: garage
				});
			});
}




exports.saveSettings = function(req, res) {
    
	var garageId =  req.body.garageId;

	Lk_garageModel
		.findOne({ _id: garageId })
		.exec(function(err, garage) {

            if(err || !garage) {
                res.status(200).json({
                	status: true,
                    message: "Fail",
                    data: err
                });
            }

            garage.ccwaitingtime = req.body.ccwaitingtime;
            garage.searchpsgamount = req.body.searchpsgamount;
            garage.searchpsgradian = req.body.searchpsgradian;
			garage.bidjobwaitingtime = req.body.bidjobwaitingtime;
			garage.bidjobradian = req.body.bidjobradian;
			
            garage.broadcast = req.body.broadcast;
            garage.allow_adjust_poi = req.body.allow_adjust_poi;

            garage.save(function(err, result) {

                if(err) {
                    res.status(200).json({
                        status: false,
                        message: "Fail",
                        data: err
                    });
                }

                res.status(200).json({
                    status: true,
                    message: "Success",
                    data: result
                })
            });
        });
}




exports.addNewPOI = function(req, res) {

	if(!req.body.parkingname 
		|| !req.body.curloc instanceof Array 
		|| req.body.curloc.length != 2) {
			res.json({ status: false, msg: 'ข้อมูลที่ส่งมาไม่ถูกต้อง'});
			return;
		}

	var query = { 'parkingname': req.body.parkingname.trim(), 'cgroup': req.user.local.cgroup };
	PoirecommendedModel.findOne(query, function(err, poi) {
		if(err || poi) {
			res.json({ status: false, data: err || 'มีจุดชื่อนี้อยู่แล้ว'});
			return;
		} else {
			PoirecommendedModel.create({ 
				cgroup: req.user.local.cgroup,
				parkingname: req.body.parkingname, 
				description: req.body.description,
				curloc: [parseFloat(req.body.curloc[0]), parseFloat(req.body.curloc[1])],
				created: Date.now()
			}, function(err, result) {
				if(err) {
					res.json({ status: false, data: err });
					return;
				}
				res.json({ status: true, data: result });
			});
		}
	});
}




exports.editPOI = function (req, res) {

	if (!req.body._id
		|| !req.body.parkingname
		|| !req.body.curloc instanceof Array
		|| req.body.curloc.length != 2) {
		res.json({ status: false, msg: 'ข้อมูลที่ส่งมาไม่ถูกต้อง' });
		return;
	}

	PoirecommendedModel.findOne({ _id: { $ne: req.body._id }, cgroup: req.user.local.cgroup, parkingname: req.body.parkingname }).exec(function (err, alreadyPOI) {

		if (err || alreadyPOI != null) {
			res.json({ status: false, data: err ? err : 'มีจุดชื่อนี้อยู่แล้ว' });
			return;
		}

		var query = { _id: req.body._id };
		PoirecommendedModel.findOneAndUpdate(query, {
			curloc: [parseFloat(req.body.curloc[0]), parseFloat(req.body.curloc[1])],
			parkingname: req.body.parkingname,
			description: req.body.description,
			updated: Date.now()
		}, { upsert: false, new: true }, function (err, result) {
			if (err) {
				res.json({ status: false, data: err });
				return;
			}
			res.json({ status: true, data: result });
		});
	});
}




exports.removePOI = function(req, res) {

	if (!req.body._id) {
		res.json({ status: false, msg: 'ข้อมูลที่ส่งมาไม่ถูกต้อง' });
		return;
	}

	PoirecommendedModel.remove({ _id: req.body._id }, function(err, poi) {
		if(err) {
			res.json({ status: false, data: err });
			return;
		}
		res.json({ status: true, data: poi });
	});
}




exports.exportPOI = function (req, res) {

	var json2csv = require('json2csv');

	PoirecommendedModel.find(
		{ "cgroup": req.user.local.cgroup },
		{ _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1, description: 1 },
		{ sort: { 'parkinglot': 1, 'parkingname': 1 } },
		function (err, result) {
			if (result == 0) {
				res.json({
					status: false,
					msg: 'No data'
				});
			} else {

				var fields = ['curloc', 'parkingname', 'parkinglot', 'parkingtype', 'description'];
				var csv = json2csv({ data: result, fields: fields });

				res.setHeader('Content-disposition', 'attachment; filename=poi_data.csv');
				res.set('Content-Type', 'text/csv');
				res.charset = 'utf-8';
				res.status(200).send(csv);
			}
		});
}


exports.updateCurrentPOI = function (req, res) {
	if ( !req.body.parkingname
		|| !req.body.lat
		|| !req.body.lng) {
		res.json({ status: false, msg: 'ข้อมูลที่ส่งมาไม่ถูกต้อง' });
		return;
	}


	PoirecommendedModel.findOne({ parkingname: req.body.parkingname }, function (err, poi) {
		
		if (err) {
			results.json({ status: false, data: err });
			return;
		}

		poi.curloc = [parseFloat(req.body.lng), parseFloat(req.body.lat)];
		poi.parkingname = req.body.parkingname;
		poi.description = req.body.description;
		poi.updated = Date.now();
		
		poi.save(function (err, result) {
			if (err) {
				res.json({ status: false, data: err });
				return;
			}
			return res.json({ status: true, data: result });
		});
	})
}