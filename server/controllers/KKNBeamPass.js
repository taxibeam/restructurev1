////////////////////////////////////
// TaxiBeam API for KKNBeamPass
// version : 1.0.0
// UpDate May 15, 2017 
// Created by Hanzen@BRET
////////////////////////////////////
var config = require('../../config/func').production;
var langdrvth = require('../../config/langth').langdrvth;
var crypto = require('crypto');
var compareVersions = require('compare-versions');
var lang = 'en';

// start nodemailer and define param
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var SMTPSentEmail = config.SMTPSentEmail;
var SMTPSentPass = config.SMTPSentPass;
var InfoEmail = config.InfoEmail;
var AlertEmail = config.AlertEmail;
// end define param for nodemailer

var Url = require('url');
var mongoose = require('mongoose');

var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var moment = require('moment');

var PassengerModel = mongoose.model('PassengerModel');
var DriversModel = mongoose.model('DriversModel');
var JoblistModel = mongoose.model('JoblistModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var MessageboardModel = mongoose.model('MessageboardModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var JoblisthotelModel = mongoose.model('JoblisthotelModel');
var Lk_garageModel = mongoose.model('Lk_garageModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');

// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var qs = require('querystring');
var ios = require('socket.io');

// var crypto;
// try {
//   crypto = require('crypto');  
// } catch (err) {
//   console.log('crypto support is disabled!');
// }



// NOTE *******************************  start service for Web App HERE 
exports.gettaxilist = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var favcartype = req.body.favcartype;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var radian = req.body.radian;
	var amount = req.body.amount;
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current Longitude is not defined" });
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current Latitude is not defined" });
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof radian == 'undefined' && radian == null) {
		radian = config.psgsearchpsgradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.psgsearchpsgamount;
	}

	// ไม่สามารถให้เช็ค device_id ได้ เพราะไม่อย่างนั้น ในกรณีของ webapp ที่เปิดมาครั้งแรกยังไม่มีการสร้าง device_id จะไม่สามารถเห็นรถได้    

	var dataDriver = DriversModel.find(
		{
			status: "ON",
			active: "Y",
			curlng: { $nin: [null,0] },
			curlat: { $nin: [null,0] },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: radian
				}
			},
			cartype: { $in: favcartype },
			_id_garage: { $exists: true }
		},
		{
			_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, phone: 1, carplate: 1, curlat: 1, curlng: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1, imgface: 1, status: 1, _id_garage: 1
		}
	);
	dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1, province: 1 });
	dataDriver.exec(function (err, result) {
		if (result == 0) {
			res.json({ status: true, data: result, msg: "No data" });
		} else {
			res.json({
				status: true,
				data: result
			});
		}
	});
};




exports.gettaxiinfo = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var taxi_duid = req.body.taxi_duid;
	// ไม่สามารถให้เช็ค device_id ได้ เพราะไม่อย่างนั้น ในกรณีของ webapp ที่เปิดมาครั้งแรกยังไม่มีการสร้าง device_id จะไม่สามารถเห็นรถได้    
	var dataDriver = DriversModel.findOne(
		{
			_id: taxi_duid,
			active: "Y",
			_id_garage: { $exists: true }
		},
		{
			_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, phone: 1, carplate: 1, curlat: 1, curlng: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1, imgface: 1, status: 1, _id_garage: 1
		}
	);
	dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1 });
	dataDriver.exec(function (err, result) {
		if (result == null) {
			res.json({ status: true, data: result, msg: "No data" });
		} else {
			res.json({
				status: true,
				data: result
			});
		}
	});
};




exports.getsspsgstatus = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.params.passenger_passid;
	var displayName = req.body.displayName;
	var phone = req.body.phone;
	PassengerModel.findOne(
		{ device_id: device_id },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, createdvia: 1, job_id: 1 },
		function (err, result) {
			if (result == null) {
				PassengerModel.create({
					device_id: device_id,
					displayName: displayName,
					phone: phone,
					occupied: false,
					updated: new Date(),
					createdvia: "PASS",
					appversion: "2",
					status: "OFF"
				},
					function (err, response) {
						if (err) {
							res.json({
								status: false,
								msg: "cannot create passenger",
								data: err
							});
						} else {
							res.json({
								status: true,
								data: {
									passenger_passid: response.device_id,
									passenger_jobid: response.job_id,
									passenger_status: response.status,
									taxi_duid: response.drv_id
								}
							});
						}
					});
			} else {
				res.json({
					status: true,
					data: {
						passenger_passid: result.device_id,
						passenger_jobid: result.job_id,
						passenger_status: result.status,
						taxi_duid: result.drv_id
					}
				});
			}
		}
	);
};




exports.passengercreatejob = function (req, res) {
	var device_id = req.params.passenger_passid;
	var job_id = req.params.passenger_jobid;
	var displayName = req.body.displayName;
	var phone = req.body.phone;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curaddr = req.body.curaddr;
	var deslng = req.body.deslng;
	var deslat = req.body.deslat;
	var destination = req.body.destination;
	var favcartype = req.body.favcartype;
	var radian = req.body.radian;
	var tips = req.body.tips;
	var detail = req.body.detail;
	var createdvia = "PASS";
	var jobtype = "PASS";
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "The current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "The current latitude is not valid" })
		return;
	}
	if (typeof deslng == 'undefined' && deslng == null) {
		res.json({ status: false, msg: "The destination longitude is not valid" })
		return;
	}
	if (typeof deslat == 'undefined' && deslat == null) {
		res.json({ status: false, msg: "The destination latitude is not valid" })
		return;
	}

	JoblistModel.findOne(
		{
			psg_device_id: device_id, job_id: job_id
		}, function (err, joblist) {
			if (joblist == null) {
				Lk_provincesModel.findOne(
					{
						polygons:
						{
							$geoIntersects:
							{
								$geometry:
								{
									"type": "Point",
									"coordinates": curloc
								}
							}
						}
					},
					function (err, response) {
						console.log(response)
						if (response == null) {
							searchpsgamount = config.psgsearchpsgamount;
							searchpsgradian = config.psgsearchpsgradian;
							findPsginZone(searchpsgradian, searchpsgamount);
						} else {
							searchpsgamount = response.searchpsgamount;
							searchpsgradian = response.searchpsgradian;
							findPsginZone(searchpsgradian, searchpsgamount);
						}
					}
				);

				function findPsginZone(searchpsgradian, searchpsgamount) {
					DriversModel.aggregate(
						[
							{
								$geoNear: {
									query: {
										status: "ON",
										active: "Y",
										favcartype: { $in: [[], req.body.cartype] }
									},
									near: { type: "Point", coordinates: curloc },
									distanceField: "dist",
									maxDistance: searchpsgradian,
									num: searchpsgamount,
									spherical: true
								}
							},
							{
								$project: {
									_id: 1,
									dist: 1,
									dist_km: {
										$divide: [
											{
												$subtract: [
													{ $multiply: ['$dist', 1] },
													{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
												]
											}, 1000]
									},
								},
							},
							{ $sort: { dist: 1 } }
						],
						function (err, drvlist) {
							PassengerModel.findOne(
								{ device_id: device_id },
								{ device_id: 1, job_id: 1, curaddr: 1, carlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, favcartype: 1, status: 1, updated: 1, createdjob: 1 },
								function (err, psg) {
									if (psg == null) {
										PassengerModel.create({
											device_id: device_id,
											displayName: displayName,
											phone: phone,
											curaddr: curaddr,
											curlng: curlng,
											curlat: curlat,
											curloc: curloc,
											destination: destination,
											deslng: deslng,
											deslat: deslat,
											favcartype: favcartype,
											tips: tips,
											detail: detail,
											job_id: job_id,
											drvarroundlist: drvlist,
											occupied: false,
											createdjob: new Date(),
											updated: new Date(),
											createdvia: "PASS",
											appversion: "2",
											status: "ON"
										},
											function (err, response) {
												if (err) {
													res.json({
														status: false,
														msg: "cannot create passenger",
														data: err
													});
												} else {
													_joblistcreate(job_id, jobtype, drvlist, req);
													res.json({
														status: true,
														msg: "create new a passenger",
														data: {
															passenger_passid: response.device_id,
															passenger_jobid: response.job_id,
															passenger_status: response.status,
															taxi_duid: response.drv_id,
															taxi_list: response.drvarroundlist
														}
													});
												}
											});
									} else {
										psg.phone = req.body.phone ? phone : psg.phone;
										psg.curaddr = req.body.curaddr ? curaddr : psg.curaddr;
										psg.curlng = req.body.curlng ? curlng : psg.curlng;
										psg.curlat = req.body.curlat ? curlat : psg.curlat;
										psg.curloc = curloc ? curloc : psg.curloc;
										if (typeof req.body.favcartype == 'undefined') {
											psg.favcartype = [];
										} else {
											psg.favcartype = req.body.favcartype ? favcartype : psg.favcartype;
										}
										psg.destination = req.body.destination ? destination : psg.destination;
										psg.deslng = req.body.deslng ? deslng : psg.deslng;
										psg.deslat = req.body.deslat ? deslat : psg.deslat;
										psg.tips = req.body.tips ? tips : 0;
										psg.detail = req.body.detail ? detail : psg.detail;
										psg.createdvia = createdvia;
										psg.job_id = job_id;
										psg.status = "ON";
										psg.occupied = false;
										psg.drvarroundlist = drvlist;
										psg.updated = new Date().getTime();
										psg.createdjob = new Date().getTime();
										psg.save(function (err, response) {
											if (err) {
												res.json({
													status: false,
													msg: "error",
													data: err
												});
											} else {
												_joblistcreate(job_id, jobtype, drvlist, req);
												res.json({
													status: true,
													data: {
														passenger_passid: response.device_id,
														passenger_jobid: response.job_id,
														passenger_status: response.status,
														taxi_duid: response.drv_id,
														taxi_list: response.drvarroundlist
													}
												});
											}
										});
									}
								}
							);
						}
					);
				}
			} else {
				res.json({
					status: false,
					msg: "passenger_jobid is already exists, please use the new one"
				});
			}
		})
};




exports.passengerfinishjob = function (req, res) {
	var device_id = req.params.passenger_passid;
	var job_id = req.params.passenger_jobid;
	PassengerModel.findOneAndUpdate({
		device_id: device_id, status: "THANKS"
	},
		{
			job_id: null,
			curaddr: null,
			destination: null,
			tips: 0,
			drv_id: null,
			updated: new Date(),
			status: "OFF",
			occupied: false
		},
		{ upsert: false, new: true }, function (err, response) {
			if (err) {
				res.json({
					status: false,
					msg: "invalid error 500"
				});
			} else {
				if (response == null) {
					res.json({
						status: false,
						msg: "passenger passid is invalid"
					});
				} else {
					_joblistupdate(job_id, req, 'datepsgthanks', '');
					res.json({
						status: true,
						data: {
							passenger_passid: response.device_id,
							passenger_jobid: response.job_id,
							passenger_status: response.status,
							taxi_duid: response.drv_id
						}
					});
				}
			}
		}
	);
};




exports.passengercanceljob = function (socket) {
	return function (req, res) {
		var device_id = req.params.passenger_passid;
		var job_id = req.params.passenger_jobid;
		PassengerModel.findOne({ device_id: device_id, job_id: job_id }, { device_id: 1, job_id: 1, status: 1, updated: 1, drv_id: 1 },
			function (err, psg) {
				if (psg == null) {
					res.json({
						status: false,
						msg: "passenger passid is invalid or passenger jobid is invalid"
					});
				} else {
					old_job_id = psg.job_id;
					if (psg.drv_id == '' || psg.drv_id == null) {
						psg.job_id = null;
						psg.drv_id = null;
						psg.updated = new Date();
						psg.deniedTaxiIds = [];
						psg.status = "OFF";
						psg.occupied = false;
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error01" });
							} else {
								_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel before drv');
								res.json({
									status: true,
									msg: "yes 1",
									data: {
										passenger_passid: response.device_id,
										passenger_jobid: response.job_id,
										passenger_status: response.status,
										taxi_duid: response.drv_id
									}
								});
							}
						});
					} else {
						saveHistory(psg.drv_id, psg._id, psg.job_id, false);
						// for detected cancelled driver 
						canceldrvid = psg.drv_id;
						//
						psg.job_id = null;
						psg.drv_id = null;
						psg.updated = new Date();
						psg.deniedTaxiIds = [];
						psg.status = "OFF";
						psg.occupied = false;
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "cancelcall error02" });
							} else {
								_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel after drv');
								DriversModel.findOne({ _id: canceldrvid }, { _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, status: 1 },
									function (err, drv) {
										if (drv == null) {
											res.json({ status: false, msg: "cancelcall error03" });
										} else {
											drv.psg_id = null;
											drv.status = "ON";
											drv.save(function (err, result) {
												if (err) {
													res.json({ status: false, msg: "cancelcall error04" });
												} else {
													socket.emit("DriverSocketOn", result);
													res.json({
														status: true,
														msg: "cancelled",
														result: result,
														data: {
															passenger_passid: response.device_id,
															passenger_jobid: response.job_id,
															passenger_status: response.status,
															taxi_duid: response.drv_id
														}
													});
												}
											});
										}
									}
								)
							}
						});
					}
				}
			}
		);
	};
};




exports.getsercharge = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		{ polygons: 0, Lat: 0, Long: 0, _id_garage: 0, searchpsgamount: 0, searchpsgradian: 0 },
		function (err, response) {
			if (response == null) {
				res.json({
					status: false,
					data: {}
				});
			} else {
				res.json({
					status: true,
					data: {
						psgfare: response.psgfare,
						ProvinceID: response.ProvinceID,
						Name: response._doc["Name" + (req.body.lang ? req.body.lang : "en")] || response._doc["Nameen"]
					}
				});
			}
		}
	);
}




// NOTE *******************************  start service for KKNBeamPass Passenger App HERE!!!
exports.psgloadconfig = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var appversion = req.body.version;
	var platform = req.body.platform;	// [ ANDROID, iOS ]
	PassengerModel.findOneAndUpdate(
		{ device_id: device_id },
		{
			appversion: appversion,
			platform: platform
		},
		{ upsert: false, new: true },
		function (err, response) {
			if (err) return handleError(err);
			if (response == null) {
				res.json({
					status: true,
					data: {
						available_country: config.available_country,
						passengerappversion: config.passengerappversion,
						distoShowPhone: config.distoShowPhone,
						psgSearchDrvInterval: config.psgSearchDrvInterval,
						psgGetstatusInterval: config.psgGetstatusInterval
					}
				});
			} else {
				res.json({
					status: true,
					data: {
						passengerappversion: config.passengerappversion,
						distoShowPhone: config.distoShowPhone,
						psgSearchDrvInterval: config.psgSearchDrvInterval,
						psgGetstatusInterval: config.psgGetstatusInterval
					}
				});
			}
		}
	);
}




exports.psggetsercharge = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	if (typeof curlng == 'undefined' || curlng == null || curlng == "") {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' || curlat == null || curlat == "") {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		{ polygons: 0, Lat: 0, Long: 0, _id_garage: 0, searchpsgamount: 0, searchpsgradian: 0 },
		function (err, response) {
			if (response == null) {
				res.json({
					status: false,
					data: {}
				});
			} else {
				res.json({
					status: true,
					data: {
						sercharge: response.psgfare,
						ProvinceID: response.ProvinceID,
						Name: response._doc["Name" + (req.body.lang ? req.body.lang : "en")] || response._doc["Nameen"]
					}
				});
			}
		}
	);
}




exports.psgregister = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var displayName = req.body.displayName;
	var email = req.body.email;
	var phoneid = req.body.phoneid;
	var gensms = ranSMS();

	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof displayName == 'undefined' || displayName == null || displayName == "") {
		res.json({ status: false, msg: "Please put your name" })
		return;
	}
	if (typeof phoneid == 'undefined' || phoneid == null || phoneid == "") {
		res.json({ status: false, msg: "Please put your phoneid" })
		return;
	}	

	PassengerModel.findOne(
		{ phoneid: phoneid },
		function (err, psg) {
			if (psg == null) {
				PassengerModel.create({
					device_id: device_id,
					displayName: displayName,
					email: email,
					phoneid: phoneid,
					phone: phoneid,
					status: "OFF",
					smsconfirm: gensms,
					active: "N",
					createdvia: "BeamPass",
					created: new Date(),
					updated: new Date()
				},
					function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							// res.json({
							// 	status: true,
							// 	data: {
							// 		_id: response._id,
							// 		msg: "register success"
							// 	}
							// });
							////////////////////////////////////////////////////////							
							var https = require('https');
							var postData = JSON.stringify({
								"sender": config.smssender,
								"to": phoneid,
								"msg": " Taxi-Beam: Your OTP is " + gensms
							});
							var options = {
								hostname: config.smshostname,
								port: config.smshostport,
								path: config.smshostpath,
								method: config.smshostmethod,
								headers: {
									'Content-Type': 'application/json; charset=utf-8',
									'Content-Length': Buffer.byteLength(postData)
								}
							};
							var request = https.request(options, function (response) {
								var body = '';
								response.on('data', function (d) {
									body += d;
								});
								response.on('end', function () {
									if (body) {
										var parsed = JSON.parse(body);
										if (parsed.SendMessageResult.ErrorCode == 1) {
											res.json({
												status: true,
												msg: "SMS sent Successful"
											});
										} else {
											var transporter = nodemailer.createTransport(smtpTransport({
												service: 'gmail',
												auth: {
													user: SMTPSentEmail, // my mail
													pass: SMTPSentPass
												}
											}));
											// setup e-mail data with unicode symbols
											var mailOptions = {
												from: '"Taxi-Beam auto sent mail" <' + AlertEmail + '>', // sender address
												to: AlertEmail, // list of receivers
												subject: 'There is some errors for SMS system, please check.', // Subject line
												text: 'plaintext body', // plaintext body
												html: 'There is some errors for SMS system, please check.<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
											};
											// send mail with defined transport object
											transporter.sendMail(mailOptions, function (error, info) {
												if (error) {
													return console.log(error);
												}
											});
											//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
											_getErrDetail(lang, 'err015', function (errorDetail) {
												res.json({
													status: false,
													msg: errorDetail
												});
											});
										}
									} else {
										//msg: "ระบบไม่สามารถให้บริการได้ กรุณาติดต่อ Taxi-Beam"
										_getErrDetail(lang, 'err015', function (errorDetail) {
											res.json({
												status: false,
												msg: errorDetail
											});
										});
									}
								});
							});
							request.on('error', function (err) {
								res.json({ status: false, msg: 'ขออภัยค่ะ ไม่สามารถทำงานต่อได้\nเรากำลังดำเนินการแก้ไขให้เร็วที่สุด' });
							});
							request.write(postData);
							request.end();
							/*
							1=Successful
							21=SMS Sending Failed
							22=Invalid username/password combination
							23=Credit exhausted
							24=Gateway unavailable
							25=Invalid schedule date format
							26=Unable to schedule
							27=Username is empty
							28=Password is empty
							29=Recipient is empty
							30=Message is empty
							31=Sender is empty
							32=One or more required fields are empty
							*/
							////////////////////////////////////////////////////////							
						}
					}
				);
			} else {
				_getErrDetail(lang, 'err016', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			}
		}
	);
};




exports.psglogin = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var phoneid = req.body.phoneid;
	var gensms = ranSMS();
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}	
	if (typeof phoneid == 'undefined' || phoneid == null || phoneid == "") {
		res.json({ status: false, msg: "Please put your phoneid" })
		return;
	}	
	PassengerModel.findOneAndUpdate(
		{ phoneid: phoneid },
		{ smsconfirm: gensms },
		{ upsert: false, new: true },
		function (err, result) {
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else if (result == null) {
				// เบอร์นี้ยังไม่ลงทะเบียนฯ 
				_getErrDetail(lang, 'err017', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				////////////////////////////////////////////////////////
				var https = require('https');
				var postData = JSON.stringify({
					"sender": config.smssender,
					"to": phoneid,
					"msg": " Taxi-Beam: Your OTP is " + gensms
				});
				var options = {
					hostname: config.smshostname,
					port: config.smshostport,
					path: config.smshostpath,
					method: config.smshostmethod,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Content-Length': Buffer.byteLength(postData)
					}
				};
				var request = https.request(options, function (response) {
					var body = '';
					response.on('data', function (d) {
						body += d;
					});
					response.on('end', function () {
						if (body) {
							var parsed = JSON.parse(body);
							if (parsed.SendMessageResult.ErrorCode == 1) {
								res.json({
									status: true,
									msg: "SMS sent Successful"
								});
							} else {
								var transporter = nodemailer.createTransport(smtpTransport({
									service: 'gmail',
									auth: {
										user: SMTPSentEmail, // my mail
										pass: SMTPSentPass
									}
								}));
								// setup e-mail data with unicode symbols
								var mailOptions = {
									from: '"Taxi-Beam auto sent mail" <' + AlertEmail + '>', // sender address
									to: AlertEmail, // list of receivers
									subject: 'There is some errors for SMS system, please check.', // Subject line
									text: 'plaintext body', // plaintext body
									html: 'There is some errors for SMS system, please check.<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
								};
								// send mail with defined transport object
								transporter.sendMail(mailOptions, function (error, info) {
									if (error) {
										return console.log(error);
									}
								});
								//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
								_getErrDetail(lang, 'err015', function (errorDetail) {
									res.json({
										status: false,
										msg: errorDetail
									});
								});
							}
						} else {
							//msg: "ระบบไม่สามารถให้บริการได้ กรุณาติดต่อ Taxi-Beam"
							_getErrDetail(lang, 'err015', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail
								});
							});
						}
					});
				});
				request.on('error', function (err) {
					res.json({ status: false, msg: 'ขออภัยค่ะ ไม่สามารถทำงานต่อได้\nเรากำลังดำเนินการแก้ไขให้เร็วที่สุด' });
				});
				request.write(postData);
				request.end();
				/*
				1=Successful
				21=SMS Sending Failed
				22=Invalid username/password combination
				23=Credit exhausted
				24=Gateway unavailable
				25=Invalid schedule date format
				26=Unable to schedule
				27=Username is empty
				28=Password is empty
				29=Recipient is empty
				30=Message is empty
				31=Sender is empty
				32=One or more required fields are empty
				*/
				////////////////////////////////////////////////////////
			}
		}
	);
};




exports.psgotprequest = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var phoneid = req.body.phoneid;
	var gensms = ranSMS();
	if (typeof phoneid == 'undefined' || phoneid == null || phoneid == "") {
		res.json({ status: false, msg: "Please put your phoneid" })
		return;
	}		
	PassengerModel.findOneAndUpdate(
		{ phoneid: phoneid },
		{ smsconfirm: gensms },
		{ upsert: false, new: true },
		function (err, result) {
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else if (result == null) {
				//"ท่านใส่เบอร์โทรไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง",	  
				_getErrDetail(lang, 'err001', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				////////////////////////////////////////////////////////
				var https = require('https');
				var postData = JSON.stringify({
					"sender": config.smssender,
					"to": phoneid,
					"msg": " Taxi-Beam: Your OTP is " + gensms
				});
				var options = {
					hostname: config.smshostname,
					port: config.smshostport,
					path: config.smshostpath,
					method: config.smshostmethod,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Content-Length': Buffer.byteLength(postData)
					}
				};
				var request = https.request(options, function (response) {
					var body = '';
					response.on('data', function (d) {
						body += d;
					});
					response.on('end', function () {
						if (body) {
							var parsed = JSON.parse(body);
							if (parsed.SendMessageResult.ErrorCode == 1) {
								res.json({
									status: true,
									msg: "SMS sent Successful"
								});
							} else {
								var transporter = nodemailer.createTransport(smtpTransport({
									service: 'gmail',
									auth: {
										user: SMTPSentEmail, // my mail
										pass: SMTPSentPass
									}
								}));
								// setup e-mail data with unicode symbols
								var mailOptions = {
									from: '"Taxi-Beam auto sent mail" <' + AlertEmail + '>', // sender address
									to: AlertEmail, // list of receivers
									subject: 'There is some errors for SMS system, please check.', // Subject line
									text: 'plaintext body', // plaintext body
									html: 'There is some errors for SMS system, please check.<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
								};
								// send mail with defined transport object
								transporter.sendMail(mailOptions, function (error, info) {
									if (error) {
										return console.log(error);
									}
								});
								//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
								_getErrDetail(lang, 'err015', function (errorDetail) {
									res.json({
										status: false,
										msg: errorDetail
									});
								});
							}
						} else {
							//msg: "ระบบไม่สามารถให้บริการได้ กรุณาติดต่อ Taxi-Beam"
							_getErrDetail(lang, 'err015', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail
								});
							});
						}
					});
				});
				request.on('error', function (err) {
					res.json({ status: false, msg: 'ขออภัยค่ะ ไม่สามารถทำงานต่อได้\nเรากำลังดำเนินการแก้ไขให้เร็วที่สุด' });
				});
				request.write(postData);
				request.end();
				/*
				1=Successful
				21=SMS Sending Failed
				22=Invalid username/password combination
				23=Credit exhausted
				24=Gateway unavailable
				25=Invalid schedule date format
				26=Unable to schedule
				27=Username is empty
				28=Password is empty
				29=Recipient is empty
				30=Message is empty
				31=Sender is empty
				32=One or more required fields are empty
				*/
				////////////////////////////////////////////////////////
			}
		}
	);
};




exports.psgsmsconfirm = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var phoneid = req.body.phoneid;
	var device_id = req.body.device_id;
	var smsconfirm = req.body.smsconfirm;
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof phoneid == 'undefined' || phoneid == null || phoneid == "") {
		res.json({ status: false, msg: "Please put your phoneid" })
		return;
	}	
	if (typeof smsconfirm == 'undefined' || smsconfirm == null || smsconfirm == "") {
		res.json({ status: false, msg: "Please put your OTP" })
		return;
	}			
	PassengerModel.findOneAndUpdate(
		{ phoneid: phoneid, smsconfirm: smsconfirm },
		{ device_id: device_id, active: 'Y', updated: new Date() },
		{ upsert: false, new: true },
		function (err, psg) {
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else if (psg == null) {
				//"msg": "ขออภัยค่ะ ท่านใส่รหัสยืนยันการเข้าใช้บริการไม่ถูกต้อง กรุณาใส่รหัส
				_getErrDetail(lang, 'err002', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "login successfully",
					data: {
						displayName: psg.displayName,
						phoneid: psg.phoneid,						
						phone: psg.phone,
						email: psg.email
					}
				});
			}
		}
	);
};




exports.psgupdateprofile = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";	
	var device_id = req.body.device_id;
	var displayName = req.body.displayName;
	var phoneid = req.body.phoneid;
	var email = req.body.email;
	var gensms = ranSMS();
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof phoneid == 'undefined' || phoneid == null || phoneid == "") {
		res.json({ status: false, msg: "Please put your phoneid" })
		return;
	}	
	PassengerModel.findOne(
		{ device_id: device_id },
		{ phoneid: 1, phone: 1, device_id: 1, status: 1 },
		function (err, psg) {
			if (psg == null) {
				//"msg": "ขออภัยค่ะไม่พบผู้ใช้งานในระบบ
				_getErrDetail(lang, 'err014', function (errorDetail) {
					res.json({
						status: false,
						err: "err001",
						msg: errorDetail
					});
				});
			} else {
				if ( psg.phoneid != phoneid ) {
					PassengerModel.findOne(
						{ phoneid: phoneid, device_id: { $ne: device_id } },
						function (err, response) {
							if (response == null) {
								////////////////////////////////////////////////////////
								var https = require('https');
								var postData = JSON.stringify({
									"sender": config.smssender,
									"to": phoneid,
									"msg": " Taxi-Beam: Your OTP is " + gensms
								});
								var options = {
									hostname: config.smshostname,
									port: config.smshostport,
									path: config.smshostpath,
									method: config.smshostmethod,
									headers: {
										'Content-Type': 'application/json; charset=utf-8',
										'Content-Length': Buffer.byteLength(postData)
									}
								};
								var request = https.request(options, function (response) {
									var body = '';
									response.on('data', function (d) {
										body += d;
									});
									response.on('end', function () {
										console.log(body)
										if (body) {
											var parsed = JSON.parse(body);
											if (parsed.SendMessageResult.ErrorCode == 1) {
												// res.json({
												// 	status: true,
												// 	msg: "SMS sent Successful"
												// });
												if (req.body.email) { psg.email = req.body.email ? req.body.email : psg.email; }
												if (req.body.displayName) { psg.displayName = req.body.displayName ? req.body.displayName : psg.displayName; }
												psg.smsconfirm = gensms;
												psg.updated = new Date();
												psg.save(function (err, result) {
													if (err) {
														res.json({ status: false, msg: "error" });
													} else {
														res.json({
															//msg: "success, passenger profile updated",
															status: true,
															data: {
																displayName: result.displayName,
																phoneid: result.phoneid,						
																phone: result.phone,
																email: result.email
															}											
														});	
													}
												});																									
											} else {
												var transporter = nodemailer.createTransport(smtpTransport({
													service: 'gmail',
													auth: {
														user: SMTPSentEmail, // my mail
														pass: SMTPSentPass
													}
												}));
												// setup e-mail data with unicode symbols
												var mailOptions = {
													from: '"Taxi-Beam auto sent mail" <' + AlertEmail + '>', // sender address
													to: AlertEmail, // list of receivers
													subject: 'There is some errors for SMS system, please check.', // Subject line
													text: 'plaintext body', // plaintext body
													html: 'There is some errors for SMS system, please check.<br><br>http://admin.taxi-beam.com<br><br><hr>' // html body
												};
												// send mail with defined transport object
												transporter.sendMail(mailOptions, function (error, info) {
													if (error) {
														return console.log(error);
													}
												});
												//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
												_getErrDetail(lang, 'err006', function (errorDetail) {
													res.json({
														status: false,
														err: "err002",
														msg: errorDetail
													});
												});
											}
										} else {
											//msg: "ระบบไม่สามารถให้บริการได้ กรุณาติดต่อ Taxi-Beam"
											_getErrDetail(lang, 'err015', function (errorDetail) {
												res.json({
													status: false,
													err: "err003",
													msg: errorDetail
												});
											});
										}
									});
								});
								request.on('error', function (err) {
									res.json({ status: false, msg: 'ขออภัยค่ะ ไม่สามารถทำงานต่อได้\nเรากำลังดำเนินการแก้ไขให้เร็วที่สุด' });
								});
								request.write(postData);
								request.end();
								/*
								1=Successful
								21=SMS Sending Failed
								22=Invalid username/password combination
								23=Credit exhausted
								24=Gateway unavailable
								25=Invalid schedule date format
								26=Unable to schedule
								27=Username is empty
								28=Password is empty
								29=Recipient is empty
								30=Message is empty
								31=Sender is empty
								32=One or more required fields are empty
								*/
								////////////////////////////////////////////////////////
							} else {
								console.log(response)
								//"msg": "ขออภัยค่ะ เบอร์โทรนี้มีผู้ใช้งานแล้ว
								_getErrDetail(lang, 'err016', function (errorDetail) {
									res.json({
										status: false,
										err: "err004",
										msg: errorDetail
									});
								});
							}
						}
					)
				} else {
					if (req.body.email) { psg.email = req.body.email ? req.body.email : psg.email; }
					if (req.body.displayName) { psg.displayName = req.body.displayName ? req.body.displayName : psg.displayName; }
					psg.updated = new Date();
					psg.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							res.json({
								status: true,
								data: {
									displayName: result.displayName,
									phoneid: result.phoneid,						
									phone: result.phone,
									email: result.email
								}								
							});
						}
					});
				}
			}
		}
	);
};




exports.psgsmsprofileconfirm = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var phoneid_old = req.body.phoneid_old;
	var phoneid = req.body.phoneid;
	var smsconfirm = req.body.smsconfirm;
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof phoneid_old == 'undefined' || phoneid_old == null || phoneid_old == "") {
		res.json({ status: false, msg: "Please put your previous phone" })
		return;
	}
	if (typeof phoneid == 'undefined' || phoneid == null || phoneid == "") {
		res.json({ status: false, msg: "Please put your new phone" })
		return;
	}
	if (typeof smsconfirm == 'undefined' || smsconfirm == null || smsconfirm == "") {
		res.json({ status: false, msg: "Please put your OTP" })
		return;
	}
	PassengerModel.findOne(
		{ device_id: device_id, phoneid: phoneid_old },
		{ phoneid: 1, phone: 1, device_id: 1, status: 1 },
		function (err, psg) {
			if (err) {
				res.json({ status: false, msg: "error", data: err });
			} else if (psg == null) {
				//"msg": "ใส่เบอร์โทรไม่ถูกต้อง
				_getErrDetail(lang, 'err001', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				PassengerModel.findOneAndUpdate(
					{ device_id: device_id, phoneid: phoneid_old, smsconfirm: smsconfirm },
					{ phoneid: phoneid, phone: phoneid, updated: new Date() },
					{ upsert: false, new: true },
					function (err, psg) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else if (psg == null) {
							//"msg": "ขออภัยค่ะ ท่านใส่รหัสยืนยันการเข้าใช้บริการไม่ถูกต้อง กรุณาใส่รหัส
							_getErrDetail(lang, 'err002', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail
								});
							});
						} else {
							res.json({
								status: true,
								msg: "update psg phone successfully",
								data: {
									displayName: psg.displayName,
									phoneid: psg.phoneid,						
									phone: psg.phone,
									email: psg.email
								}					
							});
						}
					}
				);
			}
		}
	)
};




exports.psguploadface = function (req, res) {
	res.connection.setTimeout(0);
	var lang = req.body.lang ? req.body.lang : "en";
	var form = new formidable.IncomingForm();
	var imgtype = "imgface";
	form.parse(req, function (err, fields, files) {
		if (typeof files.upload == 'undefined' && files.upload == null) {
			console.log("fail upload ! ; no image input for a face")
			if (err) {
				res.json({ status: false, msg: err });
			} else {
				_getErrDetail(lang, 'err004', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			}
		} else {
			// check image type			
			if (files.upload.type == 'image/jpeg' || files.upload.type == 'image/jpg' || files.upload.type == 'image/png' || files.upload.type == 'application/octet-stream') {
				device_id = fields.device_id;
				console.log('device_id == ' + device_id)
				findandUpload(device_id);
			} else {
				// kick out
				//res.json({ status: false , msg: "Invalid image type, Please input type JPEG or PNG" });
				_getErrDetail(lang, 'err005', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			}
		}
	});
	function findandUpload(deviceId) {
		PassengerModel.findOne(
			{ device_id: deviceId },
			function (err, response) {
				if (response == null) {
					res.json({
						status: false,
						msg: "no passenger found"
					});
				} else {
					if (form) {
						var files = form.openedFiles;
						if (files) {
							var file = files[0];
							if (file) {
								var temp_path = file.path;
								var extension = path.extname(file.name);
								var new_location = '../upload_passengers/';
								var imgname = new Date().getTime();
								newimgupload = imgname + '_' + imgtype + extension;
								fs.copy(temp_path, new_location + newimgupload, function (err) {
									if (err) {
										res.json({
											status: false,
											msg: err
										});
									} else {
										console.log("success upload !")
										PassengerModel.findOneAndUpdate({
											device_id: deviceId
										},
											{
												imgface: newimgupload,
												updated: new Date()
											},
											{ upsert: false, new: true }, function (err, upfile) {
												if (err) {
													res.json({
														status: false,
														msg: err
													});
												} else {
													if (upfile == null) {
														res.json({
															status: false,
															msg: "no passenger found"
														});
													} else {
														res.json({
															status: true,
															data: { imgname: newimgupload }
														});
													}
												}
											}
										);
									}
								});
							}
						}
					} else {
						console.log("do not have form")
						res.json({ status: false, msg: "no form" });
					}
				}
			}
		);
	}
}




exports.psggettaxilist = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var favcartype = req.body.favcartype;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var radian = req.body.radian;
	var amount = req.body.amount;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current Longitude is not defined" });
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current Latitude is not defined" });
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof radian == 'undefined' && radian == null) {
		radian = config.psgsearchpsgradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.psgsearchpsgamount;
	}
	PassengerModel.findOne(
		{ device_id: device_id },
		function (err, response) {
			if (response == null) {
				res.json({ status: false, msg: "no passenger found" });
			} else {
				var dataDriver = DriversModel.find(
					{
						status: "ON",
						active: "Y",
						curlng: { $ne: null },
						curlat: { $ne: null },
						curloc: {
							$near: {
								$geometry: {
									type: "Point",
									coordinates: curloc
								},
								$maxDistance: radian
							}
						},
						cartype: { $in: favcartype },
						_id_garage: { $exists: true }
					},
					{
						_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, phone: 1, carplate: 1, curlat: 1, curlng: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1, imgface: 1, status: 1, _id_garage: 1
					}
				);
				dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1, province: 1 });
				dataDriver.exec(function (err, result) {
					if (result == 0) {
						res.json({ status: true, data: result, msg: "No data" });
					} else {
						res.json({
							status: true,
							data: result
						});
					}
				});
			}
		}
	);
	// console.log('yyy')
	// res.json({ status: true, msg: "no passenger found" });
	// return;
};




exports.psggettaxiinfo = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var drv_id = req.body.drv_id;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	PassengerModel.findOne(
		{ device_id: device_id },
		function (err, response) {
			if (response == null) {
				res.json({ status: false, msg: "no passenger found" });
			} else {
				var dataDriver = DriversModel.findOne(
					{
						_id: drv_id,
						active: "Y",
						_id_garage: { $exists: true }
					},
					{
						_id: 1, fname: 1, lname: 1, allowpsgcontact: 1, phone: 1, carplate: 1, curlat: 1, curlng: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, english: 1, imgface: 1, status: 1, _id_garage: 1
					}
				);
				dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1 });
				dataDriver.exec(function (err, result) {
					if (result == null) {
						res.json({ status: true, data: result, msg: "No data" });
					} else {
						res.json({
							status: true,
							data: result
						});
					}
				});
			}
		}
	);
};




exports.psggetstatus = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	PassengerModel.findOne(
		{ device_id: device_id },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, createdvia: 1, job_id: 1 },
		function (err, result) {
			if (result == null) {
				res.json({ status: false, msg: "no passenger found" });
			} else {
				res.json({
					status: true,
					data: {
						status: result.status,
						drv_id: result.drv_id,
						job_id: result.job_id
					}
				});
			}
		}
	);
};




exports.psgcalldrv = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var phone = req.body.phone;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curaddr = req.body.curaddr;
	var deslng = req.body.deslng;
	var deslat = req.body.deslat;
	var destination = req.body.destination;
	var favcartype = req.body.favcartype;
	var tips = req.body.tips;
	var detail = req.body.detail;
	var createdvia = "BEAMPASS";
	var jobtype = "BEAMPASS";
	var job_id = new Date().getTime() + '-' + ranSMS();
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof deslng == 'undefined' && deslng == null) {
		res.json({ status: false, msg: "The destination longitude is not valid" })
		return;
	}
	if (typeof deslat == 'undefined' && deslat == null) {
		res.json({ status: false, msg: "The destination latitude is not valid" })
		return;
	}

	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				searchpsgamount = config.psgsearchpsgamount;
				searchpsgradian = config.psgsearchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			} else {
				searchpsgamount = response.searchpsgamount;
				searchpsgradian = response.searchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			}
		}
	);

	function findPsginZone(searchpsgradian, searchpsgamount) {
		DriversModel.aggregate(
			[
				{
					$geoNear: {
						query: {
							status: "ON",
							active: "Y",
							favcartype: { $in: [[], req.body.cartype] }
						},
						near: { type: "Point", coordinates: curloc },
						distanceField: "dist",
						maxDistance: searchpsgradian,
						num: searchpsgamount,
						spherical: true
					}
				},
				{
					$project: {
						_id: 1,
						dist: 1,
						dist_km: {
							$divide: [
								{
									$subtract: [
										{ $multiply: ['$dist', 1] },
										{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
									]
								}, 1000]
						},
					},
				},
				{ $sort: { dist: 1 } }
			],
			function (err, drvlist) {
				PassengerModel.findOne(
					{ device_id: device_id },
					{ device_id: 1, job_id: 1, curaddr: 1, curloc: 1, destination: 1, desloc: 1, des_dist: 1, tips: 1, detail: 1, favcartype: 1, status: 1, updated: 1, createdjob: 1 },
					function (err, psg) {
						if (psg == null) {
							res.json({
								status: false,
								msg: "error",
								data: err
							});
						} else {
							psg.phone = req.body.phone ? req.body.phone : psg.phone;
							psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr;
							psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng;
							psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat;
							psg.curloc = curloc ? curloc : psg.curloc;
							if (typeof req.body.favcartype == 'undefined') {
								psg.favcartype = [];
							} else {
								psg.favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype;
							}
							psg.destination = req.body.destination ? req.body.destination : psg.destination;
							psg.deslng = req.body.deslng ? deslng : psg.deslng;
							psg.deslat = req.body.deslat ? deslat : psg.deslat;
							psg.tips = req.body.tips ? req.body.tips : 0;
							psg.detail = req.body.detail ? req.body.detail : psg.detail;
							psg.createdvia = createdvia;
							psg.job_id = job_id;
							psg.status = "ON";
							psg.occupied = false;
							psg.drvarroundlist = drvlist;
							psg.updated = new Date();
							psg.createdjob = new Date();
							psg.save(function (err, response) {
								if (err) {
									res.json({
										status: false,
										msg: "error",
										data: err
									});
								} else {
									_joblistcreate(job_id, jobtype, drvlist, req);
									res.json({
										status: true,
										data: {
											job_id: response.job_id,
											status: response.status,
											drv_id: response.drv_id,
											drvarroundlist: response.drvarroundlist
										}
									});
								}
							});
						}
					}
				);
			}
		);
	}
};




exports.psgrecall = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var phone = req.body.phone;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curaddr = req.body.curaddr;
	var deslng = req.body.deslng;
	var deslat = req.body.deslat;
	var destination = req.body.destination;
	var favcartype = req.body.favcartype;
	var tips = req.body.tips;
	var detail = req.body.detail;
	var createdvia = "BEAMPASS";
	var jobtype = "BEAMPASS";
	var job_id = new Date().getTime() + '-' + ranSMS();
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	if (curlng == 0) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (curlat == 0) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof deslng == 'undefined' && deslng == null) {
		res.json({ status: false, msg: "The destination longitude is not valid" })
		return;
	}
	if (typeof deslat == 'undefined' && deslat == null) {
		res.json({ status: false, msg: "The destination latitude is not valid" })
		return;
	}

	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				searchpsgamount = config.psgsearchpsgamount;
				searchpsgradian = config.psgsearchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			} else {
				searchpsgamount = response.searchpsgamount;
				searchpsgradian = response.searchpsgradian;
				findPsginZone(searchpsgradian, searchpsgamount);
			}
		}
	);

	function findPsginZone(searchpsgradian, searchpsgamount) {
		DriversModel.aggregate(
			[
				{
					$geoNear: {
						query: {
							status: "ON",
							active: "Y",
							favcartype: { $in: [[], req.body.cartype] }
						},
						near: { type: "Point", coordinates: curloc },
						distanceField: "dist",
						maxDistance: searchpsgradian,
						num: searchpsgamount,
						spherical: true
					}
				},
				{
					$project: {
						_id: 1,
						dist: 1,
						dist_km: {
							$divide: [
								{
									$subtract: [
										{ $multiply: ['$dist', 1] },
										{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
									]
								}, 1000]
						},
					},
				},
				{ $sort: { dist: 1 } }
			],
			function (err, drvlist) {
				PassengerModel.findOne(
					{ device_id: device_id },
					{ device_id: 1, job_id: 1, curaddr: 1, curloc: 1, destination: 1, desloc: 1, des_dist: 1, tips: 1, detail: 1, favcartype: 1, status: 1, updated: 1, createdjob: 1 },
					function (err, psg) {
						if (psg == null) {
							res.json({
								status: false,
								msg: "error",
								data: err
							});
						} else {
							psg.phone = req.body.phone ? req.body.phone : psg.phone;
							psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr;
							psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng;
							psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat;
							psg.curloc = curloc ? curloc : psg.curloc;
							if (typeof req.body.favcartype == 'undefined') {
								psg.favcartype = [];
							} else {
								psg.favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype;
							}
							psg.destination = req.body.destination ? req.body.destination : psg.destination;
							psg.deslng = req.body.deslng ? deslng : psg.deslng;
							psg.deslat = req.body.deslat ? deslat : psg.deslat;
							psg.tips = req.body.tips ? req.body.tips : 0;
							psg.detail = req.body.detail ? req.body.detail : psg.detail;
							psg.createdvia = createdvia;
							psg.job_id = job_id;
							psg.status = "ON";
							psg.occupied = false;
							psg.drvarroundlist = drvlist;
							psg.updated = new Date();
							psg.createdjob = new Date();
							psg.save(function (err, response) {
								if (err) {
									res.json({
										status: false,
										msg: "error",
										data: err
									});
								} else {
									_joblistcreate(job_id, jobtype, drvlist, req);
									res.json({
										status: true,
										data: {
											job_id: response.job_id,
											status: response.status,
											drv_id: response.drv_id,
											drvarroundlist: response.drvarroundlist
										}
									});
								}
							});
						}
					}
				);
			}
		);
	}
};




exports.psgendtrip = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	PassengerModel.findOneAndUpdate({
		device_id: device_id, status: "THANKS"
	},
		{
			job_id: null,
			curaddr: null,
			destination: null,
			tips: 0,
			drv_id: null,
			updated: new Date(),
			status: "OFF",
			occupied: false
		},
		{ upsert: false, new: true }, function (err, response) {
			if (err) {
				res.json({
					status: false,
					msg: "invalid error 500"
				});
			} else {
				if (response == null) {
					res.json({
						status: false,
						msg: "passenger not found"
					});
				} else {
					_joblistupdate(job_id, req, 'datepsgthanks', '');
					res.json({
						status: true,
						data: {
							status: response.status
						}
					});
				}
			}
		}
	);
};




exports.psgcancelcall = function (socket) {
	return function (req, res) {
		var lang = req.body.lang ? req.body.lang : "en";
		var device_id = req.body.device_id;
		var cancelreason = req.body.cancelreason;
		if (typeof device_id == 'undefined' && device_id == null) {
			res.json({ status: false, msg: "Please put your device" })
			return;
		}
		PassengerModel.findOne(
			{ device_id: device_id },
			{ device_id: 1, job_id: 1, status: 1, updated: 1, drv_id: 1 },
			function (err, psg) {
				if (psg == null) {
					res.json({
						status: false,
						msg: "passenger ssid is invalid or passenger jobid is invalid"
					});
				} else {
					old_job_id = psg.job_id;
					if (psg.drv_id == '' || psg.drv_id == null) {
						psg.job_id = null;
						psg.drv_id = null;
						psg.updated = new Date();
						psg.status = "OFF";
						psg.occupied = false;
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error01" });
							} else {
								_joblistupdate(old_job_id, req, 'datepsgcancel', cancelreason);
								res.json({
									status: true,
									msg: "cancel complted before got a driver",
									data: {
										status: response.status,
									}
								});
							}
						});
					} else {
						saveHistory(psg.drv_id, psg._id, psg.job_id, false);
						// for detected cancelled driver 
						canceldrvid = psg.drv_id;
						//
						psg.job_id = null;
						psg.drv_id = null;
						psg.updated = new Date();
						psg.status = "OFF";
						psg.occupied = false;
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "cancelcall error02" });
							} else {
								_joblistupdate(old_job_id, req, 'datepsgcancel', cancelreason);
								DriversModel.findOne(
									{ _id: canceldrvid },
									{ _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, status: 1, psg_id: 1 },
									function (err, drv) {
										if (drv == null) {
											res.json({ status: false, msg: "cancelcall error03" });
										} else {
											drv.psg_id = null;
											drv.status = "ON";
											drv.save(function (err, result) {
												if (err) {
													res.json({ status: false, msg: "cancelcall error04" });
												} else {
													socket.emit("DriverSocketOn", result);
													res.json({
														status: true,
														msg: "cancell completed after got a driver",
														data: {
															status: response.status,
														}
													});
												}
											});
										}
									}
								)
							}
						});
					}
				}
			}
		);
	};
};




exports.psggetwelcome = function (req, res) {	
	var lang = req.body.lang ? req.body.lang : "en";
	var msgtype = "PSG_WELCOME";
	MessageboardModel.find(
		{
			msgtype: msgtype,
			lang: lang,
			topic: { $ne: '' }, status: "Y",
			expired: { $gt: new Date().getTime() }
		},
		{ topic: 1, detail: 1 },
		{ sort: '-created' },
		//{sort:{'created':-1}},
		function (err, result) {
			if (result == 0) {
				res.json({ status: false, msg: "No data for welcome." });
			} else {
				err ? res.send(err) : res.json({
					status: true,
					data: result
				});
			}
		}
	);
};




exports.psggetabout = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var msgtype = "PSG_ABOUT";

	MessageboardModel.find(
		{
			msgtype: msgtype,
			lang: lang,
			topic: { $ne: '' }, status: "Y",
			expired: { $gt: new Date().getTime() }
		},
		{ topic: 1, detail: 1 },
		{ sort: '-created' },
		//{sort:{'created':-1}},
		function (err, result) {
			if (result == 0) {
				res.json({ status: false, msg: "No data for about us." });
			} else {
				err ? res.send(err) : res.json({
					status: true,
					data: result
				});
			}
		}
	);
};




exports.psggetterm = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var msgtype = "PSG_TERM";

	MessageboardModel.find(
		{
			msgtype: msgtype,
			lang: lang,
			topic: { $ne: '' }, status: "Y",
			expired: { $gt: new Date().getTime() }
		},
		{ topic: 1, detail: 1 },
		{ sort: '-created' },
		//{sort:{'created':-1}},
		function (err, result) {
			if (result == 0) {
				res.json({ status: false, msg: "No data for term & condition." });
			} else {
				err ? res.send(err) : res.json({
					status: true,
					data: result
				});
			}
		}
	);
};




exports.psggetnewsact = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var msgtype = "PSG_NEWSACT";

	MessageboardModel.find(
		{
			msgtype: msgtype,
			lang: lang,
			topic: { $ne: '' }, status: "Y",
			expired: { $gt: new Date().getTime() }
		},
		{ topic: 1, detail: 1 },
		{ sort: '-created' },
		//{sort:{'created':-1}},
		function (err, result) {
			if (result == 0) {
				res.json({ status: false, msg: "No data for news & activities." });
			} else {
				err ? res.send(err) : res.json({
					status: true,
					data: result
				});
			}
		}
	);
};




exports.psggetprbanner = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var msgtype = "PSG_PRBANNER";

	MessageboardModel.find(
		{
			msgtype: msgtype,
			lang: lang,
			topic: { $ne: '' }, status: "Y",
			expired: { $gt: new Date().getTime() }
		},
		{ topic: 1, detail: 1 },
		{ sort: '-created' },
		//{sort:{'created':-1}},
		function (err, result) {
			if (result == 0) {
				res.json({ status: false, msg: "No data for pr banner." });
			} else {
				err ? res.send(err) : res.json({
					status: true,
					data: result
				});
			}
		}
	);
};




exports.psggetpromotion = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var msgtype = "PSG_PROMOTION";

	MessageboardModel.find(
		{
			msgtype: msgtype,
			lang: lang,
			topic: { $ne: '' }, status: "Y",
			expired: { $gt: new Date().getTime() }
		},
		{ topic: 1, detail: 1 },
		{ sort: '-created' },
		//{sort:{'created':-1}},
		function (err, result) {
			if (result == 0) {
				res.json({ status: false, msg: "No data for promotion." });
			} else {
				err ? res.send(err) : res.json({
					status: true,
					data: result
				});
			}
		}
	);
};




exports.psggethistory = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	JoblistModel.find(
		{ psg_device_id: device_id },
		{ ProvinceID: 1, NameEN: 1, NameTH: 1, job_id: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, cgroup: 1, curaddr: 1, destination: 1, tips: 1, detail: 1, createdvia: 1, datepsgcall: 1, datedrvwait: 1, datepsgaccept: 1, datedrvpick: 1, datedrvdrop: 1, datepsgcancel: 1, datedrvcancel: 1 },
		{ limit: 15 , sort: { 'datepsgcall': -1 } },
		function (err, job) {
			if (job == 0) {
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail(lang, 'err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "Job history",
					data: job
				});
			}
		}
	);
};




exports.psggethistorydetail = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var job_id = req.body.job_id;
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}
	JoblistModel.findOne(
		{ psg_device_id: device_id, job_id: job_id },
		{ ProvinceID: 1, NameEN: 1, NameTH: 1, job_id: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, cgroup: 1, curaddr: 1, destination: 1, tips: 1, detail: 1, createdvia: 1, datepsgcall: 1, datedrvwait: 1, datepsgaccept: 1, datedrvpick: 1, datedrvdrop: 1, datepsgcancel: 1, datedrvcancel: 1 },
		function (err, job) {
			if (job == null) {
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail(lang, 'err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "History detail",
					data: job
				});
			}
		}
	);
}



///////////////////////////////// common functions //////////////////////////////////////



function _joblistcreate(job_id, jobtype, drvarroundlist, req) {
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				var ProvinceID = 0;
				var NameEN = "undefined";
				var NameTH = "undefined";
				var ZoneID = 0;
				_do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID);
			} else {
				var ProvinceID = response.ProvinceID;
				var NameEN = response.NameEN;
				var NameTH = response.NameTH;
				var ZoneID = response.ZoneID;
				_do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID);
			}
		}
	);

	function _do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID) {
		PassengerModel.findOne(
			{ device_id: req.body.passenger_ssid },
			function (err, psg) {
				if (psg == null) {
					console.log('_function_joblistcreate no device_id');
				} else {
					JoblistModel.create(
						{
							job_id: job_id,
							displayName: psg.displayName,
							psg_id: psg._id,
							psg_device_id: req.body.passenger_ssid,
							psg_phone: req.body.phone,
							curaddr: req.body.curaddr,
							curlng: req.body.curlng,
							curlat: req.body.curlat,
							curloc: [parseFloat(req.body.curlng), parseFloat(req.body.curlat)],
							destination: req.body.destination,
							deslng: req.body.deslng,
							deslat: req.body.deslat,
							tips: req.body.tips,
							detail: req.body.detail,
							datepsgcall: new Date(),
							createdvia: "SAMSUNG",
							favcartype: req.body.favcartype,
							drvarroundlist: drvarroundlist,
							status: "ON",
							jobtype: jobtype,
							ProvinceID: ProvinceID,
							NameEN: NameEN,
							NameTH: NameTH,
							ZoneID: ZoneID
						},
						function (err, record) {
							if (err) {
								console.log(err)
							} else {
								console.log(' success created SAMSUNG job ')
							}
						}
					);
				}
			}
		);
	}
}



function _joblistupdate(job_id, req, action, cwhere) {
	JoblistModel.findOne(
		{ job_id: job_id },
		function (err, job) {
			if (job == null) {
				console.log(' no job id')
			} else {
				switch (action) {
					case "datepsgaccept":
						job.status = "BUSY";
						job.datepsgaccept = new Date();
						break;
					case "datepsgthanks":
						job.status = "THANKS";
						job.datepsgthanks = new Date();
						break;
					case "datepsgcancel":
						job.status = "PSG_CANCELLED";
						job.datepsgcancel = new Date();
						job.psgcancelwhere = cwhere;
						break;
				}
				job.save(function (err, response) {
					if (err) {
						console.log(err)
					} else {
						console.log(' update job list ')
					}
				});
			}
		}
	);
}



function _drvjoblistupdate(job_id, drv_id, drv_device_id, cgroup, drv_name, drv_phone, drv_carplate, action, cwhere, curlng, curlat) {
	JoblistModel.findOne(
		{ job_id: job_id },
		function (err, job) {
			if (job == null) {
				//console.log (' no job id')
			} else {
				switch (action) {
					case "datedrvwait":
						job.drv_id = drv_id;
						job.drv_device_id = drv_device_id;
						job.drv_name = drv_name;
						job.drv_phone = drv_phone;
						job.drv_carplate = drv_carplate;
						job.cgroup = cgroup;
						job.status = "WAIT";
						job.datedrvwait = new Date();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "WAIT",
							created: new Date()
						});
						break;
					case "datedrvpick":
						job.status = "PICK";
						job.datedrvpick = new Date();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "PICK",
							created: new Date()
						});
						break;
					case "datedrvdrop":
						job.status = "DROP";
						job.datedrvdrop = new Date();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "DROP",
							created: new Date()
						});
						break;
					case "datedrvcancel":
						job.status = "DRV_CANCELLED";
						job.datedrvcancel = new Date();
						job.drv_path.push({
							curloc: [curlng, curlat],
							status: "DRV_CANCELLED",
							created: new Date()
						});
						job.drvcancelwhere = cwhere;
						break;
				}
				job.save(function (err, response) {
					if (err) {
						console.log(err)
					} else {
						console.log(' update job list ')
					}
				});
			}
		}
	);
}



function saveHistory(driverId, passengerId, job_id, success) {
	JoblistModel.findOne({ job_id: job_id }).select('psg_id').exec(function (err, job) {
		if (!err, job) {
			HistoryModel.find({
				_ownerId: driverId,
				_passengerId: passengerId,
				_passengerJobDetail: job._id
			}).exec(function (err, exists) {
				if (!err && exists.length === 0) {
					HistoryModel.create({
						type: 'PASSENGER',
						status: success ? 'SUCCESSED' : 'CANCELLED',
						_ownerId: driverId,
						_passengerId: passengerId,
						_passengerJobDetail: job._id
					}, function (err, history) {
						if (!err && history)
							console.log('history create success');
					});
				}
			});
		}
	});
}



function drvsaveHistory(driverObj, passengerId, success) {
	// canceljob
	if (driverObj.jobtype) {
		if (driverObj.jobtype === 'HOTELS') {
			JoblisthotelModel.findOne({ _id: passengerId }).exec(function (err, job) {
				if (!err && job) {

					HistoryModel.find({
						_ownerId: driverObj._id,
						_callcenterId: job._id_callcenterpsg
					}).exec(function (err, exists) {
						if (!err && exists.length === 0) {
							HistoryModel.create({
								type: 'CALLCENTER',
								status: success ? 'SUCCESSED' : 'CANCELLED',
								_ownerId: driverObj._id,
								_callcenterId: job._id_callcenterpsg,
							}, function (err, history) {
								if (!err && history)
									console.log('history create success');
							});
						}
					});
				}
			});
		} else {
			HistoryModel.find({
				_ownerId: driverObj._id,
				_callcenterId: passengerId
			}).exec(function (err, exists) {
				if (!err && exists.length === 0) {
					HistoryModel.create({
						type: 'CALLCENTER',
						status: success ? 'SUCCESSED' : 'CANCELLED',
						_ownerId: driverObj._id,
						_callcenterId: passengerId,
					}, function (err, history) {
						if (!err && history)
							console.log('history create success');
					});
				}
			});
		}
	}
	// passenger job
	else {
		JoblistModel.findOne({ job_id: driverObj.job_id }).select('psg_id').exec(function (err, job) {
			if (!err, job) {
				HistoryModel.find({
					_ownerId: driverObj._id,
					_passengerId: job.psg_id,
					_passengerJobDetail: job._id
				}).exec(function (err, exists) {
					if (!err && exists.length === 0) {
						HistoryModel.create({
							type: 'PASSENGER',
							status: success ? 'SUCCESSED' : 'CANCELLED',
							_ownerId: driverObj._id,
							_passengerId: job.psg_id,
							_passengerJobDetail: job._id
						}, function (err, history) {
							if (!err && history)
								console.log('history create success');
						});
					}
				});
			}
		});
	}
}



// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}


function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}


function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);
		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};


function _getErrDetail(lang, errCode, callback) {
	ErrorcodeModel.findOne(
		{ errcode: errCode },
		function (err, response) {
			if (response == null) {
				callback('');
			} else {
				callback(response._doc["err" + (lang ? lang : "en")] || response._doc["erren"]);
			}
		}
	);
};