////////////////////////////////////
// Taxi-Beam Ubeam API Callcenter
// version : 2.0
// Date revision: October 27, 2016 
// Created by Hanzen
////////////////////////////////////
var config = require('../../config/func').production;
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var Url = require('url');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var async = require("async");
var compareVersions = require('compare-versions');

var PassengerModel = mongoose.model('PassengerModel');
var CallcenterpsgModel = mongoose.model('CallcenterpsgModel');
var CallcenterannounceModel = mongoose.model('CallcenterannounceModel');
var UserModel = mongoose.model('UserModel');
var PoirecommendedModel = mongoose.model('PoirecommendedModel');
var PathLogModel = mongoose.model('PathLogModel');
var DriversModel = mongoose.model('DriversModel');
var CommentModel = mongoose.model('CommentModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var EmgListModel = mongoose.model('EmgListModel');
var LocalpoiModel = mongoose.model('LocalpoiModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var ParkinglotModel = mongoose.model('ParkinglotModel');
var ParkingqueueModel = mongoose.model('ParkingqueueModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var JoblistModel = mongoose.model('JoblistModel');
var Lk_garageModel = mongoose.model('Lk_garageModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');
var DAkeeplogModel = mongoose.model('DAkeeplogModel');
var HotelsModel = mongoose.model('HotelsModel');
var JoblisthotelModel = mongoose.model('JoblisthotelModel');
var MsghistoryModel = mongoose.model('MsghistoryModel');
var HistoryModel = mongoose.model('HistoryModel');

// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var ios = require('socket.io');
var qs = require('querystring');

var MoneyPerUse = config.MoneyPerUse;
var TimePerUse = config.TimePerUse;
var MoneyPerUseAPP = config.MoneyPerUseAPP;
var TimePerUseAPP = config.TimePerUseAPP;

var TimePerDay = 86400000;       // 86,400,000 millisecond = 1 day
var TimePerHour = 3600000;       // 3,600,000 millisecond = 1 hr.


// Test javascript at "http://js.do/"
function ranSMS() {
    var str = Math.random();
    var str1 = str.toString();
    var res = str1.substring(2, 6);
    return res;
}


function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


function tryParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);
        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
        if (o && typeof o === "object" && o !== null) {
            return o;
        }
    }
    catch (e) { }
    return false;
};



exports.ubeamSearchPassenger = function (req, res) {
    var radian = req.body.radian;
    var amount = req.body.amount;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
    //console.log(curloc)
    if (typeof curlng == 'undefined' && curlng == null) {
        res.json({ status: false, msg: "current longitude is not valid" })
        return;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        res.json({ status: false, msg: "current latitude is not valid" })
        return;
    }
    if (typeof radian == 'undefined' && radian == null) {
        radian = config.ccsearchradian;
    }
    if (typeof amount == 'undefined' && amount == null) {
        amount = config.ccsearchamount;
    }

    CallcenterpsgModel.find(
        {
            //curloc : { $near : curloc, $maxDistance: radian },   => for 2d index => not working
            // do not forget  => db.passengers.createIndex( { curloc : "2dsphere" } )
            curloc: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: curloc
                    },
                    $maxDistance: radian
                    //$minDistance: 1 
                }
            },
            status: "ON"
        },
        { _id: 1, createdjob: 1, phone: 1, createdvia: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1, job_id: 1, jobtype: 1 },
        { limit: amount },
        function (err, psglist) {
            // Donot forget to create 2d Index for drivers collection : curloc!!!!  - > not working  user 2d sphere instead
            if (psglist == 0) {
                res.json({
                    status: false,
                    msg: "No data"
                });
            } else {
                res.json({
                    //msg: "This is passenger list", 
                    status: true,
                    data: psglist
                });
                //Specifies a point for which a geospatial query returns the documents from nearest to farthest.
            }
        }
    );
};



exports.getpoirecommended = function (req, res) {
    // reference : http://docs.mongodb.org/manual/reference/sql-comparison/
    var keyword = req.body.keyword;
    //console.log( 'getpoirecommended = '+keyword)
    PoirecommendedModel.find(
        //{ fname :  new RegExp( '^'+ keyword+'^' )   },
        // { "parkingname": new RegExp(keyword), "cgroup": req.body.cgroup },
        { "parkingname": new RegExp(keyword), "cgroup": req.user.local.cgroup },
        { _id: 1, parkingname: 1, curloc: 1, parkinglot: 1, parkingtype: 1, description: 1, created: 1, updated: 1 },
        { sort: { 'parkinglot': 1, 'parkingname': 1 } },
        function (err, response) {
            if (response == 0) {
                res.json({
                    status: false,
                    msg: 'No data'
                });
            } else {
                res.json({
                    status: true,
                    data: response
                });
            }
        }
    );
};



exports.getpoirecommendedgroup = function (req, res) {
    // example: http://stackoverflow.com/questions/22932364/mongodb-group-values-by-multiple-fields
    // plus : http://www.mkyong.com/mongodb/mongodb-aggregate-and-group-example/
    myquery = {
        $match:
        {
            "cgroup": req.user.local.cgroup
        }
    };

    PoirecommendedModel.aggregate([
        myquery,
        {
            "$group":
            {
                "_id": {
                    "parkingtype": "$parkingtype",
                    "parkingname": "$parkingname",
                    "parkinglot": "$parkinglot",
                    "curloc": "$curloc"
                },
                "parkingCount": { "$sum": 1 },
            }
        },
        {
            "$group": {
                "_id": "$_id.parkingtype",
                "parkinglist": {
                    "$push": {
                        "parkingname": "$_id.parkingname",
                        "parkinglot": "$_id.parkinglot",
                        "curloc": "$_id.curloc",
                        //"parkingCount": "$parkingCount"
                    },
                },
                "parkingCount": { "$sum": "$parkingCount" }
            }
        },
        { "$sort": { "parkingCount": -1 } }
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });
};




exports.getdrvjobsincgroup = function (req, res) {
    // example: http://stackoverflow.com/questions/22932364/mongodb-group-values-by-multiple-fields
    // plus : http://www.mkyong.com/mongodb/mongodb-aggregate-and-group-example/
    myquery = {
        $match:
        {
            "cgroup": "ccubon", "drv_id": { $ne: '' }, "datedrvpick": { $exists: true }
        }
    };
    JoblistModel.aggregate([
        myquery,
        {
            "$group":
            {
                "_id": {
                    "drv_id": "$drv_id",
                    "drv_name": "$drv_name",
                    "drv_carplate": "$drv_carplate",
                    "curaddr": "$curaddr",
                    "destination": "$destination",
                    "curloc": "$curloc",
                    "created": "$created",
                    "datedrvpick": "$datedrvpick",
                    "datedrvdrop": "$datedrvdrop",
                },
                "jobCount": { "$sum": 1 },
            }
        },
        {
            "$group": {
                "_id": "$_id.drv_id",
                "joblist": {
                    "$push": {
                        "drv_name": "$_id.drv_name",
                        "drv_carplate": "$_id.drv_carplate",
                        "curaddr": "$_id.curaddr",
                        "destination": "$_id.destination",
                        "curloc": "$_id.curloc",
                        "created": "$_id.created",
                        "datedrvpick": "$_id.datedrvpick",
                        "datedrvdrop": "$_id.datedrvdrop",
                    },
                },
                "jobCount": { "$sum": "$jobCount" }
            }
        },
        { "$sort": { "jobCount": -1 } }
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });
};




exports.getpoirecommendedgrouplot = function (req, res) {
    // example : http://stackoverflow.com/questions/16772156/nested-grouping-with-mongodb
    // plus : http://www.mkyong.com/mongodb/mongodb-aggregate-and-group-example/
    PoirecommendedModel.aggregate([
        {
            $match:
            {
                "cgroup": req.user.local.cgroup
            }
        },
        {
            "$group":
            {
                "_id": {
                    "parkingtype": "$parkingtype",
                    "parkinglot": "$parkinglot"
                },
                "parkingname": { "$addToSet": "$parkingname" }
            }
        },
        {
            "$group": {
                "_id": "$_id.parkingtype",
                "parkinglist": {
                    "$addToSet": {
                        "parkinglot": "$_id.parkinglot", "parkingname": "$parkingname"
                    }
                }
            }
        }
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });
};

// Example : http://stackoverflow.com/questions/22932364/mongodb-group-values-by-multiple-fields
// db.books.aggregate([
//     { "$group": {
//         "_id": {
//             "addr": "$addr",
//             "book": "$book"
//         },
//         "bookCount": { "$sum": 1 }
//     }},
//     { "$group": {
//         "_id": "$_id.addr",
//         "books": { 
//             "$push": { 
//                 "book": "$_id.book",
//                 "count": "$bookCount"
//             },
//         },
//         "count": { "$sum": "$bookCount" }
//     }},
//     { "$sort": { "count": -1 } },
//     { "$limit": 2 }
// ])
// or /////////////////
// db.books.aggregate([
//     { "$group": {
//         "_id": {
//             "addr": "$addr",
//             "book": "$book"
//         },
//         "bookCount": { "$sum": 1 }
//     }},
//     { "$group": {
//         "_id": "$_id.addr",
//         "books": { 
//             "$push": { 
//                 "book": "$_id.book",
//                 "count": "$bookCount"
//             },
//         },
//         "count": { "$sum": "$bookCount" }
//     }},
//     { "$sort": { "count": -1 } },
//     { "$limit": 2 },
//     { "$project": {
//         "books": { "$slice": [ "$books", 2 ] },
//         "count": 1
//     }}
// ])  


exports.searchnamecar = function (req, res) {
    // reference : http://docs.mongodb.org/manual/reference/sql-comparison/
    var keyword = req.body.keyword;
    DriversModel.find(
        //{ fname :  new RegExp( '^'+ keyword+'^' )   },
        { $or: [{ fname: new RegExp(keyword) }, { lname: new RegExp(keyword) }, { carplate: new RegExp(keyword) }, { car_no: new RegExp(keyword) }] },
        { _id: 1, device_id: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, carplate: 1, cartype: 1, curlng: 1, curlat: 1, curloc: 1, car_no: 1, status: 1 },
        function (err, response) {
            if (response == 0) {
                res.json({
                    status: false,
                    msg: 'No data'
                });
            } else {
                res.json({
                    status: true,
                    data: response
                });
            }
        }
    );
};




exports.senddrvmsg = function (req, res) {
    var drvid = req.body.drvid;
    var msgphone = req.body.todrvphone;
    var msgnote = req.body.todrvtext;

    DriversModel.findOne(
        { _id: drvid },
        { status: 1, updated: 1 },
        function (err, drv) {
            if (drv == null) {
                _driverAutoLogin(req, res);
            } else {
                if (req.body.todrvphone) { drv.msgphone = req.body.todrvphone ? req.body.todrvphone : drv.msgphone; }
                if (req.body.todrvtext) { drv.msgnote = req.body.todrvtext ? req.body.todrvtext : drv.msgnote; }
                drv.msgstatus = "NEW";
                drv.updated = new Date().getTime();
                drv.save(function (err, response) {
                    if (err) {
                        res.json({
                            status: false,
                            msg: "",
                            data: err
                        });
                    } else {
                        res.json({
                            //msg: "success, your broken report has benn sent." 
                            status: true,
                            data: { status: response.status }
                        });
                    }
                });
            }
        }
    );
};




exports.addjoblist = function (socket) {
    return function (req, res) {
        //console.log(req.user)
        var jobid;
        var i = 0;
        var results = [];
        var drvlist = {};
        var username = req.user.local.username;
        var iscontractjob = req.body.isContractJob;
        var provincearea = req.body.provincearea;
        var curaddr = req.body.curaddr;
        var destination = req.body.destination;
        var phone = req.body.phone;
        var detail = req.body.detail;
        // var curlng = 100.57192758657 ;
        // var curlat = 13.7561979331076 ;
        var curlng = req.body.curlng;
        var curlat = req.body.curlat;
        var deslng = req.body.deslng;
        var deslat = req.body.deslat;
        var amount = req.body.amount;
        var ccstation = req.body.ccstation;
        var jobtype = req.body.jobtype;                         // { "QUEUE","ADVANCE" }
        var createdjob = req.body.createdjob;                   //  TIMESTAMP (millisec)
        var cgroup = req.user.local.cgroup;                     // cgroup of admin user
        var _id_garage = req.user._id_garage;                   // _id_garage of admin users
        var status = req.body.status;
        var emittingsocket = "addjoblist";
        var psgtype;
        var jobmode;
        var psg_name = req.body.psgname;
        var psg_phone = req.body.psg_phone;
        var tips = req.body.tips;
        var autophone = req.body.autophone;

        // generate unique id  

        if (status) {
            status = req.body.status
        } else {
            status = "INITIATE";
        }

        //status = "BROADCAST";

        if (amount) {
            if (status == "BROADCAST") {
                amount = 1;
                emittingsocket = "assigndrvtopsg";
                jobmode = "BROADCAST";
            } else {
                amount = amount;
                jobmode = "CALLCENTER";
            }
        } else {
            amount = 0;
        }

        if (createdjob) {
            createdjob = new Date(parseInt(createdjob));
        } else {
            createdjob = new Date();
        }

        if (isNaN(curlat) || isNaN(curlng)) {
            var curloc = [];
        } else {
            var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
        }
        if (isNaN(curlat)) {
            curlat = "0.0";
        }
        if (isNaN(curlng)) {
            curlng = "0.0";
        }
        if (isNaN(deslat) || isNaN(deslng)) {
            var desloc = [];
        } else {
            var desloc = [parseFloat(req.body.deslng), parseFloat(req.body.deslat)];
        }
        if (isNaN(deslat)) {
            deslat = "0.0";
        }
        if (isNaN(deslng)) {
            deslng = "0.0";
        }

        HotelsModel.findOne(
            { phone: phone, cgroup: cgroup, _id_garage: _id_garage },
            function (err, psg) {
                if (psg == null) {
                    // create psg model and add callcenterpsg
                    var device_id = new Date().valueOf() + '-ccpsg';
                    var create_hotel = {
                        device_id: device_id,
                        cgroup: cgroup,
                        _id_garage: _id_garage,
                        email: req.body.email,
                        phone: phone,
                        gender: req.body.gender,
                        lname: req.body.lname,
                        fname: req.body.fname,
                        displayName: req.body.displayName,
                        curaddr: curaddr,
                        curlng: +curlng || 0.0,
                        curlat: +curlat || 0.0,
                        curloc: curloc || [],
                        status: status,
                        createdvia: "CALLCENTER",
                        createdby: username,
                        updated: new Date().getTime(),
                        createdjob: createdjob,
                        created: new Date().getTime(),
                    }
                    HotelsModel.create(create_hotel, function (err, response) {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log('create hotel from callcenter')
                            if (status == "BROADCAST") {
                                Lk_garageModel.findOne(
                                    { _id: req.user._id_garage },
                                    function (err, garage) {
                                        if (garage == null) {
                                            console.log('_id_garage == null')
                                        } else {
                                            DriversModel.aggregate(
                                                [
                                                    {
                                                        $geoNear: {
                                                            query: {
                                                                cgroup: cgroup,
                                                                status: "ON",
                                                                active: "Y",
                                                                //favcartype: { $in: [ [] , req.body.cartype ] }
                                                            },
                                                            near: { type: "Point", coordinates: curloc },
                                                            distanceField: "dist",
                                                            maxDistance: garage.searchpsgradian,
                                                            num: garage.searchpsgamount,
                                                            spherical: true
                                                        }
                                                    },
                                                    {
                                                        $project: {
                                                            _id: 1,
                                                            dist: 1,
                                                            dist_km: {
                                                                $divide: [
                                                                    {
                                                                        $subtract: [
                                                                            { $multiply: ['$dist', 1] },
                                                                            { $mod: [{ $multiply: ['$dist', 1] }, 10] }
                                                                        ]
                                                                    }, 1000]
                                                            },
                                                        },
                                                    },
                                                    { $sort: { dist: 1 } }
                                                ],
                                                function (err, drvlist) {
                                                    response.drvarroundlist = drvlist;
                                                    response.status = "BROADCAST";
                                                    response.save(function (err, result) {
                                                        if (err) {
                                                            res.json({ status: false, msg: "error", data: err });
                                                        } else {
                                                            for (i = 0; i < amount; i++) {
                                                                InsertCallcenterpsgJob(i, response._id, cgroup, iscontractjob, provincearea, detail, amount, jobtype, createdjob, ccstation, drvlist);
                                                            }
                                                            res.json({
                                                                status: true,
                                                                msg: "Add job completed",
                                                                data: {
                                                                    "jobtype": jobtype
                                                                }
                                                            })
                                                        }
                                                    });
                                                }
                                            );
                                        }
                                    }
                                )
                            } else {
                                for (i = 0; i < amount; i++) {
                                    InsertCallcenterpsgJob(i, response._id, cgroup, iscontractjob, provincearea, detail, amount, jobtype, createdjob, ccstation, drvlist);
                                    if (i == amount - 1) {
                                        res.json({
                                            status: true,
                                            msg: "Add job completed",
                                            data: {
                                                "jobtype": jobtype
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    });
                } else {
                    // Update psg model
                    if (req.body.curaddr) { psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr; }
                    if (req.body.curlng) { psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng; }
                    if (req.body.curlat) { psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat; }
                    if (curloc) { psg.curloc = curloc ? curloc : psg.curloc; }
                    if (req.body.detail) { psg.detail = req.body.detail ? req.body.detail : psg.detail; }
                    if (req.body.createdjob) { psg.createdjob = createdjob ? createdjob : psg.createdjob; }
                    psg.save(function (err, response) {
                        if (err) {
                            res.json({ status: false, msg: "error", data: err });
                        } else {
                            if (status == "BROADCAST") {
                                Lk_garageModel.findOne(
                                    { _id: req.user._id_garage },
                                    function (err, garage) {
                                        if (garage == null) {
                                            console.log('_id_garage == null')
                                        } else {
                                            DriversModel.aggregate(
                                                [
                                                    {
                                                        $geoNear: {
                                                            query: {
                                                                cgroup: cgroup,
                                                                status: "ON",
                                                                active: "Y",
                                                                //favcartype: { $in: [ [] , req.body.cartype ] }
                                                            },
                                                            near: { type: "Point", coordinates: curloc },
                                                            distanceField: "dist",
                                                            maxDistance: garage.searchpsgradian,
                                                            num: garage.searchpsgamount,
                                                            spherical: true,
                                                        }
                                                    },
                                                    {
                                                        $project: {
                                                            _id: 1,
                                                            dist: 1,
                                                            dist_km: {
                                                                $divide: [
                                                                    {
                                                                        $subtract: [
                                                                            { $multiply: ['$dist', 1] },
                                                                            { $mod: [{ $multiply: ['$dist', 1] }, 10] }
                                                                        ]
                                                                    }, 1000]
                                                            },
                                                        },
                                                    },
                                                    { $sort: { dist: 1 } }
                                                ],
                                                function (err, drvlist) {
                                                    response.drvarroundlist = drvlist;
                                                    response.status = "BROADCAST";
                                                    response.save(function (err, result) {
                                                        if (err) {
                                                            res.json({ status: false, msg: "error", data: err });
                                                        } else {
                                                            for (i = 0; i < amount; i++) {
                                                                console.log(i)
                                                                InsertCallcenterpsgJob(i, response._id, cgroup, iscontractjob, provincearea, detail, amount, jobtype, createdjob, ccstation, drvlist);
                                                                if (i == amount - 1) {
                                                                    res.json({
                                                                        status: true,
                                                                        msg: "Add job completed",
                                                                        data: {
                                                                            "jobtype": jobtype
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    });
                                                }
                                            );
                                        }
                                    }
                                )
                            } else {
                                drvlist = {};
                                for (i = 0; i < amount; i++) {
                                    console.log(i)
                                    InsertCallcenterpsgJob(i, response._id, cgroup, iscontractjob, provincearea, detail, amount, jobtype, createdjob, ccstation, drvlist);
                                    if (i == amount - 1) {
                                        res.json({
                                            status: true,
                                            msg: "Add job completed",
                                            data: {
                                                "jobtype": jobtype
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    });
                }
            }
        );

        function InsertCallcenterpsgJob(i, _id_hotel, cgroup, iscontractjob, provincearea, detail, amount, jobtype, createdjob, ccstation, drvlist) {
            jobid = new Date().valueOf() + '-' + i;
            var datacallcenterpsg = {
                _id_hotel: _id_hotel,
                device_id: jobid,
                cgroup: cgroup,
                iscontractjob: iscontractjob,
                email: jobid + "@temp.com",
                phone: phone,
                autophone: autophone,
                provincearea: provincearea,
                curaddr: curaddr,
                curlng: +curlng || 0.0,
                curlat: +curlat || 0.0,
                curloc: curloc || [],
                destination: destination,
                deslng: +deslng || 0.0,
                deslat: +deslat || 0.0,
                desloc: desloc || [],
                detail: detail,
                jobtype: jobtype,
                status: status,
                psgtype: psgtype,
                jobmode: jobmode,
                createdvia: "CALLCENTER",
                ccstation: ccstation,
                createdby: username,
                updated: new Date().getTime(),
                createdjob: createdjob,
                ccwaitingtime: req.user.local.ccwaitingtime,
                created: new Date().getTime(),
                drvarroundlist: drvlist,
                jobhistory: [{
                    action: "add job list",
                    status: status,
                    actionby: username,
                    created: new Date()
                }]
            }

            CallcenterpsgModel.create(datacallcenterpsg, function (err, response) {
                if (err) {
                    console.log(' create CallcenterpsgModel from cc addjoblist error : ' + err)
                } else {

                    var dataJoblisthotel = {
                        job_id: jobid,
                        hotel_id: _id_hotel,
                        _id_callcenterpsg: response._id,
                        device_id: jobid,
                        cgroup: cgroup,
                        iscontractjob: iscontractjob,
                        email: jobid + "@temp.com",
                        hotel_phone: phone,
                        psg_name: psg_name,
                        psg_phone: psg_phone,
                        psgtype: psgtype,
                        jobmode: jobmode,
                        provincearea: provincearea,
                        curaddr: curaddr,
                        curlng: +curlng || 0.0,
                        curlat: +curlat || 0.0,
                        curloc: curloc || [],
                        destination: destination,
                        deslng: +deslng || 0.0,
                        deslat: +deslat || 0.0,
                        desloc: desloc || [],
                        detail: detail,
                        jobtype: jobtype,
                        status: status,
                        tips: tips,
                        _id_garage: req.user._id_garage,
                        createdvia: "CALLCENTER",
                        ccstation: ccstation,
                        createdby: username,
                        updated: new Date().getTime(),
                        createdjob: createdjob,
                        ccwaitingtime: req.user.local.ccwaitingtime,
                        created: new Date().getTime(),
                        drvarroundlist: drvlist,
                        jobhistory: [{
                            action: "add job list",
                            status: status,
                            actionby: username,
                            created: new Date()
                        }]
                    }
                    JoblisthotelModel.create(dataJoblisthotel, function (err, result) {
                        if (err) {
                            console.log(' create joblisthotel from cc addjoblist error : ' + err)
                        } else {
                            //socket.emit("CCSocketOn", response); 
                            socket.of('/' + req.user.local.cgroup).emit(emittingsocket, response);
                        }
                    })
                }
            });
        }
    };
};




exports.editjoblist = function (socket) {
    return function (req, res) {
        var username = req.user.local.username;
        var psg_id = req.body.psg_id;
        var curaddr = req.body.curaddr;
        var destination = req.body.destination;
        var phone = req.body.phone;
        var detail = req.body.detail;
        var curlng = req.body.curlng;
        var curlat = req.body.curlat;
        var jobtype = req.body.jobtype; // { "QUEUE","ADVANCE" }
        var createdjob = req.body.createdjob;   //  TIMESTAMP (millisec)
        var drv_carplate = req.body.drv_carplate;
        var cccomment = req.body.cccomment;

        var checklinejob;

        if (typeof drv_carplate == 'undefined' && drv_carplate == null) {
            drv_carplate = "";
            checklinejob = "";
        } else {
            drv_carplate = drv_carplate.trim();
            checklinejob = drv_carplate.substring(drv_carplate.length - 1, drv_carplate.length);
            // clear_drv_carplate = drv_carplate.substring(0, drv_carplate.length-1)   ;
            // clear_drv_carplate = drv_carplate.replace("+", "");
        }

        if (createdjob) {
            createdjob = new Date(parseInt(createdjob));
        } else {
            createdjob = new Date().getTime();
        }

        if (typeof curlng == 'undefined' && typeof curlat == 'undefined') {
            var curloc = [];
        } else {
            var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
        }
        if (typeof curlng == 'undefined' && curlng == null) {
            curlng = null;
        }
        if (typeof curlat == 'undefined' && curlat == null) {
            curlat = null;
        }
        var deslng = req.body.deslng;
        var deslat = req.body.deslat;
        if (typeof deslng == 'undefined' && typeof deslat == 'undefined') {
            var desloc = [];
        } else {
            var desloc = [parseFloat(req.body.deslng), parseFloat(req.body.deslat)];
        }
        if (typeof deslng == 'undefined' && deslng == null) {
            deslng = "";
        }
        if (typeof deslat == 'undefined' && deslat == null) {
            deslat = "";
        }

        CallcenterpsgModel.findOne(
            { _id: psg_id },
            function (err, psg) {
                if (psg == null) {
                    res.json({ status: false, msg: "No psg_id", data: err });
                } else {

                    psg.jobhistory.push({
                        action: "CC editjob",
                        psg_id: psg_id,
                        curaddr: curaddr,
                        destination: destination,
                        phone: phone,
                        detail: detail,
                        jobtype: jobtype, // { "QUEUE","ADVANCE" }
                        createdjob: createdjob,   //  TIMESTAMP (millisec)
                        drv_carplate: drv_carplate,
                        cccomment: cccomment,
                        actionby: username,
                        created: new Date()
                    });

                    if (req.body.phone) { psg.phone = req.body.phone ? req.body.phone : psg.phone; }
                    if (req.body.curaddr) { psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr; }
                    // if (req.body.curlng) { psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng; }
                    // if (req.body.curlat) { psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat; }
                    // if (curloc) { psg.curloc = curloc ? curloc : psg.curloc; }
                    if (req.body.destination) { psg.destination = req.body.destination ? req.body.destination : psg.destination; }
                    // if (req.body.deslng) { psg.deslng = req.body.deslng ? req.body.deslng : psg.deslng; }
                    // if (req.body.deslat) { psg.deslat = req.body.deslat ? req.body.deslat : psg.deslat; }
                    // if (desloc) { psg.desloc = desloc ? desloc : psg.desloc; }
                    if (req.body.detail) { psg.detail = req.body.detail ? req.body.detail : psg.detail; }
                    if (req.body.jobtype) { psg.jobtype = req.body.jobtype ? req.body.jobtype : psg.jobtype; }
                    if (req.body.createdjob) { psg.createdjob = createdjob ? createdjob : psg.createdjob; }
                    if (req.body.drv_carplate) { psg.drv_carplate = drv_carplate ? drv_carplate : psg.drv_carplate; }
                    if (req.body.cccomment !== undefined) { psg.cccomment = req.body.cccomment; }
                    if (req.body.cccomment !== undefined) { psg.cccommentby = username }
                    if (checklinejob == '+') {
                        if (psg.status == "ASSIGNED") {

                        } else {
                            psg.status = "DPENDING_LINE";
                            psg.createdvia = "LINE";
                        }
                    } else if (checklinejob !== '+') {
                        psg.createdvia = "CALLCENTER";
                    }
                    psg.editingby = username;
                    psg.updated = new Date().getTime();
                    psg.save(function (err, response) {
                        if (err) {
                            res.json({ status: false, msg: "error", data: err });
                        } else {
                            var data = {
                                "psg_id": response._id,
                                "cgroup": response.cgroup,
                                "curaddr": response.curaddr,
                                "destination": response.destination,
                                "phone": response.phone,
                                "detail": response.detail,
                                "status": response.status,
                                "updated": response.updated,
                                "curlng": response.curlng,
                                "curlat": response.curlat,
                                "curloc": response.curloc,
                                "deslng": response.deslng,
                                "deslat": response.deslat,
                                "desloc": response.desloc,
                                "jobtype": response.jobtype,
                                "createdjob": response.createdjob,
                                "createdvia": response.createdvia,
                                "ccstation": response.ccstation,
                                "drv_carplate": response.drv_carplate,
                                "cccomment": response.cccomment
                            };

                            JoblisthotelModel.findOne(
                                { _id_callcenterpsg: psg_id },
                                function (err, psgmodel) {
                                    if (psgmodel == null) {
                                        res.json({ status: false, msg: "no psg found", data: err });
                                    } else {
                                        if (req.body.phone) { psgmodel.phone = req.body.phone ? req.body.phone : psgmodel.phone; }
                                        if (req.body.curaddr) { psgmodel.curaddr = req.body.curaddr ? req.body.curaddr : psgmodel.curaddr; }
                                        // if (req.body.curlng) { psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng; }
                                        // if (req.body.curlat) { psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat; }
                                        // if (curloc) { psg.curloc = curloc ? curloc : psg.curloc; }
                                        if (req.body.destination) { psgmodel.destination = req.body.destination ? req.body.destination : psgmodel.destination; }
                                        // if (req.body.deslng) { psg.deslng = req.body.deslng ? req.body.deslng : psg.deslng; }
                                        // if (req.body.deslat) { psg.deslat = req.body.deslat ? req.body.deslat : psg.deslat; }
                                        // if (desloc) { psg.desloc = desloc ? desloc : psg.desloc; }
                                        if (req.body.detail) { psgmodel.detail = req.body.detail ? req.body.detail : psgmodel.detail; }
                                        psgmodel.save(function (err, result) {
                                            if (err) {
                                                res.json({ status: false, msg: "error", data: err });
                                            } else {
                                                console.log('editjoblist - ' + req.user.local.username + ' - ' + new Date());
                                                socket.of('/' + req.user.local.cgroup).emit('editjoblist', data);
                                                res.json({
                                                    status: true,
                                                    data: data
                                                });
                                            }
                                        })
                                    }
                                }
                            );
                        }
                    });
                }
            }
        );
    };
};



exports.doccbroadcast = function (socket) {
    return function (req, res) {
        var psg_id = req.body.psg_id;
        var jobid;
        var i = 0;
        var results = [];
        var drvlist = {};
        var username = req.user.local.username;
        var iscontractjob = req.body.isContractJob;
        var provincearea = req.body.provincearea;
        var curaddr = req.body.curaddr;
        var destination = req.body.destination;
        var phone = req.body.phone;
        var detail = req.body.detail;
        // var curlng = 100.57192758657 ;
        // var curlat = 13.7561979331076 ;
        var curlng = req.body.curlng;
        var curlat = req.body.curlat;
        var deslng = req.body.deslng;
        var deslat = req.body.deslat;
        var amount = 1; // req.body.amount ;                       Only 1 for BROADCAST Mode
        var ccstation = req.body.ccstation;
        var jobtype = req.body.jobtype;                         // { "QUEUE","ADVANCE" }
        var createdjob = req.body.createdjob;                   //  TIMESTAMP (millisec)
        var cgroup = req.user.local.cgroup;                     // cgroup of admin user
        var _id_garage = req.user._id_garage;                   // _id_garage of admin users
        var status = req.body.status;
        var emittingsocket = "addjoblist";
        var jobmode = "BROADCAST";
        var searchpsgradian = req.body.searchpsgradian;
        var searchpsgamount = req.body.searchpsgamount;
        // generate unique id  

        socket.emit("onJobReAssign", req.body);

        if (status) {
            status = req.body.status
        } else {
            status = "INITIATE";
        }
        status = "BROADCAST";           // set to BROADCAST for doccbroadcast API
        if (amount) {
            if (status == "BROADCAST") {
                amount = 1;
                emittingsocket = "assigndrvtopsg";
            } else {
                amount = amount;
            }
        } else {
            amount = 0;
        }

        if (typeof curlng == 'undefined' && typeof curlat == 'undefined') {
            var curloc = [];
        } else {
            var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
            //var curloc = [100.57192758657,13.7561979331076] ;         
        }
        if (typeof curlng == 'undefined' && curlng == null) {
            curlng = "";
        }
        if (typeof curlat == 'undefined' && curlat == null) {
            curlat = "";
        }

        if (typeof deslng == 'undefined' && typeof deslat == 'undefined') {
            var desloc = [];
        } else {
            var desloc = [parseFloat(req.body.deslng), parseFloat(req.body.deslat)];
        }
        if (typeof deslng == 'undefined' && deslng == null) {
            deslng = "";
        }
        if (typeof deslat == 'undefined' && deslat == null) {
            deslat = "";
        }

        CallcenterpsgModel.findOne(
            { _id: psg_id },
            { status: 1, updated: 1, jobtype: 1, cgroup: 1, jobhistory: 1, assignlineby: 1, cccomment: 1, ccstation: 1, createdjob: 1, createdvia: 1, curaddr: 1, curloc: 1, desloc: 1, destination: 1, detail: 1, dpendingjob: 1, drv_carplate: 1, drv_id: 1, phone: 1 },
            function (err, psg) {
                if (psg == null) {
                    res.json({ status: false, msg: "No passenger found for this id" });
                } else {
                    var myarr = ["INITIATE", "BROADCAST", "DPENDING", "DEPENDING_REJECT", "DEPENDING_TIMEOUT"];
                    var old_status = psg.status;
                    if ((myarr.indexOf(old_status) > -1)) {
                        console.log('in array is true for ' + old_status)
                        if (old_status == "DPENDING") {
                            DriversModel.findOne(
                                { _id: psg.drv_id },
                                function (err, drv) {
                                    if (drv == null) {
                                        res.json({
                                            status: false,
                                            msg: "No driver was found"
                                        });
                                    } else {
                                        drv.psg_id = "";
                                        drv.job_id = "";
                                        drv.status = "ON";
                                        drv.msgstatus = "OLD";
                                        drv.save(function (err, response) {
                                            if (err) {
                                                res.json({ status: false, msg: "error", data: err });
                                            } else {
                                                _sub_ClearBiddingDrv();
                                                _doccbroadcast(_id_garage, username, cgroup, curloc, psg, "CC broadcast job over direct job");
                                            }
                                        });
                                    }
                                }
                            )
                        } else {
                            _sub_ClearBiddingDrv();
                            _doccbroadcast(_id_garage, username, cgroup, curloc, psg, "CC broadcast normal job ");
                        }
                    } else {
                        console.log('out of array is true for ' + old_status)
                        res.json({
                            status: false,
                            errcode: "",
                            msg: "ไม่สามารถจ่ายงานได้ เนื่องจากงานนี้มีผู้รับไปแล้ว"
                        });
                    }
                }
            }
        );


        function _doccbroadcast(_id_garage, username, cgroup, curloc, psg, ccmsg) {
            Lk_garageModel.findOne(
                { _id: _id_garage },
                function (err, garage) {
                    if (garage == null) {
                        console.log('_id_garage == null')
                    } else {
                        if (typeof searchpsgradian == 'undefined') {
                            searchpsgradian = garage.searchpsgradian;
                        }
                        if (typeof searchpsgamount == 'undefined') {
                            searchpsgamount = garage.searchpsgamount;
                        }

                        DriversModel.aggregate(
                            [
                                {
                                    $geoNear: {
                                        query: {
                                            cgroup: cgroup,
                                            status: "ON",
                                            active: "Y",
                                            //favcartype: { $in: [ [] , req.body.cartype ] }
                                        },
                                        near: { type: "Point", coordinates: curloc },
                                        distanceField: "dist",
                                        maxDistance: parseInt(searchpsgradian),
                                        num: parseInt(searchpsgamount),
                                        spherical: true
                                    }
                                },
                                {
                                    $project: {
                                        _id: 1,
                                        dist: 1,
                                        dist_km: {
                                            $divide: [
                                                {
                                                    $subtract: [
                                                        { $multiply: ['$dist', 1] },
                                                        { $mod: [{ $multiply: ['$dist', 1] }, 10] }
                                                    ]
                                                }, 1000]
                                        },
                                    },
                                },
                                { $sort: { dist: 1 } }
                            ],
                            function (err, drvlist) {
                                psg.drvarroundlist = drvlist;
                                psg.updated = new Date().getTime();
                                psg.ccbroadcastby = username;
                                psg.datebroadcast = new Date().getTime();
                                psg.dpendingjob = new Date().getTime();
                                psg.status = status;
                                psg.jobmode = jobmode;
                                psg.jobhistory.push({
                                    action: ccmsg,
                                    actionby: username,
                                    created: new Date()
                                });
                                psg.save(function (err, response) {
                                    if (err) {
                                        res.json({ status: false, msg: "error", data: err });
                                    } else {
                                        JoblisthotelModel.findOne(
                                            { _id_callcenterpsg: psg_id },
                                            function (err, psgmodel) {
                                                if (psgmodel == null) {
                                                    res.json({ status: false, msg: "no psg found", data: err });
                                                } else {
                                                    psgmodel.status = status;
                                                    psgmodel.jobmode = jobmode;
                                                    psgmodel.drvarroundlist = drvlist;
                                                    psgmodel.occupied = false;
                                                    psgmodel.biddingstart = false;
                                                    psgmodel.biddingend = false;
                                                    psgmodel.biddingtimeout = null;
                                                    psgmodel.drvbiddingjob = [];
                                                    psgmodel.createdjob = new Date();
                                                    psgmodel.save(function (err, result) {
                                                        if (err) {
                                                            res.json({ status: false, msg: "error", data: err });
                                                        } else {
                                                            console.log('assign by doccbroadcast - ' + username + ' - ' + new Date());
                                                            socket.of('/' + cgroup).emit("assigndrvtopsg", psg);
                                                            result.drvarroundlist.forEach(function (driver, index) {
                                                                socket.to(driver._id.toString()).emit('WAKEUP_DEVICE', { title: 'มีงานจากศูนย์วิทยุ', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
                                                            });

                                                            res.json({
                                                                status: true,
                                                                psg_data: psg
                                                            });
                                                        }
                                                    })
                                                }
                                            }
                                        );
                                    }
                                });
                            }
                        );
                    }
                }
            )
        }


        function _sub_ClearBiddingDrv() {
            JoblisthotelModel.findOne(
                { _id_callcenterpsg: psg_id },
                function (err, result) {
                    if (err) return handleError(err);
                    if (result == null) {
                        console.log(' no winning drv id 111 ')
                    } else {
                        var lostdrv_id = result.drvbiddingjob;
                        if (lostdrv_id && lostdrv_id.length > 0) {
                            for (var i = 0, len = lostdrv_id.length; i < len; i++) {
                                DriversModel.findOneAndUpdate(
                                    { _id: lostdrv_id[i]._id, status: "WAIT" },
                                    { $set: { status: "ON", msgstatus: "OLD" } },
                                    { upsert: false, new: true },
                                    function (err, drv) {
                                        if (err) return handleError(err);
                                        if (result == null) {
                                            console.log(' no winning drv id 111 ')
                                        } else {
                                            console.log('  LOST BIDDING JOBS ')
                                            socket.emit("DriverSocketOn", drv);
                                            //socket.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ขออภัยค่ะ คุณไม่สามารถรับงานนี้ได้', message: 'งานนี้ได้ถูกส่งให้คนขับที่ใกล้ผู้โดยสารที่สุดแล้ว' });
                                        }
                                    }
                                );
                            }
                        }
                    }
                }
            );
        }


    }
}




exports.getinitiatelist = function (req, res) {
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js 
    //console.log ( ' cgroup = ' + req.user.local.cgroup )
    var currentdatetime = new Date().getTime();
    CallcenterpsgModel.find(
        { status: "INITIATE", "createdjob": { $lt: new Date(currentdatetime + 10800000) }, cgroup: req.user.local.cgroup },
        { _id: 1, status: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, deslng: 1, deslat: 1, desloc: 1, detail: 1, createdjob: 1, drv_id: 1, jobtype: 1, createdvia: 1, ccstation: 1, cgroup: 1, cccomment: 1 },
        { sort: { 'createdjob': 1 } },
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}




exports.getadvancelist = function (req, res) {
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    var currentdatetime = new Date().getTime();
    CallcenterpsgModel.find(
        { status: "INITIATE", jobtype: "ADVANCE", "createdjob": { $gt: new Date(currentdatetime + 11100000) }, cgroup: req.user.local.cgroup },
        { _id: 1, status: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, deslng: 1, deslat: 1, desloc: 1, createdjob: 1, drv_id: 1, jobtype: 1, detail: 1, createdvia: 1, ccstation: 1, cgroup: 1, cccomment: 1 },
        { sort: { 'createdjob': 1 } },
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}




exports.getdpendinglist = function (req, res) {
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    var currentdatetime = new Date().getTime();
    CallcenterpsgModel.find(
        { status: { $in: ["BROADCAST", "DPENDING", "DEPENDING_REJECT", "DPENDING_LINE", "DEPENDING_TIMEOUT"] }, cgroup: req.user.local.cgroup },
        { _id: 1, status: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, deslng: 1, deslat: 1, desloc: 1, createdjob: 1, drv_id: 1, jobtype: 1, detail: 1, createdvia: 1, drv_carplate: 1, prefixcarplate: 1, carplate: 1, ccstation: 1, cgroup: 1, cccomment: 1 },
        { sort: { 'createdjob': 1 } },
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}




exports.getreassignjobdetail = function (req, res) {
    var psg_id = req.body.psg_id;
    CallcenterpsgModel.findOne(
        { _id: psg_id },
        { _id: 1, phone: 1, curaddr: 1, destination: 1, curloc: 1, desloc: 1, detail: 1, status: 1, jobtype: 1, createdjob: 1, drv_id: 1, drv_carplate: 1, carplate: 1, prefixcarplate: 1, createdvia: 1, ccstation: 1, cccomment: 1 },
        function (err, psgdata) {
            if (!err) {
                DriversModel.findOne(
                    { _id: psgdata.drv_id },
                    { _id: 1, fname: 1, lname: 1, carplate: 1, prefixcarplate: 1, carplate_formal: 1, curloc: 1, status: 1, jobtype: 1, imgface: 1, psg_id: 1, imgface: 1, phone: 1 },
                    function (err, drvdata) {
                        if (!err) {
                            res.json({
                                status: true,
                                drv_register: true,
                                psg_data: psgdata,
                                drv_data: drvdata
                            });
                        } else {
                            //res.json({ status: false, msg: "No data for this driver", err: err}) 
                            res.json({
                                status: true,
                                msg: "No driver's data in our system.",
                                drv_register: false,
                                psg_data: psgdata
                            });
                        }
                    }
                )
            } else {
                res.json({ status: false, msg: "No data for this passenger", err: err })
            }
        }
    )
}




exports.deletejob = function (socket) {
    return function (req, res) {
        console.log('deletejobdeletejobdeletejob')
        var psg_id = req.body.psg_id;
        CallcenterpsgModel.findOne(
            { _id: psg_id },
            { status: 1, updated: 1, jobtype: 1, cgroup: 1, jobhistory: 1, drv_id: 1, drvarroundlist: 1 },
            function (err, psg) {
                if (psg == null) {
                    res.json({ status: false, msg: "No passenger found for this id" });
                } else {
                    var old_status = psg.status;
                    var old_drv_id = psg.drv_id;
                    var drvarroundlist = psg.drvarroundlist;
                    if (old_status === "WAIT") {
                        _sub_resetmsgstatusfordrvbiddinglist(old_drv_id);
                    }
                    _sub_resetmsgstatus(old_drv_id);
                    psg.updated = new Date().getTime();
                    psg.deletejobby = req.user.local.username;
                    psg.datedeletejob = new Date().getTime();
                    psg.drv_id = null;
                    psg.status = "DELETED";
                    psg.jobhistory.push({
                        action: "CC delete job",
                        actionby: req.user.local.username,
                        created: new Date()
                    });
                    psg.save(function (err, response) {
                        if (err) {
                            res.json({ status: false, msg: "error", data: err });
                        } else {
                            // For BROADCAST Mode
                            JoblisthotelModel.findOne(
                                { _id_callcenterpsg: psg_id },
                                function (err, psgmodel) {
                                    if (psgmodel == null) {
                                        res.json({ status: false, msg: "no psg found", data: err });
                                    } else {
                                        psgmodel.status = "DELETED";
                                        psgmodel.save(function (err, result) {
                                            if (err) {
                                                res.json({ status: false, msg: "error", data: err });
                                            } else {
                                                console.log('deletejob - ' + req.user.local.username + ' - ' + new Date());
                                                socket.of('/' + req.user.local.cgroup).emit('deletejob', {
                                                    _id: response._id,
                                                    status: old_status,
                                                    jobtype: response.jobtype,
                                                    cgroup: response.cgroup
                                                });
                                                res.json({
                                                    status: true,
                                                    data: {
                                                        psg_id: psg_id,
                                                        status: response.status
                                                    }
                                                });
                                            }
                                        })
                                    }
                                }
                            );
                        }
                    });
                }
            }
        );

        function _sub_resetmsgstatus(old_drv_id) {
            // DriversModel.findOneAndUpdate(
            //     { 
            //         psg_id: psg_id, _id: old_drv_id 
            //     }, 
            //     {
            //         status: "ON",
            //         psg_id: null,
            //         job_id: null,
            //         msgstatus: "OLD"
            //     }, 
            //     { upsert: false }, function (err, doc) {
            //     if (err) { console.log(err); }
            // });
            DriversModel.findOne(
                { psg_id: psg_id, _id: old_drv_id }, 
                { _id: 1, msgstatus:1, status:1, psg_id:1, device_id: 1, job_id:1, cgroup: 1, fname: 1, lname: 1, phone: 1, stauts: 1, drv_shift:1, firstshift:1, firstshift_id:1, firstshift_type:1, shift_start:1, shift_end:1 },
                function (err, drv) {
                    if (drv == null) {
                        res.json({ status: false, msg: "cancelcall error03" });
                    } else {
                        //if(drv.firstshift==true && drv.firstshift_id==psg_id){
                        if(drv.firstshift==true){
                            var preshift = drv.drv_shift;
                            var postshift = drv.drv_shift + 1;
                            ShifttransactionModel.create({
                                _id_driver: drv._id,
                                preshift: preshift,
                                postshift: postshift,
                                shift_start: null,
                                shift_end: null,
                                firstshift_type: "REFUND_SHIFT",
                                firstshift_id: psg_id
                            }, function (err, history) {
                                if (!err && history)
                                    console.log('shift history create success');
                            });
                            drv.drv_shift = postshift;
                            drv.firstshift = true;
                            drv.firstshift_id = null;
                            drv.firstshift_type = null; 
                            drv.shift_start = new Date();
                            drv.shift_end = new Date();
                        }
                        drv.status = "ON";
                        drv.psg_id = null;
                        drv.job_id = null;
                        drv.msgstatus = "OLD";
                        drv.save(function (err, result) {
                            if (err) {
                                console.log(err)
                            } else {
                                socket.emit("DriverSocketOn", result);
                            }
                        });
                    }
                }
            )            
        }

        function _sub_resetmsgstatusfordrvbiddinglist(drvarroundlist) {

        }
    }
}




exports.checkdrvstatus = function (req, res) {
    var carplate = req.body.carplate;
    var drv_id = req.body.drv_id;

    if (drv_id) {
        DriversModel.findOne(
            { carplate: carplate, _id: drv_id, cgroup: req.user.local.cgroup }, //new RegExp( carplate ) },
            { _id: 1, device_id: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, carplate: 1, prefixcarplate: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, status: 1, cgroup: 1 },
            //{ carplate : carplate },
            function (err, drv) {
                if (drv == null) {
                    res.json({
                        status: false,
                        msg: "No carplate was found"
                    });
                } else if (typeof drv_id == 'undefined' && drv_id == null) {
                    res.json({
                        status: false,
                        msg: "Matched carplate but  drv_id is null",
                        data: drv
                    });
                } else if (drv_id == "") {
                    res.json({
                        status: false,
                        msg: "Matched carplate but didnot give drv_id",
                        data: drv
                    });
                } else if (drv._id != drv_id) {
                    res.json({
                        status: false,
                        msg: "Matched carplate but did not match drv_id",
                        data: drv
                    });
                } else if (drv.status != "ON") {
                    res.json({
                        status: false,
                        msg: "Status is not ON, this car is unavailable",
                        data: drv
                    });
                } else {
                    res.json({
                        status: true,
                        msg: "Status is ON, this car is available",
                        data: drv
                    });
                }
            }
        )
    } else {
        DriversModel.findOne(
            { carplate: carplate, cgroup: req.user.local.cgroup }, //new RegExp( carplate ) },
            { _id: 1, device_id: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, carplate: 1, prefixcarplate: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, status: 1, cgroup: 1 },
            //{ carplate : carplate },
            function (err, drv) {
                if (drv == null) {
                    res.json({
                        status: false,
                        msg: "No carplate was found"
                    });
                } else if (typeof drv_id == 'undefined' && drv_id == null) {
                    res.json({
                        status: true,
                        msg: "Matched carplate but  drv_id is null",
                        data: drv
                    });
                } else if (drv_id == "") {
                    res.json({
                        status: true,
                        msg: "Matched carplate but didnot give drv_id",
                        data: drv
                    });
                } else if (drv._id != drv_id) {
                    res.json({
                        status: true,
                        msg: "Matched carplate but did not match drv_id",
                        data: drv
                    });
                } else if (drv.status != "ON") {
                    res.json({
                        status: true,
                        msg: "Status is not ON, this car is unavailable",
                        data: drv
                    });
                } else {
                    res.json({
                        status: true,
                        msg: "Status is ON, this car is available",
                        data: drv
                    });
                }
            }
        )
    }
}




exports.assigndrvtopsg = function (socket) {
    return function (req, res) {
        var psg_id = req.body.psg_id;
        var drv_id = req.body.drv_id;
        var lineconfirm = req.body.lineconfirm;
        var carplate = req.body.carplate;
        var prefixcarplate = req.body.prefixcarplate;
        var drv_carplate = req.body.drv_carplate;
        var username = req.user.local.username;
        var checklinejob;
        var mathrandom = Math.random();
        var cgroup = req.user.local.cgroup;
        var reqbody = req.body;
        var ccwatingtime = req.user.local.ccwatingtime;
        var savephone = "";
        var savecur = "";
        var savedes = "";
        var provincearea = "";
        console.log(' assigndrvtopsg drv_id = ' + drv_id)
        socket.emit("onJobReAssign", req.body);
        DriversModel.findOneAndUpdate({ psg_id: psg_id }, {
            status: "ON",
            psg_id: null,
            job_id: null,
            msgstatus: "OLD"
        }, { upsert: false }, function (err, doc) {
            if (err) { console.log(err); }
        });

        if (typeof drv_carplate == 'undefined' && drv_carplate == null) {
            drv_carplate = "";
            carplate = "";
            prefixcarplate = "";
            checklinejob = "";
        } else {
            drv_carplate = drv_carplate ? drv_carplate.trim() : "";
            carplate = carplate ? carplate.trim() : "";
            prefixcarplate = prefixcarplate ? prefixcarplate.trim() : "";
            checklinejob = drv_carplate.substring(drv_carplate.length - 1, drv_carplate.length);
            clear_drv_carplate = drv_carplate.substring(0, drv_carplate.length - 1);
            clear_drv_carplate = clear_drv_carplate.replace("+", "");
        }
        CallcenterpsgModel.findOne(
            { _id: psg_id },
            { _id: 1, cgroup: 1, phone: 1, curaddr: 1, destination: 1, curloc: 1, desloc: 1, detail: 1, status: 1, jobtype: 1, createdjob: 1, drv_id: 1, update: 1, drv_carplate: 1, carplate: 1, prefixcarplate: 1, createdvia: 1, assigningby: 1, ccstation: 1, dpendingjob: 1, cccomment: 1, provincearea: 1 },
            function (err, response) {
                if (response == null) {
                    res.json({ status: false, msg: "No passenger found for this id", data: err });
                } else if (response.status == "ASSIGNED") {
                    res.json({ status: false, msg: "This passenger has been assigned.", data: err });
                } else {
                    var dAteNow = new Date().getTime();
                    var dAteNew = new Date(response.dpendingjob).getTime();
                    savephone = response.phone;
                    savecur = response.curaddr;
                    savedes = response.destination;
                    provincearea = response.provincearea.code;
                    if (dAteNow - dAteNew < 2000) {
                        console.log(' assigndrvtopsg double ')
                        //res.json({ status: false , msg: "No passenger found for this id" });
                        CallcenterpsgModel.findOne(
                            { _id: psg_id },
                            function (err, psg) {
                                if (psg == null) {
                                    res.json({ status: false, msg: "No passenger found for this id" });
                                } else {
                                    psg.jobhistory.push({
                                        action: "CC added double",
                                        status: "n/a",
                                        drv_id: drv_id,
                                        drv_carplate: drv_carplate,
                                        carplate: carplate,
                                        prefixcarplate: prefixcarplate,
                                        actionby: username,
                                        mathrandom: mathrandom,
                                        reqbody: reqbody,
                                        reqcgroup: cgroup,
                                        created: new Date()
                                    });
                                    psg.save(function (err, result) {
                                        if (err) {
                                            res.json({ status: false, msg: "error", data: err });
                                        } else {
                                            res.json({ status: false, msg: "No passenger found for this id" });
                                        }
                                    });
                                }
                            }
                        );
                    } else {
                        if (response.status == "BROADCAST") {
                            // For BROADCAST Mode
                            JoblisthotelModel.findOne(
                                { _id_callcenterpsg: psg_id },
                                function (err, psgmodel) {
                                    if (psgmodel == null) {
                                        res.json({ status: false, msg: "no psg found", data: err });
                                    } else {
                                        psgmodel.status = "DISMISSED";
                                        psgmodel.occupied = true;
                                        psgmodel.save(function (err, result) {
                                            if (err) {
                                                res.json({ status: false, msg: "error", data: err });
                                            } else {
                                                _sub_ClearBiddingDrv();
                                                _sub_assigndrvtopsg(cgroup, reqbody, username, ccwatingtime, drv_id, psg_id, response)
                                            }
                                        })
                                    }
                                }
                            );
                        } else {
                            _sub_assigndrvtopsg(cgroup, reqbody, username, ccwatingtime, drv_id, psg_id, response)
                        }
                    }
                }
            }
        );


        function _sub_ClearBiddingDrv() {
            JoblisthotelModel.findOne(
                { _id_callcenterpsg: psg_id },
                function (err, result) {
                    if (err) return handleError(err);
                    if (result == null) {
                        console.log(' no winning drv id 111 ')
                    } else {
                        var lostdrv_id = result.drvbiddingjob;
                        for (var i = 0, len = lostdrv_id.length; i < len; i++) {
                            DriversModel.findOneAndUpdate(
                                { _id: lostdrv_id[i]._id, status: "WAIT" },
                                { $set: { status: "ON", msgstatus: "OLD" } },
                                { upsert: false, new: true },
                                function (err, drv) {
                                    if (err) return handleError(err);
                                    if (result == null) {
                                        console.log(' assigndrvtopsg _sub_ClearBiddingDrv no winning drv id 111 ')
                                    } else {
                                        socket.emit("DriverSocketOn", drv);
                                        socket.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ขออภัยค่ะ คุณไม่สามารถรับงานนี้ได้', message: 'งานนี้ได้ถูกส่งให้คนขับที่ใกล้ผู้โดยสารที่สุดแล้ว' });
                                    }
                                }
                            );
                        }
                    }
                }
            );
        }


        function _sub_autonotified(drv_carplate, savephone, savecur, savedes, cgroup, provincearea) {
            var reqset = '{ "drv_carplate": "' + drv_carplate + '", "savephone": "' + savephone + '", "savecur": "' + savecur + '", "savedes": "' + savedes + '", "cgroup": "' + cgroup + '", "provincearea": "' + provincearea + '" }';
            var postData = JSON.parse(JSON.stringify(reqset));
            var options = {
                host: 'lite.taxi-beam.site',
                port: 80,
                path: '/service/bg/_bgservice_autonotified',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };
            var request = http.request(options, function (response) {
                response.on('data', function (dog) {
                });
                response.on('end', function (cat) {
                });
            });
            request.on('error', function (err) {
                console.log('cannot do _sub_autonotified')
            });
            request.write(postData);
            request.end();
        }


        function _sub_assigndrvtopsg(cgroup, reqbody, username, ccwatingtime, drv_id, psg_id, response) {
            DriversModel.findOne(
                { _id: drv_id, status: "ON", cgroup: cgroup },
                { _id: 1, fname: 1, lname: 1, carplate: 1, prefixcarplate: 1, carplate_formal: 1, curloc: 1, status: 1, jobtype: 1, imgface: 1, psg_id: 1 },
                function (err, drv) {
                    if (drv == null) {
                        // จ่ายนอกระบบ
                        CallcenterpsgModel.findOne(
                            { _id: psg_id },
                            { _id: 1, cgroup: 1, phone: 1, curaddr: 1, destination: 1, curloc: 1, desloc: 1, detail: 1, status: 1, jobtype: 1, createdjob: 1, drv_id: 1, update: 1, drv_carplate: 1, carplate: 1, prefixcarplate: 1, createdvia: 1, assigningby: 1, ccstation: 1, dpendingjob: 1, cccomment: 1, jobhistory: 1 },
                            function (err, psg) {
                                if (psg == null) {
                                    res.json({ status: false, msg: "No passenger found for this id" });
                                } else {
                                    psg.carplate = carplate;
                                    psg.prefixcarplate = prefixcarplate;
                                    psg.drv_carplate = drv_carplate;
                                    psg.drv_id = null;
                                    psg.dpendingjob = new Date().getTime();
                                    psg.dassignedjob = new Date().getTime();
                                    psg.updated = new Date().getTime();
                                    if (checklinejob == '+') {
                                        if (lineconfirm == 'Y') {
                                            psg.status = "ASSIGNED";
                                            psg.assignlineby = username;
                                            psg.drv_carplate = clear_drv_carplate;
                                            psg.carplate = carplate;
                                            psg.prefixcarplate = prefixcarplate;
                                            psg.createdvia = "LINE";
                                            psg.jobhistory.push({
                                                action: "Line confirmed",
                                                status: "ASSIGNED",
                                                carplate: carplate,
                                                prefixcarplate: prefixcarplate,
                                                drv_carplate: clear_drv_carplate,
                                                actionby: username,
                                                mathrandom: mathrandom,
                                                reqbody: reqbody,
                                                //reqcgroup : JSON.stringify(req.user) , 
                                                created: new Date()
                                            });
                                        } else {
                                            psg.assigningby = username;
                                            psg.status = "DPENDING_LINE";
                                            psg.createdvia = "LINE";
                                            psg.jobhistory.push({
                                                action: "Line inserted",
                                                status: "DPENDING_LINE",
                                                carplate: carplate,
                                                prefixcarplate: prefixcarplate,
                                                drv_carplate: drv_carplate,
                                                actionby: username,
                                                mathrandom: mathrandom,
                                                reqbody: reqbody,
                                                //reqcgroup : JSON.stringify(req.user) , 
                                                created: new Date()
                                            });
                                        }
                                    } else {
                                        psg.assigningby = username;
                                        psg.status = "ASSIGNED";
                                        psg.createdvia = "CALLCENTER";
                                        psg.jobhistory.push({
                                            action: "CC assigned",
                                            status: "ASSIGNED",
                                            carplate: carplate,
                                            prefixcarplate: prefixcarplate,
                                            drv_carplate: drv_carplate,
                                            actionby: username,
                                            mathrandom: mathrandom,
                                            reqbody: reqbody,
                                            //reqcgroup : JSON.stringify(req.user) , 
                                            created: new Date()
                                        });
                                    }
                                    psg.save(function (err, result) {
                                        if (err) {
                                            res.json({ status: false, msg: "error", data: err });
                                        } else {
                                            console.log('assign out - ' + username + ' - ' + new Date());
                                            _sub_autonotified(drv_carplate, savephone, savecur, savedes, cgroup, provincearea);
                                            socket.of('/' + cgroup).emit("assigndrvtopsg", psg);
                                            res.json({
                                                status: true,
                                                psg_data: psg
                                            });
                                        }
                                    });
                                }
                            }
                        );
                    } else {
                        drv.psg_detail = response.detail;
                        drv.psg_destination = response.destination;
                        drv.psg_curaddr = response.curaddr;
                        drv.psg_id = psg_id;
                        drv.msgnote = "รับ: " + response.curaddr + "\nส่ง : " + response.destination + "\n" + response.phone;
                        drv.msgphone = response.phone;
                        drv.msgstatus = "NEW";
                        drv.msgtimeout = ccwatingtime;
                        drv.status = "DPENDING";
                        drv.jobtype = response.jobtype;
                        drv.updated = new Date().getTime();
                        //drv.status = "OFF";
                        drv.save(function (err, response) {
                            if (err) {
                                res.json({ status: false, msg: "error", data: err });
                            } else {
                                CallcenterpsgModel.findOne(
                                    { _id: psg_id },
                                    { _id: 1, cgroup: 1, phone: 1, curaddr: 1, destination: 1, curloc: 1, desloc: 1, detail: 1, status: 1, jobtype: 1, createdjob: 1, drv_id: 1, update: 1, drv_carplate: 1, carplate: 1, prefixcarplate: 1, createdvia: 1, assigningby: 1, ccstation: 1, dpendingjob: 1, cccomment: 1, jobhistory: 1 },
                                    function (err, psg) {
                                        if (psg == null) {
                                            res.json({ status: false, msg: "No passenger found for this id" });
                                        } else {
                                            psg.jobhistory.push({
                                                action: "CC dpending",
                                                status: "DPENDING",
                                                drv_id: drv_id,
                                                carplate: drv.carplate,
                                                prefixcarplate: drv.prefixcarplate,
                                                drv_carplate: drv.carplate_formal,
                                                actionby: username,
                                                mathrandom: mathrandom,
                                                reqbody: reqbody,
                                                //reqcgroup : JSON.stringify(req.user) , 
                                                created: new Date()
                                            });
                                            psg.nccdpendingcount = 1;
                                            psg.drv_id = drv_id;
                                            psg.carplate = drv.carplate;
                                            psg.prefixcarplate = drv.prefixcarplate;
                                            psg.drv_carplate = drv.carplate_formal;
                                            psg.updated = new Date().getTime();
                                            psg.status = "DPENDING";
                                            psg.assigningby = username;
                                            psg.dpendingjob = new Date().getTime();
                                            psg.save(function (err, result) {
                                                if (err) {
                                                    res.json({ status: false, msg: "error", data: err });
                                                } else {
                                                    console.log('assign in - ' + username + ' - ' + new Date());
                                                    _sub_autonotified(drv.carplate, savephone, savecur, savedes, cgroup, provincearea);
                                                    socket.emit("DriverSocketOn", drv);
                                                    socket.of('/' + cgroup).emit("assigndrvtopsg", result);
                                                    socket.to(result.drv_id.toString()).emit('WAKEUP_DEVICE', { title: 'มีงานจากศูนย์วิทยุ', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
                                                    res.json({
                                                        status: true,
                                                        psg_data: psg,
                                                        drv_data: drv
                                                    });
                                                }
                                            });
                                        }
                                    }
                                );
                            }
                        });
                    }
                }
            );
        }
    };
};




exports.getassignlist = function (req, res) {
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    //db.getCollection('passengers').find({"status":"INITIATE" , "createdjob": { $gt : new Date(10800000) } }, { _id:1, device_id:1,curaddr: 1, destination:1, phone:1 , createdjob:1 })
    var currentdatetime = new Date().getTime();
    CallcenterpsgModel.find(
        { status: { $in: ["ASSIGNED", "BUSY", "PICK"] }, cgroup: req.user.local.cgroup },
        { _id: 1, status: 1, phone: 1, curaddr: 1, destination: 1, updated: 1, createdjob: 1, created: 1, jobtype: 1, drv_id: 1, drv_carplate: 1, createdvia: 1, dassignedjob: 1, dpendingjob: 1, ccstation: 1, assigningby: 1, cgroup: 1, cccomment: 1 },
        { sort: { 'createdjob': -1 } },
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}




exports.getsendjoblist = function (req, res) {
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    CallcenterpsgModel.find(
        { status: { $in: ["INITIATE"] }, cgroup: req.user.local.cgroup },
        { _id: 1, status: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, deslng: 1, deslat: 1, desloc: 1, createdjob: 1, jobtype: 1, drv_id: 1, createdvia: 1, dassignedjob: 1, dpendingjob: 1, ccstation: 1, assigningby: 1, cgroup: 1, cccomment: 1 },
        { sort: { 'createdjob': 1 } },
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}
/*
    CallcenterpsgModel.find(
        { status: { $in: [ "INITIATE","TIMEOUT" ] } },
        // ['created'],
        { _id: 1, status: 1, phone: 1, curaddr: 1, destination: 1, updated: 1, createdjob:1, created:1, drv_id: 1 },
        {sort:{'created':1}},
        function(err,psglist){      
            if(psglist == 0) {              
                res.setHeader('Content-Type', 'application/json;charset=utf-8');
                res.setHeader('Access-Control-Allow-Origin', '*');
                    res.send(JSON.stringify({                   
                    status: false,
                }));                
            } else {                
                res.json({                              
                    status: true,
                    data: psglist                       
                });
            }
        }
    ); 
    */




exports.getpsgsphonelist = function (req, res) {
    // sql compare: https://docs.mongodb.org/manual/reference/sql-comparison/
    var phone = req.body.phone;
    CallcenterpsgModel.find(
        { phone: new RegExp(phone), cgroup: req.user.local.cgroup },
        { _id: 1, phone: 1 },
        //{sort: '-created'},
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}




exports.getpsgstatuslist = function (req, res) {
    var status = req.body.status;
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    CallcenterpsgModel.find(
        { status: status, cgroup: req.user.local.cgroup },
        { _id: 1, status: 1, phone: 1, curaddr: 1, destination: 1, updated: 1, drv_id: 1, created: 1, createdvia: 1, ccstation: 1, assigningby: 1, cgroup: 1, cccomment: 1 },
        //{sort: '-created'},
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}




exports.getPassengerDetail = function (req, res) {
    var psg_id = req.body.psg_id;
    var drv_id = '';
    CallcenterpsgModel.findOne(
        { _id: psg_id },
        { _id: 1, phone: 1, curaddr: 1, destination: 1, curloc: 1, desloc: 1, detail: 1, status: 1, jobtype: 1, createdjob: 1, drv_id: 1, drv_carplate: 1, carplate: 1, prefixcarplate: 1, createdvia: 1, ccstation: 1, assigningby: 1, cgroup: 1, dpendingjob: 1, cccomment: 1, provincearea: 1 },
        function (err, psgdata) {
            if (!err) {
                try {
                    if (psgdata.drv_id) {
                        drv_id = psgdata.drv_id;
                    }
                }
                catch (err) {
                    drv_id = '';
                }
                DriversModel.findOne(
                    { _id: drv_id },
                    { _id: 1, fname: 1, lname: 1, carplate: 1, prefixcarplate: 1, carplate_formal: 1, curloc: 1, status: 1, jobtype: 1, imgface: 1, psg_id: 1, imgface: 1 },
                    function (err, drvdata) {
                        if (!err) {
                            res.json({
                                status: true,
                                drv_register: true,
                                psg_data: psgdata,
                                drv_data: drvdata
                            });
                        } else {
                            res.json({
                                status: true,
                                msg: "No driver's data in our system.",
                                drv_register: false,
                                psg_data: psgdata
                            });
                        }
                    }
                )
            } else {
                res.json({ status: false, msg: "No data for this passenger", err: err })
            }
        }
    )
}




exports.checkinitiatepsg = function (req, res) {
    var psg_id = req.body.psg_id;
    CallcenterpsgModel.findOne(
        { _id: psg_id, cgroup: req.user.local.cgroup },
        function (err, psg) {
            if (psg == null) {
                res.json({
                    status: false,
                    msg: "no passengers"
                });
            } else {
                if (psg.status == 'INITIATE') {
                    res.json({
                        status: true,
                        msg: "available"
                    });
                } else {
                    res.json({
                        status: false,
                        msg: "n/a"
                    });
                }
            }
        }
    );
}




exports.checkavailabledrv = function (req, res) {
    var drv_id = req.body.drv_id;
    DriversModel.findOne(
        { _id: drv_id, cgroup: req.user.local.cgroup },
        { status: 1, updated: 1 },
        function (err, drv) {
            if (drv == null) {
                res.json({
                    status: false,
                    msg: "no drivers"
                });
            } else {
                if (drv.status == 'ON') {
                    res.json({
                        status: true,
                        msg: "available"
                    });
                } else {
                    res.json({
                        status: false,
                        msg: "n/a"
                    });
                }
            }
        }
    );
}



function saveHistory(driverId, callcenterId, success) {

    HistoryModel.find({
        _ownerId: driverId,
        _callcenterId: callcenterId,
    }).exec(function (err, exists) {
        if (!err && exists.length === 0) {
            HistoryModel.create({
                type: 'CALLCENTER',
                status: success ? 'SUCCESSED' : 'CANCELLED',
                _ownerId: driverId,
                _callcenterId: callcenterId,
            }, function (err, history) {
                if (!err && history)
                    console.log('history create success');
            });
        }
    });
}





exports.cancelpsgdrv = function (socket) {
    return function (req, res) {
        var psg_id = req.body.psg_id;
        CallcenterpsgModel.findOne({ _id: psg_id },
            function (err, response) {
                if (response == null) {
                    res.json({
                        status: false,
                        msg: "psg n/a"
                    });
                } else {
                    DriversModel.findOne({ _id: response.drv_id },
                        function (err, drv) {
                            if (drv == null) {
                                CallcenterpsgModel.findOne({ _id: psg_id },
                                    function (err, psg) {
                                        if (psg == null) {
                                            res.json({ status: false, msg: "This passenger is not available." });
                                        } else {
                                            psg.jobhistory.push({
                                                action: "CC cancel job",
                                                old_drv_carplate: response.drv_carplate,
                                                status: "INITIATE",
                                                actionby: req.user.local.username,
                                                created: new Date()
                                            });
                                            psg.createdvia = "";
                                            psg.drv_id = null;
                                            psg.drv_carplate = "";
                                            psg.cccomment = "";
                                            psg.updated = new Date().getTime();
                                            psg.status = "INITIATE";
                                            psg.save(function (err, response) {
                                                if (err) {
                                                    res.json({ status: false, msg: "error", data: err });
                                                } else {
                                                    JoblisthotelModel.findOne(
                                                        { _id_callcenterpsg: psg_id },
                                                        function (err, psgmodel) {
                                                            if (psgmodel == null) {
                                                                res.json({ status: false, msg: "no psg found", data: err });
                                                            } else {
                                                                psgmodel.occupied = false;
                                                                psgmodel.save(function (err, result) {
                                                                    if (err) {
                                                                        res.json({ status: false, msg: "error", data: err });
                                                                    } else {
                                                                        socket.of('/' + req.user.local.cgroup).emit("cancelpsgdrv", psg);
                                                                        res.json({
                                                                            status: true,
                                                                            data: {
                                                                                psg_id: psg_id,
                                                                                status: response.status,
                                                                                cgroup: response.cgroup
                                                                            }
                                                                        });
                                                                    }
                                                                })
                                                            }
                                                        }
                                                    );
                                                }
                                            });
                                        }
                                    }
                                );
                            } else {
                                saveHistory(drv._id, drv.psg_id, false);
                                drv.psg_id = "";
                                drv.updated = new Date().getTime();
                                drv.status = "ON";
                                drv.save(function (err, response) {
                                    if (err) {
                                        res.json({ status: false, msg: "error", data: err });
                                    } else {
                                        CallcenterpsgModel.findOne(
                                            { _id: psg_id },
                                            function (err, psg) {
                                                if (psg == null) {
                                                    res.json({ status: false, msg: "This passenger is not available." });
                                                } else {
                                                    psg.drv_id = null;
                                                    psg.drv_carplate = "";
                                                    psg.cccomment = "";
                                                    psg.updated = new Date().getTime();
                                                    psg.status = "INITIATE";
                                                    psg.jobhistory.push({
                                                        action: "CC cancel job",
                                                        old_drv_id: response.drv_id,
                                                        old_drv_carplate: response.drv_carplate,
                                                        status: "INITIATE",
                                                        actionby: req.user.local.username,
                                                        created: new Date()
                                                    });
                                                    psg.save(function (err, response) {
                                                        if (err) {
                                                            res.json({ status: false, msg: "error", data: err });
                                                        } else {
                                                            // socket.of('/'+req.user.local.cgroup).emit("cancelpsgdrv", psg);
                                                            // res.json({ 
                                                            //     status: true ,
                                                            //     data: { 
                                                            //         psg_id: psg_id,
                                                            //         status: response.status,
                                                            //         cgroup: response.cgroup
                                                            //     }
                                                            // });
                                                            JoblisthotelModel.findOne(
                                                                { _id_callcenterpsg: psg_id },
                                                                function (err, psgmodel) {
                                                                    if (psgmodel == null) {
                                                                        res.json({ status: false, msg: "no psg found", data: err });
                                                                    } else {
                                                                        psgmodel.occupied = false;
                                                                        psgmodel.save(function (err, result) {
                                                                            if (err) {
                                                                                res.json({ status: false, msg: "error", data: err });
                                                                            } else {
                                                                                socket.of('/' + req.user.local.cgroup).emit("cancelpsgdrv", psg);
                                                                                res.json({
                                                                                    status: true,
                                                                                    data: {
                                                                                        psg_id: psg_id,
                                                                                        status: response.status,
                                                                                        cgroup: response.cgroup
                                                                                    }
                                                                                });
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                            );
                                                        }
                                                    });
                                                }
                                            }
                                        );
                                    }
                                });
                            }
                        }
                    );
                }
            }
        );
    };
};





exports.searchdrv = function (req, res) {

    var _id = req.body._id;
    var device_id = req.body.device_id;
    var status = req.body.status;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
    var radian = req.body.radian;
    var amount = req.body.amount;
    // short form : if(req.body.uid) { device.uid = req.body.uid ? req.body.uid : device.uid; }     
    if (typeof curlng == 'undefined' && curlng == null) {
        res.json({ status: false, msg: "current longitude is not valid" })
        return;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        res.json({ status: false, msg: "current latitude is not valid" })
        return;
    }
    if (typeof radian == 'undefined' && radian == null) {
        radian = config.ccsearchdrvradian;
    }
    if (typeof amount == 'undefined' && amount == null) {
        amount = config.ccsearchdrvamount;
    }
    if (typeof status == 'undefined' && status == null) {
        status = ["ON", "HOLD", "WAIT", "BUSY", "PICK", "BROADCAST", "ASSIGNED", "DPENDING", "DEPENDING_REJECT", "DEPENDING_TIMEOUT"];
    } else {
        status = [status];
    }

    condition = { active: "Y", curloc: { $near: curloc, $maxDistance: radian }, status: { $in: status }, cgroup: req.user.local.cgroup };
    //condition = { active:"Y", curloc : { $near : curloc, $maxDistance: radian } ,  status: { $in: status  } };

    DriversModel.find(
        condition,
        { _id: 1, device_id: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, carplate_formal: 1, prefixcarplate: 1, carplate: 1, cartype: 1, carcolor: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, car_no: 1, psg_id: 1, active: 1, jobtype: 1, status: 1, moneyapp: 1, moneygarage: 1, expdateapp: 1, expdategarage: 1, brokenname: 1, brokendetail: 1, cgroup: 1 },
        { limit: amount },
        function (err, drvlist) {
            // Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!                             
            if (drvlist == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    //msg: condition,                 
                    data: drvlist
                });
                //Specifies a point for which a geospatial query returns the documents from nearest to farthest.
            }
        }
    );
};




exports.countTaxi = function (req, res) {
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var status = req.body.status;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
    var radian = req.body.radian;
    var amount = req.body.amount;
    // short form : if(req.body.uid) { device.uid = req.body.uid ? req.body.uid : device.uid; }     
    if (typeof curlng == 'undefined' && curlng == null) {
        res.json({ status: false, msg: "current longitude is not valid" })
        return;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        res.json({ status: false, msg: "current latitude is not valid" })
        return;
    }
    if (typeof radian == 'undefined' && radian == null) {
        radian = config.ccsearchdrvradian;
    }
    if (typeof amount == 'undefined' && amount == null) {
        amount = config.ccsearchdrvamount;
    }
    if (typeof status == 'undefined' && status == null) {
        status = ["ON", "HOLD", "OFF", "WAIT", "BUSY", "PICK", "BROKEN", "ASSIGNED", "DPENDING", "DEPENDING_REJECT", "DEPENDING_TIMEOUT"];
    } else {
        status = [status];
    }

    condition = { active: "Y", curloc: { $near: curloc, $maxDistance: radian }, status: { $in: status }, cgroup: req.user.local.cgroup };

    DriversModel.find(
        condition,
        { _id: 1, status: 1 },
        { limit: config.ccsearchdrvamount },
        function (err, drivers) {
            if (drivers == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    data: drivers
                });
            }
        }
    );
};




exports.DrvGGAddMoney = function (req, res) {
    // add to array example: http://tech-blog.maddyzone.com/node/add-update-delete-object-array-schema-mongoosemongodb
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var moneygarage = parseInt(req.body.moneygarage);

    DriversModel.findOne(
        { _id: _id, device_id: device_id },
        function (err, drv) {
            if (drv == null) {
                res.json({ status: false, msg: "No driver found for this id" });
            } else {
                drv.moneygarage = drv.moneygarage + moneygarage;
                drv.topupgaragehistory.push({ moneyadded: moneygarage, addeddate: new Date() });
                drv.save(function (err, response) {
                    if (err) {
                        res.json({ status: false, msg: "error", data: err });
                    } else {
                        res.json({
                            status: true,
                            data: {
                                moneygarage: response.moneygarage
                            }
                        });
                    }
                });
            }
        }
    );
};




exports.DrvGGAddHour = function (req, res) {
    var device_id = req.body.device_id;
    var moneygarage = req.body.moneygarage;
    DriversModel.findOne(
        { device_id: device_id },
        function (err, drv) {
            if (drv == null) {
                res.json({ status: false, msg: " No driver was found " });
            } else {
                drv.moneygarage = drv.moneygarage + moneygarage;
                drv.save(function (err, response) {
                    if (err) {
                        res.json({ status: false, msg: "error", data: err });
                    } else {
                        res.json({
                            status: true,
                            data: {
                                device_id: response.device_id,
                                moneygarage: response.moneygarage
                            }
                        });
                    }
                });
            }
        }
    );
};




exports.askGGcharge = function (req, res) {
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var drv_ans = req.body.drv_ans;
    var nowdate = new Date().getTime();
    var drvexpdategrg;
    var newexpdate;
    if (drv_ans == 'Y') {
        DriversModel.findOne(
            { _id: _id, device_id: device_id },
            { moneygarage: 1, expdategarage: 1, paymentgaragehistory: 1 },
            function (err, drv) {
                if (drv == null) {
                    res.json({ status: false, msg: " No driver was found " });
                } else {
                    previousmoney = drv.moneygarage;
                    currentmoney = drv.moneygarage - MoneyPerUse;
                    oldexpdate = drv.expdategarage;
                    console.log(drv.expdategarage)
                    if (drv.expdategarage) {
                        drvexpdategrg = new Date(drv.expdategarage).getTime();
                    } else {
                        drvexpdategrg = 0;
                    }


                    if (drv.moneygarage < MoneyPerUse) {
                        res.json({
                            status: false,
                            msg: "จำนวนเงินในระบบเหลือไม่เพียงพอต่อการใช้งาน กรุณาเติมเงินค่ะ"
                        });
                    } else {


                        if (nowdate >= drvexpdategrg) {
                            newexpdate = new Date().getTime() + (TimePerUse * TimePerHour);

                            drv.moneygarage = drv.moneygarage - MoneyPerUse;
                            drv.expdategarage = newexpdate;
                            drv.paymentgaragehistory.push({
                                previousmoney: previousmoney,
                                currentmoney: currentmoney,
                                moneypayment: MoneyPerUse,
                                oldexpdate: oldexpdate,
                                newexpdate: new Date(newexpdate),
                                actiondate: new Date()
                            });
                            drv.save(function (err, response) {
                                if (err) {
                                    res.json({ status: false, msg: "error", data: err });
                                } else {
                                    res.json({
                                        status: true,
                                        msg: "สามารถใช้งานระบบได้",
                                        data: {
                                            id: response._id,
                                            device_id: response.device_id,
                                            moneygarage: response.moneygarage,
                                            expdategarage: response.expdategarage
                                        }
                                    });
                                }
                            });
                        } else {
                            res.json({
                                status: true,
                                msg: "คนขับ ยังมีเวลาเพียงพอในการใช้งานระบบ",
                                data: {
                                    id: drv._id,
                                    device_id: drv.device_id,
                                    moneygarage: drv.moneygarage,
                                    expdategarage: drv.expdategarage
                                }
                            });
                            /*
                            newexpdate = drvexpdategrg+ (TimePerUse*TimePerHour);

                            drv.moneygarage = drv.moneygarage - MoneyPerUse ;
                            drv.expdategarage= newexpdate ;                                     
                            drv.paymentgaragehistory.push({ 
                                previousmoney: previousmoney,  
                                currentmoney: currentmoney ,
                                moneypayment: MoneyPerUse, 
                                oldexpdate: oldexpdate,
                                newexpdate: new Date(newexpdate) ,
                                addeddate: new Date() 
                            });                                 
                            drv.save(function(err, response) {
                                if(err) {
                                     res.json({ status: false , msg: "error", data: err  });
                                } else {
                                     res.json({                                                             
                                        status: true ,  
                                        msg: "002",                                                     
                                        data: {
                                            id : response._id,
                                            device_id : response.device_id,
                                            moneygarage : response.moneygarage,
                                            expdategarage: response.expdategarage
                                        } 
                                    });
                                }
                            });
                            */
                        }
                    }
                }
            }
        );
    } else {
        res.json({
            status: false,
            msg: "Driver does not want to pay and use callcenter service"
        });
    }
};




exports.DrvAPPAddMoney = function (req, res) {
    // add to array example: http://tech-blog.maddyzone.com/node/add-update-delete-object-array-schema-mongoosemongodb
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var moneyapp = parseInt(req.body.moneyapp);

    DriversModel.findOne(
        { _id: _id, device_id: device_id },
        function (err, drv) {
            if (drv == null) {
                res.json({ status: false, msg: "No driver found for this id" });
            } else {
                drv.moneyapp = drv.moneyapp + moneyapp;
                drv.topupapphistory.push({ moneyadded: moneyapp, addeddate: new Date() });
                drv.save(function (err, response) {
                    if (err) {
                        res.json({ status: false, msg: "error", data: err });
                    } else {
                        res.json({
                            status: true,
                            data: {
                                moneyapp: response.moneyapp
                            }
                        });
                    }
                });
            }
        }
    );
};




exports.DrvAPPAddHour = function (req, res) {
    var device_id = req.body.device_id;
    var moneyapp = req.body.moneyapp;
    DriversModel.findOne(
        { device_id: device_id },
        function (err, drv) {
            if (drv == null) {
                res.json({ status: false, msg: " No driver was found " });
            } else {
                drv.moneyapp = drv.moneyapp + moneyapp;
                drv.save(function (err, response) {
                    if (err) {
                        res.json({ status: false, msg: "error", data: err });
                    } else {
                        res.json({
                            status: true,
                            data: {
                                device_id: response.device_id,
                                moneyapp: response.moneyapp
                            }
                        });
                    }
                });
            }
        }
    );
};




exports.askAPPcharge = function (req, res) {
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var drv_ans = req.body.drv_ans;
    var nowdate = new Date().getTime();
    var drvexpdategrg;
    var newexpdate;
    if (drv_ans == 'Y') {
        DriversModel.findOne(
            { _id: _id, device_id: device_id },
            { moneyapp: 1, expdateapp: 1, paymentapphistory: 1 },
            function (err, drv) {
                if (drv == null) {
                    res.json({ status: false, msg: " No driver was found " });
                } else {
                    previousmoney = drv.moneyapp;
                    currentmoney = drv.moneyapp - MoneyPerUseAPP;
                    oldexpdate = drv.expdateapp;
                    console.log(drv.expdateapp)
                    if (drv.expdateapp) {
                        drvexpdategrg = new Date(drv.expdateapp).getTime();
                    } else {
                        drvexpdategrg = 0;
                    }

                    if (drv.moneyapp < MoneyPerUseAPP) {
                        res.json({
                            status: false,
                            msg: "จำนวนเงินในระบบเหลือไม่เพียงพอต่อการใช้งาน กรุณาเติมเงินค่ะ"
                        });
                    } else {

                        if (nowdate >= drvexpdategrg) {
                            newexpdate = new Date().getTime() + (TimePerUseAPP * TimePerHour);

                            drv.moneyapp = drv.moneyapp - MoneyPerUseAPP;
                            drv.expdateapp = newexpdate;
                            drv.paymentapphistory.push({
                                previousmoney: previousmoney,
                                currentmoney: currentmoney,
                                moneypayment: MoneyPerUseAPP,
                                oldexpdate: oldexpdate,
                                newexpdate: new Date(newexpdate),
                                actiondate: new Date()
                            });
                            drv.save(function (err, response) {
                                if (err) {
                                    res.json({ status: false, msg: "error", data: err });
                                } else {
                                    res.json({
                                        status: true,
                                        msg: "สามารถใช้งานระบบได้",
                                        data: {
                                            id: response._id,
                                            device_id: response.device_id,
                                            moneyapp: response.moneyapp,
                                            expdateapp: response.expdateapp
                                        }
                                    });
                                }
                            });
                        } else {
                            /*
                            res.json({ 
                                status: true , 
                                msg: "คนขับ ยังมีเวลาเพียงพอในการใช้งานระบบ",
                                data: {
                                    id : drv._id,
                                    device_id : drv.device_id,
                                    moneyapp : drv.moneyapp,
                                    expdateapp: drv.expdateapp
                                } 
                            });
                            */
                            newexpdate = drvexpdategrg + (TimePerUseAPP * TimePerHour);
                            drv.moneyapp = drv.moneyapp - MoneyPerUseAPP;
                            drv.expdateapp = newexpdate;
                            drv.paymentapphistory.push({
                                previousmoney: previousmoney,
                                currentmoney: currentmoney,
                                moneypayment: MoneyPerUseAPP,
                                oldexpdate: oldexpdate,
                                newexpdate: new Date(newexpdate),
                                addeddate: new Date()
                            });
                            drv.save(function (err, response) {
                                if (err) {
                                    res.json({ status: false, msg: "error", data: err });
                                } else {
                                    res.json({
                                        status: true,
                                        msg: "002",
                                        data: {
                                            id: response._id,
                                            device_id: response.device_id,
                                            moneyapp: response.moneyapp,
                                            expdateapp: response.expdateapp
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }
        );
    } else {
        res.json({
            status: false,
            msg: "Driver does not want to pay and use callcenter service"
        });
    }
};




// --------- start shared API
exports.announcementAdd = function (req, res) {
    username = req.user.local.username;
    password = req.body.password;
    encpassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

    UserModel.findOne(
        { username: username },
        function (err, user) {
            //console.log (' real = ' + user.local.password)
            //console.log( ' encryp = ' + encpassword)
            //console.log(bcrypt.compareSync(encpassword, user.local.password));

            if (user == null) {
                res.json({
                    status: false,
                    msg: "Your account is not valid, please try again."
                });
            } else {
                AnnounceModel.create({
                    createdby: req.user.local.username,
                    anntype: req.body.anntype,
                    topic: req.body.topic,
                    detail: req.body.detail,
                    status: req.body.status,
                    expired: new Date(req.body.expired).getTime() - 25200000,
                    updated: new Date().getTime(),
                    created: new Date().getTime()
                }, function (err, response) {
                    if (err) {
                        res.send(err)
                    } else {
                        err ? res.send(err) : res.json({
                            status: true,
                            msg: "The announcement has been created.",
                            data: response
                        });
                    }
                });
            }
        }
    );
};




exports.announcementEdit = function (req, res) {
    user_id = req.body.user_id;
    ann_id = req.body.ann_id;
    UserModel.findOne(
        { _id: user_id },
        function (err, user) {
            if (user == null) {
                res.json({
                    status: false,
                    msg: "Your account is not valid, please try again."
                });
            } else {
                AnnounceModel.findOne(
                    { _id: ann_id },
                    { status: 1, updated: 1 },
                    function (err, announce) {
                        if (req.body.anntype) { announce.anntype = req.body.anntype ? req.body.anntype : announce.anntype; }
                        if (req.body.topic) { announce.topic = req.body.topic ? req.body.topic : announce.topic; }
                        if (req.body.detail) { announce.detail = req.body.detail ? req.body.detail : announce.detail; }
                        if (req.body.status) { announce.status = req.body.status ? req.body.status : announce.status; }
                        if (req.body.expired) { announce.expired = req.body.expired ? req.body.expired : announce.expired; }
                        announce.updated = new Date().getTime();
                        announce.save(function (err, response) {
                            if (err) {
                                res.json({ status: false, msg: "error" });
                            } else {
                                res.json({
                                    status: true,
                                    msg: "success, announcement has been updated",
                                    data: {
                                        ann_id: response._id,
                                        anntype: response.anntype,
                                        topic: response.topic,
                                        detail: response.detail,
                                        status: response.status,
                                        expired: response.expired,
                                        created: response.created
                                    }
                                });
                            }
                        });
                    }
                );
            }
        }
    );
};




exports.announcementDel = function (req, res) {
    user_id = req.body.user_id;
    ann_id = req.body.ann_id;
    UserModel.findOne(
        { _id: user_id },
        function (err, user) {
            if (user == null) {
                res.json({
                    status: false,
                    msg: "Your account is not valid, please try again."
                });
            } else {
                AnnounceModel.findOne(
                    { _id: ann_id },
                    function (err, announce) {
                        if (announce == null) {
                            res.json({
                                status: false,
                                msg: "Your announce id is not valid, please try again."
                            });
                        } else {
                            announce.remove({ _id: ann_id },
                                function (err, response) {
                                    console.log(response)
                                    if (err) {
                                        res.json({ status: false, msg: "error" });
                                    } else {
                                        res.json({
                                            status: true,
                                            msg: "success, the announcement has been deleted"
                                        });
                                    }
                                }
                            );
                            res.json({
                                status: true,
                                msg: "success, the announcement has been deleted"
                            });
                        }
                    }
                );
            }
        }
    );
};




exports.announcementGet = function (req, res) {
    var lang = req.body.lang;
    var anntype = req.body.anntype;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var platform = req.body.platform;
    var appversion = req.body.version;
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

    if (typeof curlng == 'undefined' && curlng == null) {
        curlng = 0;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        curlat = 0;
    }
    var curloc = [parseFloat(curlng), parseFloat(curlat)];

    if (anntype == "PSG") {
        if (req.url.substring(1, 7) == 'smiles') {
            anntype = 'PSG_SMILES';
            AnnounceModel.findOne(
                {
                    polygons:
                    {
                        $geoIntersects:
                        {
                            $geometry:
                            {
                                "type": "Point",
                                "coordinates": curloc
                            }
                        }
                    },
                    anntype: anntype,
                    topic: { $ne: '' }, status: "Y",
                    expired: { $gt: new Date().getTime() }
                },
                { _id: 0, username: 0, ProvinceID: 0, NameEN: 0, NameTH: 0, ZoneID: 0, Lat: 0, Long: 0, polygons: 0 },
                { sort: '-created' },
                //{sort:{'created':-1}},
                function (err, response) {
                    if (response == null) {                        
                        AnnounceModel.findOne(
                            {
                                anntype: anntype,
                                topic: { $ne: '' }, status: "Y",
                                expired: { $gt: new Date().getTime() },
                                polygons: { $exists: false }
                            },
                            { _id: 0, username: 0, ProvinceID: 0, NameEN: 0, NameTH: 0, ZoneID: 0, Lat: 0, Long: 0, polygons: 0 },
                            { sort: '-created' },
                            //{sort:{'created':-1}},
                            function (err, result) {
                                if (result == null) {
                                    res.json({ status: false, msg: "No data for announcement." });
                                } else {
                                    if (lang) {
                                        err ? res.send(err) : res.json({
                                            status: true,
                                            //data :  result
                                            data: {
                                                anntype: anntype,
                                                topic: result._doc["topic" + (req.body.lang ? req.body.lang : "en")] || result._doc["topicen"],
                                                detail: result._doc["detail" + (req.body.lang ? req.body.lang : "en")] || result._doc["detailen"]
                                            }
                                        });
                                    } else {
                                        err ? res.send(err) : res.json({
                                            status: true,
                                            //data :  result
                                            data: {
                                                anntype: anntype,
                                                topic: result._doc["topicth"],
                                                detail: result._doc["detailth"]
                                            }
                                        });
                                    }
                                }
                            }
                        );
                    } else {
                        err ? res.send(err) : res.json({
                            status: true,
                            data: response
                        });
                    }
                }
            );
        } else {
            if (platform == undefined) {
                res.json({
                    status: true,
                    data: {
                        detail_new: "",
                        topicEn: "",
                        detailEn: "",
                        topic: "ยินดีต้อนรับสู่ Taxi-Beam",
                        detail: "Taxi-Beam Lite ได้ทำการเปลี่ยนชื่อเป็น Taxi-Beam แล้ว สามารถ update version ใหม่ได้จาก <a href='http://itunes.apple.com/th/app/Taxi-Beam Lite/id1081314886'>AppStore สำหรับ iOS</a> และ <a href='https://play.google.com/store/apps/details?id=com.ecartstudio.taxibeam.passenger.lite'>PlayStore สำหรับ Android</a>",
                        status: "Y"
                    }
                });
            } else {
                if (platform == "ANDROID" && compareVersions(appversion, '2.0.32') < 0) {
                    res.json({
                        status: true,
                        data: {
                            detail_new: "",
                            topicEn: "",
                            detailEn: "",
                            topic: "ยินดีต้อนรับสู่ Taxi-Beam",
                            detail: "ขึ้น Taxi-Beam วันนี้ ลุ้นรับ GalaxyS8 สำหรับท่านที่ต้องการเข้าร่วมกิจกรรม กรุณา update version ใหม่ได้ที่ <a href='https://play.google.com/store/apps/details?id=com.ecartstudio.taxibeam.passenger.lite'>GooglePlay</a><br><br><a href='https://www.facebook.com/events/165947250641092/?ti=cl'>ดูรายละเอียด</a>",
                            status: "Y"
                        }
                    });
                } else {
                    if (platform == "iOS" && compareVersions(appversion, '1.0.39') < 0) {
                        res.json({
                            status: true,
                            data: {
                                detail_new: "",
                                topicEn: "",
                                detailEn: "",
                                topic: "ยินดีต้อนรับสู่ Taxi-Beam",
                                detail: "ขึ้น Taxi-Beam วันนี้ ลุ้นรับ GalaxyS8 สำหรับท่านที่ต้องการเข้าร่วมกิจกรรม กรุณา update version ใหม่ได้ที่  <a href='http://itunes.apple.com/th/app/Taxi-Beam Lite/id1081314886'>AppStore</a><br><br><a href='https://www.facebook.com/events/165947250641092/?ti=cl'>ดูรายละเอียด</a>",
                                status: "Y"
                            }
                        });
                    } else {
                        AnnounceModel.findOne(
                            {
                                polygons:
                                {
                                    $geoIntersects:
                                    {
                                        $geometry:
                                        {
                                            "type": "Point",
                                            "coordinates": curloc
                                        }
                                    }
                                },
                                anntype: anntype,
                                topic: { $ne: '' }, status: "Y",
                                expired: { $gt: new Date().getTime() }
                            },
                            { _id: 0, username: 0, ProvinceID: 0, NameEN: 0, NameTH: 0, ZoneID: 0, Lat: 0, Long: 0, polygons: 0 },
                            { sort: '-created' },
                            //{sort:{'created':-1}},
                            function (err, response) {
                                if (response == null) {                        
                                    AnnounceModel.findOne(
                                        {
                                            anntype: anntype,
                                            topic: { $ne: '' }, status: "Y",
                                            expired: { $gt: new Date().getTime() },
                                            polygons: { $exists: false }
                                        },
                                        { _id: 0, username: 0, ProvinceID: 0, NameEN: 0, NameTH: 0, ZoneID: 0, Lat: 0, Long: 0, polygons: 0 },
                                        { sort: '-created' },
                                        //{sort:{'created':-1}},
                                        function (err, result) {
                                            if (result == null) {
                                                res.json({ status: false, msg: "No data for announcement." });
                                            } else {
                                                if (lang) {
                                                    err ? res.send(err) : res.json({
                                                        status: true,
                                                        //data :  result
                                                        data: {
                                                            anntype: anntype,
                                                            topic: result._doc["topic" + (req.body.lang ? req.body.lang : "en")] || result._doc["topicen"],
                                                            detail: result._doc["detail" + (req.body.lang ? req.body.lang : "en")] || result._doc["detailen"]
                                                        }
                                                    });
                                                } else {
                                                    err ? res.send(err) : res.json({
                                                        status: true,
                                                        //data :  result
                                                        data: {
                                                            anntype: anntype,
                                                            topic: result._doc["topicth"],
                                                            detail: result._doc["detailth"]
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    );
                                } else {
                                    err ? res.send(err) : res.json({
                                        status: true,
                                        data: response
                                    });
                                }
                            }
                        );
                    }
                }
            }
        }
    } else {
        AnnounceModel.findOne(
            {
                polygons:
                {
                    $geoIntersects:
                    {
                        $geometry:
                        {
                            "type": "Point",
                            "coordinates": curloc
                        }
                    }
                },
                anntype: anntype,
                topic: { $ne: '' }, status: "Y",
                expired: { $gt: new Date().getTime() }
            },
            { _id: 0, username: 0, ProvinceID: 0, NameEN: 0, NameTH: 0, ZoneID: 0, Lat: 0, Long: 0, polygons: 0 },
            { sort: '-created' },
            //{sort:{'created':-1}},
            function (err, response) {
                if (response == null) {
                    AnnounceModel.findOne(
                        {
                            anntype: anntype,
                            topic: { $ne: '' }, status: "Y",
                            expired: { $gt: new Date().getTime() },
                            polygons: { $exists: false }
                        },
                        { _id: 0, username: 0, ProvinceID: 0, NameEN: 0, NameTH: 0, ZoneID: 0, Lat: 0, Long: 0, polygons: 0 },
                        { sort: '-created' },
                        //{sort:{'created':-1}},
                        function (err, result) {
                            if (result == null) {
                                res.json({ status: false, msg: "No data for announcement." });
                            } else {
                                if (lang) {
                                    err ? res.send(err) : res.json({
                                        status: true,
                                        //data :  result
                                        data: {
                                            anntype: anntype,
                                            topic: result._doc["topic" + (req.body.lang ? req.body.lang : "en")] || result._doc["topicen"],
                                            detail: result._doc["detail" + (req.body.lang ? req.body.lang : "en")] || result._doc["detailen"]
                                        }
                                    });
                                } else {
                                    err ? res.send(err) : res.json({
                                        status: true,
                                        //data :  result
                                        data: {
                                            anntype: anntype,
                                            topic: result._doc["topicth"],
                                            detail: result._doc["detailth"]
                                        }
                                    });
                                }
                            }
                        }
                    );
                } else {
                    err ? res.send(err) : res.json({
                        status: true,
                        data: response
                    });
                }
            }
        );
    }
};




exports.callcenterAnnounceAll = function (req, res) {
    var announcement = CallcenterannounceModel.find({});
    announcement.limit(20);
    announcement.sort({ created: -1 });
    announcement.exec(function (err, announcements) {
        res.send({
            status: true,
            data: announcements
        });
    });
};




exports.callcenterAnnounceFind = function (req, res) {
    CallcenterannounceModel.findOne({})
};




exports.callcenterAnnounceAdd = function (req, res) {
    username = req.user.local.username;
    UserModel.findOne({ username: username }, function (err, user) {

        if (user == null) {
            res.json({
                status: false,
                msg: "Your account is not valid, please try again."
            });
        }
        else {
            CallcenterannounceModel.create({

                topic: req.body.topic,
                detail: req.body.detail,
                createdby: req.user.local.username,

                updated: new Date().getTime(),
                created: new Date().getTime()
            }, function (err, response) {
                if (err) {
                    res.send(err);
                }
                else {
                    err ? res.send(err) : res.json({
                        status: true,
                        msg: "The announcement has been created.",
                        data: response
                    });
                }
            });
        }
    });
};




exports.callcenterAnnounceEdit = function (req, res) {
    user_id = req.body.user_id;
    ann_id = req.body.ann_id;

    UserModel.findOne(
        { _id: user_id },
        function (err, user) {
            if (user == null) {
                res.json({
                    status: false,
                    msg: "Your account is not valid, please try again."
                });
            } else {
                CallcenterannounceModel.findOne(
                    { _id: ann_id },
                    { status: 1, updated: 1 },
                    function (err, announce) {
                        if (req.body.topic) { announce.topic = req.body.topic ? req.body.topic : announce.topic; }
                        if (req.body.detail) { announce.detail = req.body.detail ? req.body.detail : announce.detail; }
                        announce.updated = new Date().getTime();
                        announce.save(function (err, response) {
                            if (err) {
                                res.json({ status: false, msg: "error" });
                            } else {
                                res.json({
                                    status: true,
                                    msg: "success, announcement has been updated",
                                    data: {
                                        ann_id: response._id,
                                        topic: response.topic,
                                        detail: response.detail
                                    }
                                });
                            }
                        });
                    }
                );
            }
        }
    );
};




exports.callcenterAnnounceDel = function (req, res) {
    user_id = req.body.user_id;
    ann_id = req.body.ann_id;

    UserModel.findOne(
        { _id: user_id },
        function (err, user) {
            if (user == null) {
                res.json({
                    status: false,
                    msg: "Your account is not valid, please try again."
                });
            } else {
                CallcenterannounceModel.findOne(
                    { _id: ann_id },
                    function (err, announce) {
                        if (announce == null) {
                            res.json({
                                status: false,
                                msg: "Your announce id is not valid, please try again."
                            });
                        } else {
                            announce.remove({ _id: ann_id },
                                function (err, response) {
                                    //console.log(response)
                                    if (err) {
                                        res.json({ status: false, msg: "error" });
                                    } else {
                                        res.json({
                                            status: true,
                                            msg: "success, the announcement has been deleted"
                                        });
                                    }
                                });
                            res.json({
                                status: true,
                                msg: "success, the announcement has been deleted"
                            });
                        }
                    }
                );
            }
        }
    );
};




exports.addlocalpoi = function (req, res) {
    LocalpoiModel.findOne(
        { _id: req.body._id },
        function (err, localpoi) {
            if (localpoi == null) {
                LocalpoiModel.create({
                    name: req.body.name,
                    addr: req.body.addr,
                    provinceid: req.body.provinceid,
                    districtid: req.body.districtid,
                    subdistrictid: req.body.subdistrictid,
                    curlng: req.body.curlng,
                    curlat: req.body.curlat,
                    curloc: req.body.curloc,
                    status: "Y",
                    updated: new Date().getTime(),
                    created: new Date().getTime()
                },
                    function (err, response) {
                        if (err) {
                            res.json({ status: false, msg: "error" });
                        } else {
                            res.json({
                                status: true,
                                data: response
                            });
                        }
                    }
                );
            }
        }
    );
};




exports.getPOIandParking = function (req, res) {
    LocalpoiModel.find(
        //{ "name" : { "$exists" : true, "$ne" : "" }, "cgroup": req.user.local.cgroup }, 
        { "name": { "$exists": true, "$ne": "" } },
        { name: 1, curloc: 1, poitype: 1, iconimg: 1, status: 1 },
        function (err, localpoi) {
            if (localpoi == 0) {
                res.json({ status: false, msg: "no place" });
            } else {
                res.json({
                    status: true,
                    data: localpoi
                });
            }
        }
    );
}




exports.getAvgGetJobPerMinute = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var MinDiff = (endTime - startTime) / 1000 / 60;
    var myquery;
    CallcenterpsgModel.count({
        "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
        "status": "FINISHED",
        "cgroup": req.user.local.cgroup
    }, function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                Total: result,
                Avg: result / MinDiff
            });
        }
    })
}


// myquery = { $match: {
//         "created": { $gt: new  Date(startTime), $lt: new Date(endTime) } ,
//         "curaddr": { $ne: '' } ,
//         "status": "FINISHED",
//         "cgroup" : req.user.local.cgroup
//     }};

// CallcenterpsgModel.aggregate([
//      myquery ,
//      { $group: {_id: "$drv_carplate", count: {$sum : 1 }}},
//      { $sort: { count: -1 }},
//  ], function (err, return1) {
//     if(err) {
//         res.json({ status: false , data: err  });      
//     } else {
//          res.json({ 
//              status: true,
//              data: return1
//          });    
//      }
//  });


exports.MostHitStartPlace = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    //http://www.kdelemme.com/2014/03/19/how-to-aggregate-data-from-mongodb-with-node-js-and-mongoose/
    CallcenterpsgModel.aggregate([
        {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "curaddr": { $ne: '' },
                "cgroup": req.user.local.cgroup
            }
        },
        //{ $unwind: "$records" },  
        { $group: { _id: "$curaddr", count: { $sum: 1 } } },
        { $sort: { count: -1 } },
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });
}



exports.MostHitDestinationPlace = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    //http://www.kdelemme.com/2014/03/19/how-to-aggregate-data-from-mongodb-with-node-js-and-mongoose/
    CallcenterpsgModel.aggregate([
        {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "destination": { $ne: '' },
                "cgroup": req.user.local.cgroup
            }
        },
        //{ $unwind: "$records" },  
        { $group: { _id: "$destination", count: { $sum: 1 } } },
        { $sort: { count: -1 } },
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });
}



exports.MostHitHours = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);

    CallcenterpsgModel.aggregate([
        {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "cgroup": req.user.local.cgroup
            }
        },
        {
            "$project": {
                "y": { "$year": "$created" },
                "m": { "$month": "$created" },
                "d": { "$dayOfMonth": "$created" },
                "h": { "$hour": "$created" },
                "device_id": 1
            }
        },
        {
            "$group": {
                "_id": { "year": "$y", "month": "$m", "day": "$d", "hour": "$h" },
                "total": { "$sum": 1 }
            }
        },
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });
}




exports.CountJobPerDrv = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var drv_id = req.body._id;
    var drv_carplate = req.body.drv_carplate;
    var myquery;

    // if(drv_id){
    //     myquery = { $match: {
    //             "created": { $gt: new  Date(startTime), $lt: new Date(endTime) } ,
    //             "curaddr": { $ne: '' } ,
    //             "status": "FINISHED" ,
    //             "drv_id" : drv_id
    //         }};
    // } else {
    //     myquery = { $match: {
    //             "created": { $gt: new  Date(startTime), $lt: new Date(endTime) } ,
    //             "curaddr": { $ne: '' } ,
    //             "status": "FINISHED" 
    //         }};
    // }

    if (drv_carplate) {
        myquery = {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "curaddr": { $ne: '' },
                "status": { $in: ["FINISHED", "OFF"] },
                "drv_carplate": drv_carplate,
                "cgroup": req.user.local.cgroup
            }
        };
    } else {
        myquery = {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "curaddr": { $ne: '' },
                "status": { $in: ["FINISHED", "OFF"] },
                "cgroup": req.user.local.cgroup
            }
        };
    }
    //console.log( myquery)
    CallcenterpsgModel.aggregate([
        myquery,
        {
            "$group": {
                "_id": "$drv_carplate",
                "count": { "$sum": 1 }
            },

        },
        { $sort: { count: -1 } },
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });

}



exports.CountAppJobPerDrv = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var drv_id = req.body._id;
    var drv_carplate = req.body.drv_carplate;
    var myquery;

    if (drv_carplate) {
        myquery = {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "curaddr": { $ne: '' },
                "status": { $in: ["FINISHED", "OFF"] },
                "drv_carplate": drv_carplate,
                "cgroup": req.user.local.cgroup
            }
        };
    } else {
        myquery = {
            $match: {
                "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
                "curaddr": { $ne: '' },
                "status": { $in: ["FINISHED", "OFF"] },
                "cgroup": req.user.local.cgroup
            }
        };
    }
    //console.log( myquery)
    JoblistModel.aggregate([
        myquery,
        {
            "$group": {
                "_id": "$drv_carplate",
                "count": { "$sum": 1 }
            },

        },
        { $sort: { count: -1 } },
    ], function (err, result) {
        if (err) {
            res.json({ status: false, data: err });
        } else {
            res.json({
                status: true,
                data: result
            });
        }
    });

}




exports.searchFinishList = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var keyword = req.body.keyword;

    CallcenterpsgModel
        .find({
            $or: [{ curaddr: new RegExp(keyword) }, { destination: new RegExp(keyword) }, { phone: new RegExp(keyword) }, { drv_carplate: new RegExp(keyword) }, { status: new RegExp(keyword) }],
            created: { $gt: new Date(startTime), $lt: new Date(endTime) },
            cgroup: req.user.local.cgroup
        })
        .select("autophone phone curaddr destination curloc desloc detail status jobtype createdjob drv_id drv_carplate createdvia ccstation assigningby cgroup cccomment")
        .sort({ createdjob: -1 })
        .exec(function (err, result) {
            if (err || !result) {
                res.json({ status: false, msg: 'No data' });
                return;
            }

            res.json({ status: true, data: result });
        });
};




exports.getjoblistbydate = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    CallcenterpsgModel.find(
        {
            "created": { $gt: new Date(startTime), $lt: new Date(endTime) },
            //"status": { $in: [ "FINISHED","OFF" ] }, 
            "cgroup": req.user.local.cgroup
        },
        { _id: 1, status: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, deslng: 1, deslat: 1, desloc: 1, detail: 1, createdjob: 1, jobtype: 1, drv_id: 1, drv_carplate: 1, createdvia: 1, dassignedjob: 1, dpendingjob: 1, ccstation: 1, assigningby: 1, cgroup: 1, cccomment: 1 },
        { sort: { 'createdjob': -1 } },
        function (err, psglist) {
            if (psglist == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: psglist
                });
            }
        }
    );
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////                          ADMIN REPORT                    /////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ใช้กับตารางการโทรหาแท็กซี่
exports.PsgCallLog = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js

    if (typeof req.body.cgroup == 'undefined' || req.body.cgroup == null || req.body.cgroup == "") {
        condition = { "created": { $gt: new Date(startTime), $lt: new Date(endTime) } };
    } else {
        condition = { "created": { $gt: new Date(startTime), $lt: new Date(endTime) }, cgroup: { $in: req.body.cgroup } };
    }

    PsgcalllogModel.find(
        condition,
        { _id: 0, psg_phone: 1, drv_phone: 1, drv_carplate: 1, callby: 1, created: 1, cgroup: 1 },
        { sort: { 'created': -1 } },
        function (err, response) {
            if (response == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: response
                });
            }
        }
    );
}




// ใช้กับตาราง joblist ตัวใหญ่
exports.JoblistLog = function (req, res) {
    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);

    if (typeof req.body.cgroup == 'undefined' || req.body.cgroup == null || req.body.cgroup == "") {
        condition = { "created": { $gt: new Date(startTime), $lt: new Date(endTime) } };
    } else {
        condition = { "created": { $gt: new Date(startTime), $lt: new Date(endTime) }, cgroup: { $in: req.body.cgroup } };
    }

    // sort mongoose ; http://stackoverflow.com/questions/5825520/in-mongoose-how-do-i-sort-by-date-node-js
    JoblistModel.find(
        condition,
        { _id: 0, drvcancelwhere: 1, datedrvcancel: 1, psgcancelwhere: 1, datepsgcancel: 1, datepsgthanks: 1, datedrvdrop: 1, datedrvpick: 1, datepsgaccept: 1, datedrvwait: 1, datepsgcall: 1, destination: 1, curaddr: 1, drv_carplate: 1, drv_phone: 1, drv_name: 1, psg_phone: 1 },
        { sort: { 'created': -1 } },
        function (err, response) {
            if (response == 0) {
                res.json({
                    status: false
                });
            } else {
                res.json({
                    status: true,
                    data: response
                });
            }
        }
    );
}




//  สำหรับ งานผ่าน app ทั้งหมด + condition
exports.getJobListLogReport = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var crange = req.body.crange; // [ $created ,
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    date: { $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$created", -25200000] } } },
                    day: { $dayOfWeek: { $subtract: ["$created", -25200000] } }
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// งานผ่าน app ทั้งหมด
exports.getJobListLogAll = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    date: { $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$created", -25200000] } } },
                    day: { $dayOfWeek: { $subtract: ["$created", -25200000] } }
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// แท็กซี่รับงาน
exports.getJobListLogWait = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$datedrvwait", -25200000] } },
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// ผู้โดยสารตอบรับ
exports.getJobListLogPick = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$datedrvpick", -25200000] } },
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// แท็กซี่รับผู้โดยสาร
exports.getJobListLogAccept = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$datepsgaccept", -25200000] } },
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// แท็กซี่ส่งผู้โดยสาร
exports.getJobListLogDrop = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$datedrvdrop", -25200000] } },
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// ผู้โดยสารยกเลิก
exports.getJobListLogPSGCancel = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([

        // conditions
        {
            $match: conditions
        },

        // grouping
        {
            $group:
            {
                _id:
                {
                    $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$datepsgcancel", -25200000] } },
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




// แท็กซี่ยกเลิก
exports.getJobListLogDRVCancel = function (req, res) {

    var startTime = parseInt(req.body.startTime);
    var endTime = parseInt(req.body.endTime);
    var cgroup = req.body.cgroup;

    var conditions = {
        created: { $gt: new Date(startTime), $lt: new Date(endTime) },
    }

    if (typeof cgroup !== 'undefined' && cgroup instanceof Array && cgroup.length > 0) {
        conditions.cgroup = { $in: cgroup }
    }

    JoblistModel.aggregate([
        // conditions
        {
            $match: conditions
        },
        // grouping
        {
            $group:
            {
                _id:
                {
                    $dateToString: { format: "%Y-%m-%d", date: { $subtract: ["$datedrvcancel", -25200000] } },
                },
                count: {
                    $sum: 1
                }
            }
        },

        // sorting
        {
            $sort: { _id: 1 }
        }
    ],

        // callback
        function (err, result) {
            if (err) {
                res.json({ status: false, data: err });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




exports.getAllGarage = function (req, res) {
    var ProvinceID = parseInt(req.body.ProvinceID);

    if (ProvinceID) {
        condition = { "ProvinceID": ProvinceID };
    } else {
        condition = {};
    }

    Lk_garageModel.find(
        condition,
        { _id: 1, cgroup: 1, name: 1, phone: 1, cgroupname: 1, cprovincename: 1, ProvinceID: 1 },
        { sort: { ProvinceID: 1 } },
        function (err, result) {
            if (result == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    data: result
                });
            }
        }
    );
};




exports.FindDrvinRadian = function (req, res) {
    var searchpsgradian = req.body.searchpsgradian;
    var searchpsgamount = req.body.searchpsgamount;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var favcartype = req.body.favcartype;
    var radian = req.body.radian;
    var tips = req.body.tips;

    if (typeof curlng == 'undefined' && curlng == null) {
        res.json({ status: false, msg: "current longitude is not valid" })
        return;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        res.json({ status: false, msg: "current latitude is not valid" })
        return;
    }
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

    DriversModel.aggregate(
        [
            {
                $geoNear: {
                    query: {
                        cgroup: req.user.local.cgroup,
                        status: "ON",
                        active: "Y",
                        favcartype: { $in: [[], req.body.cartype] },
                        cgroup: req.user.local.cgroup
                    },
                    near: { type: "Point", coordinates: curloc },
                    distanceField: "dist",
                    maxDistance: parseInt(searchpsgradian),
                    num: parseInt(searchpsgamount),
                    spherical: true
                }
            },
            {
                $project: {
                    _id: 1,
                    fname: 1, lname: 1, phone: 1, prefixcarplate: 1, carplate: 1, english: 1, cartype: 1, carcolor: 1, outbound: 1, carryon: 1, imgface: 1, imgcar: 1, imglicence: 1, curlat: 1, curlng: 1, curloc: 1, status: 1, allowpsgcontact: 1,
                    dist: 1,
                    dist_km: {
                        $divide: [
                            {
                                $subtract: [
                                    { $multiply: ['$dist', 1] },
                                    { $mod: [{ $multiply: ['$dist', 1] }, 10] }
                                ]
                            }, 1000]
                    },
                },
            },
            { $sort: { dist: 1 } }
        ],
        function (err, drvlist) {
            res.json({
                status: true,
                data: drvlist
            });
        }
    );
}



exports.searchdrvall = function (req, res) {
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var status = req.body.status;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
    var radian = req.body.radian;
    var amount = req.body.amount;
    // short form : if(req.body.uid) { device.uid = req.body.uid ? req.body.uid : device.uid; }     
    if (typeof curlng == 'undefined' && curlng == null) {
        res.json({ status: false, msg: "current longitude is not valid" })
        return;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        res.json({ status: false, msg: "current latitude is not valid" })
        return;
    }
    if (typeof radian == 'undefined' && radian == null) {
        radian = 2000000;
    }
    if (typeof amount == 'undefined' && amount == null) {
        amount = config.ccsearchdrvamount;
    }
    if (typeof status == 'undefined' && status == null) {
        status = ["ON", "HOLD", "OFF", "WAIT", "BUSY", "PICK", "BROKEN", "ASSIGNED", "DPENDING", "DEPENDING_REJECT", "DEPENDING_TIMEOUT"];
    } else {
        status = [status];
    }

    if (typeof req.body.cgroup == 'undefined' || req.body.cgroup == null || req.body.cgroup == "") {
        condition = { active: "Y", curloc: { $near: { $geometry: { type: "Point", coordinates: curloc }, $maxDistance: radian } }, status: { $in: status } };
    } else {
        condition = { active: "Y", curloc: { $near: { $geometry: { type: "Point", coordinates: curloc }, $maxDistance: radian } }, status: { $in: status }, cgroup: { $in: req.body.cgroup } };
    }

    DriversModel.find(
        condition,
        { _id: 1, device_id: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, carplate: 1, cartype: 1, carcolor: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, car_no: 1, psg_id: 1, active: 1, jobtype: 1, status: 1, moneyapp: 1, moneygarage: 1, expdateapp: 1, expdategarage: 1, brokenname: 1, brokendetail: 1, cgroup: 1 },
        { limit: amount },
        function (err, drvlist) {
            // Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!                             
            if (drvlist == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    data: drvlist
                });
                //Specifies a point for which a geospatial query returns the documents from nearest to farthest.
            }
        }
    );
};




exports.driverSearchPassengerDrv = function (req, res) {
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var radian = req.body.radian;
    var amount = req.body.amount;
    var curlng = req.body.curlng;
    var curlat = req.body.curlat;
    var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
    if (typeof curlng == 'undefined' && curlng == null) {
        res.json({ status: false, msg: "current longitude is not valid" })
        return;
    }
    if (typeof curlat == 'undefined' && curlat == null) {
        res.json({ status: false, msg: "current latitude is not valid" })
        return;
    }
    if (typeof radian == 'undefined' && radian == null) {
        radian = config.drvsearchpsgradian;
    }
    if (typeof amount == 'undefined' && amount == null) {
        amount = config.drvsearchpsgamount;
    }

    JoblisthotelModel.find(
        {
            curloc: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: curloc
                    },
                    $maxDistance: radian
                }
            }
            , status: "ON"
        },
        { hotel_phone: 1, psg_phone: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1 },
        { limit: amount },
        function (err, hotellist) {
            if (hotellist == 0) {
                PassengerModel.find(
                    {
                        curloc: {
                            $near: {
                                $geometry: {
                                    type: "Point",
                                    coordinates: curloc
                                },
                                $maxDistance: radian
                                //$minDistance: 1 
                            }
                        }
                        , status: "ON"
                    },
                    { phone: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1 },
                    { limit: amount },
                    function (err, psglist) {
                        if (psglist == 0) {
                            res.json({
                                status: false,
                                msg: "No data"
                            });
                        } else {
                            res.json({
                                status: true,
                                mgs: "data passenger joblist",
                                data: psglist,
                                datahotel: hotellist
                            });
                        }
                    }
                );
            } else {
                PassengerModel.find(
                    {
                        curloc: {
                            $near: {
                                $geometry: {
                                    type: "Point",
                                    coordinates: curloc
                                },
                                $maxDistance: radian
                                //$minDistance: 1 
                            }
                        }
                        , status: "ON"
                    },
                    { phone: 1, curaddr: 1, curlat: 1, curlng: 1, curloc: 1, destination: 1, tips: 1, detail: 1 },
                    { limit: amount },
                    function (err, psglist) {
                        if (psglist == 0) {
                            res.json({
                                //msg: "This is passenger list", 
                                status: true,
                                mgs: "data : hotel joblist",
                                data: psglist,
                                datahotel: hotellist
                            });
                        } else {
                            res.json({
                                //msg: "This is passenger list", 
                                status: true,
                                mgs: "data hotel and passenger joblist",
                                data: psglist,
                                datahotel: hotellist
                            });
                        }
                    }
                );
            }
        }
    );
};





exports.getdriverdlist = function (req, res) {
    DriversModel.find(
        { cgroup: "nmataxidd", status: { $ne: "DELETED" } },
        {
            _id: 1, device_id: 1, username: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, prefixcarplate: 1, carplate: 1, cartype: 1, carcolor: 1, address: 1, tambon: 1, district: 1, province: 1, zipcode: 1,
            imgcar: 1, imgface: 1, imglicence: 1, curlng: 1, curlat: 1, curloc: 1, car_no: 1, psg_id: 1, active: 1, jobtype: 1, citizenid: 1,
            status: 1, moneyapp: 1, moneygarage: 1, expdateapp: 1, expdategarage: 1, brokenname: 1, brokendetail: 1, cgroup: 1, created: 1
        },
        { sort: { "created": 1 } },
        function (err, drvlist) {
            if (drvlist == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    data: drvlist
                });
            }
        }
    );
};




exports.getmeterstatus = function (req, res) {
    console.log(' .................. GET METER STATUS ..................')
    var data = req.body.data;
    var cgroup = req.body.cgroup;
    var _id_garage = req.body._id_garage;
    var arrayLength = data.length;

    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        _getmeterstatusUpdate(obj.id, obj.lg, obj.lt, obj.st.substring(1, 2));
        if (i == data.length - 1) { res.json({ status: true }) }
    }

    function _getmeterstatusUpdate(device_id, curlng, curlat, status) {
        //if (status==1) { status = "ON" } else { status = "OFF" } 
        DriversModel.update(
            { device_id: device_id, _id_garage: _id_garage },
            {
                $set:
                {
                    status: (status == 1) ? "ON" : "OFF",
                    curlng: parseFloat(curlng),
                    curlat: parseFloat(curlat),
                    curloc: [parseFloat(curlng), parseFloat(curlat)],
                    updated: new Date().getTime()
                }
            },
            { new: true },
            function (err, tank) {
                if (err) return handleError(err);
                if (tank.nModified == 0) {
                    //console.log(false)
                } else {
                    //console.log(true)
                }
            }
        )
    }
};




//------------------ SERVICE FOR CC ADMIN ------------------//
exports.getdriverlist = function (req, res) {
    DriversModel.find(
        { cgroup: req.user.local.cgroup, status: { $nin: ['DELETED', 'PENDING'] } },
        {
            _id: 1, device_id: 1, username: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, prefixcarplate: 1, carplate: 1, cartype: 1, carcolor: 1, address: 1, tambon: 1, district: 1, province: 1, zipcode: 1,
            imgcar: 1, imgface: 1, imglicence: 1, curlng: 1, curlat: 1, curloc: 1, car_no: 1, psg_id: 1, active: 1, jobtype: 1, citizenid: 1,
            status: 1, moneyapp: 1, moneygarage: 1, expdateapp: 1, expdategarage: 1, brokenname: 1, brokendetail: 1, cgroup: 1, created: 1, drv_wallet:1, drv_shift:1, drv_point: 1, shift_start: 1, shift_end: 1
        },
        { sort: { 'created': 1 } },
        function (err, drvlist) {
            if (drvlist == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    data: drvlist
                });
            }
        }
    );
};



exports.getdriverpendinglist = function (req, res) {
    DriversModel.find(
        { cgroup: req.user.local.cgroup, status: 'PENDING' },
        {
            _id: 1, device_id: 1, username: 1, fname: 1, lname: 1, phone: 1, taxiID: 1, carplate: 1, carplate_formal: 1, prefixcarplate: 1,
            cartype: 1, carcolor: 1, address: 1, tambon: 1, district: 1, province: 1, zipcode: 1,
            imgcar: 1, imgface: 1, imglicence: 1, curlng: 1, curlat: 1, curloc: 1, car_no: 1, psg_id: 1, active: 1, jobtype: 1, citizenid: 1,
            status: 1, moneyapp: 1, moneygarage: 1, expdateapp: 1, expdategarage: 1, brokenname: 1, brokendetail: 1, cgroup: 1, created: 1
        },
        { sort: { 'created': 1 } },
        function (err, drvlist) {
            if (drvlist == 0) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    data: drvlist
                });
            }
        }
    );
};



exports.getdriverdetail = function (req, res) {
    DriversModel.findOne(
        { _id: req.body._id, cgroup: req.user.local.cgroup },
        function (err, driver) {
            if (driver == null) {
                res.json({ status: false, msg: "No data" });
            } else {
                res.json({
                    status: true,
                    driver: driver
                });
            }
        }
    );
};

exports.drvregister = function (socket) {
    return function (req, res) {
        var prefixcarplate;
        if (typeof prefixcarplate == 'undefined' && prefixcarplate == null) {
            prefixcarplate = "";
        } else {
            prefixcarplate = req.body.prefixcarplate + ' ';
        }

        DriversModel.findOne({ $or: [{ username: req.body.username }, { citizenid: req.body.citizenid }] }, function (err, existDriver) {
            if (err) {
                removeUploadImg(req.files);
                res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
            } else {
                if (existDriver) {
                    removeUploadImg(req.files);
                    var erro_code = "CITIZENID_DUPLICATED";
                    var message = "Your current citizen id is already used.";
                    if (existDriver.username === req.body.username) { message = "Your current username is already used."; erro_code = "USERNAME_DUPLICATED" }
                    res.json({ status: false, code: erro_code, message: message });
                } else {
                    DriversModel.create({
                        device_id: ranSMS(),
                        _id_garage: req.user._id_garage,
                        cgroup: req.body.cgroup,
                        cgroupname: req.user.local.cgroupname,
                        cprovincename: req.user.local.cprovincename,
                        username: req.body.username.toLowerCase(),
                        password: crypto.createHmac('sha256', config.pwd_key).update(req.body.password).digest('base64'),
                        pwdhint: req.body.password,
                        fname: req.body.fname,
                        lname: req.body.lname,
                        gender: req.body.gender,
                        dob: req.body.dob,
                        emergencyphone: req.body.emergencyphone.replace(/-/, '').replace(/ /g, ''),
                        phone: req.body.phone.replace(/-/, '').replace(/ /g, ''),
                        allowpsgcontact: req.body.allowpsgcontact,
                        citizenid: req.body.citizenid.replace(/ /g, ''),
                        taxiID: req.body.taxiID,
                        address: req.body.address,
                        province: req.body.province,
                        district: req.body.district,
                        tambon: req.body.tambon,
                        zipcode: req.body.zipcode,
                        english: req.body.english,
                        carplate: req.body.carplate,
                        prefixcarplate: req.body.prefixcarplate,
                        carplate_formal: prefixcarplate + req.body.carplate,
                        carprovince: req.body.carprovince,
                        carprovinceid: req.body.carprovinceid,
                        cartype: req.body.cartype,
                        carcolor: req.body.carcolor,
                        outbound: req.body.outbound,
                        carryon: req.body.carryon,
                        active: "Y",
                        status: "APPROVED",
                        msgphone: "",
                        msgnote: "",
                        msgstatus: "",
                        psg_curaddr: "",
                        psg_destination: "",
                        psg_detail: "",
                        jobtype: "",
                        appversion: config.mobileappversion,
                        created: new Date().getTime(),
                        updated: new Date().getTime(),
                        expdategarage: new Date().getTime(),
                        expdateapp: new Date().getTime() + (7776000000 * 4),
                        smsconfirm: ranSMS(),
                        updateprofilehistory: {
                            register_data: req.body,
                            created: new Date()
                        }
                    },
                        function (err, newDriver) {
                            if (err) {
                                removeUploadImg(req.files);
                                res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
                            } else {

                                var id = newDriver._id;
                                var imgcar_path = '../upload_drivers/' + id + '_imgcar.png';
                                var imgface_path = '../upload_drivers/' + id + '_imgface.png';
                                var imglicence_path = '../upload_drivers/' + id + '_imglicence.png';

                                fs.move(req.files.imgcar.path, imgcar_path, function (err) {
                                    fs.move(req.files.imgface.path, imgface_path, function (err) {
                                        fs.move(req.files.imglicence.path, imglicence_path, function (err) {
                                            DriversModel.findOne(
                                                { _id: id },
                                                function (err, result) {
                                                    if (result == null) {
                                                        res.json({ status: true, driver: newDriver });
                                                    } else {
                                                        result.imgcar = imgcar_path.replace('../upload_drivers/', '');
                                                        result.imgface = imgface_path.replace('../upload_drivers/', '');
                                                        result.imglicence = imglicence_path.replace('../upload_drivers/', '');
                                                        result.save(function (err, response) {
                                                            res.json({ status: true, driver: response });

                                                            socket.of('/' + req.user.local.cgroup).emit('request approve', response);
                                                        });
                                                    }
                                                }
                                            );
                                        });
                                    });
                                });
                            }
                        });
                }
            }
        });
    };
};

exports.driverEdit = function (socket) {
    return function (req, res) {
        if (req.body.cgroup == 'bkksmileapp') {
            // chekck ว่าเป็น อู่ smileapp หรือปล่าว
            myquery = {
                citizenid: req.body.citizenid,
                _id: { $ne: req.body._id },
                _id_garage: '597b0e04dc04ea092db44a7c'
            };
        } else {
            myquery = {
                citizenid: req.body.citizenid,
                _id: { $ne: req.body._id },
                _id_garage: { $ne: '597b0e04dc04ea092db44a7c' }
            };
        }
        DriversModel.findOne(
            myquery,
            function (err, existDriver) {
                if (err) {
                    removeUploadImg(req.files);
                    res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
                } else {
                    if (existDriver) {
                        removeUploadImg(req.files);
                        var erro_code = "CITIZENID_DUPLICATED";
                        var message = "Your current citizen id is already used.";
                        if (existDriver.username === req.body.username) {
                            message = "Your current username is already used.";
                            erro_code = "USERNAME_DUPLICATED"
                        }
                        res.json({ status: false, code: erro_code, message: message });
                    } else {
                        DriversModel.findOne({ _id: req.body._id }, function (err, currentDriver) {
                            if (err) {
                                removeUploadImg(req.files);
                                res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
                            } else {

                                Object.keys(req.files).forEach(function (file) {
                                    currentDriver[file] = currentDriver._id + '_' + file + '.png';
                                });

                                var diffData = {};

                                Object.keys(req.body).forEach(function (field) {
                                    if (currentDriver[field] !== req.body[field] && field !== '_id' && field !== 'status') {
                                        diffData[field] = req.body[field];
                                    }
                                });

                                Object.keys(req.files).forEach(function (file) {
                                    diffData[file] = currentDriver._id + '_' + file + '.png';
                                });

                                Object.keys(req.body).forEach(function (field) {

                                    if (field === '_id') {
                                        return;
                                    }

                                    if (field === 'citizenid') {
                                        req.body.citizenid = req.body.citizenid.replace(/ /g, '');
                                    }

                                    if (field === 'phone') {
                                        req.body.phone = req.body.phone.replace(/-/, '').replace(/ /g, '');
                                    }

                                    if (field === 'status') {
                                        if (req.body.status === "false") {
                                            req.body.status = "PENDING";
                                        } else {
                                            if (currentDriver.status === "PENDING") {
                                                req.body.status = "APPROVED";
                                            } else {
                                                req.body.status = currentDriver.status;
                                            }
                                        }
                                    }

                                    if (currentDriver.status !== req.body.status) {
                                        diffData.status = req.body.status;
                                    }

                                    currentDriver[field] = req.body[field];

                                    if (field === 'prefixcarplate') {
                                        prefixcarplate = req.body.prefixcarplate + ' ';
                                    }

                                    if (field === 'carplate') {
                                        carplate = req.body.carplate;
                                    }

                                });

                                currentDriver.carplate_formal = prefixcarplate + carplate;
                                currentDriver.save(function (err, result) {
                                    if (err) {
                                        res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
                                    } else {

                                        DAkeeplogModel.create({
                                            logdata: diffData,
                                            logaction: "DRV_EDIT",
                                            cgroup: req.user.local.cgroup,
                                            username: req.user.username,
                                            _id_user: req.user._id
                                        }, function (err, result) {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                console.log("create new edit log success...");
                                            }
                                        });

                                        Object.keys(req.files).forEach(function (file) {
                                            var dest = '../upload_drivers/' + result._id + '_' + file + '.png';
                                            fs.move(req.files[file].path, dest, { clobber: true }, function (err) {
                                                if (err) fs.removeSync(req.files[file].path);
                                            });
                                        });

                                        res.json({ status: true, driver: result });
                                        socket.of('/' + req.user.local.cgroup).emit('request approve', result);
                                    }
                                });
                            }
                        });
                    }
                }
            });
    };
};

exports.driverRemove = function (req, res) {
    DriversModel.findOne({ _id: req.body.id }, function (err, currentDriver) {
        if (err) {
            res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
        } else {

            currentDriver.status = "DELETED";

            currentDriver.save(function (err, response) {
                // fs.removeSync('../upload_drivers/' + req.body.id + '_imgcar.png');
                // fs.removeSync('../upload_drivers/' + req.body.id + '_imgface.png');
                // fs.removeSync('../upload_drivers/' + req.body.id + '_imglicence.png');
                res.json({
                    status: true,
                    current_id: req.body.id,
                    message: "success, the driver has been deleted"
                });
            });
        }
    });
};

exports.driverToggleActive = function (req, res) {
    DriversModel.findOne({ _id: req.body.id }, function (err, driver) {
        driver.active = req.body.active;
        driver.save(function (err, result) {
            if (err) {
                res.json({ status: false, code: 'MODEL_ERROR', message: 'Something wrong!', originalError: err });
            } else {
                res.json({
                    status: true,
                    message: "success, current drvier status has changed."
                });
            }
        });
    });
};

var removeUploadImg = function (files) {
    Object.keys(files).forEach(function (file) {
        fs.removeSync(files[file].path);
    });
};


exports.checkcitizenid = function (req, res) {
    if (req.body._id) {  // Edit a driver via web
        DriversModel.findOne(
            {
                _id: { $ne: req.body._id },
                citizenid: req.body.citizenid,
                status: { $ne: "DELETED" }
            }, function (err, driver) {
                if (err) {
                    res.json({ status: false, originalError: err });
                } else {
                    if (driver == null) {
                        res.json({
                            status: true,
                            message: "valid citizenid"
                        });
                    } else {
                        res.json({
                            status: false,
                            message: "เลขบัตรประชาชนเลขนี้มีอยู่แล้ว",
                        });
                    }
                }
            }
        );
    } else {    // Register a new driver via web
        DriversModel.findOne(
            {
                citizenid: req.body.citizenid,
                status: { $ne: "DELETED" }
            }, function (err, driver) {
                if (err) {
                    res.json({ status: false, originalError: err });
                } else {
                    if (driver == null) {
                        res.json({
                            status: true,
                            message: "valid citizenid"
                        });
                    } else {

                        if (!driver._id_garage || !req.user._id_garage) {
                            res.json({ status: false, message: "พบปัญหาบางอย่าง กรุณาลองอีกครั้ง", err: "_id_garage is not defined" });
                        }
                        else if (driver._id_garage.toString() == req.user._id_garage.toString()) {
                            res.json({
                                status: false,
                                errcode: "ALREADY_DRIVER",
                                message: "เลขบัตรประชาชนเลขนี้มีอยู่แล้ว",
                                data: driver
                            });
                        }
                        else {
                            res.json({
                                status: false,
                                message: "ข้อมูลบัตรประชาชนเลขนี้มีอยู่แล้วที่อู่อื่น ไม่สามารถสมัครใหม่ได้"
                            });
                        }
                    }
                }
            }
        );
    }
};


//------------------ SERVICE FOR CC ADMIN ------------------//

exports.exp_waterfall = function (req, res) {
    async.waterfall([
        function (cb) {
            console.log('1a');
            var result1_1 = 10;
            var result1_2 = 20;
            cb(null, result1_1, result1_2);
        },
        function (resultFrom1_1, resultFrom1_2, cb) {
            console.log('2a')
            var result2 = resultFrom1_1 + resultFrom1_2;
            cb(null, result2);
        },
        function (final, cb) {
            console.log('3a')
            cb(null, final);
        }
    ], function (err, finalResult) {
        console.log('4a')
        console.log(finalResult);
    });
};




exports.addjob2dlist = function (socket) {
    return function (req, res) {
        var jobid;
        var i = 0;
        var results = [];
        var drvlist = {};
        var mathrandom = Math.random();
        var carplate = req.body.carplate;
        var username = "TEMPSYSTEM";
        var iscontractjob = req.body.isContractJob;
        var provincearea = req.body.provincearea;
        var curaddr = req.body.curaddr;
        var destination = req.body.destination;
        var phone = req.body.phone;
        var detail = req.body.detail;
        var curlng = req.body.curlng;
        var curlat = req.body.curlat;
        var deslng = req.body.deslng;
        var deslat = req.body.deslat;
        var amount = 1;
        var ccstation = req.body.ccstation;
        var jobtype = req.body.jobtype;                        // { "QUEUE","ADVANCE" }
        var createdjob = req.body.createdjob;                  //  TIMESTAMP (millisec)
        var cgroup = req.body.cgroup;                          // cgroup of admin user
        var _id_garage = "574ebf1ff6618310e78cef6a";           // _id_garage of admin users
        var status = req.body.status;                          // has to be DPENDING
        var emittingsocket = "addjoblist";
        var psgtype;
        var jobmode;
        var psg_name = req.body.psgname;
        var psg_phone = req.body.psg_phone;
        var tips = req.body.tips;

        if (typeof phone == 'undefined' && phone == null) {
            res.status(200).json({ status: false, message: "error no phone number" });
            return;
        }

        if (typeof carplate == 'undefined' && carplate == null) {
            res.status(200).json({ status: false, message: "error no carplate" });
            return;
        }

        if (typeof curaddr == 'undefined' && curaddr == null) {
            res.status(200).json({ status: false, message: "error no curaddr" });
            return;
        }

        if (typeof destination == 'undefined' && destination == null) {
            res.status(200).json({ status: false, message: "error no destination " });
            return;
        }

        if (typeof cgroup == 'undefined' && cgroup == null) {
            res.status(200).json({ status: false, message: "error no cgroup" });
            return;
        }

        if (typeof status == 'undefined' && status == null) {
            res.status(200).json({ status: false, message: "error no job status" });
            return;
        }

        if (createdjob) {
            createdjob = new Date(parseInt(createdjob));
        } else {
            createdjob = new Date().getTime();
        }

        if (typeof curlng == 'undefined' && typeof curlat == 'undefined') {
            var curloc = [];
        } else {
            var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
        }
        if (typeof curlng == 'undefined' && curlng == null) {
            res.status(200).json({ status: false, message: "error no longitude" });
            return;
        }
        if (typeof curlat == 'undefined' && curlat == null) {
            res.status(200).json({ status: false, message: "error no latitude" });
            return;
        }

        if (typeof deslng == 'undefined' && typeof deslat == 'undefined') {
            var desloc = [];
        } else {
            var desloc = [parseFloat(req.body.deslng), parseFloat(req.body.deslat)];
        }
        if (typeof deslng == 'undefined' && deslng == null) {
            deslng = "";
        }
        if (typeof deslat == 'undefined' && deslat == null) {
            deslat = "";
        }

        DriversModel.findOne(
            { carplate: carplate, cgroup: cgroup, status: "ON" },
            function (err, drvavailable) {
                if (drvavailable == null) {
                    console.log(' error no driver found or not available to get a job ')
                    res.json({ status: false, msg: "error no driver found or not available to get a job" });
                } else {
                    HotelsModel.findOne(
                        { phone: phone, cgroup: cgroup, _id_garage: _id_garage },
                        function (err, psg) {
                            if (psg == null) {
                                var device_id = new Date().valueOf() + '-ccpsg';
                                var create_hotel = {
                                    device_id: device_id,
                                    cgroup: cgroup,
                                    _id_garage: _id_garage,
                                    email: req.body.email,
                                    phone: phone,
                                    gender: req.body.gender,
                                    lname: req.body.lname,
                                    fname: req.body.fname,
                                    displayName: req.body.displayName,
                                    curaddr: curaddr,
                                    curlng: +curlng || 0.0,
                                    curlat: +curlat || 0.0,
                                    curloc: curloc || [],
                                    status: status,
                                    createdvia: "CALLCENTER",
                                    createdby: username,
                                    updated: new Date().getTime(),
                                    createdjob: createdjob,
                                    created: new Date().getTime(),
                                }
                                HotelsModel.create(create_hotel, function (err, response) {
                                    if (err) {
                                        console.log(err)
                                    } else {
                                        console.log('add2djob new phone')
                                        jobid = new Date().valueOf() + '-' + i;
                                        var datacallcenterpsg = {
                                            _id_hotel: response._id,
                                            device_id: jobid,
                                            cgroup: cgroup,
                                            email: jobid + "@temp.com",
                                            phone: phone,
                                            provincearea: provincearea,
                                            curaddr: curaddr,
                                            curlng: +curlng || 0.0,
                                            curlat: +curlat || 0.0,
                                            curloc: curloc || [],
                                            destination: destination,
                                            deslng: +deslng || 0.0,
                                            deslat: +deslat || 0.0,
                                            desloc: desloc || [],
                                            detail: detail,
                                            jobtype: jobtype,
                                            status: status,
                                            psgtype: psgtype,
                                            jobmode: jobmode,
                                            createdvia: "CALLCENTER",
                                            ccstation: ccstation,
                                            createdby: username,
                                            updated: new Date().getTime(),
                                            createdjob: createdjob,
                                            ccwaitingtime: 180000,
                                            created: new Date().getTime(),
                                            drvarroundlist: drvlist,
                                            jobhistory: [{
                                                action: "add job list",
                                                status: status,
                                                actionby: username,
                                                created: new Date()
                                            }]
                                        }
                                        CallcenterpsgModel.create(datacallcenterpsg, function (err, ccresponse) {
                                            if (err) {
                                                console.log(' create CallcenterpsgModel from cc addjoblist error : ' + err)
                                            } else {
                                                DriversModel.findOne(
                                                    { carplate: carplate, cgroup: cgroup },
                                                    function (err, drv) {
                                                        if (drv == null) {
                                                            console.log(' error no driver found ')
                                                            res.json({ status: false, msg: "error no driver found" });
                                                        } else {
                                                            drv.psg_detail = ccresponse.detail;
                                                            drv.psg_destination = ccresponse.destination;
                                                            drv.psg_curaddr = ccresponse.curaddr;
                                                            drv.psg_id = ccresponse._id;
                                                            drv.msgnote = "รับผู้โดยสารที่ " + ccresponse.curaddr + "\n\nไปส่งที่ " + ccresponse.destination + "\n\nเบอร์ติดต่อ" + ccresponse.phone;
                                                            drv.msgphone = ccresponse.phone;
                                                            drv.msgstatus = "NEW";
                                                            drv.msgtimeout = 18000;
                                                            drv.status = "DPENDING";
                                                            drv.jobtype = ccresponse.jobtype;
                                                            drv.updated = new Date().getTime();
                                                            drv.save(function (err, result) {
                                                                if (err) {
                                                                    console.log(err)
                                                                    res.json({ status: false, msg: "error", data: err });
                                                                } else {
                                                                    CallcenterpsgModel.findOne(
                                                                        { _id: ccresponse._id },
                                                                        function (err, ccpsg) {
                                                                            if (ccpsg == null) {
                                                                                console.log(' error no ccpsg found ')
                                                                                res.json({ status: false, msg: "No passenger found for this id" });
                                                                            } else {
                                                                                ccpsg.jobhistory.push({
                                                                                    action: "CC dpending",
                                                                                    status: "DPENDING",
                                                                                    drv_id: result._id,
                                                                                    drv_carplate: result.carplate,
                                                                                    actionby: username,
                                                                                    mathrandom: mathrandom,
                                                                                    reqbody: req.body,
                                                                                    created: new Date()
                                                                                });
                                                                                ccpsg.nccdpendingcount = 1;
                                                                                ccpsg.drv_id = result.drv_id;
                                                                                ccpsg.drv_carplate = result.carplate;
                                                                                ccpsg.updated = new Date().getTime();
                                                                                ccpsg.status = "DPENDING";
                                                                                ccpsg.assigningby = username;
                                                                                ccpsg.dpendingjob = new Date().getTime();
                                                                                ccpsg.save(function (err, ccresult) {
                                                                                    if (err) {
                                                                                        console.log(err);
                                                                                    } else {
                                                                                        console.log('assign in - 2djob' + ' - ' + new Date());
                                                                                        //socket.emit("DriverSocketOn", drv);
                                                                                        //socket.of('/'+req.user.local.cgroup).emit("assigndrvtopsg", psg);
                                                                                        // res.json({                                                          
                                                                                        //     status: true ,                                                          
                                                                                        //     psg_data: psg ,
                                                                                        //     drv_data: drv 
                                                                                        // });
                                                                                        res.status(200).json({ status: true, msg: "Add job completed", psg_data: ccpsg, drv_data: drv });
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    );
                                                                }
                                                            });
                                                        }
                                                    }
                                                );
                                            }
                                        });
                                    }
                                });
                            } else {
                                if (req.body.curaddr) { psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr; }
                                if (req.body.curlng) { psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng; }
                                if (req.body.curlat) { psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat; }
                                if (curloc) { psg.curloc = curloc ? curloc : psg.curloc; }
                                if (req.body.detail) { psg.detail = req.body.detail ? req.body.detail : psg.detail; }
                                if (req.body.createdjob) { psg.createdjob = createdjob ? createdjob : psg.createdjob; }
                                psg.save(function (err, response) {
                                    if (err) {
                                        res.json({ status: false, msg: "error", data: err });
                                    } else {
                                        drvlist = {};
                                        console.log('add2djob old phone')
                                        jobid = new Date().valueOf() + '-' + i;
                                        var datacallcenterpsg = {
                                            _id_hotel: response._id,
                                            device_id: jobid,
                                            cgroup: cgroup,
                                            email: jobid + "@temp.com",
                                            phone: phone,
                                            provincearea: provincearea,
                                            curaddr: curaddr,
                                            curlng: +curlng || 0.0,
                                            curlat: +curlat || 0.0,
                                            curloc: curloc || [],
                                            destination: destination,
                                            deslng: +deslng || 0.0,
                                            deslat: +deslat || 0.0,
                                            desloc: desloc || [],
                                            detail: detail,
                                            jobtype: jobtype,
                                            status: status,
                                            psgtype: psgtype,
                                            jobmode: jobmode,
                                            createdvia: "CALLCENTER",
                                            ccstation: ccstation,
                                            createdby: username,
                                            updated: new Date().getTime(),
                                            createdjob: createdjob,
                                            ccwaitingtime: 180000,
                                            created: new Date().getTime(),
                                            drvarroundlist: drvlist,
                                            jobhistory: [{
                                                action: "add job list",
                                                status: status,
                                                actionby: username,
                                                created: new Date()
                                            }]
                                        }
                                        CallcenterpsgModel.create(datacallcenterpsg, function (err, ccresponse) {
                                            if (err) {
                                                console.log(' create CallcenterpsgModel from cc addjoblist error : ' + err)
                                            } else {
                                                DriversModel.findOne(
                                                    { carplate: carplate, cgroup: cgroup },
                                                    function (err, drv) {
                                                        if (drv == null) {
                                                            console.log(' error no driver found ')
                                                            res.json({ status: false, msg: "error no driver found" });
                                                        } else {
                                                            drv.psg_detail = ccresponse.detail;
                                                            drv.psg_destination = ccresponse.destination;
                                                            drv.psg_curaddr = ccresponse.curaddr;
                                                            drv.psg_id = ccresponse._id;
                                                            drv.msgnote = "รับผู้โดยสารที่ " + ccresponse.curaddr + "\n\nไปส่งที่ " + ccresponse.destination + "\n\nเบอร์ติดต่อ" + ccresponse.phone;
                                                            drv.msgphone = ccresponse.phone;
                                                            drv.msgstatus = "NEW";
                                                            drv.msgtimeout = 18000;
                                                            drv.status = "DPENDING";
                                                            drv.jobtype = ccresponse.jobtype;
                                                            drv.updated = new Date().getTime();
                                                            drv.save(function (err, result) {
                                                                if (err) {
                                                                    console.log(err)
                                                                    res.json({ status: false, msg: "error", data: err });
                                                                } else {
                                                                    CallcenterpsgModel.findOne(
                                                                        { _id: ccresponse._id },
                                                                        function (err, ccpsg) {
                                                                            if (ccpsg == null) {
                                                                                console.log(' error no ccpsg found ')
                                                                                res.json({ status: false, msg: "No passenger found for this id" });
                                                                            } else {
                                                                                ccpsg.jobhistory.push({
                                                                                    action: "CC dpending",
                                                                                    status: "DPENDING",
                                                                                    drv_id: result._id,
                                                                                    drv_carplate: result.carplate,
                                                                                    actionby: username,
                                                                                    mathrandom: mathrandom,
                                                                                    reqbody: req.body,
                                                                                    created: new Date()
                                                                                });
                                                                                ccpsg.nccdpendingcount = 1;
                                                                                ccpsg.drv_id = result.drv_id;
                                                                                ccpsg.drv_carplate = result.carplate;
                                                                                ccpsg.updated = new Date().getTime();
                                                                                ccpsg.status = "DPENDING";
                                                                                ccpsg.assigningby = username;
                                                                                ccpsg.dpendingjob = new Date().getTime();
                                                                                ccpsg.save(function (err, ccresult) {
                                                                                    if (err) {
                                                                                        console.log(err);
                                                                                    } else {
                                                                                        console.log('assign in - 2djob' + ' - ' + new Date());
                                                                                        //socket.emit("DriverSocketOn", drv);
                                                                                        //socket.of('/'+req.user.local.cgroup).emit("assigndrvtopsg", ccpsg);
                                                                                        // res.json({                                                          
                                                                                        //     status: true ,                                                          
                                                                                        //     psg_data: ccpsg ,
                                                                                        //     drv_data: drv 
                                                                                        // });
                                                                                        res.status(200).json({ status: true, msg: "Add job completed", psg_data: ccpsg, drv_data: drv });
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    );
                                                                }
                                                            });
                                                        }
                                                    }
                                                );
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    );
                }
            }
        );

    };
};




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////// for the particular purpose ////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
exports.updatedrvcgroup = function (req, res) {

    _UpdateAllJoblistcgroup();

    function _UpdateAllJoblistcgroup() {
        PsgcalllogModel.find(
            {},
            function (err, drv) {
                if (drv == 0) {
                    console.log(' not found drv_id in joblist ')
                    res.json({ status: false, msg: "No data was found" });
                } else {
                    drv.forEach(function (record) {
                        DriversModel.findOne(
                            { _id: record.drv_id },
                            function (err, response) {
                                if (response == null) {
                                } else {
                                    PsgcalllogModel.findOne(
                                        { _id: record._id },
                                        function (err, result) {
                                            if (result == null) {
                                                res.json({ status: false, msg: "This driver is not available." });
                                            } else {
                                                result.cgroup = response.cgroup;
                                                result.save(function (err, response) {
                                                    if (err) {
                                                        console.log(' err ' + err)
                                                    } else {
                                                        console.log(' update : ' + response.cgroup)
                                                    }
                                                });
                                            }
                                        }
                                    )
                                }
                            }
                        )
                    });
                    //console.log (' log = resx ' )        
                    res.json({ status: true, data: drv });
                }
            }
        );
    };
};



exports.updatedrvappversion = function (req, res) {
    var appversion = req.body.appversion;
    DriversModel.find(
        { appversion: { $ne: appversion } },
        { _id: 1 },
        function (err, drv) {
            if (drv == 0) {
                res.json({ status: false, msg: "all appversion is " + appversion });
            } else {
                drv.forEach(function (record) {
                    DriversModel.findOne(
                        { _id: record._id },
                        function (err, result) {
                            if (result == null) {
                                res.json({ status: false, msg: "This driver is not available." });
                            } else {
                                result.appversion = appversion;
                                result.save(function (err, response) {
                                    if (err) {
                                        res.json({ status: false, msg: "error", data: err });
                                    } else {
                                        /*
                                            res.json({                             
                                            status: true ,                                                          
                                            data: { 
                                                drv_id: response._id,
                                                appversion: response.appversion
                                            }
                                        });
                                        */
                                    }
                                });
                            }
                        }
                    )
                });
                res.json({ status: true, data: drv });
            }
        }
    );
};




exports.UpdateExpAPPDate = function (req, res) {
    strDate = new Date(req.body.strDate).getTime() - 25200000;   // for example  "2016-05-23"
    drv_id = req.body.drv_id;
    if (drv_id) {
        drv_id = drv_id;
    } else {
        drv_id = "";
    }

    _UpdateExpAPPDate(strDate, drv_id);

    function _UpdateExpAPPDate(strDate, drv_id) {
        console.log(typeof strDate)
        console.log(strDate)
        //strDate = new Date(strDate).getTime();
        DriversModel.find(
            { "device_id": { "$exists": true, "$ne": "" } },
            { _id: 1 },
            function (err, drv) {
                if (drv == 0) {
                    console.log(' not found driver')
                    res.json({ status: false, msg: "No data was found" });
                } else {
                    drv.forEach(function (record) {
                        if (drv_id) {
                            DriversModel.findOne(
                                { _id: drv_id },
                                function (err, result) {
                                    if (result == null) {
                                        console.log(' not found ' + drv_id)
                                        res.json({ status: false, msg: "This driver is not available." });
                                    } else {
                                        result.expdateapp = strDate;
                                        result.save(function (err, response) {
                                            if (err) {
                                                console.log(' err ' + err)
                                            } else {
                                                console.log(' update ' + strDate + ' to ' + drv_id)
                                            }
                                        });
                                    }
                                }
                            )
                        } else {
                            DriversModel.findOne(
                                { _id: record._id },
                                function (err, result) {
                                    if (result == null) {
                                        res.json({ status: false, msg: "This driver is not available." });
                                    } else {
                                        result.expdateapp = strDate;
                                        result.save(function (err, response) {
                                            if (err) {
                                                console.log(' err ' + err)
                                            } else {
                                                console.log(' update ' + strDate + ' to ' + record._id)
                                            }
                                        });
                                    }
                                }
                            )
                        }
                    });
                    console.log(' log = res ')
                    res.json({ status: true, data: drv });
                }
            }
        );
    };
};




exports.TestUpdatePassword = function (req, res) {
    var _id = req.body._id;
    var device_id = req.body.device_id;
    var username = req.body.username;
    var otp_number = req.body.otp_number;
    var passwordreal = req.body.password;
    var newpassword = crypto.createHmac('sha256', config.pwd_key).update(passwordreal).digest('base64');

    res.json({
        status: false,
        pass: passwordreal,
        encryptpass: newpassword
    });

};




exports.UpdateAllDrivercgroupname = function (req, res) {
    sfield1 = req.body.sfield1;
    sfield2 = req.body.sfield2;

    _UpdateAllDrivercgroupname(sfield1, sfield2);

    function _UpdateAllDrivercgroupname(sfield1, sfield2) {
        DriversModel.find(
            { "device_id": { "$exists": true, "$ne": "" } },
            { _id: 1, cgroup: 1, fname: 1, lname: 1, cgroupname: 1, cprovincename: 1, status: 1 },
            function (err, drv) {
                if (drv == 0) {
                    console.log(' not found driver')
                    res.json({ status: false, msg: "No data was found" });
                } else {
                    drv.forEach(function (record) {
                        Lk_garageModel.findOne(
                            { cgroup: record.cgroup },
                            { cgroupname: 1, cprovincename: 1, _id: 1 },
                            function (err, response) {
                                if (response == null) {
                                } else {
                                    DriversModel.findOne(
                                        { _id: record._id },
                                        function (err, result) {
                                            if (result == null) {
                                                res.json({ status: false, msg: "This driver is not available." });
                                            } else {
                                                result._id_garage = response._id;
                                                result.cgroupname = response.cgroupname;
                                                result.cprovincename = response.cprovincename;
                                                result.save(function (err, response) {
                                                    if (err) {
                                                        console.log(' err ' + err)
                                                    } else {
                                                        console.log(' update : ' + response.cgroupname + ' and ' + response.cprovincename)
                                                    }
                                                });
                                            }
                                        }
                                    )
                                }
                            }
                        )
                    });
                    console.log(' log = resx ')
                    res.json({ status: true, data: drv });
                }
            }
        );
    };
};




exports.UpdateAllDriverInfo = function (req, res) {
    sfield1 = req.body.sfield1;
    sfield2 = req.body.sfield2;

    _UpdateAllDriverInfo(sfield1, sfield2);

    function _UpdateAllDriverInfo(sfield1, sfield2) {
        ///////////////// for updating drvpassword & useappfreemsg
        DriversModel.find(
            { "device_id": { "$exists": true, "$ne": "" } },
            { _id: 1, password: 1, prefixcarplate: 1, carplate: 1, carplate_formal: 1 },
            function (err, drv) {
                if (drv == 0) {
                    console.log(' not found driver')
                    res.json({ status: false, msg: "No data was found" });
                } else {
                    drv.forEach(function (record) {
                        DriversModel.findOne(
                            { _id: record._id },
                            function (err, result) {
                                if (result == null) {
                                    res.json({ status: false, msg: "This driver is not available." });
                                } else {
                                    if (record.carplate_formal == null) {
                                        if (record.prefixcarplate == undefined) {
                                            result.carplate_formal = record.carplate;
                                        } else {
                                            result.carplate_formal = record.prefixcarplate + record.carplate;
                                        }
                                        result.save(function (err, response) {
                                            if (err) {
                                                console.log(' err ' + err)
                                            } else {
                                                console.log(record.password.length)
                                                console.log(' update carplate_formmal =  ' + record.prefixcarplate + record.carplate)
                                            }
                                        });

                                    } else {
                                        console.log(' carplate_formal not null ' + record.carplate_formal)
                                    }
                                }
                            }
                        )
                    });
                    console.log(' log = res ')
                    res.json({ status: true, data: drv });
                }
            }
        );
    };
};


// function _UpdateAllDriverInfo(sfield1, sfield2) {
//     ///////////////// for updating drvpassword & useappfreemsg
//     DriversModel.find (
//         { "device_id" : { "$exists" : true, "$ne" : "" } }, 
//         { _id:1, password:1 },                       
//         function(err, drv) {
//             if(drv==0) {
//                 console.log (' not found driver')
//                 res.json({ status: false , msg: "No data was found" });
//             } else {    
//                 drv.forEach(function(record){
//                     DriversModel.findOne(
//                         { _id: record._id },
//                         function(err, result) {
//                             if(result==null) {
//                                 res.json({ status: false , msg: "This driver is not available." });
//                             } else {                                
//                                 if (record.password.length < 25){
//                                     result.pwdhint = record.password;
//                                 }
//                                 result.password = crypto.createHmac('sha256', config.pwd_key).update(record.password).digest('base64');                              
//                                 result.save(function(err, response) {
//                                     if(err) {
//                                         console.log (' err ' + err )
//                                     } else {
//                                         console.log (record.password.length)
//                                         console.log (' update : ' + record.password + ' = '+ record.pwdhint )
//                                     }
//                                 });
//                             }
//                         }
//                     )
//                 });     
//                 console.log (' log = res ' )        
//                 res.json({ status: true , data: drv });
//             }
//         }
//     );
// };


// DriversModel.find(
//     {  },                             
//     function(err, drv) {
//         if(drv==0) {
//             res.json({ status: false , msg: "cgroup is not empty" });
//         } else {    
//             drv.forEach(function(record){                   
//                 JoblistModel.findOne(
//                     { drv_id: record._id },
//                     function(err, result) {
//                         if(result==null) {
//                             res.json({ status: false , msg: "This driver is not available." });
//                         } else {                                
//                             result.cgroup = record.cgroup ;                                 
//                             result.save(function(err, response) {
//                                 if(err) {
//                                      res.json({ status: false , msg: "error", data: err  });
//                                 } else {
//                                     /*
//                                      res.json({                             
//                                         status: true ,                                                          
//                                         data: { 
//                                             drv_id: response._id,
//                                             appversion: response.appversion
//                                         }
//                                     });
//                                     */
//                                 }
//                             });
//                         }
//                     }
//                 )
//             });             
//             res.json({ status: true , data: drv });
//         }
//     }
// );




// ////UPdate  psgcalllog add ProvinceID
// PsgcalllogModel.find(
//     { } ,
//     { "_id": 1, "psg_device_id":1 },                       
//     function(err, result1) {
//         if(result1==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             result1.forEach(function(record){
//                 PassengerModel.findOne(
//                     { 
//                         device_id : record.psg_device_id 
//                     },
//                     function (err, result){
//                         if(result==null) {                                    
//                             console.log( record.psg_device_id +' is n/a ' )
//                         } else {
//                             console.log(result.displayName)
//                             console.log(result.curloc)
//                             console.log(record._id )
//                             PsgcalllogModel.findOne(
//                                 { _id: record._id },
//                                 function(err, taction) {
//                                     if(result==null) {    
//                                     } else {
//                                         console.log (result.curloc)
//                                         console.log (result.curlat)
//                                         console.log (result.curlng)
//                                         console.log (result.ZoneID)
//                                         console.log (result.NameTH)
//                                         console.log (result.NameEN)
//                                         console.log (result.ProvinceID)
//                                         console.log (result.displayName)
//                                         taction.curloc = result.curloc ,
//                                         taction.curlat = result.curlat ,
//                                         taction.curlng = result.curlng ,
//                                         taction.ZoneID = result.ZoneID ,
//                                         taction.NameTH = result.NameTH ,
//                                         taction.NameEN = result.NameEN , 
//                                         taction.ProvinceID = result.ProvinceID ,
//                                         taction.displayName = result.displayName ,
//                                         taction.save(function(err, response) {
//                                             if(err) {        
//                                                 console.log(err)
//                                             } else {
//                                                 console.log('ccccccccccccccccccccccccccccccccccccccccccccccccccccc')
//                                             }
//                                         });
//                                     }
//                                 }
//                             )
//                         }
//                     }
//                 );
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: result1 });
//         }
//     }
// );     


// ////UPdate  clear up drv_carplate in callcenterpsg with the end of +
// CallcenterpsgModel.find(
//     {  drv_carplate: new RegExp("\\+")  }, 
//     {"_id": 1, "drv_id":1, "drv_carplate": 1 },                       
//     function(err, result1) {
//         if(result1==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             result1.forEach(function(record){
//                 mm = record.drv_carplate;
//                 if (mm.indexOf("+")) {
//                     console.log( " xx = "+record.drv_carplate)
//                     var res = record.drv_carplate.replace("+/", "/");
//                     console.log( res )
//                     CallcenterpsgModel.findOne(
//                         { 
//                             _id : record._id 
//                         },
//                         function (err, result2){
//                             if(result2==null) {                                    
//                                 console.log( record.psg_device_id +' is n/a ' )
//                             } else {
//                                 result2.drv_carplate = res
//                                 result2.save(function(err, response) {
//                                     if(err) {        
//                                         console.log(err)
//                                     } else {
//                                         console.log('success update')
//                                     }
//                                 });
//                             }
//                         }
//                     );
//                 } else {
//                 }
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: result1 });
//         }
//     }
// );     

// CallcenterpsgModel.find(
//     {  drv_carplate: new RegExp("span")  }, 
//     {"_id": 1, "drv_id":1, "drv_carplate": 1 },                       
//     function(err, result1) {
//         if(result1==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             result1.forEach(function(record){
//                 mm = record.drv_carplate;
//                 if (mm.indexOf("+")) {
//                     console.log( " xx = "+record.drv_carplate)
//                     //var res = record.drv_carplate.replace("<span data-toggle=\"tooltip\" title=\"\" data-original-title=\"งานนี้ต้องจ่ายผ่าน Line\">", "");
//                     //var res = record.drv_carplate.replace("<span data-toggle=", "");
//                     var res = record.drv_carplate.replace("+</span>", "");
//                     console.log( res )
//                     CallcenterpsgModel.findOne(
//                         { 
//                             _id : record._id 
//                         },
//                         function (err, result2){
//                             if(result2==null) {                                    
//                                 console.log( record.psg_device_id +' is n/a ' )
//                             } else {
//                                 result2.drv_carplate = res
//                                 result2.save(function(err, response) {
//                                     if(err) {        
//                                         console.log(err)
//                                     } else {
//                                         console.log('success update')
//                                     }
//                                 });
//                             }
//                         }
//                     );
//                 } else {
//                 }
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: result1 });
//         }
//     }
// );     


// ////UPdate  drv_id in callcenterpsg
// CallcenterpsgModel.find(
//     {  "drv_id" : '' , "jobhistory.action" : "Drv finished job" }, 
//     {"_id": 1, "drv_id":1, "acceptedTaxiIds.drv_id":1 },                       
//     function(err, result1) {
//         if(result1==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             result1.forEach(function(record){
//                 CallcenterpsgModel.findOne(
//                     { 
//                         _id : record._id 
//                     },
//                     function (err, result2){
//                         if(result2==null) {                                    
//                             console.log( record.psg_device_id +' is n/a ' )
//                         } else {
//                             console.log( record.acceptedTaxiIds[0].drv_id )
//                             //console.log( record._id )
//                             result2.drv_id = record.acceptedTaxiIds[0].drv_id  
//                             result2.save(function(err, response) {
//                                 if(err) {        
//                                     console.log(err)
//                                 } else {
//                                     console.log('success update')
//                                 }
//                             });
//                         }
//                     }
//                 );
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: result1 });
//         }
//     }
// );     


// ////UPdate  displayName in Joblist
// JoblistModel.find(
//     { psg_device_id : { $exists:true }}, 
//     {_id: 1, job_id:1, curlat:1, curlng:1 , psg_device_id:1 },                       
//     function(err, result1) {
//         if(result1==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             result1.forEach(function(record){
//                 PassengerModel.findOne(
//                     { 
//                         device_id : record.psg_device_id 
//                     },
//                     function (err, result){
//                         if(result==null) {                                    
//                             console.log( record.psg_device_id +' is n/a ' )
//                         } else {
//                             console.log(result.displayName)
//                             JoblistModel.findOne(
//                                 { _id: record._id },
//                                 function(err, taction) {
//                                     if(result==null) {                                 
//                                     } else {       
//                                         taction.displayName = result.displayName , 
//                                         taction.psg_phone = result.phone ,    
//                                         taction.psg_id = result._id ,    
//                                         taction.save(function(err, response) {
//                                             if(err) {        
//                                             } else {
//                                             }
//                                         });
//                                     }
//                                 }
//                             )
//                         }
//                     }
//                 );
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: result1 });
//         }
//     }
// );     


// ////Update Province ID for Passenger
// PassengerModel.find(
//     { }, 
//     {_id: 1, curlat:1, curlng:1 },                       
//     function(err, drv) {
//         if(drv==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             drv.forEach(function(record){
//                 Lk_provincesModel.findOne(
//                     { polygons: 
//                         { $geoIntersects:
//                             {   $geometry:
//                                 {
//                                     "type" : "Point",
//                                     "coordinates" : [ record.curlng,record.curlat ] 
//                                 }
//                             }
//                         }
//                     },
//                     function (err, result){
//                         if(result==null) {                                    
//                             console.log( record.curlng+','+record.curlat )                                    
//                             PassengerModel.findOne(
//                                 { _id: record._id },
//                                 function(err, taction) {
//                                     if(result==null) {                                 
//                                     } else {         
//                                         taction.ProvinceID = 0 ,
//                                         taction.NameEN = "undefined" , 
//                                         taction.NameTH = "undefined" , 
//                                         taction.ZoneID = 0 , 
//                                         taction.save(function(err, response) {
//                                             if(err) {                                           
//                                             } else {
//                                             }
//                                         });
//                                     }
//                                 }
//                             )
//                         } else {
//                             console.log(result.ProvinceID)
//                             PassengerModel.findOne(
//                                 { _id: record._id },
//                                 function(err, taction) {
//                                     if(result==null) {                                 
//                                     } else {         
//                                         taction.ProvinceID = result.ProvinceID,
//                                         taction.NameEN = result.NameEN, 
//                                         taction.NameTH = result.NameTH, 
//                                         taction.ZoneID = result.ZoneID, 
//                                         taction.save(function(err, response) {
//                                             if(err) {                                           
//                                             } else {
//                                             }
//                                         });
//                                     }
//                                 }
//                             )
//                         }
//                     }
//                 );
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: drv });
//         }
//     }
// );      


//check  polygon intersec
// Lk_provincesModel.find(
//     { }, 
//     {_id: 1, Lat:1, Long:1, NameEN: 1 },                       
//     function(err, drv) {
//         if(drv==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             drv.forEach(function(record){
//                 Lk_provincesModel.findOne(
//                     { polygons: 
//                         { $geoIntersects:
//                             {   $geometry:
//                                 {
//                                     "type" : "Point",
//                                     "coordinates" : [ record.Long,record.Lat ] 
//                                 }
//                             }
//                         }
//                     },
//                     function (err, result){
//                         if(result==null) {                                    
//                             console.log(' no province id for ' + record.Long+','+record.Lat )                                    
//                         } else {
//                             //console.log(' *** ' + record.Long+','+record.Lat )   
//                             console.log(' province id ' + result.ProvinceID)
//                             //console.log(' province name ' + result.NameEN)
//                         }
//                     }
//                 );
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: drv });
//         }
//     }
// );      


// // Update Province ID
// JoblistModel.find(
//     { ProvinceID: { "$exists" : false } }, 
//     {_id: 1, job_id:1, curlat:1, curlng:1 },                       
//     function(err, drv) {
//         if(drv==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             drv.forEach(function(record){
//                 Lk_provincesModel.findOne(
//                     { polygons: 
//                         { $geoIntersects:
//                             {   $geometry:
//                                 {
//                                     "type" : "Point",
//                                     "coordinates" : [ record.curlng,record.curlat ] 
//                                 }
//                             }
//                         }
//                     },
//                     function (err, result){
//                         if(result==null) {                                    
//                             console.log( record.curlng+','+record.curlat )
//                             console.log(record.job_id)
//                             JoblistModel.findOne(
//                                 { _id: record._id },
//                                 function(err, taction) {
//                                     if(result==null) {                                 
//                                     } else {         
//                                         taction.ProvinceID = 0 ,
//                                         taction.NameEN = "undefined" , 
//                                         taction.NameTH = "undefined" , 
//                                         taction.ZoneID = 0 , 
//                                         taction.save(function(err, response) {
//                                             if(err) {                                           
//                                             } else {
//                                             }
//                                         });
//                                     }
//                                 }
//                                 )
//                         } else {
//                             console.log(result.ProvinceID)
//                             JoblistModel.findOne(
//                                 { _id: record._id },
//                                 function(err, taction) {
//                                     if(result==null) {                                 
//                                     } else {         
//                                         taction.ProvinceID = result.ProvinceID,
//                                         taction.NameEN = result.NameEN, 
//                                         taction.NameTH = result.NameTH, 
//                                         taction.ZoneID = result.ZoneID, 
//                                         taction.save(function(err, response) {
//                                             if(err) {                                           
//                                             } else {
//                                             }
//                                         });
//                                     }
//                                 }
//                              )
//                         }
//                     }
//                 );
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: drv });
//         }
//     }
// );      


// /////////////// for updating joblist curloc
// JoblistModel.find(
//     {  }, 
//     {_id: 1, curlat:1, curlng:1 },                       
//     function(err, drv) {
//         if(drv==0) {
//             console.log (' not found ')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             drv.forEach(function(record){
//                 JoblistModel.findOne(
//                     { _id: record._id },
//                     function(err, result) {
//                         if(result==null) {
//                             res.json({ status: false , msg: "This driver is not available." });
//                         } else {                                
//                             result.curloc = [ parseFloat(record.curlng), parseFloat(record.curlat) ]
//                             result.save(function(err, response) {
//                                 if(err) {
//                                     console.log (' err ' + err )
//                                 } else {
//                                     console.log (' updating : ' + sfield1 + ' and '+ sfield2 )
//                                 }
//                             });
//                         }
//                     }
//                 )
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: drv });
//         }
//     }
// );        


///////////////// for updating useappfree & useappfreemsg
// DriversModel.find(
//     { "device_id" : { "$exists" : true, "$ne" : "" } }, 
//     {_id: 1  },                       
//     function(err, drv) {
//         if(drv==0) {
//             console.log (' not found driver')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             drv.forEach(function(record){
//                 DriversModel.findOne(
//                     { _id: record._id },
//                     function(err, result) {
//                         if(result==null) {
//                             res.json({ status: false , msg: "This driver is not available." });
//                         } else {                                
//                             result.useappfree = sfield1;
//                             result.useappfreemsg = sfield2;
//                             result.save(function(err, response) {
//                                 if(err) {
//                                     console.log (' err ' + err )
//                                 } else {
//                                     console.log (' update : ' + sfield1 + ' and '+ sfield2 )
//                                 }
//                             });
//                         }
//                     }
//                 )
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: drv });
//         }
//     }
// );



///////////////// for updating drvpassword & useappfreemsg
// DriversModel.find(
//     { "device_id" : { "$exists" : true, "$ne" : "" } }, 
//     { _id:1, password:1, pwdhint:1 },                       
//     function(err, drv) {
//         if(drv==0) {
//             console.log (' not found driver')
//             res.json({ status: false , msg: "No data was found" });
//         } else {    
//             drv.forEach(function(record){
//                 DriversModel.findOne(
//                     { _id: record._id },
//                     function(err, result) {
//                         if(result==null) {
//                             res.json({ status: false , msg: "This driver is not available." });
//                         } else {                                
//                             result.password = crypto.createHmac('sha256', config.pwd_key).update(record.password).digest('base64');
//                             if (record.password.length < 25){
//                                  result.pwdhint = record.password;
//                             }                                    
//                             result.save(function(err, response) {
//                                 if(err) {
//                                     console.log (' err ' + err )
//                                 } else {
//                                     console.log (record.password.length)
//                                     console.log (' update : ' + record.password + ' and '+ record.pwdhint )
//                                 }
//                             });
//                         }
//                     }
//                 )
//             });     
//             console.log (' log = res ' )        
//             res.json({ status: true , data: drv });
//         }
//     }
// );


// exports.UpdateAllDrivercgroupname = function(req, res) {
//     _UpdateAllDrivercgroupname();
//     function _UpdateAllDrivercgroupname() {
//         DriversModel.find(
//             { "cgroup" : { "$exists" : false} }, 
//             {_id: 1, cgroup:1, fname:1, lname:1, cgroupname:1, cprovincename:1, status:1 },                        
//             function(err, drv) {
//                 if(drv==0) {
//                     console.log (' not found driver')
//                     res.json({ status: false , msg: "No data was found" });
//                 } else {
//                     drv.forEach(function(record){
//                         DriversModel.findOne(
//                             { _id: record._id },
//                             function(err, result) {
//                                 if(result==null) {
//                                     res.json({ status: false , msg: "This driver is not available." });
//                                 } else {                                
//                                     result.cgroup = "ccubon" ;                                                
//                                     result.save(function(err, response) {
//                                         if(err) {
//                                             console.log (' err ' + err )
//                                         } else {
//                                             console.log (' update cgroup to ccubon success '  )
//                                         }
//                                     });
//                                 }
//                             }
//                         )
//                     }); 
//                     console.log (' log = resx ' )        
//                     res.json({ status: true , data: drv });
//                 }
//             }
//         );
//     };
// };




// exports.updatedrvcgroup = function(req, res) {
// var cgroup = "ccubon"       ;
//     DriversModel.find(
//         {  },                             
//         function(err, drv) {
//             if(drv==0) {
//                 res.json({ status: false , msg: "cgroup is not empty" });
//             } else {    
//                 drv.forEach(function(record){                   
//                     DriversModel.findOne(
//                         { _id: record._id },
//                         function(err, result) {
//                             if(result==null) {
//                                 res.json({ status: false , msg: "This driver is not available." });
//                             } else {                                
//                                 result.cgroup = cgroup; 
//                                 result.save(function(err, response) {
//                                     if(err) {
//                                          res.json({ status: false , msg: "error", data: err  });
//                                     } else {
//                                         /*
//                                          res.json({                             
//                                             status: true ,                                                          
//                                             data: { 
//                                                 drv_id: response._id,
//                                                 appversion: response.appversion
//                                             }
//                                         });
//                                         */
//                                     }
//                                 });
//                             }
//                         }
//                     )
//                 });             
//                 res.json({ status: true , data: drv });
//             }
//         }
//     );
// };




exports.InsertRndDriver = function (req, res) {
    var nY = 0;
    var nN = 0;
    // Test javascript at "http://js.do/"
    function ranCarplate() {
        var str = Math.random();
        var str1 = str.toString();
        var res = str1.substring(2, 6);
        return res;
    }

    function ranTel() {
        var str = Math.random();
        var str1 = str.toString();
        var res = str1.substring(2, 9);
        return res;
    }

    // Test javascript at "http://js.do/"
    function ranCZID() {
        var str = Math.random();
        var str1 = str.toString();
        var res = str1.substring(2, 15);
        return res;
    }

    function ranYN() {
        var str = Math.random();
        var str1 = str.toString();
        var res = str1.substring(2, 3);
        if (res % 2) {
            res = "N";
        } else {
            res = "Y";
        }
        return res;
    }

    function ranCartype() {
        var str = Math.random();
        var str1 = str.toString();
        var res = str1.substring(2, 3);
        if (res % 2) {
            res = "car";
        } else {
            res = "minivan";
        }
        return res;
    }

    function ranCarcolor() {
        var str = Math.random();
        var str1 = str.toString();
        var res = str1.substring(2, 3);
        var ccarcolr;
        //console.log(res)
        switch (res) {
            case "1":
                ccarcolr = "น้ำเงิน";
                break;
            case "2":
                ccarcolr = "เขียว";
                break;
            case "3":
                ccarcolr = "ส้ม";
                break;
            case "4":
                ccarcolr = "ชมพู";
                break;
            case "5":
                ccarcolr = "ชุมพู-เทา";
                break;
            case "6":
                ccarcolr = "แดง";
                break;
            case "7":
                ccarcolr = "แดง-น้ำเงิน";
                break;
            case "8":
                ccarcolr = "แดง-เทา";
                break;
            case "9":
                ccarcolr = "ม่วง";
                break;
            case "10":
                ccarcolr = "เหลือง-แดง";
                break;
            case "0":
                ccarcolr = "เหลือง-เขียว";
                break;
        }
        return ccarcolr
    }

    for (i = 0; i < 100; i++) {
        device_id = new Date().getTime() + ranCarplate();
        //console.log(i)
        DriversModel.create({
            device_id: device_id,
            username: "test" + i,
            password: "123456",
            fname: "fname-test-" + ranCarplate(),
            lname: "lname-test-" + ranCarplate(),
            phone: "081" + ranTel(),
            citizenid: ranCZID(),
            taxiID: "testsimulation",
            english: ranYN(),
            carplate: ranCarplate(),
            cartype: ranCartype(),
            carcolor: ranCarcolor(),
            outbound: ranYN(),
            carryon: ranYN(),
            address: ranCarplate(),
            province: ranCarplate(),
            district: ranCarplate(),
            tambon: ranCarplate(),
            zipcode: ranCarplate(),
            imgface: "56e92c4a7e6eb6f9c2a12f64_imgface.png",
            imglicence: "56e92c4a7e6eb6f9c2a12f64_imglicence.png",
            imgcar: "56e92c4a7e6eb6f9c2a12f64_imgcar.png",
            active: "Y",
            status: "OFF",
            created: new Date().getTime(),
            updated: new Date().getTime(),
            expdategarage: new Date().getTime(),
            expdateapp: new Date().getTime() + 7776000000,
            smsconfirm: ranCarplate()
        },
            function (err, response) {
                if (err) {
                    nN++;
                } else {
                    nY++;
                }
            }
        );
    }
    res.json({ status: true, msg: "Success = " + nY + " : Not = " + nN })
}




exports.testjointable = function (req, res) {
    //var MongoClient = require('mongodb').MongoClient;
    //var assert = require('assert');
    //var ObjectId = require('mongodb').ObjectID;
    //var url = 'mongodb://localhost:27017/test';
    var psg_id = req.body.psg_id;
    var namearray = [];

    // CallcenterpsgModel.find({}, function (err, db_users) {
    //  if(err) {/*error!!!*/}
    //      DriversModel.find({}, function (err, db_articles) {
    //      if(err) {/*error!!!*/}
    //          res.render('profile/profile', {
    //          users: db_users,
    //          articles: db_articles
    //      });
    //  });
    // });
    CallcenterpsgModel.findOne(
        { _id: psg_id },
        { _id: 1, phone: 1, curaddr: 1, destination: 1, drv_id: 1 },
        function (err, position) {
            if (!err) {
                //console.log(' drv_id: '+position._id)
                //var total = position.length;
                //console.log(" total ="+total)
                //for (i=0;i< position.length; i++) {  
                //var obj = JSON.stringify(position[i]);
                //var pos = JSON.parse(obj);
                //var p = pos["player_stats_daily"]["content"]["team_sport_content"]["league_content"]["season_content"]["team_content"]["team"]["id"]
                //var p = pos["fname"]["lname"]["carplate"]
                DriversModel.find(
                    { _id: position.drv_id },
                    { _id: 1, carplate: 1, curloc: 1 },
                    function (err, team) {
                        if (!err) {
                            //var obj = JSON.stringify(team);
                            //var pos = JSON.parse(obj);
                            //namearray.push(team);
                            res.json({
                                status: true,
                                psg_data: position,
                                drv_data: team
                            });
                        }
                    }
                )
                //}
            }
        }
    )
}

/*
example : http://stackoverflow.com/questions/16584921/how-to-query-two-collection-in-mongoose-on-nodejs  for Mongoose   
    action(function getpositions(req){
    var namearray = [];
    NHLPlayerStatsDaily.find ({"created_at": {$gt: new Date(y+"-"+m+"-"+d)}}, function(err,position){
    if(!err)
    { 
    for (i=0;i< position.length; i++)
    {  
        var obj = JSON.stringify(position[i]);
        var pos = JSON.parse(obj);

        var p = pos["player_stats_daily"]["content"]["team_sport_content"]["league_content"]["season_content"]["team_content"]["team"]["id"]
        NHLTeam.find({"sdi_team_id": p}, "first_name nick_name short_name sport_id", function(err, team){
                    if (!err){
                    var obj = JSON.stringify(team);
                    var pos = JSON.parse(obj);
                    namearray.push(team)
                    }
            })
        }
        return send(namearray);
    }
        })
        })

NHLPlayerStatsDaily.find ({"created_at": {$gt: new Date(y+"-"+m+"-"+d)}}, function(err,position){
    if(!err)
    { 
    var total = position.length;

    for (i=0;i< position.length; i++)
    {  
        var obj = JSON.stringify(position[i]);
        var pos = JSON.parse(obj);

        var p = pos["player_stats_daily"]["content"]["team_sport_content"]["league_content"]["season_content"]["team_content"]["team"]["id"]
        NHLTeam.find({"sdi_team_id": p}, "first_name nick_name short_name sport_id", function(err, team){
        total--;
        if (!err){
            var obj = JSON.stringify(team);
            var pos = JSON.parse(obj);
            namearray.push(team);
        }
        if(total == 0)
            send(namearray);
        })
    }

    }
})

!!!!!!!!!!! This " http://mongoosejs.com/docs/populate.html " for mongoose 

!!!!!!!!!!! This one is working for mongodb !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! BUT how to use for mongoose ????
example : select from 2 tables without Map reduce // http://stackoverflow.com/questions/6502541/mongodb-query-multiple-collections-at-once
users
{
    "_id":"12345",
    "admin":1
},
{
    "_id":"123456789",
    "admin":0
}

posts
{
    "content":"Some content",
    "owner_id":"12345",
    "via":"facebook"
},
{
    "content":"Some other content",
    "owner_id":"123456789",
    "via":"facebook"
}

var myCursor = db.users.find({admin:1});
var user_id = myCursor.hasNext() ? myCursor.next() : null;
    db.passengers.find({owner_id : user_id._id});

posts
{
    "content":"Some content",
    "user":{"_id":"12345", "admin":1},
    "via":"facebook"
},
{
    "content":"Some other content",
    "user":{"_id":"123456789", "admin":0},
    "via":"facebook"
}
*/



exports.requestCancelJob = function (socket) {

    var getCaseId = function (len) {
        return crypto.randomBytes(Math.ceil(len / 2))
            .toString('hex') // convert to hexadecimal format
            .slice(0, len);   // return required number of characters
    }

    return function (req, res) {
        var reqdrvid = req.body._id;
        var passengerType = req.body.passengerType;
        var passengerId = req.body.passengerID;
        var jobCancelMessage = req.body.jobcancelmessage;

        var setCountdownChangeStatus = function (driverId, resObj) {

            if(driverId){
                DriversModel
                .findOne({ _id: driverId }).exec(function (err, driver) {

                    driver.status = "ON";
                    setTimeout(function () {
                        driver.save();
                    }, 1 * (60 - 5) * 1000);

                    resObj.status(200).json({
                        status: true,
                        message: "Success",
                        data: driver
                    });
                });
            } else {
                DriversModel
                .findOne({ _id: reqdrvid }).exec(function (err, driver) {

                    driver.status = "ON";
                    setTimeout(function () {
                        driver.save();
                    }, 1 * (60 - 5) * 1000);

                    resObj.status(200).json({
                        status: true,
                        message: "Success",
                        data: driver
                    });
                });                
            }

        }

        if (passengerType === 'Hotel') {
            JoblisthotelModel.findOne({ _id: passengerId }).exec(function (err, jobHotel) {

                if (err || !jobHotel) {
                    res.json({ status: false, message: "Cannot find job hotel id: " + passengerId, data: (err || jobHotel) });
                    return;
                }

                CallcenterpsgModel
                    .findOne({ _id: jobHotel._id_callcenterpsg })
                    .exec(function (err, callcenterpsg) {

                        if (err || !callcenterpsg) {
                            res.status(200).json({ status: false, message: "CallcenterpsgModel Query data fail.", data: err });
                            return;
                        }

                        if (callcenterpsg !== null) {
                            callcenterpsg.cancel_case_id = getCaseId(10);
                            callcenterpsg.cancel_message = jobCancelMessage;
                            callcenterpsg.status = "DEPENDING_REJECT";
                            callcenterpsg.jobhistory.push({
                                action: "Drv cancelled job",
                                status: "DEPENDING_REJECT",
                                reqbody: req.body,
                                created: new Date()
                            });
                            callcenterpsg.save();
                        }

                        if (jobHotel !== null) {
                            jobHotel.status = "DRVDENIED";
                            jobHotel.jobhistory.push({
                                action: "Drv cancelled job",
                                status: "DRVDENIED",
                                reqbody: req.body,
                                created: new Date()
                            });
                            jobHotel.save();
                        }
                        setCountdownChangeStatus(jobHotel.drv_id, res);
                        socket.of('/' + callcenterpsg.cgroup).emit('gotdispatchaction', { psg_data: callcenterpsg });
                    });
            });
        } else if (passengerType === 'Person') {
            PassengerModel.findOne({ _id: passengerId }).exec(function (err, passenger) {

                if (err || !passenger) {
                    res.json({ status: false, message: "Cannot find passenger id: " + passengerId, data: (err || passenger) });
                    return;
                }

                JoblistModel
                    .findOne({ job_id: passenger.job_id })
                    .exec(function (err, job) {

                        if (err || !job) {
                            res.status(200).json({ status: false, message: "JobModel Query data fail.", data: err });
                            return;
                        }

                        if (job !== null) {
                            job.cancel_case_id = getCaseId(10);
                            job.cancel_message = jobCancelMessage;
                            job.status = "DRIVER_CANCEL";
                            job.datedrvcancel = new Date().getTime();
                            job.drv_path.push({
                                status: "DRIVER_CANCEL",
                                created: new Date()
                            });                            
                            job.save();
                        }

                        if (passenger !== null) {
                            passenger.status = "DRVDENIED";
                            passenger.save();
                        }

                        setCountdownChangeStatus(job.drv_id, res);
                        _sub_samsungaction(passenger.device_id, passenger.job_id, job.drv_id, "DRVDENIED", "cancel", "PUT");

                    });
            });
        } else {
            CallcenterpsgModel
                .findOne({ _id: passengerId })
                .exec(function (err, callcenterpsg) {

                    if (err || !callcenterpsg) {
                        res.status(200).json({ status: false, message: "CallcenterpsgModel Query data fail.", data: err });
                        return;
                    }

                    if (callcenterpsg !== null) {
                        callcenterpsg.cancel_case_id = getCaseId(10);
                        callcenterpsg.cancel_message = jobCancelMessage;
                        callcenterpsg.status = "DEPENDING_REJECT";
                        callcenterpsg.jobhistory.push({
                            action: "Drv cancelled job",
                            status: "DEPENDING_REJECT",
                            reqbody: req.body,
                            created: new Date()
                        });                        
                        callcenterpsg.save();
                    }

                    setCountdownChangeStatus(callcenterpsg.drv_id, res);
                    socket.of('/' + callcenterpsg.cgroup).emit('gotdispatchaction', { psg_data: callcenterpsg });
                });
        }
        // if(req.url.substring(1,7)=='smiles'){
        // } else {
        //     if(driver.firstshift==true){
        //         var PreShift = driver.drv_shift;
        //         var PostShift = driver.drv_shift + 1;
        //         driver.drv_shift = Postshift;
        //         driver.firstshift = false;
        //         driver.firstshift_id = null;
        //         driver.firstshift_type = null; 
        //         driver.shift_start = new Date();
        //         driver.shift_end = new Date();
        //     }
        // }
		// function _returnshift(_id, PreShift, PostShift) {
		// 	ShifttransactionModel.create({              
        //         AccountIDs: [ { _id: _id } ] ,
        //         PreShift: [ { _id: _id, Amount: PreShift } ] ,
        //         PostShift: [ { _id: _id, Amount: PostShift } ] ,
        //         Amount: 1 ,
        //         Type: "Refund",
        //         AdminID: "SYST",
        //         DateTime: new Date()
		// 	},
		// 		function (err, response) {
		// 			if (err) {
		// 				console.log(' _returnshift by user is error ')
		// 			} else {
		// 				return next();
		// 			}
		// 		}
		// 	);
        // }
                
    }
}




function _sub_samsungaction(passenger_ssid, passenger_jobid, taxi_duid, passenger_status, action, method) {
    var reqset = '{"taxi_duid": "' + taxi_duid + '", "status": "' + passenger_status + '"}';
    var postData = JSON.parse(JSON.stringify(reqset));
    var options = {
        host: config.samsunghost,
        port: config.samsungport,
        path: config.samsungpath + '/' + passenger_ssid + '/requests' + passenger_jobid + '/' + action,
        method: method,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Content-Length': Buffer.byteLength(postData)
        }
    };

    var request = http.request(options, function (response) {
        response.on('data', function (dog) {
        });
        response.on('end', function (cat) {
        });
    });
    request.on('error', function (err) {
        console.log('cannot do _sub_drvlogservice')
    });
    request.write(postData);
    request.end();
}