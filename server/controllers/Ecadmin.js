////////////////////////////////////
// TaxiBeam API Controller 
// version : 1.0.0.1 
// Date July 16, 2015 
// Wrote by Hanzen
////////////////////////////////////
var config = require('../../config/func').production;

// var hostrun = os.hostname();
var Url = require('url') ;
var git = require('git-rev-sync');
var mongoose  = require('mongoose') ;
var TemplModel = mongoose.model('TemplModel') ;
var PassengerModel  = mongoose.model('PassengerModel') ;
var UserModel  = mongoose.model('UserModel') ;
var PathLogModel = mongoose.model('PathLogModel') ;
var DriversModel = mongoose.model('DriversModel') ;
var G_usergroupModel = mongoose.model('G_usergroupModel') ;
var CommentModel  = mongoose.model('CommentModel') ;
var AnnounceModel	 = mongoose.model('AnnounceModel') ;
var EmgListModel = mongoose.model('EmgListModel') ;
var ErrorcodeModel =  mongoose.model('ErrorcodeModel') ;
var Lk_garageModel = mongoose.model('Lk_garageModel') ;
var MsghistoryModel = mongoose.model('MsghistoryModel');
var HistoryModel = mongoose.model('HistoryModel');
var CallcenterpsgModel = mongoose.model('CallcenterpsgModel');
var BoontermtransactionModel = mongoose.model('BoontermtransactionModel');
var ios = require('socket.io');

// for upload file
var path = require('path'),
	formidable = require('formidable'),
	util = require('util'),
	fs = require('fs-extra'),
	qt = require('quickthumb'),
	http = require('http'),
	qs = require('querystring');


// for all API
var _id = "",
	hmap_key = { "localhost": "Evs0z2OaYLDO8e3sl8HwK7MwiS66Y@7Yqo-K$eeRwRQkxqrbO$", "ubeam.demo.taxi-beam.com": "NM7SZ26ZV$jN6Ed8-TJ2@dx4cXXOuj9H9rrD5OOgU4Fh4hSavu" },
	device_id = "",
	smsconfirm = "",
	newimgupload = "",
	bupload = "",
	imgtype = "",
	curloc = "",
	cartype = "",
	radian = "5000",		// in meters
	amount = "",
	psg_id = "",
	commtype = "",
	topic = "",
	comment = "",
	favcartype = "",
	favtaxi = "",
	radian = "",
	amount = "",
	curaddr = "",
	destination = "",
	desloc = "",
	des_dist = "",
	tips = "",
	detail = "",
	favcartype = "",
	taxi_id = "",
	emgtype = "";



// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}



function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}



function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);
		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};



function _pathLog(data, res) {
	//console.log(data);
	//data.timestamp = data.timestamp
	PathLogModel.create(data)
}



function _getErrDetail(errid) {
	ErrorcodeModel.findOne(
		{ errid: errid },
		{ erren: 1, errth: 1 },
		function (err, response) {
			if (response == null) {
				return "";
			} else {

				return response[0];
			}
		}
	);
};


////////////////////////////////////////////////////
function array_include(arr, obj) {
	return (arr.indexOf(obj) != -1);
}



////// for the top right menu of the admin page
function chk_access_menu(user_gr, acc_callback) {
	var usermenu = [];
	//console.log('user_gr ='+user_gr)
	//var roleall =[] ;
	TemplModel.find({},
		'', // 	filter colum	
		{ limit: 500 },
		function (err, roleall) {
			for (var i = 0; i < roleall.length; i++) {
				//  var acc = roleall[i].groupacc ;    
				if (roleall[i].templtype == "menu") {
					for (var j = 0, k = user_gr.length; j < k; j++) {
						if (array_include(roleall[i].groupacc, user_gr[j])) {
							usermenu.push(roleall[i])
							break;
						}
					}
				}
			}
			//return usermenu ; 
			acc_callback(usermenu);
		})
}
////



function find_templ(gr_filter, aid, callback) {
	var templ_list = [];
	var filters = {};
	if (typeof aid == 'string') {
		filters.aid = aid;
	} else {
		if (gr_filter.indexOf('ecartadmin') == -1) { //
			filters.templtype = 'templ'
		}
		//filters.templtype = 'templ'    
	}

	TemplModel.find(
		filters,
		'', // 	filter colum	
		//{   limit: 5000    },
		{ sort: { 'aid': 1 } },
		function (err, t_list) {
			formData_ = t_list;
			//  var gr_filter = Euser_gr; // ["ecartadmin","ecartstaff","user" ]   ; // Euser_gr ; //  user_gr_arr; // 
			var endans = [];
			if (gr_filter.indexOf('ecartadmin') !== -1) { //
				endans = t_list;
			} else {

				for (var i = 0; i < formData_.length; i++) {
					var test_data = formData_[i].json[0];
					var upindex = [];
					var result = [];
					var result2 = [];
					test_data.rows.filter(function (row, index) {
						upindex[index] = result2
						result2 = []
						row.forEach(function (obj) {
							obj.rolegroup.forEach(function (Egroup) {
								if (gr_filter.indexOf(Egroup) !== -1) { // 
									var count = 0;
									result.forEach(function (res) {
										res.name == obj.name && count++;
									});
									if (count == 0) {
										result.push(obj);
										result2.push(obj);
									}
								}
							});
						});
						upindex[index] = result2
						result2 = []
					});
					objA = {}
					objA = {
						"rows": upindex,
						"hidden": []
					}
					formData_[i].json = [objA]
					endans.push(formData_[i])
				}

			}
			callback(endans);
		} // find
	).sort({ templtype: 1 });
}




function find_hmap_key(h) {
	var b = h.split(':')[0]
	hmap_key = {
		"localhost": "Evs0z2OaYLDO8e3sl8HwK7MwiS66Y@7Yqo-K$eeRwRQkxqrbO$",
		"ubeam.demo.taxi-beam.com": "NM7SZ26ZV$jN6Ed8-TJ2@dx4cXXOuj9H9rrD5OOgU4Fh4hSavu",
		"lite.taxi-beam.com": "wi69PDxOI48nsUnEo$BfKZzo$DbRxc6IZZYd@gLOpKbpV8eSui",
		"lite-dev.taxi-beam.com": "jcMwkY50Yu1aTMTtKL-7B0sK4PcaaSEXbXVHHE--1swEThFfoo",
		"dispatcher-dev.taxi-beam.com": "XpIj4K0fKUHHRVO4LB04XHsRnXwvPMK3wyptxgKCG17YSn0QHq",
		"web-dev.taxi-beam.com": "5@98Q8zakZG6Cg65Wvt09Xpts0IynuwxJqjZDFO9Abp@$-NJsy",
		"lite-test.taxi-beam.com": "jXZ5Zzuyi_PyF6fUwB7yeyfv@EQNZ7V2yNJqNjUC6TfNGwdACj",
		"dispatcher-test.taxi-beam.com": "Sw34ArtWhv1x3rdAmRBCW5d7JyB7q_yMkTJzgF6jhzEgdv_keh",
		"web-test.taxi-beam.com": "By3R73D@ZWBktIBfnHAWqX552qae@Pr3zkf0T72igEB1LrH6cq",
		"dispatcher.taxi-beam.com": "1bhbrKJEK13KZknb7NqsgZ_nKsn6msiSKkr1nPfYUvgkFvRXxM",
		"web.taxi-beam.com": 'hy16Hs@C4rMKuE1r8t2x0XQK@ZA6kLKCyd3a$Ggc2diUh1kZ5l',
		"callcenter.taxi-beam.com": 'HJq_rFQ_AnO5AzWqA@hNPigqcR8PuXxUq9HP17G61oJTN86pYO',
		"callcenter-dev.taxi-beam.com": '6AXnCI5p5johsESn4Z4tfTLcGqISZ5kvIfUR_YTtg1qCcECm6F',
		"callcenter-test.taxi-beam.com": 'Vw4s021$DdaMugLVCRu_a88ZQX6sicLItBYk1jUYib2sGDlXox',
		"admin.taxi-beam.com": "_eVuQZAwxLV0poz9YM4en$vA7E28dPpVUGq@1ON-cxZOOgPNPk",
		"admin-test.taxi-beam.com": "2wFPXxYYHgTWD9q2F42pUiSVyFHv76fkIoAhWaO_emCnOL0O8K",
		"admin-dev.taxi-beam.com": "FWuYJU9HWMT3sT87jsOjtTYCI$IMd9lj4m5_@oLNoIudItRVAu"
	}
	return hmap_key[b];
}



exports.ChkAccess = function (Euser_gr, chkurl, ChkAccess_callback) {
	var useraccess = false;
	TemplModel.find(
		{},
		'', // 	filter colum	
		{ limit: 500 },
		function (err, roleall) {
			for (var i = 0, l = roleall.length; i < l; i++) {
				var acc = roleall[i].groupacc;
				for (var j = 0, k = Euser_gr.length; j < k; j++) {
					if (array_include(acc, Euser_gr[j])) {
						if (roleall[i].url == chkurl) {
							useraccess = true
							console.log(useraccess)
							ChkAccess_callback(useraccess);
						}
						break;
					}
				}
			}
			//return useraccess ; 
		}
	)
}




exports.signup = function (req, res) {
	var gp = [{ name: "ecartadmin" }, { name: "ecartstaff" }, { name: "user" }];
	G_usergroupModel.find(
		//	{ user_gr:"user_gr" } ,
		{
			//active:"Y"
		}, '',		// 	filter colum	
		{ limit: 5000 },
		function (err, t_list) {
			res.render('signup.ejs', {
				gp: t_list
			});
		}
	);
};



exports.POIManager = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	if (req.user.local.cgroup === 'ecartadmin'
		|| req.user.local.cgroup === 'guest') {
		res.redirect('/admin');
		return;
	}
	var hmap = find_hmap_key(req.headers.host);
	var user_gr = req.user.group;
	chk_access_menu(req.user.group, function (usermenu) {
		res.render('poimanager.ejs', {
			user: req.user,
			baseUrl: req.protocol + '://' + req.get('host'),
			user_data: {
				id: req.user._id,
				_id_garage: req.user._id_garage,
				username: req.user.username,
				group: req.user.local.cgroup,
				email: req.user.local.cemail,
				name: req.user.local.cname,
				groupNameTH: req.user.local.cgroupname,
				provinceTH: req.user.local.cprovincename,
				provinceEN: req.user.local.provinceEN
			},
			menu: usermenu,
			hmapkey: hmap,
			usercurloc: req.user.local.curloc
		});
	})
};




exports.ccadmindriverlist = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	if (req.user.local.cgroup === 'ecartadmin'
		|| req.user.local.cgroup === 'guest') {
		res.redirect('/admin');
		return;
	}

	//Ecadmin.	
	//    var  templ =find_templ(user_gr) ;
	var hmap = find_hmap_key(req.headers.host);
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	chk_access_menu(req.user.group, function (usermenu) {
		res.render('ccadmindriverlist.ejs', {
			user: req.user,  // get the user out of session and pass to template
			baseUrl: req.protocol + '://' + req.get('host'),
			user_data: {
				id: req.user._id,
				_id_garage: req.user._id_garage,
				username: req.user.username,
				group: req.user.local.cgroup,
				email: req.user.local.cemail,
				name: req.user.local.cname,
				groupNameTH: req.user.local.cgroupname,
				provinceTH: req.user.local.cprovincename,
				provinceEN: req.user.local.provinceEN
			},
			menu: usermenu,
			hmapkey: hmap,
			usercurloc: req.user.local.curloc
		});
	})
};



exports.driverpendinglist = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	if (req.user.local.cgroup === 'ecartadmin'
		|| req.user.local.cgroup === 'guest') {
		res.redirect('/admin');
		return;
	}
	//Ecadmin.	
	//    var  templ =find_templ(user_gr) ;
	var hmap = find_hmap_key(req.headers.host);
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	chk_access_menu(req.user.group, function (usermenu) {
		res.render('driverpendinglist.ejs', {
			user: req.user,  // get the user out of session and pass to template
			baseUrl: req.protocol + '://' + req.get('host'),
			user_data: {
				id: req.user._id,
				_id_garage: req.user._id_garage,
				username: req.user.username,
				group: req.user.local.cgroup,
				email: req.user.local.cemail,
				name: req.user.local.cname,
				groupNameTH: req.user.local.cgroupname,
				provinceTH: req.user.local.cprovincename,
				provinceEN: req.user.local.provinceEN
			},
			menu: usermenu,
			hmapkey: hmap,
			usercurloc: req.user.local.curloc
		});
	})
};



exports.ccadminsettings = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	if (req.user.local.cgroup === 'ecartadmin'
		|| req.user.local.cgroup === 'guest') {
		res.redirect('/admin');
		return;
	}
	//Ecadmin.	
	//    var  templ =find_templ(user_gr) ;
	var hmap = find_hmap_key(req.headers.host);
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	chk_access_menu(req.user.group, function (usermenu) {
		res.render('ccadmin_settings.ejs', {
			user: req.user,  // get the user out of session and pass to template
			baseUrl: req.protocol + '://' + req.get('host'),
			user_data: {
				id: req.user._id,
				_id_garage: req.user._id_garage,
				username: req.user.username,
				group: req.user.local.cgroup,
				email: req.user.local.cemail,
				name: req.user.local.cname,
				groupNameTH: req.user.local.cgroupname,
				provinceTH: req.user.local.cprovincename,
				provinceEN: req.user.local.provinceEN
			},
			menu: usermenu,
			hmapkey: hmap,
			usercurloc: req.user.local.curloc
		});
	})
};




exports.web = function (req, res) {
	res.render('index.ejs', {
		appversion: config.passengerappversion
	});
	// this exports.web is for xxx/passenger
	//res.sendfile('./public/app/passenger/index.html');
};




exports.calltaxi_lite = function (req, res) {
	res.render('calltaxi_lite.ejs', {
		appversion: config.passengerappversion,
		DistoShowPhone: config.DistoShowPhone,
		psgsearchpsgradian: config.psgsearchpsgradian,
		psgsearchpsgamount: config.psgsearchpsgamount
	});
};




exports.smartbus = function (req, res) {
	res.render('smartbus.ejs', {
		appversion: config.passengerappversion,
		DistoShowPhone: config.DistoShowPhone,
		psgsearchpsgradian: config.psgsearchpsgradian,
		psgsearchpsgamount: config.psgsearchpsgamount
	});
};




exports.viewalldrv = function (req, res) {
	res.render('viewalldrv.ejs', {
		appversion: config.passengerappversion,
		DistoShowPhone: config.DistoShowPhone,
		psgsearchpsgradian: 1000000,
		psgsearchpsgamount: 300
	});
};




exports.viewdrvpsg_lite = function (req, res) {
	res.render('viewdrvpsg_lite.ejs', {
		appversion: config.passengerappversion,
		DistoShowPhone: config.DistoShowPhone,
		psgsearchpsgradian: config.psgsearchpsgradian,
		psgsearchpsgamount: config.psgsearchpsgamount
	});
};




exports.calltaxi = function (req, res) {
	//console.log( ' calltaxi  = '+req.user )
	res.render('calltaxi.ejs', {
		appversion: config.passengerappversion,
		user: req.user  // get the user out of session and pass to template          		
	});
};




exports.psgprofile = function (req, res) {
	res.render('psgprofile.ejs', {
		appversion: config.passengerappversion,
		user: req.user  // get the user out of session and pass to template          		
	});
};




exports.jsontable = function (req, res) {
	res.render('jsontable.ejs', {});
};




exports.question = function (req, res) {
	res.render('question.ejs', {});
};




exports.download = function (req, res) {
	res.render('download.ejs', {});
};




exports.psgdownload = function (req, res) {
	res.render('psgdownload.ejs', {});
};




exports.drvdownload = function (req, res) {
	res.render('drvdownload.ejs', {});
};




exports.contactus = function (req, res) {
	res.render('contactus.ejs', {});
};




exports.profile = function (req, res) {
	chk_access_menu(req.user.group, function (usermenu) {
		res.render('profile.ejs', {
			user: req.user,  // get the user out of session and pass to template
			menu: usermenu
		});
	})
};



exports.adminV1 = function (req, res) {
	if (!req.user || req.user.local.cgroup !== 'ecartadmin') {
		res.redirect('./login');
		return;
	}

	chk_access_menu(req.user.group, function (usermenu) {
		//console.log(' chk_access_menu usermenu = ' + usermenu);
		res.render('admin.ejs', {
			user: req.user,  // get the user out of session and pass to template
			menu: usermenu
		});
	})
};



exports.admin = function (req, res) {

	if (!req.user) {
		res.redirect('/login');
		return;
	}

	if (req.user.local.cgroup === 'guest') {

		res.redirect('/admin/report');
		return;

	} else if (req.user.local.cgroup === 'ecartadmin') {

		var baseUrl = req.protocol + '://' + req.get('host');

		res.render('./admin_v2.ejs', {
			baseUrl: baseUrl,
			user_data: {
				id: req.user._id,
				name: req.user.local.cname,
				email: req.user.local.cemail,
				group: req.user.local.cgroup,
				username: req.user.username
			}
		});
	} else {
		res.redirect('./login');
		return;
	}
};



exports.adminviewgarage = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	chk_access_menu(req.user.group, function (usermenu) {
		res.render('adminviewgarage.ejs', {
			user: req.user,  // get the user out of session and pass to template
			menu: usermenu
		});
	})
};




exports.callcenter = function (req, res) {
	if (!req.user) {
		res.redirect('./login');
		return;
	}

	Lk_garageModel
		.findOne({ cgroup: req.user.local.cgroup })
		.exec(function (err, garage) {

			if (err) {
				res.redirect('./login');
				return;
			}

			if (!garage) {
				res.redirect('./admin');
				return;
			}

			var hmap = find_hmap_key(req.headers.host);
			var user_gr = req.user.group;
			chk_access_menu(req.user.group, function (usermenu) {
				res.render('callcenter.ejs', {
					user: req.user,
					garage: garage,
					user_data: {
						id: req.user._id,
						username: req.user.username,
						group: req.user.local.cgroup,
						email: req.user.local.cemail,
						name: req.user.local.cname,
						groupNameTH: req.user.local.cgroupname,
						provinceTH: req.user.local.cprovincename,
						provinceEN: req.user.local.provinceEN
					},
					menu: usermenu,
					hmapkey: hmap,
					usercurloc: req.user.local.curloc
				});
			});
		});
};




exports.autocall = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	Lk_garageModel
		.findOne({ cgroup: req.user.local.cgroup })
		.exec(function (err, garage) {

			if (err) {
				res.redirect('./login');
				return;
			}

			if (!garage) {
				res.redirect('./admin');
				return;
			}

			var hmap = find_hmap_key(req.headers.host);
			var user_gr = req.user.group;
			chk_access_menu(req.user.group, function (usermenu) {
				res.render('autocall.ejs', {
					user: req.user,
					garage: garage,
					user_data: {
						id: req.user._id,
						username: req.user.username,
						group: req.user.local.cgroup,
						email: req.user.local.cemail,
						name: req.user.local.cname,
						groupNameTH: req.user.local.cgroupname,
						provinceTH: req.user.local.cprovincename,
						provinceEN: req.user.local.provinceEN
					},
					menu: usermenu,
					hmapkey: hmap,
					usercurloc: req.user.local.curloc
				});
			});
		});
};




exports.adminEcartReport = function (req, res) {

	if (!req.user || req.user.local.cgroup !== 'ecartadmin') {

		res.redirect('./login');
		return;
	}

	if (req.user.local.cgroup != 'ecartadmin') {
		res.redirect('./login');
		return;
	}

	res.render('./admin/EcartReport.ejs', {
		user_data: {
			id: req.user._id,
			username: req.user.username,
			group: req.user.local.cgroup,
			email: req.user.local.cemail,
			name: req.user.local.cname
		}
	});
};




exports.adminReport = function (req, res) {

	if (!req.user) {
		res.redirect('./login');
		return;
	}

	if (req.user.local.cgroup === 'ecartadmin' ||
		req.user.local.cgroup === 'guest') {
		res.render('./admin/report.ejs', {
			user_data: {
				id: req.user._id,
				username: req.user.username,
				group: req.user.local.cgroup,
				email: req.user.local.cemail,
				name: req.user.local.cname
			}
		});
	} else {
		res.redirect('./login');
		return;
	}
};




exports.adminMonitoring = function (req, res) {
	if (!req.user || req.user.local.cgroup !== 'ecartadmin') {
		res.redirect('./login');
		return;
	}
	res.render('./admin/monitor.ejs', {
		user_data: {
			id: req.user._id,
			username: req.user.username,
			group: req.user.local.cgroup,
			email: req.user.local.cemail,
			name: req.user.local.cname
		},
	});
};



exports.jobCancelManager = function (req, res) {

	var baseUrl = req.protocol + '://' + req.get('host');

	if (!req.user) {
		res.redirect(baseUrl + '/login');
		return;
	}

	if (req.user.local.cgroup != 'ecartadmin') {
		res.redirect(baseUrl + '/login');
		return;
	}

	res.render('./admin_v2/job_cancel_manager.ejs', {
		baseUrl: baseUrl,
		user_data: {
			id: req.user._id,
			name: req.user.local.cname,
			email: req.user.local.cemail,
			group: req.user.local.cgroup,
			username: req.user.username
		}
	});
};



exports.driverList = function (req, res) {

	var baseUrl = req.protocol + '://' + req.get('host');

	if (!req.user) {
		res.redirect(baseUrl + '/login');
		return;
	}

	if (req.user.local.cgroup != 'ecartadmin') {
		res.redirect(baseUrl + '/login');
		return;
	}

	res.render('./admin_v2/driver_list.ejs', {
		baseUrl: baseUrl,
		user_data: {
			id: req.user._id,
			name: req.user.local.cname,
			email: req.user.local.cemail,
			group: req.user.local.cgroup,
			username: req.user.username
		}
	});
};



exports.changepwd = function (req, res) {
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	var umenu = chk_access_menu(user_gr);
	res.render('user/changepasswd.ejs', {
		user: req.user,  // get the user out of session and pass to template
		menu: umenu
	});
};



exports.template = function (req, res) {
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	find_templ(user_gr, null, function (endans) {
		var html = 'var  formData_ =' + JSON.stringify(endans) + ''
		res.setHeader('Content-Type', 'text/html');
		res.setHeader('Cache-Control', 'no-cache');
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.send(html);
	})
};




exports.templ_index = function (req, res) {
	res.render('templ/templ_index.ejs', {});
};




exports.templ_new = function (req, res) {
	res.render('templ/templ_new.ejs', {});
};




exports.templ_edit = function (req, res) {
	res.render('templ/templ_edit.ejs', {});
};




exports.templ_get = function (req, res) {
	var aid = req.query.aid; // '1' ;
	var templ = '';
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	if (typeof aid !== 'undefined') {
		find_templ(user_gr, aid, function (endans) {
			res.setHeader('Content-Type', 'text/html');
			res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
			res.setHeader('Expires', '-1');
			res.setHeader('Pragma', 'no-cache');
			res.send(endans);
		});
	} else {
		find_templ(user_gr, null, function (endans) {
			//var html = templ  ; //JSON.stringify(templ )  
			res.setHeader('Content-Type', 'text/html');
			res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
			res.setHeader('Expires', '-1');
			res.setHeader('Pragma', 'no-cache');
			res.send(endans);
		});
	}


};




exports.get_combofile = function (req, res) {
	//  var  templ =find_templ(req) ; 
	var aid = '1';
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	var html = [
		{ "id": 1, "combofile": "province", "combofilepath": "province.json" },
		{ "id": 2, "combofile": "amphoe", "combofilepath": "amphoe.json" },
		{ "id": 3, "combofile": "tambon", "combofilepath": "tambon.json" },
		{ "id": 4, "combofile": "village", "combofilepath": "village.json" }
	];
	res.setHeader('Content-Type', 'text/html');
	//res.setHeader('Access-Control-Allow-Origin', '*');
	res.send(html);
};



exports.list_folder_pins = function (req, res) {
	//  var  templ =find_templ(req) ;     
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	var html = ["taxi", "num", "pins"];
	res.setHeader('Content-Type', 'text/html');
	//res.setHeader('Access-Control-Allow-Origin', '*');
	res.send(html);
};




exports.list_pins = function (req, res) {
	//  var  templ =find_templ(req) ; 
	var aid = '1';
	var user_gr = req.user.group; // ["user"]   // req.user.group ;
	var html = ["2hand.png", "administrativeboundary.png"];
	res.setHeader('Content-Type', 'text/html');
	//res.setHeader('Access-Control-Allow-Origin', '*');
	res.send(html);
};



exports.getusergr = function (req, res) {
	G_usergroupModel.find(
		{
			//active:"Y"
		}, '',		// 	filter colum	
		{ limit: 500 },
		function (err, t_list) {
			//var html = JSON.stringify(t_list ) +'' 
			//res.setHeader('Content-Type', 'text/html');
			res.setHeader('Content-Type', 'application/json;charset=utf-8');
			//	res.setHeader('Access-Control-Allow-Origin', '*');
			res.send(JSON.stringify({
				//msg: "This is your driver's list.", 
				status: true,
				data: t_list
			}));
			//res.send(html );
		}
	);

};


////////////////////////////////////////////
exports.ListUser = function (req, res) {
	// amount = 400;
	// var col = req.body.req ;
	var templ = {};
	// for (i = 0; i < col.length; i++) { 
	// 	 templ[ col[i] ] = 1     		
	// 	}		
	UserModel.find(
		{
			//	 query		active:"Y"
		},
		templ,	// 	filter colum	
		{ limit: amount },
		function (err, drvlist) {
			// Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!
			if (drvlist == null) {
				//res.json({status: false, msg: "No data"});
				res.setHeader('Content-Type', 'application/json;charset=utf-8');
				res.setHeader('Access-Control-Allow-Origin', '*');
				res.send(JSON.stringify({
					status: false,
				}));
			} else {
				res.setHeader('Content-Type', 'application/json;charset=utf-8');
				res.setHeader('Access-Control-Allow-Origin', '*');
				res.send(JSON.stringify({
					//msg: "This is your driver's list.", 
					status: true,
					data: drvlist
				}));
				//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
			}
		}
	);
};



exports.ListPassenger = function (req, res) {
	var templ = {};
	PassengerModel.find(
		//{ active:"Y", status: "ON", curloc : { $near : curloc, $maxDistance: radian  }  },
		{

		}, templ,
		{ limit: amount },
		function (err, drvlist) {
			// Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!
			if (drvlist == null) {
				//res.json({status: false, msg: "No data"});
				res.setHeader('Content-Type', 'application/json;charset=utf-8');
				res.setHeader('Access-Control-Allow-Origin', '*');
				res.send(JSON.stringify({
					//msg: "No data", 
					status: false,
				}));
			} else {
				res.setHeader('Content-Type', 'application/json;charset=utf-8');
				res.setHeader('Access-Control-Allow-Origin', '*');
				res.send(JSON.stringify({
					//msg: "This is your driver's list.", 
					status: true,
					data: drvlist
				}));
				//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
			}
		}
	);
};



exports.ListPendingDrv = function (req, res) {	  // taxilistall 		
	templ_filter = {}
	var user_gr = req.user.group;
	var col = req.body.aid;
	var query;
	DriversModel.find(
		{
			status: "PENDING"	// aid: col 
		}, {
			_id: 1, device_id: 1, username: 1, pwdhint: 1, fname: 1, lname: 1, phone: 1, citizenid: 1, cgroup: 1, province: 1, prefixcarplate: 1, carplate: 1, carcolor: 1, status: 1, created: 1, imgface: 1, imgcar: 1, imglicence: 1	// templ_filter 
		},
		//{ limit : amount },
		function (err, drvlist) {
			if (drvlist == null) {
				res.json({ status: false, msg: "No data" });
				// res.setHeader('Content-Type', 'application/json;charset=utf-8');
				// res.setHeader('Access-Control-Allow-Origin', '*');
				// res.send(JSON.stringify({
				// 	msg: "No data", 
				// 	status: false,
				// }));
			} else {
				res.json({ status: true, data: drvlist });
				// res.setHeader('Content-Type', 'application/json;charset=utf-8');
				// res.setHeader('Access-Control-Allow-Origin', '*');
				// res.send(JSON.stringify({					
				// 	status: true, 										
				// 	data: drvlist
				// }));
				//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
			}
		}
	);
};



exports.ListDrv = function (req, res) {	  // taxilistall 	
	//console.log(req.query)	
	templ_filter = {}
	//var user_gr = req.user.group ;	
	var col = req.body.aid;
	var querycgroup = req.query.querycgroup;
	var myquery;
	if (querycgroup) {
		myquery = { cgroup: querycgroup };
	} else {
		myquery = {};
	}
	//console.log(myquery)
	DriversModel.find(
		myquery,
		{ _id: 1, device_id: 1, username: 1, pwdhint: 1, fname: 1, lname: 1, phone: 1, citizenid: 1, cgroup: 1, province: 1, prefixcarplate: 1, carplate: 1, carcolor: 1, status: 1, created: 1, imgface: 1, imgcar: 1, imglicence: 1 },
		//{ limit : amount },
		function (err, drvlist) {
			if (drvlist == null) {
				res.json({ status: false, msg: "No data" });
			} else {
				res.json({ status: true, data: drvlist })
			}
		}
	);
};




exports.update_main = function (req, res) {
	var gid = req.body.gid;
	var collectx = req.body.coll;
	var main = JSON.parse(req.body.main);
	//var user_ = main.username
	//console.log(user_) ;
	if (collectx == "drivers") {
		DriversModel.findOne(
			{ "_id": gid },
			function (err, psg) {
				if (psg == null) {
					//_passengerPassLogin (req, res);
					// CREATE Passenger *****************************************
					var newpsg = {}; // { "username":'xxx',"email":"eee"} ;
					for (var index in main) {
						newpsg[index] = main[index]
					}
					UserModel.create(newpsg
						, function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error" });
							} else {
								res.json({
									status: true, data: { _id: response._id }
								});
							}
						});
				} else {
					//psg.updated = new Date().getTime();	
					for (var index in main) {
						var attr = main[index];
						for (var psgindex in psg) {
							if (psgindex == index) {
								psg[psgindex] = attr;
							}
						}
					}
					psg.save(function (err, response) {
						if (err) {
							res.json({
								status: false,
								msg: "error",
								data: err
							});
						} else {
							Lk_garageModel.findOne(
								{ cgroup: main.cgroup },
								{ _id: 1, cgroupname: 1, cprovincename: 1 },
								function (err, response) {
									if (response == null) {

									} else {
										DriversModel.findOne(
											{ _id: gid },
											function (err, result) {
												if (result == null) {
													res.json({ status: false, msg: "This driver is not available." });
												} else {
													result.carplate_formal = result.prefixcarplate + ' ' + result.carplate;
													result._id_garage = response._id;
													result.cgroupname = response.cgroupname;
													result.cprovincename = response.cprovincename;
													result.save(function (err, response) {
														if (err) {
															//console.log (' err ' + err )
															res.json({
																status: false,
																msg: "OK drivers"
															});
														} else {
															//console.log (' update : ' + response.cgroupname + ' and '+ response.cprovincename )
															res.json({
																status: true,
																msg: "OK drivers"
															});
														}
													});
												}
											}
										)
									}
								}
							)

						}
					});
				}
			});
	} else if (collectx == "passengers") {
		PassengerModel.findOne(
			{ "_id": gid },
			function (err, psg) {
				if (psg == null) {
					var newpsg = {}; // { "username":'xxx',"email":"eee"} ;
					for (var index in main) {
						newpsg[index] = main[index]
					}
					PassengerModel.create(newpsg
						, function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error" });
							} else {
								res.json({
									status: true, data: { _id: response._id }
								});
							}
						}
					);
				} else {
					//psg.updated = new Date().getTime();	
					for (var index in main) {
						var attr = main[index];
						for (var psgindex in psg) {
							if (psgindex == index) {
								psg[psgindex] = attr;
							}
						}
					}
					psg.save(function (err, response) {
						if (err) {
							res.json({
								status: false, msg: "error", data: err
							});
						} else {
							res.json({ status: "OK passengers" });
						}
					});
				}
			}
		);
	} else if (collectx == "users") {
		UserModel.findOne(
			{ "_id": gid },
			function (err, psg) {
				if (psg == null) {
					var newpsg = {}; // { "username":'xxx',"email":"eee"} ;
					for (var index in main) {
						newpsg[index] = main[index]
					}
					UserModel.create(newpsg
						, function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error" });
							} else {
								res.json({
									//msg:  "Your account has been created.", 
									status: true,
									data: {
										_id: response._id,
										status: response.status
									}
								});
							}
						});
				} else {   //  update 
					for (var index in main) {
						var attr = main[index];
						for (var psgindex in psg) {
							if (psgindex == index) {
								psg[psgindex] = attr;
							}
						}
					}
					psg.save(function (err, response) {
						if (err) {
							res.json({
								action: false, msg: "error", data: err
							});
						} else {
							res.json({ action: "OK users" });
						}
					});
				}
			}
		);
	} else {
		res.json({
			status: false,
			msg: "error",
			data: err
		});
	}
};




exports._bgservice_autonotified = function (socket) {
	return function (req, res) {
		console.log('_bgservice_autonotified')
		console.log(req.body)
		console.log('_bgservice_autonotified')
		var automessage;
		var provincearea = req.body.provincearea;
		if (provincearea) {
			switch (provincearea) {
				case "ubonratchathani":
					cgroup = "ccubon";
					break;
				case "khonkaen":
					cgroup = "kknscgarage";
					break;
				case "chiangrai":
					cgroup = "cricritaxi";
					break;
				case "nakhonratchasima":
					cgroup = "nmataxidd";
					break;
				case "phuket":
					cgroup = "pktsahakorn";
					break;
				default:
					cgroup = req.body.cgroup;
					break;
			}
		} else {
			cgroup = req.body.cgroup;
		}

		drv_carplate = req.body.drv_carplate.replace("+", "");

		_bg_autonotified(drv_carplate, req.body.savephone, req.body.savecur, req.body.savedes, cgroup);

		function _bg_autonotified(drv_carplate, savephone, savecur, savedes, cgroup) {
			automessage = " เบอร์ " + savephone + "\nรับ " + savecur + "\nส่ง " + savedes;
			DriversModel.find(
				{ carplate: drv_carplate, cgroup: cgroup },
				function (err, drvlist) {
					if (drvlist == 0) {
						console.log('sss')
						res.status(200).json({ status: false });
					} else {
						console.log('ddd')
						for (j = 0; j < drvlist.length; j++) {
							console.log('drv_phone = ' + drvlist[j].phone)
							_bg_msghistory(drvlist[j].phone, drv_carplate, cgroup, savephone, savecur, savedes);
							console.log('automessage for bg service : ' + automessage + ' for ' + drvlist[j].phone)
							socket.emit('new message', {
								status: true,
								phone: drvlist[j].phone,
								message: automessage || '...'
							});
						}
						res.status(200).json({ status: true });
					}
				}
			);
		}

		function _bg_msghistory(phone, plate, cgroup, savephone, savecur, savedes) {
			MsghistoryModel.create({
				drv_phone: phone,
				plate: plate,
				cgroup: cgroup,
				phone: savephone,
				cur: savecur,
				des: savedes
			}, function (err, doc) {
				if (err) { console.log(err); }
			});
		}
	}
}




exports.getPassengerHistoryByPhone = function (req, res) {
	try {
		CallcenterpsgModel.find({ cgroup: req.user.local.cgroup, phone: req.body.phone }).select('cccomment curaddr destination created').limit(5).sort({ created: -1 }).exec(function (err, result) {
			if (err) res.json(err);
			if (!err) res.json({ status: true, data: result });
		});
	} catch (exception) {
		console.log(exception);
		res.json({ status: false });
	}
}




exports.notificationManagement = function(req, res) {

	if(!req.user) {
		res.redirect('/login');
		return;
	}

	if (req.user.local.cgroup === 'guest') {

		res.redirect('/admin/report');
		return;

	} else if(req.user.local.cgroup === 'ecartadmin') {

		var baseUrl = req.protocol + '://' + req.get('host');
		
		res.render('./admin_v2/notification_management.ejs', {
			git: {
				branch: git.branch(),
				short: git.short(),
				long: git.long()
			},
			baseUrl: baseUrl,
			user_data : {
				id: req.user._id,
				name: req.user.local.cname,
				email: req.user.local.cemail,
				group: req.user.local.cgroup,
				username: req.user.username
			}
		});
	} else {
		res.redirect('./login');
		return;
	}
}