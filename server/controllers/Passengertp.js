////////////////////////////////////
// Taxi-Beam Passenger API Passenger
// version : 1.0.22
// Date November 3, 2016
// Created by Hanzen
////////////////////////////////////
var config = require('../../config/func').production;
var lang = 'en';
var Url = require('url');
var mongoose = require('mongoose');
var PassengerModel = mongoose.model('PassengerModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var JoblistModel = mongoose.model('JoblistModel');
var UserModel = mongoose.model('UserModel');
var PathLogModel = mongoose.model('PathLogModel');
var DriversModel = mongoose.model('DriversModel');
var CommentModel = mongoose.model('CommentModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var EmgListModel = mongoose.model('EmgListModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');
var HistoryModel = mongoose.model('HistoryModel');
var ShifttransactionModel = mongoose.model('ShifttransactionModel');
var ShareActivitiesModel = mongoose.model('ShareActivitiesModel');
var ShareActivitiesLogModel = mongoose.model('ShareActivitiesLogModel');
var WallettransactionModel = mongoose.model('WallettransactionModel');
// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var qs = require('querystring');
var ios = require('socket.io');

// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}



function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}



function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);

		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};



function _pathLog(data, res) {
	//console.log(data);
	//data.timestamp = data.timestamp
	PathLogModel.create(data)
}



function _getErrDetail(lang, errCode, callback) {
	ErrorcodeModel.findOne(
		{ errcode: errCode },
		function (err, response) {
			if (response == null) {
				callback('');
			} else {
				callback(response._doc["err" + (lang ? lang : "en")] || response._doc["erren"]);
			}
		}
	);
};



function _joblistcreate(job_id, jobtype, drvarroundlist, req) {
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		function (err, response) {
			if (response == null) {
				var ProvinceID = 0;
				var NameEN = "undefined";
				var NameTH = "undefined";
				var ZoneID = 0;
				_do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID);
			} else {
				var ProvinceID = response.ProvinceID;
				var NameEN = response.NameEN;
				var NameTH = response.NameTH;
				var ZoneID = response.ZoneID;
				_do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID);
			}
		}
	);

	function _do_joblistcreate(ProvinceID, NameEN, NameTH, ZoneID) {
		PassengerModel.findOne(
			{ device_id: req.body.device_id },
			function (err, psg) {
				if (psg == null) {
					console.log('_function_joblistcreate no device_id');
				} else {
					JoblistModel.create(
						{
							job_id: job_id,
							displayName: psg.displayName,
							psg_id: psg._id,
							psg_device_id: req.body.device_id,
							psg_phone: req.body.phone,
							curaddr: req.body.curaddr,
							curlng: req.body.curlng,
							curlat: req.body.curlat,
							curloc: [parseFloat(req.body.curlng), parseFloat(req.body.curlat)],
							destination: req.body.destination,
							deslng: req.body.deslng,
							deslat: req.body.deslat,
							tips: req.body.tips,
							detail: req.body.detail,
							datepsgcall: new Date().getTime(),
							createdvia: req.body.createdvia,
							favcartype: req.body.favcartype,
							drvarroundlist: drvarroundlist,
							status: "ON",
							jobtype: jobtype,
							ProvinceID: ProvinceID,
							NameEN: NameEN,
							NameTH: NameTH,
							ZoneID: ZoneID
						},
						function (err, record) {
							if (err) {
								console.log(err)
							} else {
								console.log(' success created job ')
							}
						}
					);
				}
			}
		);
	}
}



function _joblistupdate(job_id, req, action, cwhere) {
	JoblistModel.findOne(
		{ job_id: job_id },
		function (err, job) {
			if (job == null) {
				console.log(' no job id')
			} else {
				switch (action) {
					case "datepsgaccept":
						job.status = "BUSY";
						job.datepsgaccept = new Date().getTime();
						break;
					case "datepsgthanks":
						job.status = "THANKS";
						job.datepsgthanks = new Date().getTime();
						break;
					case "datepsgcancel":
						job.status = "PSG_CANCELLED";
						job.datepsgcancel = new Date().getTime();
						job.psgcancelwhere = cwhere;
						break;
				}
				job.save(function (err, response) {
					if (err) {
						console.log(err)
					} else {
						console.log(' update job list ')
					}
				});
			}
		}
	);
}


function _passengerAutoLogin(req, res) {
	device_id = req.body.device_id;
	_id = req.body._id;
	PassengerModel.findOne(
		{ device_id: device_id },
		function (err, psg) {
			if (psg == null) {
				// This phone(IMEI) have never been in the system, send to registration page.
				res.json({
					status: false,
					target: "REGISTRATION"
				});
			} else {
				if (psg._id == _id) {
					psg.status = "OFF";
					psg.updated = new Date().getTime();
					psg.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							// device_id: Y, _id: Y =>  Welcome to Taxi-Beam ]
							res.json({
								status: true,
								data: {
									status: psg.status
								}
							});
						}
					});
				} else {
					// in case of change mobile owner, send to update page to update driver info
					res.json({
						status: false,
						target: "BLANKPROFILE"
					});
				}
			}
		}
	);
};




function _passengerPassLogin(req, res) {
	device_id = req.body.device_id;
	_id = req.body._id;
	PassengerModel.findOne(
		{ device_id: device_id },
		function (err, psg) {
			if (psg == null) {
				// This phone(IMEI) have never been in the system, send to registration page.
				res.json({
					status: false,
					target: "REGISTRATION"
				});
			} else {
				if (psg._id == _id) {
					psg.status = "OFF";
					psg.updated = new Date().getTime();
					psg.save(function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							// device_id: Y, _id: Y =>  Welcome to Taxi-Beam ]
							res.json({
								status: true,
								data: {
									status: psg.status
								}
							});
						}
					});
				} else {
					// in case of change mobile owner, send to update page to update driver info
					res.json({
						status: false,
						target: "BLANKPROFILE"
					});
				}
			}
		}
	);
};




// start ::: Passenger API Mobile Version
exports.passengerAutoLogin = function (req, res) {
	_passengerAutoLogin(req, res);
};




exports.passengerloadconfig = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var appversion = req.body.version;
	var platform = req.body.platform;	// [ ANDROID, iOS ]
	var mobiledata = req.body.model;	// driver's mobile data info
	var myquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}	

	if (req.url.substring(1, 7) == 'smiles') {
		myquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		myquery = { device_id: device_id, cgroup: 'beam' };
	}

	PassengerModel.update(
		myquery,
		{
			$set:
			{
				appversion: appversion,
				platform: platform,
				mobiledata: mobiledata
			}
		},
		{ new: true },
		function (err, tank) {
			if (err) {
				res.json({
					status: false,
					msg: err
				});
			} else {
				res.json({
					status: true,
					data: {
						available_country: config.available_country,
						distoShowPhone: config.distoShowPhone,
						psgSearchDrvInterval: config.psgSearchDrvInterval,
						psgGetstatusInterval: config.psgGetstatusInterval
					}
				});
			}
		}
	);
}




exports.passengerRegister = function (req, res) {
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var psgquery;
	var cgroup;
	
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
		cgroup = 'smiles';
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
		cgroup = 'beam';
	}
	
	PassengerModel.findOne(
		psgquery,
		function (err, psg) {
			if (psg == null) {
				PassengerModel.create({
					device_id: device_id,
					displayName: req.body.displayName,
					email: req.body.email,
					phone: req.body.phone,
					createdvia: "APP",
					cgroup: cgroup
				},
					function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							res.json({
								//msg:  "Your account has been created.", 
								status: true,
								data: {
									_id: response._id,
									msg: "register success",
									status: response.status
								}
							});
						}
					}
				);
			} else {
				// duplicated IMEI/App ID => updated the current data for device_id.	
				psg.displayName = req.body.displayName;
				psg.email = req.body.email;
				psg.phone = req.body.phone;
				psg.curaddr = "";
				psg.curlng = null;
				psg.curlat = null;
				psg.curloc = [];
				psg.destination = "";
				psg.des_dist = "";
				psg.deslng = null;
				psg.deslat = null;
				psg.desloc = [];
				psg.tips = "";
				psg.detail = "";
				psg.favcartype = [];
				psg.favdrv = [];
				psg.drv_id = "";
				psg.status = "OFF";
				psg.createdvia = "APP";
				psg.created = new Date().getTime();
				psg.updated = new Date().getTime();
				psg.cgroup = cgroup;
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							//msg:  "Your account has been updated." , 
							status: true,
							msg: "",
							data: {
								_id: response._id,
								msg: "re-register success",
								status: response.status
							}
						});
					}
				});
			}
		}
	);
};




exports.passengerUpdateProfile = function (req, res) {
	var _id = req.body._id;
	//var device_id = req.body.device_id	;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	//var curloc 	= [parseFloat(req.body.curlng),parseFloat(req.body.curlat)];
	var psgquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };		
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
	}

	PassengerModel.findOne(
		psgquery,
		{ device_id: 1, status: 1 },
		function (err, psg) {
			if (psg == null) {
				_passengerAutoLogin(req, res);
			} else {
				if (req.body.email) { psg.email = req.body.email ? req.body.email : psg.email; }
				if (req.body.phone) { psg.phone = req.body.phone ? req.body.phone : psg.phone; }
				psg.updated = new Date().getTime();
				psg.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							//msg: "success, passenger profile updated",
							status: true
						});
					}
				});
			}
		}
	);
};




exports.passengerGetStatus = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var reallng = req.body.curlng;
	var reallat = req.body.curlat;
	var realloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	var platform = req.body.platform;	// [ ANDROID, iOS ]
	var psgquery;
	var checkSmilesAPP;
	var cgroup;
	var appversion;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (typeof reallng == 'undefined' && reallng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}

	if (typeof reallat == 'undefined' && reallat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
		cgroup = "smiles";
		checkSmilesAPP = true;
		if (platform == "ANDROID"){
			appversion = config.psgsmilesappversion_android;
		} else {
			appversion = config.psgsmilesappversion_ios;
		}
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
		cgroup = "beam";
		if (platform == "ANDROID"){
			appversion = config.passengerappversion_android;
		} else {
			appversion = config.passengerappversion_ios;
		}		
	}

	PassengerModel.findOne(
		psgquery,
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, reallat: 1, reallng: 1, realloc: 1, createdvia: 1, job_id: 1 },
		function (err, psg) {
			if (psg == null) {
				PassengerModel.create({
					device_id: device_id,
					displayName: req.body.displayName,
					email: req.body.email,
					phone: req.body.phone,
					reallat: req.body.reallat,
					reallng: req.body.reallng,
					updated: new Date().getTime(),
					cgroup: cgroup,
					createdvia: "WEBAPP"
				},
					function (err, response) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							res.json({
								//msg:  "Your account has been created.", 
								status: true,
								data: {
									_id: response._id,
									msg: "register success",
									status: response.status,
									appversion: checkSmilesAPP ? platform=="ANDROID" ? config.psgsmilesappversion_android : config.psgsmilesappversion_ios : platform=="ANDROID" ? config.passengerappversion_android : config.passengerappversion_ios
								}
							});
						}
					}
				);
			} else {
				//Set the two dates
				var currentTime = new Date()
				var currDate = currentTime.getMonth() + 1 + "/" + currentTime.getDate() + "/" + currentTime.getFullYear() //Todays Date - implement your own date here.
				var iniPastedDate = psg.updated // "8/7/2012" //PassedDate - Implement your own date here.

				//currDate = 8/17/12 and iniPastedDate = 8/7/12

				function DateDiff(date1, date2) {
					var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
					return (datediff / (24 * 60 * 60 * 1000)); //Convert values to -/+ days and return value      
				}

				//Write out the returning value should be using this example equal -10 which means 
				//it has passed by ten days. If its positive the date is coming +10.    
				//console.log (DateDiff(new Date(iniPastedDate),new Date(currDate))); //Print the results...
				if (reallng) { psg.reallng = reallng ? reallng : psg.reallng; }
				if (reallat) { psg.reallat = reallat ? reallat : psg.reallat; }
				if (realloc) { psg.realloc = realloc ? realloc : psg.realloc; }
				psg.updated = new Date().getTime();
				psg.save(function (err, result) {
					if (err) {
						res.json({
							status: false,
							msg: "error"
						});
					} else {
						res.json({
							status: true,
							data: {
								_id: result._id,
								createdvia: result.createdvia,
								status: result.status,
								drv_id: result.drv_id,
								job_id: result.job_id,
								phone: result.phone,
								device_id: result.device_id,
								curlat: result.curlat,
								curlng: result.curlng,
								curloc: result.curloc,
								reallat: result.reallat,
								reallng: result.reallng,
								realloc: result.realloc,																
								appversion: checkSmilesAPP ? platform=="ANDROID" ? config.psgsmilesappversion_android : config.psgsmilesappversion_ios : platform=="ANDROID" ? config.passengerappversion_android : config.passengerappversion_ios
							}
						});
					}
				});
			}
		}
	);
};




exports.passengerGetByID = function (req, res) {
	var _id = req.body._id;
	var lang = req.body.lang ? req.body.lang : "en";

	if (typeof _id == 'undefined' && _id == null) {
		res.json({ status: false, msg: "id is not valid" })
		return;
	}

	PassengerModel.findOne(
		{ _id: _id },
		{ _id: 1, email: 1, phone: 1, curaddr: 1, curlng: 1, curlat: 1, curloc: 1, destination: 1, des_dist: 1, deslng: 1, deslat: 1, tips: 1, detail: 1, favcartype: 1, favdrv: 1, drv_id: 1, status: 1, cgroup: 1 },
		function (err, psg) {
			if (psg == null) {
				//res.json({ status: false , msg: "There is some data missing, please try again."});
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail(lang, 'err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				if (err) {
					res.json({ status: false, msg: "error", data: err });
				} else {
					res.json({
						status: true,
						msg: "Passenger detail",
						data: {
							email: psg.email,
							phone: psg.phone,
							curaddr: psg.curaddr,
							curlng: psg.curlng,
							curlat: psg.curlat,
							destination: psg.destination,
							des_dist: psg.des_dist,
							deslng: psg.deslng,
							deslat: psg.deslat,
							tips: psg.tips,
							detail: psg.detail,
							favcartype: psg.favcartype,
							favdrv: psg.favdrv,
							drv_id: psg.drv_id,
							status: psg.status
						}
					});
				}
			}
		}
	);
};




exports.passengerSearchDrv = function (req, res) {
	var device_id = req.body.device_id;
	var favcartype = req.body.favcartype;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var radian = req.body.radian;
	var amount = req.body.amount;
	var myquery;
	// short form : if(req.body.uid) { device.uid = req.body.uid ? req.body.uid : device.uid; }		
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}

	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (Array.isArray(favcartype)) {
		favcartype = req.body.favcartype;
	} else {
		favcartype = ['car', 'minivan'];
		//favcartype = [req.body.favcartype];
	}

	if (typeof radian == 'undefined' && radian == null) {
		radian = config.psgsearchpsgradian;
	}

	if (typeof amount == 'undefined' && amount == null) {
		amount = config.psgsearchpsgamount;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		myquery = {
			_id_garage: '597b0e04dc04ea092db44a7c',
			status: "ON",
			active: "Y",
			curlng: { $ne: null },
			curlat: { $ne: null },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: radian
				}
			},
			cartype: { $in: favcartype }
		};
	} else {
		myquery = {
			//_id_garage: { $ne:'597b0e04dc04ea092db44a7c'},
			status: "ON",
			active: "Y",
			curlng: { $ne: null },
			curlat: { $ne: null },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: radian
				}
			},
			cartype: { $in: favcartype }
		};
	}

	// ไม่สามารถให้เช็ค device_id ได้ เพราะไม่อย่างนั้น ในกรณีของ webapp ที่เปิดมาครั้งแรกยังไม่มีการสร้าง device_id จะไม่สามารถเห็นรถได้
	// PassengerModel.findOne(
	// 	{ device_id : device_id }, 
	// 	function(err, psg) {
	// 		if (psg == null) { 
	// 			res.json({status: false, msg: "No psg device_id"});		
	//     	} else {	    		
	//{ active:"Y", status: "ON", curloc : { $near : curloc, $maxDistance: radian  }  },
	//curloc : { $near : curloc, $maxDistance: radian },   => for 2d index => not working
	// do not forget  => db.passengers.createIndex( { curloc : "2dsphere" } )
	DriversModel.find(
		myquery,
		{ fname: 1, lname: 1, phone: 1, carplate: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, status: 1, allowpsgcontact: 1, carreturn: 1, carreturnwhere: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
		{ limit: amount },
		function (err, drvlist) {
			// Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!
			if (drvlist == null) {
				res.json({ status: false, msg: "No data" });
			} else {
				res.json({
					status: true,
					data: drvlist
				});
				//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
			}
		}
	);
	// 		}
	// 	}
	// );
};




exports.passengerSearchDrvView = function (req, res) {
	var device_id = req.body.device_id;
	var favcartype = req.body.favcartype;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var radian = req.body.radian;
	var amount = req.body.amount;
	var myquery;

	// short form : if(req.body.uid) { device.uid = req.body.uid ? req.body.uid : device.uid; }		
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "current longitude is not valid" })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "current latitude is not valid" })
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (Array.isArray(favcartype)) {
		favcartype = req.body.favcartype;
	} else {
		favcartype = ['car', 'minivan'];
		//favcartype = [req.body.favcartype];
	}

	if (req.url.substring(1, 7) == 'smiles') {
		myquery = {
			_id_garage: '597b0e04dc04ea092db44a7c',
			status: "ON",
			active: "Y",
			curlng: { $ne: null },
			curlat: { $ne: null },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: 1000000
				}
			},
			cartype: { $in: favcartype }
		};
	} else {
		myquery = {
			//_id_garage: { $ne:'597b0e04dc04ea092db44a7c'},
			status: "ON",
			active: "Y",
			curlng: { $ne: null },
			curlat: { $ne: null },
			curloc: {
				$near: {
					$geometry: {
						type: "Point",
						coordinates: curloc
					},
					$maxDistance: 1000000
				}
			},
			cartype: { $in: favcartype }
		};
	}

	DriversModel.find(
		myquery,
		{ fname: 1, lname: 1, phone: 1, carplate: 1, prefixcarplate: 1, cartype: 1, carcolor: 1, imgface: 1, curlng: 1, curlat: 1, curloc: 1, status: 1, allowpsgcontact: 1, carreturn: 1, carreturnwhere: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
		{ limit: amount },
		function (err, drvlist) {
			// Donot forget to create 2d Index for passengers collection : curloc & descloc!!!!
			if (drvlist == null) {
				res.json({ status: false, msg: "No data" });
			} else {
				res.json({
					status: true,
					data: drvlist
				});
				//Specifies a point for which a geospatial query returns the documents from nearest to farthest.
			}
		}
	);
};



exports.passengerCallDrv = function (socket) {
	return function (req, res) {
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var phone = req.body.phone;
		var curlng = req.body.curlng;
		var curlat = req.body.curlat;
		var favcartype = req.body.favcartype;
		var radian = req.body.radian;
		var tips = req.body.tips;
		var job_id = new Date().getTime() + '-' + ranSMS();
		var jobtype = "APPON";
		var myquery;
		var psgquery;

		if (typeof device_id == 'undefined' && device_id == null) {
			res.json({ status: false, msg: "device id is not valid" })
			return;
		}

		if (typeof curlng == 'undefined' && curlng == null) {
			res.json({ status: false, msg: "current longitude is not valid" })
			return;
		}
		if (typeof curlat == 'undefined' && curlat == null) {
			res.json({ status: false, msg: "current latitude is not valid" })
			return;
		}
		if (curlng == 0) {
			res.json({ status: false, msg: "current longitude is not valid" })
			return;
		}
		if (curlat == 0) {
			res.json({ status: false, msg: "current latitude is not valid" })
			return;
		}

		var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

		if (Array.isArray(favcartype)) {
			favcartype = req.body.favcartype;
		} else {
			favcartype = ['car', 'minivan'];
			//favcartype = [req.body.favcartype];
		}

		if (req.url.substring(1, 7) == 'smiles') {
			cgroup = "smiles";
			psgquery = { device_id: device_id, cgroup: 'smiles' };
			myquery = {
				_id_garage: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c'),
				status: "ON",
				active: "Y",
				cartype: { $in: favcartype }
			};
		} else {
			cgroup = "beam";
			psgquery = { device_id: device_id, cgroup: 'beam' };
			myquery = {
				//_id_garage: { $ne:'597b0e04dc04ea092db44a7c'},
				status: "ON",
				active: "Y",
				cartype: { $in: favcartype }
			};
		}

		Lk_provincesModel.findOne(
			{
				polygons:
				{
					$geoIntersects:
					{
						$geometry:
						{
							"type": "Point",
							"coordinates": curloc
						}
					}
				}
			},
			function (err, response) {
				if (response == null) {
					searchpsgamount = config.psgsearchpsgamount;
					searchpsgradian = config.psgsearchpsgradian;
					findPsginZone(searchpsgradian, searchpsgamount);
				} else {
					searchpsgamount = response.searchpsgamount;
					searchpsgradian = response.searchpsgradian;
					findPsginZone(searchpsgradian, searchpsgamount);
				}
			}
		);

		function findPsginZone(searchpsgradian, searchpsgamount) {
			// DriversModel.find(
			// 	{ 						
			// 		status: "ON", 
			// 		active:"Y",
			// 		curlng: { $ne: null },
			// 		curlat: { $ne: null },
			// 		curloc:  { 
			// 			$near: {
			// 				$geometry: {
			// 					type: "Point" ,
			// 					coordinates: curloc
			// 				} , 
			// 				$maxDistance: searchpsgradian
			// 			}
			// 		},			
			// 		cartype: { $in:  favcartype  } 			
			// 	},		
			DriversModel.aggregate(
				[
					{
						$geoNear: {
							query: myquery,
							near: { type: "Point", coordinates: curloc },
							distanceField: "dist",
							maxDistance: searchpsgradian,
							num: searchpsgamount,
							spherical: true
						}
					},
					{
						$project: {
							_id: 1,
							dist: 1,
							dist_km: {
								$divide: [
									{
										$subtract: [
											{ $multiply: ['$dist', 1] },
											{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
										]
									}, 1000
								]
							},
						},
					},
					{ $sort: { dist: 1 } }
				],
				function (err, drvlist) {
					//console.log(drvlist)
					// if(drvlist == null) {
					// 	PassengerModel.findOne(
					// 		{ device_id: device_id }, 
					// 		{ device_id:1, job_id:1, curaddr:1, curloc:1, destination:1, desloc:1, des_dist:1, tips:1, detail:1, favcartype:1, status:1, updated:1, createdjob: 1 },
					// 		function(err, psg) {			
					// 			if(psg == null) {
					// 				res.json({
					// 					status: false , 
					// 					msg: "no psg id"
					// 				});			
					// 			} else {		    																			
					// 				psg.status = "NOCARNEARBY";
					// 				psg.updated = new Date().getTime();								
					// 				psg.save(function(err, response) {
					// 					if (err) {
					// 						res.json({ 
					// 							status: false , 
					// 							msg: "error", 
					// 							data: err							
					// 						});
					// 					} else {										
					// 						res.json({status: true, msg: "ขออภัยค่ะ ขณะนี้ไม่มีแท็กซี่ว่างในบริเวณใกล้เคียง"});
					// 					}
					// 				});
					// 			}
					// 		}
					// 	);		
					// } else {	
					PassengerModel.findOne(
						psgquery,
						{ device_id: 1, job_id: 1, curaddr: 1, curloc: 1, destination: 1, desloc: 1, des_dist: 1, tips: 1, detail: 1, favcartype: 1, status: 1, updated: 1, createdjob: 1 },
						function (err, psg) {
							if (psg == null) {
								PassengerModel.create({
									device_id: device_id,
									phone: req.body.phone,
									createdvia: "SERVICES",
									cgroup: cgroup,
									status: "OFF"
								},
									function (err, psg) {
										if (err) {
											res.json({ status: false, msg: "error", data: err });
										} else {
											psg.phone = req.body.phone ? req.body.phone : psg.phone;
											psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr;
											psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng;
											psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat;
											psg.curloc = curloc ? curloc : psg.curloc;
											psg.favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype;
											psg.destination = req.body.destination ? req.body.destination : psg.destination;
											psg.tips = req.body.tips ? req.body.tips : 0;
											psg.detail = req.body.detail ? req.body.detail : psg.detail;
											psg.createdvia = req.body.createdvia;
											psg.job_id = job_id;
											psg.status = "ON";
											psg.drvarroundlist = drvlist;
											psg.updated = new Date().getTime();
											psg.createdjob = new Date().getTime();
											psg.occupied = false;
											psg.save(function (err, response) {
												if (err) {
													res.json({
														status: false,
														msg: "error",
														data: err
													});
												} else {
													_joblistcreate(job_id, jobtype, drvlist, req);
													if (Array.isArray(drvlist)) {
														response.drvarroundlist.forEach(function (driver, index) {
															socket.to(driver._id.toString()).emit('WAKEUP_DEVICE', { title: 'มีงานจากผู้โดยสารเรียก', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
														});
													}
													res.json({
														status: true,
														msg: "create new a passenger",
														data: response
													});
												}
											});
										}
									});
							} else {
								psg.phone = req.body.phone ? req.body.phone : psg.phone;
								psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr;
								psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng;
								psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat;
								psg.curloc = curloc ? curloc : psg.curloc;
								if (typeof req.body.favcartype == 'undefined') {
									psg.favcartype = [];
								} else {
									psg.favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype;
								}
								psg.destination = req.body.destination ? req.body.destination : psg.destination;
								psg.tips = req.body.tips ? req.body.tips : 0;
								psg.detail = req.body.detail ? req.body.detail : psg.detail;
								psg.createdvia = req.body.createdvia;
								psg.job_id = job_id;
								psg.status = "ON";
								psg.occupied = false;
								psg.drvarroundlist = drvlist;
								psg.updated = new Date().getTime();
								psg.createdjob = new Date().getTime();
								psg.save(function (err, response) {
									if (err) {
										res.json({
											status: false,
											msg: "error",
											data: err
										});
									} else {
										_joblistcreate(job_id, jobtype, drvlist, req);
										if (Array.isArray(drvlist)) {
											response.drvarroundlist.forEach(function (driver, index) {
												socket.to(driver._id.toString()).emit('WAKEUP_DEVICE', { title: 'มีงานจากผู้โดยสารเรียก', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
											});
										}
										res.json({
											status: true,
											msg: "update previouse a passenger",
											data: response
										});
									}
								});
							}
						}
					);
					//}
				}
			);
		}
	}
};




exports.passengerReCall = function (socket) {
	return function (req, res) {
		console.log( ' passengerReCall ')	
		console.log(req.body)
		var _id = req.body._id;
		var device_id = req.body.device_id;	// Lite version use phone as a unique key
		var job_id = new Date().getTime() + '-' + ranSMS();
		var psgquery;

		if (req.url.substring(1, 7) == 'smiles') {
			psgquery = { device_id: device_id, cgroup: 'smiles' };
		} else {
			psgquery = { device_id: device_id, cgroup: 'beam' };
		}

		PassengerModel.findOne(
			psgquery,
			function (err, psg) {
				if (psg == null) {
					res.json({
						status: false,
						msg: "error",
						data: err
					});
				} else {
					if (psg.cgroup == 'smiles') {
						cgroup = "smiles";
						psgquery = { device_id: device_id, cgroup: 'smiles' };
						myquery = {
							_id_garage: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c'),
							status: "ON",
							active: "Y",
							cartype: { $in: psg.favcartype }
						};
					} else {
						cgroup = "beam";
						psgquery = { device_id: device_id, cgroup: 'beam' };
						myquery = {
							status: "ON",
							active: "Y",
							cartype: { $in: psg.favcartype }
						};
					}
			
					Lk_provincesModel.findOne(
						{
							polygons:
							{
								$geoIntersects:
								{
									$geometry:
									{
										"type": "Point",
										"coordinates": psg.curloc
									}
								}
							}
						},
						function (err, response) {
							if (response == null) {
								searchpsgamount = config.psgsearchpsgamount;
								searchpsgradian = config.psgsearchpsgradian;
								findPsginZone(searchpsgradian, searchpsgamount);
								ProvinceID = '';
								NameEN = '';
								NameTH = '';
								ZoneID = '';
							} else {
								searchpsgamount = response.searchpsgamount;
								searchpsgradian = response.searchpsgradian;
								findPsginZone(searchpsgradian, searchpsgamount);
								ProvinceID = response.ProvinceID;
								NameEN = response.NameEN;
								NameTH = response.NameTH;
								ZoneID = response.ZoneID;

							}
						}
					);
			
					function findPsginZone(searchpsgradian, searchpsgamount) {
						DriversModel.aggregate(
							[
								{
									$geoNear: {
										query: myquery,
										near: { type: "Point", coordinates: psg.curloc },
										distanceField: "dist",
										maxDistance: searchpsgradian,
										num: searchpsgamount,
										spherical: true
									}
								},
								{
									$project: {
										_id: 1,
										dist: 1,
										dist_km: {
											$divide: [
												{
													$subtract: [
														{ $multiply: ['$dist', 1] },
														{ $mod: [{ $multiply: ['$dist', 1] }, 10] }
													]
												}, 1000
											]
										},
									},
								},
								{ $sort: { dist: 1 } }
							],
							function (err, drvlist) {
								psg.job_id = job_id;
								psg.status = "ON";
								psg.occupied = false;
								psg.drvarroundlist = drvlist;
								psg.updated = new Date().getTime();
								psg.createdjob = new Date().getTime();
								psg.save(function (err, result) {
									if (err) {
										res.json({
											status: false,
											msg: "error",
											data: err
										});
									} else {
										JoblistModel.create({
											job_id: job_id,
											displayName: psg.displayName,
											psg_id: psg._id,
											psg_device_id: psg.device_id,
											psg_phone: psg.phone,
											curaddr: psg.curaddr,
											curlng: psg.curlng,
											curlat: psg.curlat,
											curloc: [parseFloat(psg.curlng), parseFloat(psg.curlat)],
											destination: psg.destination,
											deslng: psg.deslng,
											deslat: psg.deslat,
											tips: psg.tips,
											detail: psg.detail,
											datepsgcall: new Date().getTime(),
											drvarroundlist: drvlist,
											status: "ON",
											favcartype: psg.favcartype,
											createdvia: psg.createdvia,
											jobtype: "APPON",
											ProvinceID: ProvinceID,
											NameEN: NameEN,
											NameTH: NameTH,
											ZoneID: ZoneID							
										},
											function (err, psgrecall) {
												if (err) {
													console.log(err)
												} else {
													if (Array.isArray(drvlist)) {
														drvlist.forEach(function (driver, index) {
															socket.to(driver._id.toString()).emit('WAKEUP_DEVICE', { title: 'มีงานจากผู้โดยสารเรียก', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
														});
													}
													res.json({
														status: true,
														msg: "passenger recall",
														data: psgrecall
													});
												}
											}
										);
									}
								});
							}
						);
					}
				}
			}
		);
	}
};




exports.passengerAcceptDrv = function (socket) {
	return function (req, res) {
		var lang = req.body.lang ? req.body.lang : "en";
		var _id = req.body._id;
		var device_id = req.body.device_id;	// Lite version use phone as a unique key
		var drv_id = req.body.drv_id;
		var psgquery;

		if (typeof device_id == 'undefined' && device_id == null) {
			res.json({ status: false, msg: "device id is not valid" })
			return;
		}

		if (req.url.substring(1, 7) == 'smiles') {
			psgquery = { device_id: device_id, cgroup: 'smiles' };
		} else {
			psgquery = { device_id: device_id, cgroup: 'beam' };
		}

		PassengerModel.findOne(
			psgquery,
			{ device_id: 1, status: 1, job_id: 1 },
			function (err, psg) {
				if (psg == null) {
					_passengerAutoLogin(req, res);
				} else {
					if (psg.status == "WAIT") {
						if (req.body.drv_id) { psg.drv_id = req.body.drv_id ? req.body.drv_id : psg.drv_id; }
						psg.updated = new Date().getTime();
						psg.status = "BUSY";
						psg.save(function (err, result) {
							if (err) {
								res.json({ status: false, msg: "error" });
							} else {
								_joblistupdate(psg.job_id, req, 'datepsgaccept', '');
								DriversModel.findOne(
									{ _id: drv_id },
									{ status: 1, updated: 1, _id: 1, device_id: 1, psg_id: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
									function (err, drv) {
										if (drv == null) {
											_getErrDetail(lang, 'err009', function (errorDetail) {
												res.json({
													status: false,
													msg: errorDetail,
												});
											});
										} else {
											drv.psg_id = psg._id;
											drv.updated = new Date().getTime();
											drv.status = "BUSY";
											drv.save(function (err, response) {
												if (err) {
													res.json({ status: false, msg: "error" });
												} else {
													socket.emit("DriverSocketOn", response);
													//socket.emit("passengerAcceptDrv", response);												
													res.json({
														status: true,
														//msg: "Update passenger to BUSY => passenger accept driver" 
														data: response
													});
												}
											});
										}
									}
								);
							}
						});
					} else {
						psg.updated = new Date().getTime();
						psg.save(function (err, response) {
							if (err) {
								res.json({ status: false, msg: "error" });
							} else {
								res.json({
									status: true,
									//msg: "Update passenger to BUSY => passenger accept driver" 
									data: { status: response.status }
								});
							}
						});
					}
				}
			}
		);
	};
}




exports.passengerGetDrvLoc = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var drv_id = req.body.drv_id;
	var psgquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, drv_id: drv_id, cgroup: 'smiles' };
	} else {
		psgquery = { device_id: device_id, drv_id: drv_id, cgroup: 'beam' };
	}

	PassengerModel.findOne(
		psgquery,
		{ _id: 1, status: 1 },
		function (err, psg) {
			if (psg == null) {
				//_passengerAutoLogin (req, res);		
				res.json({
					status: false,
					error: 001,
					msg: "No matching this passenger and driver"
				});
			} else {
				//console.log(psg._id)	
				//if (psg.status == "BUSY") {
				DriversModel.findOne(
					{ _id: drv_id, psg_id: psg._id },
					{ _id: 1, psg_id: 1, curlng: 1, curlat: 1, curloc: 1, accuracy: 1, degree: 1, status: 1, active: 1, fname: 1, lname: 1, carplate: 1, prefixcarplate: 1, phone: 1, carcolor: 1, imgface: 1, car_no: 1, allowpsgcontact: 1, carreturn: 1, carreturnwhere: 1, cgroup: 1, cgroupname: 1, cprovincename: 1 },
					function (err, drv) {
						if (drv == null) {
							_getErrDetail(lang, 'err009', function (errorDetail) {
								res.json({
									status: false,
									error: 002,
									msg: errorDetail
								});
							});
						} else {
							if (err) {
								res.json({ status: false, msg: "error " });
							} else {
								res.json({
									//msg: "success: get drv location ", 
									status: true,
									data: drv
								});
							}
						}
					}
				);
				//} else {
				//	_getErrDetail(lang, 'err009', function (errorDetail){
				//		res.json({ 
				//			status: false, 	
				//			msg: errorDetail
				//		});
				//	});
				//}
			}
		}
	);
};




function saveHistory(driverId, passengerId, job_id, success) {
	JoblistModel.findOne({ job_id: job_id }).select('psg_id').exec(function (err, job) {
		if (!err, job) {
			HistoryModel.find({
				_ownerId: driverId,
				_passengerId: passengerId,
				_passengerJobDetail: job._id
			}).exec(function (err, exists) {
				if (!err && exists.length === 0) {
					HistoryModel.create({
						type: 'PASSENGER',
						status: success ? 'SUCCESSED' : 'CANCELLED',
						_ownerId: driverId,
						_passengerId: passengerId,
						_passengerJobDetail: job._id
					}, function (err, history) {
						if (!err && history)
							console.log('history create success');
					});
				}
			});
		}
	});
}




exports.passengerCancelCall = function (socket) {
	return function (req, res) {
		console.log(' passengerCancelCall ')
		console.log(req.body)		
		var lang = req.body.lang ? req.body.lang : "en";
		var _id = req.body._id;
		var device_id = req.body.device_id;
		var psgquery;
		var Preshift;
		var Postshift;

		if (typeof device_id == 'undefined' && device_id == null) {
			res.json({ status: false, msg: "device id is not valid" })
			return;
		}

		if (req.url.substring(1, 7) == 'smiles') {
			psgquery = { device_id: device_id, cgroup: 'smiles' };
			PassengerModel.findOne(
				psgquery,
				{ status: 1, updated: 1, drv_id: 1, job_id: 1, occupied: 1, deniedTaxiIds: 1 },
				function (err, psg) {
					if (psg == null) {
						res.json({ status: false, msg: "error00" });
					} else {
						old_job_id = psg.job_id;
						if (psg.drv_id == '') {
							psg.job_id = "";
							psg.drv_id = "";
							psg.updated = new Date().getTime();
							psg.deniedTaxiIds = [];
							psg.status = "OFF";
							psg.occupied = false;
							psg.save(function (err, response) {
								if (err) {
									res.json({ 
										status: false, 
										msg: "passengerCancelCall error01" 
									});
								} else {
									_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel before drv');
									res.json({
										status: true,
										msg: "passengerCancelCall pass 1",
										data: { status: response.status }
									});
								}
							});
						} else {
							saveHistory(psg.drv_id, psg._id, psg.job_id, false);
							canceldrvid = psg.drv_id;
							psg.job_id = "";
							psg.drv_id = "";
							psg.updated = new Date().getTime();
							psg.deniedTaxiIds = [];
							psg.status = "OFF";
							psg.occupied = false;
							psg.save(function (err, response) {
								if (err) {
									res.json({ 
										status: false, 
										msg: "passengerCancelCall error02" 
									});
								} else {
									_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel after drv');
									DriversModel.findOne(
										{ _id: canceldrvid }, 
										{ _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, stauts: 1, drv_shift:1, firstshift:1, firstshift_id:1, firstshift_type:1, shift_start:1, shift_end:1 },
										function (err, drv) {
											if (drv == null) {
												res.json({ status: false, msg: "cancelcall error03" });
											} else {
												if(drv.firstshift_id==psg._id){
													PresWallet = drv.drv_wallet;
													PostWallet = drv.drv_wallet + config.smilesprice;													
													drv.drv_shift = Postshift ;
													drv.firstshift = false;
													drv.firstshift_id = null;
													drv.firstshift_type = null; 
													drv.shift_start = null;
													drv.shift_end = null;
													_returnsmilemoney(drv._id, PresWallet, PostWallet);
												}
												drv.psg_id = null;
												drv.status = "ON";
												drv.save(function (err, result) {
													if (err) {
														res.json({ status: false, msg: "cancelcall error04" });
													} else {
														socket.emit("DriverSocketOn", result);
														//socket.emit("passengerCancelCall", result);
														res.json({
															status: true,
															msg: "Update passenger to ON => passenger cancel driver" ,
															data: response,
															result: result
														});
													}
												});
											}
										}
									)
								}
							});
						}
					}
				}
			);			
		} else {
			psgquery = { device_id: device_id, cgroup: 'beam' };
			PassengerModel.findOne(
				psgquery,
				{ status: 1, updated: 1, drv_id: 1, job_id: 1, occupied: 1, deniedTaxiIds: 1 },
				function (err, psg) {
					if (psg == null) {
						res.json({ status: false, msg: "error00" });
					} else {
						old_job_id = psg.job_id;
						if (psg.drv_id == '') {
							psg.job_id = "";
							psg.drv_id = "";
							psg.updated = new Date().getTime();
							psg.deniedTaxiIds = [];
							psg.status = "OFF";
							psg.occupied = false;
							psg.save(function (err, response) {
								if (err) {
									res.json({ 
										status: false, 
										msg: "passengerCancelCall error01" 
									});
								} else {
									_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel before drv');
									res.json({
										status: true,
										msg: "passengerCancelCall pass 1",
										data: { status: response.status }
									});
								}
							});
						} else {
							saveHistory(psg.drv_id, psg._id, psg.job_id, false);
							canceldrvid = psg.drv_id;
							psg.job_id = "";
							psg.drv_id = "";
							psg.updated = new Date().getTime();
							psg.deniedTaxiIds = [];
							psg.status = "OFF";
							psg.occupied = false;
							psg.save(function (err, response) {
								if (err) {
									res.json({ 
										status: false, 
										msg: "passengerCancelCall error02" 
									});
								} else {
									_joblistupdate(old_job_id, req, 'datepsgcancel', 'cancel after drv');
									DriversModel.findOne(
										{ _id: canceldrvid }, 
										{ _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, stauts: 1, drv_shift:1, firstshift:1, firstshift_id:1, firstshift_type:1, shift_start:1, shift_end:1 },
										function (err, drv) {
											if (drv == null) {
												res.json({ status: false, msg: "cancelcall error03" });
											} else {
												if(drv.firstshift==true && drv.firstshift_id==psg._id){
													Preshift = drv.drv_shift;
													Postshift = drv.drv_shift + 1;													
													drv.drv_shift = Postshift ;
													drv.firstshift = false;
													drv.firstshift_id = null;
													drv.firstshift_type = null; 
													drv.shift_start = null;
													drv.shift_end = null;
													_returnshift(drv._id, Preshift, Postshift);
												}
												drv.psg_id = null;
												drv.status = "ON";
												drv.save(function (err, result) {
													if (err) {
														res.json({ status: false, msg: "cancelcall error04" });
													} else {
														socket.emit("DriverSocketOn", result);
														//socket.emit("passengerCancelCall", result);
														res.json({
															status: true,
															msg: "Update passenger to ON => passenger cancel driver" ,
															data: response,
															result: result
														});
													}
												});
											}
										}
									)
								}
							});
						}
					}
				}
			);			
		}



		function _returnshift(_id, PreShift, PostShift) {
			ShifttransactionModel.create({              
                AccountIDs: [ { _id: _id } ] ,
                PreShift: [ { _id: _id, Amount: PreShift } ] ,
                PostShift: [ { _id: _id, Amount: PostShift } ] ,
                Amount: 1 ,
                Type: "Refund",
                AdminID: "SYST",
                DateTime: new Date()
			},
				function (err, response) {
					if (err) {
						console.log(' _returnshift by user is error ')
					} else {
						console.log(' _returnshift succeeded ')
						//return next();
					}
				}
			);
		}

		function _returnsmilemoney(_id, PreWallet, PostWallet) {
			WallettransactionModel.create({              
                AccountIDs: [ { _id: _id } ] ,
                PreWallet: [ { _id: _id, Amount: PreWallet } ] ,
                PostWallet: [ { _id: _id, Amount: PostWallet } ] ,
                Amount: 1 ,
                Type: "Refund",
                AdminID: "SYST",
                DateTime: new Date()
			},
				function (err, response) {
					if (err) {
						console.log(' _returnWallet by user is error ')
					} else {
						console.log(' _returnWallet succeeded ')
						//return next();
					}
				}
			);
		}

	};
};




exports.passengerCancelDrv = function (socket) {
	return function (req, res) {
		console.log(' passengerCancelDrv >>')
		console.log(req.body)
		var lang = req.body.lang ? req.body.lang : "en";
		var _id = req.body._id;
		var device_id = req.body.device_id;	// Lite version use phone as a unique key
		var drv_id = req.body.drv_id;
		var psgquery;

		if (typeof device_id == 'undefined' && device_id == null) {
			res.json({ status: false, msg: "device id is not valid" })
			return;
		}

		if (req.url.substring(1, 7) == 'smiles') {
			psgquery = { device_id: device_id, cgroup: 'smiles' };
		} else {
			psgquery = { device_id: device_id, cgroup: 'beam' };
		}
		//var curloc = [parseFloat(req.body.curlng),parseFloat(req.body.curlat)];
		PassengerModel.findOne(
			psgquery,
			{ status: 1, updated: 1, job_id: 1, drv_id: 1, deniedTaxiIds: 1, occupied: 1 },
			function (err, psg) {
				if (psg == null) {
					_passengerAutoLogin(req, res);
				} else {
					psg.drv_id = "";
					psg.updated = new Date().getTime();
					psg.deniedTaxiIds = [];
					psg.status = "ON";
					psg.occupied = false;
					psg.save(function (err, result) {
						if (err) {
							res.json({ status: false, msg: "error" });
						} else {
							_joblistupdate(psg.job_id, req, 'datepsgcancel', 'cancel the drv');
							DriversModel.findOne(
								{ _id: drv_id },
								{ _id: 1, device_id: 1, cgroup: 1, fname: 1, lname: 1, phone: 1, status: 1, updated: 1, psg_id: 1 },
								function (err, drv) {
									if (drv == null) {
										_getErrDetail(lang, 'err009', function (errorDetail) {
											res.json({
												status: false,
												msg: errorDetail
											});
										});
									} else {
										drv.psg_id = "";
										drv.status = "ON";
										drv.save(function (err, response) {
											if (err) {
												res.json({ status: false, msg: "error", data: err });
											} else {
												socket.emit("DriverSocketOn", drv);
												//socket.emit("passengerCancelDrv", drv);									
												res.json({
													status: true,													
													data: { status: response.status }
												});
											}
										});
									}
								}
							);
						}
					});
				}
			}
		);
	};
};




exports.passengerFavDrvAdd = function (req, res) {
	var _id = req.body._id;
	//var device_id = req.body.device_id	;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var oldfav = req.body.favdrv;
	var psgquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
	}

	PassengerModel.findOne(
		psgquery,
		{ favdrv: 1 },
		function (err, psg) {
			if (psg == null) {
				_passengerAutoLogin(req, res);
			} else {
				arrfav = psg.favdrv;

				favorder = arrfav.indexOf(oldfav)
				//console.log('favorder='+favorder)
				if (favorder >= 0) {
					res.json({ status: true, msg: "You already has this favorite driver" });
					return;
				} else {
					arrfav.push(req.body.favdrv);			// push arrary at the last
				}

				psg.favdrv = arrfav;
				psg.updated = new Date().getTime();
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							status: true,
							msg: "Add favorite completed"
						});
					}
				});
			}
		}
	);
};




exports.passengerFavDrvDel = function (req, res) {
	var _id = req.body._id;
	//var device_id = req.body.device_id	;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var oldfav = req.body.favdrv;
	var psgquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
	}

	PassengerModel.findOne(
		psgquery,
		{ favdrv: 1 },
		function (err, psg) {
			if (psg == null) {
				_passengerAutoLogin(req, res);
			} else {
				arrfav = psg.favdrv;
				//console.log('arrfav='+arrfav)
				favorder = arrfav.indexOf(oldfav)
				//console.log('favorder='+favorder)
				if (favorder >= 0) {
					arrfav.splice(favorder, 1);	// delete array at favorder
				}
				//console.log('arrfav2='+arrfav)
				psg.favdrv = arrfav;
				psg.updated = new Date().getTime();
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							status: true
						});
					}
				});
			}
		}
	);
};




exports.passengerSendComment = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var commtype = "PSG";
	var topic = req.body.topic;
	var comment = req.body.comment;
	var psgquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
	}

	PassengerModel.findOne(
		psgquery,
		function (err, psg) {
			if (psg == null) {
				_passengerAutoLogin(req, res);
			} else {
				CommentModel.create({
					commtype: commtype,
					user_id: _id,
					device_id: device_id,
					topic: topic,
					comment: comment
				},
				function (err, response) {
					if (err) {
						res.send(err)
					} else {
						_getErrDetail(lang, 'err010', function (errorDetail) {
							res.json({
								status: true,
								msg: errorDetail
							});
						});
					}
				});
			}
		}
	);
};




exports.passengerEndTrip = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var drv_id = req.body.drv_id;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	var psgquery;

	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "device id is not valid" })
		return;
	}

	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
	}
		
	PassengerModel.findOne(
		psgquery,
		{ status: 1, updated: 1, job_id: 1 },
		function (err, psg) {
			old_job_id = psg.job_id;
			if (psg == null) {
				_passengerAutoLogin(req, res);
			} else {
				psg.job_id = "";
				psg.curaddr = "";
				psg.destination = "";
				psg.desloc = [];
				psg.des_dist = "";
				psg.tips = "";
				psg.detail = "";
				psg.drv_id = "";
				psg.updated = new Date().getTime();
				psg.status = "OFF";
				psg.occupied = false;
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						_joblistupdate(old_job_id, req, 'datepsgthanks', '');
						res.json({
							//msg: "Update passenger to OFF => passenger end trip" 
							status: true,
							data: { status: response.status }
						});
					}
				});
			}
		}
	);
};




exports.Psgcalllog = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var psg_id;
	var psg_device_id;
	var psg_phone;
	var drv_id;
	var drv_phone;
	var drv_carplate;
	var callby;

	DriversModel.findOne(
		{ _id: req.body.drv_id },
		function (err, drv) {
			if (drv == null) {
				console.log('psgcallog')
				_getErrDetail(lang, 'err009', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			} else {
				PsgcalllogModel.create({
					psg_id: req.body.psg_id,
					psg_device_id: req.body.psg_device_id,
					psg_phone: req.body.psg_phone,
					drv_id: req.body.drv_id,
					drv_phone: req.body.drv_phone,
					drv_carplate: req.body.drv_carplate,
					callby: req.body.callby,
					cgroup: drv.cgroup
				},
				function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						res.json({
							status: true,
							data: {
								_id: response._id,
								status: response.phone
							}
						});
					}
				});
			}
		}
	);
};




exports.transporter = function (req, res) {
	var email = req.body.email;
	var nodemailer = require('nodemailer');
	// create reusable transporter object using the default SMTP transport
	var transporter = nodemailer.createTransport('smtps://user%40gmail.com:pass@smtp.gmail.com');
	var psgquery;
	
	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { email: email, cgroup: 'smiles' };
	} else {
		psgquery = { email: email, cgroup: 'beam' };
	}
	
	PassengerModel.findOne(
		psgquery,
		{ username: 1, smsconfirm: 1 },
		function (err, psg) {
			if (psg == null) {
				res.json({ status: false, msg: "email is invalid" });
			} else {
				psg.smsconfirm = ranSMS();
				psg.save(function (err, response) {
					if (err) {
						res.json({ status: false, msg: "error" });
					} else {
						// setup e-mail data with unicode symbols
						var mailOptions = {
							from: '"xxx" <foo@blurdybloop.com>', // sender address
							to: 'bar@blurdybloop.com', // list of receivers
							subject: 'Hello', // Subject line
							text: 'Hello world ', // plaintext body
							html: '<b>Hello world</b>' // html body
						};

						// send mail with defined transport object
						transporter.sendMail(mailOptions, function (error, info) {
							if (error) {
								return console.log(error);
							}
							console.log('Message sent: ' + info.response);
							res.json({ status: true, msg: "error" });
						});
					}
				});
			}
		}
	);
};




//--------------------------------------- new version 0.3 ------------------------------




exports.getPassengerHistory = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var _id = req.body._id;
	JoblistModel.find(
		{ psg_id: _id },
		{ ProvinceID: 1, NameEN: 1, NameTH: 1, job_id: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, cgroup: 1, curaddr: 1, destination: 1, tips: 1, detail: 1, createdvia: 1, curloc: 1, datepsgcall: 1, datedrvwait: 1, datepsgaccept: 1, datedrvpick: 1, datedrvdrop: 1, datepsgcancel: 1, datedrvcancel: 1 },
		{ sort: { 'datepsgcall': -1 } },
		function (err, job) {
			if (job == 0) {
				//ระบบมีการเปลี่ยนแปลงข้อมูลล่าสุดของแท็กซี่ กรุณาลองอีกครั้ง
				_getErrDetail(lang, 'err011', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				res.json({
					status: true,
					msg: "History detail",
					data: job
				});
			}
		}
	);
};




exports.passengerCallDirect = function (req, res) {
	var _id = req.body._id;
	var device_id = req.body.device_id;	// Lite version use phone as a unique key
	var drv_id = req.body.drv_id;
	var phone = req.body.phone;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var favcartype = req.body.favcartype;
	var tips = req.body.tips;
	var job_id = new Date().getTime() + '-' + ranSMS();
	var jobtype = "DIRECT";
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	var psgquery;
	var cgroup;
	
	if (req.url.substring(1, 7) == 'smiles') {
		cgroup = "smiles";
		psgquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		cgroup = "beam";
		psgquery = { device_id: device_id, cgroup: 'beam'};
	}

	if (typeof drv_id == 'undefined' && drv_id == null) {
		res.json({ status: false, msg: "drv_id is required." })
		return;
	}
	PassengerModel.findOne(
		psgquery,
		{ device_id: 1, job_id: 1, curaddr: 1, curloc: 1, destination: 1, desloc: 1, des_dist: 1, tips: 1, detail: 1, favcartype: 1, status: 1, updated: 1, createdjob: 1 },
		function (err, psg) {
			if (psg == null) {
				PassengerModel.create({
					device_id: device_id,
					phone: req.body.phone,
					createdvia: "SERVICES",
					cgroup: cgroup,
					status: "OFF"
				},
					function (err, psg) {
						if (err) {
							res.json({ status: false, msg: "error", data: err });
						} else {
							if (req.body.phone) { psg.phone = req.body.phone ? req.body.phone : psg.phone; }
							if (req.body.curaddr) { psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr; }
							if (req.body.curlng) { psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng; }
							if (req.body.curlat) { psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat; }
							if (req.body.favcartype) { psg.favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype; }
							if (curloc) { psg.curloc = curloc ? curloc : psg.curloc; }
							if (req.body.destination) { psg.destination = req.body.destination ? req.body.destination : psg.destination; }
							if (req.body.tips) { psg.tips = req.body.tips ? req.body.tips : psg.tips; }
							if (req.body.detail) { psg.detail = req.body.detail ? req.body.detail : psg.detail; }
							psg.createdvia = req.body.createdvia;
							psg.job_id = job_id;
							psg.status = "DIRECT";
							psg.occupied = false;
							psg.updated = new Date().getTime();
							psg.createdjob = new Date().getTime();
							psg.save(function (err, response) {
								if (err) {
									res.json({
										status: false,
										msg: "error",
										data: err
									});
								} else {
									_joblistcreate(job_id, jobtype, drvarroundlist, req);
									res.json({
										status: true,
										msg: "create new a passenger",
										data: response
									});
								}
							});
						}
					});
			} else {
				// Update passenger info and status to be "DIRECT"				
				if (req.body.phone) { psg.phone = req.body.phone ? req.body.phone : psg.phone; }
				if (req.body.curaddr) { psg.curaddr = req.body.curaddr ? req.body.curaddr : psg.curaddr; }
				if (req.body.curlng) { psg.curlng = req.body.curlng ? req.body.curlng : psg.curlng; }
				if (req.body.curlat) { psg.curlat = req.body.curlat ? req.body.curlat : psg.curlat; }
				if (typeof req.body.favcartype == 'undefined') {
					psg.favcartype = [];
				} else {
					if (req.body.favcartype) { psg.favcartype = req.body.favcartype ? req.body.favcartype : psg.favcartype; }
				}
				if (curloc) { psg.curloc = curloc ? curloc : psg.curloc; }
				if (req.body.destination) { psg.destination = req.body.destination ? req.body.destination : psg.destination; }
				if (req.body.tips) { psg.tips = req.body.tips ? req.body.tips : psg.tips; }
				if (req.body.detail) { psg.detail = req.body.detail ? req.body.detail : psg.detail; }
				psg.createdvia = req.body.createdvia;
				psg.job_id = job_id;
				psg.status = "DIRECT";
				psg.occupied = false;
				psg.updated = new Date().getTime();
				psg.createdjob = new Date().getTime();
				psg.save(function (err, result) {
					if (err) {
						res.json({
							status: false,
							msg: "error",
							data: err
						});
					} else {
						_joblistcreate(job_id, jobtype, drvarroundlist, req);
						DriversModel.findOne(
							{ _id: drv_id },
							{ status: 1, updated: 1, _id: 1, device_id: 1, psg_id: 1, cgroup: 1, cgroupname: 1, cprovincename: 1, fname: 1, lname: 1, phone: 1 },
							function (err, drv) {
								if (drv == null) {
									res.json({
										status: false,
										msg: "drv_id is not valid."
									});
								} else {
									drv.psg_id = psg._id;
									drv.updated = new Date().getTime();
									drv.status = "DIRECT";
									drv.save(function (err, response) {
										if (err) {
											res.json({ status: false, msg: "error" });
										} else {
											//socket.emit("DriverSocketOn", response);
											res.json({
												status: true,
												psg_data: result,
												drv_data: response
											});
										}
									});
								}
							}
						);
					}
				});
			}
		}
	);
};




/*
http.get("http://www.google.com/index.html", function(res) {
  console.log("Got response: " + res.statusCode);
}).on('error', function(e) {
  console.log("Got error: " + e.message);
});
*/
exports.sendSMSRegister = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var gensms = ranSMS();
	var _id = req.body._id;
	var device_id = req.body.device_id;
	PassengerModel.findOne(
		{ _id: _id, device_id: device_id },
		{ _id: 1, status: 1, phone: 1, smsconfirm: 1 },
		function (err, response) {
			if (response == null) {
				//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
				_getErrDetail(lang, 'err006', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail
					});
				});
			} else {
				response.smsconfirm = gensms;
				response.save(function (err, result) {
					if (err) {
						res.json({ status: false, msg: "error", data: err });
					} else {
						var smsconfirm = gensms;
						var smsphone = response.phone;
						var https = require('https');
						var postData = JSON.stringify({
							"sender": "TaxiBeam",
							"to": smsphone,
							"msg": "กรุณาใส่เลขรหัส 4 หลักนี้  " + smsconfirm + " ในช่องที่กำหนด เพื่อยืนยันการเข้าใช้บริการค่ะ  "
						});
						var options = {
							hostname: 'sms.narun.me',
							port: 443,
							path: '/SMSMeJson.svc/SendMessage',
							method: 'POST',
							headers: {
								'Content-Type': 'application/json; charset=utf-8'
							}
						};
						var request = https.request(options, function (response) {
							var body = '';
							response.on('data', function (d) {
								body += d;
							});
							response.on('end', function () {
								var parsed = JSON.parse(body);
								if (parsed.SendMessageResult.ErrorCode == 1) {
									res.json({
										status: true
									});
								} else {
									//msg: "ระบบไม่สามารถส่งเลขรหัสยืนยันการเข้าใช้บริการได้ กรุณาตรวจสอบเบอร์โทรศัพท์อีกครั้ง"
									_getErrDetail(lang, 'err006', function (errorDetail) {
										res.json({
											status: false,
											msg: errorDetail
										});
									});
								}
							});
						});
						request.write(postData);
						request.end();
					}
				});
			}
		}
	);
};
/*
1=Successful
21=SMS Sending Failed
22=Invalid username/password combination
23=Credit exhausted
24=Gateway unavailable
25=Invalid schedule date format
26=Unable to schedule
27=Username is empty
28=Password is empty
29=Recipient is empty
30=Message is empty
31=Sender is empty
32=One or more required fields are empty
*/




exports.UpdatePhoneNumber = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var otp_number = req.body.otp_number;
	var newphone = req.body.newphone;
	PassengerModel.findOne(
		{ _id: _id, device_id: device_id },
		{ _id: 1, device_id: 1, smsconfirm: 1, phone: 1 },
		function (err, psg) {
			if (psg == null) {
				_getErrDetail(lang, 'err014', function (errorDetail) {
					res.json({
						status: false,
						msg: errorDetail,
					});
				});
			} else {
				if (psg.smsconfirm == otp_number) {
					psg.phone = newphone;
					psg.updated = new Date().getTime();
					psg.save(function (err, response) {
						if (err) {
							_getErrDetail(lang, 'err099', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail,
								});
							});
						} else {
							var myToken = jwt.sign({
								_id: req.body._id
							}, 'myScottSummer', {
									algorithm: 'HS256',
									expiresIn: 604800		// in second = 7 days
								});
							res.json({
								status: true,
								myToken,
								data: psg
							});
						}
					});
				} else {
					// "msg": "เลข OPT ไม่ถูกต้อง"
					_getErrDetail(lang, 'err013', function (errorDetail) {
						res.json({
							status: false,
							msg: errorDetail,
						});
					});
				}
			}
		}
	);
};




exports.loadpsgfare = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, data: {} })
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, data: {} })
		return;
	}
	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];
	Lk_provincesModel.findOne(
		{
			polygons:
			{
				$geoIntersects:
				{
					$geometry:
					{
						"type": "Point",
						"coordinates": curloc
					}
				}
			}
		},
		{ polygons: 0, Lat: 0, Long: 0, _id_garage: 0, searchpsgamount: 0, searchpsgradian: 0 },
		function (err, response) {
			if (response == null) {
				res.json({
					status: false,
					data: {}
				});
			} else {
				res.json({
					status: true,
					data: {
						psgfare: response.psgfare,
						ProvinceID: response.ProvinceID,
						Name: response._doc["Name" + (req.body.lang ? req.body.lang : "en")] || response._doc["Nameen"]
					}
				});
			}
		}
	);
}




exports.psgshareactivities = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var _id = req.body._id;
	var device_id = req.body.device_id;
	var job_id = req.body.job_id;
	var sharetype = "facebook";
	var psgtype;
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}

	var psgquery;
	
	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
		psgtype = 'smiles';
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
		psgtype = 'beam';
	}

	PassengerModel.findOne(
		psgquery,
		{ device_id: 1, job_id: 1, status: 1, updated: 1, drv_id: 1 },
		function (err, psg) {
			if (psg == null) {
				// เบอร์นี้ยังไม่ลงทะเบียนฯ 
				res.json({
					status: false,
					msg: "Please login"
				});
			} else {
				ShareActivitiesModel.findOne(
					{
						lang: lang,
						sharetype: sharetype,
						psgtype: psgtype,
						expired: { $gte: new Date().getTime() }
					},
					{ _id: 1, sharetype: 1, topic: 1, detail: 1, sharelink: 1, shareimg: 1, hashtag: 1 },
					{ sort: '-expired' },
					function (err, result) {
						if (result == null) {
							res.json({ status: false, msg: "No data for share activities." });
						} else {
							JoblistModel.findOne(
								{ psg_device_id: device_id, job_id: job_id },
								{ ProvinceID: 1, NameEN: 1, NameTH: 1, job_id: 1, drv_id: 1, drv_name: 1, drv_phone: 1, drv_carplate: 1, cgroup: 1, curaddr: 1, destination: 1, tips: 1, detail: 1, createdvia: 1, curloc: 1, desloc: 1, datepsgcall: 1, datedrvwait: 1, datepsgaccept: 1, datedrvpick: 1, datedrvdrop: 1, datepsgcancel: 1, datedrvcancel: 1, _id_shareactivities: 1 },
								{ sort: { 'datepsgcall': -1 } },
								function (err, job) {
									if (job == null) {
										// เกิดความผิดพลาดกรุณาลองใหม่อีกครั้ง
										_getErrDetail(lang, 'err099', function (errorDetail) {
											res.json({
												status: false,
												msg: errorDetail
											});
										});
									} else {
										job._id_shareactivities = result._id;
										job.save(function (err, response) {
											if (err) {
												res.json({
													status: false,
													data: job,
													sharedata: result
												});
											} else {
												res.json({
													status: true,
													data: job,
													sharedata: result
												});
											}
										});
									}
								}
							);
						}
					}
				);
			}
		}
	);
}




exports.psgsaveactivities = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var _id = req.body.psg_id;
	var device_id = req.body.device_id;
	var job_id = req.body.job_id;
	var sharetype = "facebook";
	var _id_shareactivities = req.body._id_shareactivities;
	if (typeof device_id == 'undefined' || device_id == null || device_id == "") {
		res.json({ status: false, msg: "Please put your device" })
		return;
	}

	var psgquery;
	
	if (req.url.substring(1, 7) == 'smiles') {
		psgquery = { device_id: device_id, cgroup: 'smiles' };
	} else {
		psgquery = { device_id: device_id, cgroup: 'beam' };
	}

	PassengerModel.findOne(
		psgquery,
		{ device_id: 1, job_id: 1, status: 1, updated: 1, drv_id: 1 },
		function (err, psg) {
			if (psg == null) {
				// เบอร์นี้ยังไม่ลงทะเบียนฯ 
				res.json({
					status: false,
					msg: "Please login"
				});
			} else {
				JoblistModel.findOneAndUpdate({
					psg_device_id: device_id, job_id: job_id
				},
					{
						share_succeed: true,
						share_date: new Date(),
						share_fbdata: req.body
					},
					{ upsert: false, new: true }, function (err, response) {
						if (err) {
							res.json({
								status: false,
								msg: "invalid error 500"
							});
						} else {
							if (response == null) {
								res.json({
									status: false,
									msg: "passenger passid is invalid"
								});
							} else {
								// Thank you fro participation
								_getErrDetail(lang, 'err018', function (errorDetail) {
									res.json({
										status: false,
										msg: errorDetail
									});
								});
							}
						}
					}
				);
			}
		}
	);
}




/* Dialog Wongnai
ลืมรหัสผ่านใช่ไหมครับ ไม่เป็นไร เราช่วยได้

Copy and paste URL ต่อไปนี้ลงบน browser หรือกดลิงค์ทางด้านล่างนี้เพื่อเปลี่ยนรหัสผ่านใหม่

https://www.wongnai.com/guest?_f=changePassword&token=4a1c773f124ed34c54f4833a2569115a

ยินดีต้อนรับกลับมาสนุกกับการค้นหาและรีวิวร้านอาหารครับ

The Wongnai Team

*ถ้าคุณไม่ได้สมัครเป็นสมาชิกกับวงในและคิดว่านี่อาจจะเป็นข้อผิดพลาดของระบบ กรุณาแจ้งมาที่ admin@wongnai.com.
**หากมีคำถามใดๆ เกี่ยวกับการใช้งาน ขอเชิญติดต่อเราผ่านทาง หน้า webboard ที่ http://www.wongnai.com/forums/contact-us




https://www.wongnai.com/download | ค้นหาร้านเด็ดๆด้วย Wongnai ที่่ง่ายและเร็วกว่า 

Copyright @2010-2016 Wongnai.com All right reserved. https://www.wongnai.com
*/


/*
device.save(function(err,response){
	if(err) {
		res.json({ status: false , msg: "error" });
	}				
	else {
		// waiting to on , busy to on 
		if(req.body.passengerID) {
			PassengerModel.findOne(
			{
				uid : req.body.passengerID
			}, 
			function(err, psg) {
				// update psg by UID 
				if(req.body.action) { 	psg.status 	= statusValue.psg; }

				if(device.status==statusConstant.on){ msg = 'on';}
				else{ msg = 'waiting'; }

				psg.save(function(err, response) {
					err ? res.send(err) : res.json({ status: true , msg: "success: taxi status "+msg });
				});
			});
		}
		else {
			res.json({ status: true , msg: "no taxi" });
		}
	}
});
*/