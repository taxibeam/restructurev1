////////////////////////////////////
// TaxiBeam-Boontermn API Controller 
// version : 1.0 
// Date April 18, 2016
// Wrote by Hanzen
////////////////////////////////////
var config = require('../../config/func').production;
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var Url = require('url');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var async = require("async");
var compareVersions = require('compare-versions');

// var hostrun = os.hostname();
var DriversModel = mongoose.model('DriversModel');
var BoontermtransactionModel = mongoose.model('BoontermtransactionModel');

// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var https = require('https');
var qs = require('querystring');
var ios = require('socket.io');

// Test javascript at "http://js.do/"
function ranSMS() {
    var str = Math.random();
    var str1 = str.toString();
    var res = str1.substring(2, 6);
    return res;
}



function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}



function tryParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);
        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
        if (o && typeof o === "object" && o !== null) {
            return o;
        }
    }
    catch (e) { }
    return false;
};



function _getErrDetail(errid) {
    ErrorcodeModel.findOne(
        { errid: errid },
        { erren: 1, errth: 1 },
        function (err, response) {
            if (response == null) {
                return "";
            } else {
                return response[0];
            }
        }
    );
};



function array_include(arr, obj) {
    return (arr.indexOf(obj) != -1);
}



function validrefid(inputStr) {
    if (inputStr.length > 20) {
        return false;
    } else {
        return true;
    }
}



function validcitizenid(inputStr) {
    if (!inputStr) {
        return false;
    } else if (isNaN(inputStr)) {
        return false;
    } else if (inputStr.length != 13) {
        return false;
    } else {
        return true;
    }
}



function validNaN(inputStr) {
    var Amount = parseFloat(inputStr);
    var priceindex = [20, 40, 60, 100, 140, 200, 300, 600, 900];
    if (!inputStr) {
        return false;
    } else if (isNaN(Amount)) {
        return false;
    } else if (Amount < 0) {
        return false;
    } else if (priceindex.indexOf(Amount) < 0) {
        return false;
    } else {
        return true;
    }
}



function validStringLength(inputStr, LimitL) {
    if (!inputStr) {
        return false;
    } else if (inputStr.length < LimitL) {
        return false;
    } else {
        return true;
    }
}



function validDataStr(inputStr) {
    if (!inputStr) {
        return false;
    } else {
        return true;
    }
}



//////////////////////////////////////
exports.Bsession = function (req, res) {
    var API_KEY = req.headers.api_key;
    var REFID = req.body.REFID;
    var Channel = req.body.Channel;
    var ServiceName = req.body.ServiceName;
    var Options = req.body.Options;         // [{ "Key":"citizenid", "Value":"xxx" },{ "Key":,"Value":"yyy"}]
    var citizenid;
    var SessionID = ranSMS() + new Date().valueOf();
        
    try {
        for (var k in Options) {
            if (Options[k].Key == "citizenid") {
                citizenid = Options[k].Value;
            }
        }

        if (API_KEY != config.BOONTERM_API_KEY) {
            res.json({
                REFID: REFID,
                Code: "0001",
                Desc: "Invalid API Key"
            });
        } else if (Channel != "Boonterm") {
            res.json({
                REFID: REFID,
                Code: "0010",
                Desc: "Invalid Input"
            });
        } else if (!REFID) {
            res.json({
                REFID: REFID,
                Code: "0002",
                Desc: "Invalid REFID"
            });
        } else if (!validrefid(REFID)) {
            res.json({
                REFID: REFID,
                Code: "0002",
                Desc: "Invalid REFID"
            });
        } else if (citizenid=='0000000000000') {
            // for test Processing Error
            res.json({
                REFID: REFID,
                Code: "0040",
                Desc: "Processing Error"
            });
        } else if (citizenid=='1111111111111') {
            // for test Processing Error
            res.json({
                REFID: REFID,
                Code: "1000",
                Desc: "Under Maintenance"
            }); 
        } else if (citizenid=='2222222222222') {
            // for test Processing Error
            res.json({
                REFID: REFID,
                Code: "2000",
                Desc: "System Error"
            });                        
        } else if (!validcitizenid(citizenid)) {
            res.json({
                REFID: REFID,
                Code: "0004",
                Desc: "Invalid citizenid"
            });
        } else {
            BoontermtransactionModel.findOne({
                REFID: REFID
            }, function (err, bresult) {
                if (bresult == null) {
                    // REFID is new ... so create a new order
                    DriversModel.findOne(
                        { citizenid: citizenid, _id_garage: { $ne: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c') } }
                    ).select(
                        '_id citizenid fname lname phone email drv_wallet drv_round'
                        ).exec(function (err, result) {
                            if (err) {
                                res.json({
                                    REFID: REFID,
                                    Code: "0040",
                                    Desc: "Processing Error"
                                });
                            }
                            if (result == null) {
                                res.json({
                                    REFID: REFID,
                                    Code: "0004",
                                    Desc: "Invalid citizenid"
                                });
                            } else {
                                var BSessionTimeout = new Date().getTime() + config.BSessionTimeout;
                                BoontermtransactionModel.create({
                                    _id_driver: result._id,
                                    citizenid: citizenid,
                                    REFID: REFID,
                                    SessionID: SessionID,
                                    PreAmount: result.drv_wallet,
                                    PostAmount: result.drv_wallet,
                                    ReqSess: req.body,
                                    BSessionTimeout: BSessionTimeout,
                                    status: "INITIATED",
                                    Sessiondate: new Date()
                                }, function (err, doc) {
                                    if (err) {
                                        res.json({
                                            REFID: REFID,
                                            Code: "0040",
                                            Desc: "Processing Error"
                                        });
                                    } else {
                                        var BToken = jwt.sign(
                                            {
                                                Channel: req.body.Channel
                                            }, 'myScottSummer',
                                            {
                                                typ: 'JWT',
                                                algorithm: 'HS256',
                                                expiresIn: 1200		// in second 
                                            }
                                        );
                                        var Name = result.fname + ' ' + result.lname;
                                        res.json({
                                            REFID: REFID,
                                            Code: "0000",
                                            Desc: "Success",
                                            //BToken: BToken,
                                            Reference: [
                                                { Key: "Amount", Value: "" + result.drv_wallet },
                                                { Key: "SessionID", Value: SessionID },
                                                { Key: "Name", Value: Name }
                                            ]
                                        });
                                    }
                                });
                            }
                        }
                        );
                } else {
                    res.json({
                        REFID: REFID,
                        Code: "0002",
                        Desc: "Invalid REFID"
                    });
                }
            })
        }
    } catch (exception) {
        res.json({ status: false, msg: exception });
    }

};




exports.Btransaction = function (socket) {
    return function (req, res) {
        var API_KEY = req.headers.api_key;
        var REFID = req.body.REFID;
        var Channel = req.body.Channel;
        var ServiceName = req.body.ServiceName;
        var Amount = req.body.Amount;
        var Options = req.body.Options;         // [{ "Key":"Boonterm", "Value":"xxx" },{ }]
        var SessionID;
        var PreAmount;
        var PostAmount;
        try {
            for (var k in Options) {
                if (Options[k].Key == "SessionID") {
                    SessionID = Options[k].Value;
                }
            }

            if (!validNaN(Amount)) {
                res.json({
                    REFID: REFID,
                    Code: "0008",
                    Desc: "Invalid amount"
                });
                return;
            } else {
                Amount = parseFloat(req.body.Amount);
            }

            if (API_KEY != config.BOONTERM_API_KEY) {
                res.json({
                    REFID: REFID,
                    Code: "0001",
                    Desc: "Invalid API Key"
                });
            } else if (Channel != "Boonterm") {
                res.json({
                    REFID: REFID,
                    Code: "0010",
                    Desc: "Invalid input"
                });
            } else if (!REFID) {
                res.json({
                    REFID: REFID,
                    Code: "0002",
                    Desc: "Invalid REFID"
                });
            } else if (!validrefid(REFID)) {
                res.json({
                    REFID: REFID,
                    Code: "0002",
                    Desc: "Invalid REFID"
                });
            } else if (!validDataStr(SessionID)) {
                res.json({
                    REFID: REFID,
                    Code: "0003",
                    Desc: "Invalid SessionID"
                });
            } else {
                var KeyOperator = ranSMS() + new Date().valueOf();
                BoontermtransactionModel.findOne({
                    "REFID": REFID, "SessionID": SessionID
                }, function (err, bresult) {
                    if (bresult == null) {
                        res.json({
                            REFID: REFID,
                            Code: "0002",
                            Desc: "Invalid REFID"
                        });
                    } else {
                        DriversModel.findOne(
                            { citizenid: bresult.citizenid, _id_garage: { $ne: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c') } }
                        ).select(
                            '_id citizenid fname lname phone email drv_wallet drv_round'
                            ).exec(function (err, resPre) {
                                if (err) {
                                    res.json({
                                        REFID: REFID,
                                        Code: "0040",
                                        Desc: "Processing Error"
                                    });
                                }
                                if (resPre == null) {
                                    res.json({
                                        REFID: REFID,
                                        Code: "0004",
                                        Desc: "Invalid citizenid"
                                    });
                                } else {
                                    PreAmount = resPre.drv_wallet;
                                    PostAmount = resPre.drv_wallet + Amount ;
                                    BoontermtransactionModel.findOneAndUpdate(
                                        {
                                            "REFID": REFID,
                                            "SessionID": SessionID,
                                            "BSessionTimeout": { $gt: new Date().getTime() },
                                            "Transactiondate": { $exists: false }
                                        }, {
                                            ReqTrans: req.body,
                                            status: "SUCCESS",
                                            PreAmount: PreAmount,
                                            PostAmount: PostAmount,
                                            Amount: Amount,
                                            KeyOperator: KeyOperator,
                                            Transactiondate: new Date()
                                        }, { upsert: false }, function (err, doc) {
                                            if (err) {
                                                res.json({
                                                    REFID: REFID,
                                                    Code: "0040",
                                                    Desc: "Processing Error"
                                                });
                                            } else {
                                                if (doc == null) {
                                                    BoontermtransactionModel.findOne(
                                                        {
                                                            "REFID": REFID
                                                        },
                                                        function (err, errcode) {
                                                            if (errcode == null) {
                                                                res.json({
                                                                    REFID: REFID,
                                                                    Code: "0040",
                                                                    Desc: "Processing Error"
                                                                });
                                                            } else {
                                                                if (errcode.SessionID != SessionID) {
                                                                    res.json({
                                                                        REFID: REFID,
                                                                        Code: "0003",
                                                                        Desc: "Invalid SessionID"
                                                                    });
                                                                } else if (errcode.BSessionTimeout < new Date().getTime()) {
                                                                    res.json({
                                                                        REFID: REFID,
                                                                        Code: "0020",
                                                                        Desc: "Session expires"
                                                                    });
                                                                } else if (errcode.Transactiondate) {
                                                                    res.json({
                                                                        REFID: REFID,
                                                                        Code: "0002",
                                                                        Desc: "Invalid REFID"
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    );
                                                } else {
                                                    DriversModel.findOneAndUpdate(
                                                        { citizenid: doc.citizenid, _id_garage: { $ne: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c') } },
                                                        { $inc: { drv_wallet: Amount } },
                                                        { upsert: false, new: true },
                                                        function (err, result) {
                                                            if (err) {
                                                                res.json({
                                                                    REFID: REFID,
                                                                    Code: "0040",
                                                                    Desc: "Processing Error"
                                                                });
                                                            }
                                                            if (result == null) {
                                                                res.json({
                                                                    REFID: REFID,
                                                                    Code: "0040",
                                                                    Desc: "Processing Error"
                                                                });
                                                            } else {
                                                                socket.to(result._id.toString()).emit('WAKEUP_DEVICE', { title: 'Taxi-Beam Driver', message: 'มีการเติมเงินผ่านระบบเข้า driver wallet' });
                                                                res.json({
                                                                    REFID: REFID,
                                                                    Code: "0000",
                                                                    Desc: "Success",
                                                                    Reference: [
                                                                        { Key: "SessionID", Value: SessionID }
                                                                    ]
                                                                });
                                                            }
                                                        }
                                                    );
                                                }
                                            }
                                        }
                                    );












                                }
                            }
                        );                        
                    }
                })
            }
        } catch (exception) {
            res.json({ status: false, msg: exception });
        }

    }
};