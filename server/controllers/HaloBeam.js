////////////////////////////////////
// TaxiBeam API for Samsung
// version : 1.0.0
// UpDate April 25, 2017 
// Created by Hanzen@BRET
////////////////////////////////////
var config = require('../../config/func').production;
var langdrvth = require('../../config/langth').langdrvth;
var crypto = require('crypto');
var compareVersions = require('compare-versions');
var lang = 'en';

// start nodemailer and define param
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var SMTPSentEmail = config.SMTPSentEmail;
var SMTPSentPass = config.SMTPSentPass;
var InfoEmail = config.InfoEmail;
var distance = require('google-distance');
// end define param for nodemailer

var Url = require('url');
var mongoose = require('mongoose');

var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');
var moment = require('moment');

var PassengerModel = mongoose.model('PassengerModel');
var DriversModel = mongoose.model('DriversModel');
var JoblistModel = mongoose.model('JoblistModel');
var AnnounceModel = mongoose.model('AnnounceModel');
var ErrorcodeModel = mongoose.model('ErrorcodeModel');
var JoblisthotelModel = mongoose.model('JoblisthotelModel');
var Lk_garageModel = mongoose.model('Lk_garageModel');
var PsgcalllogModel = mongoose.model('PsgcalllogModel');
var Lk_provincesModel = mongoose.model('Lk_provincesModel');
var HistoryModel = mongoose.model('HistoryModel');

// for upload file
var path = require('path');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs-extra');
var qt = require('quickthumb');
var http = require('http');
var qs = require('querystring');
var ios = require('socket.io');

// var crypto;
// try {
//   crypto = require('crypto');  
// } catch (err) {
//   console.log('crypto support is disabled!');
// }



// NOTE *******************************  start service for Halo not Hello... 
exports.getgaragelist = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var phone = req.body.phone;
	var favcartype = req.body.favcartype;
	var radian = req.body.radian;
	var amount = req.body.amount;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please send device id" });
		return;
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current Longitude is not defined" });
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current Latitude is not defined" });
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof radian == 'undefined' && radian == null) {
		radian = config.halodrvsearchradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.halodrvsearchamount;
	}

	PassengerModel.findOne(
		{ device_id: device_id, cgroup: "halobeam" },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, createdvia: 1, job_id: 1 },
		function (err, result) {
			if (result == null) {
				PassengerModel.create({
					device_id: device_id,
					displayName: "HALOBEAM",
					phone: phone,
					occupied: false,
					updated: new Date(),
					createdvia: "HALOBEAM",
					appversion: "2",
					cgroup: "halobeam",
					status: "OFF"
				},
					function (err, response) {
						if (err) {
							res.json({
								status: false,
								msg: "cannot create passenger",
								data: err
							});
						} else {
							_getgaragelist(response._id);
						}
					}
				);
			} else {
				_getgaragelist(curloc);
			}
		}
	);

	function _getgaragelist(curloc) {
		Lk_provincesModel.findOne(
			{
				polygons:
				{
					$geoIntersects:
					{
						$geometry:
						{
							"type": "Point",
							"coordinates": curloc
						}
					}
				}
			},
			{ polygons: 0, Lat: 0, Long: 0, _id_garage: 0, searchpsgamount: 0, searchpsgradian: 0 },
			function (err, response) {
				if (response == null) {
					res.json({
						status: false,
						msg: "No province info, please put lat & long"
					});
				} else {					
					var myquery = { $match: { active: 'Y', ProvinceID: response.ProvinceID, _id: { $ne: new mongoose.Types.ObjectId('597b0e04dc04ea092db44a7c') } } };			
					Lk_garageModel.aggregate([
						myquery,
						{
							"$group": {
								"_id": {
									"_id": "$_id",
									"ProvinceID": "$ProvinceID",
									"province": "$province",
									"cgroup": "$cgroup",
									"cgroupname": "$cgroupname",
									"cprovincename": "$cprovincename",
									"phone": "$phone",
									"iconurl": "$iconurl"
								},
								"grageCount": { "$sum": 1 }
							}
						},
						{ $sample: { size: 3 } },
						{
							"$group": {
								"_id": {
									"provinceid": "$_id.ProvinceID",
									"province": "$_id.province"
								},
								"garagelist": {
									"$push": {
										"_id": "$_id._id",
										"cgroup": "$_id.cgroup",
										"cgroupname": "$_id.cgroupname",
										"cprovincename": "$_id.cprovincename",
										"phone": "$_id.phone",
										"iconurl": "$_id.iconurl"
									},
								},
								"grageCount": { "$sum": "$grageCount" }
							}
						},
						{ $sort: { "grageCount": -1 } }
					], function (err, result) {
						if (err) {
							res.json({ status: false, data: err });
						} else {
							res.json({
								status: true,
								data: result
							});
						}
					});
				}
			}
		);
	}
};




exports.gettaxilist = function (req, res) {
	var lang = req.body.lang ? req.body.lang : "en";
	var device_id = req.body.device_id;
	var curlng = req.body.curlng;
	var curlat = req.body.curlat;
	var phone = req.body.phone;
	var favcartype = req.body.favcartype;
	var radian = req.body.radian;
	var amount = req.body.amount;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please send device id" });
		return;
	}
	if (typeof curlng == 'undefined' && curlng == null) {
		res.json({ status: false, msg: "The current Longitude is not defined" });
		return;
	}
	if (typeof curlat == 'undefined' && curlat == null) {
		res.json({ status: false, msg: "The current Latitude is not defined" });
		return;
	}

	var curloc = [parseFloat(req.body.curlng), parseFloat(req.body.curlat)];

	if (typeof radian == 'undefined' && radian == null) {
		radian = config.halodrvsearchradian;
	}
	if (typeof amount == 'undefined' && amount == null) {
		amount = config.halodrvsearchamount;
	}

	// ไม่สามารถให้เช็ค device_id ได้ เพราะไม่อย่างนั้น ในกรณีของ webapp ที่เปิดมาครั้งแรกยังไม่มีการสร้าง device_id จะไม่สามารถเห็นรถได้    

	PassengerModel.findOne(
		{ device_id: device_id, cgroup: "halobeam" },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, createdvia: 1, job_id: 1 },
		function (err, result) {
			if (result == null) {
				PassengerModel.create({
					device_id: device_id,
					displayName: "HALOBEAM",
					phone: phone,
					occupied: false,
					updated: new Date(),
					createdvia: "HALOBEAM",
					appversion: "2",
					cgroup: "halobeam",
					status: "OFF"
				},
					function (err, response) {
						if (err) {
							res.json({
								status: false,
								msg: "cannot create passenger",
								data: err
							});
						} else {
							_gettaxilist(response._id);
						}
					}
				);
			} else {
				_gettaxilist(result._id);
			}
		}
	);

	function _gettaxilist(psg_id) {
		var dataDriver = DriversModel.find(
			{
				status: "ON",
				active: "Y",
				curlng: { $ne: null },
				curlat: { $ne: null },
				curloc: {
					$near: {
						$geometry: {
							type: "Point",
							coordinates: curloc
						},
						$maxDistance: radian
					}
				},
				//cartype: { $in: favcartype },
				//_id_garage: { $exists: true }
			},
			{
				_id: 1, fname: 1, lname: 1, phone: 1, prefixcarplate:1, carplate: 1, curlat: 1, curlng: 1, imgface: 1
			}
		);
		//dataDriver.populate('_id_garage', { _id: 0, cgroupname: 1, phone: 1, province: 1 });
		dataDriver.exec(function (err, result) {
			if (result == 0) {
				res.json({ status: true, data: result, msg: "No data" });
			} else {
				res.json({
					status: true,
					data: result
				});
			}
		});
	}
};




exports.psgcalllog = function (req, res) {
	var lang = req.params.lang ? req.params.lang : "en";
	var device_id = req.body.device_id;
	var taxi_id = req.body.taxi_id;
	if (typeof device_id == 'undefined' && device_id == null) {
		res.json({ status: false, msg: "Please send device id" });
		return;
	}
	if (typeof taxi_id == 'undefined' && taxi_id == null) {
		res.json({ status: false, msg: "Please send your driver" });
		return;
	}	
	PassengerModel.findOne(
		{ device_id: device_id, cgroup: "halobeam" },
		{ device_id: 1, drv_id: 1, status: 1, curloc: 1, updated: 1, phone: 1, curlat: 1, curlng: 1, createdvia: 1, job_id: 1 },
		function (err, result) {
			if (result==null){
				res.json({
					status: false,
					msg: "no passenger data"
				});
			} else {
				DriversModel.findOne(
					{ _id: taxi_id },
					function (err, drv) {
						if (drv == null) {
							_getErrDetail(lang, 'err009', function (errorDetail) {
								res.json({
									status: false,
									msg: errorDetail,
								});
							});
						} else {
							PsgcalllogModel.create({
								psg_id: result._id,
								psg_device_id: device_id,
								psg_phone: result.phone,
								drv_id: drv._id,
								drv_phone: drv.phone,
								drv_carplate: drv.carplate,
								callby: "HALOBEAM",
								cgroup: drv.cgroup
							},
								function (err, response) {
									if (err) {
										res.json({ status: false, msg: err });
									} else {
										res.json({
											status: true
										});
									}
								}
							);
						}
					}
				);
			}
		}
	);
};




///////////////////////////////// common functions //////////////////////////////////////
// Test javascript at "http://js.do/"
function ranSMS() {
	var str = Math.random();
	var str1 = str.toString();
	var res = str1.substring(2, 6);
	return res;
}


function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}


function tryParseJSON(jsonString) {
	try {
		var o = JSON.parse(jsonString);
		// Handle non-exception-throwing cases:
		// Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		// but... JSON.parse(null) returns 'null', and typeof null === "object", 
		// so we must check for that, too.
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }
	return false;
};


function _getErrDetail(lang, errCode, callback) {
	ErrorcodeModel.findOne(
		{ errcode: errCode },
		function (err, response) {
			if (response == null) {
				callback('');
			} else {
				callback(response._doc["err" + (lang ? lang : "en")] || response._doc["erren"]);
			}
		}
	);
};