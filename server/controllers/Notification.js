var fs = require('fs-extra');
var moment = require('moment');
var mongoose = require('mongoose');
var DriversModel = mongoose.model('DriversModel');
var NotificationModel = mongoose.model('NotificationModel');
var Lk_garageModel = mongoose.model('Lk_garageModel');

var errorCallback = function (resObj, errObj) {
    resObj.json({ status: false, data: errObj });
}

exports.addNotification = function (notificationSender) {

    return function (req, res) {

        NotificationModel.create({
            type: req.body.type,
            title: req.body.title,
            message: req.body.message,
            image_url: '',
            status: req.body.status,
            start: moment((req.body.start_date + ' ' + req.body.start_time), "DD/MM/YYYY HH:mm").toDate(),
            end: moment((req.body.end_date + ' ' + req.body.end_time), "DD/MM/YYYY HH:mm").toDate(),
            start_now: req.body.start_now === "on" ? true : false,
            driver_selected: req.body.driver_selected,
            garage_selected: req.body.garage_selected,
            province_selected: req.body.province_selected,
            created: moment().toDate(),
            updated: moment().toDate(),
        }, function (err, result) {

            if (err) {
                return res.json({ status: false, data: err });
            }

            fs.move(req.files.base_image.path, '../upload_notifications/' + result._id + '.png', function (err) {
                if (!err) {

                    var data = {
                        image_url: result.id + '.png',
                        updated: moment().toDate()
                    };

                    NotificationModel.update({ _id: result.id }, { $set: data }, function (err, result) {
                        fs.removeSync(req.files.base_image.path);
                        res.json({ status: true, data: result });
                    });

                    if(result.status === 'ACTIVE') {
                        notificationSender.startCountdown(result.uid, result._id.toString(), 3000);
                    } else if(result.status === 'SCHEDULED') {
                        var delay = moment(result.start).diff(moment());
                        if(delay > 0) {
                            notificationSender.startCountdown(result.uid, result._id.toString(), delay);
                        }
                    }
                }
            });
        });
    }
}

exports.editNotification = function (notificationSender) {

    return function (req, res) {

        if (!req.body.id) {
            return res.json({ status: false, data: 'invalid parameter.' });
        }

        NotificationModel.findOne({ _id: req.body.id }).exec(function (err, notification) {

            if (err) {
                return res.json({ status: false, data: err });
            }

            if (notification.status === 'OBSOLETE') {
                return res.json({ status: false, data: 'ข้อความนี้ไม่สามารถแก้ไขได้แล้ว' });
            }

            if (notification.status === 'DRAFT' || notification.status === 'SCHEDULED') {

                notification.type = req.body.type;
                notification.title = req.body.title;
                notification.message = req.body.message;

                notification.status = req.body.status;

                notification.driver_selected = req.body.driver_selected;
                notification.garage_selected = req.body.garage_selected;
                notification.province_selected = req.body.province_selected;

                notification.start = moment((req.body.start_date + ' ' + req.body.start_time), "DD/MM/YYYY HH:mm").toDate();
                notification.end = moment((req.body.end_date + ' ' + req.body.end_time), "DD/MM/YYYY HH:mm").toDate();

                notification.updated = moment().toDate();

                notification.save(function (err, updatedNotification) {
                    if (err) return res.json({ status: true, data: err });

                    if (req.files.base_image) {
                        fs.move(req.files.base_image.path, '../upload_notifications/' + notification._id + '.png', function (err) {
                            if (!err) {
                                fs.removeSync(req.files.base_image.path);
                                res.json({ status: true, data: notification });
                            }
                        });
                    } else {
                        res.json({ status: true, data: notification });
                    }

                    var delay = moment(updatedNotification.start).diff(moment());
                    if (delay > 0) {
                        notificationSender.startCountdown(updatedNotification.uid, updatedNotification._id.toString(), delay);
                    }
                });

            } else if (notification.status === 'ACTIVE') {
                notification.save(function (err, updatedNotification) {

                    if (err) return res.json({ status: true, data: err });

                    notification.updated = moment().toDate();
                    notification.end = moment((req.body.end_date + ' ' + req.body.end_time), "DD/MM/YYYY HH:mm").toDate();

                    if (req.files.base_image) {
                        fs.removeSync(req.files.base_image.path);
                    }
                    res.json({ status: true, data: notification });

                    notificationSender.startCountdown(updatedNotification.uid, updatedNotification._id.toString(), 3000);
                });
            }
        });
    }
}

exports.getNotificationList = function (req, res) {
    NotificationModel.find({}).select('uid type title status start end').exec(function (err, result) {
        if (err) {
            return res.json({ status: true, data: err });
        }
        res.json({ status: true, data: result })
    })
}

exports.getNotificationDetail = function (req, res) {

    !req.body.id && errorCallback(res, 'invalid parameter');

    NotificationModel.findOne({ _id: req.body.id }).exec(function (err, result) {
        err && errorCallback(res, err);

        res.json({ status: true, data: result });
    });
}

exports.getAllGarage = function (req, res) {
    Lk_garageModel.find({}).select('cgroup name ProvinceID').sort('ProvinceID').exec(function (err, result) {
        if (err) {
            return res.status(500).json({
                message: err
            });
        }

        res.json({ status: true, data: result });
    })
}

exports.getAllDriver = function (req, res) {
    var conditions = {
        fname: { '$ne': null },
        lname: { '$ne': null },
        cgroup: { '$ne': null },
        status: { '$ne': 'DELETED' }
    };
    DriversModel.find(conditions).select('fname lname carplate prefixcarplate cgroup').sort('fname').exec(function (err, result) {
        if (err) {
            return res.status(500).json({
                message: err
            });
        }

        res.json({ status: true, data: result });
    })
}