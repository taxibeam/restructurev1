/*
 *
 * TaxiBeam 2.0
 * Copyright 2017 Taxi-Beam Team
 */

// Modules Dependencies ====================================
var express = require('express');
var path = require('path');
var http = require('http');
var https = require('https');
var mongoose = require('mongoose');

mongoose.Promise = require('bluebird');

var fs = require('fs-extra');
var bodyParser = require('body-parser');

var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');

var methodOverride = require('method-override');
var util = require('util');
var pg = require("pg");
var im = require('imagemagick');
var request = require('request');
var split = require('split');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var formidable = require('formidable');
var MongoStore = require('connect-mongo')(session);

var util = require('util');
var log_file = fs.createWriteStream(__dirname + '/../debug.log', {flags : 'w'});
var log_stdout = process.stdout;

var app = express();

// Passport Configuration ===========================================
require('./config/passport')(passport);

// Configuration ===========================================
var env = process.env.NODE_ENV || 'production',
    // ไม่มี config สำหรับ development ต้องกำหนดเป็น production ไปเลย  
    config = require('./config/func')['production'];

app.version = config.app_version;

exports.config = config;

app.set('view engine', 'ejs'); // set up ejs for templating

var ejs = require('ejs'); ejs.open = '{{'; ejs.close = '}}';

// Connect Database ========================================
var dbUrl = config.database.url;
//dbUrl = "mongodb://taxiadmin:abc123@localhost:27017/taxi";
//dbUrl = "mongodb://localhost:27017/taxi";
var db = mongoose.connect(dbUrl, function (err) {
    var _log = err ?
        'Error Connecting Database to: ' + dbUrl + '. ' + err :
        'Database Succeeded Connecting to: ' + dbUrl;
    console.log(_log); // log in cmd
});

// set files location
app.use(express.static(path.join(__dirname, 'public'))); // set the static files location
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

// use morgan to log request to the console
//app.use(morgan('dev'));

////////////////////////  add app.use  express JWT  from npm install express-jwt --save  below bodyParser
//app.use(expressJWT({ secret: 'myScottSummer'}).unless({ path: [ '/service/drivercs/login' ] }));
////////////////////////

app.use(methodOverride());

// required for passport
app.use(session({
    secret: 'taxi-beam',
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 1 * 24 * 60 * 60 // -> session persist for 1 days
    })
})); // session secret

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
//app.use(passport.authenticate('remember-me'));
//app.use(app.router);

app.use("/assets/theme", express.static(__dirname + '/public/theme'));
app.use("/assets/css", express.static(__dirname + '/public/css'));
app.use("/assets/libs", express.static(__dirname + '/public/libs'));
app.use("/assets/img", express.static(__dirname + '/public/img'));
app.use("/assets/data", express.static(__dirname + '/public/data'));
app.use("/assets/sounds", express.static(__dirname + '/public/sounds'));
app.use("/assets/plugins", express.static(__dirname + '/public/plugins'));
app.use("/assets/stylesheets", express.static(__dirname + '/public/stylesheets'));
app.use("/assets/javascripts", express.static(__dirname + '/public/javascripts'));

// Bower Componenets
app.use("/bower_components", express.static(__dirname + '/bower_components'));

app.use("/template", express.static(__dirname + '/public/app/template'));
app.use("/ecmap", express.static(__dirname + '/public/app/ecmap'));
//app.use("/passenger", express.static(__dirname + '/public/app/passenger'));
app.use("/c3", express.static(__dirname + '/public/app/c3'));

app.use("/image/promotion", express.static(__dirname + '/../promotion_images'));
app.use("/image/passenger", express.static(__dirname + '/../upload_passengers'));
app.use("/image/driver", express.static(__dirname + '/../upload_drivers'));
app.use("/image/broken", express.static(__dirname + '/../upload_brokens'));
app.use("/image/notification", express.static(__dirname + '/../upload_notifications'));
app.use("/bbox/assets", express.static(__dirname + '/../upload_assets'));


// Include models ==========================================
var models_path = __dirname + '/server/models';
fs.readdirSync(models_path).forEach(function (file) {
    require(models_path + '/' + file);
})


// Start App ===============================================
var os = require("os");
var hostrun = os.hostname();

var port = process.env.PORT || config.PORT;

var server = app.listen(port);
console.log('Server running ' + hostrun + ' on port ' + port);

//expose app
exports = module.exports = app;


app.get('/zoiper', function (req, res) {
    console.log(req.param('call'));
    res.json({ status: true, message: 'Success' });
});

var io = require('socket.io').listen(server);
require('./server/sockets/sockets')(io);
require('./server/sockets/driver')(io);

require('./server/sockets/notification')(io);


var notificationSender = require('./server/controllers/NotificationSender');
notificationSender.initialize(io);

app.set('notificationSender', notificationSender);


// Routes ==================================================
require('./server/routes')(app, io, passport);  //with passport Pang+

// var apirouter = express.Router();
// require('./server/routes/api.js')(apirouter,io,passport);
// app.use('/api',apirouter);

// app.use(function (err, req, res, next) {
// 	if (err.name === 'UnauthorizedError') {		
// 		console.log(err)
// 		res.status(401).send('invalid token...');		
// 	}
// });

//require('./server/routes')(app);

var allowCrossDomain = function (req, res, next) {
    if ('OPTIONS' == req.method) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
        res.send(200);
    }
    else {
        next();
    }
};

app.use(allowCrossDomain);



//----------------------// crate bot running module
DriversModel = mongoose.model('DriversModel');
PassengerModel = mongoose.model('PassengerModel');
CallcenterpsgModel = mongoose.model('CallcenterpsgModel');
SessionModel = mongoose.model('SessionModel');
JoblistModel = mongoose.model('JoblistModel');
JoblisthotelModel = mongoose.model('JoblisthotelModel');

setInterval(function () { closeIdleTaxis() }, 310000);
setInterval(function () { closeIdlePassengers() }, 330000);
setInterval(function () { closeNotOFFPassengers() }, 350000);
setInterval(function () { closeHotelJob() }, 370000);
setInterval(function () { closeBlankSessionLogin() }, 600000); // 600,000 = run every 10 minute
setInterval(function () { closeCallcenterJob() }, 900000); // 1,800,000 = run every 30 minute
BiddingCCJobs();


// Bot functions  

function BiddingCCJobs() {
    JoblisthotelModel.find(
        { "occupied": false, "biddingstart": true, "biddingend": false, "status": { $in: ["BROADCAST"] } },
        { _id: 1 },
        function (err, bidjob) {
            if (err) return handleError(err);
            if (bidjob == 0) {
                console.log(' no time remnant for bidding timeout ')
            } else {
                for (var i = 0, len = bidjob.length; i < len; i++) {
                    JoblisthotelModel.findOneAndUpdate(
                        { "_id": bidjob[i]._id, "occupied": false, "status": { $in: ["BROADCAST"] } },
                        { $set: { occupied: true } },
                        { upsert: false, new: true },
                        function (err, result) {
                            if (err) return handleError(err);
                            if (result == null) {
                                console.log(' no winning drv id 111 ')
                            } else {

                                var arrshuffle = result.drvbiddingjob;
                                var arrsort = arrshuffle.sort(dynamicSort('dist'));
                                var winnindrv_id = arrsort[0]._id;
                                var lostdrv_id = arrsort.slice(1);

                                for (var i = 0, len = lostdrv_id.length; i < len; i++) {
                                    DriversModel.findOneAndUpdate(
                                        { _id: lostdrv_id[i]._id },
                                        { $set: { status: "ON", msgstatus: "OLD" } },
                                        { upsert: false, new: true },
                                        function (err, drv) {
                                            if (err) return handleError(err);
                                            if (result == null) {
                                                console.log(' no winning drv id 111 ')
                                            } else {
                                                console.log('  LOST BIDDING JOBS ')
                                                io.emit("DriverSocketOn", drv);
                                                io.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ขออภัยค่ะ คุณไม่สามารถรับงานนี้ได้', message: 'งานนี้ได้ถูกส่งให้คนขับที่ใกล้ผู้โดยสารที่สุดแล้ว' });
                                            }
                                        }
                                    );
                                }

                                DriversModel.findOne(
                                    { _id: winnindrv_id },
                                    function (err, drv) {
                                        if (drv == null) {
                                            console.log(' no winning drv id 222 ')
                                        } else {
                                            drv.psg_id = bidjob[i]._id;
                                            drv.job_id = result.job_id;
                                            drv.updated = new Date();
                                            drv.status = "BUSY";
                                            drv.jobtype = "HOTELS";
                                            drv.save(function (err, response) {
                                                if (err) {
                                                    console.log(err)
                                                } else {
                                                    CallcenterpsgModel.findOne(
                                                        { _id: result._id_callcenterpsg },
                                                        function (err, psg) {
                                                            if (psg == null) {
                                                                console.log(' no winning drv id 333 ')
                                                            } else {
                                                                //////////////////////
                                                                // รับงาน
                                                                psg.acceptedTaxiIds.push({
                                                                    device_id: drv.device_id,
                                                                    drv_id: drv._id,
                                                                    acceptedDate: new Date()
                                                                });
                                                                psg.jobhistory.push({
                                                                    action: "Drv winby bot ",
                                                                    status: "ASSIGNED",
                                                                    actionby: drv._id,
                                                                    created: new Date()
                                                                });
                                                                /////////////////////////	
                                                                psg.status = "ASSIGNED";	// if previous is depending, psg_status get data from input to function
                                                                psg.drv_id = drv._id;
                                                                psg.drv_carplate = drv.carplate_formal;
                                                                psg.carplate = drv.carplate;
                                                                psg.prefixcarplate = drv.prefixcarplate;
                                                                psg.updated = new Date();
                                                                psg.datereadmsg = new Date();
                                                                psg.dassignedjob = new Date();
                                                                psg.save(function (err, result) {
                                                                    if (err) {
                                                                        console.log(err)
                                                                    } else {
                                                                        JoblisthotelModel.findOne(
                                                                            { _id: bidjob[i]._id },
                                                                            function (err, psgjoblist) {
                                                                                if (psgjoblist == null) {
                                                                                    console.log('This passenger is not available.');
                                                                                } else {
                                                                                    psgjoblist.biddingend = true;
                                                                                    psgjoblist.drv_id = drv._id;
                                                                                    psgjoblist.drv_device_id = drv.device_id;
                                                                                    psgjoblist.drv_name = drv.fname + ' ' + drv.lname;
                                                                                    psgjoblist.drv_phone = drv.phone;
                                                                                    psgjoblist.drv_carplate = drv.carplate_formal;
                                                                                    psgjoblist.carplate = drv.carplate;
                                                                                    psgjoblist.prefixcarplate = drv.prefixcarplate;
                                                                                    psgjoblist.datedrvwait = new Date();
                                                                                    psgjoblist.updated = new Date();
                                                                                    psgjoblist.status = "BUSY";
                                                                                    psgjoblist.save(function (err, psgjoblistresult) {
                                                                                        if (err) {
                                                                                            console.log(' no winning drv id 666 ')
                                                                                        } else {
                                                                                            console.log('  SUCCESS BIDDING FUNCTION ')
                                                                                            //_sub_ClearBiddingDrv();
                                                                                            io.emit("DriverSocketOn", drv);
                                                                                            io.to(drv._id.toString()).emit('WAKEUP_DEVICE', { title: 'ยินดีด้วย คุณได้รับงาน', message: 'กรุณาเปิดแอปเพื่อดูรายละเอียด' });
                                                                                            io.of('/' + psg.cgroup).emit("gotdispatchaction", { response: response, psg_data: result });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        );
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    );
                                                }
                                            });
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        }
    );
};

function dynamicSort(property) {
    return function (a, b) {
        return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
    }
}


function closeIdleTaxis() {
    DriversModel.find(
        {
            status: { $in: ["ON", "DPENDING"] }
        },
        function doChangeStatusToOFF(err, taxi) {
            if (taxi == null || taxi.length == 0) {
                //console.log("No Passenger Left.");
            } else {
                var currentTimeInSeconds = new Date();
                for (j = 0; j < taxi.length; j++) {
                    var d1 = new Date(taxi[j].updated).getTime(); // 10:09 to
                    var d2 = new Date(currentTimeInSeconds.toISOString()).getTime(); // 10:20 is 11 mins
                    var diff = d2 - d1;
                    var secondsDifference = Math.floor(diff / 1e3) + 35
                    if (secondsDifference > 1200) {	// 1200 sec = 20 mins
                        taxi[j].status = "OFF";
                        taxi[j].msgstatus = "OLD";
                        taxi[j].save(function (err, response) {
                            if (err) {
                                //console.log("Error Change Status To OFF. " + err);
                            } else {
                                //console.log("Success write matched Taxi.");
                            }
                        });
                    }
                }
            }
        }
    );
};




function closeIdlePassengers() {
    PassengerModel.find(
        {
            status: { $in: ["ON"] }
        },
        function doChangeStatusToTIMEOUT(err, psg) {
            if (psg == null || psg.length == 0) {
                //console.log("No Passenger Left.");
            } else {
                var currentTimeInSeconds = new Date();
                for (j = 0; j < psg.length; j++) {
                    var d1 = new Date(psg[j].updated).getTime(); // 10:09 to
                    var d2 = new Date(currentTimeInSeconds.toISOString()).getTime(); // 10:20 is 11 mins
                    var diff = d2 - d1;
                    var secondsDifference = Math.floor(diff / 1e3) + 35
                    if (secondsDifference > 1200) {	// 12600 sec = 20 mins
                        psg[j].status = "OFF";
                        psg[j].save(function (err, response) {
                            if (err) {
                                //console.log("Error Change Passenger Status To OFF. " + err);
                            } else {
                                //console.log("Success change Passenger Status to OFF.");
                            }
                        });
                    }
                }
            }
        }
    );
};




function closeNotOFFPassengers() {
    PassengerModel.find(
        {
            status: { $in: ["ON", "DRVDENIED", "BUSY", "PICK", "THANKS"] }
        },
        function doChangeStatusToTIMEOUT(err, psg) {
            if (psg == null || psg.length == 0) {

            } else {
                var currentTimeInSeconds = new Date();
                for (j = 0; j < psg.length; j++) {
                    var d1 = new Date(psg[j].createdjob).getTime(); // 10:09 to
                    var d2 = new Date(currentTimeInSeconds.toISOString()).getTime(); // 10:20 is 11 mins
                    var diff = d2 - d1;
                    var secondsDifference = Math.floor(diff / 1e3) + 35
                    if (secondsDifference > 3600) {	// 3600 sec = 60 mins
                        psg[j].status = "OFF";
                        psg[j].save(function (err, response) {
                            if (err) {
                                //console.log("Error Change Passenger Status To OFF. " + err);
                            } else {
                                //console.log("Success change Passenger Status to OFF.");
                            }
                        });
                    }
                }
            }
        }
    );
};




function closeCallcenterJob() {
    CallcenterpsgModel.find(
        {
            status: { $in: ["ASSIGNED"] }
        },
        function doChangeStatus(err, psg) {
            if (psg == null || psg.length == 0) {
                //
            } else {
                var currentTimeInSeconds = new Date();
                for (j = 0; j < psg.length; j++) {
                    var d1 = new Date(psg[j].updated).getTime(); // 10:09 to
                    var d2 = new Date(currentTimeInSeconds.toISOString()).getTime(); // 10:20 is 11 mins
                    var diff = d2 - d1;
                    if (diff > 1800000) {   // 1,800,000 sec = clear after 0.5 hr.
                        if (psg[j].drv_id) {
                            psg[j].status = "OFF";
                        } else {
                            psg[j].status = "FINISHED";
                        }
                        psg[j].save(function (err, response) {
                            if (err) {
                                //console.log("Error Change Passenger Status To OFF. " + err);
                            } else {
                                //console.log('closeCallcenterJob--2')
                                //io.on('connection', function(socket) {
                                //socket.emit('closeCallcenterJob', response);
                                io.of('/' + response.cgroup).emit("closeCallcenterJob", response);
                                //});
                                //console.log("Success change Passenger Status to OFF.");
                            }
                        }
                        );
                    }
                }
            }
        }
    );
};




function closeBlankSessionLogin() {
    SessionModel.remove(
        {
            "session": { $not: new RegExp("user") }
        },
        function (err) {
            if (!err) {
                //message.type = 'notification!';
            } else {
                //message.type = 'error';
            }
        }
    );
}




function closeHotelJob() {
    JoblisthotelModel.find(
        {
            status: { $in: ["PICK"] }
        },
        function doChangeStatus(err, psg) {
            if (psg == null || psg.length == 0) {
                //
            } else {
                var currentTimeInSeconds = new Date();
                for (j = 0; j < psg.length; j++) {
                    var d1 = new Date(psg[j].updated).getTime(); // 10:09 to
                    var d2 = new Date(currentTimeInSeconds.toISOString()).getTime(); // 10:20 is 11 mins
                    var diff = d2 - d1;
                    if (diff > 3600000) {   // clear after 1 hr.
                        if (psg[j].drv_id) {
                            psg[j].status = "OFF";
                        } else {
                            psg[j].status = "FINISHED";
                        }
                        psg[j].save(function (err, response) {
                            if (err) {
                                //console.log("Error Change Passenger Status To OFF. " + err);
                            } else {
                                //console.log('closeHoteljob--2');
                            }
                        });
                    }
                }
            }
        }
    );
};




module.exports = function (io) {
    //
}




/*
https://docs.nodejitsu.com/articles/javascript-conventions/what-are-the-built-in-timer-functions
var recursive = function () {
    console.log("It has been one second!");
    setTimeout(recursive,1000);
}
recursive();
setTimeout(function() { console.log("setTimeout: It's been one second!"); }, 1000);
setInterval(function() { console.log("setInterval: It's been one second!"); }, 1000);
setTimeout(console.log, 1000, "This", "has", 4, "parameters");
setInterval(console.log, 1000, "This only has one");
*/

/**************************
var i = 0;
setInterval(function() {
  i++
  bot0(i);
}, 1500);


function bot0(i) {
  //Reset 
  if (i > 3) {
    i = 0
  };

  findInitiatePassenger();

  // Bot functions
  function findInitiatePassenger(req, res) {
    PassengerModel.findOne({
        status: "INITIATE",
      },

      function doMatching(err, psg) {
        if (psg == null || psg.length == 0) {
          //console.log(". ");
          //console.log("No Passenger Left. ");
        } else {
          var deniedTaxiIds = psg.deniedTaxiIds;
          var radian = "10000";
          var curloc = psg.curloc; //[100.5720846, 13.7556048]; //psg.curloc;

          DriversModel.find({
              status: "ON",
              curloc: {
                $near: {
                  $geometry: {
                    type: "Point",
                    coordinates: curloc
                  },
                  $maxDistance: radian
                }
              },
            },
            //{ device_id:0, email:0, des_dist:0, deslng:0, deslat:0, favcartype:0, favdrv:0, drv_id:0, status:0, curloc:0, desloc:0, updated:0, created:0 },

            function(err, taxilist) {
              var noMatchTaxi = true;
              if (typeof taxilist == 'undefined' || taxilist.length == 0) {
                 console.log("No Taxi Left");
              } else {
                for (i = 0; i < taxilist.length; i++) {

                  console.log(taxilist[i].psg_id + " " + taxilist[i].psg_id.length);
                  var alreadyDenyThisPassenger = false;
                  if(taxilist[i].psg_id.length > 0){
                    alreadyDenyThisPassenger = true;
                  }

                  for (j = 0; j < psg.deniedTaxiIds.length; j++) {
                    if (psg.deniedTaxiIds[j] == taxilist[i].device_id) {
                      console.log("Deny Passenger");
                      alreadyDenyThisPassenger = true;
                      break;
                    }
                  }

                  //Found a taxi that did not deny this passenger and still available(Status "ON").
                  if (!alreadyDenyThisPassenger) {
                    noMatchTaxi = false;
                    psg.drv_id = taxilist[i]._id;
                    psg.status = "ON";
                    psg.deniedTaxiIds.push(taxilist[i].device_id);
                    psg.save(function(err, response) {
                      if (err) {
                        console.log("Error Writing Passenger = ", taxilist.length);
                      } else {
                        console.log("Success write matched Taxi ", taxilist.length);
                      }
                    });

                    taxilist[i].psg_id = psg._id;
                    taxilist[i].save(function(err, response) {
                      if (err) {
                        console.log("Error Writing Passenger = ", taxilist.length);
                      } else {
                        console.log("Success write matched Taxi ", taxilist.length);
                      }
                    });
                    break;
                  }
                }
              }

              if (noMatchTaxi) {
                psg.drv_id = "";
                psg.status = "MANUAL";
                psg.deniedTaxiIds = [];
                psg.save(function(err, response) {
                  if (err) {
                    console.log("Error Writing Passenger");
                  } else {
                    console.log("Success Change Passenger Status To Manual.");
                  }
                });
              }
            }
          );
        }
      }
    );
  };
}
***********************/
/* edit */