var Splash = new function () {

    this.options = {
        img: "taxi_going_1.gif",
        timeout: 300
    };


    this.setOption = function(options) {
        if (options !== undefined && typeof options == 'object') {
            $.extend( this.options, options );
        }
    }

    this.show = function () {
        $('body').find('.splash').remove();
        var splash = '<div class="splash"><img src="assets/plugins/splash-mobi/' + this.options.img + '" /></div>';
        $('body').append(splash);
    };

    this.hide = function () {
        setTimeout(function () {
            $('body').find('.splash').remove();
        }, this.options.timeout);
    };

    this.fadeOut = function() {
        $('body').find('.splash').fadeOut();
    }
}