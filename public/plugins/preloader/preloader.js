var Preloader = new function () {

    this.options = {
        timeout: 1000,
        img: "Preloader_2.gif",
        className: 'la-ball-clip-rotate la-dark la-3x',
        //color: '#2A2C36'
    };


    this.setOption = function(options) {
        if (options !== undefined && typeof options == 'object') {
            $.extend( this.options, options );
        }
    }

    this.show = function () {
        $('body').find('.preloader').remove();
        // var preloader = '<div class="preloader"><div class="loader"><div class="' + this.options.className + '"><div></div></div>';
        var preloader = '<div class="preloader"><div class="loader"><span class="glyphicon glyphicon-refresh fa-spin"></span> กำลังโหลด...</div>';
        $('body').append(preloader);
    };

    this.hide = function () {
        setTimeout(function () {
            $('body').find('.preloader').remove();
        }, this.options.timeout);
    };
}