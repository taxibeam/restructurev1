var AdminMonitorAppValues = {
	map: {
		baseZoom: 14,
	},
	service: {
		ubeam: {
			searchdrv: "../service/ubeam/searchdrv",
			assigndrvtopsg: "../service/ubeam/assigndrvtopsg",
			checkdrvstatus: "../service/ubeam/checkdrvstatus"
		},
		drivercs: {
			all: "../service/drivercs/all"
		}
	}
};