var HttpService = function(serviceListener) {


	function initialize() {
		$.extend(this, {}, serviceListener);
	}


	this.post = function(url, data, callback) {

		var preloader = getApplicationContext("Preloader");

		if(typeof url !== "string") { return; }
		if(typeof data !== "object") { return; }

		var onAlways = function() {
			preloader.hide();
		}

		var onFail = function() {
			if (this.connection_error_time < 1) {
				this.connection_error_time++;
			}
			else if (this.connection_error_time >= 1) {

				this.connection_error_time = 0;
				this.onConnectionLost();
			}
		}

		$.post(url, data, function (data) {
			if(callback !== undefined && typeof callback === "function") {
				callback(data);
			}
		})
		.fail(onFail)
		.always(onAlways);
	}


	this.registerService = function(serviceListener) {
		
		$.extend(this, {}, serviceListener);
	}


	var ServiceListener = function(ApplicationContext, services) {

		if ( this === window ) throw "InitializeError :: Cannot direct use this without new class.";

		return $.extend(this, {}, services);
	};


	initialize();
	this.ServiceListener = ServiceListener;
};