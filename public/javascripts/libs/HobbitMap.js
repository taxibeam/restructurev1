var HobbitMap = function(data) {
	
	this.Map = {};
	
	this.options = $.extend({}, data);

	createMap = function(Map) {

		Map = H.map('map');

		Map.toggle3D();
		Map.setBasemap(3);
		Map.setMaxZoom(18);
		Map.setLanguage("TH");

		Map.control.locate.disable();

		return Map;
	}

	return createMap(this.Map);
};