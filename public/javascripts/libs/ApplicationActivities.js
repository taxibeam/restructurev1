define([

	"libs/defineValues",
	"libs/HttpService",
	"libs/HttpConnectionHandler",
	"libs/Preloader",

	], function() {

});


var ApplicationActivities = function(options, Application) {

	var CONSTRAINTS = {};

	this.options = {
		serviceListener: {},
		preloader: {
			timeout: 700,
			className: 'la-ball-clip-rotate la-dark',
			color: '#B21616'
		}
	};

	this.options = $.extend(this.options, options);

	this.HttpService = $.extend(new HttpService(this.options.serviceListener), {});
	this.Preloader = $.extend(new Preloader(this.options.preloader, {}));

	this.intervalManager = function (flag, action, time) {
		if (flag) {
			return setInterval(action, time);
		}
		else {
			typeof action !== 'undefined' && clearInterval(action);
		}
	}

	this.getConstraints = function() {
		return CONSTRAINTS;
	}

	this.setConstraints = function(data) {

		if(!data instanceof defineValues) throw "TypeError :: data is not defineValues";

		$.extend(CONSTRAINTS, {}, data);
	}

	this.onCreate = function() {
		this.Preloader.show();
	}

	window.C = CONSTRAINTS;
	window.applicationContext = Application;
	window.getApplicationContext = function(context) {

		if(typeof context !== "string") { 
			return window.applicationContext;
		}

		return applicationContext[context];
	}
};