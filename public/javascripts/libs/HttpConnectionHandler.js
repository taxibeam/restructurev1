var HttpConnectionHandler = function(data) {

	this.data = $.extend({
		limit_error: 1,
		timeout_test_connection: 5000,
		url: null,
		data: {}
	}, data);

	this.connection_error_time = 0;

	var initial = function() {
		if(('HttpConnectionHandler' in window)) {

			console.warn('HttpConnectionHandler cannot be re-create or has more than one in window object');
			return;
		}

		this.testConnection = testConnection;
		this.onConnectionLost = onConnectionLost;
	}

	var onConnectionLost = function() {

		var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

		$('body').find("> .connect_lost_message").remove();
		$('body').append(message);
		$('body').find("> .connect_lost_message").fadeIn('fast');

		if (this.intervalId == undefined) {
			this.intervalId = App.intervalManager(true, this.testConnection, 5000);
		}
	}

	var testConnection = function () {
		$.post(this.URLTestConnection, { }, function (data) {
			clearInterval(Http.intervalId);
			App.intervalManager(false, Http.intervalId);
			delete Http.intervalId;
			Http.connection_error_time = 0;
			$('body').find("> .connect_lost_message").remove();
		});
	}

	initial();
};