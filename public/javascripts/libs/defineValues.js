var defineValues = function(objectValues) {

	if ( this === window ) throw "InitializeError :: Cannot direct use this without new class.";

	return $.extend(this, {}, objectValues);
}