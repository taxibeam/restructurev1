var Preloader = function(data) {

    this.options = {
        timeout: 1000,
        img: "Preloader_2.gif",
        className: 'la-ball-clip-rotate la-dark la-3x',
        //color: '#2A2C36'
    };

    $.extend(this.options, {}, data);

    this.setOption = function(options) {
        if (options !== undefined && typeof options == 'object') {
            $.extend( this.options, options );
        }
    }

    this.show = function () {
        $('body').find('.preloader').remove();
        // var preloader = '<div class="preloader"><div class="loader"><div class="' + this.options.className + '"><div></div></div>';
        var preloader = '<div class="preloader"><div class="loader"><span class="glyphicon glyphicon-refresh fa-spin"></span> กำลังโหลด...</div>';
        $('body').append(preloader);
    };

    this.hide = function (time) {
        
        time = typeof time === 'number' ? time : this.options.timeout;

        setTimeout(function () {
            $('body').find('.preloader').remove();
        },  time);
    };
};