
var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    serviceURI: {
        getAllAnnouncement: serviceBaseURI + '/service/ubeam/announcement/all',
        addAnnouncement: serviceBaseURI + '/service/ubeam/announcement/add',
        editAnnouncement: serviceBaseURI + '/service/ubeam/announcement/edit',
        deleteAnnouncement: serviceBaseURI + '/service/ubeam/announcement/delete',
        getInitiateList: serviceBaseURI + '/service/ubeam/getinitiatelist',
        getAdvanceList: serviceBaseURI + '/service/ubeam/getadvancelist',
        getDependingList: serviceBaseURI + '/service/ubeam/getdpendinglist',
        getAssignList: serviceBaseURI + '/service/ubeam/getassignlist',
        getReAssignJobDetail: serviceBaseURI + '/service/ubeam/getreassignjobdetail',
        countJobPerDrv: serviceBaseURI + '/service/ubeam/CountJobPerDrv',
        searchFinishList: serviceBaseURI + '/service/ubeam/searchFinishList',
        addJobList: serviceBaseURI + '/service/ubeam/addjoblist',
        editJobList: serviceBaseURI + '/service/ubeam/editjoblist',
        getPOIRecommended: serviceBaseURI + '/service/ubeam/getpoirecommended',
        searchNameCar: serviceBaseURI + '/service/ubeam/searchnamecar',
        searchDriver: serviceBaseURI + '/service/ubeam/searchdrv',
        getPassengerDetail: serviceBaseURI + '/service/ubeam/getPassengerDetail',
        getPOIandParking: serviceBaseURI + '/service/ubeam/getPOIandParking',
        cancelPSGDRV: serviceBaseURI + '/service/ubeam/cancelpsgdrv/',
        deleteJob: serviceBaseURI + '/service/ubeam/deletejob',
        countTaxi: serviceBaseURI + '/service/ubeam/countTaxi',
        assignDRVtoPSG: serviceBaseURI + '/service/ubeam/assigndrvtopsg',
        mostHitStartPlace: serviceBaseURI + '/service/ubeam/MostHitStartPlace',
        checkDRVStatus: serviceBaseURI + '/service/ubeam/checkdrvstatus',
        mostHitDestinationPlace: serviceBaseURI + '/service/ubeam/MostHitDestinationPlace',
        mostHitHours: serviceBaseURI + '/service/ubeam/MostHitHours',
        addNewPOI: serviceBaseURI + '/passenger/poi/add/',
        getPOIsByPhone: serviceBaseURI + '/passenger/poi/list/byphone/',
        FindDrvinRadian: serviceBaseURI + '/service/ubeam/FindDrvinRadian',
        googleMapIconMarkerBlueDot: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'
    },
    driverImageURL: 'http://callcenter.taxi-beam.com',
    daterangepicker: {
        options: {
            opens: "left",
            timePicker: false,
            autoApply: false,
            locale: {
                "format": "DD/MM/YYYY HH:mm",
                "separator": " - ",
                "applyLabel": "ตกลง",
                "cancelLabel": "ยกเลิก",
                "fromLabel": "จาก",
                "toLabel": "ถึง",
                "customRangeLabel": "กำหนดเอง",
                "daysOfWeek": [
                    "อ.",
                    "จ.",
                    "อ.",
                    "พ.",
                    "พฤ.",
                    "ศ.",
                    "ส."
                ],
                "monthNames": [
                    "มกราคม",
                    "กุมภาพันธ์",
                    "มีนาคม",
                    "เมษายน",
                    "พฤษภาคม",
                    "มิถุนายน",
                    "กรกฎาคม",
                    "สิงหาคม",
                    "กันยายน",
                    "ตุลาคม",
                    "พฤศจิกายน",
                    "ธันวาคม"
                ],
                "firstDay": 1
            },
            alwaysShowCalendars: false,
            ranges: {
                'วันนี้': [moment(), moment()],
                'เมื่อวาน': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 วันที่แล้ว': [moment().subtract(6, 'days'), moment()],
                '30 วันที่แล้ว': [moment().subtract(29, 'days'), moment()],
                'เดือนนี้': [moment().startOf('month'), moment().endOf('month')],
                'เดือนที่แล้ว': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }
    }
};

var App = {
    bootstrap: function () {
        Socket.initial();
        JQuery.NumberOnlyInit();
        iOSwitchery.initial();
        DateTimePicker.initial();
        Form.createTaskForm.initial();
        Modal.pickUpPOIModal.initial();
        HistoryTable.initial();
    }
}

var JQuery = {
    NumberOnlyInit: function () {
        jQuery.fn.ForceNumericOnly = function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.charCode || e.keyCode || 0 || e.which;
                    return (
                        key == 8 ||
                        key == 9 ||
                        key == 13 ||
                        key == 27 ||
                        key == 46 ||
                        key == 190 ||
                        (key >= 35 && key <= 40) ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
                });
            });
        };
    }
};

var LocalStorageManager = {
    initial: function () {

        Array.prototype.move = function (old_index, new_index) {
            while (old_index < 0) {
                old_index += this.length;
            }
            while (new_index < 0) {
                new_index += this.length;
            }
            if (new_index >= this.length) {
                var k = new_index - this.length;
                while ((k--) + 1) {
                    this.push(undefined);
                }
            }
            this.splice(new_index, 0, this.splice(old_index, 1)[0]);
            return this; // for testing purposes
        };
    },
    get: function (name) {
        return localStorage.getItem(name);
    },
    new: function (key, data) {
        return localStorage.setItem(key, data);
    },
    add: function (key, data) {
        var _data = localStorage.getItem(key);
        _data ? localStorage.setItem(key, _data + ",," + data) : localStorage.setItem(key, data);
    },
    append: function (key, data) {
        var _data = localStorage.getItem(key);
        return localStorage.setItem(key, _data + ",," + data);
    },
    prepend: function (key, data) {
        var _data = localStorage.getItem(key);
        return localStorage.setItem(key, data + ",," + _data);
    },
    getAsArray: function (key) {
        var poi = localStorage.getItem(key);
        return poi ? poi.split(",,") : [];
    }
}

var Notification = {
    show: function (message) {
        options = {
            title: message.title,
            text: message.text,
            sticky: message.sticky ? message.sticky : false,
            time: message.time ? message.time : 2000,
            class_name: 'gritter-' + (!message.$class ? 'warning' : message.$class),
            position: message.position ? message.position : 'bottom-right'
        };

        $.gritter.add(options);
    },
    defaultSuccess: function (title, text) {
        Notification.remove();
		
        $.gritter.add({
            title: !title ? 'Success!' : title,
            text: !text ? 'คำสั่ง ทำงานเรียบร้อย' : text,
            sticky: false,
            time: 1500,
            class_name: 'gritter-success',
            position: 'bottom-right'
        });
        // window.open('','_parent',''); 
        // window.close(); 
        window.open('closeopener','_self',''); 
        window.close(); 
        //window.self.close()
        // window.opener.justClose();
        
    },
    defaultError: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Server Error!' : title,
            text: !text ? 'Pls. refresh page and then try it again.' : text,
            sticky: false,
            time: 2500,
            class_name: 'gritter-danger',
            position: 'bottom-right'
        });
    },
    removeAll: function () {
        $.gritter.removeAll({
            time: 0,
            before_close: function (e) {
            },
            after_close: function () {
            }
        });
    },
    remove: function () {
        $("#gritter-notice-wrapper").remove();
    }
};

var Http = {
    connection_error_time: 0,
    getJson: function (url, options) {

        if (options.loader !== undefined && !options.loader) { Preloader.show(); }

        $.getJSON(url, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        })
        .fail(function () {
            Notification.defaultError();
        })
        .always(function () {
            Preloader.hide();
        });
    },
    post: function (url, options) {

        if (options.loader || options.loader == undefined) { Preloader.show(); }

        if (options.data == undefined) { options.data = {}; }
        $.post(url, options.data, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        })
        .fail(function () {
            if (Http.connection_error_time < 1) {
                Http.connection_error_time++;
            }
            else if (Http.connection_error_time >= 1) {

                Http.connection_error_time = 0;
                Http.onConnectionLost();
            }
        })
        .always(function () {
            Preloader.hide();
        });
    },
    onConnectionLost: function () {

        if( $('body').find("> .connect_lost_message").size() === 0 ) { 

            var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

            $('body').find("> .connect_lost_message").remove();
            $('body').append(message);
            $('body').find("> .connect_lost_message").fadeIn('fast');
        }

        if (this.intervalId == undefined) {
            this.intervalId = App.intervalManager(true, Http.testConnection, 5000);
        }
    },
    testConnection: function () {

        $.post(CONSTRAINTS.serviceURI.getPassengerDetail, { "psg_id": "" }, function (data) {

            clearInterval(Http.intervalId);

            App.intervalManager(false, Http.intervalId);

            delete Http.intervalId;

            Http.connection_error_time = 0;

            $('body').find("> .connect_lost_message").remove();
        });
    }
};

var DateTimePicker = {
    minHour: 3,
    picker: $('#createdjob'),
    initial: function () {

        var minDate = new Date().getTime() + (DateTimePicker.minHour * 60 * 60 * 1000);

        DateTimePicker.picker.datetimepicker({
            inline: true,
            format: "MM/dd/YYYY HH:mm",
            minDate: new Date(minDate)
        });
    },
    setMinDate: function () {
        var minDate = new Date().getTime() + (DateTimePicker.minHour * 60 * 60 * 1000);
        DateTimePicker.picker.data("DateTimePicker").minDate(new Date(minDate));
    }
};

var iOSwitchery = {
    initial: function () {
        this.create();
        this.eventHandler();
    },
    eventHandler: function () {
        $("#createTaskForm").find('.js-switch').on('change', function (event) {
            this.checked ? $('.datetime-overlay').fadeOut() : $('.datetime-overlay').fadeIn();
        });

        $(".taxi-counter").find(".js-switch").on('change', function (event) {
            var button = $(event.target).closest(".list-group-item");
            if (button.hasClass("OFF")) {
                Taxi.toggleStatus("OFF", this.checked);
            }
            else if (button.hasClass("ON")) {
                Taxi.toggleStatus("ON", this.checked);
            }
            else if (button.hasClass("WAIT")) {
                Taxi.toggleStatus("WAIT", this.checked);
            }
            else if (button.hasClass("DPENDING")) {
                Taxi.toggleStatus("DPENDING", this.checked);
            }
            else if (button.hasClass("BUSY")) {
                Taxi.toggleStatus("BUSY", this.checked);
            }
            else if (button.hasClass("PICK")) {
                Taxi.toggleStatus("PICK", this.checked);
            }
            else if (button.hasClass("ASSIGNED")) {
                Taxi.toggleStatus("ASSIGNED", this.checked);
            }
            else if (button.hasClass("BROADCAST")) {
                Taxi.toggleStatus("BROADCAST", this.checked);
            }
            else if (button.hasClass("PARKING")) {
                LandMark.toggleDisplay(this.checked);
            }
        });
    },
    create: function () {

        var html = $("#createTaskForm").find('.js-switch')[0];
        var switchery = new Switchery(html);

        $(".taxi-counter").find(".js-switch").each(function (i, html) {
            var switchery = new Switchery(html, {
                size: 'small'
            });
        });
    }
};

var Form = {
    createTaskForm: {
        form: $("#createTaskForm"),
        initial: function () {
            Form.createTaskForm.onSubmit();
            Form.createTaskForm.autocompleteInit();
            // Form.createTaskForm.form.find("#pass-phone-inp").ForceNumericOnly();
            Form.createTaskForm.form.find("#taxiAmount").ForceNumericOnly();
            Form.createTaskForm.setUpFieldInputStep();

            Form.createTaskForm.form.find("#pass-from-inp, #pass-dest-inp").on("focusin", function () {
                var phoneNo = Form.createTaskForm.form.find("#pass-phone-inp").val();
                if (phoneNo) {
                    $(this).autocomplete().setOptions({ serviceUrl: CONSTRAINTS.serviceURI.getPOIsByPhone + phoneNo + '/garage=1' });
                } else {
                    $(this).autocomplete().setOptions({ serviceUrl: CONSTRAINTS.serviceURI.getPOIsByPhone + 'UNKNOW/garage=1' });
                }
            });
        },
        setUpFieldInputStep: function () {

            var form = Form.createTaskForm.form;

            form.find("#pass-phone-inp").on("keyup", function (event) {

                var code = event.keyCode || event.which;

                if (code === 13) {

                    if (!Form.createTaskForm.form.valid()) { $(this).focus(); return; }
                    Form.createTaskForm.form.find("#pass-from-inp").focus();
                }

                return;
            });

            form.find("#pass-from-inp").on("keyup", function (event) {

                var code = event.keyCode || event.which;
                var error = $(this).hasClass('error');

                if (code === 13 && !error) {
                    Form.createTaskForm.form.find("#pass-dest-inp").focus();
                }

                return;
            });

            form.find("#pass-dest-inp").on("keyup", function (event) {

                var code = event.keyCode || event.which;

                (Form.createTaskForm.form.valid() && code === 13) && Form.createTaskForm.customSubmit();
                return;
            });

            form.find("button#saveBtn").on("click", function (event) {

                if (!Form.createTaskForm.form.valid()) {
                    return;
                }

                if (USER_DATA.broadcast === "true") {
                    if (Form.createTaskForm.hasLatLng() && form.find("#pass-from-inp").val() !== "") {
                        Form.createTaskForm.customSubmit();
                    } else {
                        Modal.messageBoxModal.setTitle("คำเตือน");
                        Modal.messageBoxModal.setMessage("กรุณาปักหมุดและระบุจุดรับให้เรียบร้อย");
                        Modal.messageBoxModal.open();
                        Form.createTaskForm.form.find("#pass-from-inp").focus();
                        return;
                    }
                } else {
                    Form.createTaskForm.customSubmit();
                }
                return;
            });

            form.find("button#cancelBtn").on("click", function (event) {
                window.open('closeopener','_self',''); 
                window.close(); 
                return;
            });
        },
        hasLatLng: function () {
            var form = Form.createTaskForm.form;
            var lat = form.find("#pass-from-inp").attr("data-lat");
            var lng = form.find("#pass-from-inp").attr("data-lng");

            return (lat || lng);
        },
        saveToLocalStorage: function (data) {

            // Current Address
            var hasStartPOI = false;
            var startPOIFoundIndex = -1;
            var passStartPOIArray = LocalStorageManager.getAsArray("pass_poi_start");
            passStartPOIArray.forEach(function (element, index) {
                var passStartPOIObj = JSON.parse(element);
                if (passStartPOIObj.value === data.curaddr) {
                    hasStartPOI = true;
                    startPOIFoundIndex = index;
                    return;
                }
            });

            if (hasStartPOI) {
                passStartPOIArray.splice(startPOIFoundIndex, 1);
                LocalStorageManager.new("pass_poi_start", passStartPOIArray.join(",,"));
            }
            LocalStorageManager.add("pass_poi_start", JSON.stringify({
                value: data.curaddr,
                data: $.extend({}, data, { curloc: [data.curlng, data.curlat] })
            }));

            // Limit 5 history.
            if (LocalStorageManager.getAsArray("pass_poi_start").length > 5) {
                passStartPOIArray = LocalStorageManager.getAsArray("pass_poi_start");
                passStartPOIArray.splice(0, 1);
                LocalStorageManager.new("pass_poi_start", passStartPOIArray.join(",,"));
            }

            // Phone
            var hasPhone = false;
            var phoneFoundIndex = -1;
            var passPhoneArray = LocalStorageManager.getAsArray("pass_phone");
            passPhoneArray.forEach(function (element, index) {
                var pass_phoneObj = JSON.parse(element);
                if (pass_phoneObj.value === data.phone) {
                    hasPhone = true;
                    phoneFoundIndex = index;
                    return;
                }
            });

            if (hasPhone) {
                passPhoneArray.splice(phoneFoundIndex, 1);
                LocalStorageManager.new("pass_phone", passPhoneArray.join(",,"));
            }
            LocalStorageManager.add("pass_phone", JSON.stringify({
                value: data.phone,
                data: {
                    phone: data.phone,
                    date: new Date().getTime()
                }
            }));

            // Limit 20 history.
            if (LocalStorageManager.getAsArray("pass_phone").length > 20) {
                passPhoneArray = LocalStorageManager.getAsArray("pass_phone");
                passPhoneArray.splice(0, 1);
                LocalStorageManager.new("pass_phone", passPhoneArray.join(",,"));
            }
        },
        loadPassengerStartPOI: function () {
            if (!localStorage.pass_poi_start) { return [] }
            var data = localStorage.pass_poi_start.split(",,");
            var dataArray = [];
            data.forEach(function (item, index) {
                dataArray.push(JSON.parse(item));
            });

            return dataArray;
        },
        loadPassengerPhone: function () {
            if (!localStorage.pass_phone) { return [] }
            var data = localStorage.pass_phone.split(",,");
            var dataArray = [];
            data.forEach(function (item, index) {
                dataArray.push(JSON.parse(item));
            });

            return dataArray;
        },
        customSubmit: function () {

            var form = Form.createTaskForm.form;
            var data = {};
            data.phone = form.find("#pass-phone-inp").val();
            data.autophone = form.find("#pass-autophone-inp").val();
            data.curaddr = form.find("#pass-from-inp").val();
            data.destination = form.find("#pass-dest-inp").val();
            data.detail = form.find("#pass-desc-txt").val();
            data.amount = form.find("#taxiAmount").val();
            data.ccstation = form.find("#ccstation").val();
            data.isContractJob = form.find("[name='isContractJob']:checked").val();
            data.provincearea = {
                code: form.find("#customer-province-list option:selected").val(),
                nameTH: form.find("#customer-province-list option:selected").html(),
                LatLng: form.find("#customer-province-list option:selected").attr("data-loc").split(",").map(Number)
            };
            data.username = USER_DATA.username;
            data.cgroup = USER_DATA.group;

            data.jobtype = form.find("#jobType").is(':checked') ? "ADVANCE" : "QUEUE";
            if (data.jobtype == "ADVANCE") {
                data.createdjob = new Date(DateTimePicker.picker.data("DateTimePicker").date()).getTime();
            }

            data.curlat = parseFloat(form.find("#pass-from-inp").attr("data-lat"));
            data.curlng = parseFloat(form.find("#pass-from-inp").attr("data-lng"));

            if (isNaN(data.curlat)) {
                data.curlat = 0.0;
            }
            if (isNaN(data.curlng)) {
                data.curlng = 0.0;
            }

            if (Socket.hasLostConnect) {

                var timestamp = new Date().getTime();

                data._id = timestamp;
                data.createdjob = timestamp;
                return;
            }

            Form.createTaskForm.saveToLocalStorage(data);
            form.find("#pass-from-inp").data(data);

            Http.post(CONSTRAINTS.serviceURI.addJobList, {
                data: data,
                onSuccess: function (response) {
                    if (response.status) {
                        Notification.defaultSuccess();
                        Form.createTaskForm.clearFormData();
                    } else {
                        Notification.defaultError();
                    }

                    Form.createTaskForm.form.find("#pass-phone-inp").focus();
                    DateTimePicker.setMinDate();
                }
            });
            Form.createTaskForm.savePOI(data);
            return;
        },
        onSubmit: function () {
            Form.createTaskForm.form.validate({
                rules: {
                    phone: {
                        required: true,
                        number: true
                    },
                    taxiAmount: { required: true }
                },
                submitHandler: function (form, event) {

                    event.preventDefault();
                    return;
                }
            });
        },
        clearFormData: function () {
            var form = Form.createTaskForm.form;
            form.find("#pass-phone-inp").val("");
            form.find("#pass-from-inp").val("");
            form.find("#pass-dest-inp").val("");
            form.find("#pass-desc-txt").val("");
            form.find("#taxiAmount").val("1");
            form.find("#pass-from-inp").removeAttr("data-lat");
            form.find("#pass-from-inp").removeAttr("data-lng");
            form.find("#pickUpPOIBtn").removeClass("valid");
            Modal.pickUpPOIModal.clearData();
        },
        autocompleteInit: function () {

            var optionPassengerPhone = {
                lookup: [],
                minChars: 0,
                onSearchStart: function (query) {
                    var input = $(this),
                        instance = input.data('autocomplete');

                    instance.options.lookup = Form.createTaskForm.loadPassengerPhone();
                },

                onSelect: function (suggestion) {
                    var validator = Form.createTaskForm.form.validate();
                    validator.form();
                }
            }

            var optionPassengerStartPOI = {
                type: 'POST',
                paramName: 'query',
                noCache: true,
                preserveInput: true,
                preventBadQueries: true,
                minChars: 0,
                serviceUrl: '',
                transformResult: function (response, originalQuery) {

                    var response = JSON.parse(response);
                    if (response.status && response.data) {

                        if (!Form.createTaskForm.form.find("#pass-phone-inp").val() || !response.hasHistory) {
                            Form.createTaskForm.loadPassengerStartPOI().forEach(function (item, index) {
                                response.data.splice(0, 0, { name: item.value, data: item.data, type: "TEMP" });
                            });
                        }

                        return {
                            suggestions: $.map(response.data, function (dataItem) {
                                if (!dataItem) { return; }
                                return {
                                    value: dataItem.name || dataItem.parkingname,
                                    data: dataItem.type === "TEMP" ? dataItem.data : dataItem,
                                    ID: dataItem._id,
                                    type: dataItem.type === "TEMP" ? "TEMP" : dataItem.name ? "USER" : "LOCATION"
                                };
                            })
                        };
                    } else {
                        return {
                            suggestions: {}
                        };
                    }
                },

                onSelect: function (suggestion) {

                    this.value = suggestion.value;

                    if (!suggestion.data.curloc) {
                        return;
                    }

                    var lng = parseFloat(suggestion.data.curloc[0]);
                    var lat = parseFloat(suggestion.data.curloc[1]);

                    if (lat > 0 && lng > 0) {
                        $(this).attr("data-lat", suggestion.data.curloc[1]);
                        $(this).attr("data-lng", suggestion.data.curloc[0]);
                        Form.createTaskForm.form.find("#pickUpPOIBtn").addClass("valid");
                    } else {
                        $(this).removeAttr("data-lat");
                        $(this).removeAttr("data-lng");
                        Form.createTaskForm.form.find("#pickUpPOIBtn").removeClass("valid");
                    }

                    if (Form.createTaskForm.form.find("#pass-phone-inp").val()) {
                        Form.createTaskForm.form.find("#pass-desc-txt").val(suggestion.data.detail || "");
                        Form.createTaskForm.form.find("#pass-dest-inp").val(suggestion.data.destination || "");
                    }
                },

                onSearchStart: function (query) {

                    function isInValidValue(keyword) {
                        var rex = /\*[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g;
                        return rex.test(keyword);
                    }

                    if (isInValidValue(query.query)) {
                        return;
                    }

                    var input = $(this),
                        instance = input.data('autocomplete');

                    var phoneNo = Form.createTaskForm.form.find("#pass-phone-inp").val();

                    if (phoneNo) {
                        instance.options.serviceUrl = '/passenger/poi/list/byphone/' + phoneNo + '/garage=1';
                    } else {
                        instance.options.serviceUrl = '/passenger/poi/list/byphone/UNKNOW/garage=1';
                    }
                },

                onSearchComplete: function (query, suggestions) {
                    if (!query) {
                        $(this).removeAttr("data-lat");
                        $(this).removeAttr("data-lng");
                        Form.createTaskForm.form.find("#pickUpPOIBtn").removeClass("valid");
                    }
                },

                beforeRender: function (container, data) {
                    data.forEach(function (item, index) {

                        var element = $(container).find(".autocomplete-suggestion")[index];
                        switch (item.type) {
                            case "USER":
                                $(element).addClass("user").prepend('<i class="material-icons">&#xE8A6;</i>');
                                break;
                            case "LOCATION":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("location").prepend('<i class="material-icons">&#xE55F;</i>');
                                break;
                            case "TEMP":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("temp").prepend('<i class="material-icons">&#xE889;</i>');
                                break;
                        }
                    });
                }
            };

            var optionPassengerEndPOI = {
                type: 'POST',
                paramName: 'query',
                noCache: true,
                preserveInput: true,
                preventBadQueries: true,
                minChars: 0,
                serviceUrl: '',
                transformResult: function (response, originalQuery) {

                    var response = JSON.parse(response);
                    if (response.status && response.data) {

                        if (!Form.createTaskForm.form.find("#pass-phone-inp").val() || !response.hasHistory) {
                            Form.createTaskForm.loadPassengerStartPOI().forEach(function (item, index) {
                                response.data.splice(0, 0, { name: item.value, data: item.data, type: "TEMP" });
                            });
                        }

                        return {
                            suggestions: $.map(response.data, function (dataItem) {
                                if (!dataItem) { return; }
                                return {
                                    value: dataItem.name || dataItem.parkingname,
                                    data: dataItem.type === "TEMP" ? dataItem.data : dataItem,
                                    ID: dataItem._id,
                                    type: dataItem.type === "TEMP" ? "TEMP" : dataItem.name ? "USER" : "LOCATION"
                                };
                            })
                        };
                    } else {
                        return {
                            suggestions: {}
                        };
                    }
                },

                onSelect: function (suggestion) {
                    this.value = suggestion.value;
                },

                onSearchStart: function (query) {

                    function isInValidValue(keyword) {
                        var rex = /\*[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g;
                        return rex.test(keyword);
                    }

                    if (isInValidValue(query.query)) {
                        return;
                    }

                    var input = $(this),
                        instance = input.data('autocomplete');

                    var phoneNo = Form.createTaskForm.form.find("#pass-phone-inp").val();

                    if (phoneNo) {
                        instance.options.serviceUrl = '/passenger/poi/list/byphone/' + phoneNo + '/garage=1';
                    } else {
                        instance.options.serviceUrl = '/passenger/poi/list/byphone/UNKNOW/garage=1';
                    }
                },

                beforeRender: function (container, data) {
                    data.forEach(function (item, index) {

                        var element = $(container).find(".autocomplete-suggestion")[index];
                        switch (item.type) {
                            case "USER":
                                $(element).addClass("user").prepend('<i class="material-icons">&#xE8A6;</i>');
                                break;
                            case "LOCATION":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("location").prepend('<i class="material-icons">&#xE55F;</i>');
                                break;
                            case "TEMP":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("temp").prepend('<i class="material-icons">&#xE889;</i>');
                                break;
                        }
                    });
                }
            };

            Form.createTaskForm.form.find('#pass-phone-inp').autocomplete(optionPassengerPhone);
            Form.createTaskForm.form.find('#pass-from-inp').autocomplete(optionPassengerStartPOI);
            Form.createTaskForm.form.find('#pass-dest-inp').autocomplete(optionPassengerEndPOI);
        },
        savePOI: function (taskData) {

            if (!taskData) {
                return;
            }

            var formatted_address = "";
            if (Modal.pickUpPOIModal.currentPickupData) {
                formatted_address = Modal.pickUpPOIModal.currentPickupData.formatted_address;
            }

            var POIData = {
                "type": "start",
                "name": taskData.curaddr,
                "formatter": formatted_address,
                "destination": taskData.destination,
                "detail": taskData.detail,
                "curloc": [
                    taskData.curlng,
                    taskData.curlat
                ]
            }
            $.post(CONSTRAINTS.serviceURI.addNewPOI + taskData.phone, POIData, function (response) { });
        }
    }
};

var Modal = {
    pickUpPOIModal: {
        modal: $('#pickUpPOIModal'),
        map: null,
        marker: null,
        poiMarker: null,
        currentPickupData: null,
        currentPickupImage: CONSTRAINTS.serviceURI.googleMapIconMarkerBlueDot,
        initial: function () {

            Modal.pickUpPOIModal.modal.on('show.bs.modal', function (e) {
            });

            Modal.pickUpPOIModal.modal.on('shown.bs.modal', function (e) {

                if (Modal.pickUpPOIModal.map === null) {
                    Modal.pickUpPOIModal.initMap();
                }

                var lat = Form.createTaskForm.form.find("#pass-from-inp").attr("data-lat");
                var lng = Form.createTaskForm.form.find("#pass-from-inp").attr("data-lng");

                if (lat && lng) {

                    if (Modal.pickUpPOIModal.marker !== null) {
                        Modal.pickUpPOIModal.marker.setPosition(new google.maps.LatLng(lat, lng));
                        Modal.pickUpPOIModal.marker.setMap(Modal.pickUpPOIModal.map);
                        Modal.pickUpPOIModal.modal.find(".current-lat-lng").val(lat + ", " + lng);
                    }
                    else if (Modal.pickUpPOIModal.marker === null) {
                        Modal.pickUpPOIModal.placeMarker(Modal.pickUpPOIModal.map, new google.maps.LatLng(lat, lng));
                        Modal.pickUpPOIModal.marker.setMap(Modal.pickUpPOIModal.map);
                        Modal.pickUpPOIModal.modal.find(".current-lat-lng").val(lat + ", " + lng);
                    }
                    Modal.pickUpPOIModal.map.setZoom(15);
                    Modal.pickUpPOIModal.map.panTo({ lat: parseFloat(lat), lng: parseFloat(lng) });
                } else {
                    var placeName = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
                    Modal.pickUpPOIModal.getGeocodeByPlaceName(placeName);
                    Modal.pickUpPOIModal.map.panTo({ lat: USER_CURRENT_LOCATION[0], lng: USER_CURRENT_LOCATION[1] });
                    Modal.pickUpPOIModal.map.setZoom(13);
                }
                Modal.pickUpPOIModal.modal.find("#pac-input").val("");
                Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text("");
            });

            Modal.pickUpPOIModal.modal.on('hidden.bs.modal', function (e) {
            });

            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").on("click", function () {
                Modal.pickUpPOIModal.modal.find("#pac-input").val($(this).text());
            });

            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel #confirm").on("click", Modal.pickUpPOIModal.onSubmit);
        },
        open: function (event) {
            Modal.pickUpPOIModal.fillData();
            Modal.pickUpPOIModal.modal.modal("show");
        },
        close: function (event) {
            Modal.pickUpPOIModal.modal.modal("hide");
        },
        initMap: function () {

            Modal.pickUpPOIModal.map = new google.maps.Map(document.getElementById('pickUpPOIMap'), {
                center: { lat: USER_CURRENT_LOCATION[0], lng: USER_CURRENT_LOCATION[1] },
                zoom: 12
            });

            Modal.pickUpPOIModal.map.setClickableIcons(false);

            Modal.pickUpPOIModal.placeMarker(Modal.pickUpPOIModal.map, Modal.pickUpPOIModal.map.getCenter());

            var input = document.getElementById('pac-input');

            Modal.pickUpPOIModal.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input, {
                types: []
            });

            autocomplete.bindTo('bounds', Modal.pickUpPOIModal.map);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (place.geometry && place.geometry.viewport) {
                    Modal.pickUpPOIModal.map.fitBounds(place.geometry.viewport);
                    moveMarker(place.name, place.geometry.location);
                }
                else if (place.geometry && place.geometry.location) {
                    Modal.pickUpPOIModal.map.setCenter(place.geometry.location);
                    Modal.pickUpPOIModal.map.setZoom(17);
                    moveMarker(place.name, place.geometry.location);
                }
            });

            // http://jsfiddle.net/dodger/pbbhH/
            $(input).one("focusin", function () {
                $(document).one("keypress", function (e) {
                    if (e.which == 13) {
                        var firstResult = $(".pac-container .pac-item:first").text();

                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ "address": firstResult, 'partialmatch': true, 'region': 'th' }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var lat = results[0].geometry.location.lat(),
                                    lng = results[0].geometry.location.lng(),
                                    placeName = results[0].address_components[0].long_name,
                                    latlng = new google.maps.LatLng(lat, lng);

                                moveMarker(placeName, latlng);
                                $("input").val(firstResult);
                            }
                        });
                    }
                });
            });

            function moveMarker(placeName, latlng) {
                Modal.pickUpPOIModal.marker.setPosition(latlng);
                Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text(placeName);

                Modal.pickUpPOIModal.modal
                    .find(".current-lat-lng")
                    .val(latlng.lat().toString().substring(0, 10) + ", " + latlng.lng().toString().substring(0, 10));
            }

            Modal.pickUpPOIModal.setOnMapClick(Modal.pickUpPOIModal.map, Modal.pickUpPOIModal.marker);
        },
        setMapCenter: function (lat, lng) {
            Modal.pickUpPOIModal.map.setCenter({ lat: lat, lng: lng });
            Modal.pickUpPOIModal.map.setZoom(18);
        },
        getGeocode: function (location) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': location, 'partialmatch': true, 'region': 'th' }, function (results, status) {
                if (status === 'OK') {

                    Modal.pickUpPOIModal.currentPickupData = results[0];

                    if (results[0]) {
                        Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text(results[0].formatted_address);
                        $("#pac-input").val(results[0].formatted_address);
                    } else {
                        console.log('No results found');
                    }
                } else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
        },
        getGeocodeByPlaceName: function (place) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'address': place, 'region': 'th' }, function (results, status) {
                if (status === 'OK') {

                    Modal.pickUpPOIModal.currentPickupData = results[0];

                    if (results[0]) {
                        Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text(results[0].formatted_address);
                        $("#pac-input").val(results[0].formatted_address);
                    } else {
                        console.log('No results found');
                    }
                } else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
        },
        setOnMapClick: function (map, marker) {
            google.maps.event.addListener(map, 'click', function (event) {
                Modal.pickUpPOIModal.placeMarker(map, event.latLng);
                Modal.pickUpPOIModal.getGeocode(event.latLng);
            });
        },
        placeMarker: function (map, location) {

            if (Modal.pickUpPOIModal.marker === null) {
                Modal.pickUpPOIModal.marker = new google.maps.Marker({
                    position: location,
                    draggable: true,
                    map: map
                });

                google.maps.event.addListener(Modal.pickUpPOIModal.marker, "dragend", function (event) {

                    Modal.pickUpPOIModal.getGeocode(event.latLng);

                    Modal.pickUpPOIModal.modal
                        .find(".current-lat-lng")
                        .val(event.latLng.lat().toString().substring(0, 10) + ", " + event.latLng.lng().toString().substring(0, 10));
                });
            } else {
                Modal.pickUpPOIModal.marker.setPosition(location);
                Modal.pickUpPOIModal.marker.setMap(Modal.pickUpPOIModal.map);
            }
            Modal.pickUpPOIModal.map.panTo(location);
            Modal.pickUpPOIModal.modal
                .find(".current-lat-lng")
                .val(location.lat().toString().substring(0, 10) + ", " + location.lng().toString().substring(0, 10));
        },
        fillData: function () {

            var currentAddress = Form.createTaskForm.form.find("#pass-from-inp").val();
            var descriptionText = Form.createTaskForm.form.find("#pass-desc-txt").val();

            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text(currentAddress);
            Modal.pickUpPOIModal.modal.find("#pass-desc-txt").val(descriptionText);
        },
        clearData: function () {
            Modal.pickUpPOIModal.modal.find("a.current-address").text("");
            Modal.pickUpPOIModal.modal.find(".current-lat-lng").val("");
            Modal.pickUpPOIModal.modal.find("p.caption.formatted_address").text("");
            Modal.pickUpPOIModal.modal.find("#pass-desc-txt").val("");
            Modal.pickUpPOIModal.modal.find("#pac-input").val("");

            if (USER_DATA.broadcast === "true" && Modal.pickUpPOIModal.marker) {
                Modal.pickUpPOIModal.marker.setMap(null);
            }
            Modal.pickUpPOIModal.currentPickupData = null;
        },
        onSubmit: function () {

            var valid = Modal.pickUpPOIModal.checkFormValue();

            if (valid) {
                var currentAddress = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
                var descriptionText = Modal.pickUpPOIModal.modal.find("#pass-desc-txt").val();

                Form.createTaskForm.form.find("#pass-from-inp").val(
                    currentAddress || Modal.pickUpPOIModal.currentPickupData.address_components[0].long_name);

                Form.createTaskForm.form.find("#pass-desc-txt").val(descriptionText);
                Form.createTaskForm.form.find("#pass-from-inp").attr("data-lat", Modal.pickUpPOIModal.marker.getPosition().lat());
                Form.createTaskForm.form.find("#pass-from-inp").attr("data-lng", Modal.pickUpPOIModal.marker.getPosition().lng());

                Modal.pickUpPOIModal.close();
                Form.createTaskForm.form.find("#pickUpPOIBtn").addClass("valid");
            } else {
                Modal.messageBoxModal.setTitle("คำเตือน");
                Modal.messageBoxModal.setMessage("กรุณาปักหมุดให้เรียบร้อย");
                Modal.messageBoxModal.open();
            }
        },
        checkFormValue: function () {
            var latlngString = Modal.pickUpPOIModal.modal.find(".current-lat-lng").val();
            if (latlngString) {
                var latlng = latlngString.split(",");
                return Modal.pickUpPOIModal.isValidLatLng(latlng[0].trim(), latlng[1].trim());
            }
            return false;
        },
        isValidLatLng: function (latitude, longitude) {

            if (parseInt(latitude) === NaN || parseInt(longitude) === NaN) {
                return false;
            }

            var latArr = latitude.split(".");
            var longArr = longitude.split(".");
            var latReg = /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
            var longReg = /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;

            if (latArr[0] <= 2) {
                latitude = latitude.substring(0, 10);
            } else if (latArr[0] > 2) {
                latitude = latitude.substring(0, 9);
            }

            if (longArr[0] <= 2) {
                longitude = longitude.substring(0, 10);
            } else if (longArr[0] > 2) {
                longitude = longitude.substring(0, 9);
            }

            var valid = false;

            if (latReg.test(latitude) && longReg.test(longitude)) {
                valid = true;
            }

            return valid;
        }
    }
}

var Socket = {
    initial: function () {

        Socket.validate();

        var origin = window.location.origin;

        Socket = io("/" + USER_DATA.group);

        Socket.on("connect", function (data) {

            if (Socket.hasLostConnect !== undefined && Socket.hasLostConnect) {

                delete Socket.hasLostConnect;

                $('body').find("> .connect_lost_message").remove();
            }
        });

        Socket.on("error", function (data) {
            console.log(new Date());
            console.log(data);
        });

        Socket.on('disconnect', function () {
            Socket.hasLostConnect = true;
            Http.onConnectionLost();
        });

        return Socket;
    },
    validate: function () {
        window.addEventListener("beforeunload", function (e) {
            Socket.emit('window beforeunload', USER_DATA);
        }, false);
    }
}

var HistoryTable = {
    initial: function() {

        this.getHistory();

        $('#pass-phone-inp').on("keyup", this.onPhoneNoChange);
    },

    getHistory: function () {
        function findGetParameter(parameterName) {
            var result = null,
                tmp = [];
            var items = location.search.substr(1).split("&");
            for (var index = 0; index < items.length; index++) {
                tmp = items[index].split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            }
            return result;
        }

        var phone = findGetParameter("call");
        $.post('/passenger/getHistory/byphone', { phone: phone }).done(function(response) {
            var tbdoy = "";
            if(response && response.data) {
                response.data.filter(function(history, i) {

                    var duration = moment.duration(moment().diff(moment(history.created)));
                    var diff = "";
                    if(duration.asSeconds() <= 60) {
                        diff = parseFloat(duration.asSeconds()).toFixed(0) + " วินาทีที่แล้ว"
                    } else if(duration.asSeconds() > 60 && duration.asMinutes() <= 60) {
                        diff = parseFloat(duration.asMinutes()).toFixed(0) + " นาทีที่แล้ว"
                    } else if( duration.asMinutes() > 60 && duration.asHours() <= 23) {
                        diff = parseFloat(duration.asHours()).toFixed(0) + " ชม. ที่แล้ว"
                    } else {
                        diff = parseFloat(duration.asDays()).toFixed(0) + " วันที่แล้ว"
                    }

                    if (moment.duration(moment().diff(moment(history.created))).asMinutes() < 20) {
                        tbdoy += "<tr style='background-color:rgb(228, 134, 127)'>";
                    } else {
                        tbdoy += "<tr>";
                    }

                    tbdoy += "<td>" + (i+1) + "</td>";
                    tbdoy += "<td>" + moment(history.created).format('DD/MM/YYYY HH:mm') + "</td>";
                    tbdoy += "<td>" + diff + "</td>";
                    tbdoy += "<td>" + history.cccomment + "</td>";
                    tbdoy += "<td>" + history.curaddr + "</td>";
                    tbdoy += "<td>" + history.destination + "</td>";
                    tbdoy += "</tr>";
                });
            }

            $("#historyTable").find("tbody").html(tbdoy);
        });
    },

    onPhoneNoChange: function (event) {
        var value = this.value;

        if (value.length < 8) {
            $("#historyTable").find("tbody").empty();
            return;
        }

        HistoryTable.getHistory();
    }
}

App.bootstrap();