var CONSTRAINTS = {
	service: {
		ubeam: {
			searchdrv: "../service/ubeam/searchdrv",
			assigndrvtopsg: "../service/ubeam/assigndrvtopsg",
			checkdrvstatus: "../service/ubeam/checkdrvstatus"
		},
		drivercs: {
			all: "../service/drivercs/all"
		}
	}
};

var Map;

var App =  {
	initial: function() {

		Loader.initial();
		App.createMap();
		Taxi.initial();
	},
	createMap: function () {

		Map = H.map('map');

		Map.setBasemap(3)
		Map.toggle3D();
		Map.setMaxZoom(18);
		Map.setLanguage("TH");

		Map.control.locate.disable();
	}
};

function imgError(image) {
	image.onerror = "";
	image.src = "https://placeholdit.imgix.net/~text?txtsize=14&txt=user&w=50&h=50";
	return true;
}

var Taxi = {
	initial: function () {
		Map && Map.addLayer(this.layer);
	},
	line: {},
	active: {},
	circle: L.circle(),
	layer: L.layerGroup(),
	addToMap: function (TaxiList, options, clear) {

		if (clear == undefined || clear) {
			Taxi.clear();
		}

		$(TaxiList).each(function (index, taxi) {

			if (taxi.lat == undefined || taxi.lng == undefined) {
				return;
			}

			var carplateStyle = L.divIcon({
				className: 'carplate-pin ' + taxi.status.toLocaleLowerCase(),
						// html: '<div class="message"><img src="/assets/img/map/tx_' + taxi.status.toLocaleUpperCase() + '.png">' + taxi.license_plate + '<div class="arrow"></div></div>',
						html: '<div class="message"><img src="/image/driver/' + taxi.imgface + '" onerror="imgError(this);">' + taxi.license_plate + '<div class="arrow"></div></div>',
						iconAnchor: [35, 55]
					});

			var taxiMarker = L.marker([taxi.lat, taxi.lng], {
				riseOnHover: true,
				icon: carplateStyle
			});

			taxi.imgface = Taxi.getImageFace(taxi.imgface);


			var HTMLPopup = '<div class="row">';
			HTMLPopup += '<div class="col-md-6">';
			HTMLPopup += '<img class="driver-img" src="' + taxi.imgface + '" />';
			if (options == undefined && taxi.status.toLocaleUpperCase() == "ON") {
				HTMLPopup += '<button class="btn choose" onclick="Taxi.goToPassenger();">เลือกคนนี้</button>';
			}
			HTMLPopup += '</div>';
			HTMLPopup += '<div class="driver-info col-md-6">';
			HTMLPopup += '<p class="full_name value">' + taxi.full_name + '</p>';
			HTMLPopup += '<p class="caption">เบอร์โทรติดต่อ</p>';
			HTMLPopup += '<p class="tel value">' + taxi.tel + '</p>';
			HTMLPopup += '<div class="pull-left">';
			HTMLPopup += '<p class="caption">ทะเบียน</p>';
			HTMLPopup += '<p class="license_plate value">' + taxi.license_plate + '</p>';
			HTMLPopup += '</div>';
			HTMLPopup += '<div class="pull-left">';
			HTMLPopup += '<p class="caption">สถานะ</p>';
			HTMLPopup += '<p class="status value">' + taxi.status + '</p>';
			HTMLPopup += '</div>';
			HTMLPopup += '</div>';
			HTMLPopup += '</div>';
			if (taxi.status.toLocaleUpperCase() == "BROKEN") {
				var label = '<div class="label-message-status"><h3 class="title">รถเสีย</h3><a class="call-message" href="#" onclick="Taxi.toggleBrokenDetail(this);">';
				label += '<span class="txt-show">แสดง</span><span class="txt-hide">ซ่อน</span>รายละเอียด';
				label += '</a>';
				if (Object.prototype.toString.call(taxi.brokenname) === '[object Array]') {
					label += '<p class="list"><i>อาการ</i><br>' + taxi.brokenname.toString() + '</p>';
				}
				label += '<p class="detail"><i>รายละเอียด</i><br>' + taxi.brokendetail + '</p>';
				label += '</div>';
				HTMLPopup += label;
			}

			taxiMarker.bindPopup(HTMLPopup, {
				offset: L.point(0, -18),
				className: 'driver-popup'
			}).on('popupopen', function (e) {
				Taxi.active = taxiMarker;
			}).on('popupclose', function () {
				Taxi.active = {};
			});

			taxiMarker.options.taxi_info = taxi;
			Taxi.layer.addLayer(taxiMarker);
		});
	},
	getImageFace: function (imgface) {
		if (imgface == undefined || imgface == "") {
			imgface = "assets/img/thumbnail_photo.jpg";
		} else {
			imgface = "/image/driver/" + imgface;
		}

		return imgface;
	},
	filter: function (status) {

		var taxi_list = LOCAL_DATA.taxi;
		if (status !== 'all' && LOCAL_DATA.taxi !== undefined) {
			taxi_list = LOCAL_DATA.taxi.filter(function (taxi) {
				return taxi.status.toLocaleLowerCase() == status.toLocaleLowerCase();
			});
		}

		this.addToMap(taxi_list);
	},
	onFilterClick: function (filter, event) {

		var isActive = $(filter).hasClass('active');
		$('.filter-marker.active').removeClass('active');

		!isActive && $(filter).addClass('active');

		var status = $(filter).attr('data-status');
		var count = $('.filter-marker.active').size();

		if (count == 0) { status = "all"; }
		this.filter(status);
	},
	findInCenterMap: function () {

		var center = Map.getCenter();

		Http.post(CONSTRAINTS.service.drivercs.all, {
			loader: false,
			data: {
				"curlat": center.lat,
				"curlng": center.lng
			},
			onSuccess: function (response) {
						// if (response.status) {

							var taxi_list = [];

							$(response).each(function (index, driver) {
								taxi_list.push({
									"_id": driver._id,
									"license_plate": driver.carplate,
									"lat": driver.curlat,
									"lng": driver.curlng,
									"status": "ON",
									"imgface": driver.imgface,
									"full_name": driver.fname + ' ' + driver.lname,
									"tel": driver.phone,
									"rating": "5"
								});
							});

							Taxi.addToMap(taxi_list);
						// } else {
						// 	Taxi.clear();
						// }
					}
				});
	},
	findByCarplate: function(plateNumber) {
		var currentTaxi = null;
		Taxi.layer.eachLayer(function(taxi) { 
			if(taxi.options.taxi_info.license_plate == plateNumber) {
				currentTaxi = taxi;
				return false;
			}
		});

		if(currentTaxi !== null) {
			Notification.remove();
			Monitoring.toggleFilterControl($(".taxi-counter .puller"));
			setTimeout(function () {
				Map.invalidateSize();
				currentTaxi.openPopup();
				Map.setView([currentTaxi.options.taxi_info.lat, currentTaxi.options.taxi_info.lng], 17);
			}, 100);
		} else {
			Notification.remove();
			Notification.show({
				className:"warning",
				title:'ไม่พบแท็กซี่ดังกล่าว',
			});
		}
	},
	goToPassenger: function () {

		var driver = Taxi.active.options.taxi_info;
		var passenger_id = Modal.assignTaskModal.modal.find("#pass_id").val();
		var data = {
			"action": "adminmonitor.goToPassenger",
			"psg_id": passenger_id,
			"drv_id": driver._id,
			"username": USER_DATA.username
		};

		Http.post(CONSTRAINTS.service.ubeam.assigndrvtopsg, {
			data: data,
			onSuccess: function (response) {
				if (response.status) {

					Preloader.show();
					setTimeout(function () {

						var html = Taxi.active.getPopup().getContent();
						html = html.replace("choose", "choose hide");
						Taxi.active.setPopupContent(html);
						$(html).find(".btn.choose").hide();

						Taxi.layer.eachLayer(function (marker) {
							if (marker._leaflet_id != Taxi.active._leaflet_id && !(marker instanceof L.Polyline)) {
								Taxi.layer.removeLayer(marker);
							}
						});

						Modal.assignTaskModal.modal.focus();
						Map.off("dragend", Taxi.findInCenterMap);
						Form.assignTaskForm.form.find("input[type=hidden]").val("");
						Form.assignTaskForm.form.find("button[type=submit]").hide();
						Form.searchTaxiBeforeAssignForm.form.find("#car-plate-inp").prop("disabled", true);
						Form.searchTaxiBeforeAssignForm.form.find("#car-plate-inp").val(response.drv_data.carplate);

						Preloader.hide();
						Notification.remove();
						Notification.defaultSuccess();
						Passenger.onAssignTaskSuccess();
					}, 700);
				} else {
					Notification.defaultError();
				}
			}
		});
	},
	assignByLineApp: function (passenger_id, drv_carplate) {

		Preloader.show();

		var data = {
			"action": "adminmonitor.assignByLineApp",
			"psg_id": passenger_id,
			"drv_carplate": drv_carplate,
			"lineconfirm": "Y",
			"username": USER_DATA.username
		};

		Http.post(CONSTRAINTS.service.ubeam.assigndrvtopsg, {
			data: data,
			onSuccess: function (response) {
				if (response.status) {
					Preloader.hide();
					Notification.remove();
					Notification.defaultSuccess();
					Passenger.onAssignTaskSuccess();
					Modal.assignTaskByLineModal.modal.find(".modal-footer").hide();
				} else {
					Notification.defaultError();
				}
			}
		});
	},
	checkStatus: function (data, callback) {
		Http.post(CONSTRAINTS.service.ubeam.checkdrvstatus, {
			loader: false,
			data: data,
			onSuccess: callback
		});
	},
	cancelFind: function () {
		Map.removeLayer(this.circle);
	},
	clear: function () {
		this.layer.clearLayers();
	},
	toggleStatus: function (status, show) {
		if (show) {
			var taxi = Monitoring.taxi.filter(function (taxi) {
				return taxi.status == status;
			});
			taxi.length > 0 && Taxi.addToMap(taxi, { monitoring: true }, false);
		}
		else {
			Taxi.layer.eachLayer(function (taxi) {
				if (taxi.options.taxi_info.status == status) {
					Taxi.layer.removeLayer(taxi);
				}
			});
		}
	},
	ghostTaxi: function (number) {

		var taxi_list = [];
		var bounds = this.circle.getBounds();

		if (number === undefined || number == 0) {
			number = 1;
		}

		for (var i = 1; i <= number; i++) {
			var latlng = Taxi.getRandomLatLng(bounds);

			taxi_list.push({
				"_id": new Date().getTime(),
				"license_plate": "1กล 2254",
				"lat": latlng.lat,
				"lng": latlng.lng,
				"status": "ON",
				"image_driver": "",
				"full_name": "กนกกร",
				"tel": "082-456-8899",
				"rating": "5"
			});
		};

		return taxi_list;
	},
	getRandomLatLng: function (bounds) {
		southWest = bounds.getSouthWest(),
		northEast = bounds.getNorthEast(),
		lngSpan = northEast.lng - southWest.lng,
		latSpan = northEast.lat - southWest.lat;

		return new L.LatLng(
			southWest.lat + latSpan * Math.random(),
			southWest.lng + lngSpan * Math.random());
	},
	toggleBrokenDetail: function (elem) {
		$(elem).closest('.label-message-status').toggleClass('open');
	}
};


var Http = {
	connection_error_time: 0,
	getJson: function (url, options) {

		if (options.loader !== undefined && !options.loader) { Preloader.show(); }

		$.getJSON(url, function (data) {
			if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
				options.onSuccess(data);
			}
		})
		.fail(function () {
			Notification.defaultError();
		})
		.always(function () {
			Preloader.hide();
		});
	},
	post: function (url, options) {

		if (options.loader || options.loader == undefined) { Preloader.show(); }

		if (options.data == undefined) { options.data = {}; }
		$.post(url, options.data, function (data) {
			if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
				options.onSuccess(data);
			}
		})
		.fail(function () {
			if (Http.connection_error_time < 1) {
				Http.connection_error_time++;
			}
			else if (Http.connection_error_time >= 1) {

				Http.connection_error_time = 0;
				Http.onConnectionLost();
			}
		})
		.always(function () {
			Preloader.hide();
		});
	},
	onConnectionLost: function () {

		var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

		$('body').find("> .connect_lost_message").remove();
		$('body').append(message);
		$('body').find("> .connect_lost_message").fadeIn('fast');

		if (this.intervalId == undefined) {
			this.intervalId = App.intervalManager(true, Http.testConnection, 5000);
		}
	},
	testConnection: function () {
		$.post('service/ubeam/getPassengerDetail', { "psg_id": "" }, function (data) {
			clearInterval(Http.intervalId);
			App.intervalManager(false, Http.intervalId);
			delete Http.intervalId;
			Http.connection_error_time = 0;
			$('body').find("> .connect_lost_message").remove();
		});
	}
};


var ApplicationActivities = function() {

	// http://stackoverflow.com/a/10935062
	this.intervalManager: function (flag, action, time) {
        if (flag) {
            return setInterval(action, time);
        }
        else {
            typeof action !== 'undefined' && clearInterval(action);
        }
    }

    this.HttpService = $.extend({}, new HttpService());
    this.Preloader = Preloader;
}

new HttpConnectionHandler({
	url: 'service/ubeam/getPassengerDetail'
});

var HttpConnectionHandler = function(data) {

	this.data = $.extend({
		limit_error: 1,
		timeout_test_connection: 5000,
		url: null,
		data: {}
	}, data);

	this.connection_error_time = 0;

	var initial = function() {
		if(('HttpConnectionHandler' in window)) {
			console.log('HttpConnectionHandler cannot be re-create or has more than one in window object');

			return;
		}

		this.testConnection = testConnection;
		this.onConnectionLost = onConnectionLost;
	}

	var onConnectionLost = function() {

		var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

		$('body').find("> .connect_lost_message").remove();
		$('body').append(message);
		$('body').find("> .connect_lost_message").fadeIn('fast');

		if (this.intervalId == undefined) {
			this.intervalId = App.intervalManager(true, this.testConnection, 5000);
		}
	}

	var testConnection = function () {
		$.post(this.URLTestConnection, { }, function (data) {
			clearInterval(Http.intervalId);
			App.intervalManager(false, Http.intervalId);
			delete Http.intervalId;
			Http.connection_error_time = 0;
			$('body').find("> .connect_lost_message").remove();
		});
	}

	initial();
}

var HttpService = function(data) {

	var post = function(url, data, callback) {

		if(typeof url !== "string") { return; }
		if(typeof data !== "object") { return; }

		$.post(url, data, function (data) {
			if(callback !== undefined && typeof callback === "function") {
				callback(data);
			}
		})
		.fail(function () {
			if (this.connection_error_time < 1) {
				this.connection_error_time++;
			}
			else if (this.connection_error_time >= 1) {

				this.connection_error_time = 0;
				this.onConnectionLost();
			}
		})
		.always(function () {
			Preloader.hide();
		});
	}

	this.getAllTaxi = function(query, callback) {
		post('ubeam/getAllTaxi', query, callback); 
	}
}


var Loader = {
	initial: function () {
		Preloader.setOption({
			timeout: 700,
			className: 'la-ball-clip-rotate la-dark',
			color: '#B21616'
		});
	}
};

$(function() {
	App.initial();
});