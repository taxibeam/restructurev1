var TaxiDriver = function(data) {

	if ( this === window ) throw "InitializeError :: Cannot direct use this without new class.";

	var taxi = this.data = $.extend({}, data);

	this.addToMap = function(taxiLayer) {

		if( !taxiLayer instanceof TaxiLayer ) { throw "TypeError :: data is not defineValues"; }

		var marker = this.createMarker();
		taxiLayer.addTaxi(marker);
	}

	this.createMarker = function() {

		if (taxi.curlat == undefined || taxi.curlng == undefined) {
			return;
		}

		var carplateStyle = L.divIcon({
			className: 'carplate-pin ' + taxi.status.toLocaleLowerCase(),
			html: '<div class="message"><img src="/assets/img/map/tx_' + taxi.status.toLocaleUpperCase() + '.png"><div class="arrow"></div></div>',
			iconSize: [41, 51], // size of the icon
		    iconAnchor: [20, 51], // point of the icon which will correspond to marker's location
		    popupAnchor: [0, -51] // point from which the popup should open relative to the iconAnchor
		});

		var taxiMarker = L.marker([taxi.curlat, taxi.curlng], {
			riseOnHover: true,
			icon: carplateStyle
		});

		this.setImageFace();

		taxiMarker.bindPopup(this.createPopup(), {
			offset: L.point(1, -6),
			className: 'driver-popup'
		}).on('popupopen', function (e) {
			getApplicationContext().taxiLayer.active = taxiMarker;
		}).on('popupclose', function () {
			getApplicationContext().taxiLayer.active = {};
		});

		return taxiMarker;
	}

	this.setImageFace = function () {
		if (taxi.imgface == undefined || taxi.imgface == "") {
			taxi.imgface = "assets/img/thumbnail_photo.jpg";
		} else {
			taxi.imgface = "/image/driver/" + taxi.imgface;
		}
	}

	this.createPopup = function() {

		var HTMLPopup = '<div class="row">';
		HTMLPopup += '<div class="col-md-6">';
		HTMLPopup += '<img class="driver-img" src="' + taxi.imgface + '" />';
		HTMLPopup += '</div>';
		HTMLPopup += '<div class="driver-info col-md-6">';
		HTMLPopup += '<p class="full_name value">' + taxi.fname + ' ' + taxi.lname + '</p>';
		HTMLPopup += '<p class="caption">เบอร์โทรติดต่อ</p>';
		HTMLPopup += '<p class="tel value">' + taxi.phone + '</p>';
		HTMLPopup += '<div class="pull-left">';
		HTMLPopup += '<p class="caption">ทะเบียน</p>';
		HTMLPopup += '<p class="license_plate value">' + taxi.carplate + '</p>';
		HTMLPopup += '</div>';
		HTMLPopup += '<div class="pull-left">';
		HTMLPopup += '<p class="caption">สถานะ</p>';
		HTMLPopup += '<p class="status value">' + taxi.status + '</p>';
		HTMLPopup += '</div>';
		HTMLPopup += '</div>';
		HTMLPopup += '</div>';

		if (taxi.status.toLocaleUpperCase() == "BROKEN") {
			var label = '<div class="label-message-status"><h3 class="title">รถเสีย</h3><a class="call-message" href="#" onclick="Taxi.toggleBrokenDetail(this);">';
			label += '<span class="txt-show">แสดง</span><span class="txt-hide">ซ่อน</span>รายละเอียด';
			label += '</a>';
			if (Object.prototype.toString.call(taxi.brokenname) === '[object Array]') {
				label += '<p class="list"><i>อาการ</i><br>' + taxi.brokenname.toString() + '</p>';
			}
			label += '<p class="detail"><i>รายละเอียด</i><br>' + taxi.brokendetail + '</p>';
			label += '</div>';
			HTMLPopup += label;
		}

		return HTMLPopup;
	}
};