var Map = null;
var LOCAL_STATE = [];
var LOCAL_STATE_CONTROL = {
    remove: function (data) {
        LOCAL_STATE = LOCAL_STATE.filter(function (task) {
            return task.id !== data.id;
        });
    }
};


var TEMP_STATE_DATA = [];

var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    serviceURI: {
        getAllAnnouncement: serviceBaseURI + '/service/ubeam/announcement/all',
        addAnnouncement: serviceBaseURI + '/service/ubeam/announcement/add',
        editAnnouncement: serviceBaseURI + '/service/ubeam/announcement/edit',
        deleteAnnouncement: serviceBaseURI + '/service/ubeam/announcement/delete',
        getInitiateList: serviceBaseURI + '/service/ubeam/getinitiatelist',
        getAdvanceList: serviceBaseURI + '/service/ubeam/getadvancelist',
        getDependingList: serviceBaseURI + '/service/ubeam/getdpendinglist',
        getAssignList: serviceBaseURI + '/service/ubeam/getassignlist',
        getReAssignJobDetail: serviceBaseURI + '/service/ubeam/getreassignjobdetail',
        countJobPerDrv: serviceBaseURI + '/service/ubeam/CountJobPerDrv',
        searchFinishList: serviceBaseURI + '/service/ubeam/searchFinishList',
        addJobList: serviceBaseURI + '/service/ubeam/addjoblist',
        editJobList: serviceBaseURI + '/service/ubeam/editjoblist',
        getPOIRecommended: serviceBaseURI + '/service/ubeam/getpoirecommended',
        searchNameCar: serviceBaseURI + '/service/ubeam/searchnamecar',
        searchDriver: serviceBaseURI + '/service/ubeam/searchdrv',
        getPassengerDetail: serviceBaseURI + '/service/ubeam/getPassengerDetail',
        getPOIandParking: serviceBaseURI + '/service/ubeam/getPOIandParking',
        cancelPSGDRV: serviceBaseURI + '/service/ubeam/cancelpsgdrv/',
        deleteJob: serviceBaseURI + '/service/ubeam/deletejob',
        countTaxi: serviceBaseURI + '/service/ubeam/countTaxi',
        assignDRVtoPSG: serviceBaseURI + '/service/ubeam/assigndrvtopsg',
        mostHitStartPlace: serviceBaseURI + '/service/ubeam/MostHitStartPlace',
        checkDRVStatus: serviceBaseURI + '/service/ubeam/checkdrvstatus',
        mostHitDestinationPlace: serviceBaseURI + '/service/ubeam/MostHitDestinationPlace',
        mostHitHours: serviceBaseURI + '/service/ubeam/MostHitHours',
        addNewPOI: serviceBaseURI + '/passenger/poi/add/',
        getPOIsByPhone: serviceBaseURI + '/passenger/poi/list/byphone/',
        searchPOIsByPhone: serviceBaseURI + '/passenger/poi/search/byphone/',
        getPOIsByGarage: serviceBaseURI + '/passenger/poi/list/all',
        FindDrvinRadian: serviceBaseURI + '/service/ubeam/FindDrvinRadian',
        googleMapIconMarkerBlueDot: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png',
        addPOI: 'garage/poi/add'
    },
    driverImageURL: 'http://callcenter.taxi-beam.com',
    daterangepicker: {
        options: {
            opens: "left",
            timePicker: false,
            autoApply: false,
            locale: {
                "format": "DD/MM/YYYY HH:mm",
                "separator": " - ",
                "applyLabel": "ตกลง",
                "cancelLabel": "ยกเลิก",
                "fromLabel": "จาก",
                "toLabel": "ถึง",
                "customRangeLabel": "กำหนดเอง",
                "daysOfWeek": [
                "อ.",
                "จ.",
                "อ.",
                "พ.",
                "พฤ.",
                "ศ.",
                "ส."
                ],
                "monthNames": [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
                ],
                "firstDay": 1
            },
            alwaysShowCalendars: false,
            ranges: {
                'วันนี้': [moment(), moment()],
                'เมื่อวาน': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 วันที่แล้ว': [moment().subtract(6, 'days'), moment()],
                '30 วันที่แล้ว': [moment().subtract(29, 'days'), moment()],
                'เดือนนี้': [moment().startOf('month'), moment().endOf('month')],
                'เดือนที่แล้ว': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }
    }
};


var App = {
    init: function () {

        App.show();
        Preloader.show();
        
        Sound.initial();
        Socket.initial();
        App.createMap();
        Taxi.initial();
        Loader.initial();
        HotKey.initial();
        Report.initial();
        LandMark.initial();
        Bootstrap.initial();
        JQuery.NumberOnlyInit();
        DateTimePicker.initial();
        iOSwitchery.initial();

        Monitoring.initial();

        Form.announceForm.initial();
        Form.createTaskForm.initial();
        Form.assignTaskForm.initial();
        Form.announceEditForm.initial();
        Form.broadcastTaskForm.initial();
        Form.searchFinishedTaskForm.initial();
        Form.assignTaskSearchTaxiForm.initial();
        Form.assignedTaskEditableForm.initial();
        Form.searchTaxiBeforeAssignForm.initial();

        Modal.initial();
        Modal.reportModal.initial();
        Modal.confirmModal.initial();
        Modal.successModal.initial();
        Modal.pickUpPOIModal.initial();
        Modal.areaSuggestion.initial();
        Modal.noCurrentPlace.initial();
        Modal.reCurrentPlace.initial();
        Modal.createTaskModal.initial();
        Modal.assignTaskModal.initial();
        Modal.assignedTaskModal.initial();
        Modal.announcementModal.initial();
        Modal.createNewPOIModal.initial();
        Modal.assignTaskByLineModal.initial();
        Modal.announcementEditModal.initial();
        Modal.searchTaskWithResultListModal.initial();
        Modal.confirmNoneRegisterDriverModal.initial();
        
        DataTable.searchTaskResultTable.initial();

        NotificationRequestApprove.init();
        POIRecommended.loadToAutocompleteLookup();
        POIRecommended.setUpLookup();
    },
    show: function () {
        setTimeout(function () {
            $(".intial-app-loading").find('.loader, .text').fadeOut('fast');
            setTimeout(function () {
                $(".intial-app-loading").fadeOut('normal');
                setTimeout(function () {

                    $("body > .container-fluid.app").fadeIn(700);
                    $("body > .navbar").fadeIn(700);

                    setTimeout(function () {
                        DataTable.queueTaskTable.initial(true);
                        DataTable.taxiReportTable.initial();
                        DataTable.advanceTaskTable.initial(true);
                        DataTable.pendingTaskTable.initial(true);
                        DataTable.assignedTaskTable.initial(true);

                        window.screen.availWidth <= 414 && App.layout.setMobileLayout();

                        Socket.emit('update state', { user: USER_DATA });
                    }, 700);

                    setTimeout(function () {
                        $("body").removeClass("loading");
                        $(".intial-app-loading").remove();
                    }, 700);

                }, 700);
            }, 700)
        }, 1000);
    },
    createMap: function () {

        Map = L.map('map');
        var roads = L.gridLayer.googleMutant({
            type: 'roadmap'
        }).addTo(Map);
        App.setDefaultPosition();
    },
    setDefaultPosition: function () {
        Map.setView(USER_CURRENT_LOCATION, 12, { animate: false });
    },
    setCurrentPosition: function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                Map.setView([position.coords.latitude, position.coords.longitude], 13, { animate: false });
            });
        }
    },
    toggleSidebar: function () {
        $('#st-container').toggleClass('st-menu-open');
        $('.hb-locator_search').toggleClass('relative-sidebar');
    },
    hideSidebar: function () {
        $('#st-container').removeClass('st-menu-open');
        $('.hb-locator_search').removeClass('relative-sidebar');
    },
    intervalManager: function (flag, action, time) {
        // http://stackoverflow.com/a/10935062
        if (flag) {
            return setInterval(action, time);
        }
        else {
            typeof action !== 'undefined' && clearInterval(action);
        }
    },
    layout: {
        setToRowAlign: function () {
            var layout = $(".container-fluid.app").find(">.row > .col-lg-6");
            layout.removeClass("col-lg-6");
            layout.addClass("col-lg-12");
            setTimeout(function () {
                DataTable.queueTaskTable.datatable.draw();
                DataTable.advanceTaskTable.datatable.draw();
                DataTable.pendingTaskTable.datatable.draw();
                DataTable.assignedTaskTable.datatable.draw();
            }, 500);
        },
        setToColumnAlign: function () {
            var layout = $(".container-fluid.app").find(">.row > .col-lg-12");
            layout.removeClass("col-lg-12");
            layout.addClass("col-lg-6");
            setTimeout(function () {
                DataTable.queueTaskTable.datatable.draw();
                DataTable.advanceTaskTable.datatable.draw();
                DataTable.pendingTaskTable.datatable.draw();
                DataTable.assignedTaskTable.datatable.draw();
            }, 500);
        },
        setMobileLayout: function () {

            $("#table-pending-tab").detach().appendTo("#leftTabContent");
            $("#table-assigned-tab").detach().appendTo("#leftTabContent");

            $("#table-pending-tab").removeClass("active");

            $("#rightTabList, #rightTabContent").css({ display: "none" });

            $("body .app .tab-bottom").show();

            DataTable.queueTaskTable.datatable.draw();
            DataTable.pendingTaskTable.datatable.draw();
            DataTable.assignedTaskTable.datatable.draw();
            DataTable.advanceTaskTable.datatable.draw();

            $(".tab-bottom").find(".nav.nav-tabs").on('shown.bs.tab', function (e) {
                switch (e.target.hash) {
                    case "#table-queue-tab":
                    DataTable.queueTaskTable.datatable.draw();
                    break;
                    case "#table-advance-tab":
                    DataTable.advanceTaskTable.datatable.draw();
                    break;
                    case "#table-pending-tab":
                    DataTable.pendingTaskTable.datatable.draw();
                    break;
                    case "#table-assigned-tab":
                    DataTable.assignedTaskTable.datatable.draw();
                    break;
                }
            });
        }
    }
};


var Announcement = {
    getAll: function () {
        Http.post(CONSTRAINTS.serviceURI.getAllAnnouncement, {
            onSuccess: function (response) {
                if (response.status) {
                    Modal.announcementModal.modal.find(".modal-body").empty();
                    $(response.data).each(function (index, announce) {
                        var HTMLMedia = '<div class="media" data-ref="' + announce._id + '">';
                        HTMLMedia += '<div class="media-left">';
                        HTMLMedia += '<a href="#">'
                        HTMLMedia += '<img class="media-object img-circle" src="assets/img/callcenter-profile-sample-face.png" style="width: 64px; height: 64px;">';
                        HTMLMedia += '</a>';
                        HTMLMedia += '</div>';
                        HTMLMedia += '<div class="media-body">';
                        HTMLMedia += '<h4 class="media-heading">' + announce.topic + '</h4>';
                        HTMLMedia += announce.detail;
                        if (announce.createdby === USER_DATA.username) {
                            HTMLMedia += '<a href="#" class="btn btn-link" onclick="Announcement.showEditModal(this);"><span class="glyphicon glyphicon-pencil edit" aria-hidden="true"></span> แก้ไบ</a>';
                            HTMLMedia += '<a href="#" class="btn btn-link" onclick="Announcement.showConfirmRemoveModal(\'' + announce._id + '\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ลบทิ้ง</a>';
                        }
                        HTMLMedia += '</div>';
                        HTMLMedia += '</div>';

                        Modal.announcementModal.modal.find(".modal-body").append(HTMLMedia);
                        Modal.announcementModal.modal.find(".modal-body [data-ref='" + announce._id + "']").data(announce);
                    });
                }
                else {
                    Modal.announcementModal.modal.find(".modal-body").empty();
                }
            }
        });
    },
    remove: function (id) {
        Preloader.show();
        Http.post(CONSTRAINTS.serviceURI.deleteAnnouncement, {
            data: {
                "user_id": USER_DATA._id,
                "ann_id": id
            },
            onSuccess: function (response) {
                if (response.status) {
                    Notification.defaultSuccess();
                    Modal.announcementModal.modal.find(".modal-body [data-ref='" + id + "']").remove();
                    Modal.confirmModal.modal.modal("hide");
                }
                else {
                    Notification.defaultError();
                }
            }
        });
    },
    showEditModal: function (elem) {
        var announce = $(elem).closest('.media').data();
        Modal.announcementEditModal.modal.find('[name="ann_id"]').val(announce._id);
        Modal.announcementEditModal.modal.find('[name="topic"]').val(announce.topic);
        Modal.announcementEditModal.modal.find('[name="detail"]').val(announce.detail);

        Modal.announcementEditModal.open();
    },
    showConfirmRemoveModal: function (id) {

        var onShow = function (event) {
            Modal.confirmModal.modal.find('[data-dismiss="modal"]').focus();
            Modal.confirmModal.modal.find('.confirm').on("click", function () { window["Announcement"]["remove"](id) });
        }

        var onClose = function (event) {
            Modal.confirmModal.modal.find('.confirm').unbind();

            Modal.confirmModal.modal.unbind('shown.bs.modal', onShow);
            Modal.confirmModal.modal.unbind('hidden.bs.modal', onClose);
        }

        Modal.confirmModal.modal.on('shown.bs.modal', { modal: this }, onShow);

        Modal.confirmModal.modal.on('hidden.bs.modal', { modal: this }, onClose);

        Modal.confirmModal.modal.find(".modal-body > .message").html("ต้องลบประกาศนี้ใช่มั้ย?");
        Modal.confirmModal.open();
    }
};


var Bootstrap = {
    initial: function () {
        $(".modal").modal({
            show: false,
            keyboard: true
        });

        $('[data-toggle="tooltip"]').tooltip({ animation: false });

        $('.user-profile-tab .dropdown-menu li:first').on("click", function (e) {
            e.stopPropagation();
        });

        $('#leftTabList, #rightTabList').find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if (e.target.hash == "#table-queue-tab") {
                DataTable.queueTaskTable.datatable.draw(false);
            }
            else if (e.target.hash == "#table-advance-tab") {
                DataTable.advanceTaskTable.datatable.order(2, "desc").draw(false);
            }
            else if (e.target.hash == "#table-pending-tab") {
                DataTable.pendingTaskTable.datatable.order(2, "desc").draw(false);
            }
            else if (e.target.hash == "#table-assigned-tab") {
                DataTable.assignedTaskTable.datatable.order(2, "desc").draw(false);
            }
        });
    }
};


var Chat = {
    $this: $("#chat-board"),
    $caller: $("#navbar .chat-tab"),
    open: function () {
        Chat.$this.addClass('open');
    },
    toggle: function () {
        Chat.$this.toggleClass('open');
    },
    close: function () {
        Chat.$this.removeClass('open');
    },
    popOut: function () {
        this.close();
        Chat.$caller.hide();
        setTimeout(function () {
            window.open(window.location.origin + "/chat_box", "_blank", "toolbar=yes, scrollbars=no, resizable=yes, top=100, left=500, width=400, height=760");
        }, 300);
    }
};


var ChatControl = {
    timeoutId: null,
    record: $("#liveChatModal").find(".controller .record"),
    cancel: $("#liveChatModal").find(".controller .cancel"),
    initial: function () {
        ChatControl.record.on("click", function () {

            // On Stop
            if (ChatControl.record.hasClass('active')) {

                clearTimeout(ChatControl.timeoutId);

                ChatControl.record.removeClass("active");
                ChatControl.cancel.hide();

                ChatControl.record.disabled = true;
                mediaRecorder.state == "recording" && mediaRecorder.stop();
                Notification.defaultSuccess();

                setTimeout(function () {
                    Socket.emit('radio', LiveCall.voice);
                    ChatControl.record.disabled = false;
                }, 1000);

            }
                // On Start
                else {
                    ChatControl.record.addClass("active");
                    ChatControl.cancel.show();

                    mediaRecorder.state == "inactive" && mediaRecorder.start();

                    ChatControl.timeoutId = setTimeout(function () {
                        ChatControl.record.disabled = false;
                        ChatControl.record.removeClass("active");
                        ChatControl.cancel.hide();

                        mediaRecorder.state == "recording" && mediaRecorder.stop();
                    }, 15000);
                }
            });

        ChatControl.cancel.on("click", function () {

            clearTimeout(ChatControl.timeoutId);

            mediaRecorder.state == "recording" && mediaRecorder.stop();
            ChatControl.record.removeClass("active");
            ChatControl.cancel.hide();

        });
    }
};


var LiveBeam = {
    drivers: [],
    listView: $("#driverListChatModal").find(".driver-list"),
    initial: function () {
        LiveBeam.update();
    },
    update: function () {
        Http.post("socket/getDriverOnline", {
            loader: false,
            onSuccess: function (response) {
                if (response.status) {
                    LiveBeam.listView.empty();
                    response.data.filter(function (driver) {
                        LiveBeam.toListView(driver);
                    });
                }
            }
        });
    },
    toListView: function (driver) {

        Modal.driverListChatModal.modal.find("[data-ref='" + driver.socketId + "']").remove();

        var html = '<button data-ref="' + driver.socketId + '" type="button" class="list-group-item" ';
        html += 'onclick="Modal.liveChatModal.open(this);">' + driver._driver.fname + ' ' + driver._driver.lname + '</button>';
        LiveBeam.listView.append(html);
        Modal.driverListChatModal.modal.find("[data-ref='" + driver.socketId + "']").data(driver);
    }
};


var DateTimePicker = {
    minHour: USER_DATA.group === 'cricritaxi' ? 0.5 : 3,
    picker: $('#createdjob'),
    initial: function () {

        var minDate = new Date().getTime() + (DateTimePicker.minHour * 60 * 60 * 1000);

        DateTimePicker.picker.datetimepicker({
            inline: true,
            format: "MM/dd/YYYY HH:mm",
            minDate: new Date(minDate)
        });
    },
    setMinDate: function () {
        var minDate = new Date().getTime() + (DateTimePicker.minHour * 60 * 60 * 1000);
        DateTimePicker.picker.data("DateTimePicker").minDate(new Date(minDate));
    }
};


var DataTable = {
    queueTaskTable: {
        $table: $("#queueTaskTable"),
        datatable: null,
        initial: function (loader) {

            DataTable.queueTaskTable.datatable = DataTable.queueTaskTable.$table.DataTable({
                scrollY: (window.screen.availHeight - (window.screen.availHeight > 414 ? 300 : 132)),
                scrollCollapse: true,
                pageLength: 10,
                paging: true,
                autoWidth: true,
                order: [[1, 'asc'], [2, 'asc']],
                deferRender: true,
                searching: true,
                oLanguage: {
                    "sEmptyTable": "ไม่มีข้อมูลให้แสดงผล",
                    "sLengthMenu": " _MENU_ ",
                    "sInfo": "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ"
                },
                columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row) {
                        return (data == undefined || data == "" ? "-" : data);
                    }
                },
                {
                    targets: 1,
                    visible: window.screen.availWidth > 414,
                    className: "jobtype",
                    render: function (data, type, row) {
                        var tooltipNormal = '<span class="circle-text default" data-toggle="tooltip" title="คิวงานปกติ">N</span>';
                        var tooltipAdvance = '<span class="task-type circle-text orange" data-toggle="tooltip" title="งานจองล่วงหน้า">A</span>';
                        return (data != undefined && data == 'QUEUE' ? tooltipNormal : tooltipAdvance);
                    }
                },
                {
                    targets: 2,
                    render: function (data, type, row) {
                        return (data == undefined ? '' : moment(data).format('HH:mm'));
                    }
                },
                {
                    targets: [3, 4, 5],
                    className: "inline-edit",
                    createdCell: function (td, cellData, rowData, row, col) {

                        if (col == 3) {
                            $(td).attr("data-field-name", "curaddr");
                        }
                        else if (col == 4) {
                            $(td).attr("data-field-name", "destination");
                        }
                        else if (col == 5) {
                            $(td).attr("data-field-name", "phone");
                        }

                        $(td).attr({
                            'title': 'ดับเบิ้ลคลิ๊กเพื่อแก้ไข',
                            'data-toggle': 'tooltip'
                        }).on('dblclick', DataTable.inlineEditable);
                    }
                },
                {
                    targets: 5,
                    width: "70px",
                    visible: window.screen.availWidth > 414,
                    render: function (data, type, row) {
                        return (data == undefined ? '' : Util.toReadablePhoneNumber(data.toString()));
                    }
                },
                {
                    targets: 6,
                    width: "60px",
                    visible: window.screen.availWidth > 414,
                    render: function (data, type, row) {

                        var cell = '<span class="status-time ' + Util.getTimeStatus(data) + '">';
                        cell += '<i class="fa fa-bookmark"></i>';
                        var diff = (row[1] != undefined && row[1] == 'QUEUE' ? Util.getDiffTime(row[6]) : { time: 'งานจอง', unit: 'ล่วงหน้า' });
                        cell += '<span class="detail">' + diff.time + '<br><span>' + diff.unit + '</span>';
                        cell += '</span>';

                        return cell;
                    }
                },
                {
                    targets: 7,
                    width: "70px",
                    visible: window.screen.availWidth > 414,
                    render: function (data, type, row) {
                        var cell = '<div class="btn-group" role="group" aria-label="...">';
                        cell += '<button type="button" class="btn btn-default showFindTaxi" data-toggle="tooltip" title="จ่ายงาน" onclick="DataTable.queueTaskTable.callTaxiModal(this);"><i class="fa fa-taxi"></i></button>';
                        cell += '<button type="button" class="btn btn-default removeCurrentPassenger" data-toggle="tooltip" title="ลบออก" onclick="Passenger.showConfirmRemoveModal(\'' + data + '\');"><i class="fa fa-trash-o"></i></button>';
                        cell += '</div>';
                        return cell;
                    }
                }
                ],
                createdRow: function (row, data, index) {

                    $(row).attr("data-ref", data[7]);

                    $(row).find('[data-toggle="tooltip"]')
                    .tooltip({ placement: 'top', container: 'body', animation: false })
                    .on('show.bs.tooltip', function () { $('body').find('.tooltip.in').remove(); });
                },
                rowCallback: function (row, data, index) {
                    $(row).find(".showFindTaxi").data(data);
                },
                drawCallback: function (settings) {
                    this.api().rows({ page: 'current' }).data().filter(function (data) {
                        if (LOCAL_STATE.length <= 0) {
                            DataTable.unlockLine({
                                id: data[7],
                                table: "queueTaskTable"
                            });
                        }
                        else {
                            LOCAL_STATE.filter(function (task) {

                                if (DataTable.queueTaskTable.$table.find('[data-ref=' + task.id + ']').find('.inline-edit.active').size() >= 1) { return; }

                                task.id == data[7] ? DataTable.lockLine(task) : DataTable.unlockLine({ id: data[7], table: "queueTaskTable" });
                            });
                        }
                    });
                }
            });

            DataTable.queueTaskTable.update(loader);
        },
        render: function (state) {

            if (DataTable.queueTaskTable.$table.find('.inline-edit.active').size() >= 1) { return; }

            DataTable.queueTaskTable.datatable.draw(state);
        },
        update: function (loader) {
            Http.post(CONSTRAINTS.serviceURI.getInitiateList, {
                loader: loader,
                onSuccess: function (response) {
                    if (response.status) {
                        DataTable.queueTaskTable.datatable.clear();
                        DataTable.queueTaskTable.create(response.data);
                        DataTable.queueTaskTable.customTableHeader();
                    } else {
                        DataTable.queueTaskTable.datatable.clear().draw(false);
                        DataTable.queueTaskTable.setIndicator();
                        DataTable.queueTaskTable.customTableHeader();
                    }
                }
            });
        },
        create: function (data) {

            if (data == undefined) { return false; }

            $(data).each(function (index, task) {
                DataTable.queueTaskTable.datatable.row.add([
                    task.ccstation,
                    task.jobtype,
                    task.createdjob,
                    task.curaddr,
                    task.destination,
                    task.phone,
                    task.createdjob,
                    task._id
                ]).order([1, 'asc'], [2, 'asc']).draw(false);
            });

            DataTable.queueTaskTable.setIndicator();
        },
        addRow: function (data) {

            if (data == undefined) { return false; }

            DataTable.queueTaskTable.datatable.row.add([
                data.ccstation,
                data.jobtype,
                data.createdjob,
                data.curaddr,
                data.destination,
                data.phone,
                data.createdjob,
                data._id
            ]);

            DataTable.queueTaskTable.render(false);

            DataTable.queueTaskTable.setIndicator();
        },
        updateRow: function (data) {

            var row = DataTable.queueTaskTable.$table.find('[data-ref="' + data.psg_id + '"]')[0];

            var rowIndex = DataTable.queueTaskTable.datatable.row(row).index();
            DataTable.queueTaskTable.datatable.row(rowIndex).data([
                data.ccstation,
                data.jobtype,
                data.createdjob,
                data.curaddr,
                data.destination,
                data.phone,
                data.createdjob,
                data.psg_id
            ]);

            DataTable.queueTaskTable.render(false);
        },
        updateTimeStatus: function () {
            if (DataTable.queueTaskTable.datatable != null) {
                DataTable.queueTaskTable.$table.find("tr").each(function (index, row) {
                    if ($(row).is('[data-ref]')) {
                        var html = DataTable.queueTaskTable.datatable.cell(row, 6).render("type");
                        $(row).find("td:eq(6)").html(html);
                        DataTable.queueTaskTable.setIndicator();
                    }
                });
            }
        },
        customTableHeader: function () {
            $(".queues.table-data").find('#searchQueueInput').on('keyup', function () {
                DataTable.queueTaskTable.datatable.search(this.value).draw(false);
            });

            var controls = $(".queues.table-data").find(".paging-length");
            $("#queueTaskTable_length").detach().appendTo(controls);
        },
        setIndicator: function () {
            var count = DataTable.queueTaskTable.$table.find(".status-time.new").size();
            if (count > 0) {
                $('[aria-controls="table-queue-tab"]').find(".indicator").html(count).show();
            }
            else {
                $('[aria-controls="table-queue-tab"]').find(".indicator").hide();
            }
        },
        callTaxiModal: function (button) {
            var data = $(button).data();
            Passenger.getData(data[7], function (response) {

                if (response.status) {

                    var passenger = response.psg_data;

                    Modal.assignTaskModal.modal.find(".text.phone").html(Util.toReadablePhoneNumber(passenger.phone.toString()));
                    Modal.assignTaskModal.modal.find(".text.current").html(passenger.curaddr);
                    Modal.assignTaskModal.modal.find(".text.destination").html(passenger.destination);

                    if (USER_DATA.broadcast === "true") {
                        var LatLngString = passenger.curloc[1] + "," + passenger.curloc[0];
                        if (passenger.curloc[0] === null 
                            || passenger.curloc[1] === null 
                            || passenger.curloc[0] === undefined 
                            || passenger.curloc[1] === undefined
                            || passenger.curloc[0] <= 0
                            || passenger.curloc[1] <= 0) {
                                LatLngString = "";

                            Modal.assignTaskModal.modal.find(".radian-wrapper").hide();
                            Modal.assignTaskModal.modal.find(".taxi-list-table").hide();
                            Form.broadcastTaskForm.form.find("button[type=submit]").hide();
                        } else {
                            Modal.assignTaskModal.modal.find(".radian-wrapper").show();
                            Modal.assignTaskModal.modal.find(".taxi-list-table").show();
                            Form.broadcastTaskForm.form.find("button[type=submit]").show();
                        }
                        var googleMapLink = '<a href="https://www.google.co.th/maps/@' + LatLngString + ',15z" target="_blank">' + LatLngString + '</a>';
                        Modal.assignTaskModal.modal.find(".text.current-lat-lng").html(googleMapLink);
                    }

                    if (passenger.provincearea == undefined) {
                        Modal.assignTaskModal.modal.find(".text.province").html("<i>-</i>");
                    }
                    else {
                        Modal.assignTaskModal.modal.find(".text.province").html(passenger.provincearea.nameTH);
                    }

                    if (passenger.detail == undefined || passenger.detail == "") {
                        Modal.assignTaskModal.modal.find(".text.detail").html("<i>-</i>");
                    }
                    else {
                        Modal.assignTaskModal.modal.find(".text.detail").html(passenger.detail);
                    }

                    Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false).val("");
                    Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);

                    Modal.assignTaskModal.modal.find("#pass_id").val(passenger._id);
                    Form.broadcastTaskForm.form.find("#pass_id").val(passenger._id);

                    if(passenger.curloc.length > 0 && passenger.curloc[0] !== null && passenger.curloc[1] !== null) {
                        Map.setView([passenger.curloc[1], passenger.curloc[0]], 12, { animate: false });
                    } else {
                        Map.setView(USER_CURRENT_LOCATION, 12, { animate: false });
                    }

                    Modal.assignTaskModal.modal.data({ "passengerData": passenger, "table": "queueTaskTable" });
                    Socket.emit("assigning task", { 'id': passenger._id, 'table': "queueTaskTable", 'user': USER_DATA });

                    Modal.assignTaskModal.open();

                    if (USER_DATA.broadcast === "true"
                        && (passenger.curloc[0] === null 
                        || passenger.curloc[1] === null 
                        || passenger.curloc[0] === undefined 
                        || passenger.curloc[1] === undefined
                        || passenger.curloc[0] <= 0
                        || passenger.curloc[1] <= 0)) {

                        Modal.messageBoxModal.setTitle("ขออภัยค่ะ");
                        Modal.messageBoxModal.setMessage("งานที่ท่านเลือกไม่มีตำแหน่ง ละติจุด, ลองจิจุด<br>กรุณาเลือกแท็กซี่บนแผนที่ เพื่อจ่ายงานตรง ");
                        Modal.messageBoxModal.open();
                    }
                }
                else {
                    Notification.defaultError();
                }
            });
        }
    },
    advanceTaskTable: {
        $table: $("#advanceTaskTable"),
        datatable: null,
        initial: function (loader) {

            DataTable.advanceTaskTable.datatable = DataTable.advanceTaskTable.$table.DataTable({
                "scrollY": (window.screen.availHeight - 250),
                "scrollCollapse": true,
                pageLength: 20,
                "paging": false,
                "autoWidth": false,
                "searching": true,
                deferRender: true,
                "oLanguage": {
                    "sEmptyTable": "ไม่มีข้อมูลให้แสดงผล",
                    "sInfo": "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ"
                },
                "columnDefs": [
                {
                    targets: 0,
                    width: "30px",
                    render: function (data, type, row) {
                        return (data == undefined || data == "" ? "-" : data);
                    }
                },
                {
                    targets: 1,
                    width: "50px",
                    render: function (data, type, row) {
                        return (data == undefined) ? '' : '<span style="font-size:12px">' + moment(data).format('DD/MM/YYYY')+ '</sapn><br>' + moment(data).format('HH:mm');
                    }
                },
                {
                    targets: [2, 3, 4],
                    className: "inline-edit",
                    createdCell: function (td, cellData, rowData, row, col) {

                        if (col == 2) {
                            $(td).attr("data-field-name", "curaddr");
                        }
                        else if (col == 3) {
                            $(td).attr("data-field-name", "destination");
                        }
                        else if (col == 4) {
                            $(td).attr("data-field-name", "phone");
                        }

                        $(td).attr({
                            'title': 'ดับเบิ้ลคลิ๊กเพื่อแก้ไข',
                            'data-toggle': 'tooltip'
                        }).on('dblclick', DataTable.inlineEditable);
                    }
                },
                {
                    targets: 4,
                    render: function (data, type, row) {
                        return (data == undefined ? '' : Util.toReadablePhoneNumber(data.toString()));
                    }
                },
                {
                    targets: 5,
                    width: "80px",
                    render: function (data, type, row) {
                        var cell = '<div class="btn-group" role="group">';
                        cell += '<button type="button" class="btn btn-default" data-toggle="tooltip" title="ลบออก" onclick="Passenger.showConfirmRemoveModal(\'' + data + '\');"><i class="fa fa-trash-o"></i></button>';
                        cell += '</div>';
                        return cell;
                    }
                },
                {
                    targets: [4, 5],
                    visible: window.screen.availWidth > 414,
                }
                ],
                createdRow: function (row, data, index) {

                    $(row).attr("data-ref", data[5]);

                    $(row).find('[data-toggle="tooltip"]')
                    .tooltip({ placement: 'top', container: 'body', animation: false })
                    .on('show.bs.tooltip', function () { $('body').find('.tooltip.in').remove(); });
                },
                drawCallback: function (settings) {
                    this.api().rows({ page: 'current' }).data().filter(function (data) {
                        if (LOCAL_STATE.length <= 0) {
                            DataTable.unlockLine({
                                id: data[5],
                                table: "advanceTaskTable"
                            });
                        }
                        else {
                            LOCAL_STATE.filter(function (task) {

                                if (DataTable.advanceTaskTable.$table.find('[data-ref=' + task.id + ']').find('.inline-edit.active').size() >= 1) { return; }

                                task.id == data[5] ? DataTable.lockLine(task) : DataTable.unlockLine({ id: data[5], table: "advanceTaskTable" });
                            });
                        }
                    });
                }
            });

            DataTable.advanceTaskTable.update(loader);
        },
        render: function (state) {

            if (DataTable.advanceTaskTable.$table.find('.inline-edit.active').size() >= 1) { return; }

            DataTable.advanceTaskTable.datatable.draw(state);
        },
        update: function (loader) {
            Http.post(CONSTRAINTS.serviceURI.getAdvanceList, {
                loader: loader,
                onSuccess: function (response) {
                    if (response.status) {
                        DataTable.advanceTaskTable.datatable.clear();
                        DataTable.advanceTaskTable.create(response.data);
                        DataTable.advanceTaskTable.customTableHeader();
                    } else {
                        DataTable.advanceTaskTable.datatable.clear().draw(false);
                    }
                }
            });
        },
        create: function (data) {

            if (data == undefined) { return false; }
            $(data).each(function (index, task) {
                DataTable.advanceTaskTable.datatable.row.add([
                    task.ccstation,
                    task.createdjob,
                    task.curaddr,
                    task.destination,
                    task.phone,
                    task._id
                ]).order(1, "asc").draw(false);
            });
        },
        addRow: function (data) {

            if (data == undefined) { return false; }

            DataTable.advanceTaskTable.datatable.row.add([
                data.ccstation,
                data.createdjob,
                data.curaddr,
                data.destination,
                data.phone,
                data._id
            ]);

            DataTable.advanceTaskTable.render(false);
        },
        updateRow: function (data) {

            var row = DataTable.advanceTaskTable.$table.find('[data-ref="' + data.psg_id + '"]')[0];
            var rowIndex = DataTable.advanceTaskTable.datatable.row(row).index();

            DataTable.advanceTaskTable.datatable.row(rowIndex).data([
                data.ccstation,
                data.createdjob,
                data.curaddr,
                data.destination,
                data.phone,
                data.psg_id
            ]);

            DataTable.advanceTaskTable.render(false);
        },
        customTableHeader: function () {
            $(".advance.table-data").find('#searchAdvanceTaskInput').on('keyup', function () {
                DataTable.advanceTaskTable.datatable.search(this.value).draw(false);
            });
        },
        intervalUpdate: function () {
            setInterval(function () {

                var count = 0;

                DataTable.advanceTaskTable.datatable.data().filter(function (data) {
                    var currentTime = new Date().getTime();
                    var taskTime = new Date(data[1]).getTime();
                    var diffTime = USER_DATA.group === 'cricritaxi' ? 0.5*60*60*1000 : 10800000;
                    if ((taskTime - currentTime) <= diffTime) {
                        count++;
                    }
                });

                if (count > 0) {
                    DataTable.queueTaskTable.update(true);
                    DataTable.advanceTaskTable.update(false);

                    Notification.remove();
                    Notification.show({
                        title: "เตือนงานจองล่วงหน้า",
                        text: "งานจองล่วงหน้าถึงเวลาแล้ว " + count + " งาน",
                        time: 6000,
                        $class: "info"
                    });

                    count = 0;
                }
            }, 15000);
        }
    },
    pendingTaskTable: {
        $table: $("#pendingTaskTable"),
        datatable: null,
        initial: function (loader) {

            DataTable.pendingTaskTable.datatable = DataTable.pendingTaskTable.$table.DataTable({
                "scrollY": (window.screen.availHeight - 250),
                "scrollCollapse": true,
                "paging": false,
                pageLength: 20,
                order: [[2, 'asc']],
                "autoWidth": false,
                deferRender: true,
                "searching": true,
                "oLanguage": {
                    "sEmptyTable": "ไม่มีข้อมูลให้แสดงผล",
                    "sInfo": "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ"
                },
                "columnDefs": [
                {
                    targets: 0,
                    render: function (data, type, row) {
                        return (data == undefined || data == "" ? "-" : data);
                    }
                },
                {
                    targets: 1,
                    visible: window.screen.availWidth > 414,
                    className: "jobtype",
                    render: function (data, type, row) {
                        var tooltipNormal = '<span class="circle-text default" data-toggle="tooltip" title="คิวงานปกติ">N</span>';
                        var tooltipAdvance = '<span class="task-type circle-text orange" data-toggle="tooltip" title="งานจองล่วงหน้า">A</span>';
                        return (data != undefined && data == 'QUEUE' ? tooltipNormal : tooltipAdvance);
                    }
                },
                {
                    targets: 2,
                    render: function (data, type, row) {
                        return (data == undefined) ? '' : '<span style="font-size:12px">' + moment(data).format('DD/MM/YYYY')+ '</sapn><br>' + moment(data).format('HH:mm');
                    }
                },
                {
                    targets: 3,
                    render: function (data, type, row) {
                        if (data.createdvia == "LINE") {

                            var carplate = data.drv_carplate;

                            if (carplate.substr(carplate.length - 1, 1) === "+") {
                                carplate = carplate.substr(0, carplate.length - 1);
                            }

                            return '<span data-toggle="tooltip" title="งานนี้ถูกจ่ายผ่าน Line">' + carplate + '</span>';
                        } else {
                            if(data.status === "BROADCAST") {
                                return "";
                            } else {
                                return data.drv_carplate;
                            }
                        }
                    },
                    createdCell: function (td, cellData, rowData, row, col) {

                        $(td).attr("data-field-name", "drv_carplate");

                        if(cellData.status === "BROADCAST") {
                            $(td).css({ 'background-image': 'url(\"/assets/img/ellipsis.gif\")', 'background-repeat': 'no-repeat', 'background-position': 'center' });
                        } else {
                            $(td).css({ 'background': 'tranparent' });
                        }

                        cellData.createdvia == "LINE" ?
                            $(td).attr("class", "line-job") : $(td).attr("class", "normal-job");
                    }
                },
                {
                    targets: [4, 5, 6],
                    className: "inline-edit",
                    createdCell: function (td, cellData, rowData, row, col) {

                        if (col == 4) {
                            $(td).attr("data-field-name", "curaddr");
                        }
                        else if (col == 5) {
                            $(td).attr("data-field-name", "destination");
                        }
                        else if (col == 6) {
                            $(td).attr("data-field-name", "phone");
                        }

                        $(td).attr({
                            'title': 'ดับเบิ้ลคลิ๊กเพื่อแก้ไข',
                            'data-toggle': 'tooltip'
                        }).on('dblclick', DataTable.inlineEditable);
                    }
                },
                {
                    targets: [4, 6],
                    visible: window.screen.availWidth > 414,
                },
                {
                    targets: [4, 5],
                    width: "60px"
                },
                {
                    targets: 6,
                    width: "80px",
                    render: function (data, type, row) {
                        return (data == undefined ? '' : Util.toReadablePhoneNumber(data.toString()));
                    }
                },
                {
                    targets: 7,
                    visible: window.screen.availWidth > 414,
                    render: function (data, type, row) {
                        var cell = "";
                        if (data.status == "DEPENDING_REJECT") {
                            cell += '<span class="label-rejected"><i class="fa fa-exclamation"></i> งานถูกปฏิเสธ</span>';
                        } else if (data.status == "DEPENDING_TIMEOUT") {
                            cell += '<span class="label-rejected"><i class="fa fa-exclamation"></i>หมดเวลาคนขับไม่ตอบสนอง</span>';
                        } else {
                            cell += '<span class="status-time ' + Util.getTimeStatus(data.createdjob) + '">';
                            cell += '<i class="fa fa-bookmark"></i>';
                            var diff = Util.getDiffTime(data.createdjob);
                            cell += '<span class="detail">' + diff.time + '<br><span>' + diff.unit + '</span>';
                            cell += '</span>';
                        }

                        return cell;
                    }
                },
                {
                    targets: 8,
                    visible: window.screen.availWidth > 414,
                    render: function (data, type, row) {

                        var cell = '<div class="btn-group" role="group" aria-label="...">';
                        if (data.status == "DPENDING_LINE") {
                            cell += '<button type="button" class="btn btn-default showAssignLineModal" data-toggle="tooltip" title="จ่ายงานผ่าน Line" onclick="DataTable.pendingTaskTable.callAssignByLineAppModal(this);">';
                            cell += '<i class="material-icons">chat</i>';
                            cell += '</button>';
                        } else {
                            cell += '<button type="button" class="btn btn-default showFindTaxi" data-toggle="tooltip" title="จ่ายงาน" onclick="DataTable.pendingTaskTable.callTaxiModal(this);"><i class="fa fa-taxi"></i></button>';
                        }
                        cell += '<button type="button" class="btn btn-default removeCurrentPassenger" data-toggle="tooltip" title="ลบออก" onclick="Passenger.showConfirmRemoveModal(\'' + data._id + '\');"><i class="fa fa-trash-o"></i></button>';
                        cell += '</div>';
                        return cell;
                    }
                }
                ],
                createdRow: function (row, data, index) {

                    $(row).attr("data-ref", data[8]._id);
                    $(row).addClass("pending");

                    $(row).find('[data-toggle="tooltip"]')
                    .tooltip({ placement: 'top', container: 'body', animation: false })
                    .on('show.bs.tooltip', function () { $('body').find('.tooltip.in').remove(); });
                },
                rowCallback: function (row, data, index) {
                    $(row).find(".showFindTaxi").data(data);
                    $(row).find(".showAssignLineModal").data(data);
                },
                drawCallback: function (settings) {
                    this.api().rows({ page: 'current' }).data().filter(function (data) {
                        if (LOCAL_STATE.length <= 0) {
                            DataTable.unlockLine({
                                id: data[8]._id,
                                table: "pendingTaskTable"
                            });
                        }
                        else {
                            LOCAL_STATE.filter(function (task) {

                                if (DataTable.pendingTaskTable.$table.find('[data-ref=' + task.id + ']').find('.inline-edit.active').size() >= 1) { return; }

                                task.id == data[8]._id ? DataTable.lockLine(task) : DataTable.unlockLine({ id: data[8]._id, table: "pendingTaskTable" });
                            });
                        }
                    });
                }
            });

            DataTable.pendingTaskTable.update(loader);
        },
        render: function (state) {

            if (DataTable.pendingTaskTable.$table.find('.inline-edit.active').size() >= 1) { return; }

            DataTable.pendingTaskTable.datatable.draw(state);
        },
        update: function (loader) {
            Http.post(CONSTRAINTS.serviceURI.getDependingList, {
                loader: loader,
                onSuccess: function (response) {
                    if (response.status) {
                        DataTable.pendingTaskTable.datatable.clear();
                        DataTable.pendingTaskTable.create(response.data);
                        DataTable.pendingTaskTable.customTableHeader();
                    } else {
                        DataTable.pendingTaskTable.datatable.clear().draw(false);
                        DataTable.pendingTaskTable.setIndicator();
                    }
                }
            });
        },
        create: function (data) {
            if (data == undefined) { return false; }
            $(data).each(function (index, task) {
                DataTable.pendingTaskTable.datatable.row.add([
                    task.ccstation,
                    task.jobtype,
                    task.createdjob,
                    { 
                        status: task.status, 
                        prefixcarplate: task.prefixcarplate || '',
                        carplate: task.carplate,
                        drv_carplate: task.drv_carplate, 
                        createdvia: task.createdvia
                    },
                    task.curaddr,
                    task.destination,
                    task.phone,
                    { status: task.status, createdjob: task.createdjob },
                    { _id: task._id, status: task.status }
                ]).draw(false);
            });
            DataTable.pendingTaskTable.setIndicator();
        },
        addRow: function (data) {

            if (data == undefined) { return false; }

            DataTable.pendingTaskTable.datatable.row.add([
                data.ccstation,
                data.jobtype,
                data.createdjob,
                { 
                    status: data.status, 
                    prefixcarplate: data.prefixcarplate || '',
                    carplate: data.carplate,
                    drv_carplate: data.drv_carplate, 
                    createdvia: data.createdvia
                },
                data.curaddr,
                data.destination,
                data.phone,
                { status: data.status, createdjob: data.createdjob },
                { _id: data._id, status: data.status }
            ]);

            DataTable.pendingTaskTable.render(false);

            DataTable.pendingTaskTable.setIndicator();
        },
        updateRow: function (data) {

            var row = DataTable.pendingTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
            var rowIndex = DataTable.pendingTaskTable.datatable.row(row).index();

            DataTable.pendingTaskTable.datatable.row(rowIndex).data([
                data.ccstation,
                data.jobtype,
                data.createdjob,
                { 
                    status: data.status, 
                    prefixcarplate: data.prefixcarplate || '',
                    carplate: data.carplate,
                    drv_carplate: data.drv_carplate, 
                    createdvia: data.createdvia
                },
                data.curaddr,
                data.destination,
                data.phone,
                { status: data.status, createdjob: data.createdjob },
                { _id: data._id, status: data.status }
            ]);

            DataTable.pendingTaskTable.render(false);
        },
        updateTimeStatus: function () {
            if (DataTable.pendingTaskTable.datatable != null) {
                DataTable.pendingTaskTable.$table.find("tr").each(function (index, row) {
                    if ($(row).is('[data-ref]')) {
                        var html = DataTable.pendingTaskTable.datatable.cell(row, 7).render("type");
                        $(row).find("td:eq(7)").html(html);
                        DataTable.pendingTaskTable.setIndicator();
                    }
                });
            }
        },
        customTableHeader: function () {
            $(".pending.table-data").find('#searchPendingTaskInput').on('keyup', function () {
                DataTable.pendingTaskTable.datatable.search(this.value).draw(false);
            });
        },
        setIndicator: function () {

            var count = DataTable.pendingTaskTable.datatable.rows()[0].length;
            if (count > 0) {
                $('#leftTabList, #rightTabList').find('[aria-controls="table-pending-tab"]').find(".indicator").html(count).show();
            }
            else {
                $('#leftTabList, #rightTabList').find('[aria-controls="table-pending-tab"]').find(".indicator").hide();
            }
        },
        callTaxiModal: function (button) {
            var data = $(button).data();
            Passenger.getData(data[8]._id, function (response) {

                if (response.status) {

                    var passenger = response.psg_data;

                    Modal.assignTaskModal.modal.find(".text.phone").html(Util.toReadablePhoneNumber(passenger.phone.toString()));
                    Modal.assignTaskModal.modal.find(".text.current").html(passenger.curaddr);
                    Modal.assignTaskModal.modal.find(".text.destination").html(passenger.destination);
                    
                    if (USER_DATA.broadcast === "true") {
                        var LatLngString = passenger.curloc[1] + "," + passenger.curloc[0];
                        if (passenger.curloc[0] === null 
                            || passenger.curloc[1] === null 
                            || passenger.curloc[0] === undefined 
                            || passenger.curloc[1] === undefined
                            || passenger.curloc[0] <= 0
                            || passenger.curloc[1] <= 0) {
                                LatLngString = "";

                            Modal.assignTaskModal.modal.find(".radian-wrapper").hide();
                            Modal.assignTaskModal.modal.find(".taxi-list-table").hide();
                            Form.broadcastTaskForm.form.find("button[type=submit]").hide();
                        } else {
                            Modal.assignTaskModal.modal.find(".radian-wrapper").show();
                            Modal.assignTaskModal.modal.find(".taxi-list-table").show();
                            Form.broadcastTaskForm.form.find("button[type=submit]").show();
                        }
                        var googleMapLink = '<a href="https://www.google.co.th/maps/@' + LatLngString + ',15z" target="_blank">' + LatLngString + '</a>';
                        Modal.assignTaskModal.modal.find(".text.current-lat-lng").html(googleMapLink);
                    }

                    if (passenger.detail == undefined || passenger.detail == "") {
                        Modal.assignTaskModal.modal.find(".text.detail").html("<i>-</i>");
                    }
                    else {
                        Modal.assignTaskModal.modal.find(".text.detail").html(passenger.detail);
                    }
                    if(passenger.drv_carplate && USER_DATA.broadcast === "false") {
                        Modal.assignTaskModal.modal.find("#car-plate-inp").val(passenger.drv_carplate)
                        Modal.assignTaskModal.modal.find("#car-plate-inp")[0].setSelectionRange(0, 8);   
                    }

                    Modal.assignTaskModal.modal.data({ "passengerData": passenger, "table": "pendingTaskTable" });
                    var data = { 'id': passenger._id, 'table': "queueTaskTable", 'user': USER_DATA };
                    LOCAL_STATE.push(data);
                    Socket.emit("assigning task", data);

                    Modal.assignTaskModal.modal.find("#pass_id").val(passenger._id);
                    Modal.assignTaskModal.open();

                    if (USER_DATA.broadcast === "true"
                        && (passenger.curloc[0] === null 
                        || passenger.curloc[1] === null 
                        || passenger.curloc[0] === undefined 
                        || passenger.curloc[1] === undefined
                        || passenger.curloc[0] <= 0
                        || passenger.curloc[1] <= 0)) {

                        Modal.messageBoxModal.setTitle("ขออภัยค่ะ");
                        Modal.messageBoxModal.setMessage("งานที่ท่านเลือกไม่มีตำแหน่ง ละติจุด, ลองจิจุด<br>กรุณาเลือกแท็กซี่บนแผนที่ เพื่อจ่ายงานตรง ");
                        Modal.messageBoxModal.open();
                    }
                }
                else {
                    Notification.defaultError();
                }
            });
        },
        callAssignByLineAppModal: function (button) {

            var data = $(button).data();
            Passenger.getData(data[8]._id, function (response) {

                if (response.status) {

                    var passenger = response.psg_data;

                    Modal.assignTaskByLineModal.modal.find(".text.phone").html(Util.toReadablePhoneNumber(passenger.phone.toString()));
                    Modal.assignTaskByLineModal.modal.find(".text.current").html(passenger.curaddr);
                    Modal.assignTaskByLineModal.modal.find(".text.destination").html(passenger.destination);
                    Modal.assignTaskByLineModal.modal.find(".text.detail").html(passenger.detail);
                    Modal.assignTaskByLineModal.modal.find(".text.carplate").html(passenger.drv_carplate);

                    Modal.assignTaskByLineModal.modal.data({ "passengerData": passenger, "table": "pendingTaskTable" });
                    var data = { 'id': passenger._id, 'table': "pendingTaskTable", 'user': USER_DATA };
                    LOCAL_STATE.push(data);
                    Socket.emit("assigning task", data);

                    Modal.assignTaskByLineModal.modal.data("task", passenger);
                    Modal.assignTaskByLineModal.open();

                }
                else {
                    Notification.defaultError();
                }
            });
        },
    },
    assignedTaskTable: {
        $table: $("#assignedTaskTable"),
        datatable: null,
        initial: function (loader) {

            DataTable.assignedTaskTable.datatable = DataTable.assignedTaskTable.$table.DataTable({
                paging: true,
                autoWidth: false,
                pageLength: 50,
                order: [[1, 'desc']],
                deferRender: true,
                searching: true,
                scrollCollapse: true,
                scrollY: (window.screen.availHeight - 270),
                oLanguage: {
                    sEmptyTable: "ไม่มีข้อมูลให้แสดงผล",
                    sLengthMenu: " _MENU_ ",
                    sInfo: "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ"
                },
                columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row) {
                        return (data == undefined || data == "" ? "-" : data);
                    }
                },
                {
                    targets: 1,
                    visible: window.screen.availWidth > 414,
                    render: function (data, type, row) {
                        return (data == undefined) ? '' : '<span style="font-size:12px">' + moment(data).format('DD/MM/YYYY')+ '</sapn><br>' + moment(data).format('HH:mm');
                    },
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).attr("data-field-name", "createdjob");
                    }
                },
                {
                    targets: 2,
                    render: function (data, type, row) {
                        return (data == undefined) ? '' : '<span style="font-size:12px">' + moment(data).format('DD/MM/YYYY')+ '</sapn><br>' + moment(data).format('HH:mm');
                    },
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).attr("data-field-name", "createdjob");
                    }
                },
                {
                    targets: 3,
                    render: function (data, type, row) {
                        if (data.createdvia == "LINE") {

                            var carplate = data.drv_carplate;

                            if (carplate.substr(carplate.length - 1, 1) === "+") {
                                carplate = carplate.substr(0, carplate.length - 1);
                            }

                            return '<span data-toggle="tooltip" title="งานนี้ถูกจ่ายผ่าน Line">' + carplate + '</span>';
                        } else {
                            return data.drv_carplate;
                        }
                    },
                    createdCell: function (td, cellData, rowData, row, col) {

                        $(td).attr("data-field-name", "drv_carplate");

                        cellData.createdvia == "LINE" ?
                        $(td).attr("class", "inline-edit line-job") : $(td).attr("class", "inline-edit normal-job");

                        $(td).on('dblclick', DataTable.inlineEditable);
                    }
                },
                {
                    targets: [4, 5, 6, 7],
                    className: "inline-edit",
                    createdCell: function (td, cellData, rowData, row, col) {

                        if (col == 4) {
                            $(td).attr("data-field-name", "curaddr");
                        }
                        else if (col == 5) {
                            $(td).attr("data-field-name", "destination");
                        }
                        else if (col == 6) {
                            $(td).attr("data-field-name", "phone");
                        }
                        else if (col == 7) {
                            $(td).attr("data-field-name", "cccomment");
                        }

                        $(td).attr({
                            'title': 'ดับเบิ้ลคลิ๊กเพื่อแก้ไข',
                            'data-toggle': 'tooltip'
                        }).on('dblclick', DataTable.inlineEditable);
                    }
                },
                {
                    targets: [4, 6],
                    visible: window.screen.availWidth > 414,
                },
                {
                    targets: 6,
                    width: "70px",
                    render: function (data, type, row) {
                        return (data == undefined ? '' : Util.toReadablePhoneNumber(data.toString()));
                    }
                },
                {
                    targets: 8,
                    render: function (data, type, row) {
                        var cell = '<div class="btn-group" role="group">';
                        cell += '<button type="button" class="btn showTaskDetail" onclick="DataTable.assignedTaskTable.showTaskDetail(\'' + data + '\');">';
                        cell += '<i class="material-icons">info_outline</i>';
                        cell += '</button>';
                        cell += '</div>';
                        return cell;
                    },
                    createdCell: function (td, cellData, rowData, row, col) {
                        $(td).find(".showTaskDetail").attr({
                            'title': 'แสดงรายละเอียด',
                            'data-toggle': 'tooltip'
                        });
                    }
                }
                ],
                createdRow: function (row, data, index) {

                    $(row).attr("data-ref", data[8]);

                    var tooltipOptions = {
                        placement: 'top', 
                        container: 'body', 
                        animation: false
                    }

                    var onTooltipShow = function() {
                        $('body').find('.tooltip.in').remove();
                    }

                    $(row).find('[data-toggle="tooltip"]').tooltip(tooltipOptions).on('show.bs.tooltip', onTooltipShow);
                },
                rowCallback: function (row, data, index) {
                    $(row).find(".showTaskDetail").data(data);
                    if (data[9].cccomment !== "") {
                        $(row).addClass("has-comment");
                    } else {
                        $(row).removeClass("has-comment");
                    }
                },
                drawCallback: function (settings) {
                    this.api().rows({ page: 'current' }).data().filter(function (data) {
                        if (LOCAL_STATE.length <= 0) {
                            DataTable.unlockLine({
                                id: data[8],
                                table: "assignedTaskTable"
                            });
                        }
                        else {
                            LOCAL_STATE.filter(function (task) {

                                if (DataTable.assignedTaskTable.$table.find('[data-ref=' + task.id + ']').find('.inline-edit.active').size() >= 1) { return; }

                                task.id == data[8] ? DataTable.lockLine(task) : DataTable.unlockLine({ id: data[8], table: "assignedTaskTable" });;
                            });
                        }

                        DataTable.assignedTaskTable.datatable.columns([2]).visible(DataTable.assignedTaskTable.$table.width() >= 850);
                    });
                }
            });

            DataTable.assignedTaskTable.update(loader);
        },
        render: function (state) {

            if (DataTable.assignedTaskTable.$table.find('.inline-edit.active').size() >= 1) { return; }

            DataTable.assignedTaskTable.datatable.draw(state);
        },
        update: function (loader) {
            Http.post(CONSTRAINTS.serviceURI.getAssignList, {
                loader: loader,
                onSuccess: function (response) {
                    if (response.status) {
                        DataTable.assignedTaskTable.datatable.clear();
                        DataTable.assignedTaskTable.create(response.data);
                        DataTable.assignedTaskTable.customTableHeader();
                        TaskComment.updateAll();
                    } else {
                        DataTable.assignedTaskTable.datatable.clear().draw();
                        DataTable.assignedTaskTable.customTableHeader();
                    }
                }
            });
        },
        create: function (data) {

            if (data == undefined) { return false; }

            $(data).each(function (index, task) {
                DataTable.assignedTaskTable.datatable.row.add([
                    task.ccstation || '',
                    task.createdjob || '',
                    task.dpendingjob || '',
                    {
                        prefixcarplate: task.prefixcarplate || '',
                        carplate: task.carplate,
                        drv_carplate: task.drv_carplate, 
                        createdvia: task.createdvia
                    },
                    task.curaddr || '',
                    task.destination || '',
                    task.phone || '',
                    task.cccomment || '',
                    task._id || '',
                    task
                ]).order(2, "desc").draw();
            });
        },
        addRow: function (data) {

            if (data == undefined) { return false; }

            DataTable.assignedTaskTable.datatable.row.add([
                data.ccstation || '',
                data.createdjob || '',
                data.dpendingjob || '',
                {
                    prefixcarplate: data.prefixcarplate || '',
                    carplate: data.carplate,
                    drv_carplate: data.drv_carplate,
                    createdvia: data.createdvia
                },
                data.curaddr || '',
                data.destination || '',
                data.phone || '',
                data.cccomment || '',
                data._id || '',
                data
            ]);

            DataTable.assignedTaskTable.render(true);
        },
        updateRow: function (data) {

            var row = DataTable.assignedTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
            var rowIndex = DataTable.assignedTaskTable.datatable.row(row).index();

            DataTable.assignedTaskTable.datatable.row(rowIndex).data([
                data.ccstation,
                data.createdjob,
                data.dpendingjob,
                {
                    prefixcarplate: data.prefixcarplate || '',
                    carplate: data.carplate,
                    drv_carplate: data.drv_carplate,
                    createdvia: data.createdvia
                },
                data.curaddr,
                data.destination,
                data.phone,
                data.cccomment,
                data._id,
                data
            ]);

            DataTable.assignedTaskTable.render(true);
        },
        customTableHeader: function () {
            $(".assigned.table-data").find('#searchAssignedTaskInput').on('keyup', function () {
                DataTable.assignedTaskTable.datatable.search(this.value).draw();
            });
            var controls = $(".assigned.table-data").find(".paging-length");
            $("#assignedTaskTable_length").detach().appendTo(controls);
        },
        showTaskDetail: function (pass_id) {

            Http.post(CONSTRAINTS.serviceURI.getReAssignJobDetail, {
                loader: false,
                data: { "psg_id": pass_id },
                onSuccess: function (response) {
                    if (response.status) {
                        if (response.drv_register) {

                            var passenger = response.psg_data;
                            var driver = response.drv_data;

                            Modal.assignedTaskModal.modal.find(".text.phone").html(Util.toReadablePhoneNumber(passenger.phone.toString()));
                            Modal.assignedTaskModal.modal.find(".text.current").html(passenger.curaddr);
                            Modal.assignedTaskModal.modal.find(".text.destination").html(passenger.destination);

                            if (passenger.detail == undefined || passenger.detail == "") {
                                Modal.assignedTaskModal.modal.find(".text.detail").html("<i>-</i>");
                            }
                            else {
                                Modal.assignedTaskModal.modal.find(".text.detail").html(passenger.detail);
                            }

                            if(driver !== null) {
                                driver.imgface = Taxi.getImageFace(driver.imgface);

                                Modal.assignedTaskModal.modal.find("img.driver-img").attr("src", CONSTRAINTS.driverImageURL + driver.imgface);
                                Modal.assignedTaskModal.modal.find(".text.driver_fullname").html(driver.fname + " " + driver.lname);
                                Modal.assignedTaskModal.modal.find(".text.driver_carplate").html(driver.prefixcarplate + " " + driver.carplate);
                                Modal.assignedTaskModal.modal.find(".text.driver_phone").html(Util.toReadablePhoneNumber(driver.phone.toString()));
                                Modal.assignedTaskModal.modal.find(".commentForm > input[type=text]").val(driver.cccomment);

                                Modal.assignedTaskModal.addDriverToMap(driver);
                            }

                            Modal.assignedTaskModal.modal.find(".reject-btn").prop("disabled", false);
                            Modal.assignedTaskModal.modal.find(".reject-btn").attr("data-ref", passenger._id);
                            Modal.assignedTaskModal.open();
                            
                        }
                        else {

                            var passenger = response.psg_data;

                            Modal.assignedTaskModal.modal.find(".text.phone").html(Util.toReadablePhoneNumber(passenger.phone.toString()));
                            Modal.assignedTaskModal.modal.find(".text.current").html(passenger.curaddr);
                            Modal.assignedTaskModal.modal.find(".text.destination").html(passenger.destination);
                            Modal.assignedTaskModal.modal.find(".commentForm > input[type=text]").val(passenger.cccomment);

                            if (passenger.detail == undefined || passenger.detail == "") {
                                Modal.assignedTaskModal.modal.find(".text.detail").html("<i>-</i>");
                            }
                            else {
                                Modal.assignedTaskModal.modal.find(".text.detail").html(passenger.detail);
                            }

                            Modal.assignedTaskModal.modal.find(".reject-btn").prop("disabled", false);
                            Modal.assignedTaskModal.modal.find(".reject-btn").attr("data-ref", passenger._id);

                            Modal.assignedTaskModal.modal.find(".driver-wrap").hide();
                            Modal.assignedTaskModal.modal.find(".alert-missing-driver").html("งานนี้ถูกจ่ายไปยังแท็กซี่ที่<u>ไม่ได้ใช้</u>แอพฯ!<br>หมายเลข: " + passenger.drv_carplate).show();

                            Modal.assignedTaskModal.open();
                        }
                    } else {
                        Notification.defaultError();
                    }
                }
            });
        },
    },
    taxiReportTable: {
        $table: $("#taxiReportTable"),
        datatable: null,
        initial: function () {
            DataTable.taxiReportTable.datatable = DataTable.taxiReportTable.$table.DataTable({
                responsive: true,
                deferRender: true,
                "scrollY": "400px",
                "scrollCollapse": true,
                "paging": false,
                "language": {
                    search: "",
                    searchPlaceholder: "ค้นหาแท็กซี่",
                    "sInfo": "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ"
                },
                "footer": false,
                "dom": 'Bfrtip',
                "buttons": [
                'csv', 'excel', 'pdf'
                ]
            });

            $("#report-date-filter").datetimepicker({
                format: "DD/MM/YYYY",
            });
        },
        update: function (loader) {
            Http.post(CONSTRAINTS.serviceURI.countJobPerDrv, {
                loader: loader,
                data: {
                    "startTime": parseInt(Report.getCurrentDate().getTime() - Report.day),
                    "endTime": parseInt(Report.getCurrentDate().getTime())
                },
                onSuccess: function (response) {
                    if (response.status) {
                        DataTable.taxiReportTable.create(response.data);
                        DataTable.taxiReportTable.customTableHeader();
                    } else {
                    }
                }
            });
        },
        create: function (taxi_list) {

            if (taxi_list == undefined) { return false; }

            $(taxi_list).each(function (index, taxi) {
                DataTable.taxiReportTable.addRow(index, taxi);
            });
        },
        addRow: function (index, taxi) {
            var amount = Math.floor((Math.random() * 20) + 1);
            DataTable.taxiReportTable.datatable.row.add([
                (index + 1),
                "-",
                taxi._id,
                "-",
                "-",
                taxi.count,
                "-",
            ]).draw(false);
        },
        customTableHeader: function () {
            $(".report-driver.table-data").find('#searchReportDriverInput').on('keyup', function () {
                DataTable.taxiReportTable.datatable.search(this.value).draw(false);
            });
        },
    },
    searchTaskResultTable: {
        $table: $("#searchTaskResultTable"),
        datatable: null,
        initial: function () {
            DataTable.searchTaskResultTable.datatable = DataTable.searchTaskResultTable.$table.dataTable({
                paging: true,
                scrollY: "650px",
                deferRender: true,
                scrollCollapse: true,
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ],
                language: {
                    search: "",
                    searchPlaceholder: "ค้นหา",
                    sInfo: "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ",
                },
                columnDefs: [
                {
                    targets: [1, 2, 3, 4, 5],
                    className: "inline-edit",
                    createdCell: function (td, cellData, rowData, row, col) {

                        switch (col) {
                            case 1: td.setAttribute("data-field-name", "phone");
                                break;
                            case 2: td.setAttribute("data-field-name", "curaddr");
                                break;
                            case 3: td.setAttribute("data-field-name", "destination");
                                break;
                            case 4: td.setAttribute("data-field-name", "drv_carplate");
                                break;
                            case 5: td.setAttribute("data-field-name", "cccomment");
                                break;
                        }

                        td.setAttribute("title", "ดับเบิ้ลคลิ๊กเพื่อแก้ไข");
                        td.setAttribute("data-toggle", "tooltip");

                        $(td).inlineEditable();
                    }
                },
                {
                    targets: 8,
                    render: function (data, type, row) {
                        return '<a href="#" class="data-status" data-toggle="tooltip" data-original-title="' + DataTable.searchTaskResultTable.statusText[data] + '">' + data + '</a>';
                    }
                },
                {
                    targets: 9,
                    width: "45px"
                }],
                createdRow: function (row, data, index) {
                    row.setAttribute("data-ref", data[10]);
                },
                drawCallback: function (settings) {
                    Preloader.hide(0);
                    DataTable.searchTaskResultTable.$table.find('.data-status[data-toggle="tooltip"]').tooltip({ animation: false });
                }
            });
        },
        statusText: {
            INITIATE: "งานใหม่", OFF: "ปิดแอพฯ", ON: "พร้อมรับงาน", WAIT: "รอผู้โดยสารตอบกลับ", DPENDING: "งานรอการตอบรับ",
            BUSY: "กำลังไปรับผู้โดยสาร", PICK: "กำลังไปส่งผู้โดยสาร", ASSIGNED: "จ่ายให้แท็กซี่แล้ว", BROADCAST: "กระจายงานออกไปแล้ว",
            FINISHED: "จบงานเรียบร้อย", DELETED: "งานถูกลบทิ้ง"
        },
        render: function (data) {

            if(!data) {
                return;
            }

            if(data instanceof Array && data.length <= 0) {
                DataTable.searchTaskResultTable.clear();
                return;
            } 

            var tasks = [];
            data.filter(function (task, index) {
                tasks.push([
                    (index + 1) || '',
                    task.autophone || '',
                    task.phone || '',
                    task.curaddr || '',
                    task.destination || '',
                    task.drv_carplate || '',
                    task.cccomment || '',
                    task.createdvia || '',
                    task.ccstation || '',
                    task.status || '',
                    Util.getFullDate(task.createdjob) + " " + Util.getCurrentTime(task.createdjob) + ' น.',
                    task._id
                ]);
            });

            var oTable = DataTable.searchTaskResultTable.datatable;

            oTable.fnClearTable();
            oTable.fnAddData(tasks);
        },
        clear: function () {
            DataTable.searchTaskResultTable.datatable.fnClearTable();
        },
        getData: function (data) {

            Http.post(CONSTRAINTS.serviceURI.searchFinishList, {
                data: data,
                onSuccess: function (response) {
                    if (response.status) {
                        Notification.remove();
                        DataTable.searchTaskResultTable.render(response.data);
                    }
                    else {
                        DataTable.searchTaskResultTable.clear();
                    }
                }
            });
        }
    },
    inlineEditable: function (event) {

        event.stopPropagation();

        if ($(event.target).closest("tr").hasClass("lock")) {
            return false;
        }

        var id = $(event.target).closest("tr").attr("data-ref");
        var table = $(event.target).closest('table').prop('id');
        var data = { 'id': id, 'table': table, 'user': USER_DATA };

        LOCAL_STATE.push(data);
        Socket.emit('inline edit', data);

        var inlineEdit = $(this);
        var isLineJob = $(this).hasClass("line-job");

        var originHtml = "";

        if (isLineJob) {
            originHtml = $(this).find("span").html() + "+";
        }
        else {
            originHtml = $(this).html();
        }


        inlineEdit.addClass("active");
        inlineEdit.html('<form><input type="text" value="' + originHtml + '" /></form>');
        inlineEdit.find("form > input").focus();


        inlineEdit.unbind("dblclick", DataTable.inlineEditable);


        var onDocumentClick = function (event) {

            if (!$(event.target).parent().parent().hasClass("inline-edit")) {

                $(".inline-edit.active").removeClass("active");
                inlineEdit.on("dblclick", DataTable.inlineEditable);
                $(document).unbind('click', onDocumentClick);

                var id = inlineEdit.closest("tr").attr("data-ref");
                var table = inlineEdit.closest('table').prop('id');
                var data = { 'id': id, 'table': table };
                LOCAL_STATE_CONTROL.remove(data);
                Socket.emit('unlock line', data);

                DataTable[table].datatable.draw();
            }
        };


        var editData = function (newValue) {

            var newText = "";

            if (newValue.substr(newValue.length - 1, 1) === "+") {

                newText = '<span data-toggle="tooltip" title="" data-original-title="งานนี้ต้องจ่ายผ่าน Line">' + newValue.slice(0, -1) + '</span>';

                inlineEdit.addClass("line-job");
                inlineEdit.removeClass("normal-job");
            }
            else {
                newText = newValue;

                inlineEdit.addClass("normal-job");
                inlineEdit.removeClass("line-job");
            }

            inlineEdit.html(newText);

            if (newValue !== originHtml) {

                var id = $(inlineEdit).closest("tr").attr("data-ref");
                var name = $(inlineEdit).attr("data-field-name");
                var data = {};
                $(data).data(name, newValue);
                Passenger.editData(id, $(data).data());
            }

            inlineEdit.on("dblclick", DataTable.inlineEditable);
            $(document).unbind('click', onDocumentClick);
            $("table .inline-edit.active").removeClass("active");

            var id = inlineEdit.closest("tr").attr("data-ref");
            var table = inlineEdit.closest('table').prop('id');

            var data = { 'id': id, 'table': table };

            LOCAL_STATE_CONTROL.remove(data);

            Socket.emit('unlock line', data);

            DataTable[table].datatable.draw();
        }


        //On Document Click
        $(document).on('click', onDocumentClick);


        // On Form Submit
        inlineEdit.find("form").on("submit", function (event) {

            event.preventDefault();

            editData(this.firstElementChild.value);
        });


        // On Input lost focus
        inlineEdit.find("input").on("blur", function (event) {

            editData(this.value);
        });
    },
    updateAll: function (immediate) {
        DataTable.queueTaskTable.update(true);
        DataTable.advanceTaskTable.update(true);
        DataTable.pendingTaskTable.update(true);
        DataTable.assignedTaskTable.update(true);
        Socket.emit('update state', { user: USER_DATA });
    },
    lockLine: function (data) {
        var html = '<p class="user-locker">' + data.user.name + '<br>กำลังทำงานนี้อยู่...</p>';
        var row = DataTable[data.table].$table.find('[data-ref="' + data.id + '"]')[0];
        if (row !== undefined) {
            $(row).addClass("lock");
            $(row).find(".user-locker").remove();
            window.screen.availWidth > 414 ? $(row).find("td").last().append(html) : $(row).append(html);
        }
    },
    unlockLine: function (data) {
        var row = DataTable[data.table].$table.find('[data-ref="' + data.id + '"]')[0];
        if (row !== undefined) {
            $(row).removeClass("lock");
            $(row).find(".user-locker").remove();
        }
    }
};


var LocalStorageManager = {
    initial: function() {

        Array.prototype.move = function (old_index, new_index) {
            while (old_index < 0) {
                old_index += this.length;
            }
            while (new_index < 0) {
                new_index += this.length;
            }
            if (new_index >= this.length) {
                var k = new_index - this.length;
                while ((k--) + 1) {
                    this.push(undefined);
                }
            }
            this.splice(new_index, 0, this.splice(old_index, 1)[0]);
            return this; // for testing purposes
        };
    },
    get: function(name) {
        return localStorage.getItem(name);
    },
    new: function(key, data) {
        return localStorage.setItem(key, data);
    },
    add: function(key, data) {
        var _data = localStorage.getItem(key);
        _data ? localStorage.setItem(key, _data + ",," + data) : localStorage.setItem(key, data);
    },
    append: function(key, data) {
        var _data = localStorage.getItem(key);
        return localStorage.setItem(key, _data + ",," + data);
    },
    prepend: function(key, data) {
        var _data = localStorage.getItem(key);
        return localStorage.setItem(key, data + ",," + _data);
    },
    getAsArray: function(key) {
            var poi = localStorage.getItem(key);
            return poi ? poi.split(",,") : [];
    }
}


var Form = {
    createTaskForm: {
        form: $("#createTaskForm"),
        initial: function () {
            Form.createTaskForm.onSubmit();
            Form.createTaskForm.autocompleteInit();
            Form.createTaskForm.form.find("#taxiAmount").ForceNumericOnly();
            Form.createTaskForm.setUpFieldInputStep();
            Form.createTaskForm.setUpDuplicatePhoneNoPopover();
            Form.createTaskForm.form.find("#pass-phone-inp").on("focusin", Form.createTaskForm.checkDuplicatePhoneNo);
            Form.createTaskForm.form.find("#pass-phone-inp").on("keyup", Form.createTaskForm.checkDuplicatePhoneNo);
        },
        setUpFieldInputStep: function () {

            var form = Form.createTaskForm.form;

            form.find("#pass-phone-inp").on("keyup", function (event) {

                var code = event.keyCode || event.which;

                if (code === 13) {

                    if (!Form.createTaskForm.form.valid()) { $(this).focus(); return; }
                    Form.createTaskForm.form.find("#pass-from-inp").focus();
                }

                return;
            });

            form.find("#pass-from-inp").on("keyup", function (event) {

                var code = event.keyCode || event.which;
                var error = $(this).hasClass('error');

                if (code === 13 && !error) {
                    Form.createTaskForm.form.find("#pass-dest-inp").focus();
                }

                return;
            });

            form.find("#pass-dest-inp").on("keyup", function (event) {

                var code = event.keyCode || event.which;

                (Form.createTaskForm.form.valid() && code === 13) && Form.createTaskForm.customSubmit();
                return;
            });

            form.find("button#saveBtn").on("click", function (event) {

                if(!Form.createTaskForm.form.valid()) {
                    return;
                }

                if(USER_DATA.broadcast === "true") {
                    if(Form.createTaskForm.hasLatLng() && form.find("#pass-from-inp").val() !== "") {
                        Form.createTaskForm.customSubmit();
                    } else {
                        Modal.messageBoxModal.setTitle("คำเตือน");
                        Modal.messageBoxModal.setMessage("กรุณาปักหมุดและระบุจุดรับให้เรียบร้อย");
                        Modal.messageBoxModal.open();
                        Form.createTaskForm.form.find("#pass-from-inp").focus();
                        return;
                    }
                } else {
                    Form.createTaskForm.customSubmit();
                }
                return;
            });
        },
        hasLatLng: function() {
            var form = Form.createTaskForm.form;
            var lat = form.find("#pass-from-inp").attr("data-lat");
            var lng = form.find("#pass-from-inp").attr("data-lng");

            return (lat || lng);
        },
        saveToLocalStorage: function(data) {

            // Current Address
            var hasStartPOI = false;
            var startPOIFoundIndex = -1;
            var passStartPOIArray = LocalStorageManager.getAsArray("pass_poi_start");
            passStartPOIArray.forEach(function(element, index) {
                var passStartPOIObj = JSON.parse(element);
                if(passStartPOIObj.value === data.curaddr) {
                    hasStartPOI = true;
                    startPOIFoundIndex = index;
                    return;
                }
            });

            if(hasStartPOI) {
                passStartPOIArray.splice(startPOIFoundIndex, 1);
                LocalStorageManager.new("pass_poi_start", passStartPOIArray.join(",,"));
            }
            LocalStorageManager.add("pass_poi_start", JSON.stringify({
                value: data.curaddr,
                data: $.extend({}, data, { curloc: [ data.curlng, data.curlat ] })
            }));

            // Limit 5 history.
            if(LocalStorageManager.getAsArray("pass_poi_start").length > 5 ) {
                passStartPOIArray = LocalStorageManager.getAsArray("pass_poi_start");
                passStartPOIArray.splice(0, 1);
                LocalStorageManager.new("pass_poi_start", passStartPOIArray.join(",,"));
            }

            // Phone
            var hasPhone = false;
            var phoneFoundIndex = -1;
            var passPhoneArray = LocalStorageManager.getAsArray("pass_phone");
            passPhoneArray.forEach(function(element, index) {
                var pass_phoneObj = JSON.parse(element);
                if (pass_phoneObj.value === data.phone) { 
                    hasPhone = true;
                    phoneFoundIndex = index;
                    return; 
                }
            });

            if(hasPhone) {
                passPhoneArray.splice(phoneFoundIndex, 1);
                LocalStorageManager.new("pass_phone", passPhoneArray.join(",,"));
            }
            LocalStorageManager.add("pass_phone", JSON.stringify({
                value: data.phone,
                data: {
                    phone: data.phone,
                    date: new Date().getTime()
                }
            }));

            // Limit 20 history.
            if(LocalStorageManager.getAsArray("pass_phone").length > 20 ) {
                passPhoneArray = LocalStorageManager.getAsArray("pass_phone");
                passPhoneArray.splice(0, 1);
                LocalStorageManager.new("pass_phone", passPhoneArray.join(",,"));
            }
        },
        loadPassengerStartPOI: function() {
            if(!localStorage.pass_poi_start) { return [] }
            var data = localStorage.pass_poi_start.split(",,");
            var dataArray = [];
            data.forEach(function(item, index) {
                dataArray.push(JSON.parse(item));
            });

            return dataArray;
        },
        loadPassengerPhone: function () {
            if (!localStorage.pass_phone) { return [] }
            var data = localStorage.pass_phone.split(",,");
            var dataArray = [];
            data.forEach(function (item, index) {
                dataArray.push(JSON.parse(item));
            });

            return dataArray;
        },
        customSubmit: function () {

            var form = Form.createTaskForm.form;
            var data = {};
            data.phone = form.find("#pass-phone-inp").val();
            data.curaddr = form.find("#pass-from-inp").val();
            data.destination = form.find("#pass-dest-inp").val();
            data.detail = form.find("#pass-desc-txt").val();
            data.amount = form.find("#taxiAmount").val();
            data.ccstation = form.find("#ccstation").val();
            data.isContractJob = form.find("[name='isContractJob']:checked").val();
            data.provincearea = {
                code: form.find("#customer-province-list option:selected").val(),
                nameTH: form.find("#customer-province-list option:selected").html(),
                LatLng: form.find("#customer-province-list option:selected").attr("data-loc").split(",").map(Number)
            };
            data.username = USER_DATA.username;
            data.cgroup = USER_DATA.group;

            data.jobtype = form.find("#jobType").is(':checked') ? "ADVANCE" : "QUEUE";
            if (data.jobtype == "ADVANCE") {
                data.createdjob = new Date(DateTimePicker.picker.data("DateTimePicker").date()).getTime();
            }

            data.curlat = parseFloat(form.find("#pass-from-inp").attr("data-lat"));
            data.curlng = parseFloat(form.find("#pass-from-inp").attr("data-lng"));

            if(isNaN(data.curlat)) {
                data.curlat = 0.0;
            }
            if(isNaN(data.curlng)) {
                data.curlng = 0.0;
            }

            if (Socket.hasLostConnect) {

                var timestamp = new Date().getTime();

                data._id = timestamp;
                data.createdjob = timestamp;
                return;
            }

            Form.createTaskForm.saveToLocalStorage(data);
            form.find("#pass-from-inp").data(data);

            Http.post(CONSTRAINTS.serviceURI.addJobList, {
                data: data,
                onSuccess: function (response) {
                    if (response.status) {
                        Form.createTaskForm.savePOI(data);
                        Notification.defaultSuccess();
                        Form.createTaskForm.clearFormData();
                    } else {
                        Notification.defaultError();
                    }

                    Form.createTaskForm.form.find("#pass-phone-inp").focus();
                    DateTimePicker.setMinDate();
                }
            });
            return;
        },
        onSubmit: function () {
            Form.createTaskForm.form.validate({
                rules: {
                    phone: {
                        required: true,
                        number: true
                    },
                    taxiAmount: { required: true }
                },
                submitHandler: function (form, event) {

                    event.preventDefault();
                    return;
                }
            });
        },
        clearFormData: function () {
            var form = Form.createTaskForm.form;
            form.find("#pass-phone-inp").val("");
            form.find("#pass-from-inp").val("");
            form.find("#pass-dest-inp").val("");
            form.find("#pass-desc-txt").val("");
            form.find("#taxiAmount").val("1");
            form.find("#pass-from-inp").removeAttr("data-lat");
            form.find("#pass-from-inp").removeAttr("data-lng");
            form.find("#pickUpPOIBtn").removeClass("valid");
            Modal.pickUpPOIModal.clearData();
        },
        autocompleteInit: function () {

            var optionPassengerPhone = {
                lookup: [],
                minChars: 0,
                onSearchStart: function (query) {
                    var input = $(this),
                        instance = input.data('autocomplete');

                    instance.options.lookup = Form.createTaskForm.loadPassengerPhone();
                },

                onSelect: function (suggestion) {
                    var validator = Form.createTaskForm.form.validate();
                    validator.form();
                    
                    POIRecommended.updateLookup(suggestion.value);
                }
            };

            var optionPassengerStartPOILookup = {
                lookup: [],
                minChars: 0,
                autoSelectFirst: false,
                onSelect: function (suggestion) {

                    this.value = suggestion.value;

                    if(!suggestion.data.curloc) {
                        return;
                    }

                    var lng = parseFloat( suggestion.data.curloc[0] );
                    var lat = parseFloat( suggestion.data.curloc[1] );
                    
                    if(lat > 0 && lng > 0) {
                        $(this).attr("data-lat", suggestion.data.curloc[1]);
                        $(this).attr("data-lng", suggestion.data.curloc[0]);
                        Modal.createTaskModal.modal.find("#pickUpPOIBtn").addClass("valid");
                    } else {
                        $(this).removeAttr("data-lat");
                        $(this).removeAttr("data-lng");
                        Modal.createTaskModal.modal.find("#pickUpPOIBtn").removeClass("valid");
                    }

                    if(Modal.createTaskModal.modal.find("#pass-phone-inp").val() && suggestion.type === "USER") {
                        Modal.createTaskModal.modal.find("#pass-desc-txt").val(suggestion.data.detail || "");
                        Modal.createTaskModal.modal.find("#pass-dest-inp").val(suggestion.data.destination || "");
                    }
                },

                onSearchComplete: function (query, suggestions) {
                    if(!query) {
                        $(this).removeAttr("data-lat");
                        $(this).removeAttr("data-lng");
                        Modal.createTaskModal.modal.find("#pickUpPOIBtn").removeClass("valid");
                    }
                },

                beforeRender: function (container, data) {
                    data.forEach(function (item, index) {

                        var element = $(container).find(".autocomplete-suggestion")[index];
                        switch (item.type) {
                            case "USER":
                                $(element).addClass("user").prepend('<i class="material-icons">&#xE8A6;</i>');
                            break;
                            case "LOCATION":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("location").prepend('<i class="material-icons">&#xE55F;</i>');
                            break;
                            case "TEMP":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("temp").prepend('<i class="material-icons">&#xE889;</i>');
                            break;
                        }
                    });
                }
            };

            var optionPassengerEndPOILookup = {
                lookup: [],
                minChars: 0,
                autoSelectFirst: false,
                onSelect: function (suggestion) {
                    this.value = suggestion.value;
                },

                beforeRender: function (container, data) {
                    data.forEach(function (item, index) {

                        var element = $(container).find(".autocomplete-suggestion")[index];
                        switch (item.type) {
                            case "USER":
                                $(element).addClass("user").prepend('<i class="material-icons">&#xE8A6;</i>');
                            break;
                            case "LOCATION":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("location").prepend('<i class="material-icons">&#xE55F;</i>');
                            break;
                            case "TEMP":
                                var element = $(container).find(".autocomplete-suggestion")[index];
                                $(element).addClass("temp").prepend('<i class="material-icons">&#xE889;</i>');
                            break;
                        }
                    });
                }
            };

            Form.createTaskForm.form.find('#pass-phone-inp').autocomplete(optionPassengerPhone);
            Form.createTaskForm.form.find('#pass-from-inp').autocomplete(optionPassengerStartPOILookup);
            Form.createTaskForm.form.find('#pass-dest-inp').autocomplete(optionPassengerEndPOILookup);
        },
        checkDuplicatePhoneNo: function() {

            var value = this.value;

            if(value.length < 8) {
                Form.createTaskForm.duplicatePhoneNoPopover.popover("hide");
            }

            else {

                var getDiffTime = function(past) {

                    // past than 60 minutes
                    if(moment(past).diff(moment(), 'minutes') <= -60) {
                        return moment(past).diff(moment(), 'hours').toString().replace("-", "") + " ชม. ที่แล้ว";
                    }
                    else if(moment(past).diff(moment(), 'minutes') === 0) {
                        return "นาทีที่แล้ว";
                    }
                    else {
                        return moment(past).diff(moment(), 'minutes').toString().replace("-", "") + " นาที. ที่แล้ว";
                    }
                }

                var resultInQueueTask = DataTable.queueTaskTable.datatable.data().filter(function(data) {
                    return data[5] === value;
                });

                var resultInPendingTask = DataTable.pendingTaskTable.datatable.data().filter(function(data) {
                    return data[6] === value;
                });

                var resultInAssignedTask = DataTable.assignedTaskTable.datatable.data().filter(function(data) {
                    return data[6] === value;
                });

                if(resultInQueueTask.length > 0) {

                    $("#pass-phone-inp").data('bs.popover').options.content = "เบอร์นี้เคยเรียกรถแล้ว<br>เมื่อ " + getDiffTime(resultInQueueTask[0][2]);
                    Form.createTaskForm.duplicatePhoneNoPopover.popover("show");
                }

                else if(resultInPendingTask.length > 0) {

                    $("#pass-phone-inp").data('bs.popover').options.content = "เบอร์นี้เคยเรียกรถแล้ว<br>เมื่อ " + getDiffTime(resultInPendingTask[0][2]);
                    Form.createTaskForm.duplicatePhoneNoPopover.popover("show");
                }

                else if(resultInAssignedTask.length > 0) {

                    $("#pass-phone-inp").data('bs.popover').options.content = "เบอร์นี้เคยเรียกรถแล้ว<br>เมื่อ " + getDiffTime(resultInAssignedTask[0][1]);
                    Form.createTaskForm.duplicatePhoneNoPopover.popover("show");
                }
                else {
                    Form.createTaskForm.duplicatePhoneNoPopover.popover("hide");
                }
            }
        },
        setUpDuplicatePhoneNoPopover: function() {
            var popover = $("#pass-phone-inp").popover({
                trigger: "manual",
                placement: "top",
                animation: false,
                html: true
            });

            Form.createTaskForm.duplicatePhoneNoPopover = popover;
        },
        savePOI: function(taskData) {

            if(!taskData) {
                return;
            }

            var formatted_address = "";
            if(Modal.pickUpPOIModal.currentPickupData) {
                formatted_address = Modal.pickUpPOIModal.currentPickupData.formatted_address;
            }

            var POIData = {
                "type": "start",
                "name": taskData.curaddr,
                "formatter": formatted_address,
                "destination": taskData.destination,
                "detail": taskData.detail,
                "curloc": [
                    taskData.curlng,
                    taskData.curlat
                ]
            }
            $.post(CONSTRAINTS.serviceURI.addNewPOI + taskData.phone, POIData, function(response) {
            });
        }
    },
    assignTaskForm: {
        form: $("#assignTaskForm"),
        initial: function () {
            Form.assignTaskForm.onSubmit();
        },
        onSubmit: function () {
            Form.assignTaskForm.form.validate({
                rules: { },
                submitHandler: function (form, event) {

                    event.preventDefault();
                    var form = Form.assignTaskForm.form;

                    var carplateInput = Form.searchTaxiBeforeAssignForm.form.find('#car-plate-inp');

                    var passenger_id = form.find('#pass_id').val();
                    var carplate = carplateInput.val();

                    if (carplate === "") {
                        carplateInput.focus();
                        return;
                    }

                    if (carplate != "" && passenger_id != undefined) {

                        Form.assignTaskForm.form.find('button').attr('disabled', true);
                        Form.searchTaxiBeforeAssignForm.form.find('input').attr('disabled', true);

                        var sendJob = function (response) {
                            if (!response.status) {
                                var drv_carplate = ((this.data.prefixcarplate || " ") + this.data.carplate).trim();
                                Passenger.sendToNoneRegisterDriver(this.data.psg_id, this.data.carplate, drv_carplate, this.data.prefixcarplate);
                            }
                            else if (response.status && response.data.status === "ON") {
                                Passenger.sendtoRegisterDriver(this.data.psg_id, response.data._id);
                            }
                            else if (response.status && response.data.status === "WAIT") {
                                Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Modal.messageBoxModal.setTitle("คำเตือน!");
                                Modal.messageBoxModal.setMessage("ผู้ใช้งานดังกล่าว กำลังรอตอบกลับจากผู้โดยสาร<br>ไม่สามารถจ่ายงานให้ได้");
                                Modal.messageBoxModal.open();
                            }
                            else if (response.status && response.data.status === "DPENDING") {
                                Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Modal.messageBoxModal.setTitle("คำเตือน!");
                                Modal.messageBoxModal.setMessage("ผู้ใช้งานดังกล่าว กำลังรับงานอื่นจาก Callcenter<br>ไม่สามารถจ่ายงานให้ได้");
                                Modal.messageBoxModal.open();
                            }
                            else if (response.status && response.data.status === "BUSY") {
                                Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Modal.messageBoxModal.setTitle("คำเตือน!");
                                Modal.messageBoxModal.setMessage("ผู้ใช้งานดังกล่าว กำลังไปรับผู้โดยสาร<br>ไม่สามารถจ่ายงานให้ได้");
                                Modal.messageBoxModal.open();
                            }
                            else if (response.status && response.data.status === "PICK") {
                                Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Modal.messageBoxModal.setTitle("คำเตือน!");
                                Modal.messageBoxModal.setMessage("ผู้ใช้งานดังกล่าว กำลังไปส่งผู้โดยสาร<br>ไม่สามารถจ่ายงานให้ได้");
                                Modal.messageBoxModal.open();
                            }
                            else if (response.status && response.data.status === "BROADCAST") {
                                Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Modal.messageBoxModal.setTitle("คำเตือน!");
                                Modal.messageBoxModal.setMessage("ผู้ใช้งานดังกล่าว รถเสีย<br>ไม่สามารถจ่ายงานให้ได้");
                                Modal.messageBoxModal.open();
                            }
                            else if (response.status && response.data.status === "OFF") {
                                var drv_carplate = (this.data.prefixcarplate || '') + " " + this.data.carplate;
                                Passenger.sendToNoneRegisterDriver(this.data.psg_id, this.data.carplate, drv_carplate, this.data.prefixcarplate);
                            }
                            else if (response.status && response.data.status === "APPROVED") {
                                var drv_carplate = ((this.data.prefixcarplate || " ") + this.data.carplate).trim();
                                Passenger.sendToNoneRegisterDriver(this.data.psg_id, this.data.carplate, drv_carplate, this.data.prefixcarplate);
                            }
                            else if (response.status && response.data.status === "ASSIGNED") {
                                Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Modal.messageBoxModal.setTitle("คำเตือน!");
                                Modal.messageBoxModal.setMessage("ผู้ใช้งานดังกล่าว กำลังรับงานอื่นจาก Callcenter<br>ไม่สามารถจ่ายงานให้ได้");
                                Modal.messageBoxModal.open();
                            }
                            else {
                                var input = Modal.assignTaskModal.modal.find("#car-plate-inp").prop("disabled", false)[0];
                                input.focus();
                                input.setSelectionRange(0, 8);
                                Modal.assignTaskModal.modal.find("[type=submit]").prop("disabled", false);
                                Notification.defaultError();
                            }
                        };

                        var data = {
                            "carplate": carplate,
                            "psg_id": passenger_id
                        };

                        if(!$.isEmptyObject(Taxi.active)) {
                            data.drv_id = Taxi.active.options.taxi_info._id
                        }

                        Taxi.checkStatus(data, sendJob);
                    }
                    return false;
                }
            });
        },
        autocompleteInit: function () {
            var options = {
                type: 'POST',
                paramName: 'keyword',
                serviceUrl: CONSTRAINTS.serviceURI.searchNameCar,
                transformResult: function (response, originalQuery) {

                    var response = JSON.parse(response);
                    if (response.status) {
                        return {
                            suggestions: $.map(response.data, function (dataItem) {
                                return { value: dataItem.carplate, data: dataItem, ID: dataItem._id };
                            })
                        };
                    } else {
                        return {
                            suggestions: {}
                        };
                    }
                },
                onSearchStart: function (query) {
                    Form.assignTaskForm.form.find('#carNumber').removeAttr('data-driver-ref');
                },
                onSelect: function (suggestion) {
                    Form.assignTaskForm.form.find('#carNumber').attr('data-driver-ref', suggestion.data._id);
                }
            };

            Form.assignTaskForm.form.find('#carNumber').autocomplete(options);
        },
        clearFormData: function () {
            Modal.assignTaskModal.modal.find('p.value').text("");
            Form.searchTaxiBeforeAssignForm.form.find('input[type=text]').val("");
        }
    },
    broadcastTaskForm: {
        form: $("#broadcastTaskForm"),
        initial: function () {
            Form.broadcastTaskForm.onSubmit();
        },
        onSubmit: function () {
            Form.broadcastTaskForm.form.validate({
                rules: { },
                submitHandler: function (form, event) {

                    event.preventDefault();

                    var form = Form.broadcastTaskForm.form;
                    var passenger_id = form.find('#pass_id').val();

                    var data = {
                        "psg_id": passenger_id,
                        "curlat": Modal.assignTaskModal.passengerMarker.getLatLng().lat,
                        "curlng": Modal.assignTaskModal.passengerMarker.getLatLng().lng,
                        "searchpsgradian": Modal.assignTaskModal.passengerCircle.getRadius(),
                        "searchpsgamount": GARAGE_DATA.searchpsgamount
                    };

                    $.post("/service/ubeam/doccbroadcast", data, function(response) {
                        if(response.status) {
                            Form.assignTaskForm.clearFormData();
                            Modal.assignTaskModal.close();
                        }
                    });

                    return false;
                }
            });
        }
    },
    searchTaxiBeforeAssignForm: {
        form: $("#searchTaxiBeforeAssignForm"),
        initial: function () {
            Form.searchTaxiBeforeAssignForm.onSubmit();
            Form.searchTaxiBeforeAssignForm.form.find("#car-plate-inp").off("keyup").on("keyup", this.onkeyup);

            this.setUpPopover();
        },
        onSubmit: function () {

            Form.searchTaxiBeforeAssignForm.form.validate({
                rules: {
                    carplate: { required: true }
                },
                submitHandler: function (form, event) {

                    event.preventDefault();

                    Form.assignTaskForm.form.submit();
                    return;
                }
            });
        },
        onkeyup: function () {

            var closePopup = function(triggerPopover) {

                var marker = Form.searchTaxiBeforeAssignForm.activeMarker;
                if( marker && marker instanceof L.Marker ) { marker.closePopup(); }

                Form.searchTaxiBeforeAssignForm.activeMarker = null;

                triggerPopover ? $("#car-plate-inp").popover("show") : $("#car-plate-inp").popover("hide");
            }

            var openPopup = function() {

                currentTaxi.openPopup();
                Form.searchTaxiBeforeAssignForm.activeMarker = currentTaxi;
                Map.setView([currentTaxi.options.taxi_info.lat, currentTaxi.options.taxi_info.lng], 17, { animate: false });

                $("#car-plate-inp").popover("hide");
            }

            var lastChar = this.value.substr(this.value.length -1, 1);

            if (lastChar !== "+" && this.value.length >= 1) {

                var carplate = this.value;
                var currentTaxi = null;

                if(!Taxi.taxiList) {
                    return;
                }

                var matchTaxi = Taxi.taxiList.filter(function(taxi) {
                    return taxi.carplate === carplate;
                });


                if(matchTaxi.length > 0) {

                    var matchStatus = matchTaxi.filter(function(taxi) {
                        return taxi.status === "ON";
                    });

                    if(matchStatus.length > 0) {
                        Taxi.layer.eachLayer(function (taxi) {
                            if (taxi.options.taxi_info._id === matchStatus[0]._id) {
                                currentTaxi = taxi;
                                return;
                            }
                        });
                        currentTaxi && openPopup();
                    }
                    else {
                        var popover = $("#car-plate-inp").data('bs.popover');
                        popover.options.content = '<i class="material-icons">info_outline</i> <span>ทะเบียนนี้ <u>ไม่ได้เปิด</u>แอพฯ</span>';

                        closePopup(true);
                    }
                }
                else {
                    var popover = $("#car-plate-inp").data('bs.popover');
                    popover.options.content = '<i class="material-icons">info_outline</i> <span>ทะเบียนนี้ <u>ไม่ได้ใช้</u>แอพฯ</span>';

                    closePopup(true);
                }
            }
            else if (lastChar === "+" && this.value.length > 2) {
                $("#car-plate-inp").popover("hide");
            }
            else {
                closePopup(false);
            }
        },
        setUpPopover: function(messagesType) {
            var popover = $("#car-plate-inp").popover({
                trigger: "manual",
                placement: "top",
                animation: false,
                html: true
            });
        },
        activeMarker: null
    },
    assignedTaskEditableForm: {
        form: $("#assignedTaskEditableForm"),
        initial: function () {
            Form.assignedTaskEditableForm.onSubmit();
        },
        onSubmit: function () {
            Form.assignedTaskEditableForm.form.validate({
                rules: {
                    phone: { required: true, number: true, maxlength: 10 },
                    curaddr: { required: true, maxlength: 64 },
                    destination: { required: true, maxlength: 64 },
                },
                messages: {
                    phone: {
                        required: "กรุณาระบบข้อมูลนี้",
                        number: "ระบุได้เป็นตัวเลขเท่านั้น",
                        maxlength: "กรอกได้ไม่เกิน 10 ตัว"
                    },
                    curaddr: "กรุณาระบบข้อมูลนี้",
                    destination: "กรุณาระบบข้อมูลนี้",
                },
                submitHandler: function (form, event) {

                    event.preventDefault();
                    var form = Form.assignedTaskEditableForm.form;

                    var data = {
                        psg_id: form.find("#psg_id").val(),
                        phone: form.find("#phone").val(),
                        curaddr: form.find("#curaddr").val(),
                        destination: form.find("#destination").val(),
                        detail: form.find("#detail").val(),
                        cccomment: form.find("#cccomment").val()
                    };

                    Http.post(CONSTRAINTS.serviceURI.editJobList, {
                        loader: false,
                        data: data,
                        onSuccess: function (response) {
                            if (response.status) {
                                Notification.remove();
                                Notification.defaultSuccess();
                                Modal.assignedTaskEditableModal.close();
                                Modal.assignedTaskEditableModal.clearFormData();
                            } else {
                                Notification.remove();
                                Notification.defaultError();
                            }
                        }
                    });
                }
            });
        }
    },
    announceForm: {
        form: $("#announceForm"),
        initial: function () {
            Form.announceForm.onSubmit();
        },
        onSubmit: function () {
            Form.announceForm.form.validate({
                rules: {
                    topic: { required: true },
                    detail: { required: true }
                },
                submitHandler: function (form, event) {

                    event.preventDefault();

                    var data = {
                        username: USER_DATA.username,
                        topic: $(form).find('[name="topic"]').val(),
                        detail: $(form).find('[name="detail"]').val()
                    }

                    Http.post(CONSTRAINTS.serviceURI.addAnnouncement, {
                        data: data,
                        onSuccess: function (response) {
                            if (response.status) {
                                Notification.defaultSuccess();
                                $(form).find('[name="topic"]').val("");
                                $(form).find('[name="detail"]').val("");

                                Modal.announcementModal.modal.find(".modal-body").empty();
                                Announcement.getAll()
                            }
                            else {
                                Notification.defaultError();
                            }
                        }
                    });

                }
            });
        }
    },
    announceEditForm: {
        form: $("#announceEditForm"),
        initial: function () {
            Form.announceEditForm.onSubmit();
        },
        onSubmit: function () {
            Form.announceEditForm.form.validate({
                rules: {
                    topic: { required: true },
                    detail: { required: true }
                },
                submitHandler: function (form, event) {

                    event.preventDefault();

                    var data = {
                        user_id: USER_DATA._id,
                        ann_id: $(form).find('[name="ann_id"]').val(),
                        topic: $(form).find('[name="topic"]').val(),
                        detail: $(form).find('[name="detail"]').val()
                    }

                    Http.post(CONSTRAINTS.serviceURI.editAnnouncement, {
                        data: data,
                        onSuccess: function (response) {
                            if (response.status) {
                                Notification.defaultSuccess();
                                $(form).find('[name="topic"]').val("");
                                $(form).find('[name="detail"]').val("");

                                Modal.announcementEditModal.close();
                                Announcement.getAll()
                            }
                            else {
                                Notification.defaultError();
                            }
                        }
                    });

                }
            });
        }
    },
    searchFinishedTaskForm: {
        form: $("#searchFinishedTaskForm"),
        initial: function () {
            Form.searchFinishedTaskForm.onSubmit();
        },
        onSubmit: function () {
            Form.searchFinishedTaskForm.form.validate({
                rules: {
                    keyword: { required: true, maxlength: 32, }
                },
                errorPlacement: function (error, element) { },
                submitHandler: function (form, event) {

                    event.preventDefault();
                    var daterange = Modal.searchTaskWithResultListModal.modal.find('.daterange').data('daterangepicker');

                    Http.post(CONSTRAINTS.serviceURI.searchFinishList, {
                        data: {
                            keyword: $(form).find('[name="keyword"]').val(),
                            startTime: new Date(daterange.startDate).getTime(),
                            endTime: new Date(daterange.endDate).getTime(),
                        },
                        onSuccess: function (response) {
                            if (response.status) {
                                Notification.remove();
                                DataTable.searchTaskResultTable.render(response.data);
                            }
                            else {
                                DataTable.searchTaskResultTable.clear();
                                Notification.show({
                                    time: 5000,
                                    title: 'ไม่มีข้อมูลที่ค้นห้า',
                                    text: 'ไม่พบข้อมูลที่เกี่ยวข้องกับ "' + $(form).find('[name="keyword"]').val() + '"',
                                    $class: "info"
                                });
                            }
                        }
                    });

                }
            });
        }
    },
    assignTaskSearchTaxiForm: {
        form: $("#assignTaskSearchTaxiForm"),
        initial: function () {
            Form.assignTaskSearchTaxiForm.onSubmit();
        },
        onSubmit: function () {
            Form.assignTaskSearchTaxiForm.form.validate({
                rules: {
                    phoneNumber: { required: true, maxlength: 64, }
                },
                errorPlacement: function (error, element) { },
                submitHandler: function (form, event) {

                    event.preventDefault();

                    var carplate = $(form).find('#map-search-inp').val();
                    Taxi.findByCarplate(carplate);
                }
            });
        }
    }
};


var HotKey = {
    initial: function () {
        window.addEventListener('keyup', this.doubleCtrl);
        window.addEventListener('keyup', this.F2);
        window.addEventListener('keyup', this.ESC);
    },
    delta: 500,
    lastKeypressTime: 0,
    doubleCtrl: function (event) {
        // http://stackoverflow.com/a/1223775
        if (event.keyCode == 17 || event.which == 17) // Ctrl Key
        {
            var thisKeypressTime = new Date();
            if (thisKeypressTime - HotKey.lastKeypressTime <= HotKey.delta) {
                var visible = Modal.assignTaskModal.modal.is(":visible");
                visible && Modal.assignTaskModal.toggleFullScreen();
                thisKeypressTime = 0;
            }
            HotKey.lastKeypressTime = thisKeypressTime;
        }
    },
    F2: function (event) {
        if (event.keyCode == 113 || event.which == 113) // F2 key
        {
            if (!$("#createTaskModal").is(":visible")) {
                Modal.createTaskModal.open(event);
            }
        }
    },
    ESC: function (event) {
        if (event.keyCode == 27 || event.which == 27) // ESC key
        {
            if ($("#createTaskModal").is(":visible")) {
                Modal.createTaskModal.modal.modal("hide");
            }
            if ($("#assignTaskModal").is(":visible")) {
                Modal.assignTaskModal.modal.modal("hide");
            }
        }
    }
};


var Http = {
    connection_error_time: 0,
    getJson: function (url, options) {

        if (options.loader !== undefined && !options.loader) { Preloader.show(); }

        $.getJSON(url, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        })
        .fail(function () {
            Notification.defaultError();
        })
        .always(function () {
            Preloader.hide();
        });
    },
    post: function (url, options) {

        if (options.loader || options.loader == undefined) { Preloader.show(); }

        if (options.data == undefined) { options.data = {}; }
        $.post(url, options.data, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        })
        .fail(function () {
            if (Http.connection_error_time < 1) {
                Http.connection_error_time++;
            }
            else if (Http.connection_error_time >= 1) {

                Http.connection_error_time = 0;
                Http.onConnectionLost();
            }
        })
        .always(function () {
            Preloader.hide();
        });
    },
    onConnectionLost: function () {

        if( $('body').find("> .connect_lost_message").size() === 0 ) { 

            var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

            $('body').find("> .connect_lost_message").remove();
            $('body').append(message);
            $('body').find("> .connect_lost_message").fadeIn('fast');
        }

        if (this.intervalId == undefined) {
            this.intervalId = App.intervalManager(true, Http.testConnection, 5000);
        }
    },
    testConnection: function () {

        $.post(CONSTRAINTS.serviceURI.getPassengerDetail, { "psg_id": "" }, function (data) {

            clearInterval(Http.intervalId);

            App.intervalManager(false, Http.intervalId);

            delete Http.intervalId;

            Http.connection_error_time = 0;

            $('body').find("> .connect_lost_message").remove();
        });
    }
};


var iOSwitchery = {
    initial: function () {
        this.create();
        this.eventHandler();
    },
    eventHandler: function () {
        $("#createTaskForm").find('.js-switch').on('change', function (event) {
            this.checked ? $('.datetime-overlay').fadeOut() : $('.datetime-overlay').fadeIn();
        });

        $(".taxi-counter").find(".js-switch").on('change', function (event) {
            var button = $(event.target).closest(".list-group-item");
            if (button.hasClass("HOLD")) {
                Taxi.toggleStatus("HOLD", this.checked);
            }
            else if (button.hasClass("ON")) {
                Taxi.toggleStatus("ON", this.checked);
            }
            else if (button.hasClass("DPENDING")) {
                Taxi.toggleStatus("DPENDING", this.checked);
            }
            else if (button.hasClass("BUSY")) {
                Taxi.toggleStatus("BUSY", this.checked);
            }
            else if (button.hasClass("PICK")) {
                Taxi.toggleStatus("PICK", this.checked);
            }
            else if (button.hasClass("ASSIGNED")) {
                Taxi.toggleStatus("ASSIGNED", this.checked);
            }
            else if (button.hasClass("PARKING")) {
                LandMark.toggleDisplay(this.checked);
            }
        });
    },
    create: function () {

        var html = $("#createTaskForm").find('.js-switch')[0];
        var switchery = new Switchery(html);

        $(".taxi-counter").find(".js-switch").each(function (i, html) {
            var switchery = new Switchery(html, {
                size: 'small'
            });
        });
    }
};


var JQuery = {
    NumberOnlyInit: function () {
        jQuery.fn.ForceNumericOnly = function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.charCode || e.keyCode || 0 || e.which;
                    return (
                        key == 8 ||
                        key == 9 ||
                        key == 13 ||
                        key == 27 ||
                        key == 46 ||
                        key == 190 ||
                        (key >= 35 && key <= 40) ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
                });
            });
        };
    }
};


var Loader = {
    initial: function () {
        Preloader.setOption({
            timeout: 700,
            className: 'la-ball-clip-rotate la-dark',
            color: '#B21616'
        });
    }
};


var Modal = {
    initial: function () {
        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function () {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });
    },
    createTaskModal: {
        modal: $('#createTaskModal'),
        initial: function () {
            
            // Jquery draggable
            $(Modal.createTaskModal.modal).find('.modal-dialog').draggable({
                handle: ".modal-body"
            });

            Modal.createTaskModal.modal.on('shown.bs.modal', function (e) {
                document.getElementById("pass-phone-inp").focus();
                $(this).find("#customer-province-list option[value=" + USER_DATA.provinceEN[0] + "]").prop("selected", true);
            });

            Modal.createTaskModal.modal.on('hidden.bs.modal', function (e) {
                Form.createTaskForm.form.find("label.error").hide();
                Form.createTaskForm.duplicatePhoneNoPopover.popover("hide");
            });
        },
        open: function (event) {
            event !== undefined && event.stopPropagation();
            Modal.createTaskModal.modal.modal("show");
        },
        landMarkSuggestion: function (input) {
            Http.post(CONSTRAINTS.serviceURI.getPOIandParking, {
                loader: false,
                onSuccess: function (response) {
                    if (response.status) {

                        var html = '<div class="land-mark-suggestion">';
                        html += '<div class="list-group">';

                        $(response.data).each(function (index, location) {
                            html += '<a href="#" tabindex="-1" class="list-group-item col-md-6">' + location.name + '</a>';
                        });

                        html += '</div>'
                        html += '</div>';

                        var landMark = $(input).popover({
                            html: true,
                            placement: "right",
                            title: '<i class="material-icons">star_border</i> <span>สถานที่แนะนำ</span>',
                            content: html,
                        }).on("shows.bs.popover", function (event) {
                            if (event.keyCode == 27) {
                                event.preventDefault();
                                $(this).popover('hide');
                                $(this).popover('destroy');
                            }
                        });

                        landMark.popover("show");
                    }
                }
            });
        }
    },
    assignTaskModal: {
        modal: $('#assignTaskModal'),
        passengerMarker: null,
        initial: function () {

            Modal.assignTaskModal.modal.on('show.bs.modal', function (e) {
                var mapWrapper = $(this).find(".map-wrap");
                jQuery("#map").detach().appendTo(mapWrapper).removeClass('hide');
                Taxi.clear();
            });

            Modal.assignTaskModal.modal.on('shown.bs.modal', function (e) {

                var boxHeight = Modal.assignTaskModal.modal.find(".detail-wrap").height();
                Modal.assignTaskModal.modal.find(".detail-wrap").scrollTop(boxHeight);
                var carplateInp = document.getElementById("car-plate-inp");
                if( carplateInp !== null) {
                    document.getElementById("car-plate-inp").focus();
                }

                setTimeout(function () {

                    Map.invalidateSize();

                    setTimeout(function() {

                        var psgData = Modal.assignTaskModal.modal.data().passengerData;

                        if( psgData.curloc.length > 0 
                            && psgData.curloc[0] !== null 
                            && psgData.curloc[1] !== null 
                            && (psgData.curloc[0] > 0 || psgData.curloc[1] > 0 )) {
                                var LatLng = [psgData.curloc[1], psgData.curloc[0]];
                                Modal.assignTaskModal.addPassengerMarker(psgData, LatLng);
                                Map.setView(LatLng, 14, { animate: false });
                        }
                        else if (psgData.provincearea !== undefined) {
                            Map.setView(psgData.provincearea.LatLng.map(Number), 15, { animate: false });
                        }
                        else {
                            App.setDefaultPosition();
                        }

                        Taxi.findInCenterMap();

                        Map.off("dragend", Taxi.findInCenterMap);
                        Map.on("dragend", Taxi.findInCenterMap);

                        LandMark.getAll();

                    }, 200);
                }, 200);
            });

            Modal.assignTaskModal.modal.on('hidden.bs.modal', function (e) {
                
                Taxi.clear();
                
                Map.off("dragend", Taxi.findInCenterMap);
                Form.assignTaskForm.form.find('.text').val("");
                Form.assignTaskForm.form.find('button').attr('disabled', false);
                Form.assignTaskForm.form.find('button[type=submit]').show();

                Form.searchTaxiBeforeAssignForm.form.find('input').val("");
                Form.searchTaxiBeforeAssignForm.form.find("label.error").hide();
                Form.searchTaxiBeforeAssignForm.form.find('input').attr('disabled', false);

                $("#car-plate-inp").popover("hide");
                if(Modal.assignTaskModal.passengerMarker) {
                    Map.removeLayer(Modal.assignTaskModal.passengerMarker);
                    Modal.assignTaskModal.passengerMarker = null;
                }
                if(Modal.assignTaskModal.passengerCircle) {
                    Map.removeLayer(Modal.assignTaskModal.passengerCircle);
                    Modal.assignTaskModal.passengerCircle = null;
                }

                var data = { 'id': $(this).data("passengerData")._id, 'table': $(this).data("table") };
                LOCAL_STATE_CONTROL.remove(data);
                Socket.emit('unlock line', data);
                $.removeData(this, ["passengerData", "table"]);
                
                Modal.assignTaskModal.removeLabelLockModal();
            });
        },
        open: function (event) {
            Modal.assignTaskModal.modal.modal("show");
        },
        close: function (event) {
            Modal.assignTaskModal.modal.modal("hide");
        },
        toggleFullScreen: function () {
            Modal.assignTaskModal.modal.find(".modal-dialog").toggleClass("full-screen");

            if (Modal.assignTaskModal.modal.find(".modal-dialog").hasClass("full-screen")) {
                Modal.assignTaskModal.modal.find(".map-wrap").parent().removeClass("col-md-8 col-lg-8");
                Modal.assignTaskModal.modal.find(".map-wrap").parent().addClass("col-md-9 col-lg-9");

                Modal.assignTaskModal.modal.find(".detail-wrap").parent().removeClass("col-md-4 col-lg-4");
                Modal.assignTaskModal.modal.find(".detail-wrap").parent().addClass("col-md-3 col-lg-3");
            }
            else {
                Modal.assignTaskModal.modal.find(".map-wrap").parent().removeClass("col-md-9 col-lg-9");
                Modal.assignTaskModal.modal.find(".map-wrap").parent().addClass("col-md-8 col-lg-8");

                Modal.assignTaskModal.modal.find(".detail-wrap").parent().removeClass("col-md-3 col-lg-3");
                Modal.assignTaskModal.modal.find(".detail-wrap").parent().addClass("col-md-4 col-lg-4");

                Modal.assignTaskModal.modal.find(".detail-wrap")[0].scrollTop = Modal.assignTaskModal.modal.find(".detail-wrap")[0].scrollHeight;
            }

            setTimeout(function () {
                Map.invalidateSize();
            }, 100);
        },
        addPassengerMarker: function(passengerData, LatLng) {
            if (LatLng[0] !== null && LatLng[0] > 0) {
                Modal.assignTaskModal.passengerMarker = L.marker(LatLng)
                    .bindPopup('จุดรับผู้โดยสาร "' + passengerData.curaddr + '"').addTo(Map);

                if (USER_DATA.broadcast === "true") {

                    function findDriverInRadian(data) {

                        Modal.assignTaskModal.passengerCircle.setRadius(data.from);
                        var latLng = Modal.assignTaskModal.passengerMarker.getLatLng();
                        Modal.assignTaskModal.modal.find(".taxi-list-table tbody").html("");

                        Taxi.findInCenterMap();

                        $.post('/service/ubeam/FindDrvinRadian', {
                            "curlat":latLng.lat,
                            "curlng":latLng.lng,
                            "searchpsgradian": data.from,
                            "searchpsgamount": GARAGE_DATA.searchpsgamount
                        }, function(response) {

                            if(response.status && response.data.length > 0) {
                                var rows = "";
                                for(i = 0; i < response.data.length; i++) {

                                    var taxi = response.data[i];
                                    rows += "<tr>";
                                    rows += "<td>" + (taxi.prefixcarplate || '') + " " + (taxi.carplate || '') + "</td>";
                                    rows += "<td>" + taxi.dist_km + "</td>";
                                    rows += "</tr>";
                                }
                                Modal.assignTaskModal.modal.find(".taxi-list-table tbody").html(rows);
                                Modal.assignTaskModal.modal.find("#amountBroadcastDetail .count").text(response.data.length);
                            } else {
                                Modal.assignTaskModal.modal.find("#amountBroadcastDetail .count").text("0");
                            }
                            
                            var boxHeight = Modal.assignTaskModal.modal.find(".detail-wrap").height();
                            Modal.assignTaskModal.modal.find(".detail-wrap").scrollTop(boxHeight);
                        });
                    }

                    Modal.assignTaskModal.modal.find(".taxi-list-table tbody").html("");
                    Modal.assignTaskModal.modal.find("#amountBroadcastDetail .count").text("0");

                    if($("#searchpsgradian-slider").data().from) {
                        Modal.assignTaskModal.passengerCircle = L.circle([passengerData.curloc[1], passengerData.curloc[0]], { radius: $("#searchpsgradian-slider").data().from }).addTo(Map);
                        findDriverInRadian({ from: $("#searchpsgradian-slider").data().from });
                    } else {
                        Modal.assignTaskModal.passengerCircle = L.circle([passengerData.curloc[1], passengerData.curloc[0]], { radius: GARAGE_DATA.searchpsgradian }).addTo(Map);
                        findDriverInRadian({ from: GARAGE_DATA.searchpsgradian });
                    }

                    $("#searchpsgradian-slider").ionRangeSlider({
                        type: "single",
                        min: 1000,
                        max: parseInt(GARAGE_DATA.searchpsgradian),
                        from: parseInt(GARAGE_DATA.searchpsgradian),
                        step: 100,
                        postfix: " เมตร",
                        onStart: function (data) {
                            findDriverInRadian(data);
                        },
                        onChange: function (data) {
                            Modal.assignTaskModal.passengerCircle.setRadius(data.from);
                        },
                        onFinish: function (data) {
                            findDriverInRadian(data);
                        },
                        onUpdate: function (data) {
                        }
                    });
                }
            }
        },
        removeLabelLockModal: function() {

            Modal.assignTaskModal.modal.find(".modal-dialog").removeClass("lock-modal");
            Modal.assignTaskModal.modal.find(".wrap-lock").remove();
            Modal.assignTaskModal.modal.find(".overlay-lock").remove();
        },
        showPopupTaskAssigned: function(currentTaskId, driverDetail) {
            var popupTaskId = Modal.assignTaskModal.modal.data().passengerData._id;
            if (popupTaskId === currentTaskId) {

                Modal.assignTaskModal.removeLabelLockModal();

                Modal.assignTaskModal.modal.find(".modal-dialog")
                    .addClass("lock-modal")
                    .prepend('<div class="wrap-lock"><h3>งานนี้ ถูกรับไปแล้ว</h3><p>โดยแท็กซี่ทะเบียน ' 
                        + driverDetail.carplate + ', ชื่อ ' 
                        + driverDetail.fname + ' '  
                        + driverDetail.lname + '</p><br><button class="btn btn-default" data-dismiss="modal">ปิดหน้าต่าง</button></div><div class="overlay-lock"></div>');
                Modal.assignTaskModal.modal.find(".modal-dialog").show();
            }
        }
    },
    assignTaskByLineModal: {
        modal: $("#assignTaskByLineModal"),
        initial: function () {
            Modal.assignTaskByLineModal.modal.on('show.bs.modal', function (event) {
                $(this).find(".modal-footer").show();
                $(this).find("button[type=submit]").on("click", Modal.assignTaskByLineModal.onSubmit);
            });
            Modal.assignTaskByLineModal.modal.on('shown.bs.modal', function (event) {
                $(this).find("button[type='submit']").focus();
            });
            Modal.assignTaskByLineModal.modal.on('hidden.bs.modal', function (event) {
                $.removeData(this, "task");
                $(this).find("p.text").html("");
                $(this).find("button[type=submit]").unbind("click");

                var data = { 'id': $(this).data("passengerData")._id, 'table': $(this).data("table") };
                LOCAL_STATE_CONTROL.remove(data);
                Socket.emit('unlock line', data);
                $.removeData(this, ["passengerData", "table"]);
            });
        },
        open: function (data) {
            Modal.assignTaskByLineModal.modal.modal("show");
        },
        close: function (event) {
            Modal.assignTaskByLineModal.modal.modal("hide");
        },
        onSubmit: function () {
            var data = Modal.assignTaskByLineModal.modal.data("task");
            Taxi.assignByLineApp(data._id, data.drv_carplate);
        }
    },
    assignedTaskModal: {
        modal: $('#assignedTaskModal'),
        layer: null,
        initial: function () {

            Modal.assignedTaskModal.layer = L.layerGroup();

            Map && Map.addLayer(this.layer);

            //Prevent error durning load tile layer.
            Modal.assignedTaskModal.modal.on('shown.bs.modal', function (e) {

                var mapWrapper = $(this).find(".map-wrap");
                jQuery("#map").detach().appendTo(mapWrapper).removeClass('hide');

                $(this).find(".reject-btn").focus();

                setTimeout(function () {
                    Map.invalidateSize();
                }, 100);

                var boxHeight = Modal.assignedTaskModal.modal.find(".detail-wrap").height();
                Modal.assignedTaskModal.modal.find(".detail-wrap").scrollTop(boxHeight);
            });

            Modal.assignedTaskModal.modal.on('hidden.bs.modal', function (e) {
                $(this).find(".text").html("");
                $(this).find("img.driver-img").attr("src", "");
                $(this).find('.ribbon-message').remove();

                $(this).find(".reject-btn").show();
                $(this).find(".reject-btn").prop("disabled", false).attr("data-ref", "");

                $(this).find(".driver-wrap").show();
                $(this).find(".alert-missing-driver").hide();
                Modal.assignedTaskModal.clearMarker();
            });

            Modal.assignedTaskModal.modal.find(".reject-btn").on("click", function (event) {
                $(this).attr("disabled", true);
                var pass_id = $(this).attr("data-ref");
                Passenger.showConfirmRejectModal(pass_id);
            });

            Modal.assignedTaskModal.modal.find(".commentForm").find("button").on("click", function (event) {
                var comment = $(this).closest(".commentForm").find("input[type=text]").val();
                var psg_id = Modal.assignedTaskModal.modal.find(".reject-btn").attr("data-ref");
                Modal.assignedTaskModal.addComment(psg_id, comment);
            });
        },
        open: function () {
            Modal.assignedTaskModal.modal.modal("show");
        },
        close: function () {
            Modal.assignedTaskModal.modal.modal("hide");
        },
        addDriverToMap: function (driver) {

            Modal.assignedTaskModal.clearMarker();

            var taxiMarker = L.marker([driver.curloc[1], driver.curloc[0]], {
                icon: L.icon({
                    iconUrl: '/assets/img/map/tx_PICK@2x.png',
                    iconAnchor: [20, 20],
                    labelAnchor: [10, 0]
                })
            });

            Modal.assignedTaskModal.layer.addLayer(taxiMarker);
            Map.panTo(taxiMarker.getLatLng());
            //Map.setZoom(12);
        },
        clearMarker: function () {
            Modal.assignedTaskModal.layer.clearLayers();
        },
        addComment: function (psg_id, message) {
            Http.post(CONSTRAINTS.serviceURI.editJobList, {
                loader: false,
                data: {
                    "psg_id": psg_id,
                    "cccomment": message
                },
                onSuccess: function (response) {
                    if (response.status) {
                        Notification.defaultSuccess();
                        Modal.assignedTaskModal.close();
                    } else {
                        Notification.defaultError();
                    }
                }
            });
        }
    },
    assignedTaskEditableModal: {
        modal: $("#assignedTaskEditableModal"),
        initial: function () {
            Modal.assignedTaskEditableModal.modal.on("shown.modal.bs", function () {
            });
        },
        showDatail: function (id) {
            Passenger.getData(id, function (response) {
                if (response.status) {
                    var form = Modal.assignedTaskEditableModal.modal.find("form");
                    form.find("#psg_id").val(response.psg_data._id);
                    form.find("#phone").val(response.psg_data.phone);
                    form.find("#curaddr").val(response.psg_data.curaddr);
                    form.find("#destination").val(response.psg_data.destination);
                    form.find("#detail").val(response.psg_data.detail);
                    form.find("#cccomment").val(response.psg_data.cccomment);

                    Modal.assignedTaskEditableModal.open();
                }
            });
        },
        open: function () {
            Modal.assignedTaskEditableModal.modal.modal("show");
        },
        close: function () {
            Modal.assignedTaskEditableModal.modal.modal("hide");
        },
        clearFormData: function () {
            Modal.assignedTaskEditableModal.modal.find("form input").val("");
        }
    },
    confirmModal: {
        modal: $("#confirmModal"),
        initial: function () {
            Modal.confirmModal.modal.on("shown.bs.modal", function () {
                $(window).bind("keydown", { modal: Modal.confirmModal.modal }, Modal.navigatingKeyboard);
            });
            Modal.confirmModal.modal.on("hidden.bs.modal", function () {
                $(window).unbind("keydown", Modal.navigatingKeyboard);
            });
        },
        open: function () {
            Modal.confirmModal.modal.modal("show");
        }
    },
    confirmNoneRegisterDriverModal: {
        data: {},
        modal: $("#confirmNoneRegisterDriverModal"),
        initial: function () {

            Modal.confirmNoneRegisterDriverModal.modal.on("shown.bs.modal", function () {
                $(this).find('.btn.confirm').focus();
                $(window).bind("keydown", { modal: Modal.confirmNoneRegisterDriverModal.modal }, Modal.navigatingKeyboard);
            });

            Modal.confirmNoneRegisterDriverModal.modal.on("hidden.bs.modal", function () {
                Modal.confirmNoneRegisterDriverModal.data = {};
                Form.assignTaskForm.form.find("button").prop("disabled", false);
                Form.searchTaxiBeforeAssignForm.form.find("input").prop("disabled", false);
                Form.searchTaxiBeforeAssignForm.form.find("input").focus();

                $(window).unbind("keydown", Modal.navigatingKeyboard);
            });

            Modal.confirmNoneRegisterDriverModal.modal.find('.confirm').on("click", function () {
                if (!$.isEmptyObject(Modal.confirmNoneRegisterDriverModal.data)) {
                    var data = Modal.confirmNoneRegisterDriverModal.data;
                    Passenger.sendToNoneRegisterDriver(data.psg_id, data.carplate);
                }
            });

            Modal.confirmNoneRegisterDriverModal.modal.find(".modal-footer > .btn");
        },
        open: function () {
            Modal.confirmNoneRegisterDriverModal.modal.modal("show");
        },
        close: function () {
            Modal.confirmNoneRegisterDriverModal.modal.modal("hide");
        },
    },
    successModal: {
        modal: $("#successModal"),
        initial: function () {
            Modal.successModal.modal.on("shown.bs.modal", function (event) {
                $(this).find('[data-dismiss="modal"]').focus();
                Modal.successModal.modal.unbind("hidden.bs.modal", Modal.assignTaskModal.close);
            });
            Modal.successModal.modal.on("hidden.bs.modal", function (event) {
                $(this).find(".message").empty();
            });
        },
        open: function () {
            Modal.successModal.modal.modal("show");
        },
        close: function () {
            Modal.successModal.modal.modal("hide");
        },
        setMessage: function (message) {
            Modal.successModal.modal.find(".message").html(message);
        }
    },
    announcementModal: {
        modal: $("#announcementModal"),
        initial: function () {
            Modal.announcementModal.modal.on("show.bs.modal", function (event) {
                Announcement.getAll();
            });

            Modal.announcementModal.modal.on("shown.bs.modal", function (event) {
            });

            Modal.announcementModal.modal.on("hidden.bs.modal", function (event) {
                $(this).find(".modal-body").empty();
                Form.announceForm.form.find("label.error").remove();
            });
        },
        open: function () {
            Modal.announcementModal.modal.modal("show");
        },
        close: function () {
            Modal.announcementModal.modal.modal("hide");
        },
    },
    announcementEditModal: {
        modal: $("#announcementEditModal"),
        initial: function () {
            Modal.announcementEditModal.modal.on("hidden.bs.modal", function (event) {
                $(this).find(".modal-body").find("input[type='text'], input[type='hidden'], textarea").val("");
                Form.announceEditForm.form.find("label.error").remove();
            });
        },
        open: function () {
            Modal.announcementEditModal.modal.modal("show");
        },
        close: function () {
            Modal.announcementEditModal.modal.modal("hide");
        }
    },
    reportModal: {
        modal: $("#reportModal"),
        initial: function () {

            Modal.reportModal.modal.on("hidden.bs.modal", function (event) {
                DataTable.taxiReportTable.datatable.clear().draw(false);
            });

            Modal.reportModal.modal.on("show.bs.modal", function (event) {
                DataTable.taxiReportTable.update();
            });

            Modal.reportModal.modal.on("shown.bs.modal", function (event) {

                Report.setDetail();

                var daterange = {
                    startTime: Report.getCurrentDate().getTime() - Report.day,
                    endTime: Report.getCurrentDate().getTime()
                };

                Report.countJobPerDrv(daterange);
                Report.getMostHitStartPlace(daterange);
                Report.getMostHitDestinationPlace(daterange);
                Report.getMostHitHours(daterange);
            });

            Modal.reportModal.modal.find(".modal-body").find('.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                if (e.target.hash == "#summary-tab") {
                }
                else if (e.target.hash == "#taxi-tab") {
                    DataTable.taxiReportTable.datatable.draw(false);
                }
            });
        },
        open: function () {
            Modal.reportModal.modal.modal("show");
        },
        close: function () {
            Modal.reportModal.modal.modal("hide");
        }
    },
    messageBoxModal: {
        modal: $("#messageBoxModal"),
        initial: function () {
            Modal.messageBoxModal.modal.on("hidden.bs.modal", function (event) {
                Modal.messageBoxModal.clearData();
            });
        },
        open: function () {
            Modal.messageBoxModal.modal.modal("show");
        },
        close: function () {
            Modal.messageBoxModal.modal.modal("hide");
        },
        setTitle: function (text) {
            Modal.messageBoxModal.modal.find(".modal-body").find(".title").html(text);
        },
        setMessage: function (text) {
            Modal.messageBoxModal.modal.find(".modal-body").find(".message").html(text);
        },
        clearData: function () {
            Modal.messageBoxModal.modal.find(".modal-body").find(".title").empty();
            Modal.messageBoxModal.modal.find(".modal-body").find(".message").empty();
        }
    },
    searchTaskWithResultListModal: {
        modal: $("#searchTaskWithResultListModal"),
        initial: function () {

            Modal.searchTaskWithResultListModal.modal.find('.modal-dialog').draggable({
                handle: ".modal-header"
            });

            Modal.searchTaskWithResultListModal.modal.on('shown.bs.modal', function (e) {
                DataTable.searchTaskResultTable.getData({
                    startTime: moment().subtract(1, 'days').toDate().getTime(),
                    endTime: moment().toDate().getTime()
                });
            });
            
            Modal.searchTaskWithResultListModal.setupDateRangePicker();
            Modal.searchTaskWithResultListModal.modal.on("shown.bs.modal", function () {
                DataTable.searchTaskResultTable.datatable.sort([9, "asc"]).fnDraw();

                Form.searchFinishedTaskForm.form.find("[name=keyword]").focus();
            });

            DataTable.searchTaskResultTable.getData({
                startTime: moment().subtract(1, 'days').toDate().getTime(),
                endTime: moment().toDate().getTime()
            });
        },
        open: function () {
            Modal.searchTaskWithResultListModal.modal.modal("show");
        },
        close: function () {
            Modal.searchTaskWithResultListModal.modal.modal("hide");
        },
        setupDateRangePicker: function () {

            var daterange = Modal.searchTaskWithResultListModal.modal.find(".daterange");

            function cb(start, end) {
                daterange.find("span").html(start.format('DD/MM/YYYY HH:mm') + ' - ' + end.format('DD/MM/YYYY HH:mm'));
            }

            cb(moment().subtract(1, 'days'), moment());

            daterange.daterangepicker(CONSTRAINTS.daterangepicker.options, cb).on('apply.daterangepicker', function (ev, picker) {

                var message = daterange.find("span").html();
                Modal.reportModal.modal.find(".modal-header > .modal-message .detail").text(message);

                var data = {
                    startTime: new Date(picker.startDate).getTime(),
                    endTime: new Date(picker.endDate).getTime()
                };

                var keyword = Form.searchFinishedTaskForm.form.find('[name="keyword"]').val();
                if (keyword !== "") {
                    data.keyword = keyword;
                }

                Http.post(CONSTRAINTS.serviceURI.searchFinishList, {
                    data: data,
                    onSuccess: function (response) {
                        if (response.status) {
                            Notification.remove();
                            DataTable.searchTaskResultTable.render(response.data);
                        }
                        else {
                            DataTable.searchTaskResultTable.clear();
                            Notification.show({
                                title: 'ไม่มีข้อมูลที่ค้นห้า',
                                time: 5000,
                                text: 'ไม่พบข้อมูลที่เกี่ยวข้องกับ "' + Form.searchFinishedTaskForm.form.find('[name="keyword"]').val() + '"',
                                $class: "info"
                            });
                        }
                    }
                });

            }).on('showCalendar.daterangepicker', function (ev, picker) {

            });
        }
    },
    areaSuggestion: {
        modal: $("#areaSuggestion"),
        initial: function () {
            Http.post(CONSTRAINTS.serviceURI.getPOIRecommended, {
                data: {
                    "cgroup": USER_DATA.group
                },
                onSuccess: function (response) {
                    if (response.status) {

                        if (response.data.length > 0) {
                            var row_length = Math.ceil(response.data[0]["parkingCount"] / 4);
                            var html = '<div class="mdl-cell mdl-cell--3-col">';
                            html += '<ul class="mdl-list">';
                            for (var i = 0; i >= row_length; i++) {
                                html += '<li class="mdl-list__item mdl-list__item--two-line">';
                                html += '<span class="mdl-list__item-primary-content">';
                                html += '<i class="material-icons mdl-list__item-icon">store_mall_directory</i>';
                                html += '<span>' + response.data[0]["parkinglist"][i]["parkingname"] + '</span>';
                                html += '<span class="mdl-list__item-sub-title">' + response.data[0]["parkinglist"]["parkinglot"] + '</span>';
                                html += '</span>';
                                html += '</li>'
                            }
                            html += '</ul>';
                            html += '</div>';
                        }
                    }
                }
            });

            Modal.areaSuggestion.modal.on("shown.bs.modal", function (event) {
                $(this).data({ input: $(event.relatedTarget).closest(".input-group").find("input[type=text]") });
            });

            Modal.areaSuggestion.modal.on("hidden.bs.modal", function (event) {
                
            });

        },
        open: function () {

        },
        close: function () {

        }
    },
    driverListChatModal: {
        modal: $("#driverListChatModal"),
        initial: function () {
            Modal.driverListChatModal.modal.on("shown.bs.modal", function () {

            });
        },
        open: function () {
            Modal.driverListChatModal.modal.modal("show");
        },
        close: function () {
            Modal.driverListChatModal.modal.modal("hide");
        }
    },
    liveChatModal: {
        modal: $("#liveChatModal"),
        initial: function () {
            Modal.liveChatModal.modal.on("shown.bs.modal", function () {

            });

            Modal.liveChatModal.modal.on("hidden.bs.modal", function () {
                $(this).find(".driver-img-wrapper img").prop("src", "https://placeholdit.imgix.net/~text?txtsize=22&txt=150%C3%97150&w=150&h=150");
                $(this).find(".driver-detail .name").text("");
                $(this).find(".driver-detail .carplate").text("");
            });
        },
        open: function (caller) {

            var driver = $(caller).data()._driver;
            Modal.liveChatModal.modal.find(".driver-img-wrapper img").prop("src", CONSTRAINTS.driverImageURL + driver.imgface);
            Modal.liveChatModal.modal.find(".driver-detail .name").text(driver.fname);
            Modal.liveChatModal.modal.find(".driver-detail .carplate").text(driver.carplate);

            Modal.liveChatModal.modal.modal("show");
        },
        close: function () {
            Modal.liveChatModal.modal.modal("hide");
        }
    },
    navigatingKeyboard: function (event) {

        var buttons = $(event.data.modal).find(".modal-footer .btn");
        var activeElem = document.activeElement;

        if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
            if ($(activeElem).closest(".modal-footer").size() <= 0) {
                $(buttons[0]).focus();
                return false;
            }
        }

        if (event.keyCode == 38 || event.keyCode == 39) {
            var index = $(buttons).index(activeElem);
            buttons.length >= (index + 1) ? $(buttons[index + 1]).focus() : $(buttons[buttons.length - 1]).focus();
        }
        else if (event.keyCode == 37 || event.keyCode == 40) {
            var index = $(buttons).index(activeElem);
            index == 0 ? $(buttons[0]).focus() : $(buttons[index - 1]).focus();
        }
    },
    pickUpPOIModal: {
        modal: $('#pickUpPOIModal'),
        map: null,
        marker: null,
        poiMarker: null,
        currentPickupData: null,
        currentPickupImage: CONSTRAINTS.serviceURI.googleMapIconMarkerBlueDot,
        initial: function () {
            
            Modal.pickUpPOIModal.modal.on('show.bs.modal', function (e) {
                 Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").hide();
                 Modal.pickUpPOIModal.modal.find("#confirmAsImprovePOI").hide();       
            });

            Modal.pickUpPOIModal.modal.on('shown.bs.modal', function (e) {
                
                if(Modal.pickUpPOIModal.map === null) {
                    Modal.pickUpPOIModal.initMap();
                }

                var lat = Modal.createTaskModal.modal.find("#pass-from-inp").attr("data-lat");
                var lng = Modal.createTaskModal.modal.find("#pass-from-inp").attr("data-lng");

                if(lat && lng) {
                    
                    if(Modal.pickUpPOIModal.marker !== null) {
                        Modal.pickUpPOIModal.marker.setPosition(new google.maps.LatLng(lat, lng));
                        Modal.pickUpPOIModal.marker.setMap(Modal.pickUpPOIModal.map);
                        Modal.pickUpPOIModal.modal.find(".current-lat-lng").val(lat + ", " + lng);
                    }
                    else if(Modal.pickUpPOIModal.marker === null) {
                        Modal.pickUpPOIModal.placeMarker(Modal.pickUpPOIModal.map, new google.maps.LatLng(lat, lng));
                        Modal.pickUpPOIModal.marker.setMap(Modal.pickUpPOIModal.map);
                        Modal.pickUpPOIModal.modal.find(".current-lat-lng").val(lat + ", " + lng);
                    }
                    Modal.pickUpPOIModal.map.setZoom(15);
                    Modal.pickUpPOIModal.map.panTo({ lat: parseFloat(lat), lng: parseFloat(lng) });
                } else {
                    var placeName = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
                    Modal.pickUpPOIModal.getGeocodeByPlaceName(placeName);
                    Modal.pickUpPOIModal.map.panTo({ lat: USER_CURRENT_LOCATION[0], lng: USER_CURRENT_LOCATION[1] });
                    Modal.pickUpPOIModal.map.setZoom(13);
                }
                Modal.pickUpPOIModal.modal.find("#pac-input").val("");
                Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text("");  
            });

            Modal.pickUpPOIModal.modal.on('hidden.bs.modal', function (e) {
                Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").hide();
                Modal.pickUpPOIModal.modal.find("#confirmAsImprovePOI").hide();
            });

            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").on("click", function() {
                Modal.pickUpPOIModal.modal.find("#pac-input").val($(this).text());
            });

            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel #confirm").on("click", Modal.pickUpPOIModal.onSubmit);

            Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").on("click", Modal.createNewPOIModal.open);
            Modal.pickUpPOIModal.modal.find("#confirmAsImprovePOI").on("click", Modal.createNewPOIModal.open);
        },
        open: function (event) {
            Modal.pickUpPOIModal.fillData();
            Modal.pickUpPOIModal.modal.modal("show");
        },
        close: function (event) {
            Modal.pickUpPOIModal.modal.modal("hide");
        },
        initMap: function () {

            Modal.pickUpPOIModal.map = new google.maps.Map(document.getElementById('pickUpPOIMap'), {
                center: { lat: USER_CURRENT_LOCATION[0], lng: USER_CURRENT_LOCATION[1] },
                zoom: 12
            });

            Modal.pickUpPOIModal.map.setClickableIcons(false);

            Modal.pickUpPOIModal.placeMarker(Modal.pickUpPOIModal.map, Modal.pickUpPOIModal.map.getCenter());

            var input = document.getElementById('pac-input');

            Modal.pickUpPOIModal.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var autocomplete = new google.maps.places.Autocomplete(input, {
                types: []
            });

            autocomplete.bindTo('bounds', Modal.pickUpPOIModal.map);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                if (place.geometry && place.geometry.viewport) {
                    Modal.pickUpPOIModal.map.fitBounds(place.geometry.viewport);
                    moveMarker(place.name, place.geometry.location);
                }
                else if (place.geometry && place.geometry.location) {
                    Modal.pickUpPOIModal.map.setCenter(place.geometry.location);
                    Modal.pickUpPOIModal.map.setZoom(17);
                    moveMarker(place.name, place.geometry.location);
                }
            });

            // http://jsfiddle.net/dodger/pbbhH/
            $(input).one("focusin", function () {
                $(document).one("keypress", function (e) {
                    if (e.which == 13) {
                        var firstResult = $(".pac-container .pac-item:first").text();

                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ "address": firstResult, 'partialmatch': true, 'region': 'th' }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var lat = results[0].geometry.location.lat(),
                                    lng = results[0].geometry.location.lng(),
                                    placeName = results[0].address_components[0].long_name,
                                    latlng = new google.maps.LatLng(lat, lng);

                                moveMarker(placeName, latlng);
                                $("input").val(firstResult);
                            }
                        });
                    }
                });
            });

            function moveMarker(placeName, latlng) {
                Modal.pickUpPOIModal.marker.setPosition(latlng);
                Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text(placeName);

                Modal.pickUpPOIModal.modal
                    .find(".current-lat-lng")
                    .val(latlng.lat().toString().substring(0, 10) + ", " + latlng.lng().toString().substring(0, 10));
            }

            Modal.pickUpPOIModal.setOnMapClick(Modal.pickUpPOIModal.map, Modal.pickUpPOIModal.marker);
        },
        setMapCenter: function(lat, lng) {
            Modal.pickUpPOIModal.map.setCenter({ lat: lat, lng: lng });
            Modal.pickUpPOIModal.map.setZoom(18);
        },
        getGeocode: function (location) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': location, 'partialmatch': true, 'region': 'th' }, function (results, status) {
                if (status === 'OK') {

                    Modal.pickUpPOIModal.currentPickupData = results[0];

                    if (results[0]) {
                        Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text(results[0].formatted_address);
                        $("#pac-input").val(results[0].formatted_address);
                    } else {
                        console.log('No results found');
                    }
                } else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
        },
        getGeocodeByPlaceName: function(place) {
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'address': place, 'region': 'th' }, function (results, status) {
                if (status === 'OK') {

                    Modal.pickUpPOIModal.currentPickupData = results[0];

                    if (results[0]) {
                        Modal.pickUpPOIModal.modal.find("#pickupPOIpanel").find("p.caption.formatted_address").text(results[0].formatted_address);
                        $("#pac-input").val(results[0].formatted_address);
                    } else {
                        console.log('No results found');
                    }
                } else {
                    console.log('Geocoder failed due to: ' + status);
                }
            });
        },
        setOnMapClick: function (map, marker) {
            google.maps.event.addListener(map, 'click', function(event) {
                Modal.pickUpPOIModal.placeMarker(map, event.latLng);
                Modal.pickUpPOIModal.getGeocode(event.latLng);
                placeName = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
                if (placeName !== '') {
                    Modal.pickUpPOIModal.modal.find("#confirmAsImprovePOI").show();
                } else 
                if (placeName === '') {
                    Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").show();
                }
            });
        },
        placeMarker: function (map, location) {

            if (Modal.pickUpPOIModal.marker === null) {
                Modal.pickUpPOIModal.marker = new google.maps.Marker({
                    position: location,
                    draggable: true,
                    map: map
                });

                google.maps.event.addListener(Modal.pickUpPOIModal.marker, "dragend", function (event) {

                    Modal.pickUpPOIModal.getGeocode(event.latLng);
                    if (placeName !== '') {
                        Modal.pickUpPOIModal.modal.find("#confirmAsImprovePOI").show();
                        Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").hide();
                    } else 
                    if (placeName === '') {
                        Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").show();
                    } 
                   
                    Modal.pickUpPOIModal.modal
                        .find(".current-lat-lng")
                        .val(event.latLng.lat().toString().substring(0, 10) + ", " + event.latLng.lng().toString().substring(0, 10));
                });
            } else {
                Modal.pickUpPOIModal.marker.setPosition(location);
                Modal.pickUpPOIModal.marker.setMap(Modal.pickUpPOIModal.map);
            }
            
            Modal.pickUpPOIModal.map.panTo(location);
            Modal.pickUpPOIModal.modal
                .find(".current-lat-lng")
                .val(location.lat().toString().substring(0, 10) + ", " + location.lng().toString().substring(0, 10));
        },
        fillData: function () {

            var currentAddress = Modal.createTaskModal.modal.find("#pass-from-inp").val();
            var descriptionText = Modal.createTaskModal.modal.find("#pass-desc-txt").val();

            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text(currentAddress);
            Modal.pickUpPOIModal.modal.find("#pass-desc-txt").val(descriptionText);
        },
        clearData: function() {
            Modal.pickUpPOIModal.modal.find("a.current-address").text("");
            Modal.pickUpPOIModal.modal.find(".current-lat-lng").val("");
            Modal.pickUpPOIModal.modal.find("p.caption.formatted_address").text("");
            Modal.pickUpPOIModal.modal.find("#pass-desc-txt").val("");
            Modal.pickUpPOIModal.modal.find("#pac-input").val("");

            if(USER_DATA.broadcast === "true" && Modal.pickUpPOIModal.marker) {
                Modal.pickUpPOIModal.marker.setMap(null);
            }
            Modal.pickUpPOIModal.currentPickupData = null;
        },
        onSubmit: function() {

            var valid = Modal.pickUpPOIModal.checkFormValue();

            if(valid) {
                var currentAddress = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
                var descriptionText = Modal.pickUpPOIModal.modal.find("#pass-desc-txt").val();

                Modal.createTaskModal.modal.find("#pass-from-inp").val(
                    currentAddress || Modal.pickUpPOIModal.currentPickupData.address_components[0].long_name);

                Modal.createTaskModal.modal.find("#pass-desc-txt").val(descriptionText);
                Modal.createTaskModal.modal.find("#pass-from-inp").attr("data-lat", Modal.pickUpPOIModal.marker.getPosition().lat());
                Modal.createTaskModal.modal.find("#pass-from-inp").attr("data-lng", Modal.pickUpPOIModal.marker.getPosition().lng()); 

                Modal.pickUpPOIModal.close();
                Modal.createTaskModal.modal.find("#pickUpPOIBtn").addClass("valid");
            } else {
                Modal.messageBoxModal.setTitle("คำเตือน");
                Modal.messageBoxModal.setMessage("กรุณาปักหมุดให้เรียบร้อย");
                Modal.messageBoxModal.open();
            }
        },
        checkFormValue: function() {
            var latlngString = Modal.pickUpPOIModal.modal.find(".current-lat-lng").val();
            if(latlngString) {
                var latlng = latlngString.split(",");
                return Modal.pickUpPOIModal.isValidLatLng(latlng[0].trim(), latlng[1].trim());
            }
            return false;
        },

        isValidLatLng: function(latitude, longitude) {

            // http://stackoverflow.com/a/31408260

            if(parseInt(latitude) === NaN || parseInt(longitude) === NaN) {
                return false;
            }

            var latArr = latitude.split(".");
            var longArr = longitude.split(".");
            var latReg = /^(\+|-)?(?:90(?:(?:\.0{1,15})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,15})?))$/;
            var longReg = /^(\+|-)?(?:180(?:(?:\.0{1,15})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,15})?))$/;

            if(latArr[0] <= 2) {
                latitude = latitude.substring(0, 15);
            } else if (latArr[0] > 2) {
                latitude = latitude.substring(0, 14);
            }

            if(longArr[0] <= 2) {
                longitude = longitude.substring(0, 15);
            } else if (longArr[0] > 2) {
                longitude = longitude.substring(0, 14);
            }

            var valid = false;

            if( latReg.test(latitude) && longReg.test(longitude) ) {
                valid = true;
            }
            return valid;
        }  
    },
    createNewPOIModal: {
        modal: $('#createNewPOIModal'),
        form: $('#createNewPOIModal').find("form"),
        submitBtn: $('#createNewPOIModal').find("form input[type=submit]"),
        initial: function() {
            Modal.createNewPOIModal.form.on("submit", function(event) {
                                
                event.preventDefault();
                
                var parkingname = $(this).find("input[name=parkingname]").val();
                var description = $(this).find("input[name=description]").val();
                var lat = parseFloat(Modal.pickUpPOIModal.marker.getPosition().lat().toFixed(6));
                var lng = parseFloat(Modal.pickUpPOIModal.marker.getPosition().lng().toFixed(6));
                var curloc = [lng, lat];

                if(!parkingname || parkingname === '' || !lat || !lng) {
                    Modal.noCurrentPlace.open();
                }

                $.post(CONSTRAINTS.serviceURI.addPOI, { curloc: curloc, parkingname: parkingname, description: description }, function (res) {
                    if(res.status) {
                        Modal.createNewPOIModal.modal.find("input[type=text]").val("");
                        Modal.createNewPOIModal.close();
                        Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text(parkingname);
                        Modal.createNewPOIModal.modal.find("#submit").on("click", Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").hide());
                    } else {
                        Modal.reCurrentPlace.open();
                    }
                });
            });
        },
        open: function (event) {
            var currentText = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
            if(currentText) {
                Modal.createNewPOIModal.modal.find("input[name=parkingname]").val(currentText);
            }
            Modal.createNewPOIModal.modal.modal("show");
        },
        close: function (event) {
            Modal.createNewPOIModal.modal.modal("hide");
        }
    },
    noCurrentPlace: {
        modal: $('#noCurrentPlace'),
        submitBtn: $('#noCurrentPlace').find("input[type=submit]"),
        initial: function() {
            Modal.noCurrentPlace.modal.on("click", function(event){
                Modal.noCurrentPlace.close();
        });

        },
        open: function() {
            Modal.noCurrentPlace.modal.modal("show");
        },
        close: function() {
            Modal.noCurrentPlace.modal.modal("hide");
        }    
    },
    reCurrentPlace: {
        modal: $('#reCurrentPlace'),
        submitBtn: $('#reCurrentPlace').find("input[type=submit]"),
        initial: function () {
            Modal.reCurrentPlace.modal.on("click", function (event) {

                var parkingname = Modal.createNewPOIModal.modal.find("input[name=parkingname]").val();
                var description = Modal.createNewPOIModal.modal.find("input[name=description]").val();
                var lat = parseFloat(Modal.pickUpPOIModal.marker.getPosition().lat().toFixed(6));
                var lng = parseFloat(Modal.pickUpPOIModal.marker.getPosition().lng().toFixed(6));

                $.post('/garage/poi/updateCurrentPOI',
                    {
                        parkingname: parkingname,
                        description: description,
                        lat: lat,
                        lng: lng
                    },
                    function (response) {

                        var poiArray = JSON.parse(localStorage.getItem("poi_recommended"));

                        JSON.parse(localStorage.getItem("poi_recommended")).forEach(function (poi, index) {
                            if (poi._id === response._id) {
                                poiArray[index].parkingname = response.parkingname;
                                description = response.description;
                                // Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text(parkingname);
                                // Modal.createNewPOIModal.modal.find("input[type=text]").val("");
                            }
                        });

                        var poiString = JSON.stringify(poiArray);

                        localStorage.setItem("poi_recommended", poiString);

                        var phone = Form.createTaskForm.form.find("#pass-phone-inp").val();

                        if (phone) {
                            POIRecommended.updateLookup(phone);
                        } else {

                            var garagePois = $.map(poiArray, function (dataItem) {
                                if (!dataItem) { return; }
                                return {
                                    value: dataItem.parkingname,
                                    data: dataItem,
                                    ID: dataItem._id,
                                    type: "LOCATION"
                                };
                            });

                            garagePois = POIRecommended.shuffle(garagePois);

                            Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text(parkingname);
                            Modal.createNewPOIModal.modal.find("input[type=text]").val("");
                            Form.createTaskForm.form.find("#pass-from-inp").data('autocomplete').options.lookup = garagePois;
                            Form.createTaskForm.form.find("#pass-dest-inp").data('autocomplete').options.lookup = garagePois;
                            Modal.createNewPOIModal.modal.find("#submit").on("click", Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").hide());
                        }
                    });
                Modal.reCurrentPlace.close();
                Modal.createNewPOIModal.close();
                Modal.reCurrentPlace.modal.find("#submit").on("click", Modal.pickUpPOIModal.modal.find("#confirmAsNewPOI").hide());
                Modal.reCurrentPlace.modal.find("#submit").on("click", Modal.pickUpPOIModal.modal.find("#confirmAsImprovePOI").hide());
            });
        },
    
        open: function () {
            var currentText = Modal.pickUpPOIModal.modal.find("#pickupPOIpanel a.current-address").text();
            if (currentText) {
                Modal.createNewPOIModal.modal.find("input[name=parkingname]").val(currentText);
            }
            Modal.reCurrentPlace.modal.modal("show");
        },
        close: function () {
            Modal.reCurrentPlace.modal.modal("hide");
        }
    }
};


var Monitoring = {
    taxi: null,
    status: {
        HOLD: 0, ON: 0, DPENDING: 0,
        BUSY: 0, PICK: 0, ASSIGNED: 0
    },
    initial: function () {

        this.updateNotification();

        this.intervalUpdateNotification();

        $(".navbar .navbar-right > .monitoring-tab").find('[data-toggle="tooltip"]').tooltip({ placement: 'bottom', container: 'body', animation: false });

        $(".taxi-counter")
        .find('[data-toggle="tooltip"]').tooltip({ placement: 'left', container: 'body', animation: false })
        .on('show.bs.tooltip', function () { $('body').find('.tooltip.in').remove(); });

        $(".container-fluid.monitoring").find('form.find-taxi').on("submit", function (event) {
            event.preventDefault();
            var carplate = $(this).find("#taxi-carplate").val();
            Taxi.findByCarplate(carplate);
        });

        var navbarMenuInitial = function () {
            $("#sound-switch").on("change", function (event) {
                var checked = $(this).is(":checked");

                if (checked && !Sound.control.is(":checked")) {
                    Sound.control.parent().find(".switchery").click();
                }
                else if (!checked && Sound.control.is(":checked")) {
                    Sound.control.parent().find(".switchery").click();
                }
            });
        }

        navbarMenuInitial();
    },
    isActive: false,
    toggle: function (event) {
        this.isActive ? this.hide(event) : this.show(event);
    },
    show: function (event) {

        if (this.isActive) { return false; }
        Preloader.show();

        $(".navbar .navbar-right > .monitoring-tab").find('.counter-wrapper').hide();

        $(".navbar .navbar-right > .monitoring-tab").addClass("active");
        $(".navbar .navbar-right > .callcenter-tab").removeClass("active");
        var mapWrapper = $(".container-fluid.monitoring");

        var isVisible = $("#map").is(":visible");
        var inMonitoring = $("#map").parent().is(".monitoring");
        if (!inMonitoring) {
            jQuery("#map").detach().appendTo(mapWrapper);
        }

        mapWrapper.show();
        $("#map").removeClass('hide');
        $(".container-fluid.app").hide();

        App.setDefaultPosition();

        setTimeout(function () {
            Map.invalidateSize();
        }, 100);

        this.intervalUpdateTaxiPosition();
        LandMark.getAll();

        this.isActive = true;
    },
    hide: function (event) {

        if (!this.isActive) { return false; }

        $(".navbar .navbar-right > .monitoring-tab").find('.counter-wrapper').show();

        $(".navbar .navbar-right > .monitoring-tab").removeClass("active");
        $(".navbar .navbar-right > .callcenter-tab").addClass("active");
        $(".container-fluid.monitoring").hide();
        $(".container-fluid.app").show();

        this.isActive = false;
        Monitoring.taxi = null;
        clearInterval(this.showTaxiIntervalId);
        Taxi.clear();
    },
    showTaxi: function () {

        Http.post(CONSTRAINTS.serviceURI.searchDriver, {
            loader: false,
            data: {
                "curlat": USER_CURRENT_LOCATION[0],
                "curlng": USER_CURRENT_LOCATION[1]
            },
            onSuccess: function (response) {
                if (response.status) {

                    Monitoring.taxi = [];
                    Monitoring.clearCounter();

                    $(response.data).each(function (index, driver) {

                        switch (driver.status) {
                            case "HOLD":
                            Monitoring.status.HOLD++;
                            break;
                            case "ON":
                            Monitoring.status.ON++;
                            break;
                            case "DPENDING":
                            Monitoring.status.DPENDING++;
                            break;
                            case "BUSY":
                            Monitoring.status.BUSY++;
                            break;
                            case "PICK":
                            Monitoring.status.PICK++;
                            break;
                            case "ASSIGNED":
                            Monitoring.status.ASSIGNED++;
                            break;
                        }

                        Monitoring.taxi.push({
                            "_id": driver._id,
                            "license_plate": (driver.prefixcarplate || "") + " " + driver.carplate,
                            "prefixcarplate": driver.prefixcarplate,
                            "carplate": driver.carplate,
                            "lat": driver.curloc[1],
                            "lng": driver.curloc[0],
                            "status": driver.status,
                            "imgface": driver.imgface,
                            "full_name": driver.fname + ' ' + driver.lname,
                            "tel": driver.phone,
                            "brokendetail": driver.brokendetail,
                            "brokenname": driver.brokenname
                        });
                    });

                    Taxi.clear();
                    Monitoring.taxi.filter(function (taxi) {
                        if (!$(".taxi-counter").find(".list-group-item." + taxi.status).find(".js-switch").is(":checked")) {
                            return false;
                        }
                        Taxi.addToMap(taxi, { monitoring: true }, false);
                    });
                    Monitoring.adjustCounter();
                } else {
                    Taxi.clear();
                }

                Preloader.hide();
            }
        });
    },
    toggleFilterControl: function (control) {
        $(control).parent().toggleClass('open');
        var icon = $(control).find("i");
        icon.text() == "chevron_left" ? icon.text("clear") : icon.text("chevron_left");
    },
    clearCounter: function () {
        this.status.HOLD = 0;
        this.status.ON = 0;
        this.status.DPENDING = 0;
        this.status.BUSY = 0;
        this.status.PICK = 0;
        this.status.ASSIGNED = 0;
    },
    updateNotification: function () {
        Http.post(CONSTRAINTS.serviceURI.countTaxi, {
            loader: false,
            data: {
                "curlat": USER_CURRENT_LOCATION[0],
                "curlng": USER_CURRENT_LOCATION[1]
            },
            onSuccess: function (response) {
                if (response.status) {
                    Monitoring.clearCounter();
                    $(response.data).each(function (index, driver) {
                        switch (driver.status) {
                            case "HOLD":
                            Monitoring.status.HOLD++;
                            break;
                            case "ON":
                            Monitoring.status.ON++;
                            break;
                            case "DPENDING":
                            Monitoring.status.DPENDING++;
                            break;
                            case "BUSY":
                            Monitoring.status.BUSY++;
                            break;
                            case "PICK":
                            Monitoring.status.PICK++;
                            break;
                            case "ASSIGNED":
                            Monitoring.status.ASSIGNED++;
                            break;
                        }
                    });
                    Monitoring.adjustCounter();
                }
            }
        });
    },
    adjustCounter: function () {
        var counter = $('.monitoring').find(".taxi-counter");
        counter.find(".HOLD .badge").text(this.status.HOLD);
        counter.find(".ON .badge").text(this.status.ON);
        counter.find(".DPENDING .badge").text(this.status.DPENDING);
        counter.find(".BUSY .badge").text(this.status.BUSY);
        counter.find(".PICK .badge").text(this.status.PICK);
        counter.find(".ASSIGNED .badge").text(this.status.ASSIGNED);

        var notification = $(".navbar .navbar-right > .monitoring-tab").find('.counter-wrapper');
        notification.find(".hold").text(this.status.HOLD);
        notification.find(".on").text(this.status.ON);
        notification.find(".pick").text(this.status.PICK);
    },
    intervalUpdateNotification: function () {
        //this.intervalId = App.intervalManager(true, this.updateNotification, 5000);
    },
    intervalUpdateTaxiPosition: function () {
        this.showTaxiIntervalId = App.intervalManager(true, this.showTaxi, 5000);
    }
};


var Notification = {
    show: function (message) {
        options = {
            title: message.title,
            text: message.text,
            sticky: message.sticky ? message.sticky : false,
            time: message.time ? message.time : 2000,
            class_name: 'gritter-' + (!message.$class ? 'warning' : message.$class),
            position: message.position ? message.position : 'bottom-right'
        };

        $.gritter.add(options);
    },
    defaultSuccess: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Success!' : title,
            text: !text ? 'คำสั่ง ทำงานเรียบร้อย' : text,
            sticky: false,
            time: 1500,
            class_name: 'gritter-success',
            position: 'bottom-right'
        });
    },
    defaultError: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Server Error!' : title,
            text: !text ? 'Pls. refresh page and then try it again.' : text,
            sticky: false,
            time: 2500,
            class_name: 'gritter-danger',
            position: 'bottom-right'
        });
    },
    removeAll: function () {
        $.gritter.removeAll({
            time: 0,
            before_close: function (e) {
            },
            after_close: function () {
            }
        });
    },
    remove: function () {
        $("#gritter-notice-wrapper").remove();
    }
};


var Parking = {
    openReportWindow: function () {
        var left = (window.screen.availWidth - 1400) / 2;
        window.open("./callcenter/parking-system", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=" + left + ",width=1400,height=800");
    }
};


var Passenger = {
    marker: {},
    getData: function (id, callback) {
        Http.post(CONSTRAINTS.serviceURI.getPassengerDetail, {
            loader: false,
            data: { "psg_id": id },
            onSuccess: callback
        });
    },
    editData: function (id, data) {
        var data = jQuery.extend({ "psg_id": id }, data);
        Http.post(CONSTRAINTS.serviceURI.editJobList, {
            loader: false,
            data: data,
            onSuccess: function (response) {
                if (response.status) {
                    Notification.remove();
                    Notification.defaultSuccess();
                } else {
                    Notification.remove();
                    Notification.defaultError();
                }
            }
        });
    },
    addToMap: function (latlng, passenger_info) {

        this.removeMarker();

        this.marker = L.marker(latlng, {
            icon: L.icon({
                iconUrl: '/assets/img/map/psg_pin.png',
                iconAnchor: [12, 20],
                labelAnchor: [10, 0]
            })
        });

        var HTMLPopup = '<div class="row">';
        HTMLPopup += '<div class="passenger-info">';

        HTMLPopup += '<div class="col-md-12">';
        HTMLPopup += '<p class="caption">เบอร์โทรติดต่อ</p>';
        HTMLPopup += '<p class="tel value">' + Util.toReadablePhoneNumber(passenger_info.phone.toString()) + '</p>';
        HTMLPopup += '</div>';

        HTMLPopup += '<div class="col-md-12">';
        HTMLPopup += '<p class="caption">จาก</p>';
        HTMLPopup += '<p class="current-address value">' + passenger_info.curaddr + '</p>';
        HTMLPopup += '</div>';

        HTMLPopup += '<div class="col-md-12">';
        HTMLPopup += '<p class="caption">ไปยัง</p>';
        HTMLPopup += '<p class="status value">' + passenger_info.destination + '</p>';
        HTMLPopup += '</div>';

        HTMLPopup += '<div class="col-md-12">';
        HTMLPopup += '<p class="caption">รายละเอียด</p>';
        HTMLPopup += '<p class="detail value">' + passenger_info.detail + '</p>';
        HTMLPopup += '</div>';

        HTMLPopup += '</div>';
        HTMLPopup += '</div>';

        this.marker.bindPopup(HTMLPopup, {
            offset: L.point(0, -18),
            className: 'passenger-popup'
        });

        this.marker.options.passenger_info = passenger_info;
        this.marker.addTo(Map);
    },
    showConfirmRejectModal: function (id) {

        var onShow = function (event) {
            Modal.confirmModal.modal.find('.confirm').on("click", function () { window["Passenger"]["rejectTask"](id) });

            setTimeout(function () {
                $(".modal-backdrop.in.modal-stack").css("opacity", 0.5);
            }, 0);
        }

        var onClose = function (event) {
            Modal.confirmModal.modal.find('.confirm').unbind();
            Modal.confirmModal.modal.off('show.bs.modal');
            $(".modal-backdrop.in.modal-stack").css("opacity", 0.1);

            Modal.assignedTaskModal.modal.find(".reject-btn").prop("disabled", false);
            Modal.confirmModal.modal.unbind('shown.bs.modal', onShow);
            Modal.confirmModal.modal.unbind('hidden.bs.modal', onClose);
        }

        Modal.confirmModal.modal.on('shown.bs.modal', { modal: this }, onShow);

        Modal.confirmModal.modal.on('hidden.bs.modal', { modal: this }, onClose);

        Modal.confirmModal.modal.find(".modal-body > .message").html("ต้องยกเลิกรายการนี้ใช่มั้ย?");

        Modal.confirmModal.open();
    },
    showConfirmRemoveModal: function (id) {

        var onShow = function (event) {
            Modal.confirmModal.modal.find('[data-dismiss="modal"]').focus();
            Modal.confirmModal.modal.find('.confirm').on("click", function () { window["Passenger"]["cancelTask"](id) });
        }

        var onClose = function (event) {
            Modal.confirmModal.modal.find('.confirm').unbind();
            Modal.confirmModal.modal.unbind('shown.bs.modal', onShow);
            Modal.confirmModal.modal.unbind('hidden.bs.modal', onClose);
        }

        Modal.confirmModal.modal.on('shown.bs.modal', { modal: this }, onShow);
        Modal.confirmModal.modal.on('hidden.bs.modal', { modal: this }, onClose);

        Modal.confirmModal.modal.find(".modal-body > .message").html("ต้องการลบรายการนี้ใช่มั้ย?");
        Modal.confirmModal.open();
    },
    sendtoRegisterDriver: function (psg_id, drv_id) {

        Http.post(CONSTRAINTS.serviceURI.assignDRVtoPSG, {
            data: {
                "random": Math.random(),
                "action": "sendtoRegisterDriver",
                "psg_id": psg_id,
                "drv_id": drv_id,
                "username": USER_DATA.username
            },
            onSuccess: function (response) {

                if (response.status) {

                    Notification.defaultSuccess();

                    Form.assignTaskForm.form.find("button[type=submit]").hide();
                    Form.searchTaxiBeforeAssignForm.form.find("input[type=text]").prop('disabled', true);

                    Passenger.onAssignTaskSuccess();
                } else {
                    Notification.defaultError();
                }
            }
        });
    },
    sendToNoneRegisterDriver: function (psg_id, carplate, drv_carplate, prefixcarplate) {
        Http.post(CONSTRAINTS.serviceURI.assignDRVtoPSG, {
            data: {
                "random": Math.random(),
                "action": "sendToNoneRegisterDriver",
                "psg_id": psg_id,
                "carplate": carplate,
                "drv_carplate": drv_carplate,
                "prefixcarplate": prefixcarplate,
                "username": USER_DATA.username
            },
            onSuccess: function (response) {
                if (response.status) {
                    Taxi.clear();
                    Notification.defaultSuccess();
                    Modal.confirmNoneRegisterDriverModal.close();

                    Form.assignTaskForm.form.find("button[type=submit]").hide();
                    Form.searchTaxiBeforeAssignForm.form.find("input[type=text]").prop('disabled', true);
                    Passenger.onAssignTaskSuccess();
                } else {
                    Notification.defaultError();
                }
            }
        });
    },
    cancelTask: function (pass_id) {
        Http.post(CONSTRAINTS.serviceURI.deleteJob, {
            data: { "psg_id": pass_id },
            onSuccess: function (response) {
                if (response.status) {
                    Modal.confirmModal.modal.modal("hide");
                    Notification.defaultSuccess();
                } else {
                    Notification.defaultError();
                }
            }
        });
    },
    rejectTask: function (pass_id) {
        Http.post(CONSTRAINTS.serviceURI.cancelPSGDRV, {
            data: { "psg_id": pass_id },
            onSuccess: function (response) {
                if (response.status) {
                    Modal.confirmModal.modal.modal("hide");
                    Notification.defaultSuccess();

                    Modal.assignedTaskModal.modal.find(".reject-btn").hide().attr("data-ref", "");
                    Modal.assignedTaskModal.modal.find(".modal-body > .content").find(".ribbon-message").remove();

                    Modal.assignedTaskModal.close();
                } else {
                    Notification.defaultError();
                    Modal.assignedTaskModal.modal.find(".reject-btn").prop("disabled", true).attr("data-ref", "");
                }
            }
        });
    },
    removeMarker: function () {
        Map.removeLayer(this.marker);
        this.marker = {};
    },
    onAssignTaskSuccess: function () {
        Map.off("dragend", Taxi.findInCenterMap);
        Modal.assignTaskModal.close();
        Modal.assignTaskByLineModal.close();
    }
};


var Report = {
    day: 1000 * 60 * 60 * 24,
    initial: function () {
        this.summaryReportInit();
    },
    summaryReportInit: function () {

        function cb(start, end) {
            $('#summary-reportrange span').html(start.format('DD/MM/YYYY HH:mm') + ' - ' + end.format('DD/MM/YYYY HH:mm'));
        }

        cb(moment().subtract(1, 'days'), moment());

        $('#summary-reportrange').daterangepicker(CONSTRAINTS.daterangepicker.options, cb).on('apply.daterangepicker', function (ev, picker) {

            var message = $('#summary-reportrange span').html();
            Modal.reportModal.modal.find(".modal-header > .modal-message .detail").text(message);

            var daterange = {
                startTime: new Date(picker.startDate).getTime(),
                endTime: new Date(picker.endDate).getTime(),
                cgroup: USER_DATA.group
            };

            Report.countJobPerDrv(daterange);
            Report.getMostHitStartPlace(daterange);
            Report.getMostHitDestinationPlace(daterange);
            Report.getMostHitHours(daterange);
        }).on('showCalendar.daterangepicker', function (ev, picker) {

            var userAgent = window.navigator.userAgent;

            if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
                $(".daterangepicker.dropdown-menu").find(".ranges li:last-child").css({
                    display: "none"
                });
                $(".daterangepicker.dropdown-menu").find(".calendar").css({
                    display: "none"
                });
            }
        });
    },
    getCurrentDate: function () {
        return new Date();
    },
    pastDayStart: function () {
        return new Date(Report.getCurrentDate().getMonth() + 1 + "-" + Report.getCurrentDate().getDate() + "-" + Report.getCurrentDate().getFullYear()).getTime() - Report.day;
    },
    pastDayEnd: function () {
        return new Date(Report.getCurrentDate().getMonth() + 1 + "-" + Report.getCurrentDate().getDate() + "-" + Report.getCurrentDate().getFullYear()).getTime();
    },
    setDetail: function () {
        var time = Report.getCurrentDate().getHours() + ":" + (Report.getCurrentDate().getMinutes() < 10 ? '0' : '') + Report.getCurrentDate().getMinutes();
        Modal.reportModal.modal.find(".modal-header > .modal-message .detail").text(Report.getCurrentDate());
    },
    countJobPerDrv: function (daterange) {
        Http.post(CONSTRAINTS.serviceURI.countJobPerDrv, {
            loader: false,
            data: daterange,
            onSuccess: function (response) {

                var modal = Modal.reportModal.modal;

                if (response.status) {
                    modal.find("#summary-tab .jobPerDay .number").text(response.data.length);
                    modal.find("#summary-tab .jobPerHour .number").text((response.data.length / 24).toFixed(0));
                    modal.find("#summary-tab .jobPerMinute .number").text((response.data.length / 24 / 60).toFixed(2));
                    modal.find("#summary-tab .jobPerDriver .number").text((response.data.length / 150).toFixed(0));
                    modal.find("#summary-tab .jobPerCallcenter .number").text((response.data.length / 4).toFixed(0));
                } else {
                    modal.find("#summary-tab .jobPerDay .number").text(0);
                    modal.find("#summary-tab .jobPerHour .number").text(0);
                    modal.find("#summary-tab .jobPerMinute .number").text(0);
                    modal.find("#summary-tab .jobPerDriver .number").text(0);
                    modal.find("#summary-tab .jobPerCallcenter .number").text(0);
                }
            }
        });
    },
    getMostHitStartPlace: function (daterange) {
        Http.post(CONSTRAINTS.serviceURI.mostHitStartPlace, {
            loader: false,
            data: daterange,
            onSuccess: function (response) {
                if (response.status && response.data[0] !== undefined) {
                    var place = response.data[0]._id;
                    var count = response.data[0].count;
                    Modal.reportModal.modal.find("#summary-tab .most-pick .title-badge").text(place);
                    Modal.reportModal.modal.find("#summary-tab .most-pick .detail .counter").text(count);
                } else {
                    Modal.reportModal.modal.find("#summary-tab .most-pick .title-badge").text("-");
                    Modal.reportModal.modal.find("#summary-tab .most-pick .detail .counter").text("-");
                }
            }
        });
    },
    getMostHitDestinationPlace: function (daterange) {
        Http.post(CONSTRAINTS.serviceURI.mostHitDestinationPlace, {
            loader: false,
            data: daterange,
            onSuccess: function (response) {
                if (response.status && response.data[0] !== undefined) {
                    var place = response.data[0]._id;
                    var count = response.data[0].count;
                    Modal.reportModal.modal.find("#summary-tab .most-destination .title-badge").text(place == "-" ? "ไม่ระบุ" : place);
                    Modal.reportModal.modal.find("#summary-tab .most-destination .detail .counter").text(count);
                } else {
                    Modal.reportModal.modal.find("#summary-tab .most-destination .title-badge").text("-");
                    Modal.reportModal.modal.find("#summary-tab .most-destination .detail .counter").text("-");
                }
            }
        });
    },
    getMostHitHours: function (daterange) {
        Http.post(CONSTRAINTS.serviceURI.mostHitHours, {
            loader: false,
            data: daterange,
            onSuccess: function (response) {
                if (response.status && response.data[0] !== undefined) {
                    var diffTime = 7;
                    var hour = response.data[0]._id.hour + diffTime;
                    var total = response.data[0].total;

                    Modal.reportModal.modal.find("#summary-tab .peak-hour .title-badge").text(hour + ":00 - " + (hour + 1) + ":00");
                    Modal.reportModal.modal.find("#summary-tab .peak-hour .detail .counter").text(total);
                } else {
                    Modal.reportModal.modal.find("#summary-tab .peak-hour .title-badge").text("-");
                    Modal.reportModal.modal.find("#summary-tab .peak-hour .detail .counter").text("-");
                }
            }
        });
    },
    getSearchFinishList: function (daterange) {
        Http.post(CONSTRAINTS.serviceURI.searchFinishList, {
            loader: false,
            data: {
                "keyword": "สุนี"
            },
            onSuccess: function (response) {
                console.log(response);
            }
        });
    },
    openReportWindow: function () {
        var left = (window.screen.availWidth - 1400) / 2;
        window.open(window.location.origin + "/report", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=" + left + ",width=1400,height=800");
    }
};

var Socket = {
    initial: function () {

        Socket.validate();

        var origin = window.location.origin;

        Socket = io("/" + USER_DATA.group);

        Socket.on("addjoblist", function (data) {
            if (data.jobtype == "QUEUE") {
                DataTable.queueTaskTable.addRow(data);
            }
            else if (data.jobtype == "ADVANCE") {
                DataTable.advanceTaskTable.addRow(data);
            }

            Sound.play("sounds-incoming-call");
        });

        Socket.on("deletejob", function (data) {

            if ($.inArray(data.status, ["DPENDING", "BROADCAST", "DEPENDING_REJECT", "DPENDING_LINE", "DEPENDING_TIMEOUT"]) > -1) {
                var row = DataTable.pendingTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                row !== undefined && DataTable.pendingTaskTable.datatable.row(row).remove().draw(false);
                DataTable.pendingTaskTable.setIndicator();
            }
            else if (data.jobtype == "QUEUE") {
                var row = DataTable.queueTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                row !== undefined && DataTable.queueTaskTable.datatable.row(row).remove().draw(false);
                DataTable.queueTaskTable.setIndicator();
            }
            else if (data.jobtype == "ADVANCE") {
                var row = DataTable.advanceTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                row !== undefined && DataTable.advanceTaskTable.datatable.row(row).remove().draw(false);

                var row = DataTable.queueTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                row !== undefined && DataTable.queueTaskTable.datatable.row(row).remove().draw(false);
            }
        });

        Socket.on("inline edit", function (data) {
            LOCAL_STATE.push(data);
            DataTable.lockLine(data);
        });

        Socket.on("unlock line", function (data) {
            LOCAL_STATE_CONTROL.remove(data);
            DataTable.unlockLine(data);
        });

        Socket.on("editjoblist", function (data) {

            if ($.inArray(data.status, ["DPENDING", "BROADCAST", "DEPENDING_REJECT", "DPENDING_LINE", "DEPENDING_TIMEOUT"]) > -1) {
                Passenger.getData(data.psg_id, function (response) {
                    if (response.status) {
                        DataTable.pendingTaskTable.updateRow(response.psg_data);
                        DataTable.pendingTaskTable.setIndicator();
                    }
                });
            }
            else if (data.status == "ASSIGNED") {
                Passenger.getData(data.psg_id, function (response) {
                    if (response.status) {
                        DataTable.assignedTaskTable.updateRow(response.psg_data);

                        if (response.psg_data.cccomment !== "") {
                            TaskComment.add(response.psg_data);
                        }
                        else {
                            TaskComment.remove(response.psg_data);
                        }
                    }
                });
            }
            else if (data.jobtype == "QUEUE") {
                DataTable.queueTaskTable.updateRow(data);
            }
            else if (data.jobtype == "ADVANCE") {
                DataTable.advanceTaskTable.updateRow(data);
            }
        });

        Socket.on("assigning task", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            LOCAL_STATE.push(data);
            DataTable.lockLine(data);
        });

        Socket.on("assigndrvtopsg", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            if ($.inArray(data.status, ["DPENDING", "DEPENDING_REJECT", "DPENDING_LINE", "BROADCAST","DEPENDING_TIMEOUT"]) > -1) {

                var row = DataTable.queueTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                row !== undefined && DataTable.queueTaskTable.datatable.row(row).remove().draw(false);

                var row = DataTable.pendingTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                row !== undefined && DataTable.pendingTaskTable.datatable.row(row).remove().draw(false);

                DataTable.pendingTaskTable.addRow(data);
                DataTable.queueTaskTable.setIndicator();
                DataTable.pendingTaskTable.setIndicator();
            }
            else if (data.status == "ASSIGNED") {

                if (data.drv_carplate.slice(-1) == "+") {
                    var row = DataTable.pendingTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                    row !== undefined && DataTable.pendingTaskTable.datatable.row(row).remove().draw(false);
                    DataTable.pendingTaskTable.setIndicator();
                }
                else if (data.drv_carplate.slice(-1) != "+") {
                    var row = DataTable.queueTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                    row !== undefined && DataTable.queueTaskTable.datatable.row(row).remove().draw(false);
                    DataTable.pendingTaskTable.setIndicator();

                    var row = DataTable.pendingTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
                    row !== undefined && DataTable.pendingTaskTable.datatable.row(row).remove().draw(false);
                    DataTable.pendingTaskTable.setIndicator();
                }

                DataTable.assignedTaskTable.addRow(data);
            }
        });

        Socket.on("cancelpsgdrv", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            var row = DataTable.assignedTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
            row !== undefined && DataTable.assignedTaskTable.datatable.row(row).remove().draw();

            DataTable.queueTaskTable.addRow(data);
            DataTable.queueTaskTable.setIndicator();
        });

        Socket.on("gotdispatchaction", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            Passenger.getData(data.psg_data._id, function (response) {
                if (response.status) {
                    if (response.psg_data.status == "ASSIGNED") {

                        if(Modal.assignTaskModal.modal.is(':visible')) {
                            Modal.assignTaskModal.showPopupTaskAssigned(response.psg_data._id, response.drv_data);
                        }

                        var row = DataTable.pendingTaskTable.$table.find('[data-ref="' + response.psg_data._id + '"]')[0];
                        row !== undefined && DataTable.pendingTaskTable.datatable.row(row).remove().draw(false);
                        DataTable.pendingTaskTable.setIndicator();

                        DataTable.assignedTaskTable.addRow(response.psg_data);
                    }
                    else if (response.psg_data.status == "DEPENDING_TIMEOUT") {
                        DataTable.pendingTaskTable.updateRow(response.psg_data);
                        DataTable.pendingTaskTable.setIndicator();
                    }
                    else if (response.psg_data.status == "DEPENDING_REJECT") {
                        var row = DataTable.assignedTaskTable.$table.find('[data-ref="' + response.psg_data._id + '"]')[0];
                        row !== undefined && DataTable.assignedTaskTable.datatable.row(row).remove().draw();
                        DataTable.pendingTaskTable.addRow(response.psg_data);
                        DataTable.pendingTaskTable.setIndicator();
                    }
                }
            });
        });

        Socket.on("driverCancelCallCenter", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            var row = DataTable.assignedTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
            row !== undefined && DataTable.assignedTaskTable.datatable.row(row).remove().draw();

            Passenger.getData(data._id, function (response) {
                if (response.status) {
                    DataTable.pendingTaskTable.addRow(response.psg_data);
                    DataTable.pendingTaskTable.setIndicator();
                }
            });
        });

        Socket.on("driverEndTask", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            var row = DataTable.assignedTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
            row !== undefined && DataTable.assignedTaskTable.datatable.row(row).remove().draw();
        });

        Socket.on("closeCallcenterJob", function (data) {
            Modal.assignTaskModal.modal.is(':visible') && Taxi.findInCenterMap();
            var row = DataTable.assignedTaskTable.$table.find('[data-ref="' + data._id + '"]')[0];
            row !== undefined && DataTable.assignedTaskTable.datatable.row(row).remove().draw();
        });

        Socket.on("update state", function (data) {
            data.filter(function (task) {
                LOCAL_STATE.push(task);
                DataTable.lockLine(task);
            });
        });

        Socket.on('voice', function (data) {
            var blob = new Blob([data], { 'type': 'audio/ogg; codecs=opus' });
            var audio = document.createElement('audio');
            audio.src = window.URL.createObjectURL(blob);
            audio.play();
        });

        Socket.on('driver logged in', function (data) {
            LiveBeam.toListView(data);
        });

        Socket.on('driver logged out', function (data) {
            Modal.driverListChatModal.modal.find("[data-ref='" + data + "']").remove();
        });

        Socket.on('request approve', function (data) {
            NotificationRequestApprove.getPendingStatus();
        });

        Socket.on('driver approved', function (data) {
            NotificationRequestApprove.getPendingStatus();
        });

        Socket.on("connect", function (data) {

            if (Socket.hasLostConnect !== undefined && Socket.hasLostConnect) {

                delete Socket.hasLostConnect;

                DataTable.updateAll();

                $('body').find("> .connect_lost_message").remove();
            }
        });

        Socket.on("error", function (data) {
            console.log(new Date());
            console.log(data);
        });

        Socket.on('disconnect', function () {
            Socket.hasLostConnect = true;
            Http.onConnectionLost();
        });

        return Socket;
    },
    validate: function () {
        window.addEventListener("beforeunload", function (e) {
            Socket.emit('window beforeunload', USER_DATA);
        }, false);
    }
}


var Sound = {
    initial: function () {
        var html = $(".sound-notification-tab").find(".js-switch")[0];
        var s = new Switchery(html, {
            size: "small"
        });

        $(html).on("click", function () {
            var checked = Sound.control.is(":checked");

            if (checked && !Sound.control.is(":checked")) {
                Sound.control.parent().find(".switchery").click();
            }
            else if (!checked && Sound.control.is(":checked")) {
                Sound.control.parent().find(".switchery").click();
            }
        });
    },
    control: $(".sound-notification-tab").find(".js-switch"),
    play: function (soundName) {
        var sound = document.getElementById(soundName);
        this.control.is(":checked") && sound.play();
    }
}


var TaskComment = {
    updateAll: function () {

        var AlertContainer = $("#navbar .alert-tab").find(".panel-body .list-group");
        AlertContainer.empty();

        var oTable = DataTable.assignedTaskTable.datatable;
        for (var i = 0; i < oTable.rows()[0].length; i++) {
            if (oTable.rows(i).data()[0][9] !== undefined && oTable.rows(i).data()[0][9].cccomment != "") {
                var taskData = oTable.rows(i).data()[0][9];

                var html = '<li class="list-group-item" data-ref="' + taskData._id + '" onclick="Modal.assignedTaskEditableModal.showDatail(\'' + taskData._id + '\');">';
                html += '<p class="message">' + taskData.cccomment + '</p>';
                html += '<div class="pull-left">';
                html += '<span class="datetime">' + Util.getCurrentTime(taskData.dassignedjob) + ' น.</span>';
                html += '<span class="detail">';
                html += '' + taskData.curaddr + ' <i>ไป</i> ' + taskData.destination + '';
                html += '</span>';
                html += '</div>';
                html += '<div class="pull-right">';
                html += '<span class="sender">' + taskData.assigningby + '</span>';
                html += '</div>';
                html += '<div class="clearfix"></div>';
                html += '</li>';

                AlertContainer.append(html);
            }
        }

        $("#navbar .alert-tab").find(".dropdown-toggle .label").text(AlertContainer.find(".list-group-item").size());
    },
    add: function (data) {

        var AlertContainer = $("#navbar .alert-tab").find(".panel-body .list-group");
        if (AlertContainer.find("[data-ref='" + data._id + "']").find(".message").html() == data.cccomment) {
            return false;
        }

        var html = '<li class="list-group-item" data-ref="' + data._id + '" onclick="Modal.assignedTaskEditableModal.showDatail(\'' + data._id + '\');">';
        html += '<p class="message">' + data.cccomment + '</p>';
        html += '<div class="pull-left">';
        html += '<span class="datetime">' + Util.getCurrentTime(data.dassignedjob) + ' น.</span>';
        html += '<span class="detail">';
        html += '' + data.curaddr + ' <i>ไป</i> ' + data.destination + '';
        html += '</span>';
        html += '</div>';
        html += '<div class="pull-right">';
        html += '<span class="sender">' + data.assigningby + '</span>';
        html += '</div>';
        html += '<div class="clearfix"></div>';
        html += '</li>';

        AlertContainer.find("[data-ref='" + data._id + "']").remove();
        AlertContainer.append(html);

        $("#navbar .alert-tab").find(".dropdown-toggle .label").text(AlertContainer.find(".list-group-item").size());

        Sound.play("sounds-925-hand-bell");
    },
    remove: function (data) {
        var AlertContainer = $("#navbar .alert-tab").find(".panel-body .list-group");
        AlertContainer.find("[data-ref='" + data._id + "']").remove();
        $("#navbar .alert-tab").find(".dropdown-toggle .label").text(AlertContainer.find(".list-group-item").size());
    }
};


var Taxi = {
    line: {},
    active: {},
    circle: null,
    layer: null,
    initial: function () {

        this.circle = L.circle();
        this.layer = L.layerGroup();
        Map && Map.addLayer(this.layer);
    },
    addToMap: function (TaxiList, options, clear) {

        if (clear == undefined || clear) {
            Taxi.clear();
        }

        $(TaxiList).each(function (index, taxi) {

            if (taxi.lat == undefined || taxi.lng == undefined) {
                return;
            }

            if(taxi.status === "OFF") {
                return;
            }

            var carplateStyle = L.divIcon({
                className: 'carplate-pin ' + taxi.status.toLocaleLowerCase(),
                html: '<div class="message">' + taxi.license_plate + '<div class="arrow"></div></div>',
                iconAnchor: [40, 34],
                iconSize: null
            });

            var taxiMarker = L.marker([taxi.lat, taxi.lng], {
                riseOnHover: true,
                icon: carplateStyle
            });

            taxi.imgface = Taxi.getImageFace(taxi.imgface);

            var createStatusText = function(status) {

                if ( status === "ON" ) {
                    return '<span class="label label-success">พร้อมรับงาน</span>';
                }
                else if ( status === "OFF" ) {
                    return '<span class="label label-default">ไม่ได้เปิดแอพฯ</span>';
                }
                else {
                    return '<span class="label label-warning">ไม่ว่าง</span>';
                }
            }

            var getExtendFullname = function(name) {
                return name.length > 18 ? ".." : "";
            }

            var HTMLPopup = '<div class="row">';
            HTMLPopup += '<div class="driver-img-wrapper col-md-6">';
            HTMLPopup += '<img class="driver-img" src="' + CONSTRAINTS.driverImageURL + taxi.imgface + '" />';
            if (options == undefined && taxi.status.toLocaleUpperCase() == "ON") {
                HTMLPopup += '<button class="btn choose" onclick="Taxi.goToPassenger();">เลือกคนนี้</button>';
            }
            HTMLPopup += '</div>';
            HTMLPopup += '<div class="driver-info col-md-6">';
            HTMLPopup += '<p class="full_name value" data-toggle="tooltip" title="' + taxi.full_name + '" >' + taxi.full_name.slice(0, 17) + getExtendFullname(taxi.full_name) + '</p>';
            HTMLPopup += '<p class="caption">เบอร์โทรติดต่อ</p>';
            HTMLPopup += '<p class="tel value">' + Util.toReadablePhoneNumber(taxi.tel.toString()) + '</p>';
            HTMLPopup += '<div class="pull-left carplate-wrap">';
            HTMLPopup += '<p class="caption">ทะเบียน</p>';
            HTMLPopup += '<p class="license_plate value">' + taxi.license_plate + '</p>';
            HTMLPopup += '</div>';
            HTMLPopup += '<div class="pull-left status-wrap">';
            HTMLPopup += '<p class="caption">สถานะ</p>';
            HTMLPopup += '<p class="status value">' + createStatusText(taxi.status) + '</p>';
            HTMLPopup += '</div>';
            HTMLPopup += '</div>';
            HTMLPopup += '</div>';

            if (taxi.status.toLocaleUpperCase() == "BROKEN") {
                var label = '<div class="label-message-status"><h3 class="title">รถเสีย</h3><a class="call-message" href="#" onclick="Taxi.toggleBrokenDetail(this);">';
                label += '<span class="txt-show">แสดง</span><span class="txt-hide">ซ่อน</span>รายละเอียด';
                label += '</a>';
                if (Object.prototype.toString.call(taxi.brokenname) === '[object Array]') {
                    label += '<p class="list"><i>อาการ</i><br>' + taxi.brokenname.toString() + '</p>';
                }
                label += '<p class="detail"><i>รายละเอียด</i><br>' + taxi.brokendetail + '</p>';
                label += '</div>';
                HTMLPopup += label;
            }

            taxiMarker.bindPopup(HTMLPopup, {
                offset: L.point(0, 5),
                className: 'driver-popup'
            }).on('popupopen', function (e) {
                Taxi.active = taxiMarker;
                $(this.getPopup()._contentNode).find('p.full_name').tooltip();
            }).on('popupclose', function () {
                Taxi.active = {};
            });

            taxiMarker.options.taxi_info = taxi;
            Taxi.layer.addLayer(taxiMarker);
        });
    },
    getImageFace: function (imgface) {
        if (imgface == undefined || imgface == "") {
            imgface = "assets/img/thumbnail_photo.jpg";
        } else {
            imgface = "/image/driver/" + imgface;
        }

        return imgface;
    },
    filter: function (status) {

        var taxi_list = LOCAL_DATA.taxi;
        if (status !== 'all' && LOCAL_DATA.taxi !== undefined) {
            taxi_list = LOCAL_DATA.taxi.filter(function (taxi) {
                return taxi.status.toLocaleLowerCase() == status.toLocaleLowerCase();
            });
        }

        this.addToMap(taxi_list);
    },
    onFilterClick: function (filter, event) {

        var isActive = $(filter).hasClass('active');
        $('.filter-marker.active').removeClass('active');

        !isActive && $(filter).addClass('active');

        var status = $(filter).attr('data-status');
        var count = $('.filter-marker.active').size();

        if (count == 0) { status = "all"; }
        this.filter(status);
    },
    findNearestPassenger: function (pass_latlng, pass_info) {

        if (pass_latlng.length > 0) {

            Notification.removeAll();

            this.circle.setLatLng(pass_latlng).setRadius(5000);
            Map.fitBounds(this.circle.getBounds());
            Passenger.addToMap(pass_latlng, pass_info);

            var data = {
                "curlat": pass_info.curloc[0],
                "curlng": pass_info.curloc[1],
                "status": "ON",
                "radian": 5000
            };

            var actionFind = function () {
                Http.post(CONSTRAINTS.serviceURI.searchDriver, {
                    loader: false,
                    data: data,
                    onSuccess: function (response) {
                        if (response.status) {

                            var taxi_list = [];

                            $(response.data).each(function (index, driver) {
                                taxi_list.push({
                                    "_id": driver._id,
                                    "license_plate": (driver.prefixcarplate || "") + " " + driver.carplate,
                                    "prefixcarplate": driver.prefixcarplate,
                                    "carplate": driver.carplate,
                                    "lat": driver.curloc[1],
                                    "lng": driver.curloc[0],
                                    "status": "ON",
                                    "imgface": driver.imgface,
                                    "full_name": driver.fname + ' ' + driver.lname,
                                    "tel": driver.phone,
                                    "rating": "5"
                                });
                            });

                            Taxi.addToMap(taxi_list);
                        } else {
                            Taxi.clear();
                        }
                    }
                });
            };

            actionFind();
        }
        else {
            Notification.removeAll();
            this.layer.clearLayers();
            Passenger.removeMarker();
            App.setDefaultPosition();
            setTimeout(function () {
                Notification.show({
                    sticky: false,
                    $class: "primary",
                    title: "ไม่ทราบตำแหน่งที่แน่นอนของผู้โดยสาร",
                    text: '',
                    position: "bottom-center"
                });
            }, 700);
        }
    },
    findInCenterMap: function () {

        var center = Map.getCenter();

        Http.post(CONSTRAINTS.serviceURI.searchDriver, {
            loader: false,
            data: {
                "curlat": center.lat,
                "curlng": center.lng
            },
            onSuccess: function (response) {
                if (response.status) {

                    var taxi_list = [];

                    $(response.data).each(function (index, driver) {
                        taxi_list.push({
                            "_id": driver._id,
                            "license_plate": (driver.prefixcarplate || "") + " " + driver.carplate,
                            "prefixcarplate": driver.prefixcarplate,
                            "carplate": driver.carplate,
                            "lat": driver.curlat,
                            "lng": driver.curlng,
                            "status": driver.status,
                            "imgface": driver.imgface,
                            "full_name": driver.fname + ' ' + driver.lname,
                            "tel": driver.phone,
                            "rating": "5"
                        });
                    });

                    Taxi.taxiList = taxi_list;
                    Taxi.addToMap(taxi_list);
                } else {
                    Taxi.clear();
                }
            }
        });
    },
    findByCarplate: function (plateNumber) {
        var currentTaxi = null;
        Taxi.layer.eachLayer(function (taxi) {
            if (taxi.options.taxi_info.carplate == plateNumber) {
                currentTaxi = taxi;
                return false;
            }
        });

        if (currentTaxi !== null) {
            Notification.remove();
            Monitoring.toggleFilterControl($(".taxi-counter .puller"));
            setTimeout(function () {
                Map.invalidateSize();
                currentTaxi.openPopup();
                Map.setView([currentTaxi.options.taxi_info.lat, currentTaxi.options.taxi_info.lng], 17, { animate: false });
            }, 100);
        } else {
            Notification.remove();
            Notification.show({
                className: "warning",
                title: 'ไม่พบแท็กซี่ดังกล่าว',
            });
        }
    },
    goToPassenger: function () {

        var driver = Taxi.active.options.taxi_info;
        var passenger_id = Modal.assignTaskModal.modal.find("#pass_id").val();        
        var data = {
            "random": Math.random(),
            "action": "goToPassenger",
            "psg_id": passenger_id,
            "drv_id": driver._id,
            "username": USER_DATA.username
        };

        Http.post(CONSTRAINTS.serviceURI.assignDRVtoPSG, {
            data: data,
            onSuccess: function (response) {
                if (response.status) {

                    Preloader.show();
                    setTimeout(function () {

                        var html = Taxi.active.getPopup().getContent();
                        html = html.replace("choose", "choose hide");
                        Taxi.active.setPopupContent(html);
                        $(html).find(".btn.choose").hide();

                        Taxi.layer.eachLayer(function (marker) {
                            if (marker._leaflet_id != Taxi.active._leaflet_id && !(marker instanceof L.Polyline)) {
                                Taxi.layer.removeLayer(marker);
                            }
                        });

                        Modal.assignTaskModal.modal.focus();
                        Map.off("dragend", Taxi.findInCenterMap);
                        Form.assignTaskForm.form.find("input[type=hidden]").val("");
                        Form.assignTaskForm.form.find("button[type=submit]").hide();
                        Form.searchTaxiBeforeAssignForm.form.find("#car-plate-inp").prop("disabled", true);
                        Form.searchTaxiBeforeAssignForm.form.find("#car-plate-inp").val(response.drv_data.carplate);

                        Preloader.hide();
                        Notification.remove();
                        Notification.defaultSuccess();
                        Passenger.onAssignTaskSuccess();
                    }, 700);
                } else {
                    Notification.defaultError();
                }
            }
        });
    },
    assignByLineApp: function (passenger_id, drv_carplate) {

        Preloader.show();

        var data = {
            "random": Math.random(),
            "action": "assignByLineApp",
            "psg_id": passenger_id,
            "drv_carplate": drv_carplate,
            "lineconfirm": "Y",
            "username": USER_DATA.username
        };

        Http.post(CONSTRAINTS.serviceURI.assignDRVtoPSG, {
            data: data,
            onSuccess: function (response) {
                if (response.status) {
                    Preloader.hide();
                    Notification.remove();
                    Notification.defaultSuccess();
                    Passenger.onAssignTaskSuccess();
                    Modal.assignTaskByLineModal.modal.find(".modal-footer").hide();
                } else {
                    Notification.defaultError();
                }
            }
        });
    },
    checkStatus: function (data, callback) {
        Http.post(CONSTRAINTS.serviceURI.checkDRVStatus, {
            loader: false,
            data: data,
            onSuccess: callback
        });
    },
    cancelFind: function () {
        Map.removeLayer(this.circle);
    },
    clear: function () {
        this.layer.clearLayers();
    },
    toggleStatus: function (status, show) {
        if (show) {

            if( Monitoring.taxi !== null ) {
                var taxi = Monitoring.taxi.filter(function (taxi) {
                    return taxi.status == status;
                });
                taxi.length > 0 && Taxi.addToMap(taxi, { monitoring: true }, false);
            }
        }
        else {
            Taxi.layer.eachLayer(function (taxi) {
                if (taxi.options.taxi_info.status == status) {
                    Taxi.layer.removeLayer(taxi);
                }
            });
        }
    },
    ghostTaxi: function (number) {

        var taxi_list = [];
        var bounds = this.circle.getBounds();

        if (number === undefined || number == 0) {
            number = 1;
        }

        for (var i = 1; i <= number; i++) {
            var latlng = Taxi.getRandomLatLng(bounds);

            taxi_list.push({
                "_id": new Date().getTime(),
                "license_plate": "1กล 2254",
                "lat": latlng.lat,
                "lng": latlng.lng,
                "status": "ON",
                "image_driver": "",
                "full_name": "กนกกร",
                "tel": "082-456-8899",
                "rating": "5"
            });
        };

        return taxi_list;
    },
    getRandomLatLng: function (bounds) {
        southWest = bounds.getSouthWest(),
        northEast = bounds.getNorthEast(),
        lngSpan = northEast.lng - southWest.lng,
        latSpan = northEast.lat - southWest.lat;

        return new L.LatLng(
            southWest.lat + latSpan * Math.random(),
            southWest.lng + lngSpan * Math.random());
    },
    toggleBrokenDetail: function (elem) {
        $(elem).closest('.label-message-status').toggleClass('open');
    }
};


var LandMark = {
    layer: null,
    initial: function () {
        this.layer = L.layerGroup();
        Map && Map.addLayer(this.layer);
    },
    getAll: function () {
        Http.post(CONSTRAINTS.serviceURI.getPOIandParking, {
            onSuccess: function (response) {
                if (response.status) {
                    LandMark.addToMap(response.data);
                } else {
                    //Notification.defaultError();
                }
            }
        });
    },
    clear: function () {
        this.layer.clearLayers();
    },
    addToMap: function (locations) {

        LandMark.clear();

        $(locations).each(function (index, location) {

            var icon = "";
            var className = "";
            if (location.poitype == 1) {
                className = "landmark";
                icon = '<i class="material-icons">star</i>';
            }
            else if (location.poitype) {
                className = "parking";
                icon = '<i class="material-icons">local_parking</i>';
            }

            var positionStyle = L.divIcon({
                className: 'position-pin ' + className,
                html: '<div class="wrapper"><div class="icon">' + icon + '</div><div class="text">' + location.name + '</div><div class="arrow"></div></div>',
            });

            var landMarker = L.marker([location.curloc[1], location.curloc[0]], {
                riseOnHover: true,
                icon: positionStyle
            });

            landMarker.options.landMarkerInfo = location;
            LandMark.layer.addLayer(landMarker);
        });
    },
    toggleDisplay: function (display) {
        display ? this.getAll() : this.clear();
    }
}


var NotificationRequestApprove = {
    init: function() {
        this.getPendingStatus();
    },
    notification: $(".waitapprove-tab"),
    getPendingStatus: function() {
        $.post("/service/ubeam/getdriverpendinglist", function(response) {
            if(response.status) {
                NotificationRequestApprove.setCount(response.data.length);
                if(response.data.length > 0) {
                    NotificationRequestApprove.show();
                } else {
                    NotificationRequestApprove.hide();
                }
            } else {
                if(response.msg === 'No data') {
                    NotificationRequestApprove.hide();
                }
            }
        });
    },
    setCount: function(number) {
        var _this = NotificationRequestApprove;
        _this.notification.find(".badge").text(number);
    },
    show: function() {
        var _this = NotificationRequestApprove;
        _this.notification.removeClass("hide");
    },
    hide: function() {
        var _this = NotificationRequestApprove;
        _this.notification.addClass("hide");
    }
};


var POIRecommended = {

    shuffle: function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    },

    loadToAutocompleteLookup: function () {
        Http.post(CONSTRAINTS.serviceURI.getPOIRecommended, {
            data: {
                "cgroup": USER_DATA.group
            },
            onSuccess: function (response) {
                if (response.status) {

                    if (response.data.length > 0) {
                        localStorage.setItem("poi_recommended", JSON.stringify(response.data));
                    }
                }
            }
        });
    },

    setUpLookup: function () {

        var phoneInput = Form.createTaskForm.form.find("#pass-phone-inp");
        phoneInput.keyup(function(event) {
            
            var phoneNo = this.value;
            if(phoneNo.length < 9 || phoneNo.length > 10) {
                return;
            }

            if (phoneNo) {
                POIRecommended.updateLookup(phoneNo);
            } 
        });

        var input = Form.createTaskForm.form.find("#pass-from-inp, #pass-dest-inp");
        input.on("focusin", function (event) {

            var instance = $(this).data('autocomplete');
            var phoneNo = Form.createTaskForm.form.find("#pass-phone-inp").val();

            if (!phoneNo) {

                var TempPois = [];

                var TempPois = $.map(Form.createTaskForm.loadPassengerStartPOI(), function (dataItem) {
                    if (!dataItem) { return; }
                    return {
                        value: dataItem.value,
                        data: dataItem.data,
                        type: "TEMP"
                    };
                });

                var garagePois = $.map(JSON.parse(localStorage.getItem("poi_recommended")), function (dataItem) {
                    if (!dataItem) { return; }
                    return {
                        value: dataItem.parkingname,
                        data: dataItem,
                        ID: dataItem._id,
                        type: "LOCATION"
                    };
                });

                garagePois = POIRecommended.shuffle(garagePois);

                instance.options.lookup = TempPois.concat(garagePois);
            }
        });
    },

    updateLookup: function (phoneNo) {

        $.post(CONSTRAINTS.serviceURI.getPOIsByPhone + phoneNo, function (response) {
            if (response.status && response.data.length > 0) {

                var PassengerPois = [];

                var PassengerPois = $.map(response.data, function (dataItem) {
                    if (!dataItem) { return; }
                    return {
                        value: dataItem.name,
                        data: dataItem,
                        ID: dataItem._id,
                        type: "USER"
                    };
                });

                var garagePois = $.map(JSON.parse(localStorage.getItem("poi_recommended")), function (dataItem) {
                    if (!dataItem) { return; }
                    return {
                        value: dataItem.parkingname,
                        data: dataItem,
                        ID: dataItem._id,
                        type: "LOCATION"
                    };
                });

                garagePois = POIRecommended.shuffle(garagePois);

                Form.createTaskForm.form.find("#pass-from-inp").data('autocomplete').options.lookup = PassengerPois.concat(garagePois);
                Form.createTaskForm.form.find("#pass-dest-inp").data('autocomplete').options.lookup = PassengerPois.concat(garagePois);
            } else {

                var TempPois = [];

                var TempPois = $.map(Form.createTaskForm.loadPassengerStartPOI(), function (dataItem) {
                    if (!dataItem) { return; }
                    return {
                        value: dataItem.value,
                        data: dataItem.data,
                        type: "TEMP"
                    };
                });

                var garagePois = $.map(JSON.parse(localStorage.getItem("poi_recommended")), function (dataItem) {
                    if (!dataItem) { return; }
                    return {
                        value: dataItem.parkingname,
                        data: dataItem,
                        ID: dataItem._id,
                        type: "LOCATION"
                    };
                });

                garagePois = POIRecommended.shuffle(garagePois);

                Form.createTaskForm.form.find("#pass-from-inp").data('autocomplete').options.lookup = TempPois.concat(garagePois);
                Form.createTaskForm.form.find("#pass-dest-inp").data('autocomplete').options.lookup = TempPois.concat(garagePois);
            }
        });
    }
}

var Util = {
    getCurrentTime: function (date_string) {

        var addZero = function (i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        var d = date_string == undefined ? new Date() : new Date(date_string);
        var h = addZero(d.getHours());
        var m = addZero(d.getMinutes());
        var s = addZero(d.getSeconds());

        return h + ":" + m;
    },
    getFullDate: function (time) {
        var d = new Date(time);
        var da = d.getDate();
        var mon = d.getMonth() + 1;
        var yr = d.getFullYear();
        return da + "/" + mon + "/" + yr;
    },
    diffTime: function (pass, future) {
        return (future - pass) / 1000 / 60
    },
    executeFunctionByName: function (functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    },
    getTimeStatus: function (time) {
        var diff = Util.diffTime(new Date(time).getTime(), new Date().getTime());
        var status = "new";
        if (diff < -1) {
            status = "book";
        }
        else if (diff > 0 && diff <= 1) {
            status = "new";
        }
        else if (diff > 1 && diff <= 5) {
            status = "normal";
        }
        else if (diff > 5) {
            status = "long";
        }
        return status;
    },
    getDiffTime: function (time) {

        // diff in minute time.
        var diff = Util.diffTime(new Date(time).getTime(), new Date().getTime());

        var status = {};
        // less than 1 hour.
        if (diff < 60) {
            var result = Math.floor(diff);
            status.time = (result == 0 || result < 0) ? "นาที" : result + " น.";
            status.unit = "ที่แล้ว";
        }
            // within 24 hour or 1 day
            else if (diff >= 60 && diff <= 1440) {
            //status.time = "+" + (diff / 60).toFixed(2);
            status.time = Math.floor((Math.abs(new Date(time).getTime() - new Date().getTime()) / 36e5).toFixed(2)) + " ชม.";
            status.unit = "ที่แล้ว";
        }
            // more than 1440 minute or 24 hour or 1 day
            else if (diff > 1440) {
                status.time = "นานมาก";
                status.unit = "แล้ว";
            }
            return status;
        },
        toReadablePhoneNumber: function(phoneNumber) {

            if(typeof phoneNumber === "number") {
                phoneNumber = phoneNumber.toString();
            }

            if(phoneNumber.length === 10) {

                var newNumber = phoneNumber.substr(0, 3) + "-";
                newNumber += phoneNumber.substr(3, 3) + "-";
                newNumber += phoneNumber.substr(6, 4);

                return newNumber; 
            }

            else if (phoneNumber.length === 9 && phoneNumber.substr(0,2) === "02") {

                var newNumber = phoneNumber.substr(0, 2) + "-";
                newNumber += phoneNumber.substr(2, 3) + "-";
                newNumber += phoneNumber.substr(5, 4);

                return newNumber; 
            }

            else if (phoneNumber.length === 9 && phoneNumber.substr(0,2) !== "02") {

                var newNumber = phoneNumber.substr(0, 3) + "-";
                newNumber += phoneNumber.substr(3, 3) + "-";
                newNumber += phoneNumber.substr(6, 3);

                return newNumber; 
            }

            else {
                return phoneNumber;
            }
        }
    };


    (function () {

        App.init();

        setInterval(function () {
            DataTable.queueTaskTable.updateTimeStatus();
            DataTable.pendingTaskTable.updateTimeStatus();
        }, 5000);

        DataTable.advanceTaskTable.intervalUpdate();
    })();


    (function ($) {
        jQuery.fn.inlineEditable = function (data) {

            var defaults = {};

            var settings = $.extend({}, defaults, data);

            initialize(this);

            function initialize(target) {
                target[0].addEventListener("dblclick", ondblclick);
            }

            function ondblclick() {

                var form = document.createElement('form');
                var input = document.createElement('input');
                var td = this;
                input.setAttribute("type", "text");
                input.setAttribute("originText", td.innerText)
                input.value = td.innerText;
                input.addEventListener('blur', function () { onblur(td, form, this) }, false);

                form.appendChild(input);
                form.addEventListener('submit', function () { onsubmit(td, this) }, false);

                while (td.firstElementChild) {
                    td.removeChild(td.firstElementChild);
                }
                td.innerText = "";

                td.appendChild(form);
                td.removeEventListener('dblclick', ondblclick, false);

                input.focus();
            }

            function onblur(td, form, input) {

                var value = input.value;
                var originText = input.getAttribute("originText");

                if (originText !== value) {

                    var data = {};
                    var value = input.value;
                    var name = td.getAttribute('data-field-name');
                    var id = td.parentNode.getAttribute("data-ref");

                    data[name] = value;
                    Passenger.editData(id, data);
                }

                while (td.firstElementChild) {
                    td.removeChild(td.firstElementChild);
                }

                td.innerText = value;
                td.addEventListener('dblclick', ondblclick);
            }

            function onsubmit(td, form) {

                event.preventDefault();
                form.firstChild.blur();
            }

            function isFunction(func) {
                return this.settings !== undefined
                && func !== undefined
                && typeof func == "function";
            }
        }
    })(jQuery);