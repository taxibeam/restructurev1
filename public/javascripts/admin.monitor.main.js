define(["define/AdminMonitorAppValues"], function() {

	require([

		"libs/ApplicationActivities",
		"libs/HobbitMap",

		"classie/TaxiDriver",
		"modules/TaxiLayer",

		"modules/AdminMonitorApp",
		"services/UbeamTPServiceListener",
		"modules/iOSwitchery"

		], function(UbeamTPServiceListener) {

			var appValues = new defineValues(AdminMonitorAppValues);
			new AdminMonitorApp(appValues);

		});
});