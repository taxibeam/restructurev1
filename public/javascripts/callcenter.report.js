var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
	serviceURI: {
		psgCallLog: serviceBaseURI + '/service/log/PsgCallLog',
		joblistLog: serviceBaseURI + '/service/ubeam/JoblistLog',
        getMobileLog: serviceBaseURI + '/service/log/getMobileLog',
        getJobListLogWait: serviceBaseURI + '/service/ubeam/getJobListLogWait',

        getJobListLogPick: serviceBaseURI + '/service/ubeam/getJobListLogPick',
        getJobListLogAccept: serviceBaseURI + '/service/ubeam/getJobListLogAccept',
        getJobListLogDrop: serviceBaseURI + '/service/ubeam/getJobListLogDrop',
        getJobListLogPSGCancel: serviceBaseURI + '/service/ubeam/getJobListLogPSGCancel',
        getJobListLogDRVCancel: serviceBaseURI + '/service/ubeam/getJobListLogDRVCancel',

        getTaxiUsageList: serviceBaseURI + "/service/log/getTaxiUsageList",
        getCallcenterUsageList: serviceBaseURI + "/service/log/getCallcenterUsageList",
        MostHitData: serviceBaseURI + "/service/log/mostHit",

        getCallcenterLog: serviceBaseURI + '/service/log/getCallcenterLog',

        getAllGarage: serviceBaseURI + '/service/ubeam/getAllGarage'
    },
    viewMode: [
        { th: "รายชั่วโมง", en: "hour" }, 
        { th: "รายวัน", en: "day" }, 
        { th: "รายสัปดาห์", en: "week" },
        { th: "รายเดือน", en: "month" }
    ],
    daterangepicker: {
        options: {
            startDate: moment().subtract(1, 'months').format("DD/MM/YYYY HH:mm"),
            endDate: moment(),
            opens: "left",
            timePicker: false,
            autoUpdateInput: true,
            autoApply: true,
            applyClass: "btn-success hide",
            cancelClass: "btn-default hide",
            dateLimit: {
                months: 1
            },
            ranges: {
                "วันนี้": [moment(), moment()],
                "เมื่อวาน": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                "7 วันที่แล้ว": [moment().subtract(6, 'days'), moment()],
                "30 วันที่แล้ว": [moment().subtract(29, 'days'), moment()],
                "เดือนนี้": [moment().startOf('month'), moment().endOf('month')],
                "เดือนที่แล้ว": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                format: "DD/MM/YYYY HH:mm",
                separator: " - ",
                applyLabel: "ตกลง",
                cancelLabel: "ยกเลิก",
                fromLabel: "จาก",
                toLabel: "ถึง",
                customRangeLabel: "กำหนดเอง",
                daysOfWeek: [
                "อ.",
                "จ.",
                "อ.",
                "พ.",
                "พฤ.",
                "ศ.",
                "ส."
                ],
                monthNames: [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
                ],
                firstDay: 1
            },
            alwaysShowCalendars: false,
        }
    },
    Garage: { 
        cgroup: CGroup
    }
};


var App = {
	initial: function() {

        FilterControl.initial();

        TabControl.initial();

        CallcenterTab.initial();
        DataTable.CallcenterUsageTable.initial();
        DataTable.TaxiUsageTable.initial();
    },
    intervalManager: function (flag, action, time) {
        if (flag) {
            return setInterval(action, time);
        }
        else {
            typeof action !== 'undefined' && clearInterval(action);
        }
    }
};


var FilterControl = {

    button: null,

    displayText: $("#date-range-display-text"),

    initial: function() {

        this.button = $("#submitFilter");
        this.initialEventListener();
    },

    initialEventListener: function() {
        this.button.on("click", FilterControl.EevetHandler.onSubmit);
    },

    render: function(query) {

        if(TabControl.activeTab.hash === "#JoblistLogTab") {
            HighchartsMobile();
            DataTable.TaxiUsageTable.getData(query);
        }
        else if(TabControl.activeTab.hash === "#CallcenterLogTab") {
            CallcenterTab.createGraph();
        }

        FilterControl.setDisplayText(DateRagePicker.getData());
    },

    setDisplayText: function(date) {

        var dateInfoText = "";
        var viewMode = ViewModeList.getData().viewMode;

        switch ( viewMode ) {
            case "hour" :
                dateInfoText = moment(date.startTime).format("HH:mm, DD MMM YYYY");
                dateInfoText += " ถึง "
                dateInfoText += moment(date.endTime).format("HH:mm, DD MMM YYYY");
            break;
            case "day" :
                dateInfoText = moment(date.startTime).format("DD/MM/YYYY (ddd)");
                dateInfoText += " ถึง "
                dateInfoText += moment(date.endTime).format("DD/MM/YYYY (ddd)");
            break;
            case "week" :
                dateInfoText = moment(date.startTime).format("DD/MM/YYYY");
                dateInfoText += " ถึง "
                dateInfoText += moment(date.endTime).format("DD/MM/YYYY");
            break;
            case "month" :
                dateInfoText = moment(date.startTime).format("MMMM YYYY");
                dateInfoText += " ถึง "
                dateInfoText += moment(date.endTime).format("MMMM YYYY");
            break;
        }

        FilterControl.displayText.find("h4").text(dateInfoText);
    },

    getQuery: function() {

        var garages = CONSTRAINTS.Garage;
        var query = $.extend(garages);
        return query;
    },

    EevetHandler: {

        onSubmit: function() {
            var query = FilterControl.getQuery();
            FilterControl.render(query);
            FilterControl.setDisplayText(DateRagePicker.getData());
        }
    }
};


var TabControl = {

    activeTab: $("#reportTableControl").find(".active > a")[0],

    tab: $('a[data-toggle="tab"]'),

    initial: function() {
        this.initialEventListener();
        this.EevetHandler.onInit();
    },

    initialEventListener: function() {

        this.tab.on('shown.bs.tab', TabControl.EevetHandler.onChange);

        this.tab.on('shown.bs.tab', function (e) {
            TabControl.activeTab = e.target;
        });
    },

    EevetHandler: {

        onChange: function(e) {

            var tabName = e.target.hash;
            var query = FilterControl.getQuery();

            TabControl.activeTab = e.target;
            DataTable.TaxiUsageTable.datatable.fnDraw();
            DataTable.CallcenterUsageTable.datatable.fnDraw();
            
            if(tabName === "#JoblistLogTab") {
                HighchartsMobile();
            }
            else if(tabName === "#CallcenterLogTab") {
                CallcenterTab.createGraph();
            }
        },

        onInit: function() {
            HighchartsMobile();
        }
    }
};


var DataTable = {

    CallcenterUsageTable: {

        $table: $("#CallcenterUsageTable"),

        datatable: null,

        initial: function () {
            this.setUp();
            this.initialEventListener();
        },

        initialEventListener: function() {
        },

        setUp: function() {

            DataTable.CallcenterUsageTable.datatable = DataTable.CallcenterUsageTable.$table.dataTable({
                paging: true,
                autoWidth: true,
                scrollY: "650px",
                deferRender: true,
                scrollCollapse: true,
                iDisplayLength: 10,
                language: {
                    search: "",
                    searchPlaceholder: "ค้นหา",
                    sInfo: "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ",
                },
                columns: [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '<i class="material-icons">add_circle_outline</i>'
                    },
                    { title: "#" },
                    { title: "ทะเบียน" },
                    { title: "จำนวนงาน" },
                ],
                createdRow: function (row, data, index) {
                    $(row).on("click", "td.details-control", function() {

                        var tr = $(this).closest("tr");
                        var row = DataTable.CallcenterUsageTable.datatable.DataTable().row(tr);

                        if(row.child.isShown()) {
                            row.child.hide();
                            tr.removeClass("shown");
                            $(this).find(".material-icons").text("add_circle_outline");
                        }
                        else {
                            row.child( DataTable.CallcenterUsageTable.createChildTable(data[4])).show();
                            tr.addClass("shown");
                            $(this).find(".material-icons").text("remove");
                        }
                    });
                },
                drawCallback: function (settings) {
                    Preloader.hide(0);
                }
            });
        },

        render: function(data) {

            var drivers = [];

            data.forEach(function(driver, index) {
                drivers.push([
                    "",
                    (index + 1) || "",
                    driver._id.carplate,
                    driver.jobCount,
                    driver.joblist
                ]);
            });

            var oTable = DataTable.CallcenterUsageTable.datatable;

            oTable.fnClearTable();
            
            if(drivers.length > 0)
                oTable.fnAddData(drivers, true);
        },

        createChildTable: function(data) {

            var html = "";
            html += '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

            html += '<tr><th>การจ่ายงาน</th><th>จุดรับ</th><th>จุดส่ง</th><th>เวลา</th></tr>';

            data.forEach(function(job) {
                html += '<tr>';
                html += '<td>' + (job.drv_id ? '<i class="material-icons reg">phonelink_ring</i> จ่ายให้ในระบบ' : '<i class="material-icons none-reg">portable_wifi_off</i> จ่ายให้นอกระบบ') + '</td>';
                html += '<td>' + job.curaddr + '</td>';
                html += '<td>' + job.destination + '</td>';
                html += '<td>' + moment(job.created).format("DD/MM/YYYY HH:mm") + '</td>';
                html += '</tr>';
            });
            html += '</table>';

            return html;
        },

        clear: function() {
            DataTable.CallcenterUsageTable.datatable.fnClearTable();
        },

        getData: function(query) {
            $.post(CONSTRAINTS.serviceURI.getCallcenterUsageList, query, function(response) {
                if (response.status) {
                    DataTable.CallcenterUsageTable.render(response.data);
                } else {
                    DataTable.CallcenterUsageTable.clear();
                }
            }, 'json');
        },

        EevetHandler: {
            onInit: function() {

                var query = FilterControl.getQuery();
                DataTable.CallcenterUsageTable.getData(query);
            }
        }
    },

    TaxiUsageTable: {

        $table: $("#TaxiUsageTable"),

        datatable: null,

        initial: function () {
            this.setUp();
            this.initialEventListener();
        },

        initialEventListener: function() {
        },

        setUp: function() {

            DataTable.TaxiUsageTable.datatable = DataTable.TaxiUsageTable.$table.dataTable({
                paging: true,
                autoWidth: true,
                scrollY: "650px",
                deferRender: true,
                scrollCollapse: true,
                iDisplayLength: 10,
                language: {
                    search: "",
                    searchPlaceholder: "ค้นหา",
                    sInfo: "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ",
                },
                columns: [
                    {
                        "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '<i class="material-icons">add_circle_outline</i>'
                    },
                    { title: "#" },
                    { title: "ชื่อคนขับรถ" },
                    { title: "เบอร์โทรคนขับ" },
                    { title: "ทะเบียน" },
                    { title: "จำนวนงาน" },
                ],
                createdRow: function (row, data, index) {
                    $(row).on("click", "td.details-control", function() {

                        var tr = $(this).closest("tr");
                        var row = DataTable.TaxiUsageTable.datatable.DataTable().row(tr);

                        if(row.child.isShown()) {
                            row.child.hide();
                            tr.removeClass("shown");
                            $(this).find(".material-icons").text("add_circle_outline");
                        }
                        else {
                            row.child( DataTable.TaxiUsageTable.createChildTable(data[6])).show();
                            tr.addClass("shown");
                            $(this).find(".material-icons").text("remove");
                        }
                    });
                },
                drawCallback: function (settings) {
                    Preloader.hide(0);
                }
            });
        },

        render: function(data) {

            var drivers = [];

            data.forEach(function(driver, index) {
                drivers.push([
                    "",
                    (index+1),
                    driver._id.name || '-',
                    driver._id.phone || '-',
                    driver._id.carplate || '-',
                    driver.jobCount || '-',
                    driver.joblist
                ]);
            });

            var oTable = DataTable.TaxiUsageTable.datatable;

            oTable.fnClearTable();
            
            if(drivers.length > 0)
                oTable.fnAddData(drivers, true);
        },

        createChildTable: function(data) {

            var html = "";
            html += '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

            html += '<tr><th>เบอร์โทรผู้โดยสาร</th><th>จุดรับ</th><th>จุดส่ง</th><th>เวลา</th><th>เวลาเรียกงาน</th><th>เท็กซี่รับงาน</th><th>ผู้โดยสารตอบรับ</th><th>แท็กซี่รับผู้โดยสาร</th><th>แท็กซี่ส่งผู้โดยสาร</th><th>ผู้โดยสารยกเลิก</th><th>แท็กซี่ยกเลิก</th></tr>';

            data.forEach(function(job) {
                html += '<tr>';
                html += '<td style="min-width:100px">' + job.psg_phone.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3") + '</td>';
                html += '<td>' + job.curaddr + '</td>';
                html += '<td>' + job.destination + '</td>';
                html += '<td>' + moment(job.created).format("DD/MM/YYYY HH:mm") + '</td>';
                html += '<td>' + moment(job.datepsgcall).format("DD/MMM/YYYY") + " " + moment(job.datepsgcall).format("HH:mm น.") + '</td>';
                html += '<td>' + (typeof job.datedrvwait != 'undefined' ? '<i class="material-icons">phonelink_ring</i>' : "") + '</td>';
                html += '<td>' + (typeof job.datepsgaccept != 'undefined' ? '<i class="material-icons">phonelink_ring</i>' : "") + '</td>';
                html += '<td>' + (typeof job.datedrvpick != 'undefined' ? '<i class="material-icons">local_taxi</i>' : "") + '</td>';
                html += '<td>' + (typeof job.datedrvdrop != 'undefined' ? '<i class="material-icons">airline_seat_recline_extra</i>' : "") + '</td>';
                html += '<td>' + (typeof job.datepsgcancel != 'undefined' ? '<i class="material-icons">highlight_off</i>' : "") + '</td>';
                html += '<td>' + (typeof job.datedrvcancel != 'undefined' ? '<i class="material-icons">highlight_off</i>' : "") + '</td>';
                html += '</tr>';
            });
            html += '</table>';

            return html;
        },

        clear: function() {
            DataTable.TaxiUsageTable.datatable.fnClearTable();
        },

        getData: function(query) {
            $.post(CONSTRAINTS.serviceURI.getTaxiUsageList, query, function(response) {
                if (response.status) {
                    DataTable.TaxiUsageTable.render(response.data);
                } else {
                    DataTable.TaxiUsageTable.clear();
                }
            }, 'json');
        },

        EevetHandler: {
            onInit: function() {

                var query = FilterControl.getQuery();
                DataTable.TaxiUsageTable.getData(query);
            }
        }
    }
};


var CallcenterTab = {

    tab: $("#CallcenterLogTab"),

    initial: function() {
    },
    
    getMostHitData: function(query, callback) {
        $.post(CONSTRAINTS.serviceURI.MostHitData, query, function(response) {
            if (!response.status) { return; }

            if (callback && typeof callback === "function") {
                callback(response);
            }
        }, 'json');
    },

    initialEventListener: function() {
    },

    createMostHit: function(data) {
        CallcenterTab.createMostHitStartPlace(data.data.MostHitStartPlace);
        CallcenterTab.createMostHitDestinationPlace(data.data.MostHitDestinationPlace);
        CallcenterTab.createMostHitHoursText(data.data.MostHitHours);
    },

    createMostHitStartPlace: function(MostHitStartPlace) {
        var MostHitStartPlaceText = MostHitStartPlace && MostHitStartPlace._id !== "" ? MostHitStartPlace._id : "-";
        var MostHitStartPlaceCount = MostHitStartPlace ? MostHitStartPlace.count : "-";
        CallcenterTab.tab.find(".summary.rectangle.most-pick .title-badge").text(MostHitStartPlaceText);
        CallcenterTab.tab.find(".summary.rectangle.most-pick .detail .counter").text(MostHitStartPlaceCount);
    },

    createMostHitDestinationPlace: function(MostHitDestinationPlace) {
        var MostHitDestinationPlace = MostHitDestinationPlace;
        var MostHitDestinationPlaceText = MostHitDestinationPlace && MostHitDestinationPlace._id !== "" ? MostHitDestinationPlace._id : "-";
        var MostHitDestinationPlaceCount = MostHitDestinationPlace ? MostHitDestinationPlace.count : "-";
        CallcenterTab.tab.find(".summary.rectangle.most-destination .title-badge").text(MostHitDestinationPlaceText);
        CallcenterTab.tab.find(".summary.rectangle.most-destination .detail .counter").text(MostHitDestinationPlaceCount);
    },

    createMostHitHoursText: function(MostHitHours) {
        var MostHitHoursText = MostHitHours ? moment().hour(MostHitHours._id.hour).format("HH:00") : "-";
        var MostHitHoursCount = MostHitHours ? MostHitHours.total : "-";
        CallcenterTab.tab.find(".summary.rectangle.peak-hour .title-badge").text(MostHitHoursText);
        CallcenterTab.tab.find(".summary.rectangle.peak-hour .detail .counter").text(MostHitHoursCount);
    },

    createGraph: function() {

        Preloader.show();

        var width = $("#highcharts-callcenter").width() - 15;

        var seriesOptions = [],

        allTaskArray = [],
        seriesCounter = 0,
        names = ['unregister', 'registered', 'deleted'];
        garages = CONSTRAINTS.Garage;

        function afterSetExtremes(e) {

            Preloader.show();

            var datas = [];
            allTaskArray = [];
            seriesCounter = 0;
            garages = CONSTRAINTS.Garage;

            var chart = $('#highcharts-callcenter').highcharts();

            chart.showLoading('Loading data from server...');

            var query = $.extend({}, { startTime: Math.round(e.min), endTime: Math.round(e.max) }, { logType: "all",  }, CONSTRAINTS.Garage);

            DataTable.CallcenterUsageTable.getData(query);
            CallcenterTab.getMostHitData(query, CallcenterTab.createMostHit);

            $.post("/service/log/hcdata-callcenter", query, function(data) {

                var min = "", max = "";
                toCircleCounter(data.total, "all");
                if (data.result.length === 0) {
                    min = moment(chart.axes[0].dataMin).format("YYYY-MM-DD");
                    max = moment(chart.axes[0].dataMax).format("YYYY-MM-DD");
                } else {
                    min = data.result[0]["_id"];
                    max = data.result[data.result.length - 1]["_id"];
                }
                toCircleAverageHour(data.total, [min, max]);
                toCircleAverageMinute(data.total, [min, max]);

                allTaskArray = toValidData(data.result);
                chart.series[0].setData(allTaskArray);

                $.each(names, function (i, name) {

                    var query = $.extend({}, { startTime: Math.round(e.min), endTime: Math.round(e.max) }, { logType: name,  }, CONSTRAINTS.Garage);

                    $.post("/service/log/hcdata-callcenter", query, function(data) {

                        toCircleCounter(data.total, name);
                        var dataArray = [],
                        color = '#CCCCCC',
                        named = "",
                        tempArray = [];

                        allTaskArray.forEach(function(all) {
                            var result = toValidData(data.result).find(function(data) {
                                return data[0] === all[0];
                            });

                            if(result) {
                                dataArray.push(result);
                            } else {
                                dataArray.push([ all[0], 0 ]);
                            }
                        });

                        switch(name) {
                            case 'unregister':
                            color = '#CCCCCC';
                            named = "ผู้โดยสารตอบรับ";
                            break;
                            case 'registered':
                            color = '#4CAF50';
                            named = "แท็กซี่รับผู้โดยสาร";
                            break;
                            case 'deleted':
                            color = '#607D8B';
                            named = "แท็กซี่ส่งผู้โดยสาร";
                            break;
                        }

                        datas[i+1] = dataArray

                        seriesCounter += 1;

                        if (seriesCounter === names.length) {

                            datas.forEach(function(data, index) {
                                chart.series[index].setData(data);
                            });

                            chart.hideLoading();
                        }
                    }, "json");
                });
            }, "json");
        }

        function createChart() {

            $('#highcharts-callcenter').highcharts('StockChart', {

                chart: {
                    width: width,
                    zoomType: 'x'
                },

                // legend: {
                //     enabled: true
                // },

                rangeSelector: {
                    buttons: [{
                        type: 'day',
                        count: 1,
                        text: '1d'
                    }, {
                        type: 'day',
                        count: 2,
                        text: '2d'
                    }, {
                        type: 'day',
                        count: 3,
                        text: '3d'
                    }, {
                        type: 'day',
                        count: 5,
                        text: '5d'
                    }, {
                        type: 'day',
                        count: 7,
                        text: '7d'
                    }, {
                        type: 'month',
                        count: 1,
                        text: '1m'
                    }, {
                        type: 'year',
                        count: 1,
                        text: '1y'
                    }, {
                        type: 'all',
                        text: 'All'
                    }],
                    selected : 7,
                    inputEditDateFormat: '%Y-%m-%d'
                },

                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    valueDecimals: 0
                },

                navigator : {
                    adaptToUpdatedData: false,
                    // series : {
                    //     type: 'areaspline',
                    //     color: '#4572A7',
                    //     fillOpacity: 0.05,
                    //     dataGrouping: {
                    //         enabled: false
                    //     },
                    //     lineWidth: 1,
                    //     marker: {
                    //         enabled: false
                    //     }
                    // }
                },

                scrollbar: {
                    liveRedraw: false
                },

                xAxis : {
                    gapGridLineWidth: 0,
                    events : {
                        afterSetExtremes : afterSetExtremes
                    },
                    minRange: 3600 * 1000
                },

                yAxis: {
                    floor: 0
                },

                // yAxis: {
                //     plotLines: [{
                //         value: 0,
                //         width: 1,
                //         color: 'silver'
                //     }]
                // },

                series: seriesOptions

            }, function(chart) {

                setTimeout(function () {

                    $('input.highcharts-range-selector', $(chart.container).parent()).datepicker();
                    Preloader.hide();
                }, 0);
            });

            $.datepicker.setDefaults({
                dateFormat: 'yy-mm-dd',
                onSelect: function(dateText) {
                    this.onchange();
                    this.onblur();
                }
            });
        }

        function toCircleCounter(total, name) {
            $("#CallcenterLogTab").find(".summary.circle." + name + " > .counter > .number").text( Util.formatMoney(total, 0, ".", ",") );
        }

        function toCircleAverageHour(total, daterangeArr) {
            var diff = moment(daterangeArr[1]).valueOf() - moment(daterangeArr[0]).valueOf();
            var avg = total / Math.round( diff / (1000*60*60));
            $("#CallcenterLogTab").find(".summary.circle.jobPerHour > .counter > .number").text( Util.formatMoney(avg, 0, ".", ",") );
        }

        function toCircleAverageMinute(total, daterangeArr) {
            var diff = moment(daterangeArr[1]).valueOf() - moment(daterangeArr[0]).valueOf();
            var avg = total / Math.round( diff / (1000*60));
            $("#CallcenterLogTab").find(".summary.circle.jobPerMinute > .counter > .number").text( Util.formatMoney(avg, 0, ".", ",") );
        }

        function toValidData(data) {

            if(typeof data === 'string') {
                data = JSON.parse(data);
            }

            var validData = [];

            data.forEach(function(d) {
                validData.push([new Date(d._id).getTime(), d.count]);
            });

            return validData;
        }

        function getTaskDataEachType() {

            $.each(names, function (i, name) {

                var query = $.extend({}, { logType: name }, garages );

                $.post("/service/log/hcdata-callcenter", query, function(data) {

                    toCircleCounter(data.total, name);

                    var dataArray = [],
                    color = '#CCCCCC',
                    named = "",
                    tempArray = [];

                    allTaskArray.forEach(function(all) {
                        var result = toValidData(data.result).find(function(data) {
                            return data[0] === all[0];
                        });

                        if(result) {
                            dataArray.push(result);
                        } else {
                            dataArray.push([ all[0], 0 ]);
                        }
                    });

                    switch(name) {
                        case 'unregister':
                        color = '#4CAF50';
                        named = "จ่ายให้นอกระบบ";
                        break;
                        case 'registered':
                        color = '#2196F3';
                        named = "จ่ายให้ในระบบ";
                        break;
                        case 'deleted':
                        color = '#CCCCCC';
                        named = "งานที่ยกเลิก(ลบทิ้ง)";
                        break;
                    }

                    seriesOptions[i+1] = {
                        name: named,
                        color: color,
                        marker : {
                            enabled : true,
                            radius : 3
                        },
                        shadow : true,
                        pointStart: data.pointStart,
                        pointInterval: data.pointInterval,
                        dataGrouping: {
                            enabled: false
                        },
                        data: dataArray
                    };

                    seriesCounter += 1;

                    if (seriesCounter === names.length) {
                        createChart();
                    }
                }, "json");
            });
        }

        var query = $.extend({}, { logType: "all",  }, garages);

        DataTable.CallcenterUsageTable.getData(query);
        CallcenterTab.getMostHitData(query, CallcenterTab.createMostHit);


        $.post("/service/log/hcdata-callcenter", query, function(data) {

            var min = "", max = "";
            toCircleCounter(data.total, "all");
            if (data.result.length === 0) {
                var chart = $("#highcharts-callcenter").highcharts();
                min = moment(chart.axes[0].dataMin).format("YYYY-MM-DD");
                max = moment(chart.axes[0].dataMax).format("YYYY-MM-DD");
            } else {
                min = data.result[0]["_id"];
                max = data.result[data.result.length - 1]["_id"];
            }
            toCircleAverageHour(data.total, [min, max]);
            toCircleAverageMinute(data.total, [min, max]);

            allTaskArray = toValidData(data.result);

            seriesOptions[0] = {
                name: "จำนวนงานทั้งหมด",
                color: "#FF5722",
                marker : {
                    enabled : true,
                    radius : 3
                },
                shadow : true,
                pointStart: data.pointStart,
                pointInterval: data.pointInterval,
                data: allTaskArray
            };

            getTaskDataEachType();
        }, "json");
    }
};


var Http = {

    connection_error_time: 0,

    getJson: function (url, options) {

        if (options.loader !== undefined && !options.loader) { Preloader.show(); }

        $.getJSON(url, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        })
        .fail(function () {
            Notification.defaultError();
        })
        .always(function () {
            Preloader.hide();
        });
    },

    post: function (url, options) {

        if (options.loader || options.loader == undefined) { Preloader.show(); }

        if (options.data == undefined) { options.data = {}; }

        $.post(url, options.data, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        }, 'jsont')
        .fail(function () {
            if (Http.connection_error_time < 1) {
                Http.connection_error_time++;
            }
            else if (Http.connection_error_time >= 1) {

                Http.connection_error_time = 0;
                Http.onConnectionLost();
            }
        })
        .always(function () {
            Preloader.hide();
        });
    },

    onConnectionLost: function () {

        var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

        $('body').find("> .connect_lost_message").remove();
        $('body').append(message);
        $('body').find("> .connect_lost_message").fadeIn('fast');

        if (this.intervalId == undefined) {
            this.intervalId = App.intervalManager(true, Http.testConnection, 5000);
        }
    },

    testConnection: function () {
        $.post('service/ubeam/getPassengerDetail', { "psg_id": "" }, function (data) {
            clearInterval(Http.intervalId);
            App.intervalManager(false, Http.intervalId);
            delete Http.intervalId;
            Http.connection_error_time = 0;
            $('body').find("> .connect_lost_message").remove();
        });
    }
};


var Notification = {

    show: function (message) {
        options = {
            title: message.title,
            text: message.text,
            sticky: message.sticky ? message.sticky : false,
            time: message.time ? message.time : 2000,
            class_name: 'gritter-' + (!message.$class ? 'warning' : message.$class),
            position: message.position ? message.position : 'bottom-right'
        };

        $.gritter.add(options);
    },

    defaultSuccess: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Success!' : title,
            text: !text ? 'คำสั่ง ทำงานเรียบร้อย' : text,
            sticky: false,
            time: 1500,
            class_name: 'gritter-success',
            position: 'bottom-right'
        });
    },

    defaultError: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Server Error!' : title,
            text: !text ? 'Pls. refresh page and then try it again.' : text,
            sticky: false,
            time: 2500,
            class_name: 'gritter-danger',
            position: 'bottom-right'
        });
    },

    removeAll: function () {
        $.gritter.removeAll({
            time: 0,
            before_close: function (e) {
            },
            after_close: function () {
            }
        });
    },

    remove: function () {
        $("#gritter-notice-wrapper").remove();
    }
};


var Util = {

    formatMoney: function(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }
};


var HighchartsMobile = function() {

    var width = $("#container-report").width() - 15;

    var seriesOptions = [],

        allTaskArray = [],
        seriesCounter = 0,
        names = ['accept', 'pick', 'drop', 'psgcancel', 'drvcancel'];
        garages = CONSTRAINTS.Garage;

    function afterSetExtremes(e) {

        var datas = [];
        allTaskArray = [];
        seriesCounter = 0;
        garages = CONSTRAINTS.Garage;

        var chart = $('#container-report').highcharts();

        chart.showLoading('Loading data from server...');

        var query = $.extend({}, { startTime: Math.round(e.min), endTime: Math.round(e.max) }, { logType: "wait",  }, CONSTRAINTS.Garage);

        DataTable.TaxiUsageTable.getData(query);

        $.post("/service/log/hcData/callcenter", query, function(data) {

            toCircleCounter(data.total, "wait");

            allTaskArray = toValidData(data.result);
            chart.series[0].setData(allTaskArray);

            $.each(names, function (i, name) {

                var query = $.extend({}, { startTime: Math.round(e.min), endTime: Math.round(e.max) }, { logType: name,  }, CONSTRAINTS.Garage);

                $.post("/service/log/hcData/callcenter", query, function(data) {

                    toCircleCounter(data.total, name);
                    var dataArray = [],
                    color = '#CCCCCC',
                    named = "",
                    tempArray = [];

                    allTaskArray.forEach(function(all) {
                        var result = toValidData(data.result).find(function(data) {
                            return data[0] === all[0];
                        });

                        if(result) {
                            dataArray.push(result);
                        } else {
                            dataArray.push([ all[0], 0 ]);
                        }
                    });

                    switch(name) {
                        case 'accept':
                        color = '#CCCCCC';
                        named = "ผู้โดยสารตอบรับ";
                        break;
                        case 'pick':
                        color = '#4CAF50';
                        named = "แท็กซี่รับผู้โดยสาร";
                        break;
                        case 'drop':
                        color = '#607D8B';
                        named = "แท็กซี่ส่งผู้โดยสาร";
                        break;
                        case 'psgcancel':
                        color = '#E91E63';
                        named = "ผู้โดยสารยกเลิก";
                        break;
                        case 'drvcancel':
                        color = '#009688';
                        named = "แท็กซี่ยกเลิก";
                        break;
                    }

                    datas[i+1] = dataArray

                    seriesCounter += 1;

                    if (seriesCounter === names.length) {

                        datas.forEach(function(data, index) {
                            chart.series[index].setData(data);
                        });

                        chart.hideLoading();
                    }
                }, "json");
            });
        }, "json");
    }

    function createChart() {

        $('#container-report').highcharts('StockChart', {

            chart: {
                width: width,
                zoomType: 'x'
            },

            legend: {
                enabled: true
            },

            rangeSelector: {
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'day',
                    count: 2,
                    text: '2d'
                }, {
                    type: 'day',
                    count: 3,
                    text: '3d'
                }, {
                    type: 'day',
                    count: 5,
                    text: '5d'
                }, {
                    type: 'day',
                    count: 7,
                    text: '7d'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1y'
                }, {
                    type: 'all',
                    text: 'All'
                }],
                selected : 7,
                inputEditDateFormat: '%Y-%m-%d'
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                valueDecimals: 0
            },

            navigator : {
                adaptToUpdatedData: false,
                series : {
                    type: 'areaspline',
                    color: '#4572A7',
                    fillOpacity: 0.05,
                    dataGrouping: {
                        enabled: false
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false
                    }
                }
            },

            scrollbar: {
                liveRedraw: false
            },

            xAxis : {
                gapGridLineWidth: 0,
                events : {
                    afterSetExtremes : afterSetExtremes
                },
                minRange: 3600 * 1000
            },

            yAxis: {
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: 'silver'
                }]
            },

            series: seriesOptions

        }, function(chart) {

            setTimeout(function () {

                $('input.highcharts-range-selector', $(chart.container).parent()).datepicker();

            }, 0);
        });

        $.datepicker.setDefaults({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText) {
                this.onchange();
                this.onblur();
            }
        });
    }

    function toCircleCounter(total, name) {
        $("#JoblistLogTab").find(".summary.circle." + name + " > .counter > .number").text( Util.formatMoney(total, 0, ".", ",") );
    }

    function toValidData(data) {

        if(typeof data === 'string') {
            data = JSON.parse(data);
        }

        var validData = [];

        data.forEach(function(d) {
            validData.push([new Date(d._id).getTime(), d.count]);
        });

        return validData;
    }

    function getTaskDataEachType() {

        $.each(names, function (i, name) {

            var query = $.extend({}, { logType: name }, garages );

            $.post("/service/log/hcData/callcenter", query, function(data) {

                toCircleCounter(data.total, name);

                var dataArray = [],
                    color = '#CCCCCC',
                    named = "",
                    tempArray = [];

                allTaskArray.forEach(function(all) {
                    var result = toValidData(data.result).find(function(data) {
                        return data[0] === all[0];
                    });

                    if(result) {
                        dataArray.push(result);
                    } else {
                        dataArray.push([ all[0], 0 ]);
                    }
                });

                switch(name) {
                    case 'accept':
                    color = '#CCCCCC';
                    named = "ผู้โดยสารตอบรับ";
                    break;
                    case 'pick':
                    color = '#4CAF50';
                    named = "แท็กซี่รับผู้โดยสาร";
                    break;
                    case 'drop':
                    color = '#607D8B';
                    named = "แท็กซี่ส่งผู้โดยสาร";
                    break;
                    case 'psgcancel':
                    color = '#E91E63';
                    named = "ผู้โดยสารยกเลิก";
                    break;
                    case 'drvcancel':
                    color = '#009688';
                    named = "แท็กซี่ยกเลิก";
                    break;
                }

                seriesOptions[i+1] = {
                    name: named,
                    color: color,
                    marker : {
                        enabled : true,
                        radius : 3
                    },
                    shadow : true,
                    pointStart: data.pointStart,
                    pointInterval: data.pointInterval,
                    dataGrouping: {
                        enabled: false
                    },
                    data: dataArray
                };

                seriesCounter += 1;

                if (seriesCounter === names.length) {
                    createChart();
                }
            }, "json");
        });
    }

    var query = $.extend({}, { logType: "wait",  }, garages);
    DataTable.TaxiUsageTable.getData(query);

    $.post("/service/log/hcData/callcenter", query, function(data) {

        toCircleCounter(data.total, "wait");
        allTaskArray = toValidData(data.result);

        seriesOptions[0] = {
            name: "แท็กซี่รับงาน",
            color: "#2196F3",
            marker : {
                enabled : true,
                radius : 3
            },
            shadow : true,
            pointStart: data.pointStart,
            pointInterval: data.pointInterval,
            data: allTaskArray
        };

        getTaskDataEachType();
    }, "json");
};


$(document).ready(function() {
	App.initial();
});