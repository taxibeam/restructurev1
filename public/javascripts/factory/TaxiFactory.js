var TaxiLayer = function(ApplicationContext) {

	var context = ApplicationContext || getApplicationContext();

	var Map = context.Map;
	var layer = this.layer = L.layerGroup();
	// var layer = this.layer = new PruneClusterForLeaflet();

	if( !Map instanceof L.Map ) { throw "TypeError :: data is not LeafletJS Map"; }

	Map.addLayer(layer);

	this.addTaxi = function(taxiMarker) {
		layer.addLayer(taxiMarker);
		// layer.RegisterMarker(taxiMarker);
	}

	this.clear = function() {
		layer.clearLayers();
		// layer.RemoveMarkers();
	}
};


var TaxiDriver = function(data) {

	if ( this === window ) throw "InitializeError :: Cannot direct use this without new class.";

	var taxi = this.data = $.extend({}, data);

	this.addToMap = function(taxiLayer) {

		if( !taxiLayer instanceof L.LayerGroup ) { throw "TypeError :: data is not L.LayerGroup"; }

		var marker = this.createMarker();
		taxiLayer.layer.addLayer(marker);
	}

	this.createMarker = function() {

		if (taxi.curloc === undefined || !taxi.curloc instanceof Array || taxi.curloc.langth === 0) {
			return;
		}

		this.setImageFace();

		var taxiMarker = L.Marker.movingMarker([[taxi.curlat, taxi.curlng],[taxi.curlat, taxi.curlng]], [500]);

		taxiMarker.bindPopup(this.createPopup());
		var icon = this.createIcon();
		taxiMarker.setIcon(icon);

		taxiMarker.options.data = taxi;

		return taxiMarker;
	}

	this.createIcon = function() {
		return L.divIcon({
			className: 'carplate-pin ' + taxi.status.toLocaleLowerCase(),
			html: '<div class="message"><img src="/assets/img/map/tx_' + taxi.status.toLocaleUpperCase() + '.png"><div class="arrow"></div></div>',
			iconSize: [41, 51], // size of the icon
		    iconAnchor: [20, 51], // point of the icon which will correspond to marker's location
		    popupAnchor: [0, -51] // point from which the popup should open relative to the iconAnchor
		});
	}

	this.setImageFace = function () {
		if (taxi.imgface == undefined || taxi.imgface == "") {
			taxi.imgface = "assets/img/thumbnail_photo.jpg";
		} else {
			taxi.imgface = "/image/driver/" + taxi.imgface;
		}
	}

	this.createPopup = function() {

		var HTMLPopup = '<div class="row">';
		HTMLPopup += '<div class="col-md-6">';
		HTMLPopup += '<img class="driver-img" src="' + taxi.imgface + '" />';
		HTMLPopup += '</div>';
		HTMLPopup += '<div class="driver-info col-md-6">';
		HTMLPopup += '<p class="full_name value">' + taxi.fname + ' ' + taxi.lname + '</p>';
		HTMLPopup += '<p class="caption">เบอร์โทรติดต่อ</p>';
		HTMLPopup += '<p class="tel value">' + taxi.phone + '</p>';
		HTMLPopup += '<div class="pull-left">';
		HTMLPopup += '<p class="caption">ทะเบียน</p>';
		HTMLPopup += '<p class="license_plate value">' + taxi.carplate + '</p>';
		HTMLPopup += '</div>';
		HTMLPopup += '<div class="pull-left">';
		HTMLPopup += '<p class="caption">สถานะ</p>';
		HTMLPopup += '<p class="status value">' + taxi.status + '</p>';
		HTMLPopup += '</div>';
		HTMLPopup += '</div>';
		HTMLPopup += '</div>';

		if (taxi.status.toLocaleUpperCase() == "BROKEN") {
			var label = '<div class="label-message-status"><h3 class="title">รถเสีย</h3><a class="call-message" href="#" onclick="Taxi.toggleBrokenDetail(this);">';
			label += '<span class="txt-show">แสดง</span><span class="txt-hide">ซ่อน</span>รายละเอียด';
			label += '</a>';
			if (Object.prototype.toString.call(taxi.brokenname) === '[object Array]') {
				label += '<p class="list"><i>อาการ</i><br>' + taxi.brokenname.toString() + '</p>';
			}
			label += '<p class="detail"><i>รายละเอียด</i><br>' + taxi.brokendetail + '</p>';
			label += '</div>';
			HTMLPopup += label;
		}

		return HTMLPopup;
	}
};