var LandMarkLayer = function(ApplicationContext) {

	var context = ApplicationContext || getApplicationContext();

	var Map = context.Map;
	var layer = this.layer = L.layerGroup();

	if( !Map instanceof L.Map ) { throw "TypeError :: data is not LeafletJS Map"; }

	layer.addTo(Map);

	this.addLandMarker = function(landMarker) {
		layer.addLayer(landMarker);
	}

	this.clear = function() {
		layer.clearLayers();
	}
};

var LandMarker = function(data) {

	if ( this === window ) throw "InitializeError :: Cannot direct use this without new class.";

	var landmark = this.data = $.extend({}, data);

	this.addToMap = function(landMarkLayer) {

		if( !landMarkLayer instanceof LandMarkLayer ) { throw "TypeError :: data is not defineValues"; }

		var marker = this.createMarker();
		landMarkLayer.addLandMarker(marker);
	}

	this.createMarker = function() {

		var icon = "";
		var className = "";
		if (landmark.poitype == 1) {
			className = "landmark";
			icon = '<i class="material-icons">star</i>';
		}
		else if (landmark.poitype) {
			className = "parking";
			icon = '<i class="material-icons">local_parking</i>';
		}

		var positionStyle = L.divIcon({
				className: 'position-pin ' + className,
				html: '<div class="wrapper"><div class="icon">' + icon + '</div><div class="text">' + landmark.name + '</div><div class="arrow"></div></div>',
            });

		var landMarker = L.marker([landmark.curloc[1], landmark.curloc[0]], {
			riseOnHover: true,
			icon: positionStyle
		});

		landMarker.options.landMarkerInfo = landmark;

		// var landMarker = new PruneCluster.Marker(landmark.curloc[1], landmark.curloc[0]);
		// getApplicationContext('pruneCluster').RegisterMarker(landMarker);

		// landMarker.data.landMarkerInfo = landmark;

		return landMarker;
	}
};