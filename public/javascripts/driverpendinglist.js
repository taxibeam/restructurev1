var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    serviceURI: {
        driverEdit: serviceBaseURI + '/service/ubeam/driverEdit',
        getdriverdetail: serviceBaseURI + '/service/ubeam/getdriverdetail',
        getdriverpendinglist: serviceBaseURI + '/service/ubeam/getdriverpendinglist',
        checkCitizenId: serviceBaseURI + '/service/ubeam/checkcitizenid'
    },
    // driverImageURL: 'http://localhost:8888/uploads/'
    driverImageURL: 'http://callcenter.taxi-beam.com/image/driver/'
};

var C = CONSTRAINTS;


var App = {
    name: "taxi-management",
    bootstrap: function () {

        this.setActiveSideBar();

        setTimeout(function() {
            EditDriverForm.createCarProvinceSelectList(10);
            EditDriverForm.createProvinceSelectList(0);
            EditDriverForm.createDistrictSelectList(0);
            EditDriverForm.createTambonSelectList(0);
            EditDriverForm.onDropDownOpen();
        }, 1000);

        DriverEditModal.init();
        DriverTable.loadData();

        $('.masked_input').inputmask();

        $(document).on('keypress', function (evt) {
            if (evt.isDefaultPrevented()) {
                $(evt.target).trigger('input');
            }
        });

        EditDriverForm.form.find('input[name=citizenid]').on('blur', function () { onCitizenIDBlur(this, "EDIT") });

        Socket.initial();
    },
    setActiveSideBar: function() {
        $("aside#sidebar_main").find('.menu_section > ul > li[title="' + this.name + '"]').addClass("current_section");
    }
}

var DriverTable = {
    table: $("#driver-datatable"),
    instance: null,
    loadData: function () {
        $.post(C.serviceURI.getdriverpendinglist, DriverTable.createTable);
    },
    createTable: function (res) {

        if (res.status) {

            var dataSet = [];
            res.data.forEach(function (driver, index) {
                dataSet.push([
                    (index + 1),
                    driver.fname || '',
                    (driver.prefixcarplate || '') + ' ' + driver.carplate || '',
                    driver.carcolor || '',
                    driver.phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
                    moment(driver.created).format("YYYY/MM/DD"),
                    driver
                ]);
            });
        }

        DriverTable.instance = DriverTable.table.DataTable({
            data: dataSet,
            order: [[0, 'asc']],
            columnDefs: [
                {
                    targets: 6,
                    width: "100px",
                    className: "uk-text-center",
                    render: function (data, type, row) {
                        var btnGroup = '<a onclick="DriverEditModal.open()" data-uk-tooltip="{pos:\'top\'}" data-uk-tooltip="{pos:\'top\'}" title="แก้ไข"><i class="md-icon material-icons">&#xE254;</i></a>';
                        return btnGroup;
                    }
                }
            ],
            createdRow: function (row, data, index) {
                $(row).attr('data-row-id', data[6]._id);
                $(row).data(data[6]);
            },
            drawCallback: function (settings) {
            }
        });
    },
    addNewRow: function (data) {
        this.instance.row.add([
            DriverTable.instance.rows()[0].length + 1,
            data.fname || '',
            (data.prefixcarplate || '') + ' ' + data.carplate || '',
            data.carcolor || '',
            data.phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
            moment(data.created).format("DD/MM/YYY"),
            data
        ]).draw();
    },
    updateRow: function (row, data) {
        this.instance.row(row).data([
            (DriverTable.instance.row(row).index() + 1),
            data.fname || '',
            (data.prefixcarplate || '') + ' ' + data.carplate || '',
            data.carcolor || '',
            data.phone || '',
            moment(data.created).format("YYYY/MM/DD"),
            data
        ]);
        altair_forms.switches();
    },
    removeRow: function (row) {
        if(row)
            this.instance.row(row).remove().draw();
    },
    removeRowById: function(id) {
        var row = DriverTable.table.find('tr[data-row-id='  + id + ']');
        DriverTable.removeRow(row);
    }
}

var Loader = {
    show: function () {
        return UIkit.modal.blockUI('<div class="uk-text-center"><img class="uk-margin-top" src="assets/theme/altair/img/spinners/spinner.gif" alt=""><br/>กำลังทำงาน...</div>')
    }
}

var editDriverFormOnSumit = function (form, event) {

    var instance = $(form).parsley();

    if (!EditDriverForm.checkSelectListEmptyValue()) {
        return;
    }
    else if (instance.isValid()) {

        if(!EditDriverForm.checkSelectListEmptyValue()) {
            return;
        }

        var formData = new FormData(form);
        formData.set('cgroup', USER_DATA.group);
        formData.set('_id', $(form).data()._id);
        formData.set('province', $(form).find("[name=province]").text());
        formData.set('district', $(form).find("[name=district]").text());
        formData.set('tambon', $(form).find("[name=tambon]").text());
        formData.set('carprovince', $(form).find("[name=carprovince]").text());
        formData = customEditFormValue($(form), formData);

        var loader = Loader.show();

        $.ajax({
            url: C.serviceURI.driverEdit,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false
        }).done(function (response) {
            setTimeout(function () {
                loader.hide();
                if (response.status) {
                    var row = DriverTable.table.find('tr[data-row-id="' + response.driver._id + '"]')[0];
                    if(response.driver.status === 'APPROVED') {
                        DriverTable.removeRow(row);
                        UIkit.notify('ยืนยันข้อมูลเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                    } else {
                        DriverTable.updateRow(row, response.driver);
                        $(row).data(response.driver);
                        UIkit.notify('แก้ไขข้อมูลเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                    }
                    DriverEditModal.instance.hide();
                } else {
                    if (response.code === 'USERNAME_DUPLICATED') {
                        UIkit.notify('username ดังกล่าว มีผู้ใช้งานแล้ว', { pos: 'bottom-center', status: 'warning' });
                    } else {
                        UIkit.notify(response.message || response.code, { pos: 'bottom-center', status: 'success' });
                    }
                }
            }, 1000);
        }).fail(function () {
            loader.hide();
            UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
        });
    }

    event.preventDefault();
    return;
}

var customEditFormValue = function (form, formData) {
    
    formData.set('status', form.find("[name=dataStatus]")[0].checked);
    form.find('[name=active]')[0].checked ? formData.set('active', 'Y') : formData.set('active', 'N');
    form.find("[name=english]")[0].checked ? formData.set('english', 'Y') : formData.set('english', 'N');
    form.find("[name=carryon]")[0].checked ? formData.set('carryon', 'Y') : formData.set('carryon', 'N');
    form.find("[name=outbound]")[0].checked ? formData.set('outbound', 'Y') : formData.set('outbound', 'N');
    form.find("[name=allowpsgcontact]")[0].checked ? formData.set('allowpsgcontact', 'Y') : formData.set('allowpsgcontact', 'N');

    return formData;
}

var clearViewModalDisplayValue = function () {
    $('#view_driver_modal').find('[data-field]').text('');
    $('#view_driver_modal').find('[data-driver-imgcar]').attr('src', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
    $('#view_driver_modal').find('[data-driver-imgface]').attr('src', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
    $('#view_driver_modal').find('[data-driver-imglicence]').attr('src', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
}

var clearEditFormDisplayValue = function () {
    $('#edit_driver_form').parsley().reset();
    $('#edit_driver_form').find('input').val('');
    $('#edit_driver_form').find('input[type=checkbox][data-md-icheck]').iCheck('uncheck');
    $('#edit_driver_form').find('[type=file]').parent().find('.dropify-clear').click();
}

var dumpFormData = function (form) {
    for (var pair of form.entries()) {
        console.log(pair);
    }
}

var onCitizenIDBlur = function (self, type) {

    if (self.value.length !== 13) {
        return;
    }

    var data = { citizenid: self.value };
    if (type === "EDIT") {
        data._id = DriverEditModal.modal.find('form#edit_driver_form').data()._id;
    }

    $.ajax({
        url: C.serviceURI.checkCitizenId,
        type: 'POST',
        data: data
    })
    .done(function (response) {
        if (!response.status) {
            if (response.errcode) {
                var messageElem = '<div class="citizen-message-wrap invalid" tab-index="-1">';
                messageElem += '<i class="material-icons">&#xE5C9;</i> ';
                messageElem += "<span>" + response.message + "<a href='#' onclick='DriverEditModal.open(\"" + response.data._id + "\")' class='notify-action'>เรียกดู</a></span>";
                messageElem += '</div>';
            
            } else {
                var messageElem = '<div class="citizen-message-wrap invalid" tab-index="-1">';
                messageElem += '<i class="material-icons">&#xE5C9;</i> ';
                messageElem += "<span>หมายเลขนี้เป็นสมาชิกอู่อื่นอยู่ ไม่สามารถทำการสมัครสมาชิกใหม่ได้</span>";
                messageElem += '</div>';
            }
        } else {
                var messageElem = '<div class="citizen-message-wrap valid">';
                messageElem += '<i class="material-icons">&#xE86C;</i> ';
                messageElem += '<span>ท่านสามารถใช้เลขบัตรประชาชนนี้ได้</span>';
                messageElem += '</div>';
            }
        })
        .fail(function () {
            // UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
        });
}

var DriverEditModal = {
    modal: $('#edit_driver_modal'),
    instance: null,
    init: function () {
        this.instance = UIkit.modal(this.modal);

        this.instance.on({
            'show.uk.modal': DriverEditModal.event.onShow,
            'hide.uk.modal': DriverEditModal.event.onHide,
        });
    },
    open: function (id) {
        clearEditFormDisplayValue();
        DriverEditModal.instance.show();
        if (!id) {
            var id = $(event.target).closest('tr').data()._id;
        }
        this.getData(id);
    },
    hide: function() {
        DriverEditModal.instance.hide();
    },
    getData: function (id) {
        $.post(C.serviceURI.getdriverdetail, { _id: id }, function (response) {
            if (response.status) {
                DriverEditModal.fillData(response.driver);
            }
            return;
        });
    },
    fillData: function (data) {

        this.modal.find('.uk-modal-title').text((data.prefixcarplate || '') + ' ' + data.carplate + ' - ' + data.fname + ' ' + data.lname);

        this.modal.find('[name=fname]').val(data.fname);
        this.modal.find('[name=lname]').val(data.lname);
        this.modal.find('[name=citizenid]').val(data.citizenid);
        this.modal.find('[name=dob]').val(data.dob);
        this.modal.find('[name=phone]').val(data.phone);
        this.modal.find('[name=emergencyphone]').val(data.emergencyphone);
        this.modal.find('[name=address]').val(data.address);
        this.modal.find('[name=tambon]').val(data.tambon);
        this.modal.find('[name=district]').val(data.district);
        this.modal.find('[name=province]').val(data.province);
        this.modal.find('[name=zipcode]').val(data.zipcode);
        this.modal.find('[name=prefixcarplate]').val(data.prefixcarplate);
        this.modal.find('[name=carplate]').val(data.carplate);
        this.modal.find('[name=taxiID]').val(data.taxiID);
        this.modal.find('[name=carcolor]').val(data.carcolor);
        this.modal.find('[name=cartype]').val(data.cartype);

        var $selectGender = this.modal.find('[name=gender]')
        var selectizeGender = $selectGender[0].selectize;
        if(data.gender) {
            selectizeGender.setValue(data.gender);
            this.modal.find('[name=gender]').val(data.gender);
        } else {
            selectizeGender.setValue("0");
            this.modal.find('[name=gender]').val("เลือกเพศ");
        }

        var pv = new Province();

        var $select0 = this.modal.find('[name=carprovince]')
        var selectize0 = $select0[0].selectize;

        var carprovinceObj = pv.getProvince('th', data.carprovince);
        if(carprovinceObj) {
            EditDriverForm.createCarProvinceSelectList(carprovinceObj.id);
        } else {
            EditDriverForm.createCarProvinceSelectList(0);
        }

        var $select1 = this.modal.find('[name=province]')
        var selectize1 = $select1[0].selectize;

        var provinceObj = pv.getProvince('th', data.province);
        if(provinceObj) {
            EditDriverForm.createProvinceSelectList(provinceObj.id);
            EditDriverForm.createDistrictSelectList(provinceObj.id, data.district);

            var districtList = pv.getDistrictList('th', provinceObj.id);
            var districtObj = districtList.find(function (o, i) {
                return o.name === data.district;
            });

            if (!districtObj) {
                districtObj = districtList[0];
            }

            EditDriverForm.createTambonSelectList(districtObj.id, data.tambon);

        } else {
            EditDriverForm.createProvinceSelectList(0);
            EditDriverForm.createDistrictSelectList(0);
            EditDriverForm.createTambonSelectList(0);
        }

        var $select2 = this.modal.find('[name=carcolor]');
        var selectize2 = $select2[0].selectize;
        selectize2.settings.create = true;
        selectize2.addOption({ value: data.carcolor, text: data.carcolor });
        selectize2.setValue(data.carcolor);

        var $select3 = this.modal.find('[name=cartype]')
        var selectize3 = $select3[0].selectize;
        selectize3.setValue(data.cartype);

        if (data.allowpsgcontact === 'Y') {
            this.modal.find('[name=allowpsgcontact]').iCheck('check');
        } else if (data.allowpsgcontact === 'N') {
            this.modal.find('[name=allowpsgcontact]').iCheck('uncheck');
        }

        if (data.outbound === 'Y') {
            this.modal.find('[name=outbound]').iCheck('check');
        } else if (data.outbound === 'N') {
            this.modal.find('[name=outbound]').iCheck('uncheck');
        }

        if (data.carryon === 'Y') {
            this.modal.find('[name=carryon]').iCheck('check');
        } else if (data.carryon === 'N') {
            this.modal.find('[name=carryon]').iCheck('uncheck');
        }

        if (data.english === 'Y') {
            this.modal.find('[name=english]').iCheck('check');
        } else if (data.english === 'N') {
            this.modal.find('[name=english]').iCheck('uncheck');
        }

        var checked = DriverEditModal.modal.find('input[name=active]')[0].checked;
        if (data.active === 'Y' && !checked) {
            this.modal.find('input[name=active]').parent().find('.switchery').click();
        } else if (data.active === 'N' && checked) {
            this.modal.find('input[name=active]').parent().find('.switchery').click();
        }

        if(data.status === 'PENDING') {
            this.modal.find('input[name=dataStatus]').attr("data-status", data.status).iCheck('uncheck');
        } else {
            this.modal.find('input[name=dataStatus]').attr("data-status", data.status).iCheck('check');
        }

        this.modal.find('.dropify-wrapper').remove();

        this.modal.find('[data-wrapper-image-car]').append('<input type="file" name="imgcar" class="dropify" data-default-file="' + C.driverImageURL + data.imgcar + '" data-show-remove="false" />');
        this.modal.find('[data-wrapper-image-face]').append('<input type="file" name="imgface" class="dropify" data-default-file="' + C.driverImageURL + data.imgface + '" data-show-remove="false" />');
        this.modal.find('[data-wrapper-image-license]').append('<input type="file" name="imglicence" class="dropify" data-default-file="' + C.driverImageURL + data.imglicence + '" data-show-remove="false" />');

        this.modal.find('.dropify').dropify({ showRemove: false });
        this.modal.find('form#edit_driver_form').data(data);
        removeDropifyImageCached(this.modal);
    },
    event: {
        onShow: function() { },
        onHide: function() {
            $(".parsley-errors-list").remove();
        }
    }
}

var EditDriverForm = {

    form: $('#edit_driver_modal'),

    createCarProvinceSelectList: function (id) {
        
        var selectOptions = [];

        var $select = $('#edit_driver_form').find('[name="carprovince"]');
        var selectize = $select[0].selectize;
        
        selectize.clearOptions();
        selectize.renderCache = {};

        new Province().getProvince('th').find(function (o, i) {

            if (i === 0) {
                selectOptions.push({
                    value: 0,
                    text: "-- ระบุทะเบียนจังหวัด --"
                });
            }
            selectOptions.push({
                value: o.id,
                text: o.name
            });
        });
        selectize.load(function (callback) {
            callback(selectOptions);
        });
        
        selectize.settings.placeholder = 'ระบุทะเบียนจังหวัด'
        selectize.updatePlaceholder();
        
        selectize.setValue(id);
    },

    createProvinceSelectList: function (id) {
        
        var selectOptions = [];

        var $select = $('#edit_driver_form').find('[name="province"]');
        var selectize = $select[0].selectize;
        
        selectize.clearOptions();
        selectize.renderCache = {};

        new Province().getProvince('th').find(function (o, i) {

            if (i === 0) {
                selectOptions.push({
                    value: 0,
                    text: "เลือกจังหวัด"
                });
            }
            selectOptions.push({
                value: o.id,
                text: o.name
            });
        });
        selectize.load(function (callback) {
            callback(selectOptions);
        });
        selectize.off('change');
        selectize.on('change', function (value) {
            EditDriverForm.createDistrictSelectList(value);
            EditDriverForm.createTambonSelectList(0);
        });

        selectize.settings.placeholder = 'เลือกจังหวัด'
        selectize.updatePlaceholder();

        selectize.setValue(id);
    },

    createDistrictSelectList: function (provinceId, districtText) {

        var selectOptions = [];
        var districtId = 0;

        var $select = $('#edit_driver_form').find('[name="district"]');
        var selectize = $select[0].selectize;

        selectize.settings.placeholder = 'เลือกอำเภอ'
        selectize.updatePlaceholder();
        
        selectize.clearOptions();
        selectize.renderCache = {};

        if(provinceId > 0) {
            var pv = new Province();
            pv.getDistrictList('th', provinceId).find(function (o, i) {
                if(i === 0) {
                    selectOptions.push({
                        value: 0,
                        text: "เลือกอำเภอ"
                    });
                }
                if(o.name === districtText) {
                    districtId = o.id;
                }
                selectOptions.push({
                    value: o.id,
                    text: o.name
                });
            });
            selectize.load(function(callback) {
                callback(selectOptions);
            });
            selectize.off('change');
            selectize.on('change', function(value) {
                EditDriverForm.createTambonSelectList(value);
            });
        }
        
        selectize.setValue(districtId);
    },

    createTambonSelectList: function (districtId, tambonText) {
        
        var selectOptions = [];
        var tambonId = 0;

        var $select = $('#edit_driver_form').find('[name="tambon"]');
        var selectize = $select[0].selectize;

        selectize.settings.placeholder = 'เลือกตำบล'
        selectize.updatePlaceholder();

        selectize.clearOptions();
        selectize.renderCache = {};

        if(districtId > 0) {
            var pv = new Province();
            pv.getSubDistrictList('th', districtId).find(function (o, i) {
                if(i === 0) {
                    selectOptions.push({
                        value: 0,
                        text: "เลือกตำบล"
                    });
                }
                if(o.name === tambonText) {
                    tambonId = o.id;
                }
                selectOptions.push({
                    value: o.id,
                    text: o.name
                });
            });
            selectize.load(function(callback) {
                callback(selectOptions);
            });
        }
        
        selectize.setValue(tambonId);
    },

    onProvinceSelectListChange: function () {
        var pv = new Province();
        if (this.value === "") { return; }
        var p = pv.getProvince('th', this.value);
        if(p) {
            EditDriverForm.createDistrictSelectList(p.id);
        } else {
            EditDriverForm.createDistrictSelectList(0);
        }
    },

    checkSelectListEmptyValue() {

        var isValid = true;
        var selects = ['gender', 'province', 'district', 'tambon', 'carprovince', 'carcolor', 'cartype'];

        selects.forEach(function(name, index) {
            var select = EditDriverForm.form.find('[name=' + name + ']'); 
            var value = select.val();
            if(!value || value === "0") {
                
                select.parent().find(".parsley-errors-list").remove();
                select.parent().append('<div class="parsley-errors-list filled" id="parsley-id-37"><span class="parsley-required">กรุณาระบุข้อมูลนี้</span></div>')
                
                isValid = false;
            }
        });
        return isValid;
    },

    onDropDownOpen() {
        EditDriverForm.form.find('select').filter(function(index, select) {
            var selectize = select.selectize;
            if(selectize) {
                selectize.on('dropdown_open', function($dropdown) {
                    $(select).parent().find(".parsley-errors-list").remove();
                });
            }
        });
    }
}

var removeDropifyImageCached = function (modal) {
    var hash = '?' + Date.now().toString(16);
    modal.find('.dropify-render > img').each(function (index, img) {
        var src = $(img).attr('src');
        $(img).attr('src', src + hash);
    });
}

var Socket = {
    initial: function () {
        
        Socket.validate();
        
        var origin = window.location.origin;
        
        Socket = io("/" + USER_DATA.group);
        
        Socket.on('request approve', function (data) {
            if(data.status === 'PENDING') {
                var row = DriverTable.table.find('tr[data-row-id="' + data._id + '"]');
                if(!row) {
                    DriverTable.addNewRow(data);
                }
            }
            else if(data.status === 'APPROVED') {
                DriverTable.removeRowById(data._id);
                DriverEditModal.hide();
            }
        });

        Socket.on('driver approved', function (data) {
            if (data.status !== 'PENDING') {
                DriverTable.removeRowById(data._id);
                DriverEditModal.hide();
            }
        });

        return Socket;
    },
    validate: function () {
        window.addEventListener("beforeunload", function (e) {
            Socket.emit('window beforeunload', USER_DATA);
        }, false);
    }
};

App.bootstrap();