var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    MAX_POI: 500,
    serviceURI: {
        addPOI: serviceBaseURI + '/garage/poi/add',
        editPOI: serviceBaseURI + '/garage/poi/edit',
        deletePOI: serviceBaseURI + '/garage/poi/delete',
        loadPOIList: serviceBaseURI + '/service/ubeam/getpoirecommended',
    },
    driverImageURL: 'http://callcenter.taxi-beam.com/image/driver/'
};

var C = CONSTRAINTS;

var App = {
    name: "POI Manager",
    bootstrap: function () {
        this.setActiveSideBar();
        POIDatatable.loadData();
    },
    setActiveSideBar: function () {
        $("aside#sidebar_main").find('.menu_section > ul > li[title="' + this.name + '"]').addClass("current_section");
    }
}

var POIDatatable = {

    table: $("#poi_list_table"),

    instance: null,

    loadData: function () {

        clearMarkers();

        $.post(C.serviceURI.loadPOIList, { cgroup: 'ccubon' }, function (res) {
            if (res.status && res.data instanceof Array && res.data.length > 0) {
                var dataSet = [];
                res.data.forEach(function (data, index) {
                    dataSet.push([
                        data.parkingname || '',
                        data.curloc,
                        {
                            created: data.created || "",
                            updated: data.updated || ""
                        },
                        data
                    ]);
                    createMarker(data._id, { lat: data.curloc[1], lng: data.curloc[0] }, data.parkingname);
                });

                POIDatatable.createTable(dataSet);
                defindBoundMarker();
            } else {
                POIDatatable.createTable([]);
            }
        });
    },

    createTable: function (dataSet) {
        POIDatatable.instance = POIDatatable.table.DataTable({
            data: dataSet,
            order: [[0, 'asc']],
            scrollY: window.innerHeight > 900 ? 500 : 400,
            scrollCollapse: true,
            autoWidth: true,
            columnDefs: [
                {
                    targets: 0,
                    render: function(data, type, row) {
                        return data.substr(0, 20);
                    }
                },
                {
                    targets: 1,
                    className: "curlocation_column"
                },
                {
                    targets: [2],
                    render: function(data, type, row) {
                        if(!data.created) {
                            return "";
                        }
                        var created = moment(data.created).format('DD/MM/YYYY HH:mm');
                        var editDate = data.updated ? moment(data.updated).format('DD/MM/YYYY') : "-:";
                        var editTime = data.updated ? moment(data.updated).format('HH:mm') : "-";
                        return '<span data-uk-tooltip="{pos:\'top\'}" title="วันที่เพิ่ม: ' + created + '" style="font-size:12px">' + editDate + '</sapn><br>' + editTime;
                    }
                },
                {
                    targets: 3,
                    width: "80px",
                    className: "uk-text-center",
                    render: POIDatatable.createActionRow
                }
            ],
            createdRow: function (row, data, index) {
                $(row).attr('data-row-id', data[3]._id);
            },
            drawCallback: function (settings) {
            }
        });
    },

    addNewRow: function (rowData) {

        if (rowData == undefined) { return false; }

        POIDatatable.instance.row.add([
            rowData.parkingname,
            rowData.curloc.toString(),
            {
                created: rowData.created || "",
                updated: rowData.updated || ""
            },
            rowData
        ]).draw(false);
    },

    updateRow: function (rowData) {

        var rowIndex = POIDatatable.findRowIndexById(rowData._id);
        if (rowIndex < -1) {
            UIkit.notify('not found row id.', { status: 'danger', pos: 'bottom-center' });
            return;
        }

        POIDatatable.instance.row(rowIndex).data([
            rowData.parkingname,
            rowData.curloc.toString(),
            {
                created: rowData.created || "",
                updated: rowData.updated || ""
            },
            rowData
        ]).draw(false);
    },

    removeRow: function (rowObj) {
        POIDatatable.instance.row(rowObj).remove().draw();
    },

    findRowIndexById: function (id) {
        var row = POIDatatable.table.find("[data-row-id=" + id + "]");
        return POIDatatable.instance.row(row).index();
    },

    createActionRow: function (data, type, row) {

        var id = data ? data._id : row[3]._id;
        var btnGroup = '<a onclick="setActiveMarker(\'' + id + '\', \'' + row[3].parkingname + '\', \'' + row[3].description + '\')" data-uk-tooltip="{pos:\'top\'}" title="รายละเอียด"><i class="md-icon material-icons">&#xE88F;</i></a>';
        btnGroup += '<a onclick="showEditPOIBox(\'' + id + '\');" data-uk-tooltip="{pos:\'top\'}" title="แก้ไขข้อมูล"><i class="md-icon material-icons">&#xE254;</i></a>';
        btnGroup += '<a onclick="removePOI(\'' + id + '\');" data-uk-tooltip="{pos:\'top\'}" title="ลบ"><i class="md-icon material-icons">&#xE872;</i></a>';
        return btnGroup;
    }
}

var Loader = {
    show: function () {
        return UIkit.modal.blockUI('<div class="uk-text-center"><img class="uk-margin-top" src="assets/theme/altair/img/spinners/spinner.gif" alt=""><br/>กำลังทำงาน...</div>')
    }
}


var map;
var bounds;
var infowindow;
var markers = [];
var autocomplete;
var newLocationMarker;
var markerSearch;
var activeMarker;


function initMap() {

    setTimeout(function() {
        if (window.innerWidth <= 1440) {
            $("body").removeClass("sidebar_main_active sidebar_main_open");
        }
    }, 2000);

    var current = { lat: USER_CURRENT_LOCATION[0], lng: USER_CURRENT_LOCATION[1] };

    map = new google.maps.Map(document.getElementById('gmap'), {
            center: current,
            zoom: 10,
            disableDefaultUI: true
    });

    infowindow = new google.maps.InfoWindow({ maxWidth: 200 });
    google.maps.event.addDomListener(window, 'load', initializeAutocomplete);
}

function createMarker(id, location, text) {

    var marker = new google.maps.Marker({
        map: map,
        position: location,
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(text);
        infowindow.open(map, this);
    });

    markers.push({ id: id, obj: marker });
}

function findMarkerIndexById(id) {

        var index;

        for (var i = 0; i < markers.length; i++) {
            if (markers[i].id === id) {
                index = i;
                break;
            }
        }
        return index;
}

function removeMarker(id) {
    var index = findMarkerIndexById(id);
    markers[index].obj.setMap(null);
    markers.splice(index, 1);
}

function setDraggableMarker(id, draggable, infoContent) {
    var index = findMarkerIndexById(id);
    if(index === -1) {
        return;
    }
    if(!markers[index]) {
        return;
    }
    var marker = markers[index].obj;
    marker.setDraggable(draggable);
    if(draggable) {
        map.panTo(marker.getPosition());
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
        if(infoContent) {
            infowindow.setContent(infoContent);
            infowindow.open(map, marker);
        }
    } else {
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
    }
    return marker;
}

function addNewPOIMarker(location) {

    if (!location) {
        location = map.getCenter();
    }

    if(POIDatatable.instance.rows()[0].length >= C.MAX_POI) {
        UIkit.notify('ขออภัย จำนวน POI เกินโควต้าแล้ว (' + C.MAX_POI + ' จุด)', { status: 'danger', pos: 'bottom-center' });
        return;
    }

    if (newLocationMarker instanceof google.maps.Marker) {
        newLocationMarker.setMap(null);
    }

    newLocationMarker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: location,
        animation: google.maps.Animation.DROP,
        icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    });

    google.maps.event.addListener(newLocationMarker, 'dblclick', function () {
        hideAddNewPOIBox();
    });

    showAddNewPOIBox();

    setTimeout(function () {
        infowindow.setContent("ลาก marker เพื่อกำหนดจุด");
        infowindow.open(map, newLocationMarker);
    }, 600);
}

function setActiveMarker(id, title, description) {

    setInActiveMarker();

    markers.filter(function (e, i) {
        if (e.id === id) {
            activeMarker = i;
            map.setCenter(e.obj.getPosition());
            if(description) {
                title = '<b>' + title + '</b><br>' + description;
            }
            e.obj.setIcon(new google.maps.MarkerImage(
                "http://maps.google.com/mapfiles/ms/icons/purple-dot.png",
                null, /* size is determined at runtime */
                null, /* origin is 0,0 */
                null, /* anchor is bottom center of the scaled image */
                new google.maps.Size(40, 40)
            ));
            infowindow.setContent(title);
            infowindow.open(map, e.obj);
            setView(e.obj.getPosition());
            return;
        }
    });
}

function setView(center) {
    if(map && map.getZoom() < 15) {
        map.setZoom(15);
        if(center) {
            map.panTo(center);
        }
    }
}

function setInActiveMarker() {
    if(activeMarker > -1) {
        var markerObj = markers[activeMarker];
        if(markerObj) {
            markerObj.obj.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
        }
    } else {
        activeMarker = null;
    }
}

function defindBoundMarker() {
    bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markers.length; i++) {
        bounds.extend(markers[i].obj.getPosition());
    }

    map.fitBounds(bounds);
}

function showAddNewPOIBox() {
    cancelEditPOI();
    $("#poi-table-wrapper").addClass("hide");
    $('#add-poi-form-wrapper').addClass('show');
}

function showEditPOIBox(id) {

    infowindow.close();

    if(!id) {
        return;
    }
    
    var row;
    var rowIndex = POIDatatable.findRowIndexById(id);
    if(rowIndex > -1) { 
        row = POIDatatable.instance.row(rowIndex);
    }

    if(!row) {
        UIkit.notify('not found row id.', { status: 'danger', pos: 'bottom-center' });
        return;
    }

    var poiData = row.data()[3];

    var oldData = $("#edit-poi-form-wrapper").data();
    if(!jQuery.isEmptyObject(oldData)) {
        setDraggableMarker(oldData._id, false);
    }
    
    var $parkingname = $('#edit-poi-form-wrapper').find('input[name=parkingname]');
    var $description = $('#edit-poi-form-wrapper').find('input[name=description]');

    $parkingname.val(poiData.parkingname);
    $description.val(poiData.description || "");
    $('#edit-poi-form-wrapper').data(poiData);

    var marker = setDraggableMarker(id, true, poiData.parkingname);
    setView(marker.getPosition());
    $("#poi-table-wrapper").addClass("hide");
    $('#edit-poi-form-wrapper').addClass('show');
}

function hideAddNewPOIBox() {
    if (newLocationMarker instanceof google.maps.Marker) {
        newLocationMarker.setMap(null);
        newLocationMarker = null;
    }
    
    $("#poi-table-wrapper").removeClass("hide");
    $('#add-poi-form-wrapper').removeClass('show');
    $('#add-poi-form-wrapper').find('input[type=text]').val('');
}

function cancelEditPOI() {
    var $form = $('#edit-poi-form-wrapper');
    var data = $form.data(); 
    var currentMarker = setDraggableMarker(data._id, false);
    if(currentMarker) {
        currentMarker.setPosition({ lat: data.curloc[1], lng: data.curloc[0] });
    }
    hideEditPOIBox();
}

function hideEditPOIBox() {
    $("#poi-table-wrapper").removeClass("hide");
    $('#edit-poi-form-wrapper').removeClass('show');
    $('#edit-poi-form-wrapper').find('input[type=text]').val('');
    jQuery.removeData($('#edit-poi-form-wrapper')[0]);
}

function saveNewPOI(event, button) {

    var $parkingNameInput = $('form#add-poi-form-wrapper').find('input[name=parkingname]');
    var $descriptionInput = $('form#add-poi-form-wrapper').find('input[name=description]');

    if(!newLocationMarker) {
        UIkit.notify("not found location.", { status: 'danger', pos: 'bottom-center' });
        return;
    }

    if(!$parkingNameInput.val()) {
        $parkingNameInput.focus();
        UIkit.notify("Pls enter name.", { status: 'warning', pos: 'bottom-center' });
        return;
    }

    var curloc = [parseFloat(newLocationMarker.getPosition().lng().toFixed(6)), parseFloat(newLocationMarker.getPosition().lat().toFixed(6))];
    var parkingname = $parkingNameInput.val();
    var description = $descriptionInput.val();

    var loader = Loader.show();

    $.post(C.serviceURI.addPOI, { curloc: curloc, parkingname: parkingname, description: description }, function (res) {
        setTimeout(function() {
            loader.hide();
            if (res.status) {
                UIkit.notify('create new poi success.', { status: 'success', pos: 'bottom-center' });
                hideAddNewPOIBox();
                createMarker(res.data._id, { lat: res.data.curloc[1], lng: res.data.curloc[0] }, res.data.parkingname);
                POIDatatable.addNewRow(res.data);
            } else {
                UIkit.notify(res.data || res.msg, { status: 'danger', pos: 'bottom-center' });
            }
        }, 700);
    });
}

function editPOI(event, button) {

    var data = $('form#edit-poi-form-wrapper').data();
    var id = data._id;

    var $parkingNameInput = $('form#edit-poi-form-wrapper').find('input[name=parkingname]');
    var $descriptionInput = $('form#edit-poi-form-wrapper').find('input[name=description]');

    if(!id) {
        UIkit.notify("not found marker id.", { status: 'danger', pos: 'bottom-center' });
        return;
    }

    if(!$parkingNameInput.val()) {
        $parkingNameInput.focus();
        UIkit.notify("Pls enter name.", { status: 'warning', pos: 'bottom-center' });
        return;
    }

    var marker = markers[findMarkerIndexById(id)].obj;
    
    var curloc = [parseFloat(marker.getPosition().lng().toFixed(6)), parseFloat(marker.getPosition().lat().toFixed(6))];
    var parkingname = $parkingNameInput.val();
    var description = $descriptionInput.val();

    var loader = Loader.show();

    $.post(C.serviceURI.editPOI, { _id: id, curloc: curloc, parkingname: parkingname, description: description }, function (res) {
        setTimeout(function() {
            loader.hide();
            if (res.status) {
                UIkit.notify('edit poi success.', { status: 'success', pos: 'bottom-center' });
                setDraggableMarker(data._id, false);
                hideEditPOIBox();
                POIDatatable.updateRow(res.data);
            } else {
                UIkit.notify(res.data || res.msg, { status: 'danger', pos: 'bottom-center' });
            }
        }, 700);
    });
}

function removePOI(id) {
    
    var row;
    var rowIndex = POIDatatable.findRowIndexById(id);
    if(rowIndex > -1) { 
        row = POIDatatable.instance.row(rowIndex);
    }

    if(!row) {
        UIkit.notify('not found row id.', { status: 'danger', pos: 'bottom-center' });
        return;
    }
    
    UIkit.modal.confirm('ท่านต้องการลบจุดนี้ใช่หรือไม่', function () {
        $.post(C.serviceURI.deletePOI, { _id: id }, function (res) {
            if (res.status) {
                UIkit.notify('Remove poi success.', { status: 'success', pos: 'bottom-center' });
                POIDatatable.removeRow(row);
                removeMarker(id);
            } else {
                UIkit.notify(res.data || res.msg, { status: 'danger', pos: 'bottom-center' });
            }
        });
    }, { labels: {'Ok': 'ตกลง', 'Cancel': 'ยกเลิก'}});
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].obj.setMap(null);
    }

    markers = [];
}

function fitBoundSearch() {

    var findMarker = markers[findMarkerIndexById($('#edit-poi-form-wrapper').data()._id)];
    if(findMarker) {
        var editMarker = findMarker.obj;
    }
    
    if(editMarker && markerSearch) {
        var bounds = new google.maps.LatLngBounds();

        bounds.extend(markerSearch.getPosition());
        bounds.extend(editMarker.getPosition());

        map.fitBounds(bounds);
    }
}

function initializeAutocomplete() {
    var input = document.getElementById('search-poi-input');
    autocomplete = new google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);
    
    markerSearch = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
        });

    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        markerSearch.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            UIkit.notify("No details available for input: '" + place.name + "'", { status: 'danger', pos: 'bottom-center' });
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }

        markerSearch.setPosition(place.geometry.location);
        markerSearch.setVisible(true);
        fitBoundSearch();
    });
}

function exportPOI(e) {
    var loader = Loader.show();
    e.preventDefault();  //stop the browser from following
    window.location.href = '/garage/poi/export';
    setTimeout(function() {
        loader.hide();
    }, 700);
}

function clickZoom(click) {
    var zoomOut = 0;
    var zoomIn = 1;
    if (click === 1) {
        map.setZoom(map.getZoom() + 1);
    }
    if (click === 0) {
        map.setZoom(map.getZoom() - 1);
    }
}


App.bootstrap();