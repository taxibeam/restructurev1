var UbeamTPServiceListener = function(appContext) {

	return new appContext.HttpService.ServiceListener(appContext, {

		getAllTaxi: function(query, callback) {
			this.post(C.service.ubeam.searchdrv, query, callback); 
		},
		
		getAllDriver: function(query, callback) {
			this.post(C.service.drivercs.all, query, callback);
		}

	});
};