var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    serviceURI: {
        getSettings: serviceBaseURI + '/service/garage/settings/all',
        saveSettings: serviceBaseURI + '/service/garage/settings/save'
    },
    driverImageURL: 'http://localhost:8888/uploads/'
    // driverImageURL: 'http://callcenter-test.taxi-beam.com/image/driver/'
};

var C = CONSTRAINTS;


var App = {
    name: "settings",
    bootstrap: function () {

        this.setActiveSideBar();
        FormSetting.init();
    },
    setActiveSideBar: function() {
        $("aside#sidebar_main").find('.menu_section > ul > li[title="' + this.name + '"]').addClass("current_section");
    },
    reload: function() {
        // location.reload();
        FormSetting.loadSetting(function(response) {
            UIkit.notify("อัพเดทเรียบร้อย", { pos: 'bottom-center', status: 'success' });
        });
    }
}


var FormSetting = {
    form: $("#setting_form"),
    init: function() {
        this.loadSetting();
    },
    submit: function() {

        event.preventDefault();

        var formValue = FormSetting.form.serializeArray();
        formValue.push({ name: 'garageId', value: USER_DATA._id_garage });
        
        var loader = Loader.show();

        apiClient.saveSettings(formValue, function (response) {
            setTimeout(function () {
                loader.hide();
                if (response.status) {
                    UIkit.notify('บันทึกข้อมูลเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                } else {
                    UIkit.notify("พบปัญหาบางอย่าง!", { pos: 'bottom-center', status: 'error' });
                }
            }, 1250);
        });

        return;
    },
    loadSetting: function(callback) {

        if(USER_DATA === undefined || USER_DATA._id_garage === undefined) {
            return;
        }

        apiClient.getSettings({ garageId: USER_DATA._id_garage}, function(response) {

            var ccwaitingtime = FormSetting.form.find('[name=ccwaitingtime]')[0].selectize;
            ccwaitingtime.setValue(response.data.ccwaitingtime);

            var searchpsgamount = FormSetting.form.find('[name=searchpsgamount]')[0].selectize;
            searchpsgamount.setValue(response.data.searchpsgamount);
            
            var searchpsgradian = FormSetting.form.find('[name=searchpsgradian]')[0].selectize;
            searchpsgradian.setValue(response.data.searchpsgradian);

            var bidjobwaitingtime = FormSetting.form.find('[name=bidjobwaitingtime]')[0].selectize;
            bidjobwaitingtime.setValue(response.data.bidjobwaitingtime);

            var bidjobradian = FormSetting.form.find('[name=bidjobradian]')[0].selectize;
            bidjobradian.setValue(response.data.bidjobradian);                        
            
            var broadcast = FormSetting.form.find('[name=broadcast]');
            if(response.data.broadcast && !broadcast[0].checked) {
                broadcast.parent().find('.switchery').click();
            } 
            else if (!response.data.broadcast && broadcast[0].checked) {
                broadcast.parent().find('.switchery').click();
            }
            
            var allowadjustpoi = FormSetting.form.find('[name=allow_adjust_poi]');
            if(response.data.allow_adjust_poi && !allowadjustpoi[0].checked) {
                allowadjustpoi.parent().find('.switchery').click();
            } 
            else if (!response.data.allow_adjust_poi && allowadjustpoi[0].checked) {
                allowadjustpoi.parent().find('.switchery').click();
            }

            if(typeof callback === "function") {
                callback(response);
            }
        });
    },
}



var apiClient = {
    onFail: function() {
            loader.hide();
            UIkit.notify("พบปัญหาบางอย่าง!", { pos: 'bottom-center', status: 'error' });
    },
    getSettings: function(data, callback) {
        $.post(C.serviceURI.getSettings, data, callback).fail(apiClient.onFail);
    },
    saveSettings: function(data, callback) {
        $.post(C.serviceURI.saveSettings, data, callback).fail(apiClient.onFail);
    }
}

var Loader = {
    show: function () {
        return UIkit.modal.blockUI('<div class="uk-text-center"><img class="uk-margin-top" src="assets/theme/altair/img/spinners/spinner.gif" alt=""><br/>กำลังทำงาน...</div>')
    }
}

App.bootstrap();