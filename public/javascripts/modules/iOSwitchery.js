var iOSwitchery = {
	initial: function () {
		this.create();
		this.eventHandler();
	},
	eventHandler: function () {

		$(".taxi-counter").find(".js-switch").on('change', function (event) {
			var button = $(event.target).closest(".list-group-item");
			if (button.hasClass("OFF")) {
				Taxi.toggleStatus("OFF", this.checked);
			}
			else if (button.hasClass("ON")) {
				Taxi.toggleStatus("ON", this.checked);
			}
			else if (button.hasClass("WAIT")) {
				Taxi.toggleStatus("WAIT", this.checked);
			}
			else if (button.hasClass("DPENDING")) {
				Taxi.toggleStatus("DPENDING", this.checked);
			}
			else if (button.hasClass("BUSY")) {
				Taxi.toggleStatus("BUSY", this.checked);
			}
			else if (button.hasClass("PICK")) {
				Taxi.toggleStatus("PICK", this.checked);
			}
			else if (button.hasClass("ASSIGNED")) {
				Taxi.toggleStatus("ASSIGNED", this.checked);
			}
			else if (button.hasClass("BROKEN")) {
				Taxi.toggleStatus("BROKEN", this.checked);
			}
			else if (button.hasClass("PARKING")) {
				LandMark.toggleDisplay(this.checked);
			}
		});
	},
	create: function () {

		$(".taxi-counter").find(".js-switch").each(function (i, html) {
			var switchery = new Switchery(html, {
				size: 'small'
			});
		});
	}
};