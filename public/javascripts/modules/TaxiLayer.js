var TaxiLayer = function(ApplicationContext) {

	var context = ApplicationContext || getApplicationContext();

	var Map = context.Map;
	var layer = this.layer = L.layerGroup();

	if( !Map instanceof L.Map ) { throw "TypeError :: data is not defineValues"; }

	layer.addTo(Map);

	this.addTaxi = function(taxiMarker) {
		layer.addLayer(taxiMarker);
	}
};