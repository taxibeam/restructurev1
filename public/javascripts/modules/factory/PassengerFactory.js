var PassengerMarker = function(data) {

	if ( this === window ) throw "InitializeError :: Cannot direct use this without new class.";

	var passenger = this.data = $.extend({}, data);

	this.addToMap = function(passengerLayer) {

		if( !passengerLayer instanceof PassengerLayer ) { throw "TypeError :: data is not PassengerLayer"; }

		var marker = this.createMarker();
		passengerLayer.addMarker(marker);
	}

	this.createMarker = function() {

		var positionStyle = L.divIcon({
				className: "passenger-pin",
				html: '<div class="wrapper"><div class="pin"><i class="material-icons">record_voice_over</i></div><div class="pulse"></div></div>',
            });

		var passengerMarker = L.marker([passenger.curloc[1], passenger.curloc[0]], {
			riseOnHover: true,
			icon: positionStyle
		});

		var popupContent = this.createPopup();

		passengerMarker.bindPopup(popupContent);
		passengerMarker.options.passengerMarkerInfo = passenger;

		return passengerMarker;
	}


	this.createPopup = function() {

		var HTMLPopup = '<div class="passenger-popup">';

		HTMLPopup += '<p class="caption">เบอร์โทร</p>';
		HTMLPopup += '<p class="value">' + passenger.phone + '</p>';
		HTMLPopup += '<p class="caption">ตำแหน่งเรียกรถ</p>';
		HTMLPopup += '<p class="value">' + passenger.curaddr + '</p>';
		HTMLPopup += '<p class="caption">จุดหมาย</p>';
		HTMLPopup += '<p class="value">' + passenger.destination + '</p>';
		HTMLPopup += '<p class="caption">รายละเอียด</p>';
		HTMLPopup += '<p class="value">' + passenger.detail + '</p>';

		HTMLPopup += '</div>';

		return HTMLPopup;
	}
};

var PassengerLayer = function(ApplicationContext) {

	var context = ApplicationContext || getApplicationContext();

	var Map = context.Map;
	var layer = this.layer = L.layerGroup();

	if( !Map instanceof L.Map ) { throw "TypeError :: data is not LeafletJS Map"; }

	layer.addTo(Map);

	this.addMarker = function(passengerMarker) {
		layer.addLayer(passengerMarker);
	}

	this.clear = function() {
		layer.clearLayers();
	}
};