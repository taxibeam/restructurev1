var AdminMonitorApp = function(appValues) {


	// Extending class ApplicationActivities
	// # first param is app options (this.options)
	$.extend(this, {}, new ApplicationActivities({}, this));


	// Set Constraints with appValues
	this.setConstraints(appValues);


	// Create instant of HttpService 
	// and Register Http Service with a list of ServiceListener.
	var httpService = this.HttpService;
	httpService.registerService(new UbeamTPServiceListener(this));


	// Create Map object and render.
	var Map = this.Map = new HobbitMap();


	// Create Taxi LayerGroup and add to Map
	var taxiLayer = this.taxiLayer = new TaxiLayer();


	// Private function :: Display all taxies on Map.
	var displayTaxi = function() {

		httpService.getAllTaxi({
			curlng: Map.getCenter().lng,
			curlat: Map.getCenter().lat
		}, function(response) {
			if(!response) { return; }

 			response.data.filter(function(taxiInfo) {
 				new TaxiDriver(taxiInfo).addToMap(taxiLayer);
 			});

 			window.setTimeout(function() {
 				Map.locate({ setView: true }).setZoom(C.map.baseZoom || 14);
 			}, 1000);
		});
	};


	this.onCreate = function() {
		iOSwitchery.initial();
		displayTaxi();
	};

    // toggleStatus: function (status, show) {
    //     if (show) {
    //         var taxi = Monitoring.taxi.filter(function (taxi) {
    //             return taxi.status == status;
    //         });
    //         taxi.length > 0 && Taxi.addToMap(taxi, { monitoring: true }, false);
    //     }
    //     else {
    //         Taxi.layer.eachLayer(function (taxi) {
    //             if (taxi.options.taxi_info.status == status) {
    //                 Taxi.layer.removeLayer(taxi);
    //             }
    //         });
    //     }
    // }

	this.onCreate();
};