var serviceBaseURI = window.location.origin,



    CONSTRAINTS = {
    	serviceURI: {
    		psgCallLog: serviceBaseURI + '/service/log/PsgCallLog',
    		joblistLog: serviceBaseURI + '/service/log/JoblistLog',
            getMobileLog: serviceBaseURI + '/service/log/getMobileLog',
            getJobListLogWait: serviceBaseURI + '/service/ubeam/getJobListLogWait',

            getJobListLogPick: serviceBaseURI + '/service/ubeam/getJobListLogPick',
            getJobListLogAccept: serviceBaseURI + '/service/ubeam/getJobListLogAccept',
            getJobListLogDrop: serviceBaseURI + '/service/ubeam/getJobListLogDrop',
            getJobListLogPSGCancel: serviceBaseURI + '/service/ubeam/getJobListLogPSGCancel',
            getJobListLogDRVCancel: serviceBaseURI + '/service/ubeam/getJobListLogDRVCancel',

            getCallcenterLog: serviceBaseURI + '/service/log/getCallcenterLog',

            getAllGarage: serviceBaseURI + '/service/ubeam/getAllGarage'
        },
        viewMode: [
        { th: "รายชั่วโมง", en: "hour" }, 
        { th: "รายวัน", en: "day" }, 
        { th: "รายเดือน", en: "month" }
        ],
        daterangepicker: {
            options: {
                opens: "left",
                timePicker: false,
                autoUpdateInput: true,
                autoApply: true,
                applyClass: "btn-success hide",
                cancelClass: "btn-default hide",
                dateLimit: {
                    months: 1
                },
                ranges: {
                    "วันนี้": [moment(), moment()],
                    "เมื่อวาน": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    "7 วันที่แล้ว": [moment().subtract(6, 'days'), moment()],
                    "30 วันที่แล้ว": [moment().subtract(29, 'days'), moment()],
                    "เดือนนี้": [moment().startOf('month'), moment().endOf('month')],
                    "เดือนที่แล้ว": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                locale: {
                    format: "DD/MM/YYYY HH:mm",
                    separator: " - ",
                    applyLabel: "ตกลง",
                    cancelLabel: "ยกเลิก",
                    fromLabel: "จาก",
                    toLabel: "ถึง",
                    customRangeLabel: "กำหนดเอง",
                    daysOfWeek: [
                    "อ.",
                    "จ.",
                    "อ.",
                    "พ.",
                    "พฤ.",
                    "ศ.",
                    "ส."
                    ],
                    monthNames: [
                    "มกราคม",
                    "กุมภาพันธ์",
                    "มีนาคม",
                    "เมษายน",
                    "พฤษภาคม",
                    "มิถุนายน",
                    "กรกฎาคม",
                    "สิงหาคม",
                    "กันยายน",
                    "ตุลาคม",
                    "พฤศจิกายน",
                    "ธันวาคม"
                    ],
                    firstDay: 1
                },
                alwaysShowCalendars: false,
            }
        }
    },
    C = CONSTRAINTS;


var App = {
	initial: function() {

        BootstrapCustom.initial();

        FilterControl.initial();
        DateRagePicker.initial();
        GarageList.initial();
        ViewModeList.initial();
        ProvinceList.initial();

        ToggleChart.initial();
        TabControl.initial();

        DataTable.PsgCallLogTable.initial();
        DataTable.JoblistLogTable.initial();
        
        Highcharts.setOptions({
            global: {
                useUTC: false,
                timezoneOffset: 7 * 60
            }
        });
    }
};


var BootstrapCustom = {
    initial: function() {
        this.setTooltip();
        this.setContentRightHigh();
    },
    setTooltip: function() {
        $('[data-toggle="tooltip"]').tooltip();
    },
    setContentRightHigh: function() {
        $("#content-right").css({
            minHeight: 860,
            maxHeight: 860,
        });
    }
};


var DataTable = {

    PsgCallLogTable: {

        $table: null,

        datatable: null,

        initial: function () {

            DataTable.PsgCallLogTable.$table = $("#PsgCallLogTable");

            this.setUp();
            this.initialEventListener();
        },

        initialEventListener: function() {
        },

        setUp: function() {

            DataTable.PsgCallLogTable.datatable = DataTable.PsgCallLogTable.$table.dataTable({
                paging: true,
                scrollY: "650px",
                deferRender: true,
                scrollCollapse: true,
                iDisplayLength: 100,
                language: {
                    search: "",
                    searchPlaceholder: "ค้นหา",
                    sInfo: "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ",
                },
                columns: [
                { title: "#" },
                { title: "อู่" },
                { title: "เบอร์โทรผู้โดยสาร" },
                { title: "เบอร์โทรคนขับ" },
                { title: "ทะเบียน" },
                { title: "ตำแหน่งที่กด" },
                { title: "เวลาโทร" }
                ],
                createdRow: function (row, data, index) {
                    row.setAttribute("data-ref", data[7]);
                },
                drawCallback: function (settings) {
                    Preloader.hide(0);
                }
            });
        },

        render: function(data) {

            var tasks = []; 
            data.filter(function(task, index) {
                tasks.push([
                    (index + 1).toString(),
                    task.cgroup || "",
                    task.psg_phone || "",
                    task.drv_phone || "",
                    task.drv_carplate || "",
                    task.callby || "",
                    moment(task.created).format("DD/MM/YYYY HH:mm น. (ddd)") || "",
                    task._id
                    ]);
            });

            var oTable = DataTable.PsgCallLogTable.datatable;

            oTable.fnClearTable();
            
            if(tasks.length > 0)
                oTable.fnAddData(tasks);
        },

        clear: function() {
            DataTable.PsgCallLogTable.datatable.fnClearTable();
        },

        getData: function(query) {

            $.post(C.serviceURI.psgCallLog, query, function(response) {
                if (response.status) {
                    Notification.remove();
                    DataTable.PsgCallLogTable.render(response.data);
                } else {
                    DataTable.PsgCallLogTable.clear();
                    Notification.show({
                        time: 5000,
                        title:'ไม่มีข้อมูลที่ค้นหา',
                        text: 'ไม่พบข้อมูลระหว่างช่วงเวลาดังกล่าว',
                        $class: "warning"
                    });
                }
            }, 'json');
        },

        EevetHandler: {
            onInit: function() {
            }
        }
    },

    JoblistLogTable: {

        $table: $("#JoblistLogTable"),

        datatable: null,

        initial: function () {
            this.setUp();
            this.initialEventListener();
            DataTable.JoblistLogTable.EevetHandler.onInit();
        },

        initialEventListener: function() {
        },

        setUp: function() {

            DataTable.JoblistLogTable.datatable = DataTable.JoblistLogTable.$table.dataTable({
                paging: true,
                autoWidth: true,
                scrollY: "650px",
                deferRender: true,
                scrollCollapse: true,
                iDisplayLength: 10,
                language: {
                    search: "",
                    searchPlaceholder: "ค้นหา",
                    sInfo: "กำลังแสดงรายการที่ _START_ ถึง _END_ จากทั้งหมด _TOTAL_ รายการ",
                },
                columns: [
                { title: "#" },
                { title: "เบอร์ผู้โดยสาร" },
                { title: "ชื่อคนขับ" },
                { title: "เบอร์คนขับ" },
                { title: "ทะเบียน" },
                { title: "จังหวัด" },
                { title: "อู่" },
                { title: "จุดรับ" },
                { title: "จุดหมาย" },
                { title: "เวลาเรียกงาน" },
                { title: "เท็กซี่รับงาน" },
                { title: "ผู้โดยสารตอบรับ" },
                { title: "แท็กซี่รับผู้โดยสาร" },
                { title: "แท็กซี่ส่งผู้โดยสาร" },
                { title: "ผู้โดยสารยกเลิก" },
                { title: "แท็กซี่ยกเลิก" },
                ],
                columnDefs: [
                {
                    targets: [8,9,10,11,12,13],
                    width: "70px"
                }],
                createdRow: function (row, data, index) {
                    row.setAttribute("data-ref", data[5]);
                },
                drawCallback: function (settings) {
                    Preloader.hide(0);
                }
            });
        },

        render: function(data) {

        	var datedrvwait = 0;
        	var datepsgaccept = 0;
        	var datedrvpick = 0;
        	var datedrvdrop = 0;
        	var datepsgcancel = 0;
        	var datedrvcancel = 0;

            var tasks = []; 
            data.filter(function(task, index) {
                tasks.push([
                    (index + 1),
                    task.psg_phone || "",
                    task.drv_name || "",
                    task.drv_phone || "",
                    task.drv_carplate || "",
                    task.NameTH || "-",
                    task.cgroup || "-",
                    task.curaddr || "",
                    task.destination || "",
                    moment(task.datepsgcall).format("DD/MMM/YYYY") + " " + moment(task.datepsgcall).format("HH:mm น."),
                    typeof task.datedrvwait != 'undefined' ? '<i class="material-icons">phonelink_ring</i>' : "",
                    typeof task.datepsgaccept != 'undefined' ? '<i class="material-icons">phonelink_ring</i>' : "",
                    typeof task.datedrvpick != 'undefined' ? '<i class="material-icons">local_taxi</i>' : "",
                    typeof task.datedrvdrop != 'undefined' ? '<i class="material-icons">airline_seat_recline_extra</i>' : "",
                    typeof task.datepsgcancel != 'undefined' ? '<i class="material-icons">highlight_off</i>' : "",
                    typeof task.datedrvcancel != 'undefined' ? '<i class="material-icons">highlight_off</i>' : "",
                    task._id
                    ]);

                typeof task.datedrvwait != 'undefined' && datedrvwait++;
                typeof task.datepsgaccept != 'undefined' && datepsgaccept++;
                typeof task.datedrvpick != 'undefined' && datedrvpick++;
                typeof task.datedrvdrop != 'undefined' && datedrvdrop++;
                typeof task.datepsgcancel != 'undefined' && datepsgcancel++;
                typeof task.datedrvcancel != 'undefined' && datedrvcancel++;
            });

            var summaryCircle = $("#JoblistLogTab").find(".app-mobile.summary-circle-group");
            summaryCircle.find(".datepsgcall").find(".number").text(tasks.length);

            summaryCircle.find(".datedrvwait").find(".number").text(datedrvwait);
            summaryCircle.find(".datepsgaccept").find(".number").text(datepsgaccept);
            summaryCircle.find(".datedrvpick").find(".number").text(datedrvpick);
            summaryCircle.find(".datedrvdrop").find(".number").text(datedrvdrop);
            summaryCircle.find(".datepsgcancel").find(".number").text(datepsgcancel);
            summaryCircle.find(".datedrvcancel").find(".number").text(datedrvcancel);

            var oTable = DataTable.JoblistLogTable.datatable;

            oTable.fnClearTable();
            
            if(tasks.length > 0)
                oTable.fnAddData(tasks);
        },

        clear: function() {
            DataTable.JoblistLogTable.datatable.fnClearTable();
        },

        getData: function(data) {
        	Http.post(C.serviceURI.joblistLog, {
                data: data,
                onSuccess: function (response) {
                 if (response.status) {
                  Notification.remove();
                  DataTable.JoblistLogTable.render(response.data);
              }
              else {
                  DataTable.JoblistLogTable.clear();
                  Notification.show({
                   time: 5000,
                   title:'ไม่มีข้อมูลที่ค้นหา',
                   text: 'ไม่พบข้อมูลระหว่างช่วงเวลาดังกล่าว',
                   $class: "warning"
               });
              }
          }
      });
        },

        EevetHandler: {
            onInit: function() {

            }
        }
    }
};


var DateRagePicker = {

    picker: null,

    displayText: $("#date-range-display-text"),

    initial: function() {
        this.setUp();
    },

    setUp: function() {

        var daterange = $(".daterange");

        this.picker = daterange.daterangepicker(C.daterangepicker.options, DateRagePicker.EevetHandler.onChosen)
            .on('apply.daterangepicker', DateRagePicker.EevetHandler.onApply);

        this.picker = daterange.data('daterangepicker');

        var startDate = moment().subtract(1, "months");
        var endDate = moment();

        this.picker.setStartDate(startDate);
        this.picker.setEndDate(endDate);

        this.setInputValue(startDate, endDate);
        this.setDisplayValue();
    },

    getData: function() {
        return {
            startTime: DateRagePicker.picker.startDate.valueOf(),
            endTime: DateRagePicker.picker.endDate.valueOf()
        }
    },

    setInputValue: function(start, end) {
        $(this.picker.element).find("span").html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'))
    },

    setDisplayValue: function() {

        var timeText = "";
        var diffTime = DateRagePicker.picker.startDate.diff(DateRagePicker.picker.endDate, "hours");
        diffTime = Math.abs(diffTime);

        if(diffTime > 24) {
            var start = DateRagePicker.picker.startDate.format("DD/MM/YYYY");
            var end = DateRagePicker.picker.endDate.format("DD/MM/YYYY");
            timeText = start + ' - ' + end;
        }
        else {
            timeText = DateRagePicker.picker.startDate.format("dddd DD MMMM YYYY");
        }
        DateRagePicker.displayText.find("h4").html(timeText);
    },

    EevetHandler: {

        onChosen: function(start, end) {
            $(this.element).find("span").html(start.format('DD/MM/YYYY HH:mm') + ' - ' + end.format('DD/MM/YYYY HH:mm'));
        },

        onApply: function(ev, picker) {

            var viewMode = ViewModeList.getData().viewMode;
            var endDate = DateRagePicker.picker.endDate;

            var diffTime = moment(picker.startDate).diff(picker.endDate, "hours");

            diffTime = Math.abs(diffTime);

            if(diffTime <= 24) {
                ViewModeList.select.find("option[value=hour]").prop("selected", true);
            }
            else if(diffTime > 24 && viewMode === "hour") {
                ViewModeList.select.find("option[value=day]").prop("selected", true);
            }
            else if(diffTime > 24 && diffTime <= (24*7*4) && viewMode === "week") {
                ViewModeList.select.find("option[value=day]").prop("selected", true);
            }
            else if(diffTime > 24 && diffTime <= (24*30) && viewMode === "month") {
                ViewModeList.select.find("option[value=day]").prop("selected", true);
            }

            DateRagePicker.setDisplayValue();

            var query = FilterControl.getQuery();
            FilterControl.render(query);
        }
    },
};


var FilterControl = {

    button: null,

    initial: function() {

        this.button = $("#submitFilter");
        this.initialEventListener();
    },

    initialEventListener: function() {
        this.button.on("click", FilterControl.EevetHandler.onSubmit);
    },

    render: function(query) {

        if(TabControl.activeTab.hash == "#PsgCallLogTab") {
            DataTable.PsgCallLogTable.getData(query);
        }
        else if(TabControl.activeTab.hash == "#JoblistLogTab") {
            HightCharts();
        }
        else if(TabControl.activeTab.hash == "#CallcenterLogTab") {
            new GraphService().createCallcenterGraph(query);
        }
    },

    setDisplayText: function(date) {

        var dateInfoText = "";
        var viewMode = ViewModeList.getData().viewMode;

        switch ( viewMode ) {
            case "hour" :
            dateInfoText = moment(date.startTime).format("HH:mm, DD MMM YYYY");
            dateInfoText += " ถึง "
            dateInfoText += moment(date.endTime).format("HH:mm, DD MMM YYYY");
            break;
            case "day" :
            dateInfoText = moment(date.startTime).format("DD/MM/YYYY (ddd)");
            dateInfoText += " ถึง "
            dateInfoText += moment(date.endTime).format("DD/MM/YYYY (ddd)");
            break;
            case "week" :
            dateInfoText = moment(date.startTime).format("DD/MM/YYYY");
            dateInfoText += " ถึง "
            dateInfoText += moment(date.endTime).format("DD/MM/YYYY");
            break;
            case "month" :
            dateInfoText = moment(date.startTime).format("MMMM YYYY");
            dateInfoText += " ถึง "
            dateInfoText += moment(date.endTime).format("MMMM YYYY");
            break;
        }


        FilterControl.displayText.find("h4").text(dateInfoText);
    },

    getQuery: function() {

        var daterange = DateRagePicker.getData();
        var garages = GarageList.getData();
        var viewMode = ViewModeList.getData();

        var query = $.extend(daterange, garages, viewMode);

        return query;
    },

    EevetHandler: {

        onSubmit: function() {
            var query = FilterControl.getQuery();
            FilterControl.render(query);
        }
    }
};


var GarageList = {

    initial: function() {

        this.select = $("#garage-list");
        this.setUp();
    },

    setUp: function() {

        this.getAllGarage(function(response) {

            GarageList.create(response.data);

            GarageList.select.multiselect({
                buttonWidth: '100%',
                selectAllText: 'เลือกทั้งหมด',
                includeSelectAllOption: true,
                enableClickableOptGroups: true,
                onChange: function(option, checked) {
                },
                onDropdownHide: function(event) {
                }
            });
        });
    },

    create: function(garages) {

        var optionGroup = "";
        var previousId = 0;

        garages.filter(function (garage) {

            var provinceObj = province.th.filter(function(p) {
                return p.id === garage.ProvinceID;
            })[0];

            function createOption() { 
                optionGroup += '<option id="garage-' + garage._id + '" value="' + garage.cgroup + '">' + garage.name + '</option>';
            }

            if(previousId === 0) {
                optionGroup += '<optgroup value="' + garage.ProvinceID + '" label="' + provinceObj.name + '">';
                createOption();
            }
            else if (previousId !== 0 && garage.ProvinceID !== previousId) {
                optionGroup += '</optgroup>';
                optionGroup += '<optgroup value="' + garage.ProvinceID + '" label="' + provinceObj.name + '">';
                createOption();
            }
            else if (previousId !== 0 && garage.ProvinceID === previousId) {
                createOption();
                optionGroup += '</optgroup>';
            }

            previousId = garage.ProvinceID;
        });

        this.select.empty();
        this.select.append(optionGroup);
    },

    setEvent: function() {

        this.select.on('change', function() {

            var data = $.extend({}, GarageList.getData(), DateRagePicker.getData());

            setTimeout(function() {

                if(TabControl.activeTab.hash == "#PsgCallLogTab") {

                    DataTable.PsgCallLogTable.getData(data);
                }
                else if(TabControl.activeTab.hash == "#JoblistLogTab") {

                    // DataTable.JoblistLogTable.getData(data);
                    HightCharts();
                    // new GraphService().createMobileAppGraph(data);
                }
                else if(TabControl.activeTab.hash == "#CallcenterLogTab") {
                    alert('hello');
                }
            }, 700);
        });
    },

    getAllGarage: function(callback) {

        Http.post(C.serviceURI.getAllGarage, {

            onSuccess: function (response) {

                if(!response.status) { return; }

                if(callback && typeof callback === "function") {
                    callback(response);
                }
            }
        });
    },

    getData: function() {


        var select = this.select;
        var cgroup = select.val();
        var province = [];

        if(cgroup === null)
            return;

        cgroup.forEach(function(group) {
            var provinceId = $(select).find("option[value=" + group + "]").closest("optgroup").attr("value");
            province.push(parseInt(provinceId));
        });

        province = province.filter(function(item, pos) {
            return province.indexOf(item) == pos;
        });

        return {
            cgroup: cgroup,
            province: province
        };
    },
};


var ProvinceList = {

    initial: function() {

        this.select = $("#province-list");
        this.setUp();
    },

    setUp: function() {

        ProvinceList.select.multiselect({
            buttonWidth: '100%',
            onDropdownHide: function(event) {
            }
        });

        this.select.next().find('.multiselect-container').find('input').filter(function(index, input) {
            var switchery = new Switchery(input, { size: 'small' });
        });
    },

    getData: function() {
        return { provinceList: this.select.val() };
    },
};


var TabControl = {

	activeTab: $("#reportTableControl").find(".active > a")[0],

    tab: $('a[data-toggle="tab"]'),

    initial: function() {
        this.initialEventListener();
        this.EevetHandler.onInit();
    },

    initialEventListener: function() {

        this.tab.on('shown.bs.tab', TabControl.EevetHandler.onChange);

        this.tab.on('shown.bs.tab', function (e) {
            TabControl.activeTab = e.target;
        });
    },

    EevetHandler: {

        onChange: function(e) {

            var tabName = e.target.hash;
            var query = FilterControl.getQuery();

            TabControl.activeTab = e.target;
            DataTable.PsgCallLogTable.datatable.fnDraw();
            DataTable.JoblistLogTable.datatable.fnDraw();

            if(tabName === "#PsgCallLogTab") {
                DataTable.PsgCallLogTable.getData(query);
            }
            else if(tabName === "#JoblistLogTab") {
                HightCharts();
            }
            else if(tabName === "#CallcenterLogTab") {
                new GraphService().createCallcenterGraph(query);
            }


            if(tabName === "#JoblistLogTab") {
                ViewModeList.select.parent().hide();
                DateRagePicker.picker.element.hide();
            }
            else if(tabName === "#CallcenterLogTab") {
                ViewModeList.select.parent().show();
                DateRagePicker.picker.element.show();
            }
            else if(tabName === "#PsgCallLogTab") {
                ViewModeList.select.parent().hide();
                DateRagePicker.picker.element.show();
            }
        },

        onInit: function() {
            HightCharts();
            // var query = FilterControl.getQuery();
            // DataTable.JoblistLogTable.getData(query);
        }
    }
};


var ToggleChart = {
    
    toggleControl: {},

    initial: function() {

        this.toggleControl = $("#toggle-chart");
        this.initialEventListener();
    },

    initialEventListener: function() {
        this.toggleControl.find("input[type=checkbox]").on("change", ToggleChart.EevetHandler.onChange);
    },

    EevetHandler: {
        onChange: function() {

            var aria = $(this).prop("name");
            var isActive = $(this).closest("label").hasClass("active");

            switch(aria) {

                case "daterage":
                isActive ? $(".app-mobile.summary-circle-group").show() : $(".app-mobile.summary-circle-group").hide();
                isActive ? $("#chart-summary").show() : $("#chart-summary").hide();
                break;

                case "highcharts":
                isActive ? $("#container-report").show() : $("#container-report").hide();
                break;

                case "datatable":
                isActive ?  $("#joblist-table-container").show() : $("#joblist-table-container").hide();
                break;
            }            
        }
    }
};


var Util = {

    formatMoney: function(n, c, d, t) {
        var c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    }
};


var ViewModeList = {

    initial: function() {

        this.select = $("#view-mode-list");
        this.initialEventListener();
        this.setUp();
    },

    initialEventListener: function() {
        this.select.on("change", ViewModeList.EevetHandler.onChange);
    },

    setUp: function() {
        this.create(C.viewMode);
    },

    create: function(mode) {

        var options = "";

        mode.filter(function(mode) {

            var setSelected = function(mode) {
                return mode === 'day' ? "selected" : "";
            }

            options += '<option id="mode-' + mode.en + '" value="' + mode.en +'" ' + setSelected(mode.en) + '>' + mode.th + '</option>';
        });

        this.select.empty();
        this.select.append(options);
    },

    getData: function() {
        return  { viewMode: ViewModeList.select.val() };
    },

    EevetHandler: {
        onChange: function() {

            var viewMode = this.value;
            var endDate = moment();

            switch (viewMode) {

                case "hour":
                    DateRagePicker.picker.setStartDate(moment().format("DD-MM-YYYY"));
                    DateRagePicker.picker.setEndDate(moment().format("DD-MM-YYYY"));
                break;
                case "day":
                    DateRagePicker.picker.setEndDate(moment().format("DD-MM-YYYY"));
                    DateRagePicker.picker.setStartDate(moment().subtract(6, "days").format("DD-MM-YYYY"));
                break;
                case "week":
                    DateRagePicker.picker.setEndDate(endDate);
                    DateRagePicker.picker.setStartDate(moment().subtract(4, "weeks"));
                break;
                case "month":
                    DateRagePicker.picker.setEndDate(endDate);
                    DateRagePicker.picker.setStartDate(moment().subtract(6, "months"));
                break;
            }

            DateRagePicker.setInputValue(DateRagePicker.picker.startDate, DateRagePicker.picker.endDate);
            DateRagePicker.setDisplayValue();

            var query = FilterControl.getQuery();
            FilterControl.render(query);
        }
    }
};


var GraphService = function () {

    var getData = function (url, query, callback) {
        Http.post(url, {
            data: query,
            onSuccess: function (response) {

                if (!response.status) { return; }

                if (callback && typeof callback === "function") {
                    callback(response);
                }
            }
        });
    };

    var getDateOfWeek = function(w) {
        return moment().week(w).format("DD MMM YYYY") + " - " + moment().week(w+1).format("DD MMM YYYY");
    }

    var generateData = function (columnName, data, axis) {

        var viewMode = ViewModeList.getData().viewMode;
        var column = [columnName];

        axis.forEach(function (date) {

            if (date === "x") { return; }

            var result = data.find(function (job) {

                if (!job._id) { return; }

                var currentValue = "";

                switch (viewMode) {

                    case "hour":
                    currentValue = moment(job._id).format("HH:00");
                    currentValue += " - "
                    currentValue += moment(job._id).add(1, "hours").format("HH:00");
                    break;

                    case "day":
                    currentValue = moment(job._id).format("DD MMM YYYY(ddd)");
                    break;

                    case "week":
                    var week = job._id.year + "-W" + job._id.week;
                    currentValue = moment(week).format("DD MMM YYYY(ddd)");
                    currentValue += " - "
                    currentValue += moment(week).add(1, "weeks").format("DD MMM YYYY(ddd)");
                    break;

                    case "month":
                    currentValue = moment(job._id).format("MMM YYYY");
                    break;
                }

                return currentValue === date;
            });

            if (result) {
                column.push(result.count);
            }
            else {
                column.push(0);
            }
        });

        return column;
    }

    var generateAxis = function() {

        var viewMode = ViewModeList.getData().viewMode;
        var date = DateRagePicker.getData();
        var axis = []

        var toHourAxis = function() {

            var ranges = moment(date.startTime).format("HH:00");
            ranges += " - "
            ranges += moment(date.startTime).add(1, "hours").format("HH:00");

            var nextHour = moment(date.startTime).add(1, "hours").valueOf();
            date.startTime = nextHour;

            return ranges;
        }

        var toDayAxis = function() {

            var ranges = moment(date.startTime).format("DD MMM YYYY(ddd)");
            var nextDay = moment(date.startTime).add(1, "days").valueOf();
            date.startTime = nextDay;

            return ranges;
        }

        var toWeekAxis = function() {

            var currentYear = moment(date.startTime).year();
            var currentWeek = moment(date.startTime).week();

            var weekOfYear = currentYear + "-W" + currentWeek;

            var ranges = moment(weekOfYear).format("DD MMM YYYY(ddd)");
            ranges += " - "
            ranges += moment(weekOfYear).add(1, "weeks").format("DD MMM YYYY(ddd)");

            var nextWeek = moment(weekOfYear).add(1, "weeks").valueOf();
            date.startTime = nextWeek;

            return ranges;
        }

        var toMonthAxis = function() {

            var ranges = moment(date.startTime).format("MMM YYYY");
            var nextMonth = moment(date.startTime).add(1, "months").valueOf();
            date.startTime = nextMonth;

            return ranges;
        }

        if(viewMode !== "week") {

            while( date.startTime <= date.endTime ) {

                var axisTxt = "";

                switch( viewMode ) {
                    case "hour":
                    axisTxt = toHourAxis();
                    break;
                    case "day":
                    axisTxt = toDayAxis();
                    break;
                    case "month":
                    axisTxt = toMonthAxis();
                    break;
                }

                axis.push(axisTxt);
            }
        }
        else {
            for (var i = 0; i < 4; i++) {
                var axisTxt = "";
                axisTxt = toWeekAxis();
                axis.push(axisTxt);
            }
        }

        return axis;
    }

    this.createMobileAppGraph = function (filterData) {

        var columns = [];
        var axis = [];

        var graphOption = {
            bindto: '#chart-summary',
            data: {
                columns: [],
                names: {
                    all: 'จำนวนงานทั้งหมด',
                    wait: 'แท็กซี่รับงาน',
                    pick: 'แท็กซี่รับผู้โดยสาร',
                    accept: 'ผู้โดยสารตอบรับ',
                    drop: 'แท็กซี่ส่งผู้โดยสาร',
                    PSGCancel: 'ผู้โดยสารยกเลิก',
                    DRVCancel: 'แท็กซี่ยกเลิก',
                },
                colors: {
                    all: '#FF5722',
                    wait: '#2196F3',
                    pick: '#4CAF50',
                    accept: '#CCCCCC',
                    drop: '#607D8B',
                    PSGCancel: '#E91E63',
                    DRVCancel: '#009688'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: []
                }
            }
        };


        var toCircleCounter = function(data) {

            data.forEach(function(d, i) {

                var countNumber = 0;

                d.forEach(function(count, index) {

                    if(index === 0) { return; }

                    countNumber += count;
                });

                $("#JoblistLogTab").find(".summary.circle." + d[0] + " > .counter > .number").text( Util.formatMoney(countNumber, 0, ".", ",") );
            });
        }


        var getJobListLogAll = function () {
            getData(C.serviceURI.getMobileLog, $.extend({ logType: "all" }, filterData), function (response) {

                axis = generateAxis();
                columns.push(generateData("all", response.data, axis));
                getJobListLogWait();
            });
        }


        var getJobListLogWait = function () {
            getData(C.serviceURI.getMobileLog, $.extend({ logType: "wait" }, filterData), function (response) {

                columns.push(generateData("wait", response.data, axis));

                getJobListLogAccept();
            });
        }


        var getJobListLogPick = function () {
            getData(C.serviceURI.getMobileLog, $.extend({ logType: "pick" }, filterData), function (response) {

                columns.push(generateData("pick", response.data, axis));

                getJobListLogDrop();
            });
        }


        var getJobListLogAccept = function () {
            getData(C.serviceURI.getMobileLog, $.extend({ logType: "accept" }, filterData), function (response) {

                columns.push(generateData("accept", response.data, axis));

                getJobListLogPick();
            });
        }


        var getJobListLogDrop = function () {
            getData(C.serviceURI.getMobileLog, $.extend({ logType: "drop" }, filterData), function (response) {

                columns.push(generateData("drop", response.data, axis));

                getJobListLogPSGCancel();
            });
        }


        var getJobListLogPSGCancel = function () {

            getData(C.serviceURI.getMobileLog, $.extend({ logType: "psgcancel" }, filterData), function (response) {

                columns.push(generateData("PSGCancel", response.data, axis));

                getJobListLogDRVCancel();
            });
        }


        var getJobListLogDRVCancel = function () {

            getData(C.serviceURI.getMobileLog, $.extend({ logType: "drvcancel" }, filterData), function (response) {

                columns.push(generateData("DRVCancel", response.data, axis));

                create();
            });
        }

        var create = function() {

            graphOption.axis.x.categories = axis;
            graphOption.data.columns = columns;

            GraphService.appGraph = c3.generate(graphOption);
            toCircleCounter(graphOption.data.columns);
        }


        if (GraphService.appGraph) {
            GraphService.appGraph.destroy();
        }

        getJobListLogAll();
    }

    this.createCallcenterGraph = function(filterData) {

        var columns = [];
        var axis = [];

        var daysOfWeek = [
        'อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'
        ];

        var graphOption = {
            bindto: '#callcenter-summary',
            data: {
                columns: [],
                names: {
                    all: 'จำนวนงานทั้งหมด',
                    unregister: 'จ่ายให้นอกระบบ',
                    registered: 'จ่ายให้ในระบบ',
                    deleted: 'งานที่ยกเลิก(ลบทิ้ง)'
                },
                colors: {
                    all: '#FF5722',
                    unregister: '#4CAF50',
                    registered: '#2196F3',
                    deleted: '#CCCCCC'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    categories: []
                }
            }
        };


        var toCircleCounter = function(data) {

            data.forEach(function(d, i) {

                var countNumber = 0;

                d.forEach(function(count, index) {

                    if(index === 0) { return; }

                    countNumber += count;
                });

                $("#CallcenterLogTab").find(".summary.circle." + d[0] + " > .counter > .number").text( Util.formatMoney(countNumber, 0, ".", ",") );
            });
        }


        var getLogAll = function () {
            getData(C.serviceURI.getCallcenterLog, filterData, function (response) {

                axis = generateAxis();
                columns.push(generateData("all", response.data, axis));
                getAssignToUnRegister();
            });
        }


        var getAssignToUnRegister = function () {
            getData(C.serviceURI.getCallcenterLog, $.extend({ logType: "unregister" }, filterData), function (response) {

                columns.push(generateData("unregister", response.data, axis));

                getAssignToRegistered();
            });
        }


        var getAssignToRegistered = function () {
            getData(C.serviceURI.getCallcenterLog, $.extend({ logType: "registered" }, filterData), function (response) {

                columns.push(generateData("registered", response.data, axis));

                getLogDeleted();
            });
        }


        var getLogDeleted = function () {
            getData(C.serviceURI.getCallcenterLog, $.extend({ logType: "deleted" }, filterData), function (response) {

                columns.push(generateData("deleted", response.data, axis));

                create();
            });
        }

        var create = function() {

            graphOption.axis.x.categories = axis;
            graphOption.data.columns = columns;

            GraphService.appGraph = c3.generate(graphOption);
            toCircleCounter(graphOption.data.columns);
        }

        if (GraphService.callcenterGraph) {
            GraphService.callcenterGraph.destroy();
        }

        getLogAll();
    }
};


var HightCharts = function() {
    
    Highcharts.setOptions({
        global: {
            useUTC: false,
            timezoneOffset: 7 * 60
        }
    });

    var width = $("#content-right").width() - 15;

    var seriesOptions = [],
        allTaskArray = [],
        seriesCounter = 0,
        names = ['wait', 'accept', 'pick', 'drop', 'psgcancel', 'drvcancel'];
        garages = GarageList.getData();

    function afterSetExtremes(e) {

        var datas = [];
        allTaskArray = [];
        seriesCounter = 0;
        garages = GarageList.getData();

        var chart = $('#container-report').highcharts();

        chart.showLoading('Loading data from server...');

        var start = new Date(e.min).setHours(0,0,0,0);
        var end = new Date(e.max).setHours(23,59,59,999);

        var query = $.extend({}, { startTime: start, endTime: end }, { logType: "all",  }, GarageList.getData());

        DataTable.JoblistLogTable.getData(query);
        DateRagePicker.displayText.find("h4").html(moment(query.startTime).format("DD/MM/YYYY") + " - " + moment(query.endTime).format("DD/MM/YYYY"));

        $.post("/service/log/hcData/admin", query, function(data) {

            toCircleCounter(data.total, "all");

            allTaskArray = toValidData(data.result);
            chart.series[0].setData(allTaskArray);

            $.each(names, function (i, name) {

                var query = $.extend({}, { startTime: start, endTime: end }, { logType: name,  }, GarageList.getData());
                
                $.post("/service/log/hcData/admin", query, function(data) {

                    toCircleCounter(data.total, name);
                    var dataArray = [],
                    color = '#CCCCCC',
                    named = "",
                    tempArray = [];

                    allTaskArray.forEach(function(all) {
                        var result = toValidData(data.result).find(function(data) {
                            return data[0] === all[0];
                        });

                        if(result) {
                            dataArray.push(result);
                        } else {
                            dataArray.push([ all[0], 0 ]);
                        }
                    });

                    switch(name) {
                        case 'wait':
                        color = '#2196F3';
                        named = "แท็กซี่รับงาน";
                        break;
                        case 'accept':
                        color = '#CCCCCC';
                        named = "ผู้โดยสารตอบรับ";
                        break;
                        case 'pick':
                        color = '#4CAF50';
                        named = "แท็กซี่รับผู้โดยสาร";
                        break;
                        case 'drop':
                        color = '#607D8B';
                        named = "แท็กซี่ส่งผู้โดยสาร";
                        break;
                        case 'psgcancel':
                        color = '#E91E63';
                        named = "ผู้โดยสารยกเลิก";
                        break;
                        case 'drvcancel':
                        color = '#009688';
                        named = "แท็กซี่ยกเลิก";
                        break;
                    }

                    datas[i+1] = dataArray

                    seriesCounter += 1;

                    if (seriesCounter === names.length) {

                        datas.forEach(function(data, index) {
                            chart.series[index].setData(data);
                        });

                        chart.hideLoading();
                    }
                }, "json");
            });
        }, "json");
    }

    function createChart() {

        $('#container-report').highcharts('StockChart', {

            chart: {
                width: width,
                zoomType: 'x'
            },

            legend: {
                enabled: true
            },

            rangeSelector: {
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'day',
                    count: 2,
                    text: '2d'
                }, {
                    type: 'day',
                    count: 3,
                    text: '3d'
                }, {
                    type: 'day',
                    count: 5,
                    text: '5d'
                }, {
                    type: 'day',
                    count: 7,
                    text: '7d'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1y'
                }, {
                    type: 'all',
                    text: 'All'
                }],
                selected : 5,
                inputEditDateFormat: '%Y-%m-%d'
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                valueDecimals: 0,
                shared: true
            },

            navigator : {
                adaptToUpdatedData: false,
                series : {
                    type: 'areaspline',
                    color: '#4572A7',
                    fillOpacity: 0.05,
                    dataGrouping: {
                        enabled: false
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false
                    }
                }
            },

            scrollbar: {
                liveRedraw: false
            },

            xAxis : {
                gapGridLineWidth: 0,
                events : {
                    afterSetExtremes : afterSetExtremes
                },
                minRange: 3600 * 1000
            },

            yAxis: {
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: 'silver'
                }]
            },

            series: seriesOptions

        }, function(chart) {

            setTimeout(function () {

                $('input.highcharts-range-selector', $(chart.container).parent()).datepicker();

            }, 0);
        });

        $.datepicker.setDefaults({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText) {
                this.onchange();
                this.onblur();
            }
        });
    }

    function toCircleCounter(total, name) {
        $("#JoblistLogTab").find(".summary.circle." + name + " > .counter > .number").text( Util.formatMoney(total, 0, ".", ",") );
    }

    function toValidData(data) {

        if(typeof data === 'string') {
            data = JSON.parse(data);
        }

        var validData = [];

        data.forEach(function(d) {
            validData.push([new Date(d._id).getTime(), d.count]);
        });

        return validData;
    }

    function getTaskDataEachType() {

        $.each(names, function (i, name) {

            var query = $.extend({}, { logType: name }, garages );

            $.post("/service/log/hcData/admin", query, function(data) {

                toCircleCounter(data.total, name);

                var dataArray = [],
                    color = '#CCCCCC',
                    named = "",
                    tempArray = [];

                allTaskArray.forEach(function(all) {
                    var result = toValidData(data.result).find(function(data) {
                        return data[0] === all[0];
                    });

                    if(result) {
                        dataArray.push(result);
                    } else {
                        dataArray.push([ all[0], 0 ]);
                    }
                });

                switch(name) {
                    case 'wait':
                    color = '#2196F3';
                    named = "แท็กซี่รับงาน";
                    break;
                    case 'accept':
                    color = '#CCCCCC';
                    named = "ผู้โดยสารตอบรับ";
                    break;
                    case 'pick':
                    color = '#4CAF50';
                    named = "แท็กซี่รับผู้โดยสาร";
                    break;
                    case 'drop':
                    color = '#607D8B';
                    named = "แท็กซี่ส่งผู้โดยสาร";
                    break;
                    case 'psgcancel':
                    color = '#E91E63';
                    named = "ผู้โดยสารยกเลิก";
                    break;
                    case 'drvcancel':
                    color = '#009688';
                    named = "แท็กซี่ยกเลิก";
                    break;
                }

                seriesOptions[i+1] = {
                    name: named,
                    color: color,
                    marker : {
                        enabled : true,
                        radius : 3
                    },
                    shadow : true,
                    pointStart: data.pointStart,
                    pointInterval: data.pointInterval,
                    dataGrouping: {
                        enabled: false
                    },
                    data: dataArray
                };

                seriesCounter += 1;

                if (seriesCounter === names.length) {
                    createChart();
                }
            }, "json");
        });
    }

    var query = $.extend({}, { logType: "all",  }, garages);
    DataTable.JoblistLogTable.getData(query);

    $.post("/service/log/hcData/admin", query, function(data) {

        toCircleCounter(data.total, "all");
        allTaskArray = toValidData(data.result);

        seriesOptions[0] = {
            name: "งานทั้งหมด",
            color: "#FF5722",
            marker : {
                enabled : true,
                radius : 3
            },
            shadow : true,
            pointStart: data.pointStart,
            pointInterval: data.pointInterval,
            data: allTaskArray
        };

        getTaskDataEachType();
    }, "json");
};


var Http = {

    connection_error_time: 0,

    getJson: function (url, options) {

        if (options.loader !== undefined && !options.loader) { Preloader.show(); }

        $.getJSON(url, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        })
        .fail(function () {
            Notification.defaultError();
        })
        .always(function () {
            Preloader.hide();
        });
    },

    post: function (url, options) {

        if (options.loader || options.loader == undefined) { Preloader.show(); }

        if (options.data == undefined) { options.data = {}; }

        $.post(url, options.data, function (data) {
            if (options !== undefined && options.onSuccess !== undefined && typeof options.onSuccess == "function") {
                options.onSuccess(data);
            }
        }, "json")
        .fail(function () {
            if (Http.connection_error_time < 1) {
                Http.connection_error_time++;
            }
            else if (Http.connection_error_time >= 1) {

                Http.connection_error_time = 0;
                Http.onConnectionLost();
            }
        })
        .always(function () {
            Preloader.hide();
        });
    },

    onConnectionLost: function () {

        var message = '<div class="connect_lost_message"><span class="label label-danger">การเชื่อมต่อมีปัญหา</span></div>';

        $('body').find("> .connect_lost_message").remove();
        $('body').append(message);
        $('body').find("> .connect_lost_message").fadeIn('fast');

        if (this.intervalId == undefined) {
            this.intervalId = App.intervalManager(true, Http.testConnection, 5000);
        }
    },

    testConnection: function () {
        $.post('service/ubeam/getPassengerDetail', { "psg_id": "" }, function (data) {
            clearInterval(Http.intervalId);
            App.intervalManager(false, Http.intervalId);
            delete Http.intervalId;
            Http.connection_error_time = 0;
            $('body').find("> .connect_lost_message").remove();
        }, "json");
    }
};


var Notification = {

    show: function (message) {
        options = {
            title: message.title,
            text: message.text,
            sticky: message.sticky ? message.sticky : false,
            time: message.time ? message.time : 2000,
            class_name: 'gritter-' + (!message.$class ? 'warning' : message.$class),
            position: message.position ? message.position : 'bottom-right'
        };

        $.gritter.add(options);
    },

    defaultSuccess: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Success!' : title,
            text: !text ? 'คำสั่ง ทำงานเรียบร้อย' : text,
            sticky: false,
            time: 1500,
            class_name: 'gritter-success',
            position: 'bottom-right'
        });
    },

    defaultError: function (title, text) {
        Notification.remove();
        $.gritter.add({
            title: !title ? 'Server Error!' : title,
            text: !text ? 'Pls. refresh page and then try it again.' : text,
            sticky: false,
            time: 2500,
            class_name: 'gritter-danger',
            position: 'bottom-right'
        });
    },

    removeAll: function () {
        $.gritter.removeAll({
            time: 0,
            before_close: function (e) {
            },
            after_close: function () {
            }
        });
    },

    remove: function () {
        $("#gritter-notice-wrapper").remove();
    }
};


$(document).ready(function() {
    App.initial();
});