var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    serviceURI: {
        getDriverList: serviceBaseURI + '/service/admin/getDriverList',
        getDriverDetail: serviceBaseURI + '/service/admin/getDriverDetail',
        editDriver: serviceBaseURI + '/service/admin/editDriver',
        getAllGarage: serviceBaseURI + '/service/admin/getAllGarage'
    },
    driverImageURL: 'http://callcenter.taxi-beam.com/image/driver/'
};

var C = CONSTRAINTS;

var App = {
    bootstrap: function () {

        setTimeout(function() {
            GarageList.init();
            DriverEditForm.createCarProvinceSelectList(10);
            DriverEditForm.createProvinceSelectList(0);
            DriverEditForm.createDistrictSelectList(0);
            DriverEditForm.createTambonSelectList(0);
            DriverEditForm.onDropDownOpen();
        }, 1000);

        $('.fancybox').fancybox();
        DriverList.loadData();
        DriverDetailModal.init();
        DriverEditForm.init();
        DriverEditModal.init();
    }
};

var DriverList = {

    table: $("#driver-datatable"),

    instance: null,

    loadData: function () {
        apiClient.getDriverList(function(res) {
            if (res instanceof Array && res.length > 0) {
                var dataSet = [];
                res.forEach(function (data, index) {
                    dataSet.push([
                        (index + 1),
                        data.username || '',
                        data.fname || '',
                        data.lname || '',
                        data.phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
                        data.cgroup || '',
                        data.province || '',
                        (data.prefixcarplate || '') + ' ' + data.carplate || '',
                        data.carcolor || '',
                        data.status || '',
                        moment(data.created).format("YYYY/MM/DD") || '',
                        moment(data.updated).format("YYYY/MM/DD") || '',
                        data
                    ]);
                });
                DriverList.createTable(dataSet);
            } else {
                DriverList.createTable([]);
            }
        });
    },

    createTable: function (dataSet) {
        DriverList.instance = DriverList.table.DataTable({
            data: dataSet,
            order: [[0, 'asc']],
            columnDefs: [
                {
                    targets: 12,
                    width: "80px",
                    className: "uk-text-center",
                    render: function (data, type, row) {
                        
                        var id = data ? data._id : row[11]._id;
                        var btnGroup = '<a onclick="DriverDetailModal.open()" data-uk-tooltip="{pos:\'top\'}" title="รายละเอียด"><i class="md-icon material-icons">&#xE88F;</i></a>';
                        btnGroup += '<a onclick="DriverEditModal.open(\'' + id + '\');" data-uk-tooltip="{pos:\'top\'}" title="แก้ไขข้อมูล"><i class="md-icon material-icons">&#xE254;</i></a>';
                        return btnGroup;
                    }
                }
            ],
            createdRow: function (row, data, index) {
                $(row).attr('data-row-id', data[12]._id);
                $(row).data(data[12]);
            },
            initComplete: function () {
                this.api().columns().every(function () {

                    if($.inArray(this.index(), [5, 6, 8, 9]) < 0) {
                        $(this.footer()).empty();
                        return;
                    }

                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });
                        

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });

                    // $(select).selectize();
                });
            }
        });
    },

    updateRow: function (row, data) {
        this.instance.row(row).data([
            (DriverList.instance.row(row).index() + 1),
            data.username || '',
            data.fname || '',
            data.lname || '',
            data.phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
            data.cgroup || '',
            data.province || '',
            (data.prefixcarplate || '') + ' ' + data.carplate || '',
            data.carcolor || '',
            data.status || '',
            moment(data.created).format("YYYY/MM/DD") || '',
            moment(data.updated).format("YYYY/MM/DD") || '',
            data
        ]).draw(true);
    },
};

var DriverDetailModal = {
    modal: $('#view_driver_modal'),
    instance: null,
    init: function () {
        this.instance = UIkit.modal(this.modal);
    },
    open: function () {
        this.clearData();
        this.fillData($(event.target).closest('tr').data());
        this.instance.show();
    },
    fillData: function (data) {
        this.modal.find('.uk-modal-title').text((data.prefixcarplate || '') + ' ' + data.carplate + ' - ' + data.fname + ' ' + data.lname);

        var hash = '?' + Date.now().toString(16);

        this.modal.find('[data-driver-imgface]').attr('src', C.driverImageURL + data.imgface + hash);
        this.modal.find('[data-driver-imgcar]').attr('src', C.driverImageURL + data.imgcar + hash);
        this.modal.find('[data-driver-imglicence]').attr('src', C.driverImageURL + data.imglicence + hash);

        this.modal.find('[data-driver-imgface]').closest(".fancybox").attr('href', C.driverImageURL + data.imgface + hash);
        this.modal.find('[data-driver-imgcar]').closest(".fancybox").attr('href', C.driverImageURL + data.imgcar + hash);
        this.modal.find('[data-driver-imglicence]').closest(".fancybox").attr('href', C.driverImageURL + data.imglicence + hash);

        this.modal.find('[data-driver-fullname]').text(data.fname + ' ' + data.lname);
        this.modal.find('[data-driver-phone]').text(data.phone);
        this.modal.find('[data-driver-carplate]').text((data.prefixcarplate || '') + ' ' + data.carplate);
        this.modal.find('[data-driver-cartype]').text(data.cartype === 'car' ? 'รถทั่วไป' : 'รถตู้ขนาดเล็ก');
        this.modal.find('[data-driver-carcolor]').text(data.carcolor);
        this.modal.find('[data-driver-citizenid]').text(data.citizenid);

        this.modal.find('[data-driver-taxi-id]').text(data.taxiID);
        this.modal.find('[data-driver-username]').text(data.username);
        this.modal.find('[data-driver-password]').text(data.password);
        this.modal.find('[data-driver-fulladdress]').text(data.address + ' ต.' + data.tambon + ' อ.' + data.district + ' จ.' + data.province + ' ' + data.zipcode);
        this.modal.find('[data-driver-created]').text(moment(data.created).format('DD/MM/YYYY - HH:mm น.'));

        this.modal.find('[data-driver-drv_point]').text(data.drv_point);
        this.modal.find('[data-driver-drv_wallet]').text(data.drv_wallet);
        this.modal.find('[data-driver-drv_shift]').text(data.drv_shift);
        this.modal.find('[data-driver-shift_start]').text(data.shift_start ? moment(data.shift_start).format('DD/MM/YYYY - HH:mm น.') : "-");
        this.modal.find('[data-driver-shift_end]').text(data.shift_end ? moment(data.shift_end).format('DD/MM/YYYY - HH:mm น.') : "-");   

    },
    clearData: function() {
        DriverDetailModal.modal.find('[data-field]').text('');
        DriverDetailModal.modal.find('[data-driver-imgcar]').attr('src', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
        DriverDetailModal.modal.find('[data-driver-imgface]').attr('src', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
        DriverDetailModal.modal.find('[data-driver-imglicence]').attr('src', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');

        DriverDetailModal.modal.find('[data-driver-imgcar]').closest(".fancybox").attr('href', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
        DriverDetailModal.modal.find('[data-driver-imgface]').closest(".fancybox").attr('href', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
        DriverDetailModal.modal.find('[data-driver-imglicence]').closest(".fancybox").attr('href', 'assets/theme/altair/img/ecommerce/s6_edge.jpg');
    }
}

var DriverEditModal = {

    modal: $('#edit_driver_modal'),

    instance: null,

    init: function () {
        this.instance = UIkit.modal(this.modal);

        this.instance.on({
            'show.uk.modal': DriverEditModal.event.onShow,
            'hide.uk.modal': DriverEditModal.event.onHide,
        });
    },

    open: function (id) {
        DriverEditForm.clearFormData();
        DriverEditModal.instance.show();
        if (!id) {
            var id = $(event.target).closest('tr').data()._id;
        }
        this.getData(id);
    },

    getData: function (id) {
        apiClient.getDriverDetail(id, function(response) {
            if (response.status) {
                DriverEditModal.fillData(response.data);
            }
            return;
        });
    },

    fillData: function (data) {

        this.modal.find('.uk-modal-title').text((data.prefixcarplate || '') + ' ' + data.carplate + ' - ' + data.fname + ' ' + data.lname);

        this.modal.find('[name=fname]').val(data.fname);
        this.modal.find('[name=lname]').val(data.lname);
        this.modal.find('[name=citizenid]').val(data.citizenid);
        this.modal.find('[name=dob]').val(data.dob);
        this.modal.find('[name=phone]').val(data.phone);
        this.modal.find('[name=emergencyphone]').val(data.emergencyphone);
        this.modal.find('[name=address]').val(data.address);
        this.modal.find('[name=tambon]').val(data.tambon);
        this.modal.find('[name=district]').val(data.district);
        this.modal.find('[name=province]').val(data.province);
        this.modal.find('[name=zipcode]').val(data.zipcode);
        this.modal.find('[name=prefixcarplate]').val(data.prefixcarplate);
        this.modal.find('[name=carplate]').val(data.carplate);
        this.modal.find('[name=taxiID]').val(data.taxiID);
        this.modal.find('[name=carcolor]').val(data.carcolor);
        this.modal.find('[name=cartype]').val(data.cartype);

        var $selectGender = this.modal.find('[name=gender]')
        var selectizeGender = $selectGender[0].selectize;
        if(data.gender) {
            selectizeGender.setValue(data.gender);
            this.modal.find('[name=gender]').val(data.gender);
        } else {
            selectizeGender.setValue("0");
            this.modal.find('[name=gender]').val("เลือกเพศ");
        }

        var pv = new Province();

        var $select0 = this.modal.find('[name=carprovince]')
        var selectize0 = $select0[0].selectize;

        var carprovinceObj = pv.getProvince('th', data.carprovince);
        if(carprovinceObj) {
            DriverEditForm.createCarProvinceSelectList(carprovinceObj.id);
        } else {
            DriverEditForm.createCarProvinceSelectList(0);
        }

        var $select1 = this.modal.find('[name=province]')
        var selectize1 = $select1[0].selectize;

        var provinceObj = pv.getProvince('th', data.province);
        if(provinceObj) {

            DriverEditForm.createProvinceSelectList(provinceObj.id);
            DriverEditForm.createDistrictSelectList(provinceObj.id, data.district);

            var districtList = pv.getDistrictList('th', provinceObj.id);
            var districtObj = districtList.find(function (o, i) {
                return o.name === data.district;
            });

            if (!districtObj) {
                districtObj = districtList[0];
            }

            DriverEditForm.createTambonSelectList(districtObj.id, data.tambon);

        } else {
            DriverEditForm.createProvinceSelectList(0);
            DriverEditForm.createDistrictSelectList(0);
            DriverEditForm.createTambonSelectList(0);
        }

        var $select2 = this.modal.find('[name=carcolor]');
        var selectize2 = $select2[0].selectize;
        selectize2.settings.create = true;
        selectize2.addOption({ value: data.carcolor, text: data.carcolor });
        selectize2.setValue(data.carcolor);

        var $select3 = this.modal.find('[name=cartype]')
        var selectize3 = $select3[0].selectize;
        selectize3.setValue(data.cartype);

        if (data.allowpsgcontact === 'Y') {
            this.modal.find('[name=allowpsgcontact]').iCheck('check');
        } else if (data.allowpsgcontact === 'N') {
            this.modal.find('[name=allowpsgcontact]').iCheck('uncheck');
        }

        if (data.outbound === 'Y') {
            this.modal.find('[name=outbound]').iCheck('check');
        } else if (data.outbound === 'N') {
            this.modal.find('[name=outbound]').iCheck('uncheck');
        }

        if (data.carryon === 'Y') {
            this.modal.find('[name=carryon]').iCheck('check');
        } else if (data.carryon === 'N') {
            this.modal.find('[name=carryon]').iCheck('uncheck');
        }

        if (data.english === 'Y') {
            this.modal.find('[name=english]').iCheck('check');
        } else if (data.english === 'N') {
            this.modal.find('[name=english]').iCheck('uncheck');
        }

        var checked = DriverEditModal.modal.find('input[name=active]')[0].checked;
        if (data.active === 'Y' && !checked) {
            this.modal.find('input[name=active]').parent().find('.switchery').click();
        } else if (data.active === 'N' && checked) {
            this.modal.find('input[name=active]').parent().find('.switchery').click();
        }


        GarageList.instance.setValue(data.cgroup);

        this.modal.find('[name=status]')[0].selectize.setValue(data.status);
        
        this.modal.find('.dropify-wrapper').remove();

        this.modal.find('[data-wrapper-image-car]').append('<input type="file" name="imgcar" class="dropify" data-default-file="' + C.driverImageURL + data.imgcar + '" data-show-remove="false" />');
        this.modal.find('[data-wrapper-image-face]').append('<input type="file" name="imgface" class="dropify" data-default-file="' + C.driverImageURL + data.imgface + '" data-show-remove="false" />');
        this.modal.find('[data-wrapper-image-license]').append('<input type="file" name="imglicence" class="dropify" data-default-file="' + C.driverImageURL + data.imglicence + '" data-show-remove="false" />');

        this.modal.find('.dropify').dropify({ showRemove: false });
        this.modal.find('form#edit_driver_form').data(data);
        Util.removeDropifyImageCached(this.modal);
    },

    event: {
        onShow: function() { },
        onHide: function() {
            $(".parsley-errors-list").remove();
        }
    }
};

var DriverEditForm = {

    form: $("#edit_driver_form"),

    instance: null,

    init: function() {
        this.instance = DriverEditForm.form.parsley();
    },

    onSubmit: function(form, event) {

        var instance = $(form).parsley();

        if (!DriverEditForm.checkSelectListEmptyValue()) {
            return;
        }
        else if (instance.isValid()) {

            var formData = new FormData(form);
            formData.set('_id', $(form).data()._id);
            formData.set('cgroup', $(form).find("[name=cgroup]").val());
            formData.set('province', $(form).find("[name=province]").text());
            formData.set('district', $(form).find("[name=district]").text());
            formData.set('tambon', $(form).find("[name=tambon]").text());
            formData.set('carprovince', $(form).find("[name=carprovince]").text());

            var garageData = GarageList.getByCGroup($(form).find("[name=cgroup]").val());
            formData.set('_id_garage', garageData._id);
            formData.set('cgroupname', garageData.cgroupname);
            formData.set('cprovincename', garageData.cprovincename);

            formData = DriverEditForm.customFormValue($(form), formData);

            apiClient.editDriver(formData, function (response) {
                setTimeout(function () {
                    apiClient.loader.hide();
                    if (response.status) {
                        var row = DriverList.table.find('tr[data-row-id="' + response.driver._id + '"]')[0];
                        DriverList.updateRow(row, response.driver);
                        $(row).data(response.driver);
                        UIkit.notify('แก้ไขข้อมูลเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                        DriverEditModal.instance.hide();
                    } else {
                        if (response.code === 'USERNAME_DUPLICATED') {
                            UIkit.notify('username ดังกล่าว มีผู้ใช้งานแล้ว', { pos: 'bottom-center', status: 'warning' });
                        } else {
                            UIkit.notify(response.messages, { pos: 'bottom-center', status: 'success' });
                        }
                    }
                }, 1250);
            });
        }

        event.preventDefault();
        return;
    },

    customFormValue: function (form, formData) {
    
        form.find('[name=active]')[0].checked ? formData.set('active', 'Y') : formData.set('active', 'N');
        form.find("[name=english]")[0].checked ? formData.set('english', 'Y') : formData.set('english', 'N');
        form.find("[name=carryon]")[0].checked ? formData.set('carryon', 'Y') : formData.set('carryon', 'N');
        form.find("[name=outbound]")[0].checked ? formData.set('outbound', 'Y') : formData.set('outbound', 'N');
        form.find("[name=allowpsgcontact]")[0].checked ? formData.set('allowpsgcontact', 'Y') : formData.set('allowpsgcontact', 'N');

        return formData;
    },

    clearFormData: function() {
        var _this = DriverEditForm;
        _this.form.parsley().reset();
        _this.form.find('input').val('');
        _this.form.find('input[type=checkbox][data-md-icheck]').iCheck('uncheck');
        _this.form.find('[type=file]').parent().find('.dropify-clear').click();
    },

    createCarProvinceSelectList: function (id) {
        
        var selectOptions = [];

        var $select = $('#edit_driver_form').find('[name="carprovince"]');
        var selectize = $select[0].selectize;
        
        selectize.clearOptions();
        selectize.renderCache = {};

        new Province().getProvince('th').find(function (o, i) {

            if (i === 0) {
                selectOptions.push({
                    value: 0,
                    text: "-- ระบุทะเบียนจังหวัด --"
                });
            }
            selectOptions.push({
                value: o.id,
                text: o.name
            });
        });
        selectize.load(function (callback) {
            callback(selectOptions);
        });
        
        selectize.settings.placeholder = 'ระบุทะเบียนจังหวัด'
        selectize.updatePlaceholder();
        
        selectize.setValue(id);
    },

    createProvinceSelectList: function (id) {
        
        var selectOptions = [];

        var $select = $('#edit_driver_form').find('[name="province"]');
        var selectize = $select[0].selectize;
        
        selectize.clearOptions();
        selectize.renderCache = {};

        new Province().getProvince('th').find(function (o, i) {

            if (i === 0) {
                selectOptions.push({
                    value: 0,
                    text: "เลือกจังหวัด"
                });
            }
            selectOptions.push({
                value: o.id,
                text: o.name
            });
        });
        selectize.load(function (callback) {
            callback(selectOptions);
        });
        selectize.off('change');
        selectize.on('change', function (value) {
            DriverEditForm.createDistrictSelectList(value);
            DriverEditForm.createTambonSelectList(0);
        });

        selectize.settings.placeholder = 'เลือกจังหวัด'
        selectize.updatePlaceholder();

        selectize.setValue(id);
    },

    createDistrictSelectList: function (provinceId, districtText) {

        var selectOptions = [];
        var districtId = 0;

        var $select = $('#edit_driver_form').find('[name="district"]');
        var selectize = $select[0].selectize;

        selectize.settings.placeholder = 'เลือกอำเภอ'
        selectize.updatePlaceholder();
        
        selectize.clearOptions();
        selectize.renderCache = {};

        if(provinceId > 0) {
            var pv = new Province();
            pv.getDistrictList('th', provinceId).find(function (o, i) {
                if(i === 0) {
                    selectOptions.push({
                        value: 0,
                        text: "เลือกอำเภอ"
                    });
                }
                if(o.name === districtText) {
                    districtId = o.id;
                }
                selectOptions.push({
                    value: o.id,
                    text: o.name
                });
            });
            selectize.load(function(callback) {
                callback(selectOptions);
            });
            selectize.off('change');
            selectize.on('change', function(value) {
                DriverEditForm.createTambonSelectList(value);
            });
        }
        
        selectize.setValue(districtId);
    },

    createTambonSelectList: function (districtId, tambonText) {
        
        var selectOptions = [];
        var tambonId = 0;

        var $select = $('#edit_driver_form').find('[name="tambon"]');
        var selectize = $select[0].selectize;

        selectize.settings.placeholder = 'เลือกตำบล'
        selectize.updatePlaceholder();

        selectize.clearOptions();
        selectize.renderCache = {};

        if(districtId > 0) {
            var pv = new Province();
            pv.getSubDistrictList('th', districtId).find(function (o, i) {
                if(i === 0) {
                    selectOptions.push({
                        value: 0,
                        text: "เลือกตำบล"
                    });
                }
                if(o.name === tambonText) {
                    tambonId = o.id;
                }
                selectOptions.push({
                    value: o.id,
                    text: o.name
                });
            });
            selectize.load(function(callback) {
                callback(selectOptions);
            });
        }
        
        selectize.setValue(tambonId);
    },

    onProvinceSelectListChange: function () {
        var pv = new Province();
        if (this.value === "") { return; }
        var p = pv.getProvince('th', this.value);
        if(p) {
            DriverEditForm.createDistrictSelectList(p.id);
        } else {
            DriverEditForm.createDistrictSelectList(0);
        }
    },

    onDistrictSelectListChange: function () {

        var selected = this.value;

        var provinceSelectize = $('#add_driver_form').find('[name="province"]')[0].selectize;
        var provinceName = provinceSelectize.getValue();
        var districtSelectize = $('#add_driver_form').find('[name="district"]')[0].selectize;
        var districtName = districtSelectize.getValue();

        var pv = new Province();
        var provinceObj = pv.getProvince('th', provinceName);
        var districtList = pv.getDistrictList('th', provinceObj.id);
        var districtObj = districtList.find(function (o, i) {
            return o.name === selected;
        });

        if(districtObj) {
            DriverEditForm.createTambonSelectList(districtObj.id);
        } else {
            DriverEditForm.createTambonSelectList(0);
        }
    },

    checkSelectListEmptyValue() {

        var isValid = true;
        var selects = ['gender', 'province', 'district', 'tambon', 'carprovince', 'carcolor', 'cartype', 'cgroup', 'status'];

        selects.forEach(function(name, index) {
            var select = DriverEditForm.form.find('[name=' + name + ']'); 
            var value = select.val();
            if(!value || value === "0") {
                
                select.parent().find(".parsley-errors-list").remove();
                select.parent().append('<div class="parsley-errors-list filled" id="parsley-id-37"><span class="parsley-required">กรุณาระบุข้อมูลนี้</span></div>')
                
                isValid = false;
            }
        });
        return isValid;
    },

    onDropDownOpen() {
        DriverEditForm.form.find('select').filter(function(index, select) {
            var selectize = select.selectize;
            if(selectize) {
                selectize.on('dropdown_open', function($dropdown) {
                    $(select).parent().find(".parsley-errors-list").remove();
                });
            }
        });
    }
};

var apiClient = {
    
    loader: null,

    onFail: function() {
        apiClient.loader.hide();
        UIkit.notify("พบปัญหาบางอย่าง!", { pos: 'bottom-center', status: 'error' });
    },

    getDriverList: function(callback) {
        $.getJSON(C.serviceURI.getDriverList, callback);
    },

    getDriverDetail: function(id, callback) {
        $.getJSON(C.serviceURI.getDriverDetail + "/" + id, callback);
    },
    
    editDriver: function (data, callback) {
        apiClient.loader = Loader.show();
        $.ajax({
            url: C.serviceURI.editDriver,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        }).done(callback).fail(apiClient.onFail);
    },

    getAllGarage: function(callback) {
        $.post(C.serviceURI.getAllGarage, callback);
    }
};

var Loader = {
    show: function () {
        return UIkit.modal.blockUI('<div class="uk-text-center"><img class="uk-margin-top" src="assets/theme/altair/img/spinners/spinner.gif" alt=""><br/>กำลังทำงาน...</div>')
    }
};

var Util = {
    removeDropifyImageCached: function (modal) {
        var hash = '?' + Date.now().toString(16);
        modal.find('.dropify-render > img').each(function (index, img) {
            var src = $(img).attr('src');
            $(img).attr('src', src + hash);
        });
    }
};

var GarageList = {

    instance: null,

    data: null,

    init: function() {
        this.getAllGarage();
    },

    getAllGarage: function() {
        apiClient.getAllGarage(function(response) {
            if(response.status) {
                GarageList.data = response.data;
                GarageList.render(response.data);
            }
        });
    },

    render: function(garages) {
        
        var selectOptions = [];

        var $select = DriverEditForm.form.find('[name="cgroup"]');
        GarageList.instance = $select[0].selectize;
        
        GarageList.instance.clearOptions();
        GarageList.instance.renderCache = {};

        garages.find(function (o, i) {

            if (i === 0) {
                selectOptions.push({
                    value: 0,
                    text: "เลือกอู่"
                });
            }
            selectOptions.push({
                value: o.cgroup,
                text: o.name
            });
        });

        GarageList.instance.load(function (callback) {
            callback(selectOptions);
        });
        
        GarageList.instance.settings.placeholder = 'เลือกอู่'
        GarageList.instance.updatePlaceholder();
    },

    getByCGroup: function(cgroup) {
        return GarageList.data.find(function(data) {
            return data.cgroup === cgroup;
        });
    }
};

App.bootstrap();