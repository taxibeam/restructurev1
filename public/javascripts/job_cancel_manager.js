var serviceBaseURI = window.location.origin;
var CONSTRAINTS = {
    serviceURI: {
        getCancelJobList: serviceBaseURI + '/service/admin/getCancelJobList',
        getCancelJobDetail: serviceBaseURI + '/service/admin/getCancelJobDetail',
        getVerifiedCancelJobList: serviceBaseURI + '/service/admin/getVerifiedCancelJobList',
        verifyCancelJob: serviceBaseURI + '/service/admin/verifyCancelJob',
    }
};

var C = CONSTRAINTS;

var App = {
    bootstrap: function () {

        setTimeout(function() { }, 1000);
        
        themeDemo.init();
        JobVerifyModal.init();
        PendingTable.loadData();
        VerifiedTable.loadData();
    }
}

var JobVerifyModal = {
    modal: $('#job_verify_modal'),
    instance: null,
    init: function () {
        this.setUpReasonChoice();
        this.instance = UIkit.modal(this.modal);
        this.instance.on({
            'hide.uk.modal': this.event.hide
        });
    },
    open: function (jobId) {
        this.loadData(jobId);
    },
    loadData: function(id) {
        apiClient.getCancelJobDetail({_id: id}, JobVerifyModal.fillData);
    },
    fillData: function (data) {

        var caseIdHtml = '<i class="material-icons">&#xE85D;</i> #' + (data.cancel_case_id || 'ไม่มีเคสไอดี');
        JobVerifyModal.modal.find('.uk-modal-title').html(caseIdHtml);

        var $select = JobVerifyModal.modal.find('#cancel_reason');
        var $cancel_reason = $select[0].selectize;
        JobVerifyModal.modal.find('[name=job_id]').val(data._id);
        JobVerifyModal.modal.find('[name=driver-name]').text(data.drv_name);
        JobVerifyModal.modal.find('[name=phone-number-driver]').text(data.drv_phone);
        JobVerifyModal.modal.find('[name=phone-number-passenger]').text(data.psg_phone);
        JobVerifyModal.modal.find('[name=pickup-address]').text(data.curaddr);
        JobVerifyModal.modal.find('[name=destination]').text(data.destination);
 JobVerifyModal.modal.find('[name=cancel_message]').text(data.cancel_message);
        JobVerifyModal.modal.find('[name=cancel_memo]').val(data.cancel_memo);

        if(data.cancel_reason.length > 0) {
            $cancel_reason.setValue(data.cancel_reason);
        }

        if(data.cancel_verify) {
            JobVerifyModal.setToDisable($cancel_reason);
        } else {
            JobVerifyModal.setToEnable($cancel_reason);
        }
        JobVerifyModal.instance.show();
    },
    setUpReasonChoice: function() {
        $('#cancel_reason').selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            options: [
                {id: 1, title: 'ไม่มีเบอร์โทร' },
                {id: 2, title: 'จุดรับไม่ชัดเจน' },
                {id: 3, title: 'จุดส่งไม่ชัดเจน' },
                {id: 4, title: 'ติดต่อผู้โดยสารไม่ได้' },
                {id: 5, title: 'เดินทางไม่สะดวก' },
                {id: 6, title: 'ระยะทางใกล้เกิน' },
                {id: 7, title: 'ระยะทางไกลเกิน' },
                {id: 8, title: 'นอกเวลาทำงาน (บริการ)' }
            ],
            maxItems: null,
            valueField: 'title',
            labelField: 'title',
            searchField: 'title',
            create: false,
            render: {
                option: function(data, escape) {
                    return  '<div class="option">' +
                            '<span class="title">' + escape(data.title) + '</span>' +
                            '</div>';
                },
                item: function(data, escape) {
                    return '<div class="item">' + escape(data.title) + '</div>';
                }
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });
    },
    event: {
        hide: function() {
            JobVerifyForm.clear();
        }
    },
    setToDisable: function($cancel_reason) {
        $cancel_reason.disable();
        JobVerifyModal.modal.find('[name=cancel_memo]').attr("disabled", "disabled");
        JobVerifyModal.modal.find('button[type=submit]').hide();
    },
    setToEnable: function($cancel_reason) {
        $cancel_reason.enable();
        JobVerifyModal.modal.find('[name=cancel_memo]').removeAttr("disabled");
        JobVerifyModal.modal.find('button[type=submit]').show();
    }
}

var JobVerifyForm = {
    form: $('#job_verify_form'),
    clear: function() {
        this.form.find('[name=job_id]').val("");
        this.form.find('[name=cancel_memo]').val("");
        var control = this.form.find('#cancel_reason')[0].selectize;
        if(control) {
            control.clear();
        }
    },
    submit: function(form, event) {
        
        var data = {
            _id: this.form.find('[name=job_id]').val(),
            cancel_reason: [],
            cancel_memo: this.form.find('[name=cancel_memo]').val(),
            cancel_verify: $(form).data().verified
        }
        
        var currentRow = PendingTable.table.find('[data-row-id=' + data._id + ']');
        
        $(form).serializeArray().filter(function(field, index) { 
            if(field.name === "cancel_reason[]") {
                data.cancel_reason.push(field.value);
            }
        });

        if(data.cancel_memo === "") {
            UIkit.notify('กรุณาระบุรายละเอียดเพิ่มเติม', { pos: 'bottom-center', status: 'warning' });
            return;
        }

        if(data.cancel_reason.length <= 0) {
            UIkit.notify('กรุณาเลือกเหตุผลการยกเลิกงาน', { pos: 'bottom-center', status: 'warning' });
            return;
        }

        var loader = Loader.show();
        
        apiClient.verifyCancelJob(data, function (response) {
            setTimeout(function () {
                loader.hide();
                if (response.status) {

                    JobVerifyForm.clear();
                    if(response.data.cancel_verify) {
                        PendingTable.removeRow(currentRow);
                        VerifiedTable.addNewRow(response.data);
                    } else {
                        PendingTable.updateRow(response.data);
                    }

                    UIkit.notify('บันทึกข้อมูลเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                    JobVerifyModal.instance.hide();
                } else {
                    UIkit.notify("พบปัญหาบางอย่าง!", { pos: 'bottom-center', status: 'error' });
                }
            }, 1250);
        });
    }
}

var PendingTable = {
    table: $("#pending-datatable"),
    instance: null,
    loadData: function () {
        apiClient.getCancelJobList(null, function(res) {
            if (res instanceof Array && res.length > 0) {
                var dataSet = [];
                res.forEach(function (data, index) {
                    dataSet.push([
                        (index + 1),
                        data.cancel_case_id || '',
                        moment(data.created).format("YYYY/MM/DD HH:mm"),
                        data.drv_name || '',
                        data.drv_phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
                        data.cgroup,
                        data.curaddr || '',
                        data.destination || '',
                        data
                    ]);
                });
                PendingTable.createTable(dataSet);
            } else {
                PendingTable.createTable([]);
            }
        });
    },
    createTable: function (dataSet) {
        PendingTable.instance = PendingTable.table.DataTable({
            data: dataSet,
            order: [[0, 'asc']],
            columnDefs: [
                {
                    targets: 8,
                    width: "80px",
                    className: "uk-text-center",
                    render: function (index, type, data) {
                        var id = data[8]._id;
                        var btnGroup = '<a onclick="JobVerifyModal.open(\'' + id + '\');" data-uk-tooltip="{pos:\'top\'}" title="รายละเอียด"><i class="md-icon material-icons">&#xE88F;</i></a>';                        return btnGroup;
                    }
                }
            ],
            createdRow: function (row, data, index) {
                $(row).attr('data-row-id', data[8]._id);
                $(row).data(data[8]);
                
                if(data[8].cancel_opened) {
                    $(row).find('td:first').addClass('opened');
                }
            }
        });
    },
    removeRow: function (row) {
        PendingTable.instance.row(row).remove().draw();
    },
    updateRow: function (data) {

        var row = PendingTable.table.find('[data-row-id="' + data._id + '"]')[0];

        var rowIndex = PendingTable.instance.row(row).index();
        PendingTable.instance.row(rowIndex).data([
            rowIndex+1,
            data.cancel_case_id || '',
            moment(data.created).format("YYYY/MM/DD HH:mm"),
            data.drv_name || '',
            (data.drv_phone).replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
            data.cgroup,
            data.curaddr || '',
            data.destination || '',
            data
        ]);

        $(row).find('td:first').addClass('opened');
    }
}

var VerifiedTable = {
    table: $("#verified-datatable"),
    instance: null,
    loadData: function () {
        apiClient.getVerifiedCancelJobList(null, function(res) {
            if (res instanceof Array && res.length > 0) {
                var dataSet = [];
                res.forEach(function (data, index) {
                    dataSet.push([
                        (index + 1),
                        data.cancel_case_id || '',
                        moment(data.created).format("YYYY/MM/DD"),
                        data.drv_name || '',
                        data.drv_phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
                        data.cgroup || '',
                        data.curaddr || '',
                        data.destination || '',
                        data
                    ]);
                });
                VerifiedTable.createTable(dataSet);
            } else {
                VerifiedTable.createTable([]);
            }
        });
    },
    createTable: function (dataSet) {
        VerifiedTable.instance = VerifiedTable.table.DataTable({
            data: dataSet,
            order: [[0, 'asc']],
            columnDefs: [
                {
                    targets: 8,
                    width: "80px",
                    className: "uk-text-center",
                    render: function (data, type, row) {
                        var id = data._id;
                        var btnGroup = '<a onclick="JobVerifyModal.open(\'' + id + '\');" data-uk-tooltip="{pos:\'top\'}" title="รายละเอียด"><i class="md-icon material-icons">&#xE88F;</i></a>';
                        return btnGroup;
                    }
                }
            ],
            createdRow: function (row, data, index) {
                $(row).attr('data-row-id', data[8]._id);
                $(row).data(data[8]);
            }
        });
    },
    addNewRow: function (data) {
        VerifiedTable.instance.row.add([
            VerifiedTable.instance.rows()[0].length + 1,
            data.cancel_case_id || '',
            moment(data.created).format("YYYY/MM/DD"),
            data.drv_name || '',
            data.drv_phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3') || '',
            data.cgroup || '',
            data.curaddr || '',
            data.destination || '',
            data
        ]).draw();
    }
}

var apiClient = {
    onFail: function() {
            loader.hide();
            UIkit.notify("พบปัญหาบางอย่าง!", { pos: 'bottom-center', status: 'error' });
    },
    getCancelJobList: function(data, callback) {
        $.post(C.serviceURI.getCancelJobList, data, callback);
    },
    getCancelJobDetail: function(data, callback) {
        $.post(C.serviceURI.getCancelJobDetail, data, callback);
    },
    getVerifiedCancelJobList: function(data, callback) {
        $.post(C.serviceURI.getVerifiedCancelJobList, data, callback);
    },
    verifyCancelJob: function(data, callback) {
        $.post(C.serviceURI.verifyCancelJob, data, callback).fail(apiClient.onFail);
    }
}

var Loader = {
    show: function () {
        return UIkit.modal.blockUI('<div class="uk-text-center"><img class="uk-margin-top" src="assets/theme/altair/img/spinners/spinner.gif" alt=""><br/>กำลังทำงาน...</div>')
    }
}

var themeDemo = {
    init: function() {
        this.selectListEmail();
    },

    selectListEmail: function() {

        var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
            '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

        $('#selec_adv_2').selectize({
            plugins: {
                'remove_button': {
                    label     : ''
                }
            },
            persist: false,
            maxItems: null,
            valueField: 'email',
            labelField: 'name',
            searchField: ['name', 'email'],
            options: [
                {email: 'brian@thirdroute.com', name: 'Brian Reavis'},
                {email: 'nikola@tesla.com', name: 'Nikola Tesla'},
                {email: 'someone@gmail.com'}
            ],
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                        (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                        '</div>';
                },
                option: function(item, escape) {
                    var label = item.name || item.email;
                    var caption = item.name ? item.email : null;
                    return '<div>' +
                        '<span class="label">' + escape(label) + '</span>' +
                        (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                        '</div>';
                }
            },
            createFilter: function(input) {
                var match, regex;

                // email@address.com
                regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[0]);

                // name <email@address.com>
                regex = new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i');
                match = input.match(regex);
                if (match) return !this.options.hasOwnProperty(match[2]);

                return false;
            },
            create: function(input) {
                if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                    return {email: input};
                }
                var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
                if (match) {
                    return {
                        email : match[2],
                        name  : $.trim(match[1])
                    };
                }
                alert('Invalid email address.');
                return false;
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            }
        });
    }
}

App.bootstrap();