var serviceBaseURI = window.location.origin;
var notificationImageURI = window.location.origin + '/image/notification/';

var CONSTRAINTS = {
    serviceURI: {
        getAllGarage: serviceBaseURI + '/admin/notification/getAllGarage',
        getAllDriver: serviceBaseURI + '/admin/notification/getAllDriver',
        getNotificationList: serviceBaseURI + '/admin/notification/getNotificationList',
        getNotificationDetail: serviceBaseURI + '/admin/notification/getNotificationDetail',
        addNotification: serviceBaseURI + '/admin/notification/addNotification',
        editNotification: serviceBaseURI + '/admin/notification/editNotification'
    },
    defaultMaxLength: 3, // Months
    defaultDateLength: 7,
    Garage: null,
    mSocket: null
}

var C = CONSTRAINTS;

var Loader = {
    show: function () {
        return UIkit.modal.blockUI('<div class="uk-text-center"><img class="uk-margin-top" src="assets/theme/altair/img/spinners/spinner.gif" alt=""><br/>กำลังทำงาน...</div>')
    }
}

var App = {
    startLoader: Loader.show(),
    bootstrap: function () {

        C.mSocket = Socket.initial();

        setTimeout(function () {
            App.initialData();
            CKEditor.initial();
            App.setupPreviewDevice();
            NotificationTable.loadData();
            AddNotificationForm.init();
            EditNotificationForm.init();
            TargetGroup.init();
        }, 1000);
    },
    setupPreviewDevice: function () {
        PreviewDevice.init();
    },
    initialData: function() {
        $.post(C.serviceURI.getAllGarage, function (response) {
            if (response.status) {
                C.Garage = response.data;
                TargetGroup.garageList.init(response.data);
                TargetGroup.provinceList.init(new Province().getProvince('th'));
            }
        });
        $.post(C.serviceURI.getAllDriver, function (response) {
            if (response.status) {
                localStorage.setItem("drivers", JSON.stringify(response.data));
                IndividualSelect.init();
                App.startLoader.hide();
            }
        });
    }
}

var NotificationTable = {
    table: $('#notification-lsit-table'),
    instance: null,
    rowDataIndex: 6,
    loadData: function () {
        $.post(C.serviceURI.getNotificationList, NotificationTable.createTable);
    },
    createTable: function (res) {

        if (res.status) {

            var dataSet = [];
            res.data.forEach(function (notification, index) {
                dataSet.push([
                    notification.uid,
                    notification.type || '',
                    notification.title || '',
                    notification.status || '',
                    moment(notification.start).format("YYYY/MM/DD HH:mm"),
                    moment(notification.end).format("YYYY/MM/DD HH:mm"),
                    notification
                ]);
            });
        }

        NotificationTable.instance = NotificationTable.table.DataTable({
            data: dataSet,
            order: [[4, 'desc']],
            columnDefs: [
                {
                    targets: [0, 1, 3, 6],
                    width: "100px"
                },
                {
                    targets: [4, 5],
                    width: "150px"
                },
                {
                    targets: 6,
                    className: "uk-text-center",
                    render: function (data, type, row) {
                        var btnGroup = '<a onclick="EditNotificationForm.show(\'' + data._id + '\')" data-uk-tooltip="{pos:\'top\'}" data-uk-tooltip="{pos:\'top\'}" title="แก้ไข">';
                        btnGroup += '<i class="md-icon material-icons">&#xE254;</i></a>';
                        return btnGroup;
                    }
                }
            ],
            createdRow: function (row, data, index) {
                $(row).attr('data-row-id', data[6]._id);
                $(row).data(data[6]);
            },
            drawCallback: function (settings) {
            }
        });
    },
    addNewRow: function (notification) {
        this.instance.row.add([
            notification.uid,
            notification.type || '',
            notification.title || '',
            notification.status || '',
            moment(notification.start).format("YYYY/MM/DD HH:mm"),
            moment(notification.end).format("YYYY/MM/DD HH:mm"),
            notification
        ]).draw();
    },
    updateRow: function (row, notification) {
        var rowIndex = this.instance.row(row).index();
        this.instance.row(rowIndex).data([
            notification.uid,
            notification.type || '',
            notification.title || '',
            notification.status || '',
            moment(notification.start).format("YYYY/MM/DD HH:mm"),
            moment(notification.end).format("YYYY/MM/DD HH:mm"),
            notification
        ]).draw();
    },
    removeRow: function (row) {
        if (row)
            this.instance.row(row).remove().draw();
    },
    removeRowById: function (id) {
        var row = NotificationTable.table.find('tr[data-row-id=' + id + ']');
        NotificationTable.removeRow(row);
    },
    reloadData: function() {
        NotificationTable.instance.destroy();
        NotificationTable.instance = null;
        NotificationTable.loadData();
    }
}

var CKEditor = {
    initial: function () {
        $('form textarea[name=message]').ckeditor(function () { }, {
            customConfig: '/assets/theme/altair/js/custom/ckeditor_config.js',
            allowedContent: true
        });
    }
}

var TargetGroup = {
    modal: $('#target_group_modal'),
    instance: null,
    caller: null,
    counter: 0,
    init: function () {
        TargetGroup.instance = UIkit.modal(TargetGroup.modal, { modal: false });
        TargetGroup.modal.find('> .uk-modal-dialog').draggable();
    },
    provinceList: {
        select: $('#province-target .list-wrapper'),
        init: function (data) {
            TargetGroup.provinceList.select.empty();
            var uniqueProvince = TargetGroup.garageList.getUniqueProvince();
            data.forEach(function (value, index) {
                if (TargetGroup.provinceList.hasGarage(value.id, uniqueProvince)) {
                    var html = '<p>';
                    html += '<input type="checkbox" value="' + value.id + '" name="province_target[]" id="chk-pv-' + value.id + '" data-md-icheck />';
                    html += '<label for="chk-pv-' + value.id + '" class="inline-label"> ' + value.name + '</label>';
                    html += '</p>';
                    TargetGroup.provinceList.select.append(html);
                }
            });
            TargetGroup.provinceList.select.find('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_md',
                increaseArea: '20%'
            });
            TargetGroup.provinceList.bindEvent();
        },
        hasGarage: function (id, uniqueProvince) {
            return $.inArray(id, uniqueProvince) > -1;
        },
        bindEvent: function () {
            TargetGroup.provinceList.select.find('input[type=checkbox]').on('ifToggled', function (event) {
                if ($(this).is(':checked')) {
                    TargetGroup.garageList.select.find('input[type=checkbox][data-pv-id="' + parseInt($(this).val()) + '"]').iCheck('check');
                } else {
                    TargetGroup.garageList.select.find('input[type=checkbox][data-pv-id="' + parseInt($(this).val()) + '"]').iCheck('uncheck');
                }

                var count = 0;
                count += TargetGroup.countDriver(TargetGroup.garageList.getGaragesName());
                TargetGroup.counter = count;
                TargetGroup.modal.find('.counter-wrapper').find('.number').text(TargetGroup.counter);
            });
        },
        getValues: function () {
            var values = TargetGroup.provinceList.select.find('[name="province_target[]"]:checked').map(function (index, domElement) {
                return $(domElement).val();
            });
            return values;
        },
        disableAll: function() {
            TargetGroup.provinceList.select.find('input[type=checkbox]').iCheck('disable');
        },
        enableAll: function() {
            TargetGroup.provinceList.select.find('input[type=checkbox]').iCheck('enable');
        }
    },
    garageList: {
        select: $('#garage-target .list-wrapper'),
        init: function (data) {
            TargetGroup.garageList.select.empty();
            data.forEach(function (value, index) {
                var html = '<p>';
                html += '<input type="checkbox" value="' + value._id + '" name="garage_target[]" id="chk-' + value._id + '" ';
                html += 'data-cgroup="' + value.cgroup + '" data-pv-id="' + value.ProvinceID + '" data-md-icheck />';
                html += '<label for="chk-' + value._id + '" class="inline-label">' + value.name + '</label>';
                html += '</p>';
                TargetGroup.garageList.select.append(html);
            });
            TargetGroup.garageList.select.find('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_md',
                increaseArea: '20%'
            });
            TargetGroup.garageList.bindEvent();
        },
        bindEvent: function () {
            TargetGroup.garageList.select.find('input[type=checkbox]').on('ifClicked', function (event) {
                var count = 0;
                count += TargetGroup.countDriver(TargetGroup.garageList.getGaragesName());

                var checked = $(this).is(':checked');
                if (checked) {
                    var cgroup = $(this).data('cgroup');
                    var cgroupArr = [];
                    cgroupArr.push(cgroup);
                    count -= TargetGroup.countDriver(cgroupArr);
                } else {
                    var cgroup = $(this).data('cgroup');
                    var cgroupArr = [];
                    cgroupArr.push(cgroup);
                    count += TargetGroup.countDriver(cgroupArr);
                }
                TargetGroup.counter = count;
                TargetGroup.modal.find('.counter-wrapper').find('.number').text(TargetGroup.counter);
            });

            TargetGroup.garageList.select.find('input[type=checkbox]').on('ifChanged', function (event) {
                if ($(this).is(':checked')) {
                    var provinceID = $(this).data('pv-id');
                    var garageSelectedLength = TargetGroup.garageList.select.find('input[type=checkbox][data-pv-id=' + provinceID + ']:checked').length;
                    if(garageSelectedLength === TargetGroup.garageList.getGarageLengthByProvince(provinceID)) {
                        TargetGroup.provinceList.select.find('input[type=checkbox][value=' + provinceID + ']').iCheck('check');
                    }
                } else {
                    var selectedProvince = TargetGroup.garageList.getSelectedUniqueProvince();

                    var unselectedProvince = _.filter(TargetGroup.garageList.getUniqueProvince(), function (obj) {
                        return !_.findWhere(selectedProvince, obj);
                    });

                    if (unselectedProvince.length > 0) {
                        unselectedProvince.forEach(function (ID, index) {
                            TargetGroup.provinceList.select.find('input[type=checkbox][value=' + ID + ']').iCheck('uncheck');
                        });
                    }
                }
            });
        },
        getValues: function () {
            var values = TargetGroup.garageList.select.find('[name="garage_target[]"]:checked').map(function (index, domElement) {
                return $(domElement).val();
            });
            return values;
        },
        getGaragesName: function () {
            var values = TargetGroup.garageList.select.find('[name="garage_target[]"]:checked').map(function (index, domElement) {
                return $(domElement).data('cgroup');
            });
            return values;
        },
        getUniqueProvince: function () {
            var values = C.Garage.map(function (garage, index) {
                return garage.ProvinceID;
            });
            return _.uniq(values);
        },
        getSelectedUniqueProvince: function () {
            var datat = [];
            var selected = TargetGroup.garageList.getGaragesName().toArray();
            var dataProvinceID = [];
            C.Garage.forEach(function (garage, index) {
                $.inArray(garage.cgroup, selected) > -1 && dataProvinceID.push(garage.ProvinceID);
            });

            return _.uniq(dataProvinceID);
        },
        getGarageLengthByProvince: function(ID) {
            var count = 0;
                C.Garage.forEach(function(garage, index) {
                garage.ProvinceID === ID && count++;
            });
            return count;
        },
        disableAll: function() {
            TargetGroup.garageList.select.find('input[type=checkbox]').iCheck('disable');
        },
        enableAll: function() {
            TargetGroup.garageList.select.find('input[type=checkbox]').iCheck('enable');
        }
    },
    countDriver: function (garageArray) {

        if (garageArray.length === 0) {
            console.log('Garage array migth be an Array and length shold be more than 0.');
            return 0;
        }

        var count = 0;
        JSON.parse(localStorage.getItem('drivers')).forEach(function (driver, index) {
            $.inArray(driver.cgroup, garageArray) > -1 && count++;
        });
        return count;
    },
    countDriverByID: function(garageIDArray) {

        var garageArray = [];

        garageIDArray.forEach(function (ID, index) {
            for (var i = 0; i < C.Garage.length; i++) {
                if (C.Garage[i]._id === ID) {
                    garageArray.push(C.Garage[i].cgroup);
                    break;
                }
            }
        });

        return TargetGroup.countDriver(garageArray);
    },
    uncheckAll: function() {
        this.modal.find('input[type=checkbox]').iCheck('uncheck');
        TargetGroup.counter = 0;
        TargetGroup.modal.find('.counter-wrapper > .number').text(TargetGroup.counter);
    },
    submit: function() {
        if(this.caller === 'ADD_ACTION') {
            AddNotificationForm.garageArray = this.garageList.getValues();
            AddNotificationForm.provinceArray = this.provinceList.getValues();
            AddNotificationForm.form.find('[add-frm-target-group-counter] > .number').text(TargetGroup.counter);
        } else if (this.caller === 'EDIT_ACTION') {
            EditNotificationForm.garageArray = this.garageList.getValues().toArray();
            EditNotificationForm.provinceArray = this.provinceList.getValues().toArray();
            EditNotificationForm.form.find('[edit-frm-target-group-counter] > .number').text(TargetGroup.counter);
        }
        TargetGroup.instance.hide();
    },
    hideActionButton: function() {
        TargetGroup.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(0).hide();
        TargetGroup.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(1).hide();
    },
    showActionButton: function() {
        TargetGroup.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(0).show();
        TargetGroup.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(1).show();
    }
}

var IndividualSelect = {
    modal: $('#individual_select_modal'),
    select: $('#drivers_list'),
    instance: null,
    selectize: null,
    caller: null,
    counter: 0,
    init: function () {
        IndividualSelect.instance = UIkit.modal(IndividualSelect.modal, { modal: false });
        IndividualSelect.modal.find('> .uk-modal-dialog').draggable();
        IndividualSelect.selectize = IndividualSelect.select.selectize({
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
            options: JSON.parse(localStorage.getItem("drivers")),
            maxItems: null,
            valueField: '_id',
            labelField: 'fname',
            searchField: 'fname',
            create: false,
            render: {
                option: function (data, escape) {
                    return '<div class="option">' +
                        '<span class="title">' + data.fname + ' ' + data.lname + '</span>' +
                        '</div>';
                },
                item: function (data, escape) {
                    return '<div class="item">' + data.fname + ' ' + data.lname + '</div>';
                }
            },
            onDropdownOpen: function ($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function () {
                            $dropdown.css({ 'margin-top': '0' })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
            onDropdownClose: function ($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function () {
                            $dropdown.css({ 'margin-top': '' })
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    });
                IndividualSelect.counter = IndividualSelect.select.val() ? IndividualSelect.select.val().length : 0;
            }
        });
    },
    clearAll: function() {
        IndividualSelect.select[0].selectize.clear();
    },
    submit: function() {
        if(this.caller === 'ADD_ACTION') {
            AddNotificationForm.driverArray = IndividualSelect.select.val();
            AddNotificationForm.form.find('[add-frm-indl-slct-counter] > .number').text(IndividualSelect.counter);
        } else if (this.caller === 'EDIT_ACTION') {
            EditNotificationForm.driverArray = IndividualSelect.select.val();
            EditNotificationForm.form.find('[edit-frm-indl-slct-counter] > .number').text(IndividualSelect.counter);
        }
        IndividualSelect.instance.hide();
    },
    hideActionButton: function() {
        IndividualSelect.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(0).hide();
        IndividualSelect.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(1).hide();
    },
    showActionButton: function() {
        IndividualSelect.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(0).show();
        IndividualSelect.modal.find('.uk-modal-footer.uk-text-right').find('button').eq(1).show();
    }
}

var AddNotificationForm = {
    modal: UIkit.modal('#add_notification_modal'),
    instance: null,
    form: $('#add_notification_modal'),
    dateStart: null,
    dateEnd: null,
    message: null,
    provinceArray: [],
    garageArray: [],
    driverArray: [],
    init: function() {

        this.message = CKEDITOR.instances.message;

        var maxDate = moment().add(3, 'months').diff(moment(), 'days');

        this.dateStart = UIkit.datepicker(this.form.find('[name=start_date]'), { format: 'DD/MM/YYYY', minDate: 1, maxDate: maxDate });
        this.dateEnd = UIkit.datepicker(this.form.find('[name=end_date]'), { format: 'DD/MM/YYYY', minDate: 1, maxDate: maxDate });

        this.dateStart.element.val(moment().format('DD/MM/YYYY'));
        this.dateEnd.element.val(moment().add(C.defaultDateLength, 'days').format('DD/MM/YYYY'));

        this.form.find('[name=start_time],[name=end_time]').val(moment().format('HH:mm'));

        this.form
            .find('[name=start_date],[name=end_date]')
            .on('change', AddNotificationForm.onDateChange);

        this.form
            .find('[name=start_now]')
            .on('change', AddNotificationForm.onStartNowChange);

        this.form.find('.count-time').text(C.defaultDateLength + ' วัน');
    },
    onStartNowChange: function() {
        if($(this).is(':checked')) {
            AddNotificationForm.form.find('[name=start_date]').val(moment().format('DD/MM/YYYY')).attr('disabled', 'disabled');
            AddNotificationForm.form.find('[name=start_time]').val(moment().format('HH:mm')).attr('disabled', 'disabled');
            AddNotificationForm.onDateChange();
        } else {
            AddNotificationForm.form.find('[name=start_date]').val(moment().format('DD/MM/YYYY')).removeAttr('disabled');
            AddNotificationForm.form.find('[name=start_time]').val(moment().format('HH:mm')).removeAttr('disabled');
        }
    },
    onDateChange: function() {
        var form = AddNotificationForm.form;
        var start = moment(form.find('[name=start_date]').val(), "DD/MM/YYYY");
        var end = moment(form.find('[name=end_date]').val(), "DD/MM/YYYY");
        var duration = moment.duration(end.diff(start));
        var days = duration.asDays();
        AddNotificationForm.form.find('.count-time').text((parseFloat(days).toFixed(0)) + ' วัน');
    },
    submit: function (status) {

        var success = AddNotificationForm.validateForm();
        if(!success) {
            return;
        }

        var submit = function() {

            var loader = Loader.show();
            var formData = AddNotificationForm.getFormData();
            if(AddNotificationForm.isStartNow()) {
                formData.append('status', 'ACTIVE');
            } else {
                formData.append('status', status);
            }

            $.ajax({
                url: C.serviceURI.addNotification,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false
            }).done(function (response) {
                setTimeout(function () {
                    loader.hide();
                    if (response.status) {
                        NotificationTable.reloadData();
                        AddNotificationForm.modal.hide();
                        AddNotificationForm.clearFormData();
                        UIkit.notify('เพิ่มเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                    } else {
                        UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
                    }
                }, 1250);

            }).fail(function () {
                loader.hide();
                UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
            });
        }

        if(AddNotificationForm.isStartNow()) {
            AddNotificationForm.showConfirmSubmit('ข้อความนี้จะถูกส่งไปยังคนขับทันที ที่คุณกดตกลง', submit, null);
        } else {
            submit();
        }
    },
    validateForm: function() {

        var form = AddNotificationForm.form;

        if(form.find('[name=title]').val() === '') {
            form.find('[name=title]').focus();
            UIkit.notify("กรุณาระบุหัวข้อ", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=type]').val() === '') {
            form.find('[name=type]')[0].selectize.focus();
            UIkit.notify("กรุณาเลือกประเภท", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=message]').val() === '') {
            form.find('[name=message]').focus();
            UIkit.notify("กรุณาระบุเนื้อหา", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=base_image]').val() === '') {
            UIkit.notify("กรุณาเพิ่มรูป", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=start_date]').val() === '') {
            form.find('[name=start_date]').focus();
            UIkit.notify("กรุณาระบุวันที่เริ่ม", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=start_time]').val() === '') {
            form.find('[name=start_time]').focus();
            UIkit.notify("กรุณาระบุเวลาเริ่ม", { pos: 'bottom-center', status: 'info' });
            return false;
        }
        
        var start_date = form.find('[name=start_date]').val();
        var start_time = form.find('[name=start_time]').val();
        var diff = moment().diff(moment(start_date + ' ' + start_time, 'DD/MM/YYYY HH:mm'));
        if (diff >= 0 && !AddNotificationForm.isStartNow()) {
            UIkit.notify("กรุณาระบุวันที่เริ่มให้ถูกต้อง", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=end_date]').val() === '') {
            form.find('[name=end_date]').focus();
            UIkit.notify("กรุณาวันที่สิ้นสุด", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=end_time]').val() === '') {
            form.find('[name=end_time]').focus();
            UIkit.notify("กรุณาเวลาสิ้นสุด", { pos: 'bottom-center', status: 'info' });
            return false;
        }
        
        var end_date = form.find('[name=end_date]').val();
        var end_time = form.find('[name=end_time]').val();
        var diff = moment(end_date + ' ' + end_time, 'DD/MM/YYYY HH:mm').diff(moment(start_date + ' ' + start_time, 'DD/MM/YYYY HH:mm'));
        if (diff < 1000*60*60*24) {
            UIkit.notify("กรุณาระบุวันที่สิ้นสุดห่างจากวันที่เริ่มอย่างน้อน 1 วัน", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(parseInt(form.find('[add-frm-target-group-counter] > .number').text()) === 0 
            && parseInt(form.find('[add-frm-indl-slct-counter] > .number').text()) === 0) {
                UIkit.modal.alert("กรุณาเลือกผู้รับข้อความอย่างน้อย 1 คน");
                return false;
            }

        return true;
    },
    clearFormData: function() {

        var form = AddNotificationForm.form;

        form.find('input[name=title]').val('');
        AddNotificationForm.message.setData('');
        form.find('[add-frm-target-group-counter] > .number').text(0);
        form.find('[add-frm-indl-slct-counter] > .number').text(0);
        form.find('[name=type]')[0].selectize.setValue('');
        form.find('.count-time').text(C.defaultDateLength + ' วัน');
        form.find('input[type=file]').next('button.dropify-clear').click();
        form.find('input[name=start_date]').val(moment().format('DD/MM/YYYY'));
        form.find('input[name=start_time]').val(moment().format('HH:mm'));
        form.find('input[name=end_date]').val(moment().add(C.defaultDateLength, 'days').format('DD/MM/YYYY'));
        form.find('input[name=end_time]').val(moment().format('HH:mm'));

        var checked = AddNotificationForm.modal.find('input[name=start_now]')[0].checked;
        if(!checked) {
            AddNotificationForm.modal.find('input[name=start_now]').parent().find('.switchery').click();
        }

        AddNotificationForm.provinceArray = [];
        AddNotificationForm.garageArray = [];
        AddNotificationForm.driverArray = [];
        TargetGroup.caller = null;
        TargetGroup.uncheckAll();
        IndividualSelect.caller = null;
        IndividualSelect.clearAll();
        PreviewDevice.clearContent();
    },
    showConfirmSubmit: function(message, onConfirm, onCancel) {
        UIkit.modal.confirm(message + "<br>คุณแน่ใจแล้วใช่หรือไม่?", onConfirm, onCancel, {
            keyboard: false,
            bgclose: false
        });
    },
    getFormData: function () {

        var form = AddNotificationForm.form;
        var formData = new FormData(AddNotificationForm.form[0]);

        formData.append('start_date', form.find('[name=start_date]').val());
        formData.append('start_time', form.find('[name=start_time]').val());
        formData.set('message', AddNotificationForm.message.getData());

        for (var i = 0; i < AddNotificationForm.provinceArray.length; i++) {
            formData.append('province_selected[]', AddNotificationForm.provinceArray[i]);
        }

        for (var i = 0; i < AddNotificationForm.garageArray.length; i++) {
            formData.append('garage_selected[]', AddNotificationForm.garageArray[i]);
        }

        for (var i = 0; i < AddNotificationForm.driverArray.length; i++) {
            formData.append('driver_selected[]', AddNotificationForm.driverArray[i]);
        }

        return formData;
    },
    isStartNow: function() {
        return AddNotificationForm.form.find('[name=start_now]').is(':checked');
    },
    openTargetGroupModal: function() {
        TargetGroup.caller = 'ADD_ACTION';
        TargetGroup.instance.show();
    },
    openIndividualSelectModal: function() {
        IndividualSelect.caller = 'ADD_ACTION';
        var modal = IndividualSelect.instance;
        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
    },
    preview: function() {
        var title = AddNotificationForm.form.find('[name=title]').val();
        var message = AddNotificationForm.message.getData();
        var imageData = AddNotificationForm.form.find('[name=base_image]').parent().find('.dropify-preview img').attr('src');
        PreviewDevice.show(title, imageData, message);
    }
}

var EditNotificationForm = {
    modal: UIkit.modal('#edit_notification_modal'),
    form: $('#edit_notification_modal'),
    dateStart: null,
    dateEnd: null,
    startNow: null,
    message: null,
    provinceArray: [],
    garageArray: [],
    driverArray: [],
    init: function() {
        this.modal.on({
            'hide.uk.modal': function() {
                EditNotificationForm.clearFormData();
            }
        });
    },
    submit: function (status) {

        var success = EditNotificationForm.validateForm();
        if(!success) {
            return;
        }

        var loader = Loader.show();
        var formData = EditNotificationForm.getFormData();
        formData.append('status', status);

        $.ajax({
            url: C.serviceURI.editNotification,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false
        }).done(function (response) {
            setTimeout(function () {
                loader.hide();
                if (response.status) {
                    NotificationTable.reloadData();
                    EditNotificationForm.modal.hide();
                    EditNotificationForm.clearFormData();
                    UIkit.notify('เพิ่มเรียบร้อย', { pos: 'bottom-center', status: 'success' });
                } else {
                    UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
                }
            }, 1250);

        }).fail(function () {
            loader.hide();
            UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
        });
    },
    validateForm: function() {

        var form = EditNotificationForm.form;

        if(form.find('[type=hidden]').val() === '') {
            UIkit.notify("ไม่พบไอดีของฟอร์ม", { pos: 'bottom-center', status: 'danger' });
            return false;
        }

        if(form.find('[name=title]').val() === '') {
            form.find('[name=title]').focus();
            UIkit.notify("กรุณาระบุหัวข้อ", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=type]').val() === '') {
            form.find('[name=type]')[0].selectize.focus();
            UIkit.notify("กรุณาเลือกประเภท", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(EditNotificationForm.message.getData() === '') {
            UIkit.notify("กรุณาระบุเนื้อหา", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=start_date]').val() === '') {
            form.find('[name=start_date]').focus();
            UIkit.notify("กรุณาระบุวันที่เริ่ม", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=start_time]').val() === '') {
            form.find('[name=start_time]').focus();
            UIkit.notify("กรุณาระบุเวลาเริ่ม", { pos: 'bottom-center', status: 'info' });
            return false;
        }
        
        var start_date = form.find('[name=start_date]').val();
        var start_time = form.find('[name=start_time]').val();
        var diff = moment().diff(moment(start_date + ' ' + start_time, 'DD/MM/YYYY HH:mm'));
        if (diff >= 0) {
            form.find('[name=start_date]').focus();
            UIkit.notify("กรุณาระบุวันที่เริ่มให้ถูกต้อง", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=end_date]').val() === '') {
            form.find('[name=end_date]').focus();
            UIkit.notify("กรุณาวันที่สิ้นสุด", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if(form.find('[name=end_time]').val() === '') {
            form.find('[name=end_time]').focus();
            UIkit.notify("กรุณาเวลาสิ้นสุด", { pos: 'bottom-center', status: 'info' });
            return false;
        }
        
        var end_date = form.find('[name=end_date]').val();
        var end_time = form.find('[name=end_time]').val();
        var diff = moment(end_date + ' ' + end_time, 'DD/MM/YYYY HH:mm').diff(moment(start_date + ' ' + start_time, 'DD/MM/YYYY HH:mm'));
        if (diff < 1000*60*60*24) {
            form.find('[name=end_date]').focus();
            UIkit.notify("กรุณาระบุวันที่สิ้นสุดห่างจากวันที่เริ่มอย่างน้อน 1 วัน", { pos: 'bottom-center', status: 'info' });
            return false;
        }

        if (parseInt(form.find('[edit-frm-target-group-counter] > .number').text()) === 0
            && parseInt(form.find('[edit-frm-indl-slct-counter] > .number').text()) === 0) {
            UIkit.modal.alert("กรุณาเลือกผู้รับข้อความอย่างน้อย 1 คน");
            return false;
        }

        return true;
    },
    getFormData: function () {

        var form = EditNotificationForm.form;
        var formData = new FormData(EditNotificationForm.form[0]);

        formData.append('start_date', form.find('[name=start_date]').val());
        formData.append('start_time', form.find('[name=start_time]').val());
        formData.append('message', EditNotificationForm.message.getData());

        for (var i = 0; i < EditNotificationForm.provinceArray.length; i++) {
            formData.append('province_selected[]', EditNotificationForm.provinceArray[i]);
        }

        for (var i = 0; i < EditNotificationForm.garageArray.length; i++) {
            formData.append('garage_selected[]', EditNotificationForm.garageArray[i]);
        }

        for (var i = 0; i < EditNotificationForm.driverArray.length; i++) {
            formData.append('driver_selected[]', EditNotificationForm.driverArray[i]);
        }

        // Display the key/value pairs
        // for (var pair of formData.entries()) {
        //     console.log(pair[0]+ ', ' + pair[1]); 
        // }

        return formData;
    },
    show: function(id) {
        EditNotificationForm.getNotificationDetail(id);
    },
    getNotificationDetail: function(id) {
        var loader = Loader.show();
        $.post(C.serviceURI.getNotificationDetail, { id: id }, function(response) {
            setTimeout(function() {
                loader.hide();
                if(response.status) {
                    EditNotificationForm.fillData(response.data);
                    if(response.data.status === 'ACTIVE' || response.data.status === 'OBSOLETE') {
                        EditNotificationForm.setAsActiveStatusForm(response.data.status);
                    }
                    EditNotificationForm.modal.show();
                } else {
                    UIkit.notify('พบปัญหาบางอย่าง กรุณาลองอีกครั้ง', { pos: 'bottom-center', status: 'danger' });
                }
            }, 700);
        });
    },
    fillData: function(notification) {

        var form = EditNotificationForm.form;
        form.find('[name=title]').val(notification.title);

        EditNotificationForm.provinceArray = notification.province_selected;
        EditNotificationForm.garageArray = notification.garage_selected;
        EditNotificationForm.driverArray = notification.driver_selected;

        form.find('[name=start_date]').val(moment(notification.start).format('DD/MM/YYYY'));
        form.find('[name=start_time]').val(moment(notification.start).format('HH:mm'));
        form.find('[name=end_date]').val(moment(notification.end).format('DD/MM/YYYY'));
        form.find('[name=end_time]').val(moment(notification.end).format('HH:mm'));

        form.find('[name=type]')[0].selectize.setValue(notification.type);

        form.find('[edit-frm-target-group-counter] > .number').text(TargetGroup.countDriverByID(notification.garage_selected));
        form.find('[edit-frm-indl-slct-counter] > .number').text(notification.driver_selected.length);

        var image_uri = notificationImageURI + notification.image_url;
        var inputUpload = '<input type="file" name="base_image" class="dropify" data-height="100" data-max-file-size="200K" ';
        inputUpload += notification.status === "ACTIVE" ? "disabled" : "";
        inputUpload += ' data-max-width="300" data-default-file="' + image_uri + '" />';
        form.find('[input-wrapper-image]').html(inputUpload);
        form.find('.dropify').dropify();
        
        if(notification.status === 'ACTIVE' || 
            notification.status === 'OBSOLETE') {
            form.find('[input-wrapper-message]').html('<textarea contenteditable="true" class="md-input selecize_init" disabled name="message_edit_mode">' + notification.message + '</textarea>');
            form.find('[name=message_edit_mode]').ckeditor(function() { }, { 
                customConfig: '/assets/theme/altair/js/custom/ckeditor_config.js', 
                readOnly: true 
            });
        } else {
            form.find('[input-wrapper-message]').html('<textarea contenteditable="true" class="md-input selecize_init" name="message_edit_mode">' + notification.message + '</textarea>');
            form.find('[name=message_edit_mode]').ckeditor(function () { }, {
                customConfig: '/assets/theme/altair/js/custom/ckeditor_config.js',
                allowedContent: true,
            });
        }

        EditNotificationForm.message = CKEDITOR.instances.message_edit_mode;
        form.find('input[type=hidden][name=id]').val(notification._id);
    },
    clearFormData: function(status) {

        var form = EditNotificationForm.form;

        form.find('input[type=hidden][name=_id]').val('');

        form.find('input[name=title]').val('');
        form.find('[input-wrapper-message]').empty();
        EditNotificationForm.message = null;
        form.find('[edit-frm-target-group-counter] > .number').text(0);
        form.find('[edit-frm-indl-slct-counter] > .number').text(0);
        form.find('[name=type]')[0].selectize.setValue('');
        form.find('input[type=file]').next('button.dropify-clear').click();
        form.find('input[name=start_date]').val(moment().format('DD/MM/YYYY'));
        form.find('input[name=start_time]').val(moment().format('HH:mm'));
        form.find('input[name=end_date]').val(moment().add(C.defaultDateLength, 'days').format('DD/MM/YYYY'));
        form.find('input[name=end_time]').val(moment().format('HH:mm'));
        form.find('.uk-modal-footer.uk-text-right').find('button').eq(0).show();
        
        EditNotificationForm.form.find('[name=type]')[0].selectize.enable();
        EditNotificationForm.form.find('[name=base_image]').removeAttr('disabled');
        EditNotificationForm.form.find('input[type=text]').removeAttr('disabled');

        EditNotificationForm.provinceArray = [];
        EditNotificationForm.garageArray = [];
        EditNotificationForm.driverArray = [];
        TargetGroup.caller = null;
        TargetGroup.uncheckAll();
        TargetGroup.provinceList.enableAll();
        TargetGroup.garageList.enableAll();
        TargetGroup.showActionButton();
        IndividualSelect.caller = null;
        IndividualSelect.clearAll();
        IndividualSelect.showActionButton();
        IndividualSelect.selectize[0].selectize.enable();
        PreviewDevice.clearContent();
    },
    setAsActiveStatusForm: function(status) {
        EditNotificationForm.form.find('[name=type]')[0].selectize.disable();
        EditNotificationForm.form.find('[name=base_image]').attr('disabled', 'disabled');
        EditNotificationForm.form.find('input[type=text]').not('[name=end_date],[name=end_time]').attr('disabled', true);
        TargetGroup.provinceList.disableAll();
        TargetGroup.garageList.disableAll();
        TargetGroup.hideActionButton();
        IndividualSelect.hideActionButton();
        IndividualSelect.selectize[0].selectize.disable();
        if(status === 'OBSOLETE') {
            EditNotificationForm.form.find('input[name=end_date],input[name=end_time]').attr('disabled', true);
            EditNotificationForm.form.find('.uk-modal-footer.uk-text-right').find('button').eq(0).hide();
        }
    },
    openTargetGroupModal: function() {

        if(EditNotificationForm.provinceArray.length > 0) {
            EditNotificationForm.provinceArray.forEach(function(provinceId, index) {
                TargetGroup.provinceList.select.find('#chk-pv-' + provinceId).iCheck('check');
            });
        }

        if(EditNotificationForm.garageArray.length > 0) {
            EditNotificationForm.garageArray.forEach(function(garageId, index) {
                TargetGroup.garageList.select.find('#chk-' + garageId).iCheck('check');
            });
        }

        var count = 0;
        count += TargetGroup.countDriver(TargetGroup.garageList.getGaragesName());
        TargetGroup.counter = count;
        TargetGroup.modal.find('.counter-wrapper').find('.number').text(TargetGroup.counter);

        TargetGroup.caller = 'EDIT_ACTION';
        TargetGroup.instance.show();
    },
    openIndividualSelectModal: function() {
        IndividualSelect.select[0].selectize.setValue(EditNotificationForm.driverArray);
        IndividualSelect.caller = 'EDIT_ACTION';
        IndividualSelect.instance.show();
    },
    preview: function() {
        var title = EditNotificationForm.form.find('[name=title]').val();
        var message = EditNotificationForm.message.getData();
        var imageData = EditNotificationForm.form.find('[name=base_image]').parent().find('.dropify-preview img').attr('src');
        PreviewDevice.show(title, imageData, message);
    }
}

var PreviewDevice = {
    modal: null,
    init: function() {
        this.modal = UIkit.modal('#preview_modal', { modal: false });
        this.modal.find('.uk-modal-dialog').css({
            'width': "433px",
            'height': "800px",
            'background-repeat': "no-repeat",
            'background-size': "100%",
            'background-position-x': "center",
            'box-shadow': 'none',
            'background-color': 'transparent',
            'background-image': 'url(assets/img/notification_device_preview.png)'
        }).draggable();
    },
    show: function(title, imageData, message) {
        var content = '<h3>' + title + '</h3>';
        if( imageData ) { content += '<img src="' + imageData + '" />'; }
        content += '<p>' + message + '</p>';
        PreviewDevice.modal.find('.preview-content-wrapper').html(content);
        PreviewDevice.modal.show();
    },
    clearContent: function() {
        PreviewDevice.modal.find('.preview-content-wrapper').empty();
    }
}

var Socket = {
    initial: function () {

        Socket = io("/admin");

        Socket.on('NOTIFICATION_SENT', function (data) {

            var row = NotificationTable.table.find('[data-row-id="' + data._id + '"]');

            NotificationTable.updateRow(row, {
                end: data.end,
                start: data.start,
                status: data.status,
                title: data.title,
                type: data.type,
                uid: data.uid,
                _id: data._id
            });
        });

        Socket.on('NOTIFICATION_OBSOLETE', function (data) {

            var row = NotificationTable.table.find('[data-row-id="' + data._id + '"]');

            NotificationTable.updateRow(row, {
                end: data.end,
                start: data.start,
                status: data.status,
                title: data.title,
                type: data.type,
                uid: data.uid,
                _id: data._id
            });
        });

        Socket.on("connect", function (data) { });

        Socket.on("error", function (data) {
            console.log(new Date());
            console.log(data);
        });

        Socket.on('disconnect', function () {
        });

        return Socket;
    }
}

App.bootstrap();