$(function(){
	$('.garageadmin-forgotpassword').click(function(){
		$('.taxiadmin-overlay , .taxiadmin-dialog-forgotpassword').addClass('active');
	});
	$('.taxiadmin-dialog-close , .taxiadmin-overlay').click(function(){
		$('.taxiadmin-overlay , .taxiadmin-dialog-forgotpassword').removeClass('active');
	});

	$('.taxiadmin-change-password-btn').click(function(){

		$('.taxiadmin-change-password-overlay , .taxiadmin-change-password').addClass('active');
	});
	$('.taxiadmin-close-dialog , .taxiadmin-change-password-overlay').click(function(){
		$('.taxiadmin-change-password-overlay , .taxiadmin-change-password').removeClass('active');
	});

	if($('.taxiadmin-ico-menu').hasClass('active')){
		$('.icon-menu_show').attr('class','icon-menu_less');
	}
	$('.taxiadmin-ico-menu').click(function(){
		$(this).toggleClass('active');
		if($(this).hasClass('active')){
			$('.icon-menu_show').attr('class','icon-menu_less');
		} else {
			$('.icon-menu_less').attr('class','icon-menu_show');
		}
		$('.taxiadmin-navigation-menu , .taxiadmin-content').toggleClass('active');
	});
	$('.taxiadmin-table th a').click(function(){
		$(this).toggleClass('active');
		if($(this).hasClass('active')){
			$(this).children().attr('class','icon-ico_sortup');
		} else {
			$(this).children().attr('class','icon-ico_sortdown');
		}
	});




	$('.taxiadmin-taxi-status').click(function(){
		$('.taxiadmin-hidden-status , .taxiadmin-taxi-status').toggleClass('active');
		if($(this).hasClass('active')){
			$('.taxiadmin-taxi-status i').attr('class','icon-ico_sortdown');
		} else {
			$('.taxiadmin-taxi-status i').attr('class','icon-ico_sortup');
		}
	});
	$('.taxiadmin-header , .taxiadmin-map').click(function(){
		$('.taxiadmin-hidden-status').removeClass('active');
	});
	$('.taxiadmin-datepicker').datepicker({
		format: "dd/mm/yy",
    	language: "th",
    	disableTouchKeyboard: true
	});


	$('.taxiadmin-table td:first-child a').click(function(){
		$('.taxiadmin-driver-profile:not(.taxiadmin-taxi-profileupload)').addClass('active');
	});
	$('.taxiadmin-btn-add').click(function(){
		$('.taxiadmin-driver-uploadprofile , .taxiadmin-taxi-profileupload').addClass('active');
	});
	$('.taxiadmin-driver-profile i.icon-ico_close , .taxiadmin-driver-uploadprofile i.icon-ico_close').click(function(){
		$('.taxiadmin-driver-profile , .taxiadmin-driver-uploadprofile').removeClass('active');

	});
	$('.taxiadmin-user > ul > li').click(function(){
		$('.taxiadmin-user-option').toggleClass('active');
	});

	if($("#checkall").is(':checked')) {
		$("input[type=checkbox]").prop("checked", $("#checkall").prop("checked"));
	}
    $("#checkall").click(function() {
		$("input[type=checkbox]").prop("checked", $("#checkall").prop("checked"));
	});
    $("input[name=driver]").click(function() {
		$("#checkall").prop("checked", false);
	});

	$("table tr:nth-child(even)").addClass("even");
    $("table tr:nth-child(odd) , .taxiadmin-setting-form li:nth-child(odd)").addClass("odd");
});