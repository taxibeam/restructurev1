$(function(){

$('.garageadmin-showhidepassword').click(function() {
	if ($(this).prev().attr("type") == "password") {
		$(this).prev().attr("type", "text");
		//$(".password").attr("type", "text");
		$(this).attr("src","img/btn_hide.png");

		//$("i").text("visibility_off")
	} else {
		$(this).prev().attr("type", "password");
		//$("i").text("visibility")
		$(this).attr("src","img/btn_show.png");
		//$(".garageadmin").attr("src","img/btn_show.png");
	}
});

$('.garageadmin-changepassword .garageadmin-password').focus(function() {
	$('input[type="text"]').attr("type", "password");
	$('.garageadmin-password').next().attr("src","img/btn_show.png");
	$('input').next().removeClass('active');
	$(this).next().toggleClass('active');
});

$('.garageadmin-login input').focus(function() {
	$('input').next().removeClass('active');
	$(this).next().toggleClass('active');
	$('.garageadmin-password').attr("type", "password");
	$('.garageadmin-password').next().attr("src","img/btn_show.png");
});

$('.garageadmin-errormessage img').click(function() {
	$(".garageadmin-errormessage").removeClass('active');
});

$('.garageadmin-user > li').click(function() {
	$(".garageadmin-userhidden").toggleClass('active');
	$(".garageadmin-user").toggleClass('active');
	$(this).children('.garageadmin-arrow').attr("src","img/btn_arrowdown_w.png");
});

$('.garageadmin-user > li').hover(function() {
	$(this).children('.garageadmin-arrow').attr("src","img/btn_arrowdown_w.png");
}, function() {
	if($('.garageadmin-user').hasClass('active')) {
		$(this).children('.garageadmin-arrow').attr("src","img/btn_arrowdown_w.png");
	} else {
    	$(this).children('.garageadmin-arrow').attr("src","img/btn_arrowdown_b.png");
	}
});

var windowH = $(window).height();
console.log(windowH);

$('.garageadmin-btn-addGarage , .garageadmin-editdel .garageadmin-edit').click(function() {
	$('.garageadmin-addGarage').addClass('active');
	$('.garageadmin-addGarage-overlay').addClass('active');
	$('body').css('overflow','hidden');
	$('body').scrollTop(0);
});
$('.garageadmin-closeGarage , .garageadmin-addGarage-overlay , .garageadmin-addGarage input[type=submit]').click(function() {
	$('.garageadmin-addGarage').removeClass('active');
	$('.garageadmin-deleteGarage').removeClass('active');
	$('.garageadmin-addGarage-overlay').removeClass('active');
	$('body').css('overflow','auto');
});

/*$('.garageadmin-addGarage input[type=submit]').click(function() {
	$('.garageadmin-alertmessage').addClass('active');
});*/

$('.garageadmin-addGarage input[type=submit]').click(function() {
    $('.garageadmin-added').addClass('active');
    setTimeout(function(){
        $('.garageadmin-added').removeClass('active');          
    }, 3000);
});
$('.garageadmin-editdel .garageadmin-edit').click(function() {
    $('.garageadmin-edited').addClass('active');
    setTimeout(function(){
        $('.garageadmin-edited').removeClass('active');          
    }, 3000);
});

$('.garageadmin-editdel .garageadmin-delete').click(function() {
	$('.garageadmin-deleteGarage').addClass('active');
	$('.garageadmin-addGarage-overlay').addClass('active');
	$('body').css('overflow','hidden');
	//$('body').scrollTop(0);
});

$('.garageadmin-table-function .garageadmin-delete').click(function() {
	$('.garageadmin-deleteEmployee').addClass('active');
	$('.garageadmin-addGarage-overlay').addClass('active');
	$('body').css('overflow','hidden');
	//$('body').scrollTop(0);
});

$(document).scroll('touchmove' , function() {

    var scrolly = $(this).scrollTop();
    console.log(scrolly);


    $('.garageadmin-delete').on('click touchend' , function(){ 
      $('body').css('top', -scrolly);
      $('.garageadmin-addGarage-overlay').css('top', scrolly);
      //$('body').css('height', deviceH);
    });

    $('.garageadmin-mappreview').on('click touchend' , function(){ 
      $('body').css('top', -scrolly);
      $('.garageadmin-addGarage-overlay').css('top', scrolly);
      //$('body').css('height', deviceH);
    });
    
    $('.garageadmin-addGarage-overlay').on('click touchend' , function(){ 
      var contentScroll = $('body').css('top');
      //$('.ec-content').css('top', 300);
      var scrollFinal = contentScroll.slice(1, -2)
      $('body').removeAttr('style');
      $("html, body").scrollTop(scrollFinal);
      $('.garageadmin-addGarage-overlay').removeAttr('style');
    });
});

$('.garageadmin-table-function .garageadmin-edit').click(function() {
	$(this).parent().parent().addClass('active');
});

$('.garageadmin-map > img').click(function() {
	$('.garageadmin-mappreview').addClass('active');
	$('.garageadmin-addGarage-overlay').addClass('active');
	$('body').css('overflow','hidden');
});

$(document).mousedown(function(){
	if($('.cd-dropdown.cd-active').length){ $('.cd-dropdown span').trigger('click');
	} else {
	} 
});

/* Submit */

$('.garageadmin-login input[type=submit]').click(function() {
	//$(".garageadmin-errormessage").addClass('active');
});

$('.garageadmin-forgotpassword input[type=submit]').click(function() {
	$(".garageadmin-errormessage").addClass('active');
});

$('.garageadmin-changepassword input[type=submit]').click(function() {
	$(".garageadmin-errormessage").addClass('active');
	$(".garageadmin-errorpassword").addClass('active');
});








});