$(function(){

  var deviceH = $(window).height() , 
      documentH = $(document).height();

$('.taxibeam-why-tabs-left').click(function(){
  $('.taxibeam-why-tabs-left , .taxibeam-why-content-left').addClass('active');
  $('.taxibeam-why-tabs-right , .taxibeam-why-content-right').removeClass('active');

});

$('.taxibeam-why-tabs-right').click(function(){
  $('.taxibeam-why-tabs-left , .taxibeam-why-content-left').removeClass('active');
  $('.taxibeam-why-tabs-right , .taxibeam-why-content-right').addClass('active');
  
});

$('.taxibeam-how-tabs-left').click(function(){
  $('.taxibeam-how-tabs-left , .taxibeam-how-content-left').addClass('active');
  $('.taxibeam-how-tabs-right , .taxibeam-how-content-right').removeClass('active');
});

$('.taxibeam-how-tabs-right').click(function(){
  $('.taxibeam-how-tabs-left , .taxibeam-how-content-left').removeClass('active');
  $('.taxibeam-how-tabs-right , .taxibeam-how-content-right').addClass('active');
});

$('.taxibeam-question-tabs-left').click(function(){
  $('.taxibeam-question-tabs-left , .taxibeam-question-content-left').addClass('active');
  $('.taxibeam-question-tabs-right , .taxibeam-question-content-right').removeClass('active');
  if($(this).hasClass('not(.active)')) {
    $('.taxibeam-ask li ul , .taxibeam-ask li i').removeClass('active');
    $('.taxibeam-ask li i').attr("class","icon-arrowD");
  }
});

$('.taxibeam-question-tabs-right').click(function(){
  $('.taxibeam-question-tabs-left , .taxibeam-question-content-left').removeClass('active');
  $('.taxibeam-question-tabs-right , .taxibeam-question-content-right').addClass('active');
  if($(this).hasClass('not(.active)')) {
    $('.taxibeam-ask li ul , .taxibeam-ask li i').removeClass('active');
    $('.taxibeam-ask li i').attr("class","icon-arrowD");
  }
});

/*$('.taxibeam-btn-question').click(function(){
  $('.taxibeam-overlay , .taxibeam-popup-question').addClass('active');
  $('body').css('overflow','hidden');
  $('.taxibeam-icomenu-mb , .taxibeam-menu-mb').removeClass('active');
});

$('.taxibeam-btn-user').click(function(){
  $('.taxibeam-overlay , .taxibeam-popup-user').addClass('active');
  $('body').css('overflow','hidden');
});

$('.taxibeam-btn-problem').click(function(){
  $('.taxibeam-overlay , .taxibeam-popup-problem').addClass('active');
  $('body').css('overflow','hidden');
});

$('.taxibeam-btn-contact').click(function(){
  $('.taxibeam-overlay , .taxibeam-popup-contact').addClass('active');
  $('body').css('overflow','hidden');
});*/

$('.taxibeam-overlay , .taxibeam-popup-question i , .taxibeam-popup-contact i').click(function(){
  $('.taxibeam-overlay , .taxibeam-popup-user , .taxibeam-popup-problem , .taxibeam-popup-question , .taxibeam-popup-contact').removeClass('active');
  $('body').css('overflow-y','auto');
});


document.body.addEventListener("touchmove", function(e) {
  if($('.taxibeam-icomenu-mb').hasClass('active')){
       e.preventDefault(); 
  }  
}, false);

$('.taxibeam-icomenu-mb').click(function(){
  $(this).toggleClass('active');
  $('.taxibeam-menu-mb').toggleClass('active');
  $('body').toggleClass('active');
});

$('.taxibeam-menu-mb a').click(function(){
  $('.taxibeam-icomenu-mb , .taxibeam-menu-mb , body').removeClass('active');
  /*var link = $(this).attr('href');
  var test = link.slice(1);*/
});

var hash = window.location.hash;

if( hash == "#about") {
  $('.about a').click();
  function buildLin(first) {
    var firs = first.toString()
      document.location.hash = firs.replace(/ /g, "");
    }
  $(document).ready(function () {
      buildLin('');
  });
} else if( hash == "#why") {
  $('.why a').click();
  function buildLin(first) {
    var firs = first.toString()
      document.location.hash = firs.replace(/ /g, "");
    }
  $(document).ready(function () {
      buildLin('');
  });
} else if( hash == "#how") {
  $('.how a').click();
  function buildLin(first) {
    var firs = first.toString()
      document.location.hash = firs.replace(/ /g, "");
    }
  $(document).ready(function () {
      buildLin('');
  });
}

  $("a[href='#gototop']").bind('click , touchend' , function() {
      $("html, body").animate({ scrollTop: 0 }, "fast");
      return false;
  });

  var percentTop = (deviceH * 40)/100;

$(document).scroll('touchmove' , function() {

    var scrolly = $(this).scrollTop();

    if (scrolly > percentTop) {
      $('.taxibeam-gototop').fadeIn();
    } else {
      $('.taxibeam-gototop').fadeOut();
    }

    /*$('.taxibeam-btn-user').on('click touchend' , function(){ 
      $('body').css('top', -scrolly);
      $('.taxibeam-overlay , .taxibeam-popup-user').css('top', scrolly);
    });

    $('.taxibeam-btn-problem').on('click touchend' , function(){ 
      $('body').css('top', -scrolly);
      $('.taxibeam-overlay , .taxibeam-popup-problem').css('top', scrolly);
    });

    $('.taxibeam-btn-question').on('click touchend' , function(){ 
      $('body').css('top', -scrolly);
      $('.taxibeam-overlay , .taxibeam-popup-question').css('top', scrolly);
    });

    $('.taxibeam-btn-contact').on('click touchend' , function(){ 
      $('body').css('top', -scrolly);
      $('.taxibeam-overlay , .taxibeam-popup-contact').css('top', scrolly);
    });*/

    /*$('.taxibeam-icomenu-mb').on('click touchend' , function(){
      $('body').css('top', -scrolly);
      $('.taxibeam-menu-mb').css('top', scrolly);
   });

    $('.taxibeam-overlay').on('click touchend' , function(){ 
      var contentScroll = $('body').css('top');
      var scrollFinal = contentScroll.slice(1, -2);

      $('body').css('overflow-x','hidden');

      $("html, body").scrollTop(scrollFinal);
      $('.taxibeam-overlay').removeAttr('style');
    });*/
});

  $('.taxibeam-ask li').click(function(){
    $(this).children('ul').toggleClass('active');
    $(this).children('i').toggleClass('active');
    var current = $(this).children();
    var currenticon = $(this).children('i');
    if($('.taxibeam-ask li i').hasClass('active')){
      console.log('up');
      $(currenticon).attr("class","icon-arrowU active");   
    } else {
      console.log('down');
      $(currenticon).attr("class","icon-arrowD"); 
    }
    
    $('.taxibeam-ask li ul').not(current).removeClass('active');
    $('.taxibeam-ask li i').not(currenticon).attr("class","icon-arrowD");
  });

  var input = $('.taxibeam-popup-contact-inputform input , .taxibeam-popup-contact-inputform textarea');

  $('.taxibeam-popup-contact-submitform input').click(function(){
    if( !$(input).val() ) {
      $(input).next('i').addClass('active');
      $('.taxibeam-warning').addClass('active');
    }
    $('.taxibeam-popup-contact-inputform input').focus(function(){
      if( !$(input).val() ) { 
        cosole.log('null');
        $(this).next('i').removeClass('active');
      } else {
        $(this).next('i').addClass('active');
      }
    });
  });

  $('.taxibeam-popup-contact-inputform input').keypress(function(){
    $(this).next('i').removeClass('active');
    if( !$(input).val() ) {
      cosole.log('test');
    }
  });

});