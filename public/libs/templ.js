 var  formData_  = [{"aid":0,"name":"Taxi Approve","url":"/service/taxi/listall","coll":"drivers",
"json": {
      "rows": [
      [{
          "readOnly": true,
          "visible": true,
          "name": "imgface",
          "value": [""],
          "type": 60,
          "displayName": "รูปหน้าตรง",
          "col": "2",
          "title": "1"
        },{
          "readOnly": true,
          "visible": true,
          "name": "imglicence",
          "value": [""],
          "type": 60,
          "displayName": "รูปทะเบียน",
          "col": "2",
          "title": "1"
        },{
          "readOnly": true,
          "visible": true,
          "name": "imgcar",
          "value": [""],
          "type": 60,
          "displayName": "รูปรถ",
          "col": "2",
          "title": "1"
        },{
          "readOnly": true,
          "visible": true,
          "name": "imgczid",
          "value": [""],
          "type": 60,
          "displayName": "[บัตรประชาชน]",
          "col": "2",
          "title": "1"
        }],
        [{
          "readOnly": true,
          "visible": true,
          "name": "fname",
          "value": [""],
          "type": 10,
          "displayName": "ชื่อ",
          "col": "3",
          "title": "0"
        },{
          "readOnly": true,
          "visible": true,
          "name": "lname",
          "value": [""],
          "type": 10,
          "displayName": "นามสกุล",
          "col": "3",
          "title": "1"
        },{
          "readOnly": true,
          "visible": true,
          "name": "phone",
          "value": [""],
          "type": 10,
          "displayName": "โทรศัพท",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": true,
          "visible": true,
          "name": "carplate",
          "value": [""],
          "type": 10,
          "displayName": "ทะเบียนรถ",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": true,
          "visible": true,
          "name": "created",
          "value": [""],
          "type": 10,
          "displayName": "วันที่สมัคร",
          "col": "3",
          "title": "1"
        },{
          "readOnly": false,
          "visible": true,
          "name": "status",
          "value": [{"val":"PENDING","text":"PENDING"},{"val":"APPROVED","text":"APPROVED"},{"val":"CHANGE","text":"CHANGE"},{"val":"xxxxx","text":"xxx"}] ,
          "type": 20,
          "displayName": "สถานะ",
          "col": "4",
          "title": "1"
        },{
          "readOnly": true,
          "visible": true,
          "name": "active",
          "value": [{"val":"Y","text":"เปิด"},{"val":"N","text":"หยุด"}],
          "type": 20,
          "displayName": "เปิดใช้งาน",
          "col": "3",
          "title": "1"
        }]
      ],
      "hidden": []
    }
    },{"aid":1,"name":"taxi","url":"/service/taxi/listall","coll":"drivers",
"json": {
      "rows": [
      [{
          "readOnly": false,
          "visible": true,
          "name": "imgface",
          "value": [""],
          "type": 60,
          "displayName": "img",
          "col": "12",
          "title": "1"
        }],
        [{
          "readOnly": false,
          "visible": true,
          "name": "fname",
          "value": [""],
          "type": 10,
          "displayName": "ชื่อ",
          "col": "3",
          "title": "0"
        },{
          "readOnly": false,
          "visible": true,
          "name": "lname",
          "value": [""],
          "type": 10,
          "displayName": "นามสกุล",
          "col": "3",
          "title": "1"
        },{
          "readOnly": false,
          "visible": true,
          "name": "phone",
          "value": [""],
          "type": 10,
          "displayName": "ทะเบียนรถ",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": false,
          "visible": true,
          "name": "carplate",
          "value": [""],
          "type": 10,
          "displayName": "ทะเบียนรถ",
          "col": "3",
          "title": "1"
        },{
          "readOnly": false,
          "visible": true,
          "name": "carcolor",
          "value": [""],
          "type": 10,
          "displayName": "สี",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": false,
          "visible": true,
          "name": "status",
          "value": [""],
          "type": 10,
          "displayName": "สถานะ",
          "col": "4",
          "title": "1"
        }]
      ],
      "hidden": []
    }
    },{"aid":2,"name":"Passenger","url":"/service/passenger/listall","coll":"passengers",
    "json": {
      "rows": [
      [{
          "readOnly": false,
          "visible": true,
          "name": "phone",
          "value": [""],
          "type": 60,
          "displayName": "โทร",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": false,
          "visible": true,
          "name": "name",
          "value": [""],
          "type": 10,
          "displayName": "Name",
          "col": "3",
          "title": "0"
        },{
          "readOnly": false,
          "visible": true,
          "name": "email",
          "value": [""],
          "type": 10,
          "displayName": "email",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": false,
          "visible": true,
          "name": "curlat",
          "value": [""],
          "type": 10,
          "displayName": "curlat",
          "col": "3",
          "title": "1"
        },{
          "readOnly": false,
          "visible": true,
          "name": "curlng",
          "value": [""],
          "type": 10,
          "displayName": "curlng",
          "col": "3",
          "title": "1"
        },{
          "readOnly": false,
          "visible": true,
          "name": "deslat",
          "value": [""],
          "type": 10,
          "displayName": "deslat",
          "col": "3",
          "title": "1"
        },{
          "readOnly": false,
          "visible": true,
          "name": "deslng",
          "value": [""],
          "type": 10,
          "displayName": "deslng",
          "col": "3",
          "title": "1"
        }],
        [{
          "readOnly": false,
          "visible": true,
          "name": "deslng",
          "value": [""],
          "type": 10,
          "displayName": "deslng",
          "col": "12",
          "title": "1"
        }]
      ],
      "hidden": []      
    }},
    {"aid":3,"name":"Users","url":"/service/users/listall","coll":"users",
    "json": {
      "rows": [
         [{
          "readOnly": false,
          "visible": true,
          "name": "username",
          "value": [""],
          "type": 10,
          "displayName": "ชื่อ",
          "col": "3",
          "title": "0"
        },{
          "readOnly": false,
          "visible": true,
          "name": "email",
          "value": [""],
          "type": 10,
          "displayName": "email",
          "col": "3",
          "title": "1"
        }],        
        [{
          "readOnly": false,
          "visible": true,
          "name": "isActive",
          "value": [""],
          "type": 10,
          "displayName": "isActive",
          "col": "12",
          "title": "1"
        }]
      ],
      "hidden": []      
    }}
    ]