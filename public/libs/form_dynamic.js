function isEmpty(obj) {
  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0)    return false;
  if (obj.length === 0)  return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var formDynamic = function(){

  var init = function(){
    $(".nav-tabs").on("click", "a", function(e){
        e.preventDefault();
        $(this).tab('show');
      })
      .on("click", "span", function () {
        var anchor = $(this).siblings('a');
        $(anchor.attr('href')).remove();
        $(this).parent().remove();
        $(".nav-tabs li").children('a').first().click();
      });

    $('.add-contact').click(function(e) {
      e.preventDefault();
      var id = $(".nav-tabs").children().length;
      var el = $(this).closest('li').prev();
      el.before('<li><a href="#contact_'+id+'">เพิ่มเติม '+(id-2)+'</a><span>x</span></li>');
      $('.tab-content').append('<div class="tab-pane" id="contact_'+id+'"></div>');
      renderDataSubForm("div[id=contact_"+id+"]");
    });

    $("#add_tb_modal_value").click(function(){
      var tb = $("#tb_modal_value");
      var tr = $("<tr>");
      var td = "<td><input name='text' class='form-control input-sm'></td>";
      td += "<td><input name='value' class='form-control input-sm'></td>"
      tr.append(td)
      var btn_tool = $("<button>",{
        text: "-",
        class: "btn btn-danger btn-sm"
      }).click(function(){
        var tr = $(this).closest("tr").remove();
      });
      var tool_td = $("<td>")
      tool_td.append(btn_tool);
      tr.append(tool_td);
      tb.append(tr)
    });

    $("#save_modal_value").click(function(){
      var tr = $("#tb_modal_value").find("tr");
      var el_target = $("#modal_value input[name=input_el_target]").val();
      var json = [];
      _.each(tr, function(el){  
        json.push({
          val: $(el).find("input[name=value]").val(),
          text: $(el).find("input[name=text]").val()
        });
      });

      json = JSON.stringify(json);//.replace(/"/g, "'");
      $("#"+el_target).val(json);
      $("#modal_value").modal("hide");
    });

    $("#add_tb_modal_table").click(function(){
      var tb = $("#tb_modal_table");
      var tr = $("<tr>");
      var td = "<td><input name='id' class='form-control input-sm'></td>";
      td += "<td><input name='name' class='form-control input-sm'></td>"
      var sl = "<select name='type' class='form-control input-sm'>";
      sl += "<option value='text'>Text</option>";
      sl += "<option value='formula'>Formula</option>";
      sl += "</select>";
      td += "<td>"+sl+"</td>"

      tr.append(td)
      var btn_tool = $("<button>",{
        text: "-",
        class: "btn btn-danger btn-sm"
      }).click(function(){
        var tr = $(this).closest("tr").remove();
      });
      var tool_td = $("<td>")
      tool_td.append(btn_tool);
      tr.append(tool_td);
      tb.append(tr)
    });

    $("#save_modal_table").click(function(){
      // {"name":"สินค้า","id":"product"}
      var tr = $("#tb_modal_table").find("tr");

      var el_target = $("#modal_table input[name=input_el_target]").val();
      var json = [];
      _.each(tr, function(el){
        json.push({
          id: $(el).find("input[name=id]").val(),
          name: $(el).find("input[name=name]").val(),
          type: $(el).find("select[name=type] option:selected").val()
        });
      });

      json = JSON.stringify(json);//.replace(/"/g, "'");
      $("#"+el_target).val(json);
      $("#modal_table").modal("hide");
    });

    $("#save_modal_link_form").click(function(){
      var el_target = $("#modal_link_form input[name=input_el_target]").val();
      var val = $("#sl_link_form option:selected").val();
      $("#"+el_target).val(val);
      $("#modal_link_form").modal("hide");
    });
  };

  var getFormValueJson = function (el) {

    var datas = {main: {}, subform: [], table: [], config: {}};
    // role=config
    var form_config = el.find("form[role=config]");
    _.each(form_config.serializeArray(),function(item){
      datas.config[item.name] = item.value;
    });
    if (!isEmpty(datas.config.icon_url)) {
      var folder = $("#folder_icon option:selected").val();
      datas.config.icon_url = folder+"/"+datas.config.icon_url;
    } else {
      datas.config.icon_url = '';
    }
    // role=main
    var form_main = el.find("form[role=main]");
    var items = [];
    _.each(form_main.find("[disabled]"), function (item) {
      items.push(item);
      $(item).removeAttr("disabled");
    });
    var form_data = form_main.serializeArray();
    _.each(form_data, function(item){
      var val_tmp = item.value;
      if (this.find("[name="+item.name+"]").attr('el') == "number") {
        val_tmp = val_tmp.replace(/,/g, '');
      }
      var item_name = item.name.replace("[]", "");
      if(datas.main[item_name]){ 
        datas.main[item_name] += ","+val_tmp;
      } else {
        datas.main[item_name] = val_tmp;
      }
    }, form_main);
    _.each(items, function (item) {
      $(item).attr("disabled", '');
    });

    // role=subform
    datas.subform = [];
    var form_subform = el.find("form[role=subform]");
    _.each(form_subform, function(f, i){
      var items = [];
      datas.subform[i] = {};
      _.each($(f).find("[disabled]"), function (item) {
        items.push(item);
        $(item).removeAttr("disabled");
      });

      _.each($(f).serializeArray(),function(item){
        datas.subform[i][item.name] = item.value;
      });

      _.each(items, function (item) {
        $(item).attr("disabled", '');
      });
    });
    // role=table
    datas.table = [];
    var form_table = el.find("form[role=table] table tbody tr");
    _.each(form_table, function(tr, i){
      datas.table[i] = {};
      var tds = $(tr).find("td");
      _.each(tds, function(td){
        var el = $(td).find("input[type=text]");
        if (el.length > 0) {
          var name = el.attr("name");
          var val = el.val();
          datas.table[i][name] = val;
        }
      });

    });
    return datas;
  };

  var renderDynamicForm = function(datas, el){

    $(el).html("");
    $(el).append($("<form>", {
      role: "main",
      enctype: "multipart/form-data",
      method: "post"
    }));
    el = el+" form[role=main]";

  //  console.log(datas[0].rows)
    _.each(datas[0].rows, function(row, i) {
    // console.log(row)
      renderDataDynamicForm(row, el);
    });
    
    if(!isEmpty(datas.hidden)){
      renderDynamicHidden(datas.hidden, el);  
    }

    var el_formula = $(el).find("input[el-type=formula]");
    if (isEmpty(el_formula) == false) {
      
      $(el+" input").change(function(){
        var input = $(el+" input");
        var valEl = {};
        _.each(input, function(el){
          valEl[$(el).attr("name")] = $(el).val();
        });
        _.each(el_formula, function(el){
          var formula = $(el).attr("formula");
          
          _.each(_.keys(valEl), function(k){
            formula = formula.replace(k, Number(valEl[k]));
          });
          $(el).val( eval(formula) );
        });
      });
    }
    $(el+" input[el=number]").inputmask();
  };

  var renderDataDynamicForm = function (row, el) {
    var tmp_text = _.template($("#tmp_text").html()),
      tmp_combobox90 = _.template($("#tmp_combobox90").html()),
      tmp_header = _.template($("#tmp_header").html()),
      tmp_table = _.template($("#tmp_table").html()),
      tmp_date = _.template($("#tmp_date").html()),
      tmp_color = _.template($("#tmp_color").html()),
      tmp_checkbox = _.template($("#tmp_checkbox").html()),
      tmp_select = _.template($("#tmp_select").html()),
      tmp_select_multiple = _.template($("#tmp_select_multiple").html()),
      tmp_link = _.template($("#tmp_link").html()),
      tmp_image = _.template($("#tmp_image").html()),
      tmp_file_image = _.template($("#tmp_file_image").html()),
      tmp_formula = _.template($("#tmp_formula").html()),
      tmp_link_room = _.template($("#tmp_link_room").html()),
      tmp_number = _.template($("#tmp_number").html()),
      html;
    var div = $("<div>",{"class":"row"});

    $(el).append(div);
    _.each(row, function (r) {
      html = "";

      if (r.type == 10) { html = tmp_text(r) }
      if (r.type == 22) { html = tmp_date(r); }
      if (r.type == 23) { html = tmp_color(r); }
      if (r.type == 30) { html = tmp_checkbox(r); }
      if (r.type == 20) { html = tmp_select(r); }
      if (r.type == 21) { html = tmp_select_multiple(r); }
      if (r.type == 40) { html = tmp_link(r); }
      if (r.type == 60) { html = tmp_image(r); console.log('60' + r) }
      if (r.type == 110) { html = tmp_formula(r); }
      if (r.type == 130) { html = tmp_number(r); }

      if (r.type == 100) { 
        if (isEmpty(r.id_random)) {
          r.id_random = getRandomInt(1,10000000000);
        }
        html = tmp_file_image(r); 
      }
      if (r.type == 120) { 
        var val = r.value[0];
        if (val.trim() != "") {
          $.ajax({
            url: '/get_value_form',
            type: "GET",
            dataType: 'json',
            data: {
              dataform_id: val
            },
            async: false,
            success: function(data){
              var tmp = [];
              _.each(data, function(item){
                tmp.push({
                  text: item.name,
                  val: item.gid
                })
              });
              r.value = tmp
            }
          })
        }
        html = tmp_link_room(r); 
        r.value = [val]
      }      
      if (_.include([90, 91],r.type) ) {
        html = tmp_combobox90(r);
        html = renderType90(html, r);
      }
      if (r.type == 91) {
        html = renderType91(html, r, el);
      }
      if (r.type === 0) {
        html = tmp_header(r);
      }

      if (html) {
        div.append(html);
      }
      if(r.type == 80) {
        html = tmp_table(r);
        var form = $("<form>", {
          role: "table"
        });
        div.remove();
        form.append(html);
        div.append(form);
        $(el).parent().append(div);
        eventType80(r, div);
      }
      if(r.type == 100) {
        eventType100(r, div);
      }

    });
  };

  var renderDynamicHidden = function(row, el) {
    var tmp_hidden = _.template($("#tmp_hidden").html());
    _.each(row, function (r) {
      $(el).append(tmp_hidden(r));
    });
  };

  var renderType90 = function(html, item){
    html = $(html);
    var combobox = html.find("select");
    var value = item.value[0].split(";");
    $.ajax({
      url:  "../service/"+value[0],
      dataType: "text",
      type: "get",
      async: false,
      success: function(data) {
        data = JSON.parse(data);
        _.each(data, function(item, i){
          var opt = $("<option>",{
            value: item.id,
            parent_id: item.parent,
            text: item.name
          });
          if ( !isEmpty(value[1]) ) {
            if (value[1].trim() == item.id.trim()) {
              opt.attr("selected", "");
            }
          }
          combobox.append(opt);
        });
      },
      error: function (xhr, status, e) {
        console.log('erroe  '+e );
      }
    });
    return html;
  };

  var renderType91 = function(html, item, el) {
    var combobox = html.find("select");
    combobox.attr("disabled", "");
    var value = item.value[0].split(";");
    var parent_el = $(el+" select[name="+value[1]+"]");
    parent_el.change(function () {
      var val = $(this).find("option:selected").val();
      if ( val !== "") {
        combobox.removeAttr("disabled");
        combobox.find("option[value!='']").css("display", "none");
        combobox.find("option[parent_id="+val+"]").css("display", "block");
        combobox.find("option[value='']").attr("selected", "");
      } else {
        combobox.find("option").removeAttr("selected");
        combobox.find("option[value='']").attr("selected", "");
        combobox.attr("disabled", "");
      }
      combobox.change();
    }).change();
    return html;
  };

  var readInputFile = function(input, image) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        image.attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  };

  var eventType100 = function(item, div){

    div.find("button[id=btn"+item.id_random+"]").click(item, function(e){
      // debugger

      var r = e.data;
      var image = $("<img>",{
        height: 250,
        width: 250,
        "el-image-upload": 1,
        name: r.name,
        "id-target": r.name+r.id_random
      });

      var input_file = $("<input>",{
        type: "file",
        accept: "image/*",
        name: r.name,
        id: r.name+r.id_random
      }).change(image, function(evt){
        readInputFile(this, evt.data);
      });

      var div_row = $("<div>",{ class: 'col-sm-3'});
      div_row.append(image)
      div_row.append(input_file)
      $("#div"+r.id_random).append(div_row);


    });
  };

  var eventType80 = function(item, div) {
    var btn = div.find("#"+item.id+" button[btn-add=1]");
    btn.click(item, addTable);
  };

  var addTable = function(item) {
    var data = item.data;
    var tr = $("<tr>");
    _.each(data.cols, function(col){
      var td = $("<td>");
      var name = col.id;
      if (col.type == 'formula') {
        td.append("<span><input type='text' class='form-control input-sm' name='"+name+"' readonly table-el-type='formula' formula='"+col.name+"'></span>");
      } else {
        td.append("<span><input type='text' class='form-control input-sm' name='"+name+"'></span>");
      }
      td.append("<span style='display:none'></span>");
      tr.append(td);
    });
    var td = $("<td>",{
      style: "vertical-align: middle;",
      col_active: 1
    });
    // Save
    var btn_save = $("<button>",{
      text: "บันทึก",
      classname: "btn btn-info btn-sm",
      active: "save"
    }).click(function(evt){
      var td = $(this).closest("tr").find("td[col_active!=1]");
      _.each(td, function(item){
        span = $(item).find("span");
        val = $(span[0]).find("input").val();
        $(span[1]).html(val);
        $(span[1]).css("display", "block");
        $(span[0]).css("display", "none");
      });
      $(this).css("display", "none");
      $(this.nextElementSibling).css("display", "inline");
      return false;
    });
    td.append(btn_save);
    // Edit
    var btn_edit = $("<button>",{
      text: "แก้ไข",
      classname: "btn btn-info btn-sm",
      style: "display:none",
      active: "edit"
    }).click(function(evt){
      var td = $(this).closest("tr").find("td[col_active!=1]");
      _.each(td, function(item){
        span = $(item).find("span");
        $(span[1]).css("display", "none");
        $(span[0]).css("display", "block");
      });
      $(this).css("display", "none");
      $(this.previousElementSibling).css("display", "inline");
      return false;
    });
    td.append(btn_edit);
    // Delete
    btn_edit = $("<button>",{
      text: "ลบ",
      classname: "btn btn-danger btn-sm",
      active: "delete"
    }).click(function(evt){
      $(this).closest("tr").remove();
      return false;
    });
    td.append(btn_edit);
    tr.append(td);
    $("#"+data.id+" table tbody").append(tr);
    var el_formula = tr.find("input[table-el-type=formula]");
    if (isEmpty(el_formula) == false) {
      var input = tr.find("input[table-el-type!=formula]");
      input.change(function(){
        var valEl = {};
        _.each(input, function(el){
          valEl[$(el).attr("name")] = $(el).val();
        });
        _.each(el_formula, function(el){
          var formula = $(el).attr("formula");
          
          _.each(_.keys(valEl), function(k){
            formula = formula.replace(k, Number(valEl[k]));
          });
          $(el).val( eval(formula) );
        });
      });
    }
  };

  var renderSubForm = function(data, el) {

    _.each($(el).parent().children("div"), function(item, i){
      if (i>2) {
        $(item).remove();
      }
    });

    _.each($(el).parent().prev().find("li a[class!=add-contact]").parent(), function(item, i){
      if (i>2) {
        $(item).remove();
      }
    });

    if ( $.isEmptyObject( data ) ) {
       return false ;
    } 
    
    var li = $(el).parent().prev().find("li");
    li.removeClass("active");
    $(li.first()).find("a").click();
    renderSubForm.cache = _.find(jsonCollection, {id: data.id})
    if (!$.isEmptyObject(renderSubForm.cache) ) {
      renderSubForm.cache = renderSubForm.cache.json;
      if (!$.isEmptyObject(renderSubForm.cache) ) {
        renderSubForm.cache = JSON.stringify(renderSubForm.cache);
        renderDataSubForm(el);
      }
    }
    
  };
  renderSubForm.cache = "";

  var renderDataSubForm = function(el){
    $(el).html("");
    $(el).append($("<form>",{role: "subform"}));
    el = el+" form[role=subform]";
    data = renderSubForm.cache;
    if (data != "") {
      data = eval("(" + data + ')');
      _.each(data.rows, function(row, i) {
        renderDataDynamicForm(row, el);
      });
      renderDynamicHidden(data.hidden, el);
    }
  };

  // Get JSON for form generate  
  var getJSON = function(){
    var json = {
      rows: [],
      hidden: [],
      subform: {}
    };
    var rows = $("#element_row").children("div");
    _.each(rows, function(row) {
      var els = $(row).find("div[el=div_list_element] form");
      var r = []
      _.each(els, function(el) {
        var title = '0';
        if ($(el).find("input[type=checkbox][name=title]").is(":checked")) {
          title = '1'
        }

        var val = $(el).serializeArray();
        var type = _.find(val, { name: 'type' }).value;
        var value = _.find(val, { name: 'value' }).value;
        var readOnly = _.find(val, { name: 'readOnly' }).value;
        var visible = _.find(val, { name: 'visible' }).value;
        var name = _.find(val, { name: 'name' }).value;
        var displayName = _.find(val, { name: 'displayName' }).value;
        var col = _.find(val, { name: 'col' }).value;
        var cols = _.find(val, { name: 'cols' }).value;
        var id = _.find(val, { name: 'id' }).value;
        // if ( name == ''  ) {return false ; }
        if (type != "80" && type != '50') {
          if (type == "70") {
            json.subform = {
              type: Number(type),
              id: id
            }
          } else if (type == "60") { // Image
            console.log(JSON.parse(value))
             value = JSON.parse(value)
            r.push({
              type: Number(type),
              value: JSON.parse(value),
              title: title,
              name: name
            });
          } else if (type == "40") { //  Link
            value = "[\""+value+"\"]";
            r.push({
              type: Number(type),
              value: JSON.parse(value),
              col: col,
              displayName: displayName,
              title: title
            });
          } else if (type == "100") { // upload
            r.push({
              type: Number(type),
              name: name,
              displayName: displayName,
              title: title
            });
          } else {
            if (type == '20' || type == "21") {
              value = JSON.parse(value)
            } else {
              value = [value]
            }
            if (type == "130") {
              var sum = '0';
              if ($(el).find("input[type=checkbox][name=sum]").is(":checked")) {
                sum = '1'
              }
              r.push({
                readOnly: JSON.parse(readOnly),
                visible: JSON.parse(visible),
                name: name,
                value: value,
                type: Number(type),
                displayName: displayName,
                col: col,
                sum: sum,
                title: title
              });
            } else {
              r.push({
                readOnly: JSON.parse(readOnly),
                visible: JSON.parse(visible),
                name: name,
                value: value,
                type: Number(type),
                displayName: displayName,
                col: col,
                title: title
              });
            }
          }
        } else if (type == "80") {
          r.push({
            type: Number(type),
            cols: JSON.parse(cols),
            id: id,
            col: col,
            title: title
          })
        } else if (type == "50") {
          json.hidden.push({
            type: Number(type),
            name: name,
            value: [value],
            title: title
          })
        } 
      });
      json.rows.push(r);
    });
    return json;
  };

  return {
    init: init,
    renderDynamicForm: renderDynamicForm,
    renderSubForm: renderSubForm,
    getFormValueJson: getFormValueJson,
    getJSON: getJSON
  };

}();
