var formData = {};

if (Array.prototype.remove === undefined) {
  Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
  };
}


$(document).bind("ajaxSend", function() {
  $("#loadingPage").show();
}).bind("ajaxStop", function() {
  $("#loadingPage").hide();
}).bind("ajaxError", function() {
  $("#loadingPage").hide();
});

function decodeLine (encoded) {
  var len = encoded.length;
  var index = 0;
  var array = [];
  var lat = 0;
  var lng = 0;

  while (index < len) {
    var b;
    var shift = 0;
    var result = 0;
    do {
      b = encoded.charCodeAt(index++) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
    lat += dlat;

    shift = 0;
    result = 0;
    do {
      b = encoded.charCodeAt(index++) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
    lng += dlng;

    array.push({longitude:lng * 1e-5, latitude:lat * 1e-5});
  }

  return array;
}

function isEmpty(obj) {
  // null and undefined are "empty"
  if (obj == null) return true;

  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj.length > 0)    return false;
  if (obj.length === 0)  return true;

  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

function hex2rgb(hex,opacity){
  if ( typeof hex =='undefined' || hex=='') {return 'rgb(249, 2, 2)' ; } ;
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);

    result = 'rgba('+r+','+g+','+b+','+opacity+')';
    return result;
}

function newConfig(feature, aid) {
  resetFormConfig();
  var data_form = _.find(dataform.dataformJSON, {aid: aid});
  formData = data_form.json;
  $("#ModalCRUDLabel").html(data_form.name);

  var type = feature.getGeometry().getType();
  var form = $("#config form[role=config]");
  var clone_feature = feature.clone();
  var geo = clone_feature.getGeometry();
  geo.transform("EPSG:3857", "EPSG:4326");
  if (type == "Circle") {
    var circle_center = geo.getCenter();
    var circle_radius = feature.getGeometry().getRadius();
    form.find("[name=circle_center]").val(JSON.stringify(circle_center));
    form.find("[name=circle_radius]").val(circle_radius);
  } else {
    form.find("[name=circle_center]").val("");
    form.find("[name=circle_radius]").val("");
  }
  
  var opacity =  form.find("input[name=opacity]").closest("div");
  var icon_url =  form.find("input[name=icon_url]").closest("div");
  var line_width =  form.find("input[name=line_width]").closest("div");
  opacity.css("display", "block");
  icon_url.css("display", "block");
  line_width.css("display", "block");

  if (type == "LineString" || type == "Polygon") {
    icon_url.css("display", "none");
  }
  
  formDynamic.renderDynamicForm(formData, "#main");
  if (!$.isEmptyObject(formData.hidden)) {
    formDynamic.renderSubForm(formData, "#subform");
  }
  $('.date').datepicker({orientation: "top auto", format: "yyyy-mm-dd"});
  $('.color').colorpicker();
  $("#ModalCRUD").modal("show");
  $("#config_tab_main").click();
  $("#create_config").show();
  $("#update_config").hide();
  $("#close_config").show();
  var dataform_id = _.find(dataform.dataformJSON, {json: formData}).aid;
  $("#config form[role=config]").find("[name=dataform_id]").val(dataform_id);
  var modal = $("#ModalCRUD");
  modal.find("input").removeAttr('disabled');
  modal.find("select").removeAttr('disabled');
  modal.find("button").removeAttr('disabled');

  if (!isEmpty(data_form.config)) {
    var form = $("#config form[role=config]");          
    form.find("input[name=opacity]").val(data_form.config.opacity);
    form.find("input[name=line_width]").val(data_form.config.line_width);
    

    form.find("input[name=color_fill]").closest("div").colorpicker("setValue", data_form.config.color_fill);
    form.find("input[name=color_stroke]").closest("div").colorpicker("setValue", data_form.config.color_stroke);

    $("#folder_icon").find("option[value='"+data_form.config.folder_icon+"']").attr('selected', '');
    $("#folder_icon").change();
    $("#icon_url").find("option[value='"+data_form.config.icon_url+"']").attr('selected', '');
    $("#icon_url").change();
  }
}

function setValueFormConfig(keys, form, obj_json){
  _.each(keys, function(key){
    var el = form.find("[name="+key+"]");

   
    if (el.length == 0) {
      el = form.find("[name="+key+"\\[\\]]");
      var  val = obj_json[key];

    }

  //  if ( json_el.type == 60 ){
       
  //  }

    if(key != 'table' && key != 'subform' && el.length > 0){
      var val = obj_json[key];
      var json_el;

      _.each(formData[0].rows, function(row){ 
        var tmp = _.find(row, {name: key});
        if (!isEmpty(tmp)) {json_el = tmp;}
      });


      if (!isEmpty(json_el) && json_el.type == 100 ) {
        var div = form.find("div[id=div"+key+"]");
        _.each(val, function(v){
          var div_img = $("<div>",{class: "col-sm-3"});
          var img = $("<img>", {
            src: "/uploads/resize/"+v
          });
          div_img.append(img);
          div.append(div_img);
        });
      } else if ( !isEmpty(json_el) && json_el.type == 60) {
          var div = form.find("div[id=div_"+key+"]");
          var div_img = $("<div>",{class: "col-sm-3"});          
          var img = $("<img>", {
            src: "/image/driver/"+ val,
            alt: key ,
            height: 120,
            width: 120,
          });
          div_img.append(img);
          div.empty() ;
          div.append(div_img);

      } else if( !isEmpty(json_el) && json_el.type == 21) {
        val = val.split(',');
        _.each(val, function(v){
          if (v != "") {
            form.find("[name="+key+"\\[\\]][value="+v+"]").attr("checked", true);
          }
        });
      } else {
        val += "";
        if (val.trim() != '') {
          if(isEmpty(el)){
            el = form.find("[name="+key+"\\[\\]]")
          }
          var tag_name = el.prop('tagName');
          // textbox, radio, checkbox
          if (tag_name == "INPUT") { 
            var type = el.attr("type");
            if( type == "text"){
              el.val(val);  
            }
            if (type == 'radio' || type == 'checkbox') {
              val = val.split(',');
              _.each(val, function(v){
                form.find("[name="+key+"\\[\\]][value="+v+"]").attr("checked", true);
              });
            }
          }
          if (tag_name == "SELECT") {
            el.find("option[value="+val+"]").attr("selected", '').change();
          }

        }
      }

    } else if (key == 'table') {
      var form_table = $("#ModalCRUD .modal-body form[role=table]");
      _.each(obj_json[key], function(tb, i){
        form_table.find("[btn-add=1]").click();
        var tr = form_table.find("table tbody tr")[i];
        _.each(_.keys(tb),function(el){
          $(tr).find("input[name="+el+"]").val(tb[el]);
        });
        
      });
    }
  });
}

function editConfig(feature, callback) {
  var dataform_id = feature.get('dataform_id');
  var data = _.find(dataform.dataformJSON, {aid: dataform_id.toString()});
  formData = data.json;
  $("#ModalCRUDLabel").html(data.name);
  resetFormConfig();
  var gid = feature.get("gid");
  var type = feature.getGeometry().getType();
  var form = $("#config form[role=config]");
  // Config
  form.find("input[name=opacity]").val(feature.get("obj_bg_trans"));
  form.find("input[name=line_width]").val(feature.get("obj_ln_width"));
  form.find("input[name=color_fill]").closest("div").colorpicker("setValue", feature.get("obj_bg_color"));
  form.find("input[name=color_stroke]").closest("div").colorpicker("setValue", feature.get("obj_ln_color"));
  form.find("input[name=height]").val(feature.get("obj_height"));
  var icon = feature.get("obj_poi_icon").split("/");
  $("#folder_icon").find("option[value='"+icon[0]+"']").attr('selected', '');
  $("#folder_icon").change();
  $("#icon_url").find("option[value='"+icon[1]+"']").attr('selected', '');
  $("#icon_url").change();
  // End Config
  var clone_feature = feature.clone();
  var geo = clone_feature.getGeometry();
  geo.transform("EPSG:3857", "EPSG:4326");
  if (type == "Circle") {
    var circle_center = geo.getCenter();
    var circle_radius = feature.getGeometry().getRadius();
    form.find("[name=circle_center]").val(JSON.stringify(circle_center));
    form.find("[name=circle_radius]").val(circle_radius);
  } else {
    form.find("[name=circle_center]").val("");
    form.find("[name=circle_radius]").val("");
  }
 
  var opacity =  form.find("input[name=opacity]").closest("div");
  var icon_url =  form.find("input[name=icon_url]").closest("div");
  var line_width =  form.find("input[name=line_width]").closest("div");
  opacity.css("display", "block");
  icon_url.css("display", "block");
  line_width.css("display", "block");

  if (type == "LineString" || type == "Polygon") {
    icon_url.css("display", "none");
  }
  formDynamic.renderDynamicForm(formData, "#main");

  var form = $("#ModalCRUD .modal-body form[role=main]"),
      obj_json = feature.get("obj_json");
  var keys = _.keys(obj_json);
  setValueFormConfig(keys, form, obj_json);
  if (!$.isEmptyObject(formData.hidden)) {
    formDynamic.renderSubForm(formData, "#subform");
  }
  $('.date').datepicker({orientation: "top auto", format: "yyyy-mm-dd"});
  $('.color').colorpicker();
  $("#ModalCRUD").modal("show");
  $("#config_tab_main").click();
  $("#create_config").hide();
  $("#update_config").show();
  $("#close_config").show();
  $("#config form[role=config]").find("[name=gid]").val(gid);
  $("#update_config").unbind().click(feature, function(e){ 
    var feature = e.data;
    var wkt = new ol.format.WKT();
    var form = $(this).parent().parent().find("div.modal-body");
    var data = formDynamic.getFormValueJson( form );
    var cloneFeature = feature.clone();
    cloneFeature.getGeometry().transform('EPSG:3857', 'EPSG:4326');
    if (feature.getGeometry().getType() == "Circle") {
      var geo = cloneFeature.getGeometry();
      var center = geo.getCenter();
      var radius = feature.getGeometry().getRadius();
      var polygon = new ol.Feature({
        geometry: new ol.geom.Polygon.circular(
          new ol.Sphere(6378137), 
          center, 
          radius, 
          128
        )
      });
      data.config.wkt = wkt.writeFeature(polygon);
    } else { 
      data.config.wkt = wkt.writeFeature(cloneFeature);
    } 
    // role=main

    if (data.config.height.trim() != "") {
      if (data.config.wkt.search("POINT") != -1) {
        var str = data.config.wkt;
        str = str.replace(/POINT\(|\)/g, "")+" "+data.config.height;
        data.config.wkt = "POINT("+str+")";
      }
      if (data.config.wkt.search("POLYGON") != -1) {
        var str = data.config.wkt;
        str = str.replace(/POLYGON\(\(|\)\)/g, "");
        str = str.split(",");
        str = _.map(str, function(s){ return s+" "+data.config.height; });
        str = "POLYGON(("+str.join(",")+"))";
        data.config.wkt = str;
      }
      if (data.config.wkt.search("LINESTRING") != -1) {
        var str = data.config.wkt;
        str = str.replace(/LINESTRING\(|\)/g, "");
        str = str.split(",");
        str = _.map(str, function(s){ return s+" "+data.config.height; });
        str = "LINESTRING("+str.join(",")+")";
        data.config.wkt = str;
      } 
    }

    $.ajax({
      url:  "/update_main",
      dataType: "json",
      type: "post",
      async: false,
      data: {
        main: JSON.stringify(data.main),
        config: JSON.stringify(data.config)
      },
      success: function(data) {
        if (typeof callback === "function") {
          callback(feature);
        }
      },
      error: function (xhr, status, e) {
        console.log('erroe  '+e );
      }
    });

    // Upload
    var files = form.find("input:file")
    var formData = new FormData();
    formData.append("gid", gid)
    $.each(files, function(i, file) {
      if (file.value != "") {
        formData.append(file.name, file.files[0]);
      }
    });
    $.ajax({
        url: '/upload_image',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        async: false,
        success: function(data){}
    });

    // role=table
    _.each(data.table, function(item){
      $.ajax({
        url:  "/create_table",
        dataType: "text",
        type: "post",
        async: false,
        data: {
          table: JSON.stringify(item),
          gid: gid
        },
        success: function(data) {
          console.log("table success");
        },
        error: function (xhr, status, e) {
          console.log('erroe  '+e );
        }
      });
    });
    alert("Successfully");
    $("#mapToolbar button[data-group=cancel]").click();
    clearMap();
    setSourceLayer();
    popup.hide()
    if (MapConfig.mode == "search") {
      MapToolbar.query(MapToolbar.query.cache);
    }
    if (MapConfig.mode == "edit") {
      $("#mapToolbar").find("button").removeClass("active");
      $("#mapToolbar").find("button").removeAttr("disabled");
      $("#mapToolbar button[data-group=ok]").hide();
      $("#mapToolbar button[data-group=cancel]").hide();
      clearAll();
      $("#mapToolbar button[data-group=info]").click();
    }
    $("#mapToolbar button[data-group=info]").click();
  })
  var modal = $("#ModalCRUD");
  modal.find("input").removeAttr('disabled');
  modal.find("select").removeAttr('disabled');
  modal.find("button").removeAttr('disabled');
}

function infoConfig(feature) {
  resetFormConfig();
  var gid = feature.get("gid");
  var type = feature.getGeometry().getType();
  var form = $("#config form[role=config]");
  var clone_feature = feature.clone();
  var geo = clone_feature.getGeometry();
  geo.transform("EPSG:3857", "EPSG:4326");
  var opacity =  form.find("input[name=opacity]").closest("div");
  var icon_url =  form.find("input[name=icon_url]").closest("div");
  var line_width =  form.find("input[name=line_width]").closest("div");
  opacity.css("display", "block");
  icon_url.css("display", "block");
  line_width.css("display", "block");

  if (type == "LineString" || type == "Polygon") {
    icon_url.css("display", "none");
  }
  
  formDynamic.renderDynamicForm(formData, "#main");

  $('.date').datepicker("remove")

  if (!$.isEmptyObject(formData.hidden)) {
    formDynamic.renderSubForm(formData, "#subform");
  }
  $('.date').datepicker({orientation: "top auto", format: "yyyy-mm-dd"});
  $('.color').colorpicker();
  $("#ModalCRUD").modal("show");
  $("#config_tab_main").click();
  $("#create_config").hide();
  $("#update_config").hide();
  $("#close_config").hide();

  var modal = $("#ModalCRUD");
  modal.find("input").attr('disabled', '');
  modal.find("select").attr('disabled', '');
  modal.find("button").attr('disabled', '');
  modal.find("button.close").removeAttr('disabled');

  var form = $("#ModalCRUD .modal-body"),
      obj_json = feature.get("obj_json");
  var keys = _.keys(obj_json);

  setValueFormConfig(keys, form, obj_json);
}

function resetFormConfig(){
  var form = $("#config form[role=config]");
  form.find("input[name=name]").val("");
  form.find("input[name=opacity]").val(1);
  form.find("input[name=icon_url]").val("");
  form.find("input[name=line_width]").val("");

  form.find("input[name=gid]").val("");
  form.find("input[name=dataform_id]").val("");
  form.find("input[name=tb]").val("");
  form.find("input[name=height]").val("");
  
  colorpicker = form.find("div[colorpicker=1]");
  $.each(colorpicker, function(i, item){
    $(item).colorpicker("setValue", "");
  });
}


function renderImportWithPagination(data, div, type){
  div.empty();
  var tb = $("<table>", {
    class: "table table-striped table-bordered table-hover table-condensed",
    style: "margin-top:10px"
  });
  var th = $("<thead>");
  var tbody = $("<tbody>");
  var tr = $("<tr>");
  th.append(tr);
  tb.append(th)
  tb.append(tbody)
  tr.append("<td>รายการ</td>");

  _.each(data, function(row, i){
    var tr = $("<tr>");
    tr.attr("gid", row.gid);
    tr.attr("type", type);
    var td = $("<td>");
    var str = [];
    _.each(_.keys(row.obj_json), function(k){
      str.push("<b>"+k+": </b> "+ row.obj_json[k])
    });
    tr.append(td)
    td.append(str.join("<br>"));
    tbody.append(tr);
  });
  // gid: 16243
  // obj_json: Object
  div.append(tb); 

  var btn_ok = $("<button>", {
    class: 'btn btn-default icon-sm pull-right',
    style: 'display:none;'
  })
  btn_ok.append('<span class="glyphicon glyphicon-ok"></span>');
  div.prepend(btn_ok);

  var btn_cancel = $("<button>", {
    class: 'btn btn-default icon-sm pull-right',
    style: 'display:none;'
  })
  btn_cancel.append('<span class="glyphicon glyphicon-remove"></span>');
  div.prepend(btn_cancel);
      btn_cancel.click({tb: tb, btn_ok: btn_ok}, function(evt){
      evt.data.tb.find("tbody tr.selected").removeClass('selected');
      evt.data.btn_ok.hide();
      $(this).hide();
      featureOverlay.getFeatures().clear();
    });

  if (type == "correct") {
    btn_ok.click({tb: tb, btn_cancel: btn_cancel}, function(evt){
      var features = featureOverlay.getFeatures().getArray();
      if (features.length == 0) { return false; }
      var feature = features[0];
      var dataform_id = feature.get('dataform_id');
      var data = _.find(dataform.dataformJSON, {aid: dataform_id.toString()});
      formData = data.json;
      editConfig(feature);
      evt.data.tb.find("tbody tr.selected").removeClass('selected');
      evt.data.btn_cancel.hide();
      $(this).hide();
    });
  } 
  if (type == "incorrect") {
    btn_ok.click({tb: tb, btn_cancel: btn_cancel}, function(evt){
      var features = featureOverlay.getFeatures().getArray();
      if (features.length == 0) { return false; }
      var feature = features[0];
      var gid = Number(evt.data.tb.find("tbody tr.selected").attr("gid"));
      var data = _.find(MapConfig.dataImportIncorrect, {gid: gid});
      var dataform_id = data.dataform_id;
      var dataFrm = _.find(dataform.dataformJSON, {aid: dataform_id.toString()});
      formData = dataFrm.json;
      feature.set("obj_json", data.obj_json);
      feature.set("gid", data.gid);
      editConfig(feature, function(f){
        var gid = f.get("gid");
        var data = _.find(MapConfig.dataImportIncorrect, {gid: gid});
        var idx = _.findIndex(MapConfig.dataImportIncorrect, {gid: gid});
        MapConfig.dataImportIncorrect.remove(idx);
        MapConfig.dataImportCorrect.push(data);
        var importCorrect = $("#importCorrect");
        var importIncorrect = $("#importIncorrect");
        renderImportWithPagination(MapConfig.dataImportCorrect, importCorrect, 'correct');
        renderImportWithPagination(MapConfig.dataImportIncorrect, importIncorrect, 'incorrect');
      });
      evt.data.tb.find("tbody tr.selected").removeClass('selected');
      evt.data.btn_cancel.hide();
      $(this).hide();
    });
  }

  var table = tb.DataTable({
    language: {
      info: "_PAGE_/_PAGES_",
    },
    pagingType: "simple"
  });

  tb.find("tbody").on( 'click', 'tr', {btn_ok: btn_ok, btn_cancel: btn_cancel}, function (evt) {
    if ( $(this).hasClass('selected') ) {
      evt.data.btn_ok.hide();
      evt.data.btn_cancel.hide();
      $(this).removeClass('selected');
      featureOverlay.getFeatures().clear();
    }
    else {
      evt.data.btn_ok.show();
      evt.data.btn_cancel.show();
      table.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      var gid = Number($(this).attr("gid"));
      var type = $(this).attr("type");
      featureOverlay.getFeatures().clear();
      removeOtherInteraction();
      var rowData;
      if (type == "correct") {
        // rowData = _.find(MapConfig.dataImportCorrect, {gid: gid})
        var feature;
        var layer = map.getLayer("layer_geojson_point");
        var source = layer.getSource();
        var features = source.getFeatures();
        _.each(features, function(f, i){
          _.each(f.get("features"), function(tmp, j){
            if (Number(tmp.get("gid")) == gid) { feature = tmp };  
          });
        });

        var layer = map.getLayer("layer_geojson_other");
        var source = layer.getSource();
        var features = source.getFeatures();
        _.each(features, function(f, i){
          if (Number(f.get("gid")) == gid) { feature = f };
        });
        var collection = new ol.Collection();
        var featureClone = feature.clone();
        featureOverlay.addFeature(featureClone);
        var modify = new ol.interaction.Modify({
          features: featureOverlay.getFeatures(),
          style: overlayStyle
        });
        map.addInteraction(modify);
        view.setCenter(featureClone.getGeometry().getCoordinates());
        view.setZoom(15);
      }
      if (type == "incorrect") {
        // rowData = _.find(MapConfig.dataImportIncorrect, {gid: gid})

        var draw = new ol.interaction.Draw({
          features: featureOverlay.getFeatures(),
          type: 'Point'
        });

        draw.on('drawend', function(evt) {
          var feature = evt.feature;
          var tmpFeatures = featureOverlay.getFeatures().getArray();
          if(tmpFeatures.length > 0){
            featureOverlay.removeFeature(tmpFeatures[0])
          }
        });
        map.addInteraction(draw);

        var modify = new ol.interaction.Modify({
          features: featureOverlay.getFeatures(),
          style: overlayStyle
        });
        map.addInteraction(modify);

      }

    }
  });

}

function getDirectionCount() {
  var features = featureOverlay.getFeatures().getArray();
  var featureDirection = [];
  var way_point = [];
  _.each(features, function(f){
    if (f.get("route") == "direction") {
      featureDirection.push(f);
    }
  });
  return featureDirection.length;
}


function createPinDirection(feature) {
  var id = new Date().getTime();
  feature.set('route', 'direction');
  feature.set('seq', getDirectionCount());
  feature.set("id", id);
}