
var Manage = (function () {
  var init = function(){
 //   getDataForm();
    selectFormChange();
    eventBtnSave();
  };

var selectItem= {} ;
var aid_ , url_ ,coll_ , add_ ;
var templ_col ;

   var sl = $("#select-form");
       _.each(formData_, function(item, i){
          var opt = $("<option>",{
            value: item.aid,
            text: item.name,
            url: item.url ,
            coll:item.coll
          });
          sl.append(opt);
        });
    
  var selectFormChange = function(){
    $("#select-form").change(function(){
	       selectItem= _.find(formData_, function(obj) { return obj.aid ==  $("#select-form option:selected").val() })
           coll_ =  selectItem.coll
	  
      var dv = $("#atool");
       dv.empty() ;

      if ($("#select-form option:selected").val() == "") {
        $("#tb-detail").empty();
      } else {
             templ_col = [] ;                     
			   var form_tmp = selectItem.json ; 
              _.each(form_tmp.rows, function(row){
                _.each(row, function(item){
                  templ_col.push(item.name);           
                })
              });              

        detailGeomType();
              var btn_tool = $("<button>",{
                text: "Add New " + $("#select-form option:selected").text() ,
                class: "btn btn-danger btn-sm"
              }).click(function(){               
                      add_ = true ;
                      gid  = "";
                      var data = {} ;                      
                      formData = selectItem.json;  
                      var body_preview = $("#body-preview");
                      body_preview.empty();    
                      formDynamic.renderDynamicForm(formData, "#body-preview");
                      var form = body_preview.find("form[role=main]"),
                          obj_json = data;
                      var keys = _.keys(obj_json);    
                      setValueFormConfig(keys, form, obj_json);
                      $("#modal-preview").modal();
                   // };


              });
             dv.append(btn_tool);
      }
    });



  };

  var detailGeomType = function(){
    $.ajax({
      url:  $("#select-form option:selected").attr("url") ,
      data: {
        aid: $("#select-form option:selected").val(),
        req:templ_col
      },
      dataType: "json",
      type: "post",
      async: false,
      success: function(data) {
        
        data = data.data;
        var items = [];               
		var    form_tmp = selectItem.json;  
        _.each(form_tmp.rows, function(row){
          _.each(row, function(item){
            items.push(item);           
          })
        });
        
        var div = $("#tb-detail");
        div.empty();
        var tb = $("<table>",{
          class: "col-md-12  table table-striped table-bordered table-hover table-condensed cf"
        });
        var thead = $("<thead class='cf'>");
        var tbody = $("<tbody>");
        tb.append(thead);
        tb.append(tbody);
        div.append(tb);
        cols_data_table = [];
        var header = [];        
        _.each(data, function(row, i){          
          if (i == 0) {
            var tr = $("<tr>");
            _.each(_.keys(row), function(k){
              if (k != 'table' && k != "subform") {
                header.push(k)
                var str = k;
                var tmp = _.find(items, {name: k})                                
                if (tmp) { str =tmp.displayName   };
                
                tr.append("<th>"+str+"</th>");

                cols_data_table.push({bSortable: false})
              }
            });
            thead.append(tr);
          }
          var tr = $("<tr>");          
          _.each(header, function(k){
            if (k != 'table' && k != "subform") {              
              var a = row[k] ;              
              if ( k == '_id' ) {
                   //tr.append("<td data-title=''>"+ ( i+1)  +"</td>");
                  tr.append("<td data-title=\"" +k+" \">"+  ( i+1) +"</td>");
              } else {
                    if ( typeof a == 'string' &&  a.substring(a.length,a.length-4 ) == '.png' ) {
                        tr.append("<td>"+"ภาพ..."+"</td>");
                    } else {
                        tr.append("<td data-title=\"" +k+" \">"+(row[k]||"")+"</td>");
                    }
            }
            }
          });
          tr.click(row, function(e){
            rowClick(e.data);
          });
          tbody.append(tr);
        });
        var table = tb.DataTable({
          aaSorting: [],
          // bFilter: false,
          bLengthChange: false,
          // bAutoWidth: false,
          aoColumns: cols_data_table,
          // bProcessing: true,
          // bServerSide: true,
          language: {
            info: "_PAGE_/_PAGES_",
          },
          pagingType: "simple"
        });
      },
      error: function (xhr, status, e) {
        console.log('error  '+e );
      }
    });
  };
 
  var rowClick = function(data){
    add_ = false  ;
    gid = data._id;    
    
	 formData =  selectItem.json ;    
    var body_preview = $("#body-preview");
    body_preview.empty();    
    formDynamic.renderDynamicForm(formData, "#body-preview");

    var form = body_preview.find("form[role=main]"),
        obj_json = data;
    var keys = _.keys(obj_json);    
       // console.log( 'obj_json' )
       // console.log( obj_json )
       // console.log( 'key' )
       // console.log( keys )
       // console.log( 'form' )
       // console.log( form )
    setValueFormConfig(keys, form, obj_json);
    $("#modal-preview").modal();
  };

  var eventBtnSave = function(){
    $("#btn_save").click(function(){
      var form = $(this).parent().parent().find("div.modal-body");
      var data = formDynamic.getFormValueJson( form );
      
      // role=main
      if (add_ ) { gid = '' }
      $.ajax({
        url:  "/manage_data/update_main",
        dataType: "json",
        type: "post",
        async: false,
        data: {
          coll: coll_ ,
          main: JSON.stringify(data.main),
          gid: gid
        },
        success: function(data) {},
        error: function (xhr, status, e) {
          console.log('erroe  '+e );
        }
      });

      // Upload
      var files = form.find("input:file")
      var formData = new FormData();
      var check_file = false;
      formData.append("gid", gid)
      $.each(files, function(i, file) {
        if (file.value != "") {
          formData.append(file.name, file.files[0]);
          check_file = true;
        }
      });
      if (check_file) {
        $.ajax({
          url: '/upload_image',
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          type: 'POST',
          async: false,
          success: function(data){}
        });
      }

      // Table
      _.each(data.table, function(item){
        $.ajax({
          url:  "/create_table",
          dataType: "text",
          type: "post",
          async: false,
          data: {
            table: JSON.stringify(item),
            gid: gid
          },
          success: function(data) {
            console.log("table success");
          },
          error: function (xhr, status, e) {
            console.log('erroe  '+e );
          }
        });
      });
      $("#modal-preview").modal('hide');
      $("#select-form").change();
    });
  };


  return {
    init: init
  };
})()
