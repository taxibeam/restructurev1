/*
*  Altair Admin
*  Configuration file for ckeditor (wysiwyg editor)
*/

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.skin = 'material_design,/assets/theme/altair/skins/ckeditor/material_design/';
    
    config.toolbarGroups = [ 
        { name: 'basicstyles', groups: [ 'basicstyles'] },
        { name: 'colors' }
     ];
    
    config.colorButton_colors = 'CF5D4E,454545,FFF,CCC,DDD,CCEAEE,66AB16';

    config.extraPlugins = 'wordcount,colorbutton';

    config.wordcount = {

        // Whether or not you want to show the Paragraphs Count
        showParagraphs: false,

        // Whether or not you want to show the Word Count
        showWordCount: false,

        // Whether or not you want to show the Char Count
        showCharCount: true,

        // Whether or not you want to count Spaces as Chars
        countSpacesAsChars: true,

        // Whether or not to include Html chars in the Char Count
        countHTML: false,
        
        // Maximum allowed Word Count, -1 is default for unlimited
        maxWordCount: -1,

        // Maximum allowed Char Count, -1 is default for unlimited
        maxCharCount: 1000,

        // Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
        filter: new CKEDITOR.htmlParser.filter({
            elements: {
                div: function( element ) {
                    if(element.attributes.class == 'mediaembed') {
                        return false;
                    }
                }
            }
        })
    };

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    config.height = 328;

    config.autoGrow_minHeight = 328;
    config.autoGrow_maxHeight = 520;
    config.removePlugins = 'resize';
    config.resize_enabled = false;

    config.filebrowserUploadUrl = '/admin/uploader';

};
