$(function() {
    // datatables
    altair_datatables.dt_default();
});

altair_datatables = {
    dt_default: function() {
        var $dt_default = $('#dt_default');
        if($dt_default.length) {
            $dt_default.DataTable();
        }
    }
};


// #343642