var test;
var delDataForm = function(el, aid){
  var r = confirm("คุณแน่ใจแล้วใช่หรือไม่");
  if (!r) { return;}
  $.ajax({
    type: "get",
    url: "/delete_dataform",
    data: {aid: aid},
    dataType: "json",
    success: jQuery.proxy(function (data) {
      $(this).closest("tr").remove();
      alert("ลบเสร็จเรียบร้อย");
    },el)
    
  });
};

var getFormInit = function(){
  $.ajax({
    type: "get",
    url: "/templ/get",
    async: false,
    success: function(data){
      test = data ;
     

      var tbody = $("#tbody");
      _.each(data, function(row){
        var tr = $("<tr>");

        tr.append($("<td>", {
          text: row.templtype
        }));

        tr.append($("<td>", {
          text: row.name
        }));
        
        var td = $("<td>");

        var tag_i = $("<i>", {
          class: "glyphicon glyphicon-pencil"
        });
        var edit = $("<a>",{
          href: "edit?aid="+row.aid,
          class: "btn btn-info btn-xs"
        });
        edit.append(tag_i);
        td.append(edit)

        tag_i = $("<i>", {
          class: "glyphicon glyphicon-remove"
        });
        var del = $("<a>",{
          href: "javascript:void(0)",
          class: "btn btn-danger btn-xs",
          style: "margin-left:10px;"
        }).click(row.aid, function(e){ delDataForm(this, e.data); });
        del.append(tag_i);
        td.append(del)
        tr.append(td)
        tbody.append(tr)
      });

    },
    dataType: "json"
  });
}

$(function () {
  getFormInit()
})