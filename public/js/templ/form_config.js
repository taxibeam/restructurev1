function resetFormConfig(){
  var form = $("#tabConfig form[role=config]");
  form.find("input[name=opacity]").val(1);
  form.find("input[name=icon_url]").val("");
  form.find("input[name=line_width]").val("");

  form.find("input[name=gid]").val("");
  form.find("input[name=dataform_id]").val("");
  form.find("input[name=tb]").val("");
  
  colorpicker = form.find("div[colorpicker=1]");
  $.each(colorpicker, function(i, item){
    $(item).colorpicker("setValue", "");
  });
  $("#folder_icon").find("option").removeAttr("selected");
  $("#icon_url").empty();
  $("#icon_url").change();
}


var formConfig = (function(){
  var init = function(){
    listFolder();
    eventFolderChange();
    $("#icon_url").select2({
      allowClear:true,
      placeholder: "Select icon",
      templateResult: function formatState (state) {
        var folder = $("#folder_icon option:selected").val();
        if (!state.id) { return state.text; }
        var $state = $(
          '<span><img src="/icons/'+folder+'/' + state.element.value + '" class="img-flag" /> ' + state.text + '</span>'
        );
        return $state;
      }
    });
    $("div[colorpicker=1]").colorpicker({align: 'right'});
    $("#linkTabForm").click();
  };

  var listFolder = function(){
    $.ajax({
      url:  "/templ/list_folder_pins",
      dataType: "json",
      type: "get",
      async: false,
      success: function(data) {
        var sl  = $("#folder_icon");
        _.each(data, function(item){
          var opt = $("<option>",{
            text: item,
            value: item
          });
          sl.append(opt);
        });
      },
      error: function (xhr, status, e) {
        console.log('erroe  '+e );
      }
    });  
  };

  var eventFolderChange = function(){
    $("#folder_icon").change(function(){
      var val = $("#folder_icon option:selected").val();
      if (val == "") {
        var sl  = $("#icon_url");
        sl.empty();
        sl.change();
        return false;
      }
      $.ajax({
        url:  "/templ/list_pins",
        dataType: "json",
        type: "get",
        data: {
          folder: val
        },
        async: false,
        success: function(data) {
          var sl  = $("#icon_url");
          sl.empty();
          _.each(data, function(item){
            var opt = $("<option>",{
              text: item,
              value: item
            });
            sl.append(opt);
          });
          sl.change();
        },
        error: function (xhr, status, e) {
          console.log('erroe  '+e );
        }
      });
    });
  };

  return {
    init: init
  };
})();