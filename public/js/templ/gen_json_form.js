var jsonCollection = [];
function readURL(input, image) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      image.attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function getComboFile () {
  $.ajax({
    type: "get",
    url:  "/templ/get_combofile"  , ///get_combofile",
    async: false,
    success: function(data){
      var sl = $("#frm_combofile select[name=sl_combofile]");
      _.each(data, function(item){
        var op = $("<option>",{
          text: item.combofile,
          value: item.combofilepath
        });
        sl.append(op);
      });
    },
    dataType: "json"
  });
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(function() {
  getComboFile();
  $("#add_row").click(function() {
    var element_row = $("#element_row");
    var row = _.template($("#tmp_row").html());
    var num = element_row.children("div").length + 1;
    row = row({
      num: num
    });
    element_row.append(row);
    $(element_row.children("div").last()).find("div[el=div_list_element]").sortable();
    $(element_row.children("div").last()).find("div[el=div_tool_row] button[el=btn_add_element]").click(function() {
      var row_tmp = $("#tmp_element").html();
      row_tmp = _.template(row_tmp);
      var list_element = $(this.parentElement.parentElement).find("div[el=div_list_element]");
      list_element.append(row_tmp());

      $(this.parentElement.parentElement).find("div[el=div_list_element] form").last().find("button[el=btn_delete_element]").click(function() {
        $(this).closest("form").remove();
        return false;
      });

      $(this.parentElement.parentElement).find("div[el=div_list_element] form").last().find("button[el=btn_value_element]").click(function() {
        var type = $(this.parentElement.parentElement).find("select[name=type] option:selected").val();
        if (type == '20' || type == '21' || type == '60' || type == '90' || type == '91' || type == '80' || type == '120') {
          var el_target = $(this).attr("el-target");
          if (type == '20' || type == '21') {
            $("#tb_modal_value").html("");
            $("#modal_value").modal();
            $("#modal_value input[name=input_el_target]").val(el_target);
            var val = $("#"+ el_target).val();
            if (val != "") {
              var json = JSON.parse(val);
              _.each(json, function(item){
                var tb = $("#tb_modal_value");
                var tr = $("<tr>");
                var td = "<td><input name='text' class='form-control input-sm' value='"+item.text+"'></td>";
                td += "<td><input name='value' class='form-control input-sm' value='"+item.val+"'></td>"
                tr.append(td)
                var btn_tool = $("<button>",{
                  text: "-",
                  class: "btn btn-danger btn-sm"
                }).click(function(){
                  var tr = $(this).closest("tr").remove();
                });
                var tool_td = $("<td>")
                tool_td.append(btn_tool);
                tr.append(tool_td);
                tb.append(tr)
              });
            }
          }
          if (type == '60') {
            $("#modal_image").find(".modal-body input[name=input_el_target]").val(el_target);
            $("#frm_modal_image").html("===+==");
            $("#modal_image").modal();
          }
          if (type == '90' || type == '91') {
            var form = $("#frm_combofile");
            form.find("[name=input_el_target]").val(el_target);
            form.find("select[name=sl_combofile]").val('');
            $("#modal_combofile").modal();
          }

          if (type == '80') {
            $("#tb_modal_table").html("");
            $("#modal_table").modal();
            var el_cols = $(this).closest("form").find("[name=cols]");
            var val = el_cols.val()
            var id = el_cols.attr("id");
            $("#modal_table input[name=input_el_target]").val(id);
            if (val != "") {
              var json = JSON.parse(val);
              _.each(json, function(item){
                var tb = $("#tb_modal_table");
                var tr = $("<tr>");
                var td = "<td><input name='id' class='form-control input-sm' value='"+item.id+"'></td>";
                td += "<td><input name='name' class='form-control input-sm' value='"+item.name+"'></td>"
                var sl = "<select name='type' class='form-control input-sm'>";
                sl += "<option value='text'>Text</option>";
                sl += "<option value='formula'>Formula</option>";
                sl += "</select>";
                td += "<td>"+sl+"</td>"

                tr.append(td)
                var btn_tool = $("<button>",{
                  text: "-",
                  class: "btn btn-danger btn-sm"
                }).click(function(){
                  var tr = $(this).closest("tr").remove();
                });
                var tool_td = $("<td>")
                tool_td.append(btn_tool);
                tr.append(tool_td);
                tb.append(tr);
                tr.find("select[name=type] option[value="+item.type+"]").attr("selected", "");
              });
            }
          }

          if (type == '120') {

            $("#modal_link_form input[name=input_el_target]").val(el_target);
            $.ajax({
              url: "/get_form",
              type: 'GET',
              dataType: 'json',
              success: function(data){
                jsonCollection = data;
                var sl_link_form = $("#sl_link_form");
                sl_link_form.empty();
                _.each(data, function(form){
                  sl_link_form.append($("<option>",{
                    text: form.name,
                    value: form.aid
                  }));
                });
                $("#modal_link_form").modal();
              }
            })
          }          

        }
        
        return false;
      });

      $(this.parentElement.parentElement)
        .find("div[el=div_list_element] form")
        .last()
        .find("select[name=type]")
        .change(function(){
          var val = $(this).val();

          if (val == '80') {    //Table
            $(this.parentElement.parentElement).find("div[el=1]").hide();
            $(this.parentElement.parentElement).find("div[el=1][element=cols]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=id]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=type]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=col]").show();
            $(this.parentElement.parentElement).find("button[el=btn_value_element]").show();
          } else if (val == '50') {    //  Hidden
            $(this.parentElement.parentElement).find("div[el=1]").hide();
            $(this.parentElement.parentElement).find("div[el=1][element=type]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=name]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=value]").show();
          } else if (val == '70') {    // SubForm
            $(this.parentElement.parentElement).find("div[el=1]").hide();
            $(this.parentElement.parentElement).find("div[el=1][element=cols]").hide();
            $(this.parentElement.parentElement).find("div[el=1][element=id]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=type]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=col]").hide();
          } else {
            $(this.parentElement.parentElement).find("div[el=1]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=cols]").hide();
            $(this.parentElement.parentElement).find("div[el=1][element=id]").hide();
            $(this.parentElement.parentElement).find("div[el=1][element=type]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=name]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=readOnly]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=name]").show();
            $(this.parentElement.parentElement).find("div[el=1][element=visible]").show();
              
            //  40 =  link   ;   60 = img    ; 20  = ComboBox  ; 21 = chkbox
            if (val == '20' || val == '21' || val == '60' || val == '90' || val == '91' || val == '80'  || val == '120') {
              $(this.parentElement.parentElement).find("button[el=btn_value_element]").show();
              $(this.parentElement.parentElement).find("textarea[name=value]").val("");
            } else {
              $(this.parentElement.parentElement).find("button[el=btn_value_element]").hide();
            }

            if (val == '60') {
              $(this.parentElement.parentElement).find("div[el=1][element=name]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=readOnly]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=visible]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=displayName]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=col]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=Search]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=validate]").hide();
            }

            if (val == '40') {
              $(this.parentElement.parentElement).find("div[el=1][element=name]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=readOnly]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=visible]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=displayName]").show();
              $(this.parentElement.parentElement).find("div[el=1][element=col]").show();
              $(this.parentElement.parentElement).find("div[el=1][element=Search]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=validate]").hide();              
            }

            if (val == '100') {
              $(this.parentElement.parentElement).find("div[el=1][element=readOnly]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=visible]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=col]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=value]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=Search]").hide();
              $(this.parentElement.parentElement).find("div[el=1][element=validate]").hide();              
            }
          
          }

          if (val == '130') {
            $(this.parentElement.parentElement).find("div[el=1][element=sum]").show();
          } else {
            $(this.parentElement.parentElement).find("div[el=1][element=sum]").hide();
          }
      }).change();
    });
    $(element_row.children("div").last()).find("div[el=div_tool_row] button[el=btn_delete_row]").click(function() {
      $(this.parentElement.parentElement).remove();
    });

    $(element_row.children("div").last()).find("div[el=div_tool_row] button[el=btn_toggle_row]").click(function() {
      var el = $(this).parent().parent().find("div[el=div_list_element]");
      if(el.is(":visible")){
        el.hide();
        $(this).find("i").attr("class", "glyphicon glyphicon-chevron-down");

      } else {
        el.show();
        $(this).find("i").attr("class", "glyphicon glyphicon-chevron-up");
      }
    });
  });

  $("#add_input_file").click(function(){
    var form = $("#frm_modal_image");

    var image = $("<img>",{
      height: 200,
      width: 300
    });

    var input_file = $("<input>",{
      type: "file",
      accept: "image/*",
      name: "file[]"
    }).change(image, function(evt){
      readURL(this, evt.data);
    });

    form.append(image);
    form.append("<br><br>");
    form.append(input_file);
      
  });

  $("#save_modal_image").click(function(){
    var form = $("#frm_modal_image");
    var imgs = form.find("img");
    var data = [];
    var target = $("#modal_image").find(".modal-body input[name=input_el_target]").val();
    _.each(imgs,function(img){ 
      data.push($(img).attr("src")); 
    });
    $("#"+target).val(JSON.stringify(data));
    $("#modal_image").modal("hide");
  });

  $("#btn_gen_json").click(function() {
    if ( $('#sys_form').val() == '' ) {return false ; }
    var json = formDynamic.getJSON()
    jsonCollection.push({
      json: json,
      id: $('#sys_form').val()
    });

    var config = {};
    var form_config = $("#tabConfig form[role=config]");
    _.each(form_config.serializeArray(),function(item){
      config[item.name] = item.value;
    });

    $.ajax({
      type: "POST",
      url: "/save_json",
      data: {
        id: $('#sys_form').val(),
        json: JSON.stringify(json),
        config: JSON.stringify(config)
      },
      success: function(data){
        alert("บันทึกเสร็จเรียบร้อย")
        $("#element_row").html("");
        $('#sys_form').val("");
        window.location = "index";
      },
      dataType: "json"
    });
  });

  $("#save_config").click(function(){
    var data = formDynamic.getFormValueJson($(this).parent().parent().find("div.modal-body"));
    $.ajax({
      url:  "/create_val",
      dataType: "json",
      type: "post",
      // async: false,
      data: {
        data: JSON.stringify(data)
      },
      // contentType: "multipart/form-data",
      success: function(data) {
        alert("Success");
      },
      error: function (xhr, status, e) {
        console.log('erroe  '+e );
      }
    });
  });

  $("#btn_combofile").click(function() {
    var form = $("#frm_combofile");
    var target = form.find("[name=input_el_target]").val();
    var val = form.find("select[name=sl_combofile]").val();
    $('#'+target).val(val)
    $("#modal_combofile").modal('hide');
  })

  ////////////////  preview 
  $("#btn_preview").click(function() {
    if ( $('#sys_form').val() == '' ) {return false ; }
    var json = formDynamic.getJSON();

    formDynamic.renderDynamicForm(json, "#main");
    if(!$.isEmptyObject(json.subform)){
      formDynamic.renderSubForm(json.subform, "#subform");
    }
    $('.date').datepicker({orientation: "top auto", format: "yyyy-mm-dd"});
    $('.color').colorpicker();
    
    $("#saveModal").modal("show");      
    $("#tab_main_form").click() ;

  });

});