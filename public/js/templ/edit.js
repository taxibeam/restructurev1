var test ;
var aid = getParameterByName("aid");
var role_data  = {} ;
var editForm = (function(){
  
  var init = function(){

if ( aid !== 'add') {
    $.ajax({
      type: "get",
      url: "/templ/get" ,// "/get_dataform",
      data: {
        aid: aid
      },
      async: false,
      success: function(data){
        data = data[0] ;     
        test = data ;  
        var json = data.json[0];
        $("#sys_idform").val( aid );
        $("#sys_form").val(data.name);
 //       console.log( json ) ;
        _.each(json.rows, function(row, i){
   //       console.log( row ) ;
          $("#add_row").click();
          var el_row = $("#element_row");          
          var childrens = el_row.children();
          childrens.find("div[el=div_list_element]").sortable();
          _.each(row, function(r, j){

            var btn_row = $(childrens[i]).find("[el=btn_add_element]");
            btn_row.click();
            var element_rows = $(childrens[i]).children("[el=div_list_element]").children();
            $(element_rows[j]).find("select[name=type] option[value="+r.type+"]").attr("selected", true);
            $(element_rows[j]).find("select[name=type]").change();
            $(element_rows[j]).find("input[name=name]").val(r.name);
            $(element_rows[j]).find("input[name=displayName]").val(r.displayName);
            $(element_rows[j]).find("select[name=readOnly] option[value="+r.readOnly+"]").attr("selected", true);
            $(element_rows[j]).find("select[name=visible] option[value="+r.visible+"]").attr("selected", true);
            $(element_rows[j]).find("input[name=col]").val(r.col);
            
            if (r.sum == '1') {
              $(element_rows[j]).find("input[type=checkbox][name=sum]").attr("checked", '');
            }
            
            if (r.title == '1') {
              $(element_rows[j]).find("input[type=checkbox][name=title]").attr("checked", '');
            }
            var val = r.value;
            if (typeof val == 'object') {
              if (typeof val[0] != "object") {
                if(r.type == '60'){
                  val = JSON.stringify(val);
                } else {
                  val = val[0]  
                }
                
              } else {
                val = JSON.stringify(val);
                if (val == "[\"\"]") {val=""};
              }
            }
            $(element_rows[j]).find("textarea[name=value]").val(val);
            $(element_rows[j]).find("input[name=id]").val(r.id);
            $(element_rows[j]).find("input[name=cols]").val(JSON.stringify(r.cols));

          });
        });

        if (!isEmpty(data.config)) {
          var form = $("#tabConfig form[role=config]");
          form.find("input[name=opacity]").val(data.config.opacity);
          form.find("input[name=line_width]").val(data.config.line_width);
          form.find("input[name=color_fill]").val(data.config.color_fill);
          form.find("input[name=color_stroke]").val(data.config.color_stroke);
          form.find("textarea[name=popup]").val(data.config.popup);
          
          $("#folder_icon").find("option[value='"+data.config.folder_icon+"']").attr('selected', '');
          $("#folder_icon").change();
          $("#icon_url").find("option[value='"+data.config.icon_url+"']").attr('selected', '');
          $("#icon_url").change();
        }
        
        // if (!isEmpty(data.json.subform)) {

        //   $("#add_row").click();
        //   var subform = data.json.subform;
        //   var el_row = $("#element_row");
        //   var row = el_row.children().last();
        //   var btn_row = row.find("[el=btn_add_element]");
        //   btn_row.click();
        //   element_rows = row.children("[el=div_list_element]").children().last();
        //   element_rows.find("select[name=type] option[value="+subform.type+"]").attr("selected", true);
        //   element_rows.find("select[name=type] option[value="+subform.type+"]").change();
        //   element_rows.find("input[name=id]").val(subform.id);
        // }
        
      },
      dataType: "json"
    });

  } else  {
   var gen_id = Date.now() ;
         $("#sys_idform").val( gen_id );
      //  $("#sys_form").val( ' Name ');
  }
//   aid 

//     role
      $.ajax({
        type: "POST",
        url: "/service/users/getusergr",        
        success: function(data){                    
          var tb = $("#tb_role_table");          
           //role_data  = {}
              role_data.gr = []
                  _.each(data.data, function(row, i){
                    var tr = $("<tr>"); 

                     role_data.gr[i] = row.group ;
                     var td = "<td><b>"+ row.group +"</b></td>"
                     td +=  "<td>"+"<input type=\"checkbox\" id="+row.group+"_0"  +"></td>" ;
                     td +=  "<td>"+"<input type=\"checkbox\" id="+row.group+"_1"  +"></td>" ;
                     td +=  "<td>"+"<input type=\"checkbox\" id="+row.group+"_2"  +"></td>" ;
                     td +=  "<td>"+"<input type=\"checkbox\" id="+row.group+"_3"  +"></td>" ;
                     td +=  "<td>"+"<input type=\"checkbox\" id="+row.group+"_4"  +"></td>" ;
                     tr.append(td)
                     tb.append(tr)
                   });

        },
        dataType: "json"
      });


    $("#btn_update_json").click(function() {
      if ( $('#sys_form').val() == '' ) {return false ; }
      var json = formDynamic.getJSON();
      var config = {};
     // var groupacc=[] ;
     // var name = "Taxi Approve"
     // var url = "/service/taxi/listall"
     // var coll = "drivers" 

               var groupacc={};               
               var roleaccess = {} ;
                role_data.access=[] ;
              _.each(role_data.gr, function(row, i){
                roleaccess ={}
                role_data.access[i]=""
                for (var j=0 ; j<=4 ; j++ ){  
                   if ( $('#'+row+"_"+j).prop('checked') ) {      
                      role_data.access[i] += '1' ;
                   } else {
                     role_data.access[i] += '0' ;
                   }
                }
                if ( role_data.access[i] !== "00000" ) {
                  groupacc[row] = role_data.access[i]                   
                 }

              })
       
      var form_config = $("#tabConfig form[role=config]");
      _.each(form_config.serializeArray(),function(item){
        config[item.name] = item.value;
      });
console.log(groupacc)
      $.ajax({
        type: "POST",
        url: "/update_json",
        data: {
          id: $('#sys_form').val(),
          groupacc : JSON.stringify(groupacc),
          name : $('#sys_form').val() ,
          url : "/service/taxi/listall",
          coll : "drivers" ,          
          json: JSON.stringify(json),
          aid: aid,
          config: JSON.stringify(config)
        },
        success: function(data){
          alert("บันทึกเสร็จเรียบร้อย")
          window.location = "index"
        },
        dataType: "json"
      });
    });


$(".opt_ext").click(function() {  
    var tid = this.id
//    tid = 'btnopt_1456129757861'
    tid = tid.substr(7, tid.length ) 
     if ( $('#opt_'+tid).is(':visible') ) {
        $('#opt_'+tid).hide() ;
        this.value="v"
      } else {
         $('#opt_'+tid).show() ;         
         this.value="^"
      }

     
 }) ;

  };

  return {
    init: init
  };

})();